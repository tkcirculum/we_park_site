var module_object = {
  
create: function( data, parent_element, placement ) {


  var component_id = null;


  var local_park_list = [];

  var local_special_places_tags = [];
  we_local_arr.local_park_list = local_park_list;

  window.all_pp_all_data = null;

  get_park_list('zovem get park list unutar create PONUDA modul')
  .then(
  function() {

    if ( 
      window.all_pp_all_data             && /* ako postoji lista parkinga */
      $.isArray(window.all_pp_all_data)  && /* ako je lista array */
      window.all_pp_all_data.length > 0  && /* ako array nije pazan */
      cit_user                              /* ako postoji user */

    ) {

      $.each(window.all_pp_all_data, function(pp_ind, park) {
        local_park_list.push({ sifra: park.pp_sifra, naziv: park.pp_name });

        $.each(park.special_places, function(sp_ind, spec_place) {

        });


      });

      we_local_arr.local_park_list = local_park_list;

    };

  },
  function(error) {
    console.error('Greška prilikom get park list unutar PRODUCTS / PONUDA modul');
    console.error(error);
  });      


  var component_html =
  `
<div id="products_component" class="main_module_component">

<div class="row" style="margin-top: 30px; display: flex; justify-content: center;">
  <div class="col-md-12 col-sm-12">
    <h6 id="ponuda_big_title_monthly" style="color: #a2a2a2; text-align: center; margin-bottom: 0;">Mjesečna ili VIP pretplata</h6>
  </div>
</div>  


<div class="row" style="margin-top: 10px; display: flex; justify-content: center;">

  <div class="col-md-3 col-sm-12">
    <div id="ponuda_pp_list_label" class="cit_label">PARKIRALIŠTE</div>
    <input id="ponuda_pp_list" type="text" class="cit_input we_auto same_width" autocomplete="off"> 
  </div>


  <div class="col-md-3 col-sm-12">
    <div id="select_ponuda_user_label" class="cit_label">USER:</div>
    <input id="select_ponuda_user" type="text" class="cit_input we_auto simple" autocomplete="off">
  </div>


  <div class="col-md-3 col-sm-12">
    <div id="ponuda_special_places_label" class="cit_label">GRUPA KORISNIKA</div>
    <input id="ponuda_special_places" type="text" class="cit_input we_auto same_width" autocomplete="off"> 
  </div>


</div>  



<div class="row" style="margin-top: 10px; display: flex; justify-content: center;">

  <div class="col-md-3 col-sm-12" style="text-align: center;">
DOSTUPNOST:
    <br>
    <div id="ponuda_dostupno" style="width: 100%; height: auto; display: flex; align-items: center; justify-content: center;">--</div>
    <br>
NAJPRIJE ZAVRŠAVA:
    <br>
    <div id="ponuda_first_end"
         style="font-size: 24px;
                letter-spacing: 0.05rem;
                color: #238eff;
                text-align: center;
                font-weight: 700;
                margin-bottom: 30px;">--</div>

  </div>

</div> 



<div class="row" style="margin-top: 30px;" id="big_ponuda_table_row">


  <div class="col-md-12 col-sm-12" id="big_ponuda_table_box" style="padding: 0;">

<!--

OVO ĆU KORISTITI KASNIJE ZA FINANCIJSKI REPORT !!!!!

    <div class="cit_big_table_header_row">
      <div class="big_table_col stats_grupa_col">GRUPA <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
      <div class="big_table_col stats_donos_col">DONOS <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
      <div class="big_table_col stats_uplaceno_col">UPLAĆENO <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
      <div class="big_table_col stats_potroseno_col">POTROŠENO <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
      <div class="big_table_col stats_saldo_col">SALDO <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
    </div>

    <div class="cit_big_table_row">
      <div class="big_table_col stats_donos_col">VOUCHER</div>
      <div class="big_table_col stats_donos_col">2.545,40</div>
      <div class="big_table_col stats_uplaceno_col">3.057,32</div>
      <div class="big_table_col stats_potroseno_col">2.057,32</div>
      <div class="big_table_col stats_saldo_col">1.483,66</div>
    </div>

    <div class="cit_big_table_row">
      <div class="big_table_col stats_donos_col">MJESEČNA KARTA</div>
      <div class="big_table_col stats_donos_col">5.545,40</div>
      <div class="big_table_col stats_uplaceno_col">3.057,32</div>
      <div class="big_table_col stats_potroseno_col">9.057,32</div>
      <div class="big_table_col stats_saldo_col" style="color: red;"> -1.483,66</div>
    </div>        
-->

  </div>

</div>

<div class="row" style="display: flex; justify-content: center;">

  <div class="col-md-3 col-sm-12">
    <div id="ponuda_nadoplata_iznos_label" class="cit_label" style="text-align: center; padding: 0;">IZNOS:</div>
    <input id="ponuda_nadoplata_iznos" type="text" class="cit_input" autocomplete="off" style="text-align: center;">          
  </div> 

</div>  

<div class="row" style="display: flex; justify-content: center;">

  <div class="col-md-3 col-sm-12">
    <div id="ponuda_popust_label" class="cit_label" style="text-align: center; padding: 0;">POPUST</div>
    <input id="ponuda_popust" type="text" class="cit_input" autocomplete="off" style="text-align: center;">          
  </div> 

</div>  


<div class="row" style="display: flex; justify-content: center; margin-bottom: 30px;">

  <div class="col-md-3 col-sm-12">
    <div id="ponuda_price_label" class="cit_label" style="text-align: center; padding: 0;">CIJENA:</div>
    <input id="ponuda_price" type="text" class="cit_input" autocomplete="off" style="text-align: center;">          
  </div> 

</div>
  

  

<div class="row" style="display: flex; justify-content: center;">

  <div class="col-md-3 col-sm-12">
    <div id="ponuda_from_date_label" class="cit_label">Datum OD:</div>
    <input id="ponuda_from_date" type="text" class="cit_input" autocomplete="off">          
  </div> 

  <div class="col-md-3 col-sm-12">
    <div id="ponuda_broj_dana_label" class="cit_label">BROJ DANA:</div>
    <input id="ponuda_broj_dana" type="text" class="cit_input" autocomplete="off">          
  </div>     

  <div class="col-md-3 col-sm-12">
    <div id="ponuda_to_date_label" class="cit_label">Datum DO:</div>
    <input id="ponuda_to_date" type="text" class="cit_input" autocomplete="off">          
  </div>

</div>




<div class="row" style="margin-top: 10px; margin-bottom: 30px; display: flex; justify-content: center;">

        <div class="col-md-2 col-sm-12" style="display: flex; justify-content: center;">

        <div class="cit_switch_wrapper">

          <div id="ponuda_vip_sub_label" class="cit_label">VIP PRETPLATA</div>
          <div id="ponuda_vip_sub" class="cit_switch off">
                <div class="switch_circle"></div>
                <div class="on_text switch_text">DA</div>
                <div class="off_text switch_text">NE</div>
          </div>
        </div>

      </div> 

</div>  


<div class="row" style="display: flex; align-items: center; justify-content: center; margin-top: 30px;">

  <div class="col-md-3 col-sm-12">

    <div id="sub_create_btn" 
         class="cit_button cit_center cit_tooltip" 
         style="width: 100%; float: none; margin: 20px auto;"
         data-toggle="tooltip" data-placement="bottom"
         title="KREIRAJ PRETPLATU">

    KREIRAJ PRETPLATU

    </div>

  </div>

</div>  



<div class="row" style="margin-top: 10px; display: flex; justify-content: center;">
  <div class="col-md-12 col-sm-12">
    <h6 id="ponuda_big_title_nadoplata" style="color: #a2a2a2; text-align: center; padding: 0;">Nadoplata</h6>
  </div>
</div>



<div class="row" style="display: flex; align-items: center; justify-content: center; margin-top: 0px;">

  <div class="col-md-3 col-sm-12">

    <div id="nadoplata_create_btn" 
         class="cit_button cit_center cit_tooltip" 
         style="width: 100%; float: none; margin: 20px auto;"
         data-toggle="tooltip" data-placement="bottom"
         title="NADOPLATI UPISANI IZNOS NA RAČUN IZABRANOG KORISNIKA">

    NADOPLATA

    </div>

  </div>

</div>    

<div class="row" style="display: flex; justify-content: center;">

  <div class="col-md-12 col-sm-12">

    <div id="ponuda_clipboard_label" class="cit_label" 
        style=" float: none;
                height: auto;
                width: 80%;
                max-width: 250px;
                text-align: center;
                margin: 30px auto 0;">
      Klikom na polje ispod se automatski kopiraju u clipboard.
    </div>


    <textarea id="ponuda_multi_vouchers" 
    style=" text-align: center;
            border: 1px solid #b4b5b6;
            padding: 2px 10px;
            color: #555;
            background-color: #ffffff;
            border-radius: 13px;
            font-size: 20px;
            font-weight: 700;
            width: 250px;
            display: block;
            height: 100px;
            margin: 0 auto;"></textarea>

  </div>

</div> 



<div class="row" style="display: flex; justify-content: center; margin-top: 10px;">

  <div class="col-md-3 col-sm-12">
    <div id="voucher_how_many_label" class="cit_label" style="text-align: center; padding: 0;">KOLIKO VOUCHERA?</div>
    <input id="voucher_how_many" type="text" class="cit_input" autocomplete="off" style="text-align: center;" >          
  </div>  

</div>    
  
  
<div class="row" style="display: flex; justify-content: center; margin-top: 10px;">

  <div class="col-md-3 col-sm-12">
    <div id="opis_vouchera_label" class="cit_label" style="text-align: center; padding: 0;">OPIS VOUCHERA:</div>
    <input id="opis_vouchera" type="text" class="cit_input" autocomplete="off" style="text-align: center;" >          
  </div>  

</div>   


<div class="row" style="display: flex; align-items: center; justify-content: center; margin-top: 0px;">

  <div class="col-md-3 col-sm-12">

    <div id="voucher_create_btn" 
         class="cit_button cit_center cit_tooltip" 
         style="width: 100%; float: none; margin: 20px auto;"
         data-toggle="tooltip" data-placement="bottom"
         title="KREIRAJ BROJEVE VOUCHERA">

    KREIRAJ VOUCHERE

    </div>

  </div>

</div>   



<!-- KREIRANJE POPUSTA !!! -->

<!-- KREIRANJE POPUSTA !!! -->



<div class="row" style="display: flex; justify-content: center; margin-top: 40px;">

  <div class="col-md-12 col-sm-12">

    <div id="popust_clipboard_label" class="cit_label" 
        style=" float: none;
                height: auto;
                width: 80%;
                max-width: 250px;
                text-align: center;
                margin: 30px auto 0;">
      Klikom na polje ispod se automatski kopiraju u clipboard.
    </div>


    <textarea id="ponuda_multi_popusti" 
    style=" text-align: center;
            border: 1px solid #b4b5b6;
            padding: 2px 10px;
            color: #555;
            background-color: #ffffff;
            border-radius: 13px;
            font-size: 20px;
            font-weight: 700;
            width: 250px;
            display: block;
            height: 100px;
            margin: 0 auto;"></textarea>

  </div>

</div> 



<div class="row" style="display: flex; justify-content: center; margin-top: 10px;">

  <div class="col-md-3 col-sm-12">
    <div id="popust_how_many_label" class="cit_label" style="text-align: center; padding: 0;">KOLIKO KOMADA POPUSTA?</div>
    <input id="popust_how_many" type="text" class="cit_input" autocomplete="off" style="text-align: center;" >          
  </div>
  
</div>   

<div class="row" style="display: flex; justify-content: center; margin-top: 10px;">  
  
  <div class="col-md-3 col-sm-12">
    <div id="popust_perc_label" class="cit_label" style="text-align: center; padding: 0;">POSTOTAK POPUSTA:</div>
    <input id="popust_perc" type="text" class="cit_input" autocomplete="off" style="text-align: center;" >          
  </div>  

</div>    
  
  

<div class="row" style="display: flex; justify-content: center; margin-top: 10px;">  
  
  <div class="col-md-3 col-sm-12">
    <div id="opis_popusta_label" class="cit_label" style="text-align: center; padding: 0;">OPIS POPUSTA:</div>
    <input id="opis_popusta" type="text" class="cit_input" autocomplete="off" style="text-align: center;" >          
  </div>  

</div>  

<div class="row" style="display: flex; align-items: center; justify-content: center; margin-top: 0px;">

  <div class="col-md-3 col-sm-12">

    <div id="popust_create_btn" 
         class="cit_button cit_center cit_tooltip" 
         style="width: 100%; float: none; margin: 20px auto;"
         data-toggle="tooltip" data-placement="bottom"
         title="KREIRAJ ŠIFRE POPUSTA">

    KREIRAJ POPUSTE

    </div>

  </div>

</div>   





</div> <!--KRAJ MAIN COMPONENT DIV ZA PONUDE-->

  `;

// ovo je arbitrarni uvijet  - uzeo sam prvi label u formi kao uvijet kad se pojavi ....
  // dodatno čekam da se generira lokalna lista svih parkirališta
wait_for( `$('#select_ponuda_user').length > 0 && we_local_arr.local_park_list.length > 0`, function() {

  var this_module = window[module_url]; 

  $('.cit_tooltip').tooltip();


  $('#select_ponuda_user').data('we_auto', {

    desc: 'za odabir usera/customera u komponenti za ponude tj products !!!',
    url: '/search_customers',
    find_in: null,
    col_widths: [0, 15, 20, 20, 15, 15, 15, 0],
    return: { _id: 1, user_number: 1, email: 1, name: 1, user_tel: 1, full_name: 1, business_name: 1, 'customer_tags': 1 },
    hide_cols: ['_id', 'customer_tags'],
    query: {},
    show_on_click: false,
    list_width: 1000,

  });


  $('#ponuda_pp_list').data('we_auto', {
    desc: 'za odabir parkinga u ponuda module za kreiranje pretplata ili nadoplate',
    local: true,
    return: {},
    show_on_click: true,
    list: 'local_park_list',
  });


  $('#sub_create_btn').off('click', this_module.sub_create);
  $('#sub_create_btn').on('click', this_module.sub_create);

  $('#nadoplata_create_btn').off('click', this_module.nadoplata_create);
  $('#nadoplata_create_btn').on('click', this_module.nadoplata_create);

  $('#voucher_create_btn').off('click', this_module.voucher_create);
  $('#voucher_create_btn').on('click', this_module.voucher_create);
  
  
  $('#popust_create_btn').off('click', this_module.popust_create);
  $('#popust_create_btn').on('click', this_module.popust_create);
  

  $('#ponuda_from_date').off('blur', this_module.set_ponuda_dates);
  $('#ponuda_from_date').on('blur', this_module.set_ponuda_dates);

  $('#ponuda_to_date').off('blur', this_module.set_ponuda_dates);
  $('#ponuda_to_date').on('blur', this_module.set_ponuda_dates);

  $('#ponuda_broj_dana').off('blur', this_module.set_ponuda_dates);
  $('#ponuda_broj_dana').on('blur', this_module.set_ponuda_dates);

  
  
  
  $('#ponuda_multi_vouchers').off('click', this_module.select_all_voucher_nums);
  $('#ponuda_multi_vouchers').on('click', this_module.select_all_voucher_nums);

  $('#ponuda_multi_popusti').off('click', this_module.select_all_popust_nums);
  $('#ponuda_multi_popusti').on('click', this_module.select_all_popust_nums);
  
  $('#ponuda_nadoplata_iznos').off('blur', this_module.set_nadoplata);
  $('#ponuda_nadoplata_iznos').on('blur', this_module.set_nadoplata);
  
  
  $('#ponuda_popust').off('blur', this_module.set_nadoplata);
  $('#ponuda_popust').on('blur', this_module.set_nadoplata);
  
  
  $('#ponuda_price').off('blur', this_module.set_ponuda_price);
  $('#ponuda_price').on('blur', this_module.set_ponuda_price);


}, 500000);

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  }; 

  return {
    html: component_html,
    id: component_id
  }

},

scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  
  var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;

  var one_day_in_ms = 1000*60*60*24;

  function get_empty_cit_data() {

    this_module.cit_data = {
      
      sifra: null,
      pp_sifra: null,
      code: null,
      amount: null,
      desc: '',
      start_time: null,
      user_number: null,
      is_free: false,
      price: null,
      type: null,
      valid_from: null,
      valid_to: null,
      balance: null,
      buy_time: null,
      tag: null,
      email: null,
      how_many: 1,
      discount: 0,
      days: null,
      DUMMY_VOUCHER_ID: true,
      
      
    };  

    // --------------- automatski popunjavam polja u pouda modulu sa default vrijednostima !!!!
    // prvo pričekaj da se pojavi bilo koji input u stats formi !!!
    wait_for(`$('#ponuda_from_date').length > 0`, function() {

      var time_now = Date.now();

      $('#ponuda_from_date').val( getMeADateAndTime( time_now, 'd.m.y').datum );
      $('#ponuda_to_date').val( getMeADateAndTime( time_now + one_day_in_ms*30.5, 'd.m.y').datum ); // dodajem još pola dana tako da bude 30.5 dana

      $('#ponuda_broj_dana').val(30.5);
      
      var ponuda_from_date = $('#ponuda_from_date');
      var ponuda_to_date = $('#ponuda_to_date');

      var start_time = get_DATE_as_milisec(ponuda_from_date).ms_current_time;
      var end_time = get_DATE_as_milisec(ponuda_to_date).ms_current_time; 

      this_module.cit_data.valid_from = start_time;
      this_module.cit_data.valid_to = end_time;

    }, 5000000);

  };
  this_module.get_empty_cit_data = get_empty_cit_data;
  
  
  function ponuda_buttons_clickable(true_or_false) {
    
    if ( true_or_false == false ) {
      
      $('#sub_create_btn').css({ 'opacity': '0.3', 'pointer-events': 'none' });
      $('#nadoplata_create_btn').css({ 'opacity': '0.3', 'pointer-events': 'none' });
      $('#voucher_create_btn').css({ 'opacity': '0.3', 'pointer-events': 'none' });
      $('#popust_create_btn').css({ 'opacity': '0.3', 'pointer-events': 'none' });
      
    } else {
      
      $('#sub_create_btn').css({ 'opacity': '1', 'pointer-events': 'all' });
      $('#nadoplata_create_btn').css({ 'opacity': '1', 'pointer-events': 'all' });
      $('#voucher_create_btn').css({ 'opacity': '1', 'pointer-events': 'all' });
      $('#popust_create_btn').css({ 'opacity': '1', 'pointer-events': 'all' });
      
    };
    
  };
  
  function is_vip_sub(data) {
    if ( data == true ) {
      this_module.cit_data.type = 'vip';
    } else {
      this_module.cit_data.type = null;
    };
    
  };
  this_module.is_vip_sub = is_vip_sub;
  
  
  function set_nadoplata() {

    var ponuda_nadoplata_iznos = $('#ponuda_nadoplata_iznos').val();
    
    if ( ponuda_nadoplata_iznos !== '' && $.isNumeric(ponuda_nadoplata_iznos) == false ) {
      alert('Potrebno upisati ispravan iznos nadoplate !!!');
      return;
    };
    
    var ponuda_popust = $('#ponuda_popust').val();
    
    if (  ponuda_popust !== '' && $.isNumeric(ponuda_popust) == false ) {
      alert('Potrebno upisati ispravan popust!!!\n\nNa primjer: 10, 20 ili 30 bez znaka za postotak');
      return;
    } else if ( ponuda_popust == '' ) {
      ponuda_popust = 0;
    };    
  
    var price = Number(ponuda_nadoplata_iznos) * (100 - Number(ponuda_popust) )/100;
    this_module.cit_data.price = zaokruzi( price, 2);
    
    $('#ponuda_price').val( this_module.cit_data.price );
    
  };
  this_module.set_nadoplata = set_nadoplata;
  
  
 function set_ponuda_price() {

    var ponuda_price = $('#ponuda_price').val();
    
    if ( ponuda_price !== '' && $.isNumeric(ponuda_price) == false ) {
      alert('Potrebno upisati ispravan iznos cijene !!!');
      return;
    };
    
    var ponuda_popust = $('#ponuda_popust').val();
    
    if (  ponuda_popust !== '' && $.isNumeric(ponuda_popust) == false ) {
      alert('Potrebno upisati ispravan popust!!!\n\nNa primjer: 10, 20 ili 30 bez znaka za postotak');
      return;
    } else if ( ponuda_popust == '' ) {
      ponuda_popust = 0;
    };    
  
    var iznos = Number(ponuda_price) / (100 - Number(ponuda_popust) )*100;
    this_module.cit_data.amount = zaokruzi( iznos, 2);
    
    $('#ponuda_nadoplata_iznos').val( this_module.cit_data.amount );
    
  };
  this_module.set_ponuda_price = set_ponuda_price;
    
  
  function create_products_ajax() {
    
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "POST",
      url: "/create_products",
      data: JSON.stringify(this_module.cit_data),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {
      
      console.log(response);
      
      ponuda_buttons_clickable(true);
      
      if ( response.success == true ) {
  
        var popup_text = 'RADNJA JE USPJEŠNA !!!';
        show_popup_modal(true, popup_text, null, null );
        setTimeout( function() { show_popup_modal(false, popup_text, null, null ); }, 2*1000);
  
        
        // ako je ovo false to znači da sam htio napraviti vouchera sa pravim kodovima za upisivanje
        if (this_module.cit_data.DUMMY_VOUCHER_ID == false) {
          
          var voucheri_text = '';
          $.each(response.saved_vouchers, function(v_index, voucher) {
            voucheri_text += voucher.code + '\n';
          });
          $('#ponuda_multi_vouchers').text(voucheri_text);
          
        };
        
        // ako je ovo true znači da trebam kodove za popust
        if (this_module.cit_data.is_discount == true) {
          
          var dis_text = '';
          $.each(response.saved_discounts, function(d_index, dis_obj) {
            dis_text += dis_obj.dis_code + '\n';
          });
          $('#ponuda_multi_popusti').text(dis_text);
          
        };
        
        
        
      } 
      else {
        // ako je success == false
        if ( response.msg && typeof window[response.msg] == 'function' ) {
         window[response.msg]();
          
        } else if (response.msg) {
          
          var popup_text = response.msg;
          show_popup_modal('error', popup_text, null, 'ok' );
          $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
        }
        else {
          var popup_text = 'Došlo je do greške prilikom pozivanja ove akcije!';
          show_popup_modal('error', popup_text, null, 'ok' );
          $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
        };
        
      };
      
    })
    .fail(function(error) {
      
      ponuda_buttons_clickable(true);
      
      var popup_text = 'Došlo je do greške u zahtjevu!';
      show_popup_modal('error', popup_text, null, 'ok' );
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
      
    });    

        
    
    
  };
  this_module.create_products_ajax = create_products_ajax;
  
  function sub_create() {
    
    
    /*
    OVO SU PODACI ZA SLANJE AJAXOM
    ____________________________
    sifra: null,
    pp_sifra: null,
    code: null,
    amount: null,
    desc: '',
    start_time: null,
    user_number: null,
    is_free: false,
    price: null,
    type: null,
    valid_from: null,
    valid_to: null,
    balance: null,
    buy_time: null,
    tag: null,
    email: null,
    how_many: 1,
    discount: 0,
    days: null,
    DUMMY_VOUCHER_ID: true,

    
    */
    
    
    // -------VAŽNO !!!-------------- naštimaj datume i dane
    // -------VAŽNO !!!-------------- naštimaj datume i dane
    
    this_module.set_ponuda_dates();
    
    
    // -------VAŽNO !!!-------------- naštimaj AMOUNT I PRICE 
    this_module.set_nadoplata();
    
    
    // ako je user kliknuo gumb za pretplatu , a nije označeno da je VIP 
    // onda mora biti mjesečna !!!!
    if ( this_module.cit_data.type == null ) this_module.cit_data.type = 'monthly';
    
    this_module.cit_data.sifra = Date.now();

    if ( !this_module.cit_data.pp_sifra ) {
      alert('Potrebno odabrati parkiralište!!!');
      return;
    };

    if ( !this_module.cit_data.user_number ) {
      alert('Potrebno odabrati korisnika!!!');
      return;
    };

    
    var ponuda_broj_dana = $('#ponuda_broj_dana').val();
    if ( ponuda_broj_dana == '' || ( ponuda_broj_dana !== '' && $.isNumeric(ponuda_broj_dana) == false )  ) {
      
      $('#ponuda_broj_dana').val(30.5);
      alert('Potrebno upisati ispravan broj dana za pretplatu !!!\n\nAplikacija je sada postavila broj dana na 30.5 !!!');
      
      return;
    };
    
    this_module.cit_data.days = Number(ponuda_broj_dana);
    
    
    var ponuda_nadoplata_iznos = $('#ponuda_nadoplata_iznos').val();
    
    if ( ponuda_nadoplata_iznos !== '' && $.isNumeric(ponuda_nadoplata_iznos) == false ) {
      alert('Potrebno upisati ispravan iznos nadoplate !!!');
      return;
    };
    
    
    if ( Number(ponuda_nadoplata_iznos) == 0 || ponuda_nadoplata_iznos == '' ) {
      alert('Potrebno upisati ispravan iznos nadoplate !!!');
      return;
    };
    

    
    var ponuda_popust = $('#ponuda_popust').val();
    if ( ponuda_popust !== '' && $.isNumeric(ponuda_popust) == false ) {
      alert('Potrebno upisati ispravan popust!!!\n\nNa primjer: 10, 20 ili 30 bez znaka za postotak');
      return;
    } else if ( ponuda_popust == '' ) {
      ponuda_popust = 0;
    };    
    
    
    var ponuda_price = $('#ponuda_price').val();
    if ( ponuda_price == '' || ( ponuda_price !== '' && $.isNumeric(ponuda_price) == false )  ) {
      alert('Potrebno upisati ispravnu cijenu!!!');
      return;
    };
    
    

    this_module.cit_data.amount = Number(ponuda_nadoplata_iznos);
    this_module.cit_data.discount = zaokruzi( Number(ponuda_popust), 2);
    
    
    ponuda_buttons_clickable(false);
    clearInterval(window.timestamp_ponuda_ajax_id);
    window.timestamp_ponuda_ajax_id = setTimeout( function() {
      this_module.create_products_ajax();
    }, 400);
    
        
  };
  this_module.sub_create = sub_create;
  
  
  function nadoplata_create() {
    
    // ako sam već izabrao usera onda ga spremi u temp
    var temp_user_number = this_module.cit_data.user_number;
    
    // resetiraj sve podatke
    this_module.get_empty_cit_data();
    
    // nako resetiranja cit data vrati user number
    this_module.cit_data.user_number = temp_user_number;
    
    /*
    OVO SU PODACI ZA SLANJE AJAXOM
    ____________________________
    sifra: null,
    pp_sifra: null,
    code: null,
    amount: null,
    desc: '',
    start_time: null,
    user_number: null,
    is_free: false,
    price: null,
    type: null,
    valid_from: null,
    valid_to: null,
    balance: null,
    buy_time: null,
    tag: null,
    email: null,
    how_many: 1,
    discount: 0,
    days: null,
    DUMMY_VOUCHER_ID: true,

    
    */
    
    
    // onda mora biti  !!!!
    this_module.cit_data.type = 'voucher';
    this_module.cit_data.pp_sifra = null;
    this_module.cit_data.sifra = Date.now();
    this_module.cit_data.days = null;
    
    
    if ( !this_module.cit_data.user_number ) {
      alert('Potrebno odabrati korisnika!!!');
      return;
    };

    
    var ponuda_price = $('#ponuda_price').val();
    if ( ponuda_price == '' || ( ponuda_price !== '' && $.isNumeric(ponuda_price) == false )  ) {
      alert('Potrebno upisati ispravnu cijenu!!!');
      return;
    };
    
    
    var ponuda_popust = $('#ponuda_popust').val();
    if ( ponuda_popust !== '' && $.isNumeric(ponuda_popust) == false ) {
      alert('Potrebno upisati ispravan popust!!!\n\nNa primjer: 10, 20 ili 30 bez znaka za postotak');
      return;
    } else if ( ponuda_popust == '' ) {
      ponuda_popust = 0;
    };

    
    var ponuda_nadoplata_iznos = $('#ponuda_nadoplata_iznos').val();
    if ( ponuda_nadoplata_iznos !== '' && $.isNumeric(ponuda_nadoplata_iznos) == false ) {
      alert('Potrebno upisati ispravan iznos nadoplate !!!');
      return;
    };

    this_module.cit_data.amount = Number(ponuda_nadoplata_iznos);
    this_module.cit_data.discount = zaokruzi( Number(ponuda_popust), 2);
    
    ponuda_buttons_clickable(false);
    clearInterval(window.timestamp_ponuda_ajax_id);
    window.timestamp_ponuda_ajax_id = setTimeout( function() {
      this_module.create_products_ajax();
    }, 400);
    
    
  };
  this_module.nadoplata_create = nadoplata_create;
  
  
  
  function voucher_create() {

    // ako sam već izabrao usera onda ga spremi u temp
    var temp_user_number = this_module.cit_data.user_number;

    // resetiraj sve podatke
    this_module.get_empty_cit_data();

    // nako resetiranja cit data vrati user number
    this_module.cit_data.user_number = temp_user_number;

    /*
    OVO SU PODACI ZA SLANJE AJAXOM
    ____________________________
    sifra: null,
    pp_sifra: null,
    code: null,
    amount: null,
    desc: '',
    start_time: null,
    user_number: null,
    is_free: false,
    price: null,
    type: null,
    valid_from: null,
    valid_to: null,
    balance: null,
    buy_time: null,
    tag: null,
    email: null,
    how_many: 1,
    discount: 0,
    days: null,
    DUMMY_VOUCHER_ID: true,


    */


    // onda mora biti  !!!!
    this_module.cit_data.type = 'voucher';
    this_module.cit_data.pp_sifra = null;
    this_module.cit_data.sifra = Date.now();
    this_module.cit_data.days = null;

    this_module.cit_data.user_number = null;
    this_module.cit_data.DUMMY_VOUCHER_ID = false;


    var ponuda_price = $('#ponuda_price').val();
    if ( ponuda_price == '' || ( ponuda_price !== '' && $.isNumeric(ponuda_price) == false )  ) {
      alert('Potrebno upisati ispravnu cijenu!!!');
      return;
    };


    var ponuda_popust = $('#ponuda_popust').val();
    if ( ponuda_popust !== '' && $.isNumeric(ponuda_popust) == false ) {
      alert('Potrebno upisati ispravan popust!!!\n\nNa primjer: 10, 20 ili 30 bez znaka za postotak');
      return;
    } else if ( ponuda_popust == '' ) {
      ponuda_popust = 0;
    };


    var ponuda_nadoplata_iznos = $('#ponuda_nadoplata_iznos').val();
    if ( ponuda_nadoplata_iznos !== '' && $.isNumeric(ponuda_nadoplata_iznos) == false ) {
      alert('Potrebno upisati ispravan iznos nadoplate !!!');
      return;
    };



    var voucher_how_many = $('#voucher_how_many').val();
    if ( voucher_how_many == '' || ( voucher_how_many !== '' && $.isNumeric(voucher_how_many) == false) ) {
      alert('Potrebno upisati ispravan broj komada vouchera !!!');
      return;
    };     

    var opis_vouchera = $('#opis_vouchera').val();
    if ( opis_vouchera == '') {
      alert('Potrebno upisati opis vouchera !!!');
      return;
    };


    this_module.cit_data.desc = window.cit_user.name + '_' + getMeADateAndTime(Date.now(), 'y_m_d').datum + '_' + opis_vouchera;
    this_module.cit_data.amount = Number(ponuda_nadoplata_iznos);
    this_module.cit_data.discount = zaokruzi( Number(ponuda_popust), 2);

    this_module.cit_data.how_many = Number(voucher_how_many);  

    ponuda_buttons_clickable(false);
    clearInterval(window.timestamp_ponuda_ajax_id);
    window.timestamp_ponuda_ajax_id = setTimeout( function() {
      this_module.create_products_ajax();
    }, 400);
    

  };
  this_module.voucher_create = voucher_create;
  
  
  
  function popust_create() {
    
    
    
    this_module.cit_data = {
      
      desc: '',
      how_many: 1,
      discount: 0,
      is_discount: true

    };


    var popust_perc = $('#popust_perc').val();
    if ( popust_perc == '' || (popust_perc !== '' && $.isNumeric(popust_perc) == false) ) {
      alert('Potrebno upisati ispravan popust!!!\n\nNa primjer: 10, 20 ili 30 bez znaka za postotak');
      return;
    };


    var popust_how_many = $('#popust_how_many').val();
    if ( popust_how_many == '' || ( popust_how_many !== '' && $.isNumeric(popust_how_many) == false) ) {
      alert('Potrebno upisati ispravan broj komada popusta !!!');
      return;
    };     


    var opis_popusta = $('#opis_popusta').val();
    if ( opis_popusta == '') {
      alert('Potrebno upisati opis popusta !!!');
      return;
    };


    this_module.cit_data.desc = window.cit_user.name + '_' + getMeADateAndTime(Date.now(), 'y_m_d').datum + '_' + opis_popusta;
    this_module.cit_data.discount = zaokruzi( Number(popust_perc), 2);
    this_module.cit_data.how_many = Number(popust_how_many);  

    ponuda_buttons_clickable(false);
    clearInterval(window.timestamp_ponuda_ajax_id);
    window.timestamp_ponuda_ajax_id = setTimeout( function() {
      this_module.create_products_ajax();
    }, 400);
    

  };
  this_module.popust_create = popust_create;
  
  
  function select_all_voucher_nums() {
    /* Get the text field */
    var copyText = document.getElementById("ponuda_multi_vouchers");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
    
  };
  this_module.select_all_voucher_nums = select_all_voucher_nums;
  
    
  function select_all_popust_nums() {
    /* Get the text field */
    var copyText = document.getElementById("ponuda_multi_popusti");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
    
    
  };
  this_module.select_all_popust_nums = select_all_popust_nums;
  
  
  function set_ponuda_dates() {
    
    
    var ponuda_from_date = $('#ponuda_from_date');
    var ponuda_to_date = $('#ponuda_to_date');

    
    if ( current_input_id == 'ponuda_from_date' || current_input_id == 'ponuda_to_date' ) {
      
      var from_date = ponuda_from_date.val();
      var to_date = ponuda_to_date.val();
      
      
      if ( from_date == '' || to_date == '' ) {
        alert('POTREBNO UPISATI ISPRAVANE DATUME OD I DO !!!');
        return;
      };
      
      
      var start_time = get_DATE_as_milisec(ponuda_from_date).ms_current_time;
      var end_time = get_DATE_as_milisec(ponuda_to_date).ms_current_time;
      
      if ( start_time >= end_time ) {
        alert('DRUGI DATUM MORA BITI VEĆI OD PRVOG !!!');
        return;
      };
      
      var broj_dana = (end_time - start_time) / one_day_in_ms
      
      $('#ponuda_broj_dana').val( zaokruzi(broj_dana, 1) );

    };    
    
    
    if ( current_input_id == 'ponuda_broj_dana' ) {
      
      var broj_dana = $('#ponuda_broj_dana').val();
      
      if ( broj_dana == '' ) {
        $('#ponuda_broj_dana').val(30.5);
        alert('POTREBNO UPISATI BROJ DANA !!!\n\nAplikacija je sada postavila broj dana na 30.5 !!!');
        return;
      };
      
      if ( broj_dana !== '' && $.isNumeric(broj_dana) == false) {
        alert('DANI NISU BROJČANA VRIJEDNOST !!!');
        $('#ponuda_broj_dana').val('');
        return;
      };
      
      var start_time = get_DATE_as_milisec(ponuda_from_date).ms_current_time;
      var end_time = start_time + one_day_in_ms*Number(broj_dana);
      
      $('#ponuda_to_date').val( getMeADateAndTime( end_time, 'd.m.y').datum );
      
    };
    
    
    this_module.cit_data.valid_from = start_time;
    this_module.cit_data.valid_to = end_time;

    
    
  };
  this_module.set_ponuda_dates = set_ponuda_dates;
  
  
  function get_ponuda_count() {
    
    var tag = null;
    // ako postoji tag u data od ovog modula
    // i ako tag nije nema grupu
    if ( this_module.cit_data.tag && this_module.cit_data.tag !== 'nema_grupu' ) tag = this_module.cit_data.tag;
    
    
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: ( 
        '/get_active_pp_and_tag_products/'    +
        this_module.cit_data.pp_sifra         + 
        '/' + (tag || 'none')                 + 
        '/' + this_module.cit_data.valid_from +
        '/' + this_module.cit_data.valid_to
      ),
      dataType: "json"
    })
    .done(function (response) {

      if ( response.success == true ) {
        
        var max_count = null;
        
        console.log(response);
        
        $.each(window.all_pp_all_data, function(pp_ind, park_obj) {
          
          if( park_obj.pp_sifra ==  this_module.cit_data.pp_sifra ) {
            
            if ( this_module.cit_data.tag !== null && this_module.cit_data.tag !== 'nema_grupu' ) {
              
              max_count = park_obj.special_places[this_module.cit_data.tag].count_out;
              
            } else {
              
              max_count = park_obj.pp_count_out_num;
              
            };
            

              var pp_product_data =
`
<div class="data_box_ponuda">
<b>UKUPNO:</b>
<div class="data_box_ponuda_number">${max_count}</div>
</div>
<div class="data_box_ponuda">
<b>ZAUZETO:</b>
<div class="data_box_ponuda_number">${response.count}</div>
</div>
<div class="data_box_ponuda">
<b>SLOBODNO:</b>
<div class="data_box_ponuda_number">${max_count - response.count}</div>
</div>

`;
            
            $('#ponuda_dostupno').html( pp_product_data );
            
          };
          
        });

        var first_one_ending_on_date = '';
        
        if ( response.first_end ) {
          first_one_ending_on_date = getMeADateAndTime(response.first_end, 'd.m.y').datum + ' ' + getMeADateAndTime(response.first_end, 'd.m.y').vrijeme;
          // ako pretplata traje duže od 50 godina onda je VIP !!!
          if ( response.first_end - Date.now() > 1000*60*60*24*365*50 ) {
            first_one_ending_on_date = 'NEOGRANIČENO';
          };
        };
        
        $('#ponuda_first_end').text( first_one_ending_on_date );
        
        if ( response.products && response.products.length > 0) {
          
          create_products_gantt(response.products);
          
        };
        
          
        
      } else {
        
        console.error('greška pri provjeri slobodnih pretplata ------> SUCCESS == FALSE');  
      };

    })
    .fail(function (err) {
      
      console.error('greška pri provjeri slobodnih pretplata ------> REQUEST FAIL');  

    });
      
    
  };
  this_module.get_ponuda_count = get_ponuda_count;
  
  
  function choose_ponuda_pp(park_obj, arg_field_id) {
        
    if ( arg_field_id ) {
      $('#'+arg_field_id).val(park_obj.naziv);
    }
    else {
      $('#'+current_input_id).val(park_obj.naziv);
    };
    
    this_module.cit_data.pp_sifra = park_obj.sifra;
    
    
    // refreshaj search parameter za special place tagove !!!
    $('#ponuda_special_places').data('we_auto', {
      desc: 'za odabir special places u modulu PONUDA TJ PRODUCTS',
      local: true,
      return: {},
      show_on_click: true,
      list: `${park_obj.naziv}_special_places`,
    });

    
    this_module.choose_ponuda_special_place({ sifra: 'nema_grupu', naziv: 'NEMA GRUPU' }, 'ponuda_special_places');
    
    
    // UNUTAR CHOOSE PONUDA SPECIAL PLACE ZOVEM OVO -----> this_module.get_ponuda_count();
    
  };
  this_module.choose_ponuda_pp = choose_ponuda_pp;  
  

  function choose_ponuda_special_place(arg_place_obj, arg_field_id) {


  if ( arg_field_id ) {
    $('#'+arg_field_id).val(arg_place_obj.naziv);
  }
  else {
    $('#'+current_input_id).val(arg_place_obj.naziv);
  };

  this_module.cit_data.tag = arg_place_obj.sifra;
    
  this_module.get_ponuda_count();  
    
  };
  this_module.choose_ponuda_special_place = choose_ponuda_special_place;  

  
  function select_ponuda_user(user) {
    
    $('#'+current_input_id).val(user.name);
    
    
    // if ( !this_module.cit_data.pp_sifra ) {
    //   no_pp_selected();
    //   return;
    // };
    
    // upiši user number od user kojeg sam selektao u drop listi !!!
    this_module.cit_data.user_number = user.user_number;
    this_module.cit_data.email = user.email;
    
    this_module.cit_data.desc = user.email + '_' + getMeADateAndTime(this_module.cit_data.valid_from).datum;
    
    var found_customer_tag = null;
    
    // ako nisam odabrao parking onda stani !!!!!!
    if ( this_module.cit_data.pp_sifra == null ) return;
    
    $.each(window.all_pp_all_data, function(pp_ind, park_obj) {
      
      if ( park_obj.pp_sifra == this_module.cit_data.pp_sifra ) {
        
        // loop po svim special places ( KOJI JE OBJEKT ) unutar jednog parka
        $.each( park_obj.special_places, function( place_tag, spec_place_obj ) {

          // loop da provjerim svaki specijal place sa svakim customer tagom ( KOJI JE ARRAY )
          $.each( user.customer_tags, function( tag_index, tag_obj ) {
            
            if ( found_customer_tag == null && place_tag == tag_obj.sifra ) {
              
              found_customer_tag = place_tag;
              
              this_module.choose_ponuda_special_place({ sifra: place_tag, naziv: place_tag }, 'ponuda_special_places');
              
            };

          });  // end loop po customer tags
          
        }); // end loop po special places
        
        // ako nije našao customer tag koji se poklapa ili ovaj park uopće nema special places
        if ( found_customer_tag == null || !park_obj.special_places) {

          this_module.choose_ponuda_special_place({ sifra: 'nema_grupu', naziv: 'NEMA GRUPU' }, 'ponuda_special_places');

        };

        
      }; // kraj ifa ako je pronasao odabrani parking po sifri
      
    }); // end loop po svm parkovima
    

  };
  this_module.select_ponuda_user = select_ponuda_user;
  
  
  function crop_products_intervals(products, start, end) {
    
    

    $.each(products, function(ind, product ) {


      products[ind].valid_from_c = product.valid_from;
      products[ind].valid_to_c = product.valid_to;

      // postavi na start ako je pocetak prije intervala
      if ( product.valid_from < start  ) {
        products[ind].valid_from_c = start;
      };

      // start unutra end unutra
      if ( product.valid_from >= start && product.valid_to <= end ) {
        
        products[ind].valid_from_c = product.valid_from;
        products[ind].valid_to_c = product.valid_to;
        products[ind].duration_c = product.valid_to - product.valid_from;
        
      };

      // start prije end unutra
      if ( product.valid_from <= start && product.valid_to <= end ) {
        
        products[ind].valid_from_c = start;
        products[ind].valid_to_c = product.valid_to;
        
        products[ind].duration_c = product.valid_to - start;
      };

      // start unutra end poslje
      if ( product.valid_from >= start && product.valid_to >= end ) {
        
        products[ind].valid_from_c = product.valid_from;
        products[ind].valid_to_c = end;
        
        products[ind].duration_c = end - product.valid_from;
        
      };

      // start prije end poslje
      if ( product.valid_from <= start && product.valid_to >= end ) {
        
        products[ind].valid_from_c = start;
        products[ind].valid_to_c = end;
        
        products[ind].duration_c = end - start;
      };

    });

    return products;


    
    
  };
  this_module.crop_products_intervals = crop_products_intervals;
  
  
  
  function create_products_gantt(products) {
  
    $('#big_ponuda_table_box').html('');
    
    
    /*
    $.each(products, function(prod_ind, product) {
      products[prod_ind].valid_to = 1607209200000;
    });
    */
    
    var ponuda_from_date = $('#ponuda_from_date');
    var ponuda_to_date = $('#ponuda_to_date');
    
    var day_start = get_DATE_as_milisec(ponuda_from_date).ms_at_zero;
    var day_end = get_DATE_as_milisec(ponuda_to_date).ms_at_zero; 

    
    var croped_products = this_module.crop_products_intervals(products, day_start, day_end);
    
    var all_bars = '';
    
    var div_height = 0;
    
    var sub_count_array = []; 

    var day_count = (day_end - day_start ) / (1000*60*60*24);
    
    var sub_count_in_day = 0;
    
    var d
    for(d=1; d<=day_count; d++) {
      
      // resetiraj count o uvom danu !!!!!
      sub_count_in_day = 0;

      $.each( croped_products, function(prod_index, product ) {
        var day_min = day_start + (1000*60*60*24 * (d-1) );
        var day_max = day_start + (1000*60*60*24 * d );
        
        if ( product.valid_from <= day_min && product.valid_to >= day_max ) {
          sub_count_in_day += 1;
        };

      });
      
      sub_count_array.push(sub_count_in_day);

    };
      
    $.each( croped_products, function(prod_index, product ) {
      
      var from_start_to_end = (day_end - day_start ); /* / (1000*60*60*24) */
      
      var start_product_time = product.valid_from_c - day_start;
      var start_product_time_perc = (start_product_time / from_start_to_end * 100) + '%';
      
      // product.duration_c = from_start_to_end; // (1000*60*60*24*15);
      
      var koliko_dana_ukupno = from_start_to_end / (1000*60*60*24); 
      // console.log(koliko_dana_ukupno);
      
      var park_width_perc = ( product.duration_c / from_start_to_end * 100 ) + '%';

      var dodatne_classes_od_akcije = '';


      // dodaj strelicu ako interval počinje prije starta
      if ( product.valid_from < day_start ) dodatne_classes_od_akcije += ' start_before';

      // dodaj strelicu ako interval završava poslje enda
      if ( product.valid_to > day_end ) dodatne_classes_od_akcije += ' end_after';

      var pocetak_bar_time = getMeADateAndTime(product.valid_from).datum + ' ' +  getMeADateAndTime(product.valid_from).vrijeme;  
      var kraj_bar_time = getMeADateAndTime(product.valid_to).datum + ' ' +  getMeADateAndTime(product.valid_to).vrijeme; 

      var interval_text = 
`
${product.user_email}
<br>
PARK: ${find_pp_name(product.pp_sifra)}
<br>
OD: ${pocetak_bar_time}
<br>
DO: ${kraj_bar_time}

`;
  
    var samo_ime_maila = product.user_email ? product.user_email.split('@')[0] : 'NEMA MAIL !!!';

    var bar_html =
`
<div  class="cit_tooltip gantt_bar gantt_park${dodatne_classes_od_akcije}" 
      data-toggle="tooltip" data-placement="top" data-html="true"
      title="${interval_text}"
      style="top: ${prod_index*30 + 50}px; left: ${start_product_time_perc}; width: ${park_width_perc};">
  <span>${samo_ime_maila + ' ' + product.user_number }</span>
</div>
`;        
    all_bars += bar_html;
    div_height+= 30;

      
    }); // kraj loopa po svim products
      
    
    function dodaj_stupce(for_header) {
      
      var day_start = get_DATE_as_milisec(ponuda_from_date).ms_at_zero;
      var day_end = get_DATE_as_milisec(ponuda_to_date).ms_at_zero; 
      
      var day_count = (day_end - day_start) / one_day_in_ms;
      day_count = Math.floor(day_count) + 1 ;

      
      var all_cols = '';
      
      // --------START------------- loop koliko ima dana
      var day
      for (day=0; day < day_count; day++) {

        var date_in_ms = (day * one_day_in_ms) + day_start;
        var date =  new Date(date_in_ms);
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!

        
        
        
        
        if (for_header == 'for_header') {
          
          
          var html_col = '';
          
          if ( day >= 2 ) {
            html_col = `<div class="day_col"><div class="hour_num">${ dd + '.' + mm + '.'}</div></div>`;
          };
          
          
          if ( day == 0 ) {
            html_col = 
`<div class="day_col" style="margin-left: 0px;">
  <div class="hour_num" style="margin-left: -15px; right: unset; left: 0;">${dd + '.' + mm }</div>
  <div class="hour_num">${ (dd + 1) + '.' + mm + '.'}</div>
</div>
`;
          };
        };

        if ( for_header == 'sub_count' ) {
          
          var html_col = '';
          
          if ( day >= 2 && day < day_count - 1) {
            html_col = 
`<div class="day_col" style="height: 100%">${sub_count_array[day]}</div>
`;
          };
          
          if ( day == 0 ) {
            html_col = 
`<div class="day_col" style="height: 100%; border-left: 2px solid #ccc;">${sub_count_array[day]}</div>
<div class="day_col" style="height: 100%;">${sub_count_array[day+1]}</div>
`;
          };
                    
          
        };
        
        

        if ( for_header == 'day_columns' ) {
          
          var html_col = '';
          
          if ( day >= 2 && day < day_count - 1) {
            html_col = 
`<div class="day_col" style="height: ${div_height + 60}px;"></div>
`;
          };
          
          if ( day == 0 ) {
            html_col = 
`<div class="day_col" style="height: ${div_height + 60}px; border-left: 2px solid #ccc;"></div>
<div class="day_col" style="height: ${div_height + 60}px;"></div>
`;
          };
                    
          
        };        
        

       // div class="day_col" style="height: ${div_height + 50}px;"></div>
        
        
        all_cols += html_col;
        
      }; // kraj for loop
      // --------END------------- loop koliko ima dana
      
      return all_cols;
      
      
    };
    
    var gantt_main_div = 
`
<div id="products_gantt_main_header" style="width: calc(100% - 27px);">
  ${ dodaj_stupce('for_header') }
</div>

<div  id="products_gantt_main_div" 
      style="height: ${div_height+100}px; width: calc(100% - 27px);">
  <div id="sub_count_traka">${ dodaj_stupce('sub_count') }</div>
  ${ dodaj_stupce('day_columns') }
  ${ all_bars }
</div>
`;
    
  $('#big_ponuda_table_box').html(gantt_main_div);
    
  setTimeout(function(){ $('.cit_tooltip').tooltip();  }, 300);  

  };
  this_module.create_products_gantt = create_products_gantt;
  
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  // NAPOMENA : NAMJERNO SVAKI PUT POSTAVI CIT LOADED = NULL u file-u left menu 
  // tako da uvijek pokrenem empty data funkciju !!!
  if ( !this_module.cit_loaded ) {
    get_empty_cit_data();
  };
    
  this_module.cit_loaded = true;
 
 
}
  
  
};

