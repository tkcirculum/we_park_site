var module_object = {
  create: function( data, parent_element, placement ) {
    
    
    var pp_owners_list_module_url = '/right_sidebar/cit_search_list/owners/cit_pp_owners_list_module.js';
    get_cit_module(pp_owners_list_module_url, 'load_css');

   
    
    var component_id = null;
    var component_html =
    `
<div id="owners_component" class="main_module_component" style="float: left; height: 100%;">
  
        <div class="row" style="padding-bottom: 10px; font-size: 12px; font-weight: 700; margin-top: 20px;">
        <div class="col-md-6 col-sm-12">
Spremio: ${data ? data.user_saved.full_name : ''}&nbsp;
${ data ? getMeADateAndTime(data.user_saved.time_stamp).datum : '' }&nbsp;
${ data ? getMeADateAndTime(data.user_saved.time_stamp).vrijeme : '' }
        </div>

        <div class="col-md-6 col-sm-12">
Editirao: ${data ? data.user_edited.full_name : ''}&nbsp;
${ data ? getMeADateAndTime(data.user_edited.time_stamp).datum : ''}&nbsp;
${ data ? getMeADateAndTime(data.user_edited.time_stamp).vrijeme : '' }
        </div>

      </div>
  
        
  <div class="row">
    <div class="col-md-6 col-sm-12">

      <h6 id="pp_owner_big_label" style="color: #a2a2a2;">Vlasnik:</h6>

    </div>
    
    <div class="col-md-6 col-sm-12">

      <div id="save_pp_owner_btn" 
           class="cit_button cit_center cit_tooltip" 
           style="width: 20px; float: right;"
           data-toggle="tooltip" data-placement="bottom"
           title="SPREMI NOVOG VLASNIKA">
        <i class="fas fa-save"></i>
      </div>

      <div id="edit_pp_owner_btn" 
           class="cit_button cit_center cit_tooltip" 
           style="width: 20px; float: right; background-color: #e84030; display: none;"
           data-toggle="tooltip" data-placement="bottom"
           title="EDITIRAJ VLASNIKA">
        <i class="fas fa-edit"></i>
      </div>          

    </div>


  </div>

  <div class="row">
    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_name_label" class="cit_label">Nadimak</div>
      <input id="pp_owner_name" type="text" class="cit_input">
    </div>
    
    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_name_label" class="cit_label">Ime i Prezime</div>
      <input id="pp_owner_full_name" type="text" class="cit_input">
    </div>
    
    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_address_label" class="cit_label">Adresa Vlasnika</div>
      <input id="pp_owner_address" type="text" class="cit_input">
    </div>
  </div> 

  <div class="row">
    
   
    
    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_tel_label" class="cit_label">Tel Vlasnika</div>
      <input id="pp_owner_tel" type="text" class="cit_input">
    </div>

    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_email_label" class="cit_label">Email Vlasnika</div>
      <input id="pp_owner_email" type="text" class="cit_input">
    </div>
    
  </div>
  
  <div class="row">
    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_business_name_label" class="cit_label">Ime tvrtke</div>
      <input id="pp_owner_business_name" type="text" class="cit_input">
    </div> 
    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_business_address_label" class="cit_label">Adresa tvrtke</div>
      <input id="pp_owner_business_address" type="text" class="cit_input">
    </div>  
    
    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_business_tel_label" class="cit_label">Tel Tvrtke</div>
      <input id="pp_owner_business_tel" type="text" class="cit_input">
    </div>

    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_business_email_label" class="cit_label">Email Tvrtke</div>
      <input id="pp_owner_business_email" type="text" class="cit_input">
    </div>    
    
    
    
      
  </div>    
  
  
  
  <div class="row">
    
    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_oib_label" class="cit_label">OIB</div>
      <input id="pp_owner_oib" type="text" class="cit_input">
    </div>    
    
    <div class="col-md-4 col-sm-4">
      <div id="pp_owner_iban_label" class="cit_label">IBAN</div>
      <input id="pp_owner_iban" type="text" class="cit_input">
    </div>

    
  </div>   


  
</div>
    `;
     
    var this_module = window[module_url]; 
    
    wait_for( `$('#owners_component').length > 0`, function() {
      
      $('.cit_tooltip').tooltip();
      $('#save_pp_owner_btn').off('click');
      $('#save_pp_owner_btn').on('click', this_module.save_pp_owner('save', this_module.cit_data ) );

      $('#edit_pp_owner_btn').off('click');
      $('#edit_pp_owner_btn').on('click', this_module.save_pp_owner('edit', this_module.cit_data ) );

    }, 5000000 );       
   
    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    }

  },
 
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {

    this_module.cit_data = {

      user_saved: null,
      user_edited: null,
      
      saved: null,
      edited: null,
      
      pp_owner_name: null,
      pp_owner_address: null,
      pp_owner_tel: null,
      pp_owner_email: null,

      pp_owner_contact_person: null,
      pp_owner_contact_email: null,

      park_places_owner: null



    };
  };
  
  
  function save_pp_owner( save_or_update, pp_owner_object ) {
    
    
    return function () {
      
      
    if ( !cit_user ) {
      alert('NISTE ULOGIRANI !!!');
      return;
    };

    
    this_module.form_to_data(); 
    
    var request_type = null;
    var request_url = null;
    
    var popup_text = 'PARKIRALIŠTE JE SPREMLJENO !!!';
    
    
    if ( save_or_update == 'save') {
      
      request_type = "POST";
      request_url = "/spremi_pp_owner";
      popup_text = 'VLASNIK JE SPREMLJEN.';
      
      pp_owner_object.user_edited =  { 
        user_id: cit_user.user_id, 
        user_number: cit_user.user_number,
        user_name: cit_user.userName,
        full_name: cit_user.fullName,
        time_stamp: Date.now()
      };
     
      pp_owner_object.user_saved = { 
        user_id: cit_user.user_id,
        user_number: cit_user.user_number,
        user_name: cit_user.userName,
        full_name: cit_user.fullName,
        time_stamp: Date.now()
      };
      
      
      
    };
    
    
    if ( save_or_update == 'edit') {
      
      request_type = "PUT";
      request_url = "/spremi_pp_owner/" + pp_owner_object._id;
      popup_text = 'VLASNIK JE AŽURIRAN!';
      
      pp_owner_object.user_edited =  { 
        user_id: cit_user.user_id, 
        user_number: cit_user.user_number,
        user_name: cit_user.userName,
        full_name: cit_user.fullName,
        time_stamp: Date.now()
      };      
      
    };
    
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: request_type,
      url: request_url,
      data: JSON.stringify(pp_owner_object),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {
      
      console.log(response);
      
      if ( response.success == true ) {
        
        this_module.cit_data._id = response.pp_owner_id;
        
        // show_popup, popup_text, popup_progress, show_buttons
        show_popup_modal(true, popup_text, null, false);
        setTimeout(function() { show_popup_modal(false); }, 1000);
        
        show_save_or_edit_button('edit_pp_owner_btn');
        
      } else {
        popup_text = 'GREŠKA PRI SPREMANJU PODATAKA VLASNIKA!';
        show_popup_modal('error', popup_text, null, 'ok' );
        $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false); });
        
        
      };
    })
    .fail(function(error) {
      console.log(error);
      popup_text = 'GREŠKA PRI SPREMANJU PODATAKA VLASNIKA!';
      // show_popup, popup_text, popup_progress, show_buttons
      show_popup_modal('error', popup_text, null, 'ok' );
      
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false); });
      
    });
    
    console.log(this_module.cit_data);
      
    }; // return function
    
  }; // kraj spremi pp vlasnika
  
  this_module.save_pp_owner = save_pp_owner;

  
  function form_to_data() {
    $.each( this_module.cit_data, function( key, value ) {
      this_module.cit_data[key] = $('#' + key ).val();
    });
  };
  this_module.form_to_data = form_to_data;
  
  function data_to_form(data) {
    
    $.each( data, function( key, value ) {
      
      
      var map_db_to_html = {
        
        name: 'pp_owner_name',
        full_name: 'pp_owner_full_name',
        user_address: 'pp_owner_address',
        business_name: 'pp_owner_business_name',
        business_address: 'pp_owner_business_address',
        business_email: 'pp_owner_business_email',
        business_tel: 'pp_owner_business_tel',
        user_tel: 'pp_owner_tel',
        email: 'pp_owner_email',
        oib: 'pp_owner_oib',
        iban: 'pp_owner_iban',
      }
      
      
      
      $('#' + map_db_to_html[key] ).val(value);
      
      /*
      if ( $.isArray(value) == true ) {
        $.each( value, function( ind, item ) {
          
        });
      };
      */
      
    });
  };
  this_module.data_to_form = data_to_form;
    
  
  
  this_module.cit_loaded = true;


}
  
  
};