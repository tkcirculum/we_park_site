var module_object = {
  create: function( data, parent_element, placement ) {
    

    var this_module = window[module_url]; 

    var component_id = null;


    var c_bar_mask = 

`
<div class="circle_bar_mask" style="width: 100%; position: absolute; top: 0; left: 0; z-index: 10;">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 400 200" style="enable-background:new 0 0 400 200;" xml:space="preserve">
<style type="text/css">.st0{fill:#fff;}</style>
<path id="XMLID_7_" class="st0" d="M0,0v200h400V0H0z M342,183.7c-6.9,0-12.5-5.6-12.5-12.5C329.5,99.9,271.6,42,200.3,42
    c-71.1,0-129,57.9-129,129.2c0,6.9-5.6,12.5-12.5,12.5c-6.9,0-12.5-5.6-12.5-12.5c0-85,69.1-154.2,154-154.2
    c85,0,154.2,69.2,154.2,154.2C354.5,178.1,348.9,183.7,342,183.7z"/>
</svg>
</div>

`;
      
    var component_html =
    `
<div id="parking_places_component" class="main_module_component container-fluid"  style="float: left; height: auto; padding-bottom: 200px;">
  
  <div class="row">
    
    <!-- OVO JE MAPA  -->
    <div class="col-md-12 col-sm-12">
      <div class="row">
        <div class="col-md-12 col-sm-12" 
             style=" padding: 0;
                      margin-bottom: 30px;
                      border-radius: 10px;
                      overflow: hidden;">
          
          <div id="google_map_select_parking"></div>
          
        </div>
      </div>  
    </div>
    
  </div> <!-- END OF TOP ROW CLASS -->  
  
  <div class="row" id="red_za_btn_show_hide_pp_list" style="margin-bottom: 10px;">
    <div class="col-md-12 col-sm-12">
      
      <div id="hide_pp_list_btn" 
           class="cit_button cit_center cit_tooltip" 
           style="width: 280px; float: none; margin: 0 auto; height: 50px; border-radius: 25px; font-size: 13px; display: none;"
           data-toggle="tooltip" data-placement="bottom"
           title="SAKRIJ LISTU PARKIRALIŠTA">
        <i class="fas fa-angle-up" style="margin-right: 10px;"></i> SAKRIJ LISTU PARKIRALIŠTA
      </div>

      <div id="show_pp_list_btn" 
           class="cit_button cit_center cit_tooltip" 
           style="width: 280px; float: none; margin: 0 auto; height: 50px; border-radius: 25px; font-size: 13px;"
           data-toggle="tooltip" data-placement="bottom"
           title="PRIKAŽI LISTU PARKIRALIŠTA">
        <i class="fas fa-angle-down" style="margin-right: 10px;"></i> PRIKAŽI LISTU PARKIRALIŠTA
      </div> 

    </div>
  </div>
  
  
  <div id="park_info_lista" class="cit_hide">
    <!-- OVDJE STAVLJAM LISTU ZA REZERVACIJU -->
   
  </div>
  

  <div class="row" id="veliki_CTA_rent_pp">
    <div class="col-md-12 col-sm-12">
      <h1 id="pp_big_label" 
          style=" color: #5e5f60;
                  font-weight: 700;
                  text-align: center;
                 letter-spacing: 0.03rem;">Želite iznajmiti Vaše parkiralište kroz <span style="font-weight: 300;">We</span>Park sustav?</h1>

      <h5 id="pp_big_label" 
          style="color: #1a2840;
                 text-align: center;
                 float: none;
                 width: 100%;
                 display: block;
                 margin-bottom: 30px;">
Popunite formu ispod i mi ćemo vas nazvati u roku od 24 sata!
${ cit_user ? '' : `<br><br>Za omogućavanje upisa potrebno je se registrirati!<br>` }
</h5>

    </div>
  </div>  
  <div class="row">
    <!-- OVO JE FORMA za park place -->
    <div id="parking_places_form" class="col-md-12 col-sm-12 white_card">
      
      
      <div class="row" style="padding-bottom: 10px; font-size: 12px; font-weight: 700;">
        <div id="cit_pp_user_saved" class="col-md-6 col-sm-12">
Spremio: ${data ? data.user_saved.full_name : ''}&nbsp;
${ data ? getMeADateAndTime(data.user_saved.time_stamp).datum : '' }&nbsp;
${ data ? getMeADateAndTime(data.user_saved.time_stamp).vrijeme : '' }
        </div>

        <div id="cit_pp_user_edited" class="col-md-6 col-sm-12">
Editirao: ${data ? data.user_edited.full_name : ''}&nbsp;
${ data ? getMeADateAndTime(data.user_edited.time_stamp).datum : ''}&nbsp;
${ data ? getMeADateAndTime(data.user_edited.time_stamp).vrijeme : '' }
        </div>

      </div>  
      
      
      
        <div class="row">
<!--

          <div class="col-md-6 col-sm-12">
            <h6 id="pp_big_label" style="color: #a2a2a2;">Podaci parkirališta:</h6>
          </div>
-->

          <div class="col-md-12 col-sm-12">

            <div id="save_park_place_btn" 
                 class="cit_button cit_center cit_tooltip" 
                 style="width: 220px; float: none; margin: 20px auto; ${ cit_user ? '' : `display: none;` }"
                 data-toggle="tooltip" data-placement="bottom"
                 title="POPUNI PODATKE NOVOG PARKIRALIŠTA I SPREMI">
              <i class="fas fa-save" style="margin-right: 10px;"></i> SPREMI PARKIRALIŠTE
            </div>

            <div id="edit_park_place_btn" 
                 class="cit_button cit_center cit_tooltip" 
                 style="width: 220px; float: none; margin: 20px auto; background-color: #e84030; display: none;"
                 data-toggle="tooltip" data-placement="bottom"
                 title="EDITIRAJ PODATKE OVOG PARKIRALIŠTA I SPREMI PROMJENE">
              <i class="fas fa-edit" style="margin-right: 10px;"></i> SPREMI PROMJENE
            </div>          

          </div>


        </div>

        <div class="row">


          <div class="col-md-3 col-sm-12">
            <div id="pp_tel_label" class="cit_label">Telefon kontakt parkirališta</div>
            <input id="pp_tel" type="text" class="cit_input">
          </div>

          <div class="col-md-3 col-sm-12">
            <div id="pp_email_label" class="cit_label">Email kontakt parkirališta</div>
            <input id="pp_email" type="text" class="cit_input">
          </div>


          <div class="col-md-3 col-sm-12">
            <div id="pp_name_label" class="cit_label">Ime Parkirališta</div>
            <input id="pp_name" type="text" class="cit_input">
          </div>  

          <div class="col-md-3 col-sm-12">  
            <div id="pp_address_label" class="cit_label">Adresa Parkirališta</div>
            <input id="pp_address" type="text" class="cit_input">
          </div>

        </div>

        <div class="row">  

          <div class="col-md-3 col-sm-3">

            <div id="pp_count_out_num_label" class="cit_label">Broj Mjesta VANI</div>
            <input id="pp_count_out_num" type="number" class="cit_input">

          </div>

          <div class="col-md-3 col-sm-3">

            <div id="pp_count_in_num_label" class="cit_label">Broj Mjesta UNUTRA</div>
            <input id="pp_count_in_num" type="number" class="cit_input">

          </div>
          
          <div class="col-md-3 col-sm-3">
            <div id="pp_latitude_label" class="cit_label">Zemljopisna širina</div>
            <input  id="pp_latitude" 
                    data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-html="true"
                    title="Pronađite svoje parkiralište na karti i kliknite na to mjesto.<br>Polja zemljopisna širina i dužina će se automatski popuniti."
                    type="number" class="cit_input cit_tooltip">
          </div>
          
          
          <div class="col-md-3 col-sm-3">
            <div id="pp_longitude_label" class="cit_label">Zemljopisna dužina</div>
            <input  id="pp_longitude" 
                    data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-html="true"
                    title="Pronađite svoje parkiralište na karti i kliknite na to mjesto.<br>Polja zemljopisna širina i dužina će se automatski popuniti."
                    type="number" class="cit_input cit_tooltip">
          </div>
          

        </div>  


        <div class="row" style="display: ${ ( window.cit_user && is_super() == true ) ? 'flex' : 'none' };">
          
          <div class="col-md-3 col-sm-3">
            <div id="pp_available_out_num_label" class="cit_label">Slobodno VANI</div>
            <input id="pp_available_out_num" type="number" class="cit_input">
          </div>

          <div class="col-md-3 col-sm-3">
            <div id="pp_available_in_num_label" class="cit_label">Slobodno UNUTRA</div>
            <input id="pp_available_in_num" type="number" class="cit_input">
          </div>
          

          <div class="col-md-3 col-sm-3">
            <div id="pp_max_height_label" class="cit_label">Max visina u cm</div>
            <input id="pp_max_height" type="number" class="cit_input">
          </div>

          <div class="col-md-3 col-sm-3">
            <div id="contract_exp_date_label" class="cit_label">Ugovor s vlasnikom do:</div>
            <input id="contract_exp_date" type="text" class="cit_input">
          </div>

        </div>
      
      <div class="row" style="display: ${ ( window.cit_user && is_super() == true ) ? 'flex' : 'none' };">
              <div class="col-md-4 col-sm-12">
        <div class="cit_switch_wrapper">
          <div id="pp_fixed_open_label" class="cit_label">TRAJNO OTVORENO</div>
          <div id="pp_fixed_open" class="cit_switch off">
                <div class="switch_circle"></div>
                <div class="on_text switch_text">DA</div>
                <div class="off_text switch_text">NE</div>
          </div>
        </div>
      </div>
      
      <div class="col-md-4 col-sm-12">
        <div class="cit_switch_wrapper">
          <div id="pp_fixed_close_label" class="cit_label">ZAKLJUČANO</div>
          <div id="pp_fixed_close" class="cit_switch off">
                <div class="switch_circle"></div>
                <div class="on_text switch_text" style="background-color: #e84030;">DA</div>
                <div class="off_text switch_text">NE</div>
          </div>
        </div>
      </div> 
        
      </div>


        <div class="row" style="margin-top: 30px; display: ${ ( window.cit_user && is_super() == true ) ? 'flex' : 'none' };">
          <div class="col-md-6 col-sm-12">
            <h6 id="pp_owners_big_label" style="color: #a2a2a2;">Vlasnici:</h6>
          </div>
        </div>
      
        <div class="row" style="display: ${ ( window.cit_user && is_super() == true ) ? 'flex' : 'none' };">
          <div class="col-md-3 col-sm-12">
            <div id="pp_choose_owner_label" class="cit_label">Dodaj vlasnika:</div>
            <input autocomplete="off" id="pp_choose_owner" type="text" class="cit_input we_auto simple">
          </div>
        </div>
      
      
        <div class="row" style="display: ${ ( window.cit_user && is_super() == true ) ? 'flex' : 'none' };">
          <div class="col-md-12 col-sm-12">
            
            <div id="users_pp_owners_box" style="width: 100%; float: left; clear: both; height: auto; margin-top: 5px;">

                <div id="users_pp_owners_header">
                  <div class="pp_owner_cell" style="width: 25%">NADIMAK</div>
                  <div class="pp_owner_cell" style="width: 25%">IME I PREZIME</div>
                  <div class="pp_owner_cell" style="width: 25%">IME TVRTKE</div>
                  <div class="pp_owner_cell" style="width: 25%">&nbsp;</div>
                </div>
              
            </div>
            
          </div>
        </div>  

  
    </div>  <!--END OD WHITE CARD FORM ON RIGHT SIDE-->
    
  </div> <!-- END OF TOP ROW CLASS -->
    
  
  <div class="row" style="margin-top: 20px; display: ${ ( window.cit_user && is_super() == true ) ? 'flex' : 'none' };">
    <div class="col-md-12 col-sm-12">
      <h6 id="pp_price_interval_label" style="color: #a2a2a2;">Grupe na parkiralištu</h6>
    </div>
  </div>
  
  <div id="pp_grupe_card" class="white_row_card" 
       style="display: ${ ( window.cit_user && is_super() == true ) ? 'block' : 'none' };" >
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div id="pp_price_interval_write_label" class="cit_label">Kreiraj novu pod-grupu na parkiralištu:</div>
      </div>
    </div>
    
    <div class="row">  

        <div class="col-md-4 col-sm-4" id="pp_grupe_tag_box">

        </div>

    </div>

  </div>
  
  
  <div class="row" 
       style="margin-top: 20px; float: left; clear: both; width: 100%; display: ${ ( window.cit_user && is_super() == true ) ? 'flex' : 'none' };">
    <div class="col-md-12 col-sm-12">
      <h6 id="pp_price_interval_label" style="color: #a2a2a2;">Cijenovni intervali</h6>
    </div>
  </div>
      

  
  <div id="card_for_create_new_pp_interval" 
       class="white_row_card"
       style="display: ${ ( window.cit_user && is_super() == true ) ? 'block' : 'none' };">    
    
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div id="pp_price_interval_write_label" class="cit_label">Upiši novi interval:</div>
      </div>
    </div>      
    
    
    <div class="row">  

        <div class="col-md-4 col-sm-4" id="pp_price_days_interval_column">
          
          <div id="choose_day_label" class="cit_label">Izaberi dan:</div>
          <input id="choose_day" type="text" class="cit_input we_auto simple" autocomplete="off"> 


        </div>

        <div class="col-md-2 col-sm-2">
          <div id="pp_price_interval_time_start_label" class="cit_label">Od vremena / hh:mm:ss</div>
          <input id="pp_price_interval_time_start" type="text" class="cit_input" autocomplete="off">
        </div>

        <div class="col-md-2 col-sm-2">
          <div id="pp_price_interval_time_end_label" class="cit_label">Do vremena / hh:mm:ss</div>
          <input id="pp_price_interval_time_end" type="text" class="cit_input" autocomplete="off">
        </div> 
      
        <div class="col-md-4 col-sm-4">
          
          <div style="width: 50%; box-sizing: border-box; padding-right: 5px; float: left;">

            <div class="cit_switch_wrapper">
              <div id="dyn_pp_price_label" class="cit_label">Dinamična cijena</div>
              <div id="dyn_pp_price" class="cit_switch off">
                    <div class="switch_circle"></div>
                    <div class="on_text switch_text">DA</div>
                    <div class="off_text switch_text">NE</div>
              </div>
            </div>

          </div>
          
          <div style="width: 50%; box-sizing: border-box; padding-left: 5px; float: left;">
            <div id="max_pp_price_factor_label" class="cit_label">Max din. koeficijent</div>
            <input id="max_pp_price_factor" type="text" class="cit_input" autocomplete="off">
          </div>
          
          
        </div>       

      </div>

    <div class="row">

        <div class="col-md-4 col-sm-4">
          
          <div style="width: 50%; box-sizing: border-box; padding-right: 5px; float: left;">
            <div id="pp_basic_price_in_label" class="cit_label">Cijena IN / h</div>
            <input id="pp_basic_price_in" type="text" class="cit_input">
          </div>
          
          <div style="width: 50%; box-sizing: border-box; padding-left: 5px; float: left;">
            <div id="pp_basic_price_out_label" class="cit_label">Cijena OUT / h</div>
            <input id="pp_basic_price_out" type="text" class="cit_input">
          </div>
          
        </div>

        <div class="col-md-2 col-sm-2">
          <div id="pp_count_label" class="cit_label">Naš udio / postotak </div>
          <input id="pp_revenue" type="text" class="cit_input">
        </div>

        <div class="col-md-2 col-sm-2">
          <div id="pp_owner_revenue_label" class="cit_label">Partnerski udio / postotak</div>
          <input id="pp_owner_revenue" type="text" class="cit_input">
        </div>


        <div class="col-md-4 col-sm-4" style="display: flex; justify-content: flex-end; align-items: flex-end;">  

          <div style="width: 50%; box-sizing: border-box; padding-right: 5px; float: left;">
            <div id="dyn_zauzetost_granica_label" class="cit_label">Nakon zauzetosti od %</div>
            <input id="dyn_zauzetost_granica" type="text" class="cit_input">
          </div>          
          
          <div style="width: 50%; box-sizing: border-box; padding-left: 5px; float: left;">
            <div id="add_pp_price_interval_btn" 
                 class="cit_button cit_center cit_tooltip" 
                 style="width: 100%;"
                 data-toggle="tooltip" data-placement="bottom"
                 title="DODAJ NOVI INTERVAL">
              <i class="fas fa-plus" style="margin-right: 5px; font-size: 12px;"></i> DODAJ
            </div> 
          </div>
          

          

        </div>       
      
    </div>  
      
  </div>
  
  <div class="row" 
       style="clear: both; float: left; width: calc(100% + 28px); display: ${ ( window.cit_user && is_super() == true ) ? 'flex' : 'none' };">

    <div id="pp_price_box" class="col-md-12 col-sm-12" style="padding: 0;">
      <div id="pp_price_intervals_header">

        <div class="pp_price_cell" style="width: 10%">DAN</div>
        <div class="pp_price_cell" style="width: 10%">OD VREMENA</div>
        <div class="pp_price_cell" style="width: 10%">DO VREMENA</div>

        <div class="pp_price_cell" style="width: 15%">OSNOVNA CIJENA</div>
        <div class="pp_price_cell" style="width: 15%">MAX DIN CIJENA</div>

        <div class="pp_price_cell" style="width: 15%">UDIO WEPARK</div>
        <div class="pp_price_cell" style="width: 15%">UDIO PARTNER</div>

      </div>
      
      
      
    </div>
    
    <!-- OVDJE UBACUJEM GENERIRANE CIJENE I INTERVALE -->
  </div>

  
  
  <!-- PRIKAŽI SAMO AKO JE MARUŠIĆ -->  
  <div class="row" 
       id="pp_solar_panel_title_row"
       style="margin-top: 20px; 
              display: ${ ( window.cit_user && is_super() == true && this_module.cit_data && this_module.cit_data.pp_sifra == '15' ) ? 'block' : 'none' };
              float: left;
              width: 100%">
    
    <div class="col-md-12 col-sm-12">
      <h6 id="pp_solar_panel_label" style="color: #a2a2a2;">Solar panel</h6>
    </div>
  </div>
  
  
  <div id="pp_solar_panel_card" class="white_row_card" 
       style="  display: ${ ( window.cit_user && is_super() == true && this_module.cit_data && this_module.cit_data.pp_sifra == '15' ) ? 'block' : 'none' }; margin-top: 0;">
    
    <div class="row">
      <div id="solar_napon_box" class="col-lg-3 col-md-6 col-sm-12">
        <div class="circle_bar_box">
          <div class="circle_bar_number">...PENDING...</div>
          <div class="circle_bar_perc_value">?</div>
          ${c_bar_mask}
          <div class="circle_bar_percent" style="transform: translateX(0) translateY(-8%) rotate(-80deg);">
            <img class="circle_bar_grad" src="img/circle_bar_grad.png" style="transform: rotate(80deg);" />
          </div>
        </div>
        <div class="circle_bar_label">SOLAR NAPON</div>
      </div>
      
      <div id="solar_struja_box" class="col-lg-3 col-md-6 col-sm-12" style="opacity: 0.5;">
        <div class="circle_bar_box">
          <div class="circle_bar_number">NEAKTIVNO</div>
          <div class="circle_bar_perc_value">N/A</div>
          
          ${c_bar_mask}
          <div class="circle_bar_percent" style="transform: translateX(0) translateY(-8%) rotate(-180deg);">
            <img class="circle_bar_grad" src="img/circle_bar_grad.png" style="transform: rotate(180deg);" />
          </div>
        </div>
        <div class="circle_bar_label">SOLAR STRUJA</div>
      </div>
      
      <div id="aku_napon_box" class="col-lg-3 col-md-6 col-sm-12" style="opacity: 0.5;">
        <div class="circle_bar_box">
          <div class="circle_bar_number">NEAKTIVNO</div>
          <div class="circle_bar_perc_value">N/A</div>
          ${c_bar_mask}
          <div class="circle_bar_percent" style="transform: translateX(0) translateY(-8%) rotate(-180deg);">
            <img class="circle_bar_grad" src="img/circle_bar_grad.png" style="transform: rotate(180deg);" />
          </div>
        </div>  
        <div class="circle_bar_label">AKOMULATOR NAPON</div>
      </div>
      
      <div id="aku_struja_box" class="col-lg-3 col-md-6 col-sm-12" style="opacity: 0.5;">
        <div class="circle_bar_box">
          <div class="circle_bar_number">NEAKTIVNO</div>
          <div class="circle_bar_perc_value">N/A</div>
          ${c_bar_mask}
          <div class="circle_bar_percent" style="transform: translateX(0) translateY(-8%) rotate(-180deg);">
            <img class="circle_bar_grad" src="img/circle_bar_grad.png" style="transform: rotate(180deg);" />
          </div>
        </div>
        <div class="circle_bar_label">AKOMULATOR STRUJA</div>
        
      </div>
      
      
      
    </div>
        
    <div class="row" style="margin-top: 0px; display: flex; align-items: center; justify-content: center; margin-bottom: 0;">

      <div class="col-md-2 col-sm-12">
        <div id="solar_from_date_label" class="cit_label">Datum OD:</div>
        <input id="solar_from_date" type="text" class="cit_input" autocomplete="off">          
      </div>  

      <div class="col-md-2 col-sm-12">
        <div id="solar_to_date_label" class="cit_label">Datum DO:</div>
        <input id="solar_to_date" type="text" class="cit_input" autocomplete="off">          
      </div>
      
      <div class="col-md-2 col-sm-12">
        <div id="solar_time_grupa_label" class="cit_label">GRUPIRAJ PO:</div>
        <input id="solar_time_grupa" type="text" class="cit_input we_auto same_width" autocomplete="off"> 
      </div>

      <div class="col-md-3 col-sm-12">
        <div id="we_graf_visible_label" class="cit_label">VIDLJIVOST:</div>
        <input id="we_graf_visible" type="text" class="cit_input we_auto same_width" autocomplete="off"> 
      </div>
      
      

    </div>    
      
    <div class="row" style="">
      <div class="col-md-12 col-sm-12">
        
        <div id="refresh_solar_data" 
             class="cit_button cit_center cit_tooltip" 
             style="width: 220px; float: none; margin: 20px auto; ${ cit_user ? '' : `display: none;` }"
             data-toggle="tooltip" data-placement="top"
             title="OSVJEŽI PODATKE OD SOLAR PLOČE">
          <i class="fas fa-redo" style="margin-right: 10px;"></i> OSVJEŽI
        </div>
        
      </div>
    </div>

    <div id="graf_solar_row" class="row we_graf_row" style="float: left; width: 100%; height: 300px;">

      <!-- OVDJE STAVLJAM GRAF OD SOLARA -->
    
    </div>
    
  </div>      
  

  
</div> 
    `;
      
      
  var this_module = window[module_url]; 
    
  // ovo je arbitrarni uvijet  - uzeo sam prvi label u formi kao uvijet kad se pojavi ....
  wait_for( `$('#pp_name_label').length > 0`, function() {
    
    $('.circle_bar_number').css('font-size', parseInt($(window).width() * 0.011538461538461539) + 'px');
    $('.circle_bar_perc_value').css('font-size', parseInt($(window).width() * 0.015384615384615385) + 'px');

    this_module.we_graf_setup();
    
    // window.create_we_graf(window.graf_data);
    
    $('.cit_tooltip').tooltip();
    
    $('#hide_pp_list_btn').off('click');
    $('#hide_pp_list_btn').on('click', this_module.hide_pp_list );
  
      
    $('#show_pp_list_btn').off('click');
    $('#show_pp_list_btn').on('click', this_module.show_pp_list );
    
    
    $('#save_park_place_btn').off('click');
    $('#save_park_place_btn').on('click', this_module.save_park_place('save') );

    $('#edit_park_place_btn').off('click');
    $('#edit_park_place_btn').on('click', this_module.save_park_place('edit') );
    
    $('#add_pp_price_interval_btn').off('click');
    $('#add_pp_price_interval_btn').on('click', this_module.add_pp_price );
    
    $('#refresh_solar_data').off('click');
    $('#refresh_solar_data').on('click', function() {  get_solar_data('rampa_tzm'); });
    
    
    
    $('#pp_choose_owner').data('we_auto', {
      desc: 'pretraga usera za odabir ownera',
      local: false,
      url: '/search_customers',
      find_in: ['email', 'name', 'user_address', 'user_tel' ],
      col_widths: [0, 20, 20, 20, 10, 15, 15],
      return: { _id: 1, user_number: 1, email: 1, name: 1, user_tel: 1, full_name: 1, business_name: 1 },
      hide_cols: ['_id' ],
      query: {},
      show_on_click: false,
      list_width: 1000,
    });
    
    
    $('#choose_day').data('we_auto', {
      desc: 'za odabir dana kod definiranja cijene parkinga',
      local: true,
      return: {},
      show_on_click: true,
      list: 'cit_select_days',
    });
    
    
    
    $('#solar_time_grupa').data('we_auto', {
      desc: 'ZA SOLAR MODULE ---> grupiranja minuta, sat, dan, tjedan ili mjesec',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'solar_time_grupe',
    });
    
    
    $('#we_graf_visible').data('we_auto', {
      desc: 'ZA BIRANJE koji će graf biti vidljiv',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'we_graf_visible',
    });    
    
    // kreiraj park listu sa gumbima za reservaciju
    this_module.generiraj_park_list_za_rezervaciju()
    
  }, 5000000 );      

    if ( parent_element ) {
      
        cit_place_component(parent_element, component_html, placement);
        
        window.cit_main_area_perfectbar.update();
      
        var pp_list_module = window['/right_sidebar/cit_search_list/park_places/cit_pp_list_module.js'];
        pp_list_module.create_pp_list();
            
        wait_for( `window.pp_data_for_list !== null && window.pp_edit_mode == false`, function() {
          
          this_module.initMap();
          
        }, 500000);
      
        
        
      // }, 6000000);  
      

    }; 
    
    return {
      html: component_html,
      id: component_id
    };

  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  
    
  function set_pp_fixed_open(new_state) {
    
    if ( this_module.cit_data.fixed_close == true ) {
      var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;
      var popup_text = 'Nije moguće uključiti trajno otvaranje ako je rampa zaključana!'
      // show_popup, popup_text, popup_progress, show_buttons
      show_popup_modal('warning', popup_text, null, 'ok');
      $('#cit_popup_modal_OK').off('click');
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok'); });
      
      // vrati switch u prethodno stanje
      setTimeout(function() { set_switch_html(this_module, '#pp_fixed_open', 'fixed_open' ) }, 600);
      return;
    };
    
    this_module.cit_data.fixed_open = new_state;
  };
  this_module.set_pp_fixed_open = set_pp_fixed_open;
  
  
  function set_pp_fixed_close(new_state) {
    
    if ( this_module.cit_data.fixed_open == true ) {
      var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;
      var popup_text = 'Nije moguće zaključati rampu ako je rampa trajno otvorena!';
      // show_popup, popup_text, popup_progress, show_buttons
      show_popup_modal('warning', popup_text, null, 'ok');
      $('#cit_popup_modal_OK').off('click');
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok'); });
      
      // vrati switch u prethodno stanje
      setTimeout(function() { set_switch_html(this_module, '#pp_fixed_close', 'fixed_close' ) }, 600);
      return;
      
    };    
    
    this_module.cit_data.fixed_close = new_state;
  };
  this_module.set_pp_fixed_close = set_pp_fixed_close;
  
  
  
  window.solar_time_grupa = {"sifra": "MIN", "naziv": "PO MINUTAMA"};
  window.we_graf_visible = {"sifra": "SVI", "naziv": "SVI GRAFOVI"};
  
  
  function choose_solar_time_grupu(time_grupa) {
    var curr_input = $('#'+current_input_id);
    curr_input.val( time_grupa.naziv );
    window.solar_time_grupa = time_grupa;
  };
  this_module.choose_solar_time_grupu = choose_solar_time_grupu;
  
  
  
  function choose_we_graf_visible(graf_visible_obj) {
    var curr_input = $('#'+current_input_id);
    curr_input.val( graf_visible_obj.naziv );
    window.we_graf_visible = graf_visible_obj;
    
    
    if ( window.we_graf_visible ) { 

      
      if ( window.we_graf_visible.sifra == 'SVI') {
        // prikaži sve spremnike linija
        $('.we_graf_line_box').css('display', 'block');
        // prikaži sve točke
        $('.we_graf_point').css('display', 'flex');

      }
      else {
        
        // sakrij sve spremnike linija
        $('.we_graf_line_box').css('display', 'none');
        // sakrij sve točke
        $('.we_graf_point').css('display', 'none');

        var line_id = '';
        if ( window.we_graf_visible.sifra == 'SOLAR_N') line_id = 'line_solar_napon';
        if ( window.we_graf_visible.sifra == 'SOLAR_S') line_id = 'line_solar_struja';
        if ( window.we_graf_visible.sifra == 'AKU_N') line_id = 'line_aku_napon';
        if ( window.we_graf_visible.sifra == 'AKU_S') line_id = 'line_aku_struja';
   
          $(`#${line_id}`).css('display', 'block');
          $(`.we_graf_point.${line_id}`).css('display', 'flex');

      };
      
    };    
    
    
  };
  this_module.choose_we_graf_visible = choose_we_graf_visible;
    
  

  var solar_napon_points = [];
  var solar_struja_points = [];
  var aku_napon_points = [];
  var aku_struja_points = [];
  
  for(p = 1; p <= 1440; p++) {
    
    var p1 = cit_rand_min_max_float(22,23);
    var p2 = cit_rand_min_max_float(4,5);
    var p3 = cit_rand_min_max_float(19,20);
    var p4 = cit_rand_min_max_float(2,3);
    
    p1 = zaokruzi(p1, 2);
    p2 = zaokruzi(p2, 2);
    p3 = zaokruzi(p3, 2);
    p4 = zaokruzi(p4, 2);
    
    solar_napon_points.push(p1);
    solar_struja_points.push(p2);
    aku_napon_points.push(p3);
    aku_struja_points.push(p4);
    
  };
  
  
  window.graf_data = {
    time_grupa: (window.solar_time_grupa.sifra || 'MIN' ),
    id: 'graf_solar',
    lines: [
      {id: 'line_solar_napon', color: '#d10000', points: solar_napon_points },
      {id: 'line_solar_struja', color: '#f400d7', points: solar_struja_points },
      {id: 'line_aku_napon', color: '#00770e', points: aku_napon_points },
      {id: 'line_aku_struja', color: '#248efe', points: aku_struja_points }
    ]
  };
  
  
  function we_graf_setup() {

    var solar_from_date = $('#solar_from_date');
    var solar_to_date = $('#solar_to_date');
    
    var graf_start = null;
    var graf_end = null;
        
    // default je da datumi budu danas i sutra
    var date_now = create_ms_at_zero_and_week_day(0);
    var date_sutra = create_ms_at_zero_and_week_day(1);
    
    // ako je prvi input generiran -- onda su i svi ostali :) 
    if ( solar_from_date.length > 0 ) {
      
      
      if ( solar_from_date.val().trim() == '' ) {
        // uzmi da je početni datum now
        graf_start = date_now.ms_at_zero;
        // pretvori now u string datum i upiši ga u input polje
        $('#solar_from_date').val( getMeADateAndTime(graf_start, 'd.m.y').datum );
      } else {
        // ako je nešto upisano u prvo polje datuma onda pretvori taj datum u number
        graf_start = get_DATE_as_milisec(solar_from_date).ms_at_zero;
      };
      
      
      // ako NE postoji nešto u inputu
      if ( solar_to_date.val().trim() == '' ) {
        // uzmi da je početni datum sutra
        graf_end = date_sutra.ms_at_zero;
        // pretvori datum sutra u string datum i upiši ga u input polje
        $('#solar_to_date').val( getMeADateAndTime(graf_end, 'd.m.y').datum );
      } else {
        // ako je nešto upisano u drugo polje datuma onda pretvori taj datum u number
        graf_end = get_DATE_as_milisec(solar_to_date).ms_at_zero;
      };
      
    
      // default vremena
      window.solar_from_date = graf_start;
      window.solar_to_date = graf_end;

      // default time grupa za graf
      if ( $('#solar_time_grupa').val() == '' ) {
        window.solar_time_grupa = {"sifra": "MIN", "naziv": "PO MINUTAMA"};
        $('#solar_time_grupa').val('PO MINUTAMA');
      };
      
      // default visibility za grafove
      if ( $('#we_graf_visible').val() == '' ) {
        $('#we_graf_visible').val('SVI GRAFOVI');
        window.we_graf_visible = {"sifra": "SVI", "naziv": "SVI GRAFOVI"};
      };

    }; // kraj ako postoji prvo polje za datum
    
  };
  this_module.we_graf_setup = we_graf_setup;



  function get_week_count(ms_date) {
    var datum = new Date( ms_date );
    var godina = datum.getFullYear();  
    var week_count = 1;

    // vrti 370 dana unaprijed od početka godine
    // pošto svaka godina ima 365 ili prestupna 366 to je više nego dovoljno
    var dan = 1;
    for( dan=1; dan<= 370; dan++ ) {

      var new_date = new Date(godina, 0, dan );

      if ( Number(new_date) < ms_date  ) {          
        var week_day_num = new_date.getDay();
        if ( week_day_num == 0 ) {
          week_count += 1; 
        };
      };

    }; // kraj for loop-a

    return week_count
  };

        
  
  
  window.create_we_graf = function(graf_data) {

    var max_number_vertical = 0;
    var max_number_horizontal = 0;    
    var start_unit = null;
    
    
    var hor_num_w = 60;

    if ( graf_data.time_grupa == 'MIN' ) hor_num_w = 120;
    if ( graf_data.time_grupa == 'SAT' ) hor_num_w = 60;
    if ( graf_data.time_grupa == 'DAN' ) hor_num_w = 60;
    var hor_num_style = `width: ${hor_num_w}px;`;    

    
    var point_opacity = 'opacity: 1;';
    if ( graf_data.time_grupa == 'MIN' ) point_opacity = '';
    
    function create_numbers() {
      /*
      window.solar_from_date
      window.solar_to_date
      window.solar_time_grupa
      window.we_graf_visible
      */

      // loop po sve 4 linije grafa
      $.each(graf_data.lines, function(line_ind, line) {
        // length od points arraja predstavlja horizontalni max broj
        if ( line.points.length > max_number_horizontal ) max_number_horizontal = line.points.length;
        // gledam pojedine veličine svakog pointa da vidim kolika je max vertical veličina
        $.each(line.points, function(point_ind, point) {
          if ( point > max_number_vertical ) max_number_vertical = Math.floor(point);
        });
      });
      
      if ( graf_data.time_grupa == 'MIN' ) max_number_horizontal = 24;
      
      var all_ver_numbers = '';
      var all_body_lines = '';
      
      for(i = 0; i<= max_number_vertical; i++ ) { // prije sam dodavao max_number_vertical + 2
        var ver_num_html = `<div class="graf_ver_num">${i}</div>`;
        var body_line_html = `<div class="graf_body_line"></div>`;
        all_ver_numbers += ver_num_html;
        all_body_lines += body_line_html;
      };
      

      
      var all_hor_numbers = '';
      for(i = 1; i<= max_number_horizontal; i++ ) { // prije sam dodavao max_number_horizontal + 1
        var hor_num_html = `<div class="graf_hor_num" style="${hor_num_style}">${i}</div>`;
        all_hor_numbers += hor_num_html;  
      };

      return {
        hor_count: max_number_horizontal,
        ver_count: max_number_vertical,
        vertical: all_ver_numbers,
        horizontal: all_hor_numbers,
        body_lines: all_body_lines
      }

    };
    
    var graf_box_w = (hor_num_w * create_numbers().hor_count) + 50;
    var we_graf_box_style = `width: ${graf_box_w}px;`;
    
    var graf_html = 
`
<div id="${graf_data.id}" class="we_graf_box" style="${we_graf_box_style}">
  <div class="we_graf_top">
    <div class="we_graf_ver_numbers">
      ${create_numbers().vertical}
    </div>
    <div class="we_graf_body">
      <div class="we_graf_points_box"></div>
      ${create_numbers().body_lines}
    </div>

  </div>
  <div class="we_graf_hor_empty"></div>
  <div class="we_graf_hor_numbers">
    ${create_numbers().horizontal}
  </div>

</div>

`;
    
    
    $('#' + graf_data.id + '_row').html('');
    
    $('#' + graf_data.id + '_row').prepend(graf_html);
    
    var z_index = 10;
    
    
    var graf = $('#' + graf_data.id + ' .we_graf_body');
    
    graf.find('.we_graf_line_box').remove();
    graf.find('.we_graf_points_box .we_graf_point').remove();
    
    var ver_dist = $('#' + graf_data.id + ' .graf_body_line').eq(0).outerHeight();
    var hor_dist = $('#' + graf_data.id + ' .graf_hor_num').eq(0).outerWidth();
    
    
    if ( graf_data.time_grupa == 'MIN' ) hor_dist = hor_dist/60;
    
    /*
    <div class="we_graf_line_box">
    <div class="graf_line_segment"></div>
    */
    
    
    
    $.each(graf_data.lines, function(line_ind, line) {
      
      var hide_line_style = 'display: none;';
    
      if ( window.we_graf_visible ) { 
        if ( window.we_graf_visible.sifra == 'SVI') hide_line_style = '';
        if ( window.we_graf_visible.sifra == 'SOLAR_N' && line.id == 'line_solar_napon') hide_line_style = '';
        if ( window.we_graf_visible.sifra == 'SOLAR_S' && line.id == 'line_solar_struja') hide_line_style = '';
        if ( window.we_graf_visible.sifra == 'AKU_N' && line.id == 'line_aku_napon') hide_line_style = '';
        if ( window.we_graf_visible.sifra == 'AKU_S' && line.id == 'line_aku_struja') hide_line_style = '';
      };
      
      var line_box_html = 
          `<div class="we_graf_line_box" id="${line.id}" style="z-index: ${z_index}; ${hide_line_style}"></div>`;
      
      graf.append(line_box_html);
            
      var line_box = $(`#${line.id}`)[0];
      
      var svg_box = document.createElementNS("http://www.w3.org/2000/svg", "svg");
      svg_box.setAttribute('style', "fill: none; stroke-width: 0; stroke-linecap: round;");
      svg_box.setAttribute('width', `${graf.outerWidth()}`);
      svg_box.setAttribute('height', `${graf.outerHeight()}`);
      
      line_box.appendChild(svg_box);
      
      z_index += 1;
      var start_x = 0;
      
      var all_line_points = '';
      
      $.each(line.points, function(point_ind, point) {
        
        if ( point_ind < line.points.length - 1 ) {
          
          var start_y = null;
          
          if ( point !== null ) {
            start_y = graf.outerHeight() - (point*ver_dist);
          } else {
            start_y = graf.outerHeight();
          };
          
           
          var end_x = start_x + hor_dist;
          // pošto je koordinatni sustav od top left prema dolje 
          // moram zapravo oduzeti vrijednost y od ukupne visine 
          
          var end_y = null;
          if ( line.points[point_ind+1] !== null ) {
            end_y = graf.outerHeight() - ( line.points[point_ind+1]*ver_dist);
          } else {
            end_y = graf.outerHeight();
          };
          
          var newLine = document.createElementNS('http://www.w3.org/2000/svg','line');
          
          var line_width = 2;
          var hide_point_style = ``; 
          
          if ( point == null || line.points[point_ind+1] == null ) {
            line_width = 0;
          };
          
          newLine.setAttribute('id', `${line.id}_${point_ind}`);
          newLine.setAttribute('x1', `${start_x}`);
          newLine.setAttribute('y1', `${start_y}`);
          newLine.setAttribute('x2', `${end_x}`);
          newLine.setAttribute('y2', `${end_y}`);
          newLine.setAttribute('style', `stroke: ${line.color}; stroke-width: ${line_width};`);
          svg_box.append(newLine);
          
          
          var css_class_we_small = '';
          
          if ( graf_data.time_grupa == 'MIN' ) css_class_we_small = 'we_small';
          
          // samo prva točka mora biti označena na početku line segmenta 
          // SVE OSTALE TOČKE SU NA KRAJU LINE SEGMENTA !!!!!!!
          
// ----------------START-------------- PRVA TOČKA SEGMENTA 
          if ( point_ind == 0 ) {
            
            var we_graf_point = 
`
<div  class="we_graf_point ${css_class_we_small} ${line.id}" 
      id="${line.id}_${point_ind}_point" 
      style=" left: ${start_x}px;
              top: ${start_y}px;
              background-color: ${line.color};
              ${hide_line_style}
              ${hide_point_style}
              ${point_opacity}">

  <div class="graf_point_wrapper" style="background-color: ${line.color};">
    <div class="graf_point_value">${point}</div>
  </div>

</div>
`;
            all_line_points += we_graf_point;
          };
          
// ----------------END-------------- PRVA TOČKA SEGMENTA          

          
          var we_graf_point = 
`
<div  class="we_graf_point ${css_class_we_small} ${line.id}" 
      id="${line.id}_${point_ind+1}_point"
      style=" left: ${end_x}px;
              top: ${end_y}px;
              background-color: ${line.color};
              ${hide_line_style}
              ${hide_point_style}
              ${point_opacity}">

  <div class="graf_point_wrapper" style="background-color: ${line.color};">
    <div class="graf_point_value">${line.points[point_ind+1]}</div>
  </div>

</div>
`;
          all_line_points += we_graf_point;
          
          start_x += hor_dist;
          
        }; // ako index od pointa nije zadnji
        
      }); // loop po svim segmentima od linije
      
      var we_graf_points_box = graf.find('.we_graf_points_box');
      we_graf_points_box.prepend(all_line_points);
      
    }); // loop po svim linijama
    
    
    setTimeout( function() {
      
      var broj_vertical_polja = $(`#${graf_data.id} .graf_ver_num`).length;
      var visina_vertical_polja = broj_vertical_polja * 30;
      
      $(`#${graf_data.id} .we_graf_ver_numbers`).css('height', visina_vertical_polja + 'px');
      
      
      $('#graf_solar_row').off('scroll');
      $('#graf_solar_row').on('scroll', function() {
        // console.log($('#graf_solar_row')[0].scrollLeft);
        $(`#${graf_data.id} .we_graf_ver_numbers`).css('left', $('#graf_solar_row')[0].scrollLeft + 'px');
      });
        
      
      
      var solar_from_date = $('#solar_from_date');
      
      var graf_div = $('#graf_solar_row .we_graf_top');
      var div_pos = graf_div.offset();
        
      var graf_pos_x = div_pos.left;
      var graf_pos_y = div_pos.top;
      
      var sirina_ver_numbers = $('.we_graf_ver_numbers').eq(0).outerWidth();
      var duzina_hor_jedinice = $('.graf_hor_num').eq(0).outerWidth();
      
      graf_pos_x = div_pos.left;
      graf_pos_y = div_pos.top
      
      
      var sol_mouse_x = graf_pos_x; // - sirina_ver_numbers;
      
      var graf_needle_html =
`
<div id="graf_needle" style="transform: translate(${sol_mouse_x}px, ${0}px)">
  <div id="graf_needle_display">&nbsp;<br>&nbsp;</div>
</div>
`;
      
      
      $('#graf_needle').remove();
      
      // samo ako je graf po minutama 
      if ( window.solar_time_grupa.sifra == 'MIN' ) {
        $('#graf_solar_row .we_graf_top').prepend(graf_needle_html);

        // $('body').off('click', remove_needle);
        // $('body').on('click', remove_needle);
        
        
      };
      
      
      var sol_mouse_y = -1;
      $('#graf_solar_row .we_graf_top').off('mousemove');
      $('#graf_solar_row .we_graf_top').on('mousemove', function(event) {
        
                
        if ( window.solar_time_grupa.sifra == 'MIN' ) {
          
          if ( $('#graf_needle').length == 0 ) $('body').prepend(graf_needle_html);
        
          div_pos = graf_div.offset();
          graf_pos_x = div_pos.left;
          graf_pos_y = div_pos.top;

          sol_mouse_x = event.pageX - graf_pos_x; // - sirina_ver_numbers;
          
          // console.log('sol_mouse_x: ' + sol_mouse_x);
          
          var solar_data_index = parseInt( (sol_mouse_x-30) / 2 );
          if ( solar_data_index < 0 ) solar_data_index = 0;
          
          clearTimeout(window.graf_needle_timeout_id);
          window.graf_needle_timeout_id = setTimeout(function() {
            
            window.last_needle_timestamp = Date.now();
            var time = window.last_solar_data.solar_data_history[solar_data_index].time;
            
            var graf_start = get_DATE_as_milisec(solar_from_date).ms_at_zero;
            
            time = time - graf_start;
            
            var napon = window.last_solar_data.solar_data_history[solar_data_index].channels.ch_0;
            $('#graf_needle').css('transform', `translate( ${ sol_mouse_x }px, ${0}px )`);
            $('#graf_needle_display').html(`${get_time_as_hh_mm_ss(time)}<br>${ zaokruzi(napon, 2) ? (zaokruzi(napon, 2) + 'V' ) : '' }`);
            
            
          }, 200);
          
          // sol_mouse_y = event.pageY - graf_pos_y;
          // console.log('x mouse: ' + sol_mouse_x + '     y mouse:    ' + sol_mouse_y);
          
        }; // kraj ako je graf po minutama
        
    }); // kraj mouse move listener
      
      
    }, 1000 );
    
  };
  this_module.create_we_graf = window.create_we_graf;
  
  
  window.we_animate_segment = function( line_id, seg_id, end_y1, end_y2 ) {
    
    var line = $(`#${line_id} svg`);
    
    var seg = $('#' + seg_id)[0];
    
    var seg_index = Number( seg_id.replace( (line_id + '_'), '') );
    
    // početne pozicije su na dnu
    var y1_pos = line.outerHeight();
    var y2_pos = line.outerHeight();

    function we_step() {

      // ako je ili počete ili kraj još uvijek ispod final pozicije
      // koordinatni sustav je malo čudan jer jer y ide prema dolje
      if ( (y1_pos > end_y1 ) || (y2_pos > end_y2 ) ) {

        if (y1_pos > end_y1 ) {
          y1_pos -= 10;
          seg.setAttribute('y1', `${y1_pos}`);
        };

        if (y2_pos > end_y2 ) {
          y2_pos -= 10;
          seg.setAttribute('y2', `${y2_pos}`);
          
          // specijalni slučaj kada je segment prvi tj nulti ----> onda je selector seg index
          if ( seg_index == 0 ) $(`#${line_id}_${seg_index}_point`).css('top', y1_pos + 'px');
          // inače je seg index + 1
          $(`#${line_id}_${seg_index+1}_point`).css('top', y2_pos + 'px');
        }; 

        window.requestAnimationFrame(we_step);

      } else if ( (y1_pos < end_y1 ) || (y2_pos < end_y2 ) ) {
        
        // ovaj elese je ako sam prešišao poziciju i moram se vratiti malo nazad
        // tj na točnu final poziciju

        if (y1_pos < end_y1) seg.setAttribute('y1', `${end_y1}`);
        if (y2_pos < end_y2) {
          seg.setAttribute('y2', `${end_y2}`);
          if( seg_index == 0 ) $(`#${line_id}_${seg_index}_point`).css('top', end_y1 + 'px');
          $(`#${line_id}_${seg_index+1}_point`).css('top', end_y2 + 'px');
        };
        
        
        // ovdje više ne pozivam request anim frame jer sam došao do final pozicije !!!!!

      };

    }; // kraj we step func 

    window.requestAnimationFrame(we_step);
    
  };
  this_module.we_animate_segment = window.we_animate_segment;
  
  
  window.animate_we_graf_lines = function(graf_data) {
    
    
    var we_graf_line_box = $('#' + graf_data.id + ' .we_graf_line_box');
    we_graf_line_box.removeClass('we_animate');
    we_graf_line_box.css('opacity', '0');
    
    setTimeout(function() { we_graf_line_box.addClass('we_animate'); }, 50);
    
    setTimeout(function() { we_graf_line_box.css('opacity', '1'); }, 100);
    setTimeout(function() { we_graf_line_box.removeClass('we_animate'); }, 1100);
    
    var ver_dist = $('#' + graf_data.id + ' .graf_body_line').eq(0).outerHeight();
    
    var y1_zero = $('#' + graf_data.id + ' .we_graf_body').eq(0).outerHeight();
    var y2_zero = y1_zero;

    
    
    $.each(graf_data.lines, function(line_ind, line) {
      
      $.each(line.points, function(point_ind, point) {
        
        var seg_id = line.id + '_' + point_ind;
        var seg = $('#' + seg_id)[0];
        
        if ( point_ind < line.points.length - 1 ) {
          // spusti početak linije
          seg.setAttribute('y1', `${y1_zero}`);
          // spusti kraj linije
          seg.setAttribute('y2', `${y2_zero}`);
          // spusti točku
          $(`#${line.id}_${point_ind+1}_point`).css('top', y1_zero + 'px');
        };
        
      });
      
    });    
    
    
    $.each(graf_data.lines, function(line_ind, line) {
      $.each(line.points, function(point_ind, point) {
        
        var seg_id = line.id + '_' + point_ind;
        
        var end_y1 = y1_zero - (point * ver_dist);
        var end_y2 = y2_zero - (line.points[point_ind+1] * ver_dist);
        
        if ( point_ind < line.points.length - 1 ) window.we_animate_segment(line.id, seg_id, end_y1, end_y2);
      });
    });
    
    
  };
  this_module.animate_we_graf_lines = window.animate_we_graf_lines;
  
  
  window.animate_circle_bar = function( circle_bar_id, target_value, start_value, min_value, max_value, sufix ) {
    
    var diff = target_value - start_value;
    
    function ease_val(time_perc, diff) {
      var value = -diff/2 * (Math.cos(Math.PI*time_perc) - 1);
      return value;
    };
    
    var animation_duration = 1*1000;
    var curr_num = start_value;
    
    var circ_elem = $(`#${circle_bar_id} .circle_bar_percent`);
    var circ_grad = $(`#${circle_bar_id} .circle_bar_percent .circle_bar_grad`);
    
    var circle_bar_number = $(`#${circle_bar_id} .circle_bar_number`);
    var circle_bar_perc_value = $(`#${circle_bar_id} .circle_bar_perc_value`);
    
    
    var start_time = Date.now();
    var time_perc = 0;
    var eased_value = 0;
    
    
    circ_elem.removeClass('we_animate');
    circ_elem.css('transform', 'translateX(0) translateY(-8%) rotate(-180deg)');
    circ_grad.css('transform', `rotate(180deg)`);
    
    
    
    circle_bar_number.text( 0 + sufix);
    circle_bar_perc_value.text(0 + '%');
    

    function we_circ_step() {
      
      if ( eased_value < target_value && time_perc <= 1 ) {
        
          time_perc = (Date.now() - start_time)/animation_duration;

          eased_value = ease_val(time_perc, diff);
          var eased_perc_value = eased_value/max_value*100;

          circle_bar_number.text(zaokruziIformatirajIznos(eased_value, 2) + sufix);
          circle_bar_perc_value.text(parseInt(eased_perc_value)+'%');

          window.requestAnimationFrame(we_circ_step);
        
      } else if ( eased_value > target_value || time_perc > 1 ) {
        
        circle_bar_number.text(zaokruziIformatirajIznos(target_value, 2) + sufix);
        var eased_perc_value = target_value/max_value*100;
        circle_bar_perc_value.text(parseInt(eased_perc_value)+'%');
        
      };
      
    };
    
    window.requestAnimationFrame(we_circ_step);
    
    var target_perc_value = target_value/max_value;
    var target_angle = 180*target_perc_value;
    
    var perc = -180 + parseInt(target_angle);

    setTimeout(function() { circ_elem.addClass('we_animate'); }, 100);
    
    setTimeout(function() { 
      circ_elem.css('transform', `translateX(0) translateY(-8%) rotate(${perc}deg)`);
      circ_grad.css('transform', `rotate(${-perc}deg)`);
    }, 150);
    
  };
  
  
  function get_empty_cit_data() {
    
    this_module.cit_data = {
      
      /* ovo su postojeći propsi */
      
      pp_sifra: null,
      pp_address: null,
      
      contact: null,
      indoors: null,
      pp_latitude: null,
      pp_longitude: null,
      pp_max_height: null,
      pp_name: null,
      video: null,
      enter_device_id: null,
      exit_device_id: null,
      has_exit_device: null,
      
      pp_count_out_num: null,
      
      
      
      /* ovo su novi propsi */
      
      fixed_open: false,
      fixed_close: false,
      
      pp_tel: null,
      pp_email: null,
      
      pp_count_in_num: null,
      pp_available_out_num: null,
      pp_available_in_num: null,
      
      pp_owners: null,
      pp_prices: null,
      pp_current_price: null,
      
      pp_images: null,
      user_saved: null,
      user_edited: null,
      saved: null,
      edited: null,
      
      special_places: null,
  
      contract_exp_date: null,
      
      published: false

    };
    
    
  };
  this_module.get_empty_cit_data = get_empty_cit_data;
  
  
  var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {
    this_module.get_empty_cit_data();
  };
  
  
  function hide_pp_list() {
    $('#park_info_lista').addClass('cit_hide');
    $(this).css('display', 'none');
    $('#show_pp_list_btn').css('display', 'flex');
  };
  this_module.hide_pp_list = hide_pp_list;
  
  function show_pp_list() {
    $('#park_info_lista').removeClass('cit_hide');
    $(this).css('display', 'none');
    $('#hide_pp_list_btn').css('display', 'flex');
  };
  this_module.show_pp_list = show_pp_list;
  
  
  function save_park_place( save_or_update ) {
    
    return function () {

      var all_pp_fileds = 
        ['pp_tel', 'pp_email', 'pp_name', 'pp_address', 'pp_latitude', 'pp_longitude'];
      
      var numeric_fields = 
        ['pp_count_out_num', 'pp_count_in_num']
      
      var too_short = "";
      var pass_repeat_same = false;

      $('#parking_places_form .cit_input').css('border', '');


      $.each( all_pp_fileds, function( input_index, input ) {

        if ( $('#'+input).val().length < 3 ) {
                                          
          var pp_label_text = $('#'+input+'_label').text();
          too_short += pp_label_text + ',\n'; 
          $('#'+input).css('border', '1px solid #ff0058');

        };
      });
      

      if (too_short !== "") {
          alert('Podaci u:\n\n' + too_short.slice(0, -2) + '\nnedostaju ili imaju manje od 3 slova!');
        return;
      };      
      
      $.each( numeric_fields, function( input_index, input ) {
        if ( $('#'+input).val().length == 0 ) {
          var pp_label_text = $('#'+input+'_label').text();
          
          too_short += pp_label_text + ',\n'; 
          $('#'+input).css('border', '1px solid #ff0058');

        };
      });
      

      if (too_short !== "") {
          alert('Podaci u:\n\n' + too_short.slice(0, -2) + '\nsu obavezni!');
          return;
      };      
  
      $('#pp_email').css('border', '');

      var mail = $('#pp_email').val();
      var is_email_ok = mail.search(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

      if (is_email_ok === -1 || mail.indexOf('.') === -1) {
          alert('Email adresa za kontakt parkirališta nije ispravna!');

          $('#pp_email').css('border', '1px solid #e64532');
          return;
      };

  
      if ( !cit_user ) {
        popup_nedostaje_user();
        //alert('NISTE ULOGIRANI !!!');
        return;
      };

      var pp_object = this_module.cit_data;  

      // form to data ----> radi dvostruki posao 
      // upisuje sve vrijednosti iz polja u formu i ubacuje u cit data 
      // ujedno validira jesu li neke vrijednosti koje trebaju biti numeric
      
      var input_validation_ok = this_module.form_to_data(); 
    
      // console.log( pp_object );
      // return;
      
      if ( !input_validation_ok ) return;
      
      var request_type = null;
      var request_url = null;
    
      var popup_text = 
`
<h5>Čestitamo!</h5>
<p>
Podaci parkirališta su spremljeni.
<br>
Parkiralište će biti prikazano u mobilnoj aplikaciji i biti dostupno korisnicima nakon što potpišemo ugovor s vama.
Sustav je automatski dodjelio prosječnu cijenu od 3kn po satu, ali to ćemo promijeniti naknadno po dogovoru :).
<br>
U roku od 24 sata će Vas nazvati naš agent kako bi prikupili detaljne podatke i dogovorili sastanak.
<br>
Nadamo se uspješnoj suradnji!
</p>
`;
    
      var time_now =  Date.now();
      
    
    if ( save_or_update == 'save') {
      
      request_type = "POST";
      request_url = "/spremi_park_place";
      
      pp_object.saved = time_now;
      pp_object.edited = time_now;
      
      pp_object.user_edited =  { 
        user_id: cit_user._id, 
        user_number: cit_user.user_number,
        user_name: cit_user.name,
        full_name: cit_user.full_name,
        time_stamp: time_now
      };
     
      pp_object.user_saved = { 
        user_id: cit_user._id,
        user_number: cit_user.user_number,
        user_name: cit_user.name,
        full_name: cit_user.full_name,
        time_stamp: time_now
      };
      
      if ( pp_object.pp_owners == null ) {
        // ovo je defaultni 
        pp_object.pp_owners = [{
            _id: cit_user._id,
            name: cit_user.name,
            full_name: ( cit_user.full_name || '' ),
            business_name: ( cit_user.business_name || '' )
        }];
        
      };
      
      if ( pp_object.pp_prices == null ) {
        
        pp_object.pp_prices = [{
          id: Date.now(),
          pp_price_day : {
            sifra : "any_day",
            naziv : "BILO KOJI DAN"
          },
          pp_price_time_from: 0,
          pp_price_time_to: 86399800,
          pp_basic_price_in: 0,
          pp_basic_price_out: 3,
          pp_revenue: 50,
          pp_owner_revenue: 50,
          max_pp_price_factor: 0,
          dyn_pp_price: false,
          dyn_zauzetost_granica: 100
        }];
      };

      
      if ( !pp_object.pp_count_out_num ) pp_object.pp_count_out_num = 0;
      if ( !pp_object.pp_count_in_num ) pp_object.pp_count_in_num = 0;
      
      pp_object.pp_available_out_num = ( pp_object.pp_count_out_num || 0 );
      pp_object.pp_available_in_num = ( pp_object.pp_count_in_num || 0 );
      
      
      
    }; // kraj ako je save varijabla
    
    
    if ( save_or_update == 'edit') {
      
      request_type = "PUT";
      request_url = "/spremi_park_place/" + pp_object._id;
      popup_text = 'PARKIRALIŠTE JE AŽURIRANO !';
      
      
      pp_object.edited = time_now;
      
      pp_object.user_edited =  { 
        user_id: cit_user._id, 
        user_number: cit_user.user_number,
        user_name: cit_user.name,
        full_name: cit_user.full_name,
        time_stamp: time_now
      };
      
    };
      
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: request_type,
      url: request_url,
      data: JSON.stringify(pp_object),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {
      
      console.log(response);
      
      if ( response.success == true ) {
        
        this_module.cit_data._id = response.pp_id;
        
        // show_popup, popup_text, popup_progress, show_buttons
        show_popup_modal(true, popup_text, null, 'ok');
        
        $('#cit_popup_modal_OK').off('click');
        $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok'); });
        
        
        
        show_save_or_edit_button('edit_park_place_btn');
        
        
      } else {
        
        popup_text = 'GREŠKA PRI SPREMANJU PODATAKA PARKIRALIŠTA!';
        
        if ( response.msg ) popup_text = response.msg;
        
        show_popup_modal('error', popup_text, null, 'ok' );
        $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
        
      };
    })
    .fail(function(error) {
      
      console.log(error);
      popup_text = 'GREŠKA PRI SPREMANJU PODATAKA PARKIRALIŠTA!';
      
      if ( error.msg ) popup_text = error.msg;
      
      // show_popup, popup_text, popup_progress, show_buttons
      show_popup_modal('error', popup_text, null, 'ok' );
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
      
    });
    
    console.log(this_module.cit_data);
      
    };
    
  };
  this_module.save_park_place = save_park_place;

  
  function form_to_data() {
    
    var input_validation_ok = true;
    
    delete this_module.cit_data.__v;
    
    $.each( this_module.cit_data, function( data_key, input_id ) {
      
      var prop_done = false;
      
      var skip_this_props = 
          [
            '_id',
            'pp_owners',
            'contact',
            'indoors',
            'video',
            'enter_device_id', 
            'exit_device_id',
            'has_exit_device',
            'pp_images',
            'edited',
            'saved', 
            'user_saved',
            'user_edited',
            'pp_prices',
            'pp_sifra',
            'published',
          ];
      
      var convert_this_props_to_numbers = 
          [
            'pp_count_in_num',
            'pp_count_out_num',
            'pp_latitude',
            'pp_longitude',
            'pp_max_height',
          ];
      
      // ----------------- PRESKACEM ZAPISIVANJE OVIH PROPSA ...samo stavim prop done da bude true i ne mijenjam ništa
      // jer se te vrijednosti ne uzimaju iz input polja nego se kreiraju na druge specijalne načine :) 
      if ( $.inArray( data_key, skip_this_props ) > -1  ) { prop_done = true; };
      
      // resetiraj crveni rub na inputima da bude normalna
      $('#' + data_key).css('border', '');
      
      if ( prop_done == false && data_key == 'contract_exp_date' && $('#'+data_key).val() !== '' ) {
        prop_done = true;
        this_module.cit_data[data_key] = get_DATE_as_milisec( $('#'+data_key), 0).ms_at_zero;
      };
      
      // za sve ostale propertije upiši vrijednost iz priparadjućih polja u html-u !!!
      if ( prop_done == false ) this_module.cit_data[data_key] = $('#' + data_key ).val();
      
      // ----------------- pretvori ove propse u NUMBER
      if ( $.inArray( data_key, convert_this_props_to_numbers ) > -1 ) { 
        
        if ( $.isNumeric( this_module.cit_data[data_key] )  ) {
          // ako je numeric
          this_module.cit_data[data_key] = Number( this_module.cit_data[data_key]);
          
        } else {
          
          // ako NIJE numeric ----> upozori da je krivo samo ako NIJE PRAZNO !!!!
          if (  $('#' + data_key).val() !== '' ) {
            input_validation_ok = false;
            $('#' + data_key).css('border', '1px solid red');
          } else {
            // ako nije numeric i ako je prazan input onda resetiraj na null
            this_module.cit_data[data_key] = null;
          };

        };
        
      };
      
    });
    
    return input_validation_ok;
    
  };
  this_module.form_to_data = form_to_data;
  
  function data_to_form(data) {
    
    
    $('.pp_owner_row').remove();
    
    $('.pp_price_interval_row').remove();

    // resetiraj sve crvene bordere prilikom otvaranja novog park place
    $('#parking_places_component input').css('border', '');
    
    $.each( data, function( data_key, data_value ) {
      

      var prop_done = false;  
      
      if ( data_key == 'fixed_open' || data_key == 'fixed_close' ) {
        set_switch_html(this_module, '#pp_'+data_key, data_key);
        prop_done = true;
      };
      
      if ( data_key == 'pp_owners' && data_value && $.isArray( this_module.cit_data.pp_owners) ) { 
        
        prop_done = true;
        
          $.each( this_module.cit_data.pp_owners, function( pp_owner_index, owner ) {

            var owner_row_html = this_module.pp_owner_html(owner); 
            $('#users_pp_owners_box').append(owner_row_html);

            setTimeout(function() {
              $('.cit_tooltip').tooltip();
              $(`#delete_pp_owner_${owner._id}`).on( 'click', this_module.delete_pp_owner );
            }, 100);

          });

        };


      if ( data_key == 'pp_prices' && data_value && $.isArray( this_module.cit_data.pp_prices) ) { 

        prop_done = true;
        
          $.each( this_module.cit_data.pp_prices, function( pp_price_index, price ) {

            var price_row_html = this_module.create_price_row_html(price);

            $('#pp_price_box').append(price_row_html);

            setTimeout(function() {
              $('.cit_tooltip').tooltip();
              $(`#delete_pp_price_interval_${price.id}`).on( 'click', this_module.delete_price_row );
            }, 100);

          });

        };

      
      if ( prop_done == false && data_key == 'contract_exp_date' ) {
        
        prop_done = true;
        var date_string = getMeADateAndTime(data_value, 'd.m.y').datum;
        $('#'+data_key).val(date_string);
        // this_module.cit_data[data_key] = get_DATE_as_milisec( $('#'+data_key), 0).ms_at_zero;
      };


      if ( prop_done == false ) $('#' + data_key ).val(data_value);
      
      
    });
    
    
    fill_user_metadata(data, '#cit_pp_user_saved', '#cit_pp_user_edited');
    
    
  };
  this_module.data_to_form = data_to_form;
  
  
  function clear_all_pp_markers() {

    if (!window.all_we_map_markers) return;


    $.each(window.all_we_map_markers, function (marker_index, marker) {
      
      // brišem SVEEEEEE moguće listenere na tom markeru
      google.maps.event.clearInstanceListeners(marker);
      
      // kada postavim  map od markera na null onda zapravo obrišem marker sa mape !!!!
      // ( zaista čudan način od googla ... umjesto da su napravili neku metodu remove ???? )
      window.all_we_map_markers[marker_index].setMap(null);
      
    });
    
    window.all_we_map_markers = [];

  };
  this_module.clear_all_pp_markers = clear_all_pp_markers;


  function show_pp_markers() {

    this_module.clear_all_pp_markers();

    wait_for(`window.pp_data_for_list !== null`, function() {


      if ( !window.all_we_map_markers || window.all_we_map_markers.length == 0 ) {
        
        window.all_we_map_markers = [];

        $.each(window.pp_data_for_list, function(pp_index, park) {

          var pp_Latlng = { lat: park.pp_latitude, lng: park.pp_longitude };

          var marker = new google.maps.Marker({
            position: pp_Latlng,
            park_data: park,
            icon: '/img/google_we_icon.png'
          });

          marker.addListener('click', function(e) {
            // console.log(e);
            // console.log(marker);
            var park_data = marker.park_data;
            var pp_list_module = window['/right_sidebar/cit_search_list/park_places/cit_pp_list_module.js'];
            pp_list_module.open_park_place(park_data._id);

            
          });        

          window.all_we_map_markers.push(marker);

        });

      };


      $.each(window.all_we_map_markers, function(marker_index, marker) {
        window.all_we_map_markers[marker_index].setMap(window.we_site_map);
      });
      
      setTimeout( function() {
        
        window.we_site_map.setZoom(12);
        var center_point = new google.maps.LatLng(45.79500308999068, 15.973694855983695);
        window.we_site_map.setCenter(center_point);

      }, 200); 


    }, 50000);  

  };
  this_module.show_pp_markers = show_pp_markers;
  
  
  function map_cursor_za_odabir_koordinata() {
    
    
    // ako kojim slučajem mapa nije inizijalizirana
    if ( !window.we_site_map ) this_module.initMap();
    
    
    // obriši sve geo pinove
    this_module.clear_all_pp_markers();
    
    // obriši sve listenere da se ne duplaju !!!
    google.maps.event.clearListeners(window.we_site_map, 'click');
    
    // dodaj samo pin za park koji se otvara sada
    

    // popunio sam cit_data od ovog modula kada je user kliknuo na neki od parkirališta na listi
    // to se odvija u pp list module file-u !!!!
    
    var park = this_module.cit_data;
    
    
    if ( !park.pp_latitude || !park.pp_longitude ) return;
    
    var pp_Latlng = { lat: park.pp_latitude, lng: park.pp_longitude };

    var marker = new google.maps.Marker({
      position: pp_Latlng,
      icon: '/img/google_we_icon.png'
    });
  
    window.all_we_map_markers.push(marker);
    window.all_we_map_markers[0].setMap( window.we_site_map );
    
    
    window.we_site_map.setZoom(14);
    
    // var center_point = new google.maps.LatLng(45.79500308999068, 15.973694855983695);
    // window.we_site_map.setCenter(center_point);
    
    
    setTimeout( function() {
      window.we_site_map.panTo(marker.getPosition()); 
    }, 400);
    
    
    setTimeout( function() {
      window.we_site_map.setZoom(16);
    }, 800);
    
    
    
    /*
    // pretvori cursor u križ za označavanje koordinata na mapi
    window.we_site_map.setOptions({draggableCursor:'crosshair'});

    // dodaj listener za klik na mapu
    window.we_site_map.addListener('click', function( event ) {
      $('#pp_latitude').val( event.latLng.lat() );
      $('#pp_longitude').val( event.latLng.lng() );
    });
    
    */
    
  };
  this_module.map_cursor_za_odabir_koordinata = map_cursor_za_odabir_koordinata;
  
  
  function initMap() {
    
    
    if ( window.we_site_map ) {
      this_module.clear_all_pp_markers();
      // obriši sve listenere da se ne duplaju !!!
      google.maps.event.clearListeners(window.we_site_map, 'click');
    };

    var myLatlng = {lat: 45.79706643181554, lng: 15.979551948547169};

    // ako već nije inicijalizirana mapa
    // if ( !window.we_site_map ) {
      
        window.we_site_map = new google.maps.Map(document.getElementById('google_map_select_parking'), {
          zoom: 12,
          center: myLatlng,
          scrollwheel: false,
        });
        this_module.map = window.we_site_map;
      
    // }; // kraj ako nije inicijalizirana mapa
    
      
      
      if ( window.pp_edit_mode !== true ) {
        this_module.show_pp_markers();
      };

  };
  this_module.initMap = initMap;

  function create_new_price_object() {
    return {
      id: null,
      pp_price_day: null,
      pp_price_time_from: null,
      pp_price_time_to: null,
      pp_basic_price_in: null,
      pp_basic_price_out: null,
      pp_revenue: null,
      pp_owner_revenue: null,
      max_pp_price_factor: 0,
      dyn_pp_price: false,
      dyn_zauzetost_granica: 100

    };
  };
  this_module.create_new_price_object = create_new_price_object;
  
  function select_pp_price_day(selected_day_object) {

    var curr_input = $('#'+current_input_id);
    curr_input.val( selected_day_object.naziv );
    this_module.current_pp_price_day = selected_day_object;
    
  };
  this_module.select_pp_price_day = select_pp_price_day;
  
  this_module.current_dyn_pp_price = false;
  
  function switch_dyn_pp_price() {
    this_module.current_dyn_pp_price = $('#' + current_input_id).hasClass('on') ? true : false;
  };
  this_module.switch_dyn_pp_price = switch_dyn_pp_price;
  
  function pp_owner_html(pp_owner_object_small) {
 
    var new_pp_owner_html = 
`
<div class="pp_owner_row" id="pp_owner_row_${pp_owner_object_small._id}">

  <div class="pp_owner_cell" style="width: 25%">${ pp_owner_object_small.name }</div>
  <div class="pp_owner_cell" style="width: 25%">${ pp_owner_object_small.full_name || '' }</div>
  <div class="pp_owner_cell" style="width: 25%">${ pp_owner_object_small.business_name || '' }</div>

  <div class="pp_owner_cell" style="width: 25%">

    <div id="delete_pp_owner_${pp_owner_object_small._id}"
         class="cit_button cit_center cit_tooltip"
         style="width: 20px; float: right;"
         data-toggle="tooltip" data-placement="bottom"
         title="OBRIŠI VLASNIKA">
      <i class="fas fa-minus"></i>
    </div>
    
  </div>

</div>

`;
    return new_pp_owner_html;
  };
  this_module.pp_owner_html = pp_owner_html;      
  
  function select_pp_owner(pp_owner_object) {

    var pp_owner_object_small = {
      _id: pp_owner_object._id,
      name: pp_owner_object.name,
      full_name: (pp_owner_object.full_name || null),
      business_name: (pp_owner_object.business_name || null)
    };

    if ( $.isArray(this_module.cit_data.pp_owners) == false  ) {
      this_module.cit_data.pp_owners = [];
    };

    var pp_owner_already_exists = false;
    $.each(this_module.cit_data.pp_owners, function(pp_owner_index, owner) {
      if ( owner._id == pp_owner_object_small._id ) pp_owner_already_exists = true;
    });
    
    
    if ( pp_owner_already_exists == false ) {
      
      this_module.cit_data.pp_owners.push(pp_owner_object_small);
      
      var owner_row_html = this_module.pp_owner_html(pp_owner_object_small);
      $('#users_pp_owners_box').append(owner_row_html);
      
      setTimeout(function() {
        $('.cit_tooltip').tooltip();
        $(`#delete_pp_owner_${pp_owner_object_small._id}`).on( 'click', this_module.delete_pp_owner );
      }, 100);
      
    };
  
    
  };
  this_module.select_pp_owner = select_pp_owner;
  
  function delete_pp_owner() {
    var pp_owner_id = this.id.replace('delete_pp_owner_', '');

    $('#pp_owner_row_' + pp_owner_id ).remove();

    var delete_this_index = null;
    $.each(this_module.cit_data.pp_owners, function(pp_owner_index, owner) {
      if ( pp_owner_id == owner._id ) delete_this_index = pp_owner_index;
    });
    if ( delete_this_index !== null ) this_module.cit_data.pp_owners.splice(delete_this_index, 1);

  };
  this_module.delete_pp_owner = delete_pp_owner;

  function create_price_row_html(new_price) {
    
    
    var dinamicna_cijena_text = '--';
    
    if ( new_price.dyn_pp_price ) {

      dinamicna_cijena_text =

      `
      ${ new_price.dyn_zauzetost_granica < 100 ? ('>' + new_price.dyn_zauzetost_granica + '%' ): '' }&nbsp;|&nbsp;
      ${ new_price.max_pp_price_factor  ? (new_price.max_pp_price_factor + 'X') : ''}

      `;
      
    };
    
    
    var price_html = 
        `
        <div class="pp_price_interval_row" id="${new_price.id}">

          <div class="pp_price_cell" style="width: 10%">${new_price.pp_price_day.naziv}</div>
          
          <div class="pp_price_cell" style="width: 10%">
            ${ get_time_as_hh_mm_ss(new_price.pp_price_time_from, 'from') }
          </div>
          <div class="pp_price_cell" style="width: 10%">
            ${ get_time_as_hh_mm_ss(new_price.pp_price_time_to, 'to') }
          </div>
          
          <div class="pp_price_cell" style="width: 15%">
            ${ new_price.pp_basic_price_in } IN&nbsp;|&nbsp;${ new_price.pp_basic_price_out } OUT
          </div>
          
          <div class="pp_price_cell" style="width: 15%" style="color: red;">
            ${dinamicna_cijena_text}
          </div>          

          <div class="pp_price_cell" style="width: 15%">
            ${ new_price.pp_revenue/100 * new_price.pp_basic_price_in } | ${ new_price.pp_revenue/100 * new_price.pp_basic_price_out }
            (${ new_price.pp_revenue }%)
          </div>

          <div class="pp_price_cell" style="width: 15%">
            ${ new_price.pp_owner_revenue/100 * new_price.pp_basic_price_in } | ${ new_price.pp_owner_revenue/100 * new_price.pp_basic_price_out } (${ new_price.pp_owner_revenue }%)
          </div>

          <div class="pp_price_cell" style="width: 10%">
            <div id="delete_pp_price_interval_${new_price.id}" 
                 class="cit_button cit_center cit_tooltip" 
                 style="width: 20px; float: right;"
                 data-toggle="tooltip" data-placement="bottom"
                 title="OBRIŠI CIJENU">
              <i class="fas fa-minus"></i>
            </div>
          </div>

        </div>

        `;
    
    return price_html;
    
  };
  this_module.create_price_row_html = create_price_row_html;
  
  function add_pp_price() {
    
    var new_price = this_module.create_new_price_object();
    
    $('#card_for_create_new_pp_interval input').css('border', '');
    
    
    if ( !this_module.current_pp_price_day ) {
      $('#pp_price_days_input').css('border', '1px solid red');
      return;
    };
    
    var time_from = get_time_as_milisec( $('#pp_price_interval_time_start') );
    if ( time_from == null ) return;
    
    new_price.pp_price_time_from = time_from;
    
    var time_to = get_time_as_milisec( $('#pp_price_interval_time_end') );
    if ( time_to == null ) return;
    
    time_to = time_to - 200; // uvijek oduzimam 200 mili sekundi na end vrijeme ...dakle ako je ponoć pretvorim u 200 ms prije ponoći
    
    new_price.pp_price_time_to = time_to;
    
    var dyn_price_ok = true;
    
    
    // ako je user kliknuo da bude true za dinamična cijena
    if ( $('#dyn_pp_price').hasClass('on') ) {
      
      // provjeri jel max price numerička vrijednost i provjeri jel prazno polje
      if ( !$.isNumeric( $('#max_pp_price_factor').val()  ) || $('#max_pp_price_factor').val() == '' ) {
        $('#max_pp_price_factor').css('border', '1px solid red');
        dyn_price_ok = false;
      };
      
      if ( !$.isNumeric( $('#dyn_zauzetost_granica').val()  ) || $('#dyn_zauzetost_granica').val() == '' ) {
        $('#dyn_zauzetost_granica').css('border', '1px solid red');
        dyn_price_ok = false;
      };
      
      
    } else {
      
      // ako je user  NIJE  kliknuo da bude true za dinamična cijena
      
      var price_ok = false;
      var zauzetost_ok = false;
      
      // provjeri jel max price numerička vrijednost i provjeri jel prazno polje
      if ( $('#max_pp_price_factor').val() !== '' && $.isNumeric( $('#max_pp_price_factor').val() ) ) price_ok = true;
      
      
      // provjeri jel zauzetost numerička vrijednost i provjeri jel prazno polje
      if ( $('#dyn_zauzetost_granica').val() !== '' && $.isNumeric( $('#dyn_zauzetost_granica').val() ) ) zauzetost_ok = true;
      
      // ako je cijena i zauzetost ok klikni na dyn switch da bude true
      if ( price_ok && zauzetost_ok ) {
        // onda uključi dinamične cijene prekidač
        $('#dyn_pp_price').click();
        
      } else {
        
        dyn_price_ok = false;
        
        if ( price_ok && !zauzetost_ok ) $('#dyn_zauzetost_granica').css('border', '1px solid red');
        if ( !price_ok && zauzetost_ok ) $('#max_pp_price_factor').css('border', '1px solid red');
        
      };
      
      
    };
    
    
    if ( $('#dyn_pp_price').hasClass('on') && dyn_price_ok == false ) return;
    
    
    
    var max_pp_price_factor = Number( $('#max_pp_price_factor').val() );
    new_price.max_pp_price_factor = max_pp_price_factor;
    
    
    var dyn_zauzetost_granica = Number( $('#dyn_zauzetost_granica').val() || 100);
    new_price.dyn_zauzetost_granica = dyn_zauzetost_granica;
    
    // ovo je cijeli objekt sa propertijim sifra i naziv
    new_price.pp_price_day = this_module.current_pp_price_day;
        
    // ovo je samo boolean
    new_price.dyn_pp_price = this_module.current_dyn_pp_price;
    
    
    // ako nije ništa ali nije niti broj !!!!!
    if ( $('#pp_basic_price_in').val() !== '' && !$.isNumeric( $('#pp_basic_price_in').val() ) ) {
      $('#pp_basic_price_in').css('border', '1px solid red');
      return;
    };
    var basic_price_in = Number( $('#pp_basic_price_in').val() );
    new_price.pp_basic_price_in = basic_price_in;
    if (basic_price_in == 0)  $('#pp_basic_price_in').val(0);
    

    // ako nije ništa ali nije niti broj !!!!!
    if ( $('#pp_basic_price_out').val() !== '' && !$.isNumeric( $('#pp_basic_price_out').val() ) ) {
      $('#pp_basic_price_out').css('border', '1px solid red');
      return;
    };
    var basic_price_out = Number( $('#pp_basic_price_out').val() );
    new_price.pp_basic_price_out = basic_price_out;
    if (basic_price_out == 0)  $('#pp_basic_price_out').val(0);
    
    if ( basic_price_in == 0 && basic_price_out == 0 ) {
      $('#pp_basic_price_in').css('border', '1px solid red');
      $('#pp_basic_price_out').css('border', '1px solid red');
      return;
    };
    
    if ( !$.isNumeric( $('#pp_revenue').val() ) || $('#pp_revenue').val() == '' ) {
      $('#pp_revenue').css('border', '1px solid red');
      return;
    };
    var pp_revenue_percentage = Number( $('#pp_revenue').val() );
    new_price.pp_revenue = pp_revenue_percentage;
    
    if ( !$.isNumeric( $('#pp_owner_revenue').val() ) || $('#pp_owner_revenue').val() == '' ) {
      $('#pp_owner_revenue').css('border', '1px solid red');
      return;
    }; 
    var pp_owner_revenue_percentage = Number( $('#pp_owner_revenue').val() );
    new_price.pp_owner_revenue = pp_revenue_percentage;
    
    if ( pp_revenue_percentage + pp_owner_revenue_percentage !== 100 ) {
      alert('Zbroj udjela nije 100% !!!!');
      return;
    }; 
    
    
    new_price.id = Date.now();

    
    // UBACIVANJE NEW PRICE U ARRAY U CIT DATA
    
    if ( $.isArray(this_module.cit_data.pp_prices) == false  ) {
      this_module.cit_data.pp_prices = [];
    };

    
    var price_already_exists = false;
    
    $.each(this_module.cit_data.pp_prices, function(pp_prices_index, price) {
      
      if (
        
        new_price.pp_price_day.sifra == price.pp_price_day.sifra &&
        
        new_price.pp_price_time_from == price.pp_price_time_from &&
        new_price.pp_price_time_to == price.pp_price_time_to &&
        new_price.pp_basic_price_in == price.pp_basic_price_in &&
        new_price.pp_basic_price_out == price.pp_basic_price_out &&
        new_price.pp_revenue == price.pp_revenue &&
        new_price.pp_owner_revenue == price.pp_owner_revenue
        
        
      ) {
         price_already_exists = true;
      };
      
      
    });
    
   
    if ( price_already_exists == true ) return;
    
    
    this_module.cit_data.pp_prices.push(new_price);
    
    
    var new_price_row_html = create_price_row_html(new_price);
    $('#pp_price_box').append(new_price_row_html);
    

    setTimeout(function() {
      $('.cit_tooltip').tooltip();
      $(`#delete_pp_price_interval_${new_price.id}`).on( 'click', this_module.delete_price_row )
    }, 100);

    

  };
  this_module.add_pp_price = add_pp_price;
  
  function delete_price_row() {
    
    var price_id = this.id.replace('delete_pp_price_interval_', '');
    
    $('#' + price_id ).remove();
    
    var delete_this_index = null;
    $.each(this_module.cit_data.pp_prices, function(pp_price_index, price) {
      if ( price.id == price_id ) delete_this_index = pp_price_index;
    });
    if ( delete_this_index !== null ) this_module.cit_data.pp_prices.splice(delete_this_index, 1);
  };
  this_module.delete_price_row = delete_price_row;
    
  function generiraj_park_list_za_rezervaciju() {
    
    // resetiram array svih parkinga tako da mogu u listeneru 
    // ispod čekati da se popuni nakon završenog requesta  
    window.all_pp_all_data = null;
    get_park_list('zovem get park places iz func park lista za razervaciju unutar parking places modula');
    
    
    if ( cit_user && cit_user.user_number ) {
      // osvježi trenutno stanje user producta 
      // tako da zapravo osvježim globalni objekt window.all_user_products
      get_user_products_state(cit_user.user_number).then(function(result) {
        console.log('ovo je rezultat svih user products unutar func lista za rezervaciju !!!');
        console.log( result )
      }); // kraj get user products state
    };
    
    // ---------------------------
    wait_for( `typeof window.all_pp_all_data !== 'undefined' && window.all_pp_all_data !== null`, function() {

      var svi_redovi = '';  

      $.each( window.all_pp_all_data, function(pp_index, park) {

        var trenutna_cijena_parkinga = null;

        var park_place_open = 'true';
        var row_style = '';

        var user_status = get_user_status(park);

        if ( park.pp_current_price ) {

          trenutna_cijena_parkinga = park.pp_current_price !== null ? zaokruzi(park.pp_current_price, 1) : '';

        } else {

          // napisi da je parking zatvoren jedino ako nije neki specijalni user kao owner ili free ili super_admin
          if ( user_status == '' || user_status == null ) {
            row_style = `style="background-color: #efefef;"`;
            park_place_open = 'false';
          };

        };

        var park_info_row_html = 
        `
        <div data-pp_sifra="${park.pp_sifra}" class="park_info_row row" id="pp_for_book_${park._id}">

          <div class="col-md-3 col-sm-12 pp_info_row_box">
            <div class="pp_naziv">${park.pp_name}</div>
            <div class="pp_adresa">${park.pp_address}</div>

          </div> 


          <div class="col-md-3 col-sm-12 avail_box">
            <div class="avail_num_and_label">
              <div class="avail_num">${park.pp_available_out_num}</div>
              <div class="avail_label">SLOBODNO</div>
            </div>
            <div class="park_price_box" style="margin-left: 30px;">
              <div class="price_num">${ trenutna_cijena_parkinga || '' }</div>
              <div class="price_valuta">kn/h</div>
            </div>
          </div>  

          <div class="col-md-3 col-sm-12 book_box">
            <div class="minus_book">
              <i class="fas fa-minus-circle cit_tooltip" 
                 data-toggle="tooltip" 
                 data-placement="bottom"
                 title="SMANJI BROJ SATI REZERVACIJE"></i>
            </div>
            <div class="hours_book"><span class="hours_book_num">1</span> h</div>
            <div class="plus_book">
              <i class="fas fa-plus-circle cit_tooltip" 
                 data-toggle="tooltip" 
                 data-placement="bottom"
                 data-html="true"
                 title="POVEĆAJ BROJ SATI REZERVACIJE.<br> Nakon toliko sati rezervacija će automatski prestati!"></i>
            </div>
          </div>  

          <div class="col-md-3 col-sm-12 park_info_btn_box">

            <div class="start_book_btn cit_button cit_center cit_tooltip"
                ${ window.cit_user ? '' : `style="display: none;"` }
                 data-toggle="tooltip" 
                 data-placement="bottom"
                 title="REZERVIRACIJA MJESTA NA BROJ SATI KOJE STE ODREDILI">
              REZERVIRAJ
            </div>

            <div class="stop_book_btn cit_button cit_center cit_tooltip" style="display: none; background-color: #e84030;"
                 data-toggle="tooltip" 
                 data-placement="bottom"
                 title="PREKINI REZERVACIJU">
              PREKINI REZERVACIJU
            </div>

          </div>

        </div>    
        `;

      svi_redovi += park_info_row_html;   
      }); // kraj loop pp list

      $('#park_info_lista').html(svi_redovi);
      
      this_module.set_listeners_za_veliku_pp_listu();

    }, 500000); // kraj kada globalna lista parkinga popuni


    
  };
  this_module.generiraj_park_list_za_rezervaciju = generiraj_park_list_za_rezervaciju;
  
  
  function set_listeners_za_veliku_pp_listu() {

    // pričekaj da se renderira lista od pp
    wait_for(`$('.park_info_row').length > 0`, function() { 

      $('.cit_tooltip').tooltip();

      var all_pp_inputs = $('#parking_places_form input');
      // -------- KADA USER KLIKNE NA BILO KOJE OD INPUT POLJA !!!!
      all_pp_inputs.off('click');
      all_pp_inputs.on('click', this_module.set_mapu_u_edit_mode );

      var pp_list_row = $('.park_info_row');
      pp_list_row.off('click');
      pp_list_row.on('click', this_module.open_pp_klikom_na_veliku_pp_listu);

      var start_book_btn = $('.start_book_btn');
      start_book_btn.off('click', this_module.book_park_place );
      start_book_btn.on('click', this_module.book_park_place );

      var stop_book_btn = $('.stop_book_btn');
      stop_book_btn.off('click', this_module.STOP_booking );
      stop_book_btn.on('click', this_module.STOP_booking );

      $('.minus_book').off('click');
      $('.minus_book').on('click', this_module.set_booking_duration(-1) );

      $('.plus_book').off('click');
      $('.plus_book').on('click', this_module.set_booking_duration(1) );

      // sada kada je lista parkinga izgenerirana onda mogu napraviti loop
      // i staviti VIP oznaku na redove  u kojima je user VIP
      $.each( window.all_pp_all_data, function(pp_index, park) {
        get_user_status(park);
      });

      if ( cit_user !== null ) {
        this_module.get_active_booking_or_parking({ pp_sifra: 'all' }, cit_user.user_number);
      } else {

      };

    }, 50000); // kraj listenera kada se pojave redovi u listi parkinga


  };
  this_module.set_listeners_za_veliku_pp_listu = set_listeners_za_veliku_pp_listu;
  
  
  function set_mapu_u_edit_mode() {

    if (!window.we_set_map_crosshair) {

      // pretvori curcos na karti u križ za označavanje koordinata !!!
      window.we_set_map_crosshair = true;

      window.pp_edit_mode = true;

      window.we_site_map.setOptions({
        draggableCursor: 'crosshair'
      });

      // removaj prethodni listener ako postoji 
      google.maps.event.clearListeners(window.we_site_map, 'click');

      // dodaj listener za klik na mapu
      window.we_site_map.addListener('click', function (event) {

        $('#pp_latitude').val(event.latLng.lat());
        $('#pp_longitude').val(event.latLng.lng());

        // obriši sve pinove na karti
        this_module.clear_all_pp_markers();

        // i zatim prikaži pin na mjestu koje je user sada kliknuo !!!
        var pp_Latlng = {
          lat: event.latLng.lat(),
          lng: event.latLng.lng()
        };
        var marker = new google.maps.Marker({
          position: pp_Latlng,
          icon: '/img/google_we_icon.png'
        });

        window.all_we_map_markers.push(marker);
        window.all_we_map_markers[0].setMap(window.we_site_map);

      });

    };

  };
  this_module.set_mapu_u_edit_mode = set_mapu_u_edit_mode;
  
  
  function open_pp_klikom_na_veliku_pp_listu() {

    var vec_je_park_ili_book = false;  

    if (
      $(this).find('.parking_indicator').length > 0
      ||
      $(this).find('.book_indicator').length > 0
    ) {
      vec_je_park_ili_book = true;
    };

    if (
      vec_je_park_ili_book == false &&
      $(this).attr('data-park_place_open') == 'false' 
    ) {
      var park_id = this.id.replace('pp_for_book_', '');
      var clicked_pp_data = find_park(park_id);
      popup_closest_open_time(clicked_pp_data);
      return;
    };

    // ako je ovaj red već aktivan onda stop !!!
    if ( $(this).hasClass('cit_active') ) return;

    var pp_id = this.id.replace('pp_for_book_', '');

    // funkcija za ovaranje park place se zapravo nalazi u pp list modulu 
    // TODO ? - možda je bolje da je prebacim u ovaj glavni parking places modul ????
    var pp_list_module = window['/right_sidebar/cit_search_list/park_places/cit_pp_list_module.js'];
    if ( cit_user ) pp_list_module.open_park_place(pp_id);

    // resetiraj sve ne aktivne redove i jedan koji je aktivan ( ako je user kliknuo već na neki red )
    $('.park_info_row').removeClass('cit_dimmed cit_active');

    // loop svaki red
    $('.park_info_row').each(function() {

      // dimmaj sve redove koji nisu ovaj koji sam kliknuo
      if ( this.id !== 'pp_for_book_'+pp_id ) {
        // nije active
        $(this).addClass('cit_dimmed');
      } else {
        // ACTIVE red
        $(this).addClass('cit_active');
      };

    });
  }
  this_module.open_pp_klikom_na_veliku_pp_listu = open_pp_klikom_na_veliku_pp_listu;
  
  
  
  function set_count_number(count_element, diff) {

    var current_text = count_element.text();
    var special_tag = count_element.find('.special_tag');
    var vip_user_tag = count_element.find('.vip_user_tag');
    
    count_element.addClass('big');

    setTimeout( function() {

      if ( special_tag.length > 0 ) {

        var special_tag_text = special_tag.text();
        var current_num = Number( current_text.replace(special_tag_text, '').trim() );
        var next_num = current_num + diff;

        var new_html = 
        `
        ${next_num}
        <div class="special_tag">${special_tag_text}</div>
        `;    

        count_element.html(new_html);
        count_element.removeClass('big');        


      } else if ( vip_user_tag.length > 0 ) {


        var vip_tag_text = 'VIP';
        var current_num = Number( current_text.replace(vip_tag_text, '').trim() );
        var next_num = current_num + diff;

        var new_html = 
        `
        ${next_num}
        <div class="vip_user_tag">${vip_tag_text}</div>
        `;    

        count_element.html(new_html);
        count_element.removeClass('big');        
      } 
      else {

        var current_num = Number( current_text.trim() );
        var next_num = current_num + diff;
        count_element.text(next_num);
        count_element.removeClass('big');  

      }
    }, 400);

  };
  this_module.set_count_number = set_count_number;
  
  
  function book_park_place() {
    
    
    console.log(' ------------------- kliknuo na book park place button !!!! ')
    
    
    var this_book_btn = this;
    
    $(this_book_btn).addClass('cit_disable');
    
    var park_row = $(this_book_btn).closest('.park_info_row');
    var park_id = park_row[0].id.replace('pp_for_book_', '');
    var stop_book_button = park_row.find('.stop_book_btn');
    var count_circle = park_row.find('.avail_num');
    
    get_park_list('get park list kad sam kliknuo na book park place button !!!!').then(
    function() { 
  
      if ( !cit_user || !cit_user.user_number ) {
        popup_nedostaje_user();
        
        $(this_book_btn).removeClass('cit_disable');
        stop_book_button.css('display', 'none');
        
        return;
      };

      var park_data = find_park(park_id);

      var time_stamp = Date.now();

      var book_duration_number = Number( park_row.find('.hours_book_num').text().trim() );

      var current_tag = write_special_tag_avail_count( [park_data] );

      if ( current_tag !== null && current_tag.indexOf('zelis_biti_obican_user_umjesto') > - 1  ) {
        var only_tag = current_tag.split('-')[1];
        popup_become_regular_user(park_data, only_tag, park_row.find('.start_book_btn')[0] );
        return;
      };

      var action_object = { 

        //* postojeci fieldovi  /  
        sifra : null,
        from_time: null,
        to_time: null,
        paid: null,  
        pp_sifra : park_data.pp_sifra,
        user_number: cit_user.user_number,
        was_reserved_id: null,
        mobile_from_time: null,

        //* moji fieldovi  /  
        status: 'booked',
        user_set_book_duration: book_duration_number,

        reservation_from_time: time_stamp,
        reservation_to_time: null,
        reservation_expired: null,
        reservation_canceled: null,
        reservation_paid: null,

        current_price: park_data.pp_current_price,

        pack_type: null,
        duration: null,
        revenue: null,
        owner_revenue: null,
        currency: 'kn',

        indoor: false,

        current_tag: current_tag,

        user_status: get_user_status(park_data)

      }; 

      console.log('prije ajaxa book park place');  
      console.log(this_module.cit_data);

      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "POST",
        url: "/new_booking",
        data: JSON.stringify(action_object),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        })
      .done(function(response) {

        console.log(response);

        if ( response.success == true ) {
          
          $(this_book_btn).removeClass('cit_disable');
          $(this_book_btn).css('display', 'none');
          stop_book_button.css('display', 'flex');
          
          set_count_number(count_circle, -1);
          
          
          get_active_booking_or_parking({ pp_sifra: 'all' }, cit_user.user_number);
          

        } 
        else {
          
          $(this_book_btn).removeClass('cit_disable');
          stop_book_button.css('display', 'none');

          // unutar msg properija sam stavio ime funkcije koja poziva popup
          // pošto sam sve popup funkcije stavio u globalni window space onda ih zovam kao window propertije

          // ako postoji popup naslov u response msg onda ga pokreni !!
          if ( window[response.msg] ) {
            window[response.msg](response.park);
          } else {
            // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
            popup_greska_prilikom_bookinga();
          };
          
        };
      })
      .fail(function(error) {
        
        
        $(this_book_btn).removeClass('cit_disable');
        stop_book_button.css('display', 'none');

        console.log(error);
        popup_greska_prilikom_bookinga();
        
      });    

    
  }) // kraj od then nakon get park list

    
  };
  this_module.book_park_place = book_park_place;
  // kraj book park place
  
  
  
  function STOP_booking() {


        
    var this_stop_book_btn = this;
    
    $(this_stop_book_btn).addClass('cit_disable');
    
    var park_row = $(this_stop_book_btn).closest('.park_info_row');
    var park_id = park_row[0].id.replace('pp_for_book_', '');
    var start_book_button = park_row.find('.start_book_btn');
    var count_circle = park_row.find('.avail_num');
    var park_data = find_park(park_id);
    
    if ( !cit_user || !cit_user.user_number ) {
      
      popup_nedostaje_user();
      
      $(this_stop_book_btn).removeClass('cit_disable');
      start_book_button.css('display', 'none');
      
      return;
    };


    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: ( "/cancel_booking/" + park_data.pp_sifra + '/' + cit_user.user_number ),
      dataType: "json",
      })
    .done(function(response) {

      console.log(response);

      if ( response.success == true ) {
        
        
        $(this_stop_book_btn).removeClass('cit_disable');
        $(this_stop_book_btn).css('display', 'none');
        start_book_button.css('display', 'flex');
        
        
 
        set_count_number(count_circle, 1);
        
           
        // pokaži koliko je love user potrošio na booking
        var time_string = get_time_as_hh_mm_ss(response.action.book_duration);
        popup_show_paid_time(
          time_string, 
          zaokruziIformatirajIznos( response.action.reservation_paid), 
          response.action.currency 
        );
        
        
        get_active_booking_or_parking({ pp_sifra: 'all' }, cit_user.user_number);
        
        
      } 
      else {
        
        
        $(this_stop_book_btn).removeClass('cit_disable');
        start_book_button.css('display', 'none');
        
        
        // ako postoji popup naslov u response msg onda ga pokreni !!
        if ( window[response.msg] ) {
          window[response.msg]();
        } else {
          // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
          popup_greska_prilikom_CANCEL_bookinga();
        };
        
      };
    })
    .fail(function(error) {
      
      $(this_stop_book_btn).removeClass('cit_disable');
      start_book_button.css('display', 'none');
      
      console.log(error);
      // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
      popup_greska_prilikom_CANCEL_bookinga();
      
    });
  
  };
  this_module.STOP_booking = STOP_booking;
    
  
  function set_booking_duration(book_diff) {
    
    return function(e) {
      
      var park_row = $(e.target).closest('.park_info_row');
      var park_id = park_row[0].id.replace('pp_for_book_', '');
      var current_book_num = Number( park_row.find('.hours_book_num').text().trim() );

      var new_book_num = current_book_num;

      if ( book_diff < 0) {
        // ako je book dif zapravo minus 1
        // moram paziti da je curr broj veći ili jedan 2
        if ( current_book_num >= 2 ) new_book_num = current_book_num + book_diff;

      } 
      else {

        // ako je diff plus 1
        var this_park = find_park(park_id);
        // postavi neki veliki broj da bude maximalan broj sati za rezervaciju
        var max_reservation_sati = 999;
        // provjeri jel postoji user porducts i jel ima i jednu pretplatu 
        if ( window.all_user_products && window.all_user_products.monthly_arr.length > 0 ) {

          $.each(window.all_user_products.monthly_arr, function(pretplata_index, pretplata ) {
            // ako pronađe da ima pretplatu za park u kojem se trenutno nalazi
            if ( Number(pretplata.pp_sifra) == Number(this_park.pp_sifra) ) {
              // onda postavi da maximum sati za booking bude 3
              max_reservation_sati = 3;
            };
          });
        };
        // ako je current broj sati + dodatak manji ili jednak maximumum onda dodaj
        if ( current_book_num + book_diff <= max_reservation_sati ) new_book_num = current_book_num + book_diff;
      };
      park_row.find('.hours_book_num').text(new_book_num);

    };
    
  };
  this_module.set_booking_duration = set_booking_duration;
  
  
  function get_user_status(park) {

    var user_status = '';

      if ( 
        cit_user                            && /* ako postoji user*/
        cit_user.roles                      && /* ako postoji roles u useru */
        cit_user.roles.pp_vip               && /* ako postoji prop pp vip */ 
        $.isArray( cit_user.roles.pp_vip )  && /* ako je pp vip array */
        cit_user.roles.pp_vip.length > 0 /* ako array nije prazan */
      ) {

        if ( $.inArray( park.pp_sifra, cit_user.roles.pp_vip) > -1 ) {

          user_status = 'vip';

          var this_park_row_count_circle = $('#pp_for_book_' + park._id + ' .avail_num');
          var already_has_vip = this_park_row_count_circle.find('.vip_user_tag').length;

          if ( already_has_vip == 0 ) {
            var vip_tag = `<div class="vip_user_tag">VIP</div>`;
            this_park_row_count_circle.prepend(vip_tag);
          };

        };

      };

      if ( 
        cit_user                            &&
        cit_user.roles                      &&
        cit_user.roles.free_pp              &&
        $.isArray( cit_user.roles.free_pp ) &&
        cit_user.roles.free_pp.length > 0
      ) {

        if ( $.inArray( park.pp_sifra, cit_user.roles.free_pp) > -1 ) user_status = 'free';

      };  



      if ( 
        cit_user                   &&
        cit_user.roles             &&
        cit_user.roles.super_admin
      ) {

        if ( cit_user.roles.super_admin == true ) user_status = 'super';

      };  


    return user_status;

  };
  
  function get_active_booking_or_parking( park, user_num ) {
  
  if ( !user_num ) return;
  
  $.ajax({
      type: 'GET',
      url: ('/get_active_booking_or_parking/' + park.pp_sifra + '/' + user_num),
      dataType: "json"
  })
  .done(function(response) {
    
      console.log(response);
  
      if ( response.success == true ) {

        
        setup_list_GUI(response);


      } else {
        
        
        if ( window[response.msg] ) { window[response.msg](); }
        else {
          // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
          popup_greska_prilikom_parkinga();
        };

      };
  })
  .fail(function(error) {
    console.error("Error on get book time passed !!!");
    
    if ( window[error.msg] ) {
      window[error.msg]();
    } else {
      // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
      popup_greska_prilikom_parkinga();
    };
  });

};
  this_module.get_active_booking_or_parking = get_active_booking_or_parking;
  
  
  function setup_list_GUI(response) {

    var we_booking_in_progress = [];
    var we_park_in_progress = [];

    if ( !window.all_pp_all_data ) return;

    $.each( window.all_pp_all_data, function( park_index, park ) {

      $.each( response.booking, function( book_index, booking_obj ) {
        if ( booking_obj.pp_sifra == park.pp_sifra ) we_booking_in_progress.push(park.pp_sifra);
      });

      $.each( response.parking, function( book_index, parking_obj ) {
        if ( parking_obj.pp_sifra == park.pp_sifra ) we_park_in_progress.push(park.pp_sifra);
      });

    }); 
    

    $.each( window.all_pp_all_data, function( park_index, park ) {
      setup_pp_state_indicator(we_booking_in_progress, we_park_in_progress, park);
    }); 


    write_special_tag_avail_count();


    $('.avail_num').each( function() {
      var clone = $(this).clone();
      clone.find('div').remove();
      var count = Number( clone.text().replace(/\r?\n|\r/g, '').replace(/\s/g,'' ) );
      console.log(count);

      if ( count == 0 ) {
        $(this).css('background-color', '#e8412f');
      } else {
        $(this).css('background-color', '#034072');
      };

    });

  };

  
  function setup_pp_state_indicator(we_booking_in_progress, we_park_in_progress, park) {

    // obriši sve indikatore da bi ispod mogao napraviti nove u tom redu !!!! 
    // na taj način osvježavam cijelu listu s promjenjenim indikatorima !!!
    $(`#pp_for_book_${park._id} .book_indicator`).remove();
    $(`#pp_for_book_${park._id} .parking_indicator`).remove();
    $(`#pp_for_book_${park._id} .closed_indicator`).remove();

    // resetiraj sve tipke za rezervaciju na listi
    $(`#pp_for_book_${park._id} .start_book_btn`).css('display', 'flex');
    $(`#pp_for_book_${park._id} .stop_book_btn`).css('display', 'none');
    
    
    var pp_state_indicator = '';

    /* ---------------START---------------- OVO JE DIO ZA BOOK INDIKATORE  ------------------------------- */ 
    var reservation_indicator = '';
    if ( $.inArray( park.pp_sifra, we_booking_in_progress) > -1 ) {
      reservation_indicator = `<div class="we_indicator book_indicator">RESERVED <i class="fas fa-hourglass-half"></i></div>`;
    };
    /* ----------------END--------------- OVO JE DIO ZA BOOK INDIKATORE  ------------------------------- */ 

    /* ---------------START---------------- OVO JE DIO ZA PARK IN PROGRESS INDIKATORE  ------------------------------- */ 
    var parking_indicator = '';
    if ( $.inArray( park.pp_sifra, we_park_in_progress) > -1 ) {
      parking_indicator = `<div class="we_indicator parking_indicator">PARKED <i class="fas fa-parking"></i></div>`;
    };
    /* ----------------END--------------- OVO JE DIO ZA PARK IN PROGRESS INDIKATORE  ------------------------------- */ 

    /* ---------------START---------------- OVO JE DIO ZA PARK CLOSED INDIKATORE  ------------------------------- */ 
    var closed_indicator = '';

    var user_status = get_user_status(park);

    if ( park.pp_current_price == null && user_status !== 'owner' ) {
      closed_indicator = `<div class="we_indicator closed_indicator">CLOSED <i class="fas fa-ban"></i></div>`;
    };

    /* ----------------END--------------- OVO JE DIO ZA PARK CLOSED INDIKATORE  ------------------------------- */ 


    if ( !closed_indicator ) {
      
      // ako nije closed
      if ( parking_indicator ) {
        
        // ako je parkiran na tome parkiralištu
        pp_state_indicator = parking_indicator;
        
      } else {
        
        // ako nije parkiran ----> provjeri jel rezerviran parking
        if ( reservation_indicator ) pp_state_indicator = reservation_indicator;
      };

    } else {
      // ako je closed
      pp_state_indicator = closed_indicator;
      
    };  

    var indicator_postavljen = false;

    if ( parking_indicator ) {
      pp_state_indicator = parking_indicator;
      indicator_postavljen = true;
    };


    if ( !indicator_postavljen && closed_indicator ) {
      pp_state_indicator = closed_indicator;
    };

    if ( reservation_indicator ) {
      pp_state_indicator = reservation_indicator;
    };

    
    var jel_vec_ima_book_indicator = $(`#pp_for_book_${park._id}`).find('.book_indicator').length;
    var jel_vec_ima_park_indicator = $(`#pp_for_book_${park._id}`).find('.parking_indicator').length;
    var jel_vec_ima_closed_indicator = $(`#pp_for_book_${park._id}`).find('.closed_indicator').length;

    // ubaci book indicator samo ako već nije ubačen u html
    if ( !jel_vec_ima_book_indicator && pp_state_indicator == reservation_indicator && reservation_indicator !== '' ) {
      $(`#pp_for_book_${park._id} .park_price_box`).after(pp_state_indicator);
      $(`#pp_for_book_${park._id} .start_book_btn`).css('display', 'none');
      $(`#pp_for_book_${park._id} .stop_book_btn`).css('display', 'flex');
    };

    // ubaci PARK indicator samo ako već nije ubačen u html
    if ( !jel_vec_ima_park_indicator && pp_state_indicator == parking_indicator && parking_indicator !== '' ) {
      $(`#pp_for_book_${park._id} .park_price_box`).after(pp_state_indicator);
      $(`#pp_for_book_${park._id} .start_book_btn`).css('display', 'none');
      $(`#pp_for_book_${park._id} .stop_book_btn`).css('display', 'none');
    };

    // ubaci CLOSED indicator samo ako već nije ubačen u html
    if ( !jel_vec_ima_closed_indicator && pp_state_indicator == closed_indicator && closed_indicator !== '' ) {
      $(`#pp_for_book_${park._id} .park_price_box`).after(pp_state_indicator);
      $(`#pp_for_book_${park._id} .start_book_btn`).css('display', 'none');
      $(`#pp_for_book_${park._id} .stop_book_btn`).css('display', 'none');
    }; 
    
    
    $(`#pp_for_book_${park._id} .not_published`).remove();
    
    if ( park.published == false ) {
      
      // sakrij tipke za rezervaciju
      $(`#pp_for_book_${park._id} .start_book_btn`).css('display', 'none');
      $(`#pp_for_book_${park._id} .stop_book_btn`).css('display', 'none');
      
      // ubaci tag da parking nije aktivan
      $(`#pp_for_book_${park._id} .park_info_btn_box`)
        .prepend(`<div class="we_indicator not_published">ČEKA ODOBRENJE <i class="fas fa-eye-slash"></i></div>`)
       
    };

  };

  
  function get_obican_user_parking(park) {


    var zeli_biti_obican_user = false;

    var obican_user_parking_array = window.localStorage.getItem('obican_user_parking_array');

    if ( obican_user_parking_array !== null ) {
      obican_user_parking_array = JSON.parse(obican_user_parking_array);

      $.each(obican_user_parking_array, function(index, pp_sifra) {
        if ( pp_sifra == park.pp_sifra ) zeli_biti_obican_user = true;
      });

    };

    return zeli_biti_obican_user;

  };
  this_module.get_obican_user_parking = get_obican_user_parking;


  function set_obican_user_parking(park, state) {


    var obican_user_parking_array = window.localStorage.getItem('obican_user_parking_array');
    if ( obican_user_parking_array !== null ) {
      obican_user_parking_array = JSON.parse(obican_user_parking_array);
    } else {
      obican_user_parking_array = [];
    };


    // AKO VEĆ POSTOJI OVA PP SIFRA ONDA JU OBRIŠI --------------------------
    // to radim uvijek bez obzira jel argumnet state true ili false !!!!! --------------------------

    var obrisi_ovaj_index = null;
    $.each(obican_user_parking_array, function(index, pp_sifra) {
      if ( pp_sifra == park.pp_sifra ) obrisi_ovaj_index = index;
    });
    if ( obrisi_ovaj_index !== null ) obican_user_parking_array.splice(obrisi_ovaj_index, 1);


    // samo ako je state true onda dodaj sifru od parkinga
    if ( state == true ) obican_user_parking_array.push(park.pp_sifra);

    // spremi nove izmjene nazad u local storage
    window.localStorage.setItem('obican_user_parking_array', JSON.stringify(obican_user_parking_array) );


  };
  this_module.set_obican_user_parking = set_obican_user_parking;
  
  
  function write_special_tag_avail_count(park_place_arr) {

    var is_specific_park = false;
    var found_customer_tag = null; 

    if ( !park_place_arr ) {
      // ako NISAM dao array kao argument onda je array svi parkinzi
      park_place_arr = window.all_pp_all_data;
    } else {

      // ako sam explicitno dao array
      // onda prebacom varijablu samo da znam ispod dolje da je ovo za spcifični parking
      // INAČE AKO DAM ARRAY UVIJEK ĆU DATI SAMO JEDAN PARK U ARRAYU i to u slučaju kada otvaram park page !!!!!!!!!!!!!!!!!!!!!!!!!!!
      // u arrayu je samo zato da ne mijenjam kod ispod 

      is_specific_park = true;
    };


    $.each( park_place_arr, function( park_index, park ) {  

      var vec_parkiran = $('#pp_for_book_' + park._id).find('.parking_indicator').length;
      var vec_bookiran = $('#pp_for_book_' + park._id).find('.book_indicator').length;
      var is_closed = $('#pp_for_book_' + park._id).find('.closed_indicator').length;

      var avail_circle_elem = $('#pp_for_book_' + park._id).find('.avail_num'); 



      if ( 
        window.cit_user                             &&  
        window.cit_user.customer_tags               &&
        $.isArray(window.cit_user.customer_tags)    &&
        window.cit_user.customer_tags.length > 0    &&
        park.special_places                         &&
        Object.keys(park.special_places).length > 0 
      ) {

        // resetiraj tag match za svaki novi red parkinga u listi
        var tag_match = null;

        // loop po svim special places ( KOJI JE OBJEKT ) unutar jednog parka
        $.each( park.special_places, function( place_tag, spec_place_obj ) {

          // loop da provjerim svaki specijal place sa svakim customer tagom ( KOJI JE ARRAY )
          $.each( cit_user.customer_tags, function( tag_index, tag_obj ) {

            // ako do sada nisam našao match onda može


            if ( tag_match == null && place_tag == tag_obj.sifra ) {

              // ako jesam našao match onda ne traži dalje !!!!!!
              tag_match = place_tag;



              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 

              // spec_place_obj.avail_out = 0;

              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
              // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 


              var avail_count = 
                `
                ${spec_place_obj.avail_out}
                <div class="special_tag">${place_tag}</div>
                `;

              if (is_specific_park) {
                // ako sam tražio podatak za specifični park place onda samo returnaj customer tag
                // koji se poklapa sa special place od parkinga
                found_customer_tag = tag_obj.sifra;


                // ako je npr tele2 parking avail = nula
                // i ako ima mjesta na običnim parkinzima
                if (
                  spec_place_obj.avail_out == 0 &&
                  park.pp_available_out_num > 0
                ) {

                  // onda postavi ovaj customer tag ako user nije izabrao da bude običan user
                  if ( 
                    
                    get_obican_user_parking(park) == false &&

                    !vec_bookiran && 
                    !vec_parkiran && 
                    !is_closed
                    
                     ) {
                    found_customer_tag = 'zelis_biti_obican_user_umjesto-' + place_tag;
                  };

                  // ako je je user izabrao da bude običan user
                  if ( get_obican_user_parking(park) == true ) found_customer_tag = null;

                } 
                else if ( spec_place_obj.avail_out > 0 ) {

                  // ako ima tele2 mjesta onda vrati nazad stanje usera da bude specijalni tele2
                  // to radim tako da jednostavno obrišem iz local storage park na kojem je tražio da bude običan user

                  set_obican_user_parking( park, false );

                };

              } 
              else {

                // ako ovo nije sa jedan specifični parking onda to znači da je ova funkcija pozvana za sve parkinge u listi
                // tada to znači da trebam updateati html  za available count u svakom redu tj za svaki parking
                $(`#pp_for_book_${park._id} .avail_num`).html(avail_count);


                // ako je npr tele2 parking avail = nula
                // i ako ima mjesta na običnim parkinzima
                if (
                  spec_place_obj.avail_out == 0 &&
                  park.pp_available_out_num > 0
                ) {

                if ( 
                    get_obican_user_parking(park) == false &&

                    !vec_bookiran && 
                    !vec_parkiran && 
                    !is_closed

                  ) {
                    popup_become_regular_user(park, tag_obj.sifra); 
                  };

                  if ( get_obican_user_parking(park) == true ) {
                    // obriši crvenu boju ako je do sada bila druga boja u slučaju nula slobodnih mjesta
                    $(`#pp_for_book_${park._id} .avail_num`).css('background-color', '');
                    // ubaci u krug sa brojem slobodnih mjesta broj OBIČNIH MJESTA
                    $(`#pp_for_book_${park._id} .avail_num`).html(park.pp_available_out_num);
                  };

                } 
                else if ( spec_place_obj.avail_out > 0 ) {
                  // ako ima mjesta od tele2
                  // obriši ovaj parking iz liste u kojoj user želi biti običan
                  set_obican_user_parking( park, false );

                };

              }; // kraj jel jedan specifični park ili je cijela lista

            }; // kraj jel nasao match u tagovima od usera i u special places u parkovima
          }); // loop po customer tags
        }); // loop po park special places

      }; // provjera jel user ima tags i jel park ima special places

    }); // loop po svim parkinzima

    return found_customer_tag;


  };
  this_module.write_special_tag_avail_count = write_special_tag_avail_count;
  

  this_module.cit_loaded = true;
 
}
  
  
};