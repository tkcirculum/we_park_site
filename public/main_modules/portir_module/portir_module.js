var module_object = {
  create: function( data, parent_element, placement ) {
    

    var this_module = window[module_url]; 

    var component_id = null;

    var portir_for_pps = [];
    we_local_arr.portir_for_pps = portir_for_pps;
    
    window.all_pp_all_data = null;
    
    get_park_list('zovem get park list unutar create portir modul').then(
    function() {

      if ( 
        window.all_pp_all_data             && /* ako postoji lista parkinga */
        $.isArray(window.all_pp_all_data)  && /* ako je lista array */
        window.all_pp_all_data.length > 0  && /* ako array nije pazan */
        cit_user                           && /* ako postoji user */
        cit_user.roles                     && /* ako postoji roles u useru */
        cit_user.roles.portir              && /* ako postoji prop portir */ 
        $.isArray( cit_user.roles.portir ) && /* ako je portir array */
        cit_user.roles.portir.length > 0 /* ako array nije prazan */
      ) {



        $.each(window.all_pp_all_data, function(pp_ind, park) {
          // ovdje stavljam u listu sve parkinge kojima je ovaj user portir
          // ( naime isti user može biti portir na više parkirališta )
          if ( $.inArray( park.pp_sifra, cit_user.roles.portir ) > -1 ) {
            portir_for_pps.push({ sifra: park.pp_sifra, naziv: park.pp_name });
          };

        });

        we_local_arr.portir_for_pps = portir_for_pps;

        // ako portir ima samo jedan parking onda ga izaberi
        if ( we_local_arr.portir_for_pps.length == 1 ) {
          
          this_module.choose_portir_pp(portir_for_pps[0], 'portir_pp_list');
          
        };
        
        
      };

    },
    function(error) {
      console.error('Greška prilikom get park list unutar create portir modul');
      console.error(error);
    });  
    
    
    
    var component_html =
    `
<div id="portir_component" class="main_module_component container-fluid" style="float: left; height: 100%;">
  
  <div class="row" style="margin-top: 20px;">
    <div class="col-md-12 col-sm-12">
      <h6 id="portir_big_card_label" style="color: #a2a2a2; text-align: center; margin-bottom: 30px;">Kontrola rampe</h6>
    </div>
  </div> 

  
  <div class="row" style="display: flex; justify-content: center;">

    <div class="col-md-3 col-sm-12">
      <div id="portir_pp_list_label" class="cit_label">PARKIRALIŠTE</div>
      <input id="portir_pp_list" type="text" class="cit_input we_auto simple" autocomplete="off"> 
    </div>

    <div class="col-md-3 col-sm-12">
      <div id="portir_reg_input_label" class="cit_label">REGISTRACIJA</div>
      <input id="portir_reg_input" autocomplete="off" type="text" class="cit_input we_auto simple" style="text-transform: uppercase;" >
    </div>
    
    <div class="col-md-3 col-sm-12">
      <div id="portir_special_places_label" class="cit_label">GRUPA KORISNIKA</div>
      <input id="portir_special_places" type="text" class="cit_input we_auto simple" autocomplete="off"> 
    </div>
    
  </div>
  
  <div class="row" style="margin-top: 20px;">
    <div class="col-md-12 col-sm-12">
      <h6 id="portir_avail_count_title" style="color: #5e5f60; text-align: center;">Broj slobodnih mjesta:</h6>
    </div>
  </div> 
  
  <div class="row" style="margin: 0;">
    <div class="col-md-12 col-sm-12">
      <h6 id="portir_avail_count_number" style="font-size: 60px; color: #238eff; text-align: center; font-weight: 700;">--</h6>
    </div>
  </div> 
  
  
  <div class="row" style="display: flex; justify-content: space-around;">
  
    <div class="veliki_portir_gumbi" id="portir_enter_pp">
      <i style="font-style: normal; color: #9e0946; text-shadow: 0 0 11px #ffffff;">ULAZAK</i>
      <i class="fas fa-cog fa-spin" style="display: none;"></i>
    </div>

    <div class="veliki_portir_gumbi" id="portir_exit_pp">
      <i style="font-style: normal; color: #00755b; text-shadow: 0 0 11px #ffffff;">IZLAZAK</i>
      <i class="fas fa-cog fa-spin" style="display: none;"></i>
    </div>
    
    <div class="veliki_portir_gumbi" id="portir_fixed_open_ramp">
      <i style="font-style: normal;">TRAJNO OTVORI</i>
      <i class="fas fa-cog fa-spin" style="display: none;"></i>
    </div>
    
    <div class="veliki_portir_gumbi" id="portir_fixed_close_ramp">
      <i style="font-style: normal;">ZAKLJUČAJ</i>
      <i class="fas fa-cog fa-spin" style="display: none;"></i>
    </div>
    
  </div>
  
  
</div>
    `;
      
      
  var this_module = window[module_url]; 
    
  // ovo je arbitrarni uvijet  - uzeo sam prvi label u formi kao uvijet kad se pojavi ....
    
  wait_for( `$('#portir_big_card_label').length > 0 && window.all_pp_all_data !== null`, function() {

    
    window.get_current_reg_text = function() {
      return $('#portir_reg_input').val().replace(/(?!ć|Ć|č|Č|š|Š|đ|Đ|ž|Ž)\W/g, '').replace(/\_/g, '').toUpperCase();
    };
    
    $('#portir_reg_input').data('we_auto', {
      desc: 'pretraga registracijskih oznaka za pronalazak usera u modulu portir',
      local: false,
      url: '/search_regs',
      find_in: null,
      col_widths: [0, 15, 20, 20, 15, 15, 15, 0],
      return: { _id: 1, user_number: 1, email: 1, name: 1, user_tel: 1, full_name: 1, business_name: 1, 'customer_tags': 1 },
      hide_cols: ['_id', 'customer_tags'],
      query: {"car_plates.reg": 'window.get_current_reg_text() ....OVO JE SAMO KAO PODSJETNIK; ALI VAL OD INPUTA DOBIJEM unutar funkcije search database' }, /* inače nije mi se dalo naći način kako da injektiram u ovaj property trenutni text u input polju pa to napravim na serveru jer ionako search database remote pošalje trazi_string   */
      show_on_click: false,
      list_width: 1000,
    });
    
    

    $('#portir_enter_pp').off('click');
    $('#portir_enter_pp').on('click', function() {
      this_module.portir_open_ramp('enter');
    });
    
    $('#portir_exit_pp').off('click');
    $('#portir_exit_pp').on('click', function() {
      this_module.portir_open_ramp('exit');
    });
        
    $('#portir_fixed_open_ramp').off('click', this_module.portir_fixed_open_ramp);
    $('#portir_fixed_open_ramp').on('click', this_module.portir_fixed_open_ramp);
    
    $('#portir_fixed_close_ramp').off('click', this_module.portir_fixed_close_ramp);
    $('#portir_fixed_close_ramp').on('click', this_module.portir_fixed_close_ramp);
    
    
    
    $('#portir_pp_list').data('we_auto', {
      desc: 'za odabir parkinga u modulu portir',
      local: true,
      return: {},
      show_on_click: true,
      list: 'portir_for_pps',
    });
    
    
  }, 500*1000 );      

    if ( parent_element ) {
      
        cit_place_component(parent_element, component_html, placement);
        
        window.cit_main_area_perfectbar.update();

    }; 
    
    return {
      html: component_html,
      id: component_id
    };

  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 

  
  // ----------------- VAŽNO TODO ----> KASNIJE STAVI NEGDJE U BAZU !!!!!!!!!!!!
  // ----------------- VAŽNO TODO ----> KASNIJE STAVI NEGDJE U BAZU !!!!!!!!!!!!
  // ----------------- VAŽNO TODO ----> KASNIJE STAVI NEGDJE U BAZU !!!!!!!!!!!!
  this_module.def_grupe = {
    "14": "STED",
    "15": "TZM",
  };
  // ----------------- VAŽNO TODO ----> KASNIJE STAVI NEGDJE U BAZU !!!!!!!!!!!!
  // ----------------- VAŽNO TODO ----> KASNIJE STAVI NEGDJE U BAZU !!!!!!!!!!!!
  // ----------------- VAŽNO TODO ----> KASNIJE STAVI NEGDJE U BAZU !!!!!!!!!!!!
  
  function get_empty_cit_data() {
    
    this_module.cit_data = {
      
      pp_sifra: null,
      pp_naziv: null,
      curr_park: null,
      selected_spec_place: null,
      user_number: window.cit_user.user_number
      
    };

    
  };
  this_module.get_empty_cit_data = get_empty_cit_data;

  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {
    this_module.get_empty_cit_data();
  };
  
  
  var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;

  
  
  function choose_portir_pp(park_obj, arg_field_id) {
        
    if ( arg_field_id ) {
      $('#'+arg_field_id).val(park_obj.naziv);
    }
    else {
      $('#'+current_input_id).val(park_obj.naziv);
    };
    
    
    this_module.cit_data.pp_sifra = park_obj.sifra;
    this_module.cit_data.pp_naziv = park_obj.naziv;
    
    
    var default_tag = this_module.def_grupe[this_module.cit_data.pp_sifra];
    
    this_module.choose_portir_special_place({ sifra: default_tag, naziv: default_tag }, 'portir_special_places');
    
    
    $.each(window.all_pp_all_data, function(pp_ind, park) {
      
      if (park.pp_sifra == park_obj.sifra ) {
        
        this_module.cit_data.curr_park = park;
        
        if ( park.fixed_open == true ) {
          $('#portir_fixed_open_ramp i').not('.fa-cog').html('SPUSTI<br>RAMPU!');
          $('#portir_fixed_open_ramp').not('.fa-cog').addClass('blink_me');
        } else {
          $('#portir_fixed_open_ramp i').not('.fa-cog').html('TRAJNO<br>OTVORI');
          $('#portir_fixed_open_ramp').not('.fa-cog').removeClass('blink_me');
        };
        
        if ( park.fixed_close == true ) {
          $('#portir_fixed_close_ramp i').not('.fa-cog').html('OTKLJUČAJ');
          $('#portir_fixed_close_ramp').not('.fa-cog').addClass('blink_me');
        } else {
          $('#portir_fixed_close_ramp i').not('.fa-cog').html('ZAKLJUČAJ');
          $('#portir_fixed_close_ramp').not('.fa-cog').removeClass('blink_me');
        };
        
      };
    });
    
    // refreshaj search parameter za special place tagove !!!
    $('#portir_special_places').data('we_auto', {
      desc: 'za odabir special places u modulu portir',
      local: true,
      return: {},
      show_on_click: true,
      list: `${park_obj.naziv}_special_places`,
    });
  
  };
  this_module.choose_portir_pp = choose_portir_pp;  
  
 
  function choose_portir_special_place(arg_place_obj, arg_field_id) {
    
    
    if ( arg_field_id ) {
      $('#'+arg_field_id).val(arg_place_obj.naziv);
    }
    else {
      $('#'+current_input_id).val(arg_place_obj.naziv);
    };
    
    this_module.cit_data.selected_spec_place = arg_place_obj.sifra;
    
    get_park_list('zovem get park list kad odaberem special place u drop listi unutar portir modula').then(
    function() {
    

        if ( 
          window.all_pp_all_data             &&
          $.isArray(window.all_pp_all_data)  &&
          window.all_pp_all_data.length > 0 
        ) {

        $.each(window.all_pp_all_data, function(pp_ind, park_obj) {

          if ( 
            arg_place_obj.sifra !== 'nema_grupu'
            &&
            park_obj.pp_sifra == this_module.cit_data.pp_sifra
            &&
            park_obj.special_places
            &&
            Object.keys(park_obj.special_places).length > 0
          ) {
            
            // loop po svim special places ( KOJI JE OBJEKT ) unutar jednog parka
            $.each( park_obj.special_places, function( place_tag, spec_place_obj ) {
              
              if ( arg_place_obj.sifra == place_tag ) {
                var avail = spec_place_obj.avail_out;
                if ( avail == 0 ) avail = `<span style="color: red;">0</span>`;
                $('#portir_avail_count_number').html(avail);
              };
            });
            
          } else if ( 
            park_obj.pp_sifra == this_module.cit_data.pp_sifra
            &&
            ( !park_obj.special_places || arg_place_obj.sifra == 'nema_grupu' )
          ) {
            // ako parking nema special places
            // onda uzmi avail od OUT num
            var avail = park_obj.pp_available_out_num;
            if ( avail == 0 ) avail = `<span style="color: red;">0</span>`;
            $('#portir_avail_count_number').html(avail);
            
          };

        }); // kraj loopa po svim parkovima

      }; // kraj ifa jel postoji all parks lista
    },
    function(error) {
      console.error('Greška prilikom get park list kad odaberem special place u drop listi unutar portir modula');
      console.error(error);
    });  
    
  };
  this_module.choose_portir_special_place = choose_portir_special_place;  

  
  function select_user_row_from_search_regs(user) {
    
    $('#'+current_input_id).val(user.name);
    
    if ( !this_module.cit_data.pp_sifra ) {
      no_pp_selected();
      return;
    };
    
    // upiši user number od user kojeg sam selektao u drop listi !!!
    this_module.cit_data.user_number = user.user_number;
    
    var found_customer_tag = null;
    
    $.each(window.all_pp_all_data, function(pp_ind, park_obj) {
      
      if ( park_obj.pp_sifra == this_module.cit_data.pp_sifra ) {
        
        // loop po svim special places ( KOJI JE OBJEKT ) unutar jednog parka
        $.each( park_obj.special_places, function( place_tag, spec_place_obj ) {

          // loop da provjerim svaki specijal place sa svakim customer tagom ( KOJI JE ARRAY )
          $.each( user.customer_tags, function( tag_index, tag_obj ) {
            
            if ( found_customer_tag == null && place_tag == tag_obj.sifra ) {
              
              found_customer_tag = place_tag;
              
              this_module.choose_portir_special_place({ sifra: place_tag, naziv: place_tag }, 'portir_special_places');
              
            };

          });  // end loop po customer tags
          
        }); // end loop po special places
        
        // ako nije našao customer tag koji se poklapa ili ovaj park uopće nema special places
        if ( found_customer_tag == null || !park_obj.special_places) {

          this_module.choose_portir_special_place({ sifra: 'nema_grupu', naziv: 'NEMA GRUPU' }, 'portir_special_places');

        };

        
      }; // kraj ifa ako je pronasao odabrani parking po sifri
      
    }); // end loop po svm parkovima
    

    /*
    if ( found_customer_tag == null ) {
      var default_tag = this_module.def_grupe[this_module.cit_data.pp_sifra];
      this_module.choose_portir_special_place({ sifra: default_tag, naziv: default_tag }, 'portir_special_places');
    };
    */

  };
  this_module.select_user_row_from_search_regs = select_user_row_from_search_regs;
  


  function set_button_in_progress_state(state, button_id) {


    if ( state ) {
      
      if ( button_id.indexOf('fixed') == -1 ) $(button_id).css('transform', 'scale(0.9)' );  

      $(button_id).css('pointer-events', 'none' );
      $(button_id).css('opacity', '0.7' );
      $(button_id + ' i').not('.fa-cog').css('display', 'none');
      $(button_id).find('.fa-cog').css('display', 'block');
      
      
    } else {

      if ( button_id.indexOf('fixed') == -1 ) $(button_id).css('transform', 'scale(1)' );  
      $(button_id).css('pointer-events', 'all' );
      $(button_id).css('opacity', '1' );
      $(button_id + ' i').not('.fa-cog').css('display', 'block');
      $(button_id).find('.fa-cog').css('display', 'none');

    }

  };
  this_module.set_button_in_progress_state = set_button_in_progress_state;

  
  
  function portir_open_ramp(enter_or_exit) {
    
    if ( !this_module.cit_data.pp_sifra ) {
      no_pp_selected();
      return;
    };
    
    var ime_rampe = this_module.cit_data.curr_park.ramps[0];
    var current_tag = this_module.cit_data.selected_spec_place;
    var user_num = this_module.cit_data.user_number;
    var curr_park = this_module.cit_data.curr_park;
    
    
    var button_selector = null;
    if ( enter_or_exit == 'enter' ) button_selector = '#portir_enter_pp';
    if ( enter_or_exit == 'exit' ) button_selector = '#portir_exit_pp';
    
    
    set_button_in_progress_state(true, button_selector);

    
    // AKO JE SPECIFIČNI USER !!!!!!!!
    // AKO JE SPECIFIČNI USER !!!!!!!!
    // AKO JE SPECIFIČNI USER !!!!!!!!
    
    if ( user_num !== window.cit_user.user_number ) {

      get_active_booking_or_parking( this_module.cit_data.pp_sifra , user_num)
      .then( function( response ) {

        if ( !response.success || response.success !== true ) {
          popup_greska_prilikom_check_book_and_park();
          return;
        };

        pi_otvori_rampu(ime_rampe, user_num)
        .then(
        function(odgovor_rampe) {
          
          if ( odgovor_rampe.success == true ) {
            
            // ---------------------- PORTIR JE KLIKNUO GUMB ULAZAK !!!----------------------
            if (enter_or_exit == 'enter') {
            
                // ---------------------- PROVJERA JEL SE PORTIR ZEZNIO I UMJESTO IZLAZAK KLIKNUO ULAZ !!!---------------------- 
                // ako ovaj user ima u bazi da je NIJE parkiran onda ENTER PP
                if ( response.parking.length == 0 ) {
                  this_module.portir_enter_pp(current_tag);
                } 
                else {
                  this_module.portir_exit_pp(curr_park);
                };
              
            } 
            else {
              
              // ---------------------- PORTIR JE KLIKNUO GUMB IZLAZAK !!!----------------------
              this_module.portir_exit_pp(curr_park);
            };
            
          } else {
            
            // ---------------------- AKO JE RAMPA ODGOVORILA SUCCESS == FALSE  !!!----------------------
            
            if ( odgovor_rampe.msg && typeof window[odgovor_rampe.msg] == 'function' ) {
              window[odgovor_rampe.msg]();
            } else {
              popup_greska_prilikom_parkinga();
              
            };
            
            set_button_in_progress_state(false, button_selector );
            
          }; // kraj ako je success == false
          
        })
        .catch(
        function(error){
          console.error('ERROR kod pi open ramp');
          console.log(error);
          set_button_in_progress_state(false, button_selector );
        });
        
        // kraj success od then-a za get active book or park
      })
      .catch(
      function(error){
        console.log(error);
      });
      
    } 
    else {
      // AKO JE PORTIR !!!!!!!!!!
      // AKO JE PORTIR !!!!!!!!!!
      // AKO JE PORTIR !!!!!!!!!!
      pi_otvori_rampu(ime_rampe, user_num)
        .then(
        function(odgovor_rampe) {
          
          if ( odgovor_rampe.success == true ) {
            
            if (enter_or_exit == 'enter') this_module.portir_avail_new_state(current_tag, -1, '#portir_enter_pp' );
            if (enter_or_exit == 'exit') this_module.portir_avail_new_state(current_tag, 1, '#portir_exit_pp' );


          } else {
            
            if ( odgovor_rampe.msg && typeof window[odgovor_rampe.msg] == 'function' ) {
              window[odgovor_rampe.msg]();
            } else {
              popup_greska_prilikom_parkinga();
            };
            set_button_in_progress_state(false, button_selector );
            
          };
        })
        .catch(
        function(error){
          console.error('ERROR kod pi open ramp KAO PORTIR');
          console.log(error);
          set_button_in_progress_state(false, button_selector );
        });
      
    };
    
    
  };
  this_module.portir_open_ramp = portir_open_ramp; 
  
  
  function pi_otvori_rampu(ime_rampe, user_number) {
    
    return new Promise(function(resolve, reject) {

      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "GET",
        url: ('/open_rampa/' + ime_rampe + '/' + parseInt(user_number) ),
        dataType: "json",
        timeout: 1000*12
      })
      .done(function (response) {

        if ( response.success == true ) {

          console.log( 'pi rampa se otvorila !!!!!!!' );
          console.log( response );
          /*
          ------------------------------MOGUĆI ODGOVORI ---------------------------

          if ( rampa_msg == 'opened' ) {
            global.rampa_status_errors[rampa_i_user] = null;
            res.json({success: true, msg: 'opened' });
          };

          if ( rampa_msg == 'in_process' ) {
            res.json({success: false, msg: 'popup_ramp_in_process' });
          };

          if ( rampa_msg == 'error' ) {
            global.rampa_status_errors[rampa_i_user] = Date.now();
            res.json({ success: false, msg: 'popup_greska_prilikom_parkinga' });
          };

          if ( rampa_msg == 'napon_too_low' ) {
            global.rampa_status_errors[rampa_i_user] = Date.now();
            res.json({ success: false, msg: 'popup_napon_too_low' });
          };      

          if ( rampa_msg == 'fixed_close_state' ) {
            global.rampa_status_errors[rampa_i_user] = Date.now();
            res.json({ success: false, msg: 'popup_fixed_close_state' });
          }; 

          if ( rampa_msg == 'fixed_open_state' ) {
            global.rampa_status_errors[rampa_i_user] = Date.now();
            res.json({ success: false, msg: 'popup_fixed_open_state' });
          };  


          */

          resolve(response);

        } else {

          resolve(response);
          console.error('SUCCESS == FALSE  pri raspberry otvaranju rampe !!!!!!!');  
        };

      })
      .fail(function (fail_obj) {

        reject(fail_obj);
        console.error('FAIL pri raspberry otvaranju rampe !!!!!!!');

      });

    }); // kraj promisa
    
  };
  this_module.pi_otvori_rampu = pi_otvori_rampu;
  
  
  
  function portir_enter_pp(current_tag) {
    

    get_park_list('zovem get park unutar PORTIR ENTER PP func').then(
    function() {
      
      $.each(window.all_pp_all_data, function(pp_ind, park) {
        if (park.pp_sifra == this_module.cit_data.pp_sifra ) {
          this_module.cit_data.curr_park = park;
        };
      });
      
      var button_selector = '#portir_enter_pp';

      // var park_data = this_module.cit_data;

      var time_stamp = Date.now();

      var action_object = { 

        //* postojeci fieldovi  /  
        sifra : null,
        from_time: time_stamp,
        to_time: null,
        paid: null,  
        pp_sifra : String(this_module.cit_data.pp_sifra),
        user_number: this_module.cit_data.user_number,
        was_reserved_id: null,
        mobile_from_time: null,

        //* moji fieldovi  /

        status: 'parked',
        user_set_book_duration: null,

        reservation_from_time: null,
        reservation_to_time: null,
        reservation_expired: null,
        reservation_canceled: null,
        reservation_paid: null,


        current_price: this_module.cit_data.curr_park.pp_current_price,

        pack_type: null,
        duration: null,
        revenue: null,
        owner_revenue: null,
        currency: 'kn',

        indoor: false,

        current_tag: ( (current_tag == 'nema_grupu') ? null: current_tag ),

        user_status: '',
        
        reg: null,
        
        portir: true,
        
      }; 

      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "POST",
        url: "/enter_park",
        data: JSON.stringify(action_object),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        })
      .done(function(response) {

        console.log(response);

        if ( response.success == true ) {

          set_button_in_progress_state(false, button_selector );

          // resetiraj sve u portir modulu tako da kliknem na lef menu link !!!!!
          setTimeout( function() { $('#left_menu_button_portir').click(); }, 800);
          
        } 
        else {
          
          popup_greska_prilikom_parkinga();
          set_button_in_progress_state(false, button_selector );
          
          // resetiraj sve u portir modulu tako da kliknem na lef menu link !!!!!
          setTimeout( function() { $('#left_menu_button_portir').click(); }, 800);
        };
      })
      .fail(function(error) {
        console.log(error);
        popup_greska_prilikom_parkinga();
        set_button_in_progress_state(false, button_selector );
        // resetiraj sve u portir modulu tako da kliknem na lef menu link !!!!!
        setTimeout( function() { $('#left_menu_button_portir').click(); }, 800);
      });    

      

    },
    function(error) {
      console.error('Greška prilikom get park list unutar create portir modul');
      console.error(error);
    });  
    

  };
  this_module.portir_enter_pp = portir_enter_pp; 
    

  function portir_exit_pp(park_data) {


    // samo za testiranj GUI-ja
    // set_booking_state( false, this_module.cit_data);
    // return;

    
    var button_selector = '#portir_exit_pp';
    
    set_button_in_progress_state(true, button_selector );
    
    if ( !window.cit_user || !window.cit_user.user_number ) {
      popup_nedostaje_user();
      set_button_in_progress_state(false, button_selector );
      return;
    };
    
    
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: ( "/exit_park/" + this_module.cit_data.pp_sifra + '/' + this_module.cit_data.user_number),
      dataType: "json",
      })
    .done(function(response) {

      console.log(response);

      if ( response.success == true ) {

        set_button_in_progress_state(false, button_selector );
        
        this_module.update_avail_num_html();
        
        // resetiraj sve u portir modulu tako da kliknem na lef menu link !!!!!
        setTimeout( function() { $('#left_menu_button_portir').click(); }, 800);

      } 
      else {
        // ako postoji popup naslov u response msg onda ga pokreni !!
        if ( response.msg && typeof window[response.msg] == 'function' ) {
          window[response.msg]();
        } else {
          // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
          popup_greska_prilikom_EXIT_parkinga();
        };
        
        set_button_in_progress_state(false, button_selector );
        
        // resetiraj sve u portir modulu tako da kliknem na lef menu link !!!!!
        setTimeout( function() { $('#left_menu_button_portir').click(); }, 800);
        
      };
    })
    .fail(function(error) {
      console.log(error);
      // ako postoji popup naslov u response msg onda ga pokreni !!
      if ( error.msg && typeof window[error.msg] == 'function' ) {
        window[error.msg]();
      } else {
        // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
        popup_greska_prilikom_EXIT_parkinga();
      };
      set_button_in_progress_state(false, button_selector );
      
      // resetiraj sve u portir modulu tako da kliknem na lef menu link !!!!!
      setTimeout( function() { $('#left_menu_button_portir').click(); }, 800);
      
    });
    
  };
  this_module.portir_exit_pp = portir_exit_pp;
  
    
  function portir_avail_new_state(current_tag, num, button_selector) {
    
    var data = {
      action_obj: {current_tag: current_tag, indoor: false, from_time: Date.now() },
      num: num,
      pp_sifra: String(this_module.cit_data.pp_sifra)
    };
    
    
     $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "POST",
        url: "/portir_avail_new_state",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        })
      .done(function(response) {

        console.log(response);

        if ( response.success == true ) {

          set_button_in_progress_state(false, button_selector );
          
          

        } 
        else {
          
          popup_greska_prilikom_parkinga();
          set_button_in_progress_state(false, button_selector );
        };
       
        this_module.update_avail_num_html();
       
      })
      .fail(function(error) {
        console.log(error);
        popup_greska_prilikom_parkinga();
        set_button_in_progress_state(false, button_selector );
      });    
    
  };
  this_module.portir_avail_new_state = portir_avail_new_state;

  
  function update_avail_num_html() {
    
    var current_selected_tag = this_module.cit_data.selected_spec_place;
    var current_selected_naziv = (current_selected_tag == 'nema_grupu') ? 'NEMA GRUPU' : current_selected_tag;
    setTimeout( function() {
      this_module.choose_portir_special_place(
        { sifra: current_selected_tag, naziv: current_selected_naziv }, 
        'portir_special_places'
      );
    }, 500);
    
  };
  this_module.update_avail_num_html = update_avail_num_html;
  
  
  function portir_fixed_open_ramp() {

    
    if ( !this_module.cit_data.pp_sifra ) {
      no_pp_selected();
      return;
    };

    var button_selector = '#portir_fixed_open_ramp';
    set_button_in_progress_state(true, button_selector );
    
    
    get_park_list('zovem get park unutar PORTIR FIXED OPEN ramp').then(
    function() {
      

      $.each(window.all_pp_all_data, function(pp_ind, park) {
        if (park.pp_sifra == this_module.cit_data.pp_sifra ) {
          this_module.cit_data.curr_park = park;
        };
      });
      
      var ime_rampe = this_module.cit_data.curr_park.ramps[0];
      
      var new_state = null;
      
      // ovo je toggle !!!!!!!
      // ovo je toggle !!!!!!!
      // ovo je toggle !!!!!!!
      
      if ( this_module.cit_data.curr_park.fixed_open == true ) new_state = false;
      if ( this_module.cit_data.curr_park.fixed_open == false ) new_state = true;

      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "GET",
        url: ( "/fixed_open_rampa/" + ime_rampe + '/' + new_state),
        dataType: "json",
        })
      .done(function(response) {

        console.log(response);
        
        set_button_in_progress_state(false, button_selector );
        

        if ( response.success == true ) {
          
          if ( new_state == true ) {
            $('#portir_fixed_open_ramp i').not('.fa-cog').html('SPUSTI<br>RAMPU!');
            $('#portir_fixed_open_ramp').not('.fa-cog').addClass('blink_me');
            
          } else {
            
            $('#portir_fixed_open_ramp i').not('.fa-cog').html('TRAJNO<br>OTVORI');
            $('#portir_fixed_open_ramp').not('.fa-cog').removeClass('blink_me');
            
          };
          
        } 
        else {
          
          console.error(response);
          // ako postoji popup naslov u response msg onda ga pokreni !!
          if ( response.msg && typeof window[response.msg] == 'function' ) {
            window[response.msg]();
          } else {
            // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
            popup_greska_prilikom_rampa_move();
          };
        };
        
      })
      .fail(function(error) {
        
        console.error(error);
        // ako postoji popup naslov u response msg onda ga pokreni !!
        if ( error.msg && typeof window[error.msg] == 'function' ) {
          window[error.msg]();
        } else {
          // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
          popup_greska_prilikom_rampa_move();
        };
        
        set_button_in_progress_state(false, button_selector );
      });

    },
    function(error) {
      console.error('Greška prilikom get park list unutar portir fixed open ramp');
      console.error(error);
      set_button_in_progress_state(false, button_selector );
    });   
    

  };
  this_module.portir_fixed_open_ramp = portir_fixed_open_ramp; 
  
  
  
    
  function portir_fixed_close_ramp() {

    
    if ( !this_module.cit_data.pp_sifra ) {
      no_pp_selected();
      return;
    };

    var button_selector = '#portir_fixed_close_ramp';
    set_button_in_progress_state(true, button_selector );
    
    
    get_park_list('zovem get park unutar PORTIR FIXED CLOSEEEEEE ramp').then(
    function() {
      

      $.each(window.all_pp_all_data, function(pp_ind, park) {
        if (park.pp_sifra == this_module.cit_data.pp_sifra ) {
          this_module.cit_data.curr_park = park;
        };
      });
      
      var ime_rampe = this_module.cit_data.curr_park.ramps[0];
      
      var new_state = null;
      
      // ovo je toggle !!!!!!!
      // ovo je toggle !!!!!!!
      // ovo je toggle !!!!!!!
      
      if ( this_module.cit_data.curr_park.fixed_close == true ) new_state = false;
      if ( this_module.cit_data.curr_park.fixed_close == false ) new_state = true;

      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "GET",
        url: ( "/fixed_close_rampa/" + ime_rampe + '/' + new_state),
        dataType: "json",
        })
      .done(function(response) {

        console.log(response);
        
        set_button_in_progress_state(false, button_selector );
        

        if ( response.success == true ) {
          
          if ( new_state == true ) {
            $('#portir_fixed_close_ramp i').not('.fa-cog').html('OTKLJUČAJ!');
            $('#portir_fixed_close_ramp').not('.fa-cog').addClass('blink_me');
            
          } else {
            
            $('#portir_fixed_close_ramp i').not('.fa-cog').html('ZAKLJUČAJ!');
            $('#portir_fixed_close_ramp').not('.fa-cog').removeClass('blink_me');
            
          };
          
        } 
        else {
          
          console.error(response);
          // ako postoji popup naslov u response msg onda ga pokreni !!
          if ( response.msg && typeof window[response.msg] == 'function' ) {
            window[response.msg]();
          } else {
            // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
            popup_greska_prilikom_rampa_move();
          };
        };
        
      })
      .fail(function(error) {
        
        console.error(error);
        // ako postoji popup naslov u response msg onda ga pokreni !!
        if ( error.msg && typeof window[error.msg] == 'function' ) {
          window[error.msg]();
        } else {
          // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
          popup_greska_prilikom_rampa_move();
        };
        
        set_button_in_progress_state(false, button_selector );
      });

    },
    function(error) {
      console.error('Greška prilikom get park list unutar portir fixed open ramp');
      console.error(error);
      set_button_in_progress_state(false, button_selector );
    });   
    

  };
  this_module.portir_fixed_close_ramp = portir_fixed_close_ramp; 
  
  
  
  
  this_module.cit_loaded = true;
 
}
  
  
};