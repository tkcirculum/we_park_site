var module_object = {
  create: function( data, parent_element, placement ) {
    
    var this_module = window[module_url]; 
    
    var customers_list_module_url = '/right_sidebar/cit_search_list/customers_list/cit_customers_list_module.js';
    get_cit_module(customers_list_module_url, 'load_css');
    
    
    /*
    var owner_module_load = function() {
      return ( 
        window[owner_module_url] &&
        window[owner_module_url].cit_loaded 
        // &&
        // window[pp_list_module_url] &&
        // window[pp_list_module_url].cit_loaded
        // 
      )
    };
    */
    
    // wait_for(owner_module_load, function() {
    
    
    wait_for('true == true', function() {
      
      
    var component_id = null;


    var cit_autocomplete_module = window['/cit_components/cit_autocomplete/cit_autocomplete_module.js'];


      
      
      
      
    var component_html = 
    `
<div id="customers_component" class="main_module_component container-fluid" style="float: left; height: auto;">
      
      <div class="row" style="padding-bottom: 10px; font-size: 12px; font-weight: 700;">
        
        <div id="cit_customer_user_saved" class="col-md-4 col-sm-12">
          Spremio:
        </div>

        <div id="cit_customer_user_edited" class="col-md-4 col-sm-12">
          Editirao:
        </div>
        
        
        <div id="cit_customer_last_login" class="col-md-4 col-sm-12">
          Last Login:
        </div>        

      </div>  
  
  
      <div class="row" style="padding-bottom: 10px; font-size: 12px; font-weight: 700;">
        
        <div id="cit_customer_gdpr_saved" class="col-md-4 col-sm-12">
          GDPR SPREMLJEN:
        </div>

        <div id="cit_customer_gdpr_accassed" class="col-md-4 col-sm-12">
          GDPR POGLEDAN:
        </div>
        
        
        <div id="cit_customer_deleted" class="col-md-4 col-sm-12">
          KORISNIK OBRISAN:
        </div>        

      </div>   
  
      
      <div class="row">
        
<!--
        <div class="col-md-6 col-sm-12">
          <h6 id="customer_big_label" style="color: #a2a2a2;">Podaci korisnika:</h6>
        </div>
-->
        <div class="col-md-12 col-sm-12">
          
          <div id="save_customer_btn" 
               class="cit_button cit_center cit_tooltip" 
               style="width: 220px; float: right; display: none;"
               data-toggle="tooltip" data-placement="bottom"
               title="SPREMI NOVOG KORISNIKA">
            <i class="fas fa-save" style="margin-right: 10px;"></i>SPREMI KORISNIKA  
          </div>
          
          <div id="edit_customer_btn" 
               class="cit_button cit_center cit_tooltip" 
               style="width: 220px; float: none; background-color: #e84030; display: none; margin: 30px auto 0;"
               data-toggle="tooltip" data-placement="bottom"
               title="SPREMI IZMJENE NA PODACIMA KORISNIKA">
            <i class="fas fa-edit" style="margin-right: 10px;"></i>AŽURIRAJ KORISNIKA
          </div>          
          
        </div>

      
      </div>

  
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 col-sm-12">
          <h6 id="customer_big_card_label" style="color: #a2a2a2;">Kartica korisnika:</h6>
        </div>
      </div>  

    <div class="row" style="margin-bottom: 30px;">

      <div class="col-md-4 col-sm-12" 
           style="display: flex;
                  justify-content: center;
                  flex-direction: column;
                  align-items: center;">
        
          <div class="cit_card_label" id="customer_balance_label" style="clear: left; float: left;">Stanje računa:</div>
        
          <div class="cit_card_big_number" id="customer_balance" style="max-width: 200px;">
            <span id="customer_balance_amount">0</span>
            <span class="cit_small_label" style="color: #08aff5; margin-left: 5px;">kn</span>
          </div>

      </div>
      
      <div class="col-md-4 col-sm-12" 
           style="display: flex;
                  justify-content: center;
                  flex-direction: column;
                  align-items: center;">
        
          <div class="cit_card_label" id="monthly_products_box_label" style="clear: left; float: left;">
            ${window.we_trans( 'id__monthly_products_label', window.we_lang) }
          </div>
        
          <div id="monthly_products_box" style="max-width: 350px; width: 100%;">
            
          </div>

      </div>
      
      
      <div class="col-md-4 col-sm-12" 
           style="display: flex;
                  justify-content: center;
                  flex-direction: column;
                  align-items: center;">
        
          <div class="cit_card_label" id="customer_credit_card_list_label" style="clear: left; float: left;">Kreditne kartice:</div>
        
          <div id="customer_credit_card_list" style="max-width: 350px;">
            
          </div>

      </div>       


    </div>

    <div class="row">
      <div class="col-md-4 col-sm-12">
        <div id="customer_name_label" class="cit_label">Nadimak</div>
        <input id="customer_name" type="text" class="cit_input" autocomplete="off">
      </div>
      
      <div class="col-md-4 col-sm-12">    
        <div id="customer_email_label" class="cit_label">Email Korisnika</div>
        <input id="customer_email" type="text" class="cit_input" autocomplete="off">
      </div>

      <div class="col-md-4 col-sm-12">
        <div id="user_full_name_label" class="cit_label">Ime i prezime</div>
        <input id="customer_full_name" type="text" class="cit_input" autocomplete="off">
      </div>
      
      
  </div>
  
  <div class="row">
      

      
      <div class="col-md-4 col-sm-12">
        <div id="customer_tel_label" class="cit_label">Telefon Korisnika</div>
        <input id="customer_tel" type="text" class="cit_input" autocomplete="off">
      </div>
    
      <div class="col-md-4 col-sm-12">
        <div id="customer_address_label" class="cit_label">Adresa</div>
        <input id="customer_address" type="text" class="cit_input" autocomplete="off">
      </div> 
    
      <div class="col-md-4 col-sm-12">
        <div class="cit_switch_wrapper">
          <div id="customer_gdpr_label" class="cit_label">GDPR POTVRĐEN</div>
          <div id="customer_gdpr" class="cit_switch off">
                <div class="switch_circle"></div>
                <div class="on_text switch_text">DA</div>
                <div class="off_text switch_text">NE</div>
          </div>
        </div>
      </div>   
    
  </div>  

  <div class="row">
      
      <div class="col-md-4 col-sm-12">
        <div id="customer_business_name_label" class="cit_label">Ime Tvrtke</div>
        <input id="customer_business_name" type="text" class="cit_input" autocomplete="off">
      </div>
      
      <div class="col-md-4 col-sm-12">
        <div id="customer_business_address_label" class="cit_label">Adresa Tvrtke</div>
        <input id="customer_business_address" type="text" class="cit_input" autocomplete="off">
      </div>

      <div class="col-md-4 col-sm-12">
        <div id="customer_business_email_label" class="cit_label">Email Tvrtke</div>
        <input id="customer_business_email" type="text" class="cit_input" autocomplete="off">
      </div> 
      
  </div>
  
  
  <div class="row">
    
    <div class="col-md-4 col-sm-12">
      <div id="customer_business_tel_label" class="cit_label">Telefon Tvrtke</div>
      <input id="customer_business_tel" type="text" class="cit_input" autocomplete="off">
    </div>
    
    <div class="col-md-4 col-sm-12">
      <div id="business_contact_name_label" class="cit_label">Kontakt Osoba Tvrtke</div>
      <input id="business_contact_name" type="text" class="cit_input" autocomplete="off">
    </div>

    
    <div class="col-md-4 col-sm-12">
      <div id="customer_oib_label" class="cit_label">OIB</div>
      <input id="customer_oib" type="text" class="cit_input" autocomplete="off">
    </div> 

  </div>
  
    <div class="row">
      
      
    <div class="col-md-4 col-sm-12">
      <div id="customer_iban_label" class="cit_label">IBAN</div>
      <input id="customer_iban" type="text" class="cit_input" autocomplete="off">
    </div> 

      
      <div class="col-md-4 col-sm-12">
        <div class="cit_switch_wrapper">
          <div id="customer_pdv_label" class="cit_label">U SUSTAVU PDV</div>
          <div id="customer_pdv" class="cit_switch off">
                <div class="switch_circle"></div>
                <div class="on_text switch_text">DA</div>
                <div class="off_text switch_text">NE</div>
          </div>
        </div>
      </div> 
      
      <div class="col-md-4 col-sm-12">
        <div class="cit_switch_wrapper">
          <div id="customer_obrtnik_label" class="cit_label">OBRTNIK</div>
          <div id="customer_obrtnik" class="cit_switch off">
                <div class="switch_circle"></div>
                <div class="on_text switch_text">DA</div>
                <div class="off_text switch_text">NE</div>
          </div>
        </div>
      </div>      
      
      
  
    </div>  
  
  <div class="row">
    
    <div class="col-md-6 col-sm-6" id="customer_tag_container">
      
      <div id="tagovi_customer_label" class="cit_label">Grupe / tagovi</div>
      <input id="tagovi_customer" type="text" class="cit_input we_auto same_width" autocomplete="off">
      
      
      <div id="customer_tags_box">
        
        <!-- PRIMJER
        <div class="cit_tag_pill">TELE 2<i class="fas fa-times"></i></div>
        <div class="cit_tag_pill">SIGNUM<i class="fas fa-times"></i></div>
        <div class="cit_tag_pill">TVRTKA 001<i class="fas fa-times"></i></div>      
        -->
        

      </div>
      
      

    </div>
    
      

      

      <div class="col-md-6 col-sm-6">
        
        <div id="customer_acquisition_label" class="cit_label">Kanal Akvizicije</div>
        <input autocomplete="off" id="customer_acquisition" 
          data-cit_search="kanal_akvizicije"
          data-cit_select_fields="naziv"
          data-cit_return_fields="all"
          data-cit_list_container="drop_list"
          data-cit_hide="sifra"
          type="text" 
          class="cit_autocomplete same_width">
        
      </div>

    
  </div>
  
  <div class="row">


    <div class="col-md-6 col-sm-6">

      <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 col-sm-12">
          <h6 id="customer_big_plate_label" style="color: #a2a2a2;">REGISTRACIJSKE OZNAKE</h6>
        </div>
      </div>

      <div id="car_plates_label" class="cit_label">Upiši i dodaj novu registraciju:</div>

      <input id="write_car_plate" type="text" class="cit_input" style="float: left;" autocomplete="off">

      <div id="add_car_plate_btn" 
           class="cit_button cit_center cit_tooltip" 
           style="width: 20px; float: right; margin-right: 14px;"
           data-toggle="tooltip" data-placement="bottom"
           title="DODAJ REGISTRACIJU">
        <i class="fas fa-plus"></i>
      </div>

      
      <div id="car_plate_box" style="width: 100%; float: left; clear: both; height: auto;">
        <!-- OVDJE IDU SVE REGISTRACIJE -->
      </div>

    </div>


    <div class="col-md-6 col-sm-6">

      <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 col-sm-12">
          <h6 id="customer_big_plate_label" style="color: #a2a2a2;">VIP STATUS</h6>
        </div>
      </div>

       <!-- OVDJE IDE DROP LISTA SA VIP STATUSOM -->

      <div id="pp_vip_box" style="width: 100%; float: left; clear: both; height: auto; margin-top: 5px;">


      </div>

    </div>


  </div>

      

    
  
  
    <div class="row" style="margin-top: 30px;">
      
      <!-- START -------------------- LABEL I NAVIGACIJA i FILTER -->
      <div class="col-md-12 col-sm-12">
        
      <div class="row" style="margin-bottom: 30px;">
        <div class="col-md-1 col-sm-1">
          <div class="cit_big_table_label" id="customer_history_big_label" style="color: #a2a2a2; ">Akcije:</div>
        </div>
        
        <!--
          <div id="filter_park_actions_by_pp_label" class="cit_label">Filtriraj po parkingu</div>
          <input id="filter_park_actions_by_pp" type="text" class="cit_input" autocomplete="off">          
        -->
        
        <div class="col-md-3 col-sm-3">
          
          <div id="customers_izaberi_park_label" class="cit_label">Izaberi parking:</div>
          <input id="customers_izaberi_park" type="text" class="cit_input we_auto simple" autocomplete="off">

        </div> 

        
          
        <div class="col-md-2 col-sm-2">
          <div id="filter_park_actions_by_datum_od_label" class="cit_label">Datum od:</div>
          <input id="filter_park_actions_by_datum_od" type="text" class="cit_input" autocomplete="off">          
        </div>  

        <div class="col-md-2 col-sm-2">
          <div id="filter_park_actions_by_datum_do_label" class="cit_label">Datum do:</div>
          <input id="filter_park_actions_by_datum_do" type="text" class="cit_input" autocomplete="off">          
        </div>  
        
        <div class="col-md-1 col-sm-1">
          &nbsp;&nbsp;
          <!--
          <div id="filter_park_actions_by_spend_od_label" class="cit_label">Trošak od:</div>
          <input id="filter_park_actions_by_spend_od" type="text" class="cit_input" autocomplete="off">          
          -->
        </div>    
        
        
        <div class="col-md-1 col-sm-1">
          &nbsp;&nbsp;
          <!--
          <div id="filter_park_actions_by_spend_do_label" class="cit_label">Trošak do:</div>
          <input id="filter_park_actions_by_spend_do" type="text" class="cit_input" autocomplete="off">          
          -->
        </div>          
        
        <div class="col-md-2 col-sm-2">
          <div class="cit_big_table_navigation" id="pp_actions_navigation">
            <div class="cit_big_table_previous"><i class="fas fa-caret-left"></i></div>
            <div class="cit_big_table_page_num">1</div>
            <div class="cit_big_table_next"><i class="fas fa-caret-right"></i></div>
          </div>
        </div>
        
      </div>

      </div>
      <!-- END -------------------- LABEL I NAVIGACIJA i FILTER -->
      
      <div class="col-md-12 col-sm-12" id="pp_actions_box">
        
        <div class="cit_big_table_header_row">
          <div class="big_table_col pp_col">PARKING</div>
          
          <div class="big_table_col pp_reservation_start_col">RES. START</div>
          <div class="big_table_col pp_reservation_duration_col">RES. DURATION</div>
          <div class="big_table_col pp_reservation_paid_col">RES. PAID</div>
          
          
          <div class="big_table_col pp_start_time_col">PARK START</div>
          <div class="big_table_col pp_end_time_col">PARK END</div>
          
          
          
          <div class="big_table_col pp_duration_time_col">PARK DURATION</div>
          <div class="big_table_col pp_paid_col">PAID</div>
        </div>


        
      </div>
    </div>
       


    <div class="row" style="margin-top: 30px;">
      
      <!-- START -------------------- LABEL I NAVIGACIJA i FILTER -->
      <div class="col-md-12 col-sm-12">
        
      <div class="row" style="margin-bottom: 30px;">
        <div class="col-md-1 col-sm-1">
          <div class="cit_big_table_label" id="customer_buyed_big_label" style="color: #a2a2a2; ">Proizvodi:</div>
        </div>
        
        <div class="col-md-3 col-sm-3">
          <div id="filter_customer_buyed_by_pp_label" class="cit_label">Filtriraj po ponudi</div>
          <input id="filter_customer_buyed_by_pp" type="text" class="cit_input" autocomplete="off">          
        </div>  
          
        <div class="col-md-2 col-sm-2">
          <div id="filter_customer_buyed_by_datum_od_label" class="cit_label">Datum od:</div>
          <input id="filter_customer_buyed_by_datum_od" type="text" class="cit_input" autocomplete="off">          
        </div>  

        <div class="col-md-2 col-sm-2">
          <div id="filter_customer_buyed_by_datum_do_label" class="cit_label">Datum do:</div>
          <input id="filter_customer_buyed_by_datum_do" type="text" class="cit_input" autocomplete="off">          
        </div>  
        
        <div class="col-md-1 col-sm-1">
          <div id="filter_customer_buyed_by_iznos_od_label" class="cit_label">Iznos od:</div>
          <input id="filter_customer_buyed_by_iznos_od" type="text" class="cit_input" autocomplete="off">          
        </div>          
        
        <div class="col-md-1 col-sm-1">
          <div id="filter_customer_buyed_by_iznos_do_label" class="cit_label">Iznos do:</div>
          <input id="filter_customer_buyed_by_iznos_do" type="text" class="cit_input" autocomplete="off">          
        </div>          
        
        <div class="col-md-2 col-sm-2">
          <div class="cit_big_table_navigation" id="customer_products_navigation">
            <div class="cit_big_table_previous"><i class="fas fa-caret-left"></i></div>
            <div class="cit_big_table_page_num">1</div>
            <div class="cit_big_table_next"><i class="fas fa-caret-right"></i></div>
          </div>
        </div>
        
      </div>

      </div>
      <!-- END -------------------- LABEL I NAVIGACIJA i FILTER -->
      
      
      
      <div class="col-md-12 col-sm-12" id="customer_products_box">


        <div class="cit_big_table_header_row">
          <div class="big_table_col icon_col">PAKET</div>
          <div class="big_table_col desc_col">OPIS</div>
          <div class="big_table_col amount_col">VRIJEDNOST</div>
          <div class="big_table_col price_col">CIJENA</div>
          <div class="big_table_col buy_time_col">KUPLJENO</div>
          <div class="big_table_col valid_from_col">POČETAK</div>
          <div class="big_table_col valid_to_col">KRAJ</div>
          <div class="big_table_col balance_col">STANJE</div>
        </div>

      </div>
    </div>
    
      
</div> 
<!--END OF MAIN COMPONENT DIV-->
`;
      
      
  var this_module = window[module_url]; 
      // ovo je arbitrarni uvijet  - uzeo sam prvi label u formi kao uvijet kad se pojavi ....
  wait_for( `$('#pp_actions_box').length > 0`, function() {
    
    // reset all tables
    $('#pp_actions_box .cit_big_table_row').remove();
    $('#customer_products_box .cit_big_table_row').remove();
    
    this_module.customer_products_page = 1;
    this_module.customer_products_page = 1;
    
    $('.cit_tooltip').tooltip();
    
    
    $('#customers_izaberi_park').data('we_auto', {
      desc: 'za odabir parkinga u customers modulu - za tablicu actions',
      local: false,
      url: '/search_park_places',
      return: { pp_sifra: 1, pp_name: 1, pp_address: 1, _id: 1, pp_current_price: 1 },
      hide_cols: ['_id', 'pp_current_price'],
      query: {},
      show_on_click: true,
      // parent: '#big_stats_table_box',
      // list: 'proba_find',
    });
    
    
    $('#tagovi_customer').data('we_auto', {
      desc: 'za odabir tagova za customera',
      local: true,
      return: {},
      show_on_click: true,
      list: 'user_tags',
    });

    
    $('#save_customer_btn').off('click');
    $('#save_customer_btn').on('click', this_module.save_customer('save') );

    $('#edit_customer_btn').off('click');
    $('#edit_customer_btn').on('click', this_module.save_customer('edit') );
    

    
    $('#add_car_plate_btn').off('click');
    $('#add_car_plate_btn').on('click', this_module.add_car_plate );   

    
        
    $('#pp_actions_navigation .cit_big_table_previous').off('click');
    $('#pp_actions_navigation .cit_big_table_previous').on('click', function() {
      
      if ( this_module.customer_actions_page == 1) return;
      
      this_module.customer_actions_page -= 1;
      this_module.get_customer_park_actions();
    });   

    
    
    
    $('#pp_actions_navigation .cit_big_table_next').off('click');
    $('#pp_actions_navigation .cit_big_table_next').on('click', function() {
      
      this_module.customer_actions_page += 1;
      this_module.get_customer_park_actions();
    });   
    
    

    $('#customer_products_navigation .cit_big_table_previous').off('click');
    $('#customer_products_navigation .cit_big_table_previous').on('click', function() {
      
      if ( this_module.customer_products_page == 1) return;
      
      this_module.customer_products_page -= 1;
      this_module.get_customer_products();
    });   
    
    
    $('#customer_products_navigation .cit_big_table_next').off('click');
    $('#customer_products_navigation .cit_big_table_next').on('click', function() {
      
      this_module.customer_products_page += 1;
      this_module.get_customer_products();
    });     
    
    
    

    
  }, 5000000 );      

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);

    }; 
    
    return {
      html: component_html,
      id: component_id
    };
      
      
  }, 500000 ); // kraj wait for od cjele komponente

  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  
  
    
  function get_empty_cit_data() {
 
    var time_now = Date.now();
    
    this_module.cit_data = {

          user_saved: null,
          user_edited: null,

          saved: time_now,
          edited: time_now,
          last_login: time_now,


          user_address: '',
          user_tel: '',

          business_name: '',
          business_address: '',
          business_email: '',
          business_tel: '',
          business_contact_name: '',

          oib: null,
          iban: null,

          gdpr: true,

          /* START ---------- OVO SU SVE TIME STAMPOVI */
          gdpr_saved: time_now,
          gdpr_accassed: time_now,
          user_deleted: null,
          /* END ---------- OVO SU SVE TIME STAMPOMVI */


          car_plates: null,
          customer_tags: null,
          balance: 0,
          customer_acquisition: null,
          customer_vip: null,
          park_places_owner: null,
      
      
          name: null,
          user_number: null,
      
          full_name: null,
          hash: null,
          email: null,
          email_confirmed: null,
          welcome_email_id: null,
          roles: {
            admin: true,
            we_app_user: true,
            super_admin: false
          }

        };
    
    
    /*
    
      ---------------------------------------------------------------      
      //* START ---------- OVO SU SVE TIME STAMPOVI
      gdpr_saved: null,
      gdpr_accassed: null,
      user_deleted: null,
      //* END ---------- OVO SU SVE TIME STAMPOMVI
      
      
      //* START ---------- OVO SU SVE ARRAYS
      car_plates: null,
      customer_tags: null,
      customer_vip: null,
      park_places_owner: null,      
      //* END ---------- OVO SU SVE ARRAYS
      
      
      // ovo su switchevi
      gdpr: 'customer_gdpr',
      pdv: 'customer_pdv',
      
      
      customer_acquisition: null, // ovo je objekt
      balance: null,
      ---------------------------------------------------------------
    */  
    
    
  };
  this_module.get_empty_cit_data = get_empty_cit_data;
  
  var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {
    this_module.get_empty_cit_data();
  }; // end of if this is initial load
    
  this_module.model_to_input_map = {

    user_address: 'customer_address',
    user_tel: 'customer_tel',

    business_name: 'customer_business_name',
    business_address: 'customer_business_address',
    business_email: 'customer_business_email',
    business_tel: 'customer_business_tel',

    business_contact_name: 'business_contact_name',

    oib: 'customer_oib',
    iban: 'customer_iban',

    obrtnik: 'customer_obrtnik',
    pdv: 'customer_pdv',
    gdpr: 'customer_gdpr',

    name: 'customer_name',
    full_name: 'customer_full_name',
    email: 'customer_email',

  };
  
  function choose_pp_for_action_table(data) {
    console.log('selektiran park');
    console.log(data);
  };
  this_module.choose_pp_for_action_table = choose_pp_for_action_table;  
  
  
  function pp_for_vip_html(park_place_object) {
    
    var pp_vip_row = 
        `
        <div class="pp_vip_row" id="pp_vip_row_${park_place_object._id}">
          <div class="read_pp_vip">${park_place_object.pp_name}</div>
          <div id="delete_pp_vip_${park_place_object._id}" 
               class="cit_button cit_center cit_tooltip" 
               style="width: 20px; float: right;"
               data-toggle="tooltip" data-placement="bottom"
               title="OBRIŠI VIP STATUS">
            <i class="fas fa-minus"></i>
          </div>
        </div> 

        `; 
    return pp_vip_row;
  };
  this_module.pp_for_vip_html = pp_for_vip_html;      
  
  
  function car_plate_html(car_plate_text) {
    
    var car_plate_row = 

        `
        <div class="car_plate_row" id="car_plate_row_${car_plate_text}">
          <div class="read_car_plate">${car_plate_text}</div>
          <div id="delete_car_plate_${car_plate_text}" 
               class="cit_button cit_center cit_tooltip" 
               style="width: 20px; float: right;"
               data-toggle="tooltip" data-placement="bottom"
               title="OBRIŠI REGISTRACIJU">
            <i class="fas fa-minus"></i>
          </div>
        </div>

        `;
    return car_plate_row
    
  };
  this_module.car_plate_html = car_plate_html;
  
  
  function update_car_plates_on_ramps() {
    
    var update_obj = {
      user_number: this_module.cit_data.user_number,
      car_plates: this_module.cit_data.car_plates
    };    
    
    
    if ( !cit_user ) return;
    
      
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: 'POST',
      url: '/save_new_user_plates',
      data: JSON.stringify(update_obj),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {
      
      console.log('odgovor save new user plates');  
      console.log(response);
      
      if ( response.success == true ) {
        
        var popup_text = `REGISTRACIJE SU AŽURIRANE NA SVIM RAMPAMA!`;
        
        // show_popup, popup_text, popup_progress, show_buttons
        show_popup_modal(true, popup_text, null, false);
        // sakrij popup nakon 1 sec
        setTimeout(function() { show_popup_modal(false, popup_text, null, false); }, 1000);

      } else {
        
        var popup_text = 'GREŠKA PRI SPREMANJU REGISTRACIJA!';
        
        show_popup_modal('error', popup_text, null, 'ok' );
        $('#cit_popup_modal_OK').off('click');
        $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
        
      };
    })
    .fail(function(error) {
      
      console.log(error);
      var popup_text = 'GREŠKA PRI SPREMANJU REGISTRACIJA!';
      // show_popup, popup_text, popup_progress, show_buttons
      show_popup_modal('error', popup_text, null, 'ok' );
      $('#cit_popup_modal_OK').off('click');
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
      
    });
    
  }; 
  this_module.update_car_plates_on_ramps = update_car_plates_on_ramps;
  
  
  function add_car_plate() {
    

    // ovaj regex znači ostavi samo a
    var car_plate_text = $('#write_car_plate').val().replace(/(?!ć|Ć|č|Č|š|Š|đ|Đ|ž|Ž)\W/g, '');
    // moram dodatno obrisati underscore jer iznekog razloga on se zvanično tretira kao alphanumeric ????? !!!!!!!
    car_plate_text = car_plate_text.replace(/\_/g, '');
    car_plate_text = car_plate_text.toUpperCase();
    
    
    if ( car_plate_text == '' ) return;
        
    $('#write_car_plate').val('');
    
    
    // ako uopće nema ništa u propertiju car plates
    if ( $.isArray(this_module.cit_data.car_plates) == false  ) {
      this_module.cit_data.car_plates = [];
    };

    
    var car_plate_already_exists = false;
    $.each(this_module.cit_data.car_plates, function(car_plate_index, plate_obj) {
      if ( plate_obj.reg == car_plate_text ) car_plate_already_exists = true;
    });
   
    
    if ( car_plate_already_exists == false ) {
      this_module.cit_data.car_plates.push({ time: Date.now(), reg: car_plate_text });
      
      var row_html = car_plate_html(car_plate_text);
      $('#car_plate_box').append(row_html);
      
      setTimeout(function() {
        $(`#delete_car_plate_${car_plate_text}`).on( 'click', this_module.delete_car_plate );
      }, 100);
      
    };
    
    
    this_module.update_car_plates_on_ramps();
    
  };
  this_module.add_car_plate = add_car_plate;
  
  
  
  function delete_car_plate() {
    var car_plate_id = this.id.replace('delete_car_plate_', '');
    var delete_this_index = null;
    $.each(this_module.cit_data.car_plates, function(car_plate_index, plate_obj) {
      if ( plate_obj.reg == car_plate_id ) delete_this_index = car_plate_index;
    });
    
    if ( delete_this_index !== null ) {
      this_module.cit_data.car_plates.splice(delete_this_index, 1);
      $('#car_plate_row_' + car_plate_id ).remove();
    };
    
  };
  this_module.delete_car_plate = delete_car_plate;

  
  
  function delete_pp_vip() {
    var pp_vip_id = this.id.replace('delete_pp_vip_', '');
    var delete_this_index = null;
    $.each(this_module.cit_data.customer_vip, function(pp_vip_index, pp_vip_object) {
      if ( pp_vip_id == pp_vip_object._id ) delete_this_index = pp_vip_index;
    });
    
    if ( delete_this_index !== null ) {
      this_module.cit_data.customer_vip.splice(delete_this_index, 1);
      $('#pp_vip_row_' + pp_vip_id ).remove();
    };
  };
  this_module.delete_pp_vip = delete_pp_vip;
  
  
  function select_kanal_akvizicije(kliknuti_red_liste, current_cit_search, current_cit_return_fields) {

    var sifra = kliknuti_red_liste.find('.sifra').text().trim();
    var kanal_akvizicije_object = get_entire_object_from_list(local_array_GLOBAL, sifra, 'local' );

    $('#' + current_input_id).val(kanal_akvizicije_object.naziv);

    this_module.cit_data.customer_acquisition = kanal_akvizicije_object;

    
  };
  this_module.select_kanal_akvizicije = select_kanal_akvizicije;
  
  
  
  function customer_tag_pill_html(tag) {
    
    var pill_html = 
        
`
<div id="customer_tag_${tag.sifra}" class="cit_tag_pill" style="margin-top: 0;">
${tag.naziv}<i class="fas fa-times"></i>
</div>
`;
    
    return pill_html;
    
  }
  
  
  function choose_tagovi_customer(selected_tag) {
    
    var curr_input = $('#'+current_input_id);
    curr_input.val( selected_tag.sifra );
    
    console.log(selected_tag);
    
    var tag_pill_html = customer_tag_pill_html(selected_tag);

    // ako već ne postoji pill sa ovim tagom onda ga ubaci
    if ( $('#customer_tags_box').find(`#customer_tag_${selected_tag.sifra}`).length == 0 ) {
      
      $('#customer_tags_box').append(tag_pill_html);
      
      if ( !this_module.cit_data.customer_tags ) this_module.cit_data.customer_tags = [];
      
      this_module.cit_data.customer_tags.push(selected_tag);
      
      // obnovi listener za brisanje tag pill jer je sad došao ovaj novi
      setTimeout( function() {
        $('.cit_tag_pill .fas').off('click', this_module.delete_customer_tag);
        $('.cit_tag_pill .fas').on('click', this_module.delete_customer_tag);
      }, 100);
      
    };
    
  };
  this_module.choose_tagovi_customer = choose_tagovi_customer;

  
  function delete_customer_tag() {
    
    var customer_tag_sifra = $(this).closest('.cit_tag_pill')[0].id.replace('customer_tag_', '');
    
    $('#customer_tag_' + customer_tag_sifra ).remove();
    
    var delete_this_index = null;
    $.each(this_module.cit_data.customer_tags, function(customer_tag_index, tag_object) {
      if ( customer_tag_sifra == tag_object.sifra ) delete_this_index = customer_tag_index;
    });
    if ( delete_this_index !== null ) this_module.cit_data.customer_tags.splice(delete_this_index, 1);
      
  };
  this_module.delete_customer_tag = delete_customer_tag;
    
  
  
  
  function save_customer( save_or_update ) {
    
    
    return function () {
      
      
    if ( !cit_user ) {
      alert('NISTE ULOGIRANI !!!');
      return;
    };

    var customer_object = this_module.cit_data;
      
      
    this_module.form_to_data(); 
    
      
    var request_type = null;
    var request_url = null;
    
    var popup_text = 'USER DATA IS SAVED !';
    
    var time_now =  Date.now();
      
    
    if ( save_or_update == 'save') {
      
      request_type = "POST";
      request_url = "/create_user";
      popup_text = 'USER DATA IS SAVED !';
      
      
      
      customer_object.saved = time_now;
      customer_object.edited = time_now;
      
      customer_object.user_edited =  { 
        user_id: cit_user._id, 
        user_number: cit_user.user_number,
        user_name: cit_user.name,
        full_name: cit_user.full_name,
        time_stamp: time_now
      };
     
      customer_object.user_saved = { 
        user_id: cit_user._id,
        user_number: cit_user.user_number,
        user_name: cit_user.name,
        full_name: cit_user.full_name,
        time_stamp: time_now
      };
      
      
      
    };
    
    
    if ( save_or_update == 'edit') {
      
      request_type = "PUT";
      request_url = "/edit_user/" + customer_object._id;
      popup_text = 'KORISNIK JE AŽURIRAN!';
      
      
      customer_object.edited = time_now;
      
      customer_object.user_edited = { 
        user_id: cit_user.user_id, 
        user_number: cit_user.user_number,
        user_name: cit_user.name,
        full_name: cit_user.full_name,
        time_stamp: time_now
      };      
      
    };
    
    
      
    console.log('prije ajaxa');  
    console.log(this_module.cit_data);
      
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: request_type,
      url: request_url,
      data: JSON.stringify(customer_object),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {
      
      console.log(response);
      
      if ( response.success == true ) {
        
        console.log('poslje ajaxa');  
        console.log(response);
        
        
        this_module.cit_data._id = response.user._id;
        
        // show_popup, popup_text, popup_progress, show_buttons
        show_popup_modal(true, popup_text, null, false);
        setTimeout(function() { show_popup_modal(false, popup_text, null, false); }, 1000);
        
        show_save_or_edit_button('edit_customer_btn');
        
        
        /// VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO
        /// VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO
        /// VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO
        
        // ako je user kojeg editiram zapravo trenutni korisnik onda napravi update cit_usera !!!!!
        
        if (cit_user && cit_user._id == response.user._id ) cit_user = response.user;
        
        /// VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO
        /// VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO
        /// VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO VAŽNO
        
      } else {
        popup_text = 'GREŠKA PRI SPREMANJU PODATAKA KORISNIKA!';
        
        if ( response.msg ) popup_text = response.msg;
        
        show_popup_modal('error', popup_text, null, 'ok' );
        $('#cit_popup_modal_OK').off('click');
        $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
        
      };
    })
    .fail(function(error) {
      
      console.log(error);
      popup_text = 'GREŠKA PRI SPREMANJU PODATAKA KORISNIKA!';
      // show_popup, popup_text, popup_progress, show_buttons
      show_popup_modal('error', popup_text, null, 'ok' );
      $('#cit_popup_modal_OK').off('click');
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
      
    });
    
    
      
    }; // kraj return funkcije
    
    
  }; // kraj save customer funkcije
  this_module.save_customer = save_customer;
  
  function form_to_data() {
    
    $.each( this_module.model_to_input_map, function( data_key, input_id ) {
      
      
      var is_switch = false;
      if ( data_key == 'pdv' || data_key == 'gdpr' || data_key == 'obrtnik' ) {
        set_switch_data(this_module, '#'+input_id, data_key);
        is_switch = true;
      };
      
      // ako nije switch onda uzmi podatak iy inputa i spremi ga u property
      if ( is_switch == false ) this_module.cit_data[data_key] = $('#' + input_id ).val();
      
    });
    
  };
  this_module.form_to_data = form_to_data;
  

  
  function data_to_form(data) {
    
    $('.pp_vip_row').remove();
    $('.car_plate_row').remove();
    $('.cit_tag_pill').remove();
    
    fill_user_metadata(data, '#cit_customer_user_saved', '#cit_customer_user_edited', '#cit_customer_last_login');
    
    $.each( data, function( data_key, data_value ) {
      
      if ( data_key == 'customer_acquisition' && data_value !== null ) { 
        $('#' + data_key ).val(data_value.naziv);  
      };
      
      
      if ( data_key == 'customer_vip' && data_value !== null && $.isArray(this_module.cit_data.customer_vip) ) { 
        
        $.each( this_module.cit_data.customer_vip, function( pp_vip_index, park_place_object ) {
          
          var row_html = pp_for_vip_html(park_place_object); 
          $('#pp_vip_box').append(row_html);
          
          setTimeout(function() {
            $('.cit_tooltip').tooltip();
            $(`#delete_pp_vip_${park_place_object._id}`).on( 'click', this_module.delete_pp_vip );
          }, 100);
          
        });

      };
      
      if ( data_key == 'car_plates' && data_value !== null && $.isArray(this_module.cit_data.car_plates) ) { 
        
        $.each( this_module.cit_data.car_plates, function( car_plate_index, car_plate ) {
          
          var row_html = this_module.car_plate_html(car_plate.reg);
          $('#car_plate_box').append(row_html);
      
          setTimeout(function() {
            $('.cit_tooltip').tooltip();
            $(`#delete_car_plate_${car_plate.reg}`).on( 'click', this_module.delete_car_plate );
          }, 100);
          
        });

      };
      
      if ( data_key == 'customer_tags' && data_value !== null && $.isArray(this_module.cit_data.customer_tags) ) { 
        
        $.each( this_module.cit_data.customer_tags, function( tag_index, tag ) {
          
          var tag_html = customer_tag_pill_html(tag);
          $('#customer_tags_box').append(tag_html);

          setTimeout(function() {
            $('.cit_tooltip').tooltip();
            $(`#delete_customer_tag_${tag.sifra}`).on( 'click', this_module.delete_customer_tag );
          }, 100);
          
        });

      };
      
    });
    
    $.each( this_module.model_to_input_map, function( data_key, input_id ) {
      
      var is_switch = false;
      // ako je ovo propery tj element id koji ima class cit_switch
      if ( $('#'+input_id).hasClass('cit_switch') ) {
        set_switch_html(this_module, '#'+input_id, data_key);
        is_switch = true;
      };
      
      // ako nije switch onda je input i zato upiši value iz cit data objekta !!!!
      if ( is_switch == false ) $('#' + input_id ).val(this_module.cit_data[data_key]);
      
    });
    
    setTimeout(function() {
      this_module.get_customer_park_actions(); 
    }, 100);
    
    setTimeout(function() {
      this_module.get_customer_products(); 
    }, 500);    
    
    
  };
  this_module.data_to_form = data_to_form;
  
  
  this_module.customer_actions_page = 1;

  function get_customer_park_actions() {
      
    
    if ( !this_module.cit_data.user_number ) return;
    
    var action_search_object = {
      limit: 50,
      page: this_module.customer_actions_page,
    };
    
      
    
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: 'POST',
      url: ("/get_customer_park_actions/" + this_module.cit_data.user_number),
      data: JSON.stringify(action_search_object),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {
      
      console.log(response);
      
      if ( response.length > 0 ) {
        
        console.log('poslje ajaxa actions');  
        console.log(response);
        
        
        this_module.current_action_list = response;

        this_module.create_action_table(response);
        
      } else {
        
        if ( this_module.customer_actions_page > 1 ) this_module.customer_actions_page -= 1;
        
      };
    })
    .fail(function(error) {
      console.log(error);
      popup_text = 'GREŠKA PRI UČITAVANJU PARK AKCIJA KORISNIKA!';
      show_popup_modal('error', popup_text, null, 'ok' );
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
        
    });
      
    
    
    
  }
  this_module.get_customer_park_actions = get_customer_park_actions;
  
  
  function create_action_row(pp_action) {
    
    
    
    var park_name = find_pp_name(pp_action.pp_sifra);

    
    var book_duration_string = '';
    var park_duration_string = '';

    if ( pp_action.book_duration ) book_duration_string = get_time_as_hh_mm_ss( pp_action.book_duration );
    if ( pp_action.duration ) park_duration_string = get_time_as_hh_mm_ss( pp_action.duration );


    var action_row_html = 
`
<div class="cit_big_table_row" id="pp_action_row_${pp_action._id}">
  
  <div class="big_table_col pp_col">${park_name}</div>

  <div class="big_table_col pp_reservation_start_col">
    ${ pp_action.reservation_from_time ? getMeADateAndTime(pp_action.reservation_from_time).datum : '' }&nbsp;
    ${ pp_action.reservation_from_time ? getMeADateAndTime(pp_action.reservation_from_time).vrijeme : '' }
  </div>
  <div class="big_table_col pp_reservation_duration_col">
    ${ book_duration_string }
  </div>
  <div class="big_table_col pp_reservation_paid_col">
    ${ pp_action.reservation_paid ? zaokruziIformatirajIznos(pp_action.reservation_paid) : '' }
  </div>

  <div class="big_table_col pp_start_time_col">
    ${ pp_action.from_time ? getMeADateAndTime(pp_action.from_time).datum : '' }&nbsp;
    ${ pp_action.from_time ? getMeADateAndTime(pp_action.from_time).vrijeme : '' }
  </div>
  <div class="big_table_col pp_end_time_col">
    ${ pp_action.to_time ? getMeADateAndTime(pp_action.to_time).datum : '' }&nbsp;
    ${ pp_action.to_time ? getMeADateAndTime(pp_action.to_time).vrijeme : '' }
  </div>
  <div class="big_table_col pp_duration_time_col">
    ${ park_duration_string }
  </div>
  <div class="big_table_col pp_paid_col">
    ${ pp_action.paid ? zaokruziIformatirajIznos(pp_action.paid) : '' }
  </div>

</div>

`;
    return action_row_html;
    
  };
  this_module.create_action_row = create_action_row;
  
  
  function create_action_table(all_actions) {
    
    if ( !all_actions || all_actions.length == 0 ) return;
    
    // makni sve redove tablice !!!
    $('#pp_actions_box .cit_big_table_row').remove();
    
    
    
    $('#pp_actions_navigation .cit_big_table_page_num').text( this_module.customer_actions_page );
    
    var all_action_rows = '';
    
    $.each( all_actions, function( pp_action_index, pp_action ) {
      var action_row = create_action_row(pp_action);
      all_action_rows += action_row;
    });
    
    $('#pp_actions_box').append(all_action_rows);
    
  }
  this_module.create_action_table = create_action_table;
  
  
  this_module.customer_products_page = 1;
  
  function get_customer_products() {
      
    
    if ( !this_module.cit_data.user_number ) return;
    
    var product_search_object = {
      limit: 50,
      page: this_module.customer_products_page,
    };
    
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: 'POST',
      url: ("/get_customer_products_for_table/" + this_module.cit_data.user_number),
      data: JSON.stringify(product_search_object),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {
      
      console.log(response);
      
      if ( response.length > 0 ) {
        
        console.log('poslje ajaxa actions');  
        console.log(response);
        
        
        this_module.current_product_list = response;

        this_module.create_product_table(response);
        
      } else {
        
        if ( this_module.customer_products_page > 1 ) this_module.customer_products_page -= 1;
        
      };
    })
    .fail(function(error) {
      console.log(error);
      popup_text = 'GREŠKA PRI UČITAVANJU KORISNIČKIH PAKETA!';
      show_popup_modal('error', popup_text, null, 'ok' );
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
        
    });
      
    
    
    
  };
  this_module.get_customer_products = get_customer_products;
  
  
  function create_product_row(cus_product) {
    
    
    
    var product_image = 
`<img class="cit_row_badge cit_tooltip" src="/img/voucher_badge.png" />`;
    
    var voucher_code = 
`<div class="voucher_code cit_tooltip" data-toggle="tooltip" data-placement="bottom" title="${cus_product.code}">${cus_product.code}</div>`;
    
    if ( cus_product.type == 'monthly' ) {
      product_image = `<img class="cit_row_badge cit_tooltip" src="/img/monthly_badge.png" />`;
      voucher_code = '';
    };
    
    var product_row_html = 
`
<div class="cit_big_table_row" id="cus_product_row_${cus_product._id}">
  
  <div class="big_table_col icon_col">
    ${product_image}
    ${voucher_code}
  </div>

  <div class="big_table_col desc_col">
    ${ cus_product.desc || '' }&nbsp;
  </div>
  <div class="big_table_col amount_col">
    ${ cus_product.amount ? zaokruziIformatirajIznos(cus_product.amount) : '' }
  </div>
  <div class="big_table_col price_col">
    ${ cus_product.price ? zaokruziIformatirajIznos(cus_product.price, 0) : '0' } 
    ( ${ cus_product.price ? zaokruziIformatirajIznos( (cus_product.price/cus_product.amount * 100), 0 ) : '0' }% )
  </div>

  <div class="big_table_col buy_time_col">
    ${ cus_product.buy_time ? getMeADateAndTime(cus_product.buy_time).datum : '' }&nbsp;
    ${ cus_product.buy_time ? getMeADateAndTime(cus_product.buy_time).vrijeme : '' }
  </div>
  <div class="big_table_col valid_from_col">
    ${ cus_product.valid_from ? getMeADateAndTime(cus_product.valid_from).datum : '' }&nbsp;
    ${ cus_product.valid_from ? getMeADateAndTime(cus_product.valid_from).vrijeme : '' }
  </div>
  <div class="big_table_col valid_to_col">
    ${ cus_product.valid_to ? getMeADateAndTime(cus_product.valid_to).datum : '' }&nbsp;
    ${ cus_product.valid_to ? getMeADateAndTime(cus_product.valid_to).vrijeme : '' }
  </div>
  <div class="big_table_col balance_col">
    ${ cus_product.balance ? zaokruziIformatirajIznos(cus_product.balance) : '' }
  </div>

</div>

`;
    return product_row_html;
    
  };
  this_module.create_product_row = create_product_row;
    
  
  function create_product_table(all_products) {
    
    if ( !all_products || all_products.length == 0 ) return;
    
    $('#customer_products_box .cit_big_table_row').remove();
    
    
    $('#customer_products_navigation .cit_big_table_page_num').text( this_module.customer_products_page );
    
    var all_products_rows = '';
    
    $.each( all_products, function( product_index, product ) {
      var product_row = create_product_row(product);
      all_products_rows += product_row;
    });
    
    $('#customer_products_box').append(all_products_rows);
    
    setTimeout( function() { $('.cit_tooltip').tooltip(); }, 300);
    
    
  }
  this_module.create_product_table = create_product_table;
  
  
  
  
  function site_monthly_list(user_products) {
    
    var monthly_list = '';
    var monthly_subs = user_products.monthly_arr;
    
    
    monthly_subs.sort( window.by('valid_from', false, Number ) );
    
    
    // BITNO !!!
    // može se dogoditi i zapravo će se često događati
    // da user plati da bi produžio pretplatu u trenutnku dok mu postojeća pretplata još uvijek vrijedi !!!
    // u tom slučaju će se u usr products pojaviti dvije pretplate za isti parking
    // razlika u pretplatama će biti što ova novija ima duži period tj valid to propery je veći
    // kada se to dogodi ja zapravo prepišem staru pretplatu sa ovom dužom novijom pretplatom
    
    // razlog zašto duplam pretplate je taj što želim evidentirati plaćanje i uzimanje pretplate u customer products !!!
    // ali ne želim da user vidi na ekranu više pretplata za isti parking !!!!
    
    var monthly_subs_bez_ponavljanja = [];
    
    $.each( monthly_subs, function( monthly_index, monthly ) {
      
      var prepisao_postojeci_monthly = false;
      $.each( monthly_subs_bez_ponavljanja, function( unique_month_index, unique_month ) {
        // ako je sljedeći monthly isti kao neki koji sam već pronašao tj ako je za isti parking
        // onda provjeri jel taj sljedeći ima valid to timestamp veći od već spremljenog
        // ako ima onda prepiši već spremljeni sa ovim novim
        if (
          unique_month.pp_sifra == monthly.pp_sifra &&
          unique_month.valid_to < monthly.valid_to
          ) {
          
          var tag_u_unique_pretplati = unique_month.tag ? unique_month.tag : false;
          var novi_month_tag = monthly.tag ? monthly.tag : false;
          
          
          // ako su obje mjesečne pretplate sa tagom null tj ne postoje  tj  nisu TELE2 ili SIGNIFY
          if ( tag_u_unique_pretplati == false && novi_month_tag == false ) { 
            
            monthly_subs_bez_ponavljanja[unique_month_index] = monthly;
            prepisao_postojeci_monthly = true;
            
          } else {
            // ako obe mjesečne pretplate imaju isti tag
            if ( unique_month.tag == monthly.tag ) {
              monthly_subs_bez_ponavljanja[unique_month_index] = monthly;
              prepisao_postojeci_monthly = true;
            };
            
          };
         
        };
        
      }); // loop po unique mjesecnim pretplatama
      
      // ako nisam našao već postojeći monthly product onda ga dodaj kao novi product
      if ( prepisao_postojeci_monthly == false ) monthly_subs_bez_ponavljanja.push(monthly);
      
    }); // loop po mjesecnim pretplatama
    
    
    $.each( monthly_subs_bez_ponavljanja, function( monthly_index, monthly ) {
      
      var park_name = '';
      $.each( window.all_pp_all_data, function( park_index, park_obj ) {
        if ( park_obj.pp_sifra == monthly.pp_sifra ) park_name = park_obj.pp_name;
      });

      var duration_html = '';
      var monthly_valid_to = `<span>${ window.we_trans( 'cl__monthly_valid_to span', window.we_lang ) }</span>`;
      
      if ( monthly.type == 'vip' ) {
        // duration_html = `<img class="we_inf_icon" src="img/infinity_icon.png" />`;
        duration_html = `VIP`;
        
      } else {
      
        var time_now = Date.now();
        var time_left = monthly.valid_to - time_now;

        var time_unit = 'd';
        var min_left = Math.floor( time_left / ( 1000*60 ) );
        var hours_left = Math.floor( time_left / ( 1000*60*60 ) );
        var days_left = Math.floor( time_left / ( 1000*60*60*24 ) );

        if ( days_left == 0 ) { days_left = hours_left; time_unit = 'h'};
        if ( hours_left == 0 ) { days_left = min_left; time_unit = 'm'};

      
        duration_html = `${days_left}<span>${time_unit}</span>`;
        monthly_valid_to = `exp: ${getMeADateAndTime(monthly.valid_to).datum}`;

      };
        
      
      var monthly_row = 
          `
          <div class="site_monthly_product" id="${monthly._id}">
            
            <div class="monthly_pp_icon_box">
              ${duration_html}
            </div>

            <div class="pp_name_and_valid_to_box">
              <div class="park_name">${park_name}</div>
              <div class="monthly_valid_to">
                ${monthly_valid_to}
              </div>
            </div>
          </div> 

          `;
      
      monthly_list += monthly_row;
      
    });
    
    
    $('#monthly_products_box').html(monthly_list);
    
  };
  this_module.site_monthly_list = site_monthly_list;

  
  function site_user_balance(user_products) {
    
    $('#customer_balance_amount').text( zaokruziIformatirajIznos(user_products.balance_sum || 0) );
    
  };
  this_module.site_user_balance = site_user_balance;
  
  
  function site_card_num_list(user_products) {
    
    if ( user_products.cards && user_products.cards.length > 0 ) {
      
      var all_card_nums = "";
      $.each( user_products.cards, function( card_num_index, card ) {
      
        var card_num_row = `<div class="card_item" id="${card.sign}">${card.num}</div>`;
      
        all_card_nums += card_num_row;
      });
       $('#customer_credit_card_list').html(all_card_nums);

    } else {
      // ako nema niti jednu karticu
      $('#customer_credit_card_list').html(`<div class="we_drop_list_item" id="${'no_cards'}">${'NEMA KARTICA'}</div>`);  
    };
    
  };
  this_module.site_card_num_list = site_card_num_list;
    
  
  
  
  this_module.cit_loaded = true;
 
}
  
  
};