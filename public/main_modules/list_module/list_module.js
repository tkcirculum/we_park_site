
var module_object = {
  create: function( data, parent_element, placement ) {
    
    
    var list_item_mod = window['/main_modules/list_module/list_item_module/list_item_module.js'];
    
    var component_id = data.list_id;;
    
    var list_html = '';

    $.each(data.list, function(item_ind, item) {    
      var item_data = {parent_id: component_id, item: item };
      list_html +=  list_item_mod.create(item_data).html; 
    });
    
    
  var component_html = 
`
<div class="we_list" id="we_list_${component_id}">
  ${list_html}
</div>
`;
  
wait_for( `$('#we_list_${component_id}').length > 0`, function() {

  var this_module = window[module_url]; 
  
  $(`#we_list_${component_id}`).data('we_data', {
    desc: `data za testnu listu unutar idija #we_list_${component_id}`,
    data: data
  });  

  $('.cit_tooltip').tooltip();
  
}, 5000000);
    

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    }

  },
 
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
 
  
  // OVDJE STAVITI SVE FUNKCIJE !!!!
  
  
  this_module.cit_loaded = true;
  
  
}
  
  
};