
var module_object = {
  create: function( item_data, parent_element, placement ) {
    
  var this_module = window[module_url];

  var { id, first_name, last_name, email, gender, avatar } = item_data.item;
    
/*
EXAMPLE:
    "id": 3,
    "first_name": "Cristie",
    "last_name": "Stemp",
    "email": "cstemp2@fastcompany.com",
    "gender": "Female",
    "avatar": "https://robohash.org/sapientequiconsequatur.jpg?size=50x50&set=set1"
*/

    var component_id = id;
    
    var component_html =     
`
<div class="list_item" id="list_item_${component_id}">
  <div class="avatar"><img src="${avatar}" /></div>
  <div class="first_name">${first_name}</div>
  <div class="last_name">${last_name}</div>
  <div class="email">${email}</div>
  <div class="gender">${gender}</div>
</div>

`;
    
  // ovo je arbitrarni uvijet  - kada se u html-u pojavi ovaj element ....
  wait_for( `$('#list_item_${component_id}').length > 0`, function() {
    
    var this_module = window[module_url]; 
    
    $(`#list_item_${component_id}`).data('we_data', {
      desc: `data za item id: #list_item_${component_id} unutar testne liste id: #we_list_${item_data.parent_id}`,
      data: item_data
    });  
    

    $(`#list_item_${component_id} .avatar img`).off('click');
    $(`#list_item_${component_id} .avatar img`).on('click', function() {
      
      alert($(this).attr('src') );
      
    });
    
  }, 500000); 
   
    setTimeout(function() { 
      $('.cit_tooltip').tooltip();
    }, 20);

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    }

  },
 
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
 
  
  // OVDJE STAVITI SVE FUNKCIJE !!!!
  
  
  this_module.cit_loaded = true;
  
  
}
  
  
};