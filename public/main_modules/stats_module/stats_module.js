var module_object = {
  create: function( data, parent_element, placement ) {
    
    var this_module = window[module_url]; 
    
    wait_for('true == true', function() {
      
    var component_id = null;
      
      
    var component_html =
    `
<div id="stats_component" 
     class="main_module_component container-fluid"
     style="float: left; height: 100%;">
 
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h6 id="stats_big_label" style="color: #302f2f;">Statistika:</h6>
        </div>
      </div>

      <!-- START -------------------- LABEL I NAVIGACIJA i FILTER -->
        
      <div class="row" style="margin-top: 30px;">
        
        <div class="col-md-3 col-sm-12">
          <div id="stats_izaberi_park_label" class="cit_label">Izaberi parking:</div>
          <input id="stats_izaberi_park" type="text" class="cit_input we_auto simple" autocomplete="off"> 
        </div>         
        
        <div class="col-md-2 col-sm-12">
          <div id="stats_from_date_label" class="cit_label">Datum OD:</div>
          <input id="stats_from_date" type="text" class="cit_input" autocomplete="off">          
        </div>  

        <div class="col-md-2 col-sm-12">
          <div id="stats_to_date_label" class="cit_label">Datum DO:</div>
          <input id="stats_to_date" type="text" class="cit_input" autocomplete="off">          
        </div>
        
        
        <div class="col-md-4 col-sm-4">

          <div id="stats_choose_user_label" class="cit_label">USER:</div>
          <input autocomplete="off" id="stats_choose_user" type="text" class="cit_input we_auto simple">
          
        </div>        
        
        
      </div>    
      
      <div class="row">
        
        <div class="col-md-3 col-sm-12">
          <div id="stats_book_or_park_label" class="cit_label">VRSTA AKCIJE</div>
          <input id="stats_book_or_park" type="text" class="cit_input we_auto simple" autocomplete="off"> 
        </div>

        <div class="col-md-2 col-sm-12">
          <div id="stats_from_time_label" class="cit_label">Od vremena / hh:mm:ss</div>
          <input id="stats_from_time" type="text" class="cit_input" autocomplete="off">
        </div>

        <div class="col-md-2 col-sm-12">
          <div id="stats_to_time_label" class="cit_label">Do vremena / hh:mm:ss</div>
          <input id="stats_to_time" type="text" class="cit_input" autocomplete="off">
        </div> 
        
        <div class="col-md-3 col-sm-12">
          <div id="stats_time_grupa_label" class="cit_label">GRUPIRAJ PO:</div>
          <input id="stats_time_grupa" type="text" class="cit_input we_auto same_width" autocomplete="off"> 
        </div>
        
        <div class="col-md-2 col-sm-12">
          
          <div class="cit_switch_wrapper">
            <div id="gantt_graf_label" class="cit_label">GANTT GRAF</div>
            <div id="gantt_graf" class="cit_switch on">
                  <div class="switch_circle"></div>
                  <div class="on_text switch_text">DA</div>
                  <div class="off_text switch_text">NE</div>
            </div>
          </div>
          
        </div> 

      </div>
  
  <div class="row">
  
    <div class="col-md-2 col-sm-12">
      <div id="stats_user_tag_label" class="cit_label">Odaberi tagove:</div>
      <input id="stats_user_tag" type="text" class="cit_input we_auto simple" autocomplete="off"> 
    </div>
    
    <div class="col-md-8 col-sm-8" id="stats_tag_container">
      <div id="stats_tag_container_label" class="cit_label">Odabrani tagovi:</div>
      
<!--      
      <div class="cit_tag_pill" style="margin-top: 0;">TELE 2<i class="fas fa-times"></i></div>
      <div class="cit_tag_pill" style="margin-top: 0;">SIGNUM<i class="fas fa-times"></i></div>
      <div class="cit_tag_pill" style="margin-top: 0;">TVRTKA 001<i class="fas fa-times"></i></div>
-->
      
    </div>

    
  </div>
  
    <div class="row" style="display: flex;">
      <div class="col-md-10 col-sm-12">
        
        <div id="stats_trazi_btn" 
             class="cit_button cit_center cit_tooltip" 
             style="width: 220px; float: none; margin: 20px auto;"
             data-toggle="tooltip" data-placement="bottom"
             title="POKRENI PRETRAGU">

          <i class="fas fa-search" style="margin-right: 10px;"></i>POKAŽI REZULTATE

        </div>

      </div>
      <div class="col-md-2 col-sm-12" style="display: flex; align-items: center; justify-content: flex-end;">
        <div id="create_excel_btn" 
             class="cit_button cit_center cit_tooltip" 
             style="width: 30px; float: right; margin-right: 20px; background-color: forestgreen; color: #fff;"
             data-toggle="tooltip" data-placement="bottom"
             title="EXPORT EXCEL">

          <i class="fas fa-file-excel"></i>

        </div>
        
      </div>
    </div>  


      <!-- END -------------------- LABEL I NAVIGACIJA i FILTER -->
      

  <div class="row">
  
    
    <div class="col-md-10 col-sm-12" id="stats_od_find_col" 
    style=" font-size: 14px;
            display: flex;
            align-items: center;
            justify-content: center;">
      
      <!-- OVDJE IDE SUMA REZULTATA -->
      &nbsp;
      
    </div>
    
    <div class="col-md-2 col-sm-12">
      <div class="cit_big_table_navigation" style="height: 100%;">
        <div class="cit_big_table_previous" id="stats_previous_arrow"><i class="fas fa-caret-left"></i></div>
        <div class="cit_big_table_page_num" id="stats_page_num">1</div>
        <div class="cit_big_table_next" id="stats_next_arrow"><i class="fas fa-caret-right"></i></div>
      </div>
    </div>  
  
  </div>
  
  

  <div class="row" style="margin-top: 30px;" id="big_stats_table_row">


    <div class="col-md-12 col-sm-12" id="big_stats_table_box" style="padding: 0;">

<!--

OVO ĆU KORISTITI KASNIJE ZA FINANCIJSKI REPORT !!!!!

      <div class="cit_big_table_header_row">
        <div class="big_table_col stats_grupa_col">GRUPA <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
        <div class="big_table_col stats_donos_col">DONOS <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
        <div class="big_table_col stats_uplaceno_col">UPLAĆENO <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
        <div class="big_table_col stats_potroseno_col">POTROŠENO <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
        <div class="big_table_col stats_saldo_col">SALDO <i class="fas fa-caret-up"></i><i class="fas fa-caret-down"></i></div>
      </div>

      <div class="cit_big_table_row">
        <div class="big_table_col stats_donos_col">VOUCHER</div>
        <div class="big_table_col stats_donos_col">2.545,40</div>
        <div class="big_table_col stats_uplaceno_col">3.057,32</div>
        <div class="big_table_col stats_potroseno_col">2.057,32</div>
        <div class="big_table_col stats_saldo_col">1.483,66</div>
      </div>

      <div class="cit_big_table_row">
        <div class="big_table_col stats_donos_col">MJESEČNA KARTA</div>
        <div class="big_table_col stats_donos_col">5.545,40</div>
        <div class="big_table_col stats_uplaceno_col">3.057,32</div>
        <div class="big_table_col stats_potroseno_col">9.057,32</div>
        <div class="big_table_col stats_saldo_col" style="color: red;"> -1.483,66</div>
      </div>        
-->

    </div>

  </div>


      
</div> 
<!--END OF MAIN COMPONENT DIV-->
`;
    
      
  // ovo je arbitrarni uvijet  - uzeo sam prvi label u formi kao uvijet kad se pojavi ....
  wait_for( `$('#stats_izaberi_park').length > 0`, function() {

    var this_module = window[module_url]; 
    
    $('.cit_tooltip').tooltip();
    

    $('#stats_izaberi_park').data('we_auto', {
      desc: 'za odabir parka u stats',
      local: false,
      url: '/search_park_places',
      return: { pp_sifra: 1, pp_name: 1, pp_address: 1, _id: 1, pp_current_price: 1 },
      hide_cols: ['_id', 'pp_current_price'],
      query: {},
      show_on_click: true,
      // parent: '#big_stats_table_box',
      // list: 'proba_find',
    });
    
    
    $('#stats_user_tag').data('we_auto', {
      desc: 'za odabir customer tag u stats',
      local: true,
      return: {},
      show_on_click: true,
      list: 'user_tags',
    });
    
    
    $('#stats_book_or_park').data('we_auto', {
      desc: 'za odabir book or park u stats',
      local: true,
      return: {},
      show_on_click: true,
      list: 'stats_book_or_park',
    });
    
    
    $('#stats_time_grupa').data('we_auto', {
      desc: 'odabir grupiranja sat dan tjedan ili mjesec',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'time_grupe',
    });


        
    $('#stats_choose_user').data('we_auto', {
      desc: 'za odabir usera/customera u stats',
      local: false,
      url: '/stats_search_customers',
      find_in: ['email', 'name', 'user_address', 'user_tel' ],
      col_widths: [25, 35, 25, 15],
      return: { user_number: 1, email: 1, name: 1, user_tel: 1, visited_pp: 1 },
      hide_cols: ['_id', 'visited_pp'],
      query: {},
      show_on_click: false,
      list_width: 700,
      
    });
    
    
        
    $('#stats_trazi_btn').off('click', this_module.stats_trazi);
    $('#stats_trazi_btn').on('click', this_module.stats_trazi);
    
    
    $('#create_excel_btn').off('click', this_module.create_excel);
    $('#create_excel_btn').on('click', this_module.create_excel);
    
    
    
    
    $('#stats_previous_arrow').off('click', this_module.stats_previous_page);
    $('#stats_previous_arrow').on('click', this_module.stats_previous_page);
    
    
    
    $('#stats_next_arrow').off('click', this_module.stats_next_page);
    $('#stats_next_arrow').on('click', this_module.stats_next_page);
    
   
  }, 5000000 );      

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
      
      setTimeout( function() {
        window.cit_main_area_perfectbar.update();
      }, 300);
      // this_module.initMap();
    }; 
    
    return {
      html: component_html,
      id: component_id
    };
      
      
  }, 500000 );         

  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  
  var this_module = window[module_url]; 
  var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;
  
      
  function get_empty_cit_data() {
 
    var time_now = Date.now();
    
    this_module.cit_data = {

      from_time: get_DATE_as_milisec(null).ms_at_zero,
      to_time: get_DATE_as_milisec(null, 1).ms_at_zero,
      paid: null,
      pp_sifra : null,
      user_number: null,
      was_reserved_id: null,
      mobile_from_time: null,

      user_set_book_duration: null,

      book_duration: null,

      reservation_from_time: null,
      reservation_to_time: null,
      reservation_expired: null,
      reservation_canceled: null,
      reservation_paid: null,

      current_price: null,

      pack_type: null,
      duration: null,
      revenue: null,
      owner_revenue: null,
      currency: null,

      indoor: null,

      current_tag: null,

      user_status: null,
      
      reg: null,
      
      // ovo nije dio action objekta u bazi nego je za način prikazivanja podataka iz koje podatke uzimam !!!
      stats_time_grupa: 'SAT',
      gantt: true,
      book_or_park: 'BOTH',
      page_num: 1,
      
    };
    
    
    // --------------- automatski popunjavam polja u stats sa default vrijednostima !!!!
    // prvo pričekaj da se pojavi prvi input u stats formi !!!
    wait_for(`$('#stats_izaberi_park').length > 0`, function() {
      
      var date_now = create_ms_at_zero_and_week_day(0);
      var date_sutra = create_ms_at_zero_and_week_day(1);
      
      $('#stats_from_date').val( getMeADateAndTime(date_now.ms_at_zero, 'd.m.y').datum );
      $('#stats_to_date').val( getMeADateAndTime(date_sutra.ms_at_zero, 'd.m.y').datum );
      
      $('#stats_from_time').val( '00:00:00' );
      $('#stats_to_time').val( '00:00:00' );
      
      
      $('#stats_book_or_park').val( "REZERVACIJE i PARKING" );
      
      $('#stats_time_grupa').val( "PO SATIMA" );
      
      
    }, 5000000);
    
    
    
    
  };
  this_module.get_empty_cit_data = get_empty_cit_data;
  
  
  function select_stats_user(stats_user) {

    $('#'+current_input_id).val( stats_user.email );
    this_module.cit_data.user_number = stats_user.user_number;
  };
  this_module.select_stats_user = select_stats_user;
  
  
  function create_gantt(actions) {
    
    var stats_from_date = $('#stats_from_date');
    var stats_to_date = $('#stats_to_date');
    var stats_from_time = $('#stats_from_time');
    var stats_to_time = $('#stats_to_time');
    
    var day_start = get_DATE_as_milisec(stats_from_date).ms_at_zero;
    var day_end = get_DATE_as_milisec(stats_from_date, 1).ms_at_zero; 
    
    var all_bars = '';
    
    var div_height = 0;
    
    $.each( actions, function(action_index, action) {
      
      var has_book = false;
      var has_park = false;
      
      if ( action.reservation_from_time ) has_book = true;
      if ( action.from_time ) has_park = true;
      
      if ( has_book && action.book_duration_c ) {

        var start_book_time = action.reservation_from_time_c - day_start;
        var start_book_time_perc = start_book_time / (1000*60*60*24) * 100;
        var book_width_perc = action.book_duration_c / (1000*60*60*24) * 100;
        
        var dodatne_classes_od_akcije = '';
        var gantt_finished = '';
        
        
        // dodaj strelicu ako interval počinje prije starta
        if ( action.reservation_from_time < day_start ) dodatne_classes_od_akcije += ' start_before';
        
        // dodaj strelicu ako interval završava poslje enda
        if ( action.reservation_to_time > day_end ) dodatne_classes_od_akcije += ' end_after';
        
        
        // ako ima book duration to znači da je booking završen
        if ( action.book_finished ) gantt_finished = `<div class="gantt_finished"><i class="fas fa-sign-out-alt"></i></div>`;
          
        
        var pocetak_bar_time = 0;
        // ako je pocetak unutar intervala
        if ( action.reservation_from_time >= day_start ) {
          // POZITIVAN BROJ SATI UNUTAR INTERVALA
          pocetak_bar_time = get_time_as_hh_mm_ss(action.reservation_from_time - day_start);
            
        } else {
          // ako je pocetak prije intervala
          // onda upiši točan datum  i vrijeme
          pocetak_bar_time = getMeADateAndTime(action.reservation_from_time).datum + ' ' +  getMeADateAndTime(action.reservation_from_time).vrijeme;  
        };
        
        
        var kraj_bar_time = 'JOŠ TRAJE';
        
        if ( action.book_finished ) {

          if ( day_end >= action.reservation_to_time ) {
            kraj_bar_time = get_time_as_hh_mm_ss(action.reservation_to_time - day_start);
          } else {
            kraj_bar_time = getMeADateAndTime(action.reservation_to_time).datum + ' ' +  getMeADateAndTime(action.reservation_to_time).vrijeme;  
          };
          
        };
        
        var interval_text = 
`
${action.user_email}
<br>
PARK: ${find_pp_name(action.pp_sifra)}
<br>
OD: ${pocetak_bar_time}
<br>
DO: ${kraj_bar_time}

`;          
            
        
        var samo_ime_maila = action.user_email.split('@')[0];
        
      var bar_html =
`
<div  class="cit_tooltip gantt_bar gantt_book${dodatne_classes_od_akcije}" 
      data-toggle="tooltip" data-placement="top" data-html="true"
      title="${interval_text}"
style="top: ${action_index*30 + 20}px; left: ${start_book_time_perc}%; width: ${book_width_perc}%;">
<span>${samo_ime_maila + ' ' + action.user_number }</span>
${gantt_finished}
</div>
`;        
        all_bars += bar_html;
        div_height+= 30;
        
      }; // kraj has book
      
      if ( has_park && action.duration_c ) {
        
        if ( action.user_email == 'kruno.komorcec@tele2.com' ) {
          console.log('kruno.komorcec@tele2.com mail');
        };
        
        
        var start_park_time = action.from_time_c - day_start;
        var start_park_time_perc = start_park_time / (1000*60*60*24) * 100;
        var park_width_perc = ( action.duration_c / (1000*60*60*24) * 100) + '%';
        
        var dodatne_classes_od_akcije = '';
        var gantt_finished = '';
        
        
        // dodaj strelicu ako interval počinje prije starta
        if ( action.from_time < day_start ) dodatne_classes_od_akcije += ' start_before';
        
        // dodaj strelicu ako interval završava poslje enda
        if ( action.to_time > day_end ) dodatne_classes_od_akcije += ' end_after';
        
      
        if ( action.park_finished ) gantt_finished = `<div class="gantt_finished"><i class="fas fa-sign-out-alt"></i></div>`;
        
        var pocetak_bar_time = 0;
        
        if ( action.from_time >= day_start  ) {
          // POZITIVAN BROJ SATI UNUTAR INTERVALA
          pocetak_bar_time = get_time_as_hh_mm_ss(action.from_time - day_start);
        } else {
          // ako je pocetak prije intervala
          // onda upiši točan datum  i vrijeme
          pocetak_bar_time = getMeADateAndTime(action.from_time).datum + ' ' +  getMeADateAndTime(action.from_time).vrijeme;  
        }
        
        
        var kraj_bar_time = 'JOŠ TRAJE';
        
        
        if ( action.park_finished ) {
          // ako je kraj parkinga unutar intervala
          if ( day_end >= action.to_time ) {
            kraj_bar_time = get_time_as_hh_mm_ss(action.to_time - day_start);
          } else {
            kraj_bar_time = getMeADateAndTime(action.to_time).datum + ' ' +  getMeADateAndTime(action.to_time).vrijeme;  
          };
        };
        
        var interval_text = 
`
${action.user_email}
<br>
PARK: ${find_pp_name(action.pp_sifra)}
<br>
OD: ${pocetak_bar_time}
<br>
DO: ${kraj_bar_time}

`;          
        
        
        
        var samo_ime_maila = action.user_email.split('@')[0];
        
      var bar_html =
`
<div  class="cit_tooltip gantt_bar gantt_park${dodatne_classes_od_akcije}" 
      data-toggle="tooltip" data-placement="top" data-html="true"
      title="${interval_text}"
style="top: ${action_index*30 + 20}px; left: ${start_park_time_perc}%; width: ${park_width_perc};">
<span>${samo_ime_maila + ' ' + action.user_number }</span>
${gantt_finished}
</div>
`;        
        all_bars += bar_html;
        div_height+= 30;
        
      }; // kraj has book
      
      
    });
    
    var gantt_main_div = 
`
<!-- AKO JE HEIGHT VEĆI OD 600 ONDA ĆE SE POJAVITI SCROLL BAR SA STRANE PA ZATO MORAM ŠIRINU SMANJITI ZA 16px -->
<div id="gantt_main_header" style="width: ${ div_height < 600 ? '100%' : 'calc(100% - 16px)' };">
  <div class="hour_col">
    <div class="hour_num" style="margin-left: -15px; right: unset; left: 0;">00</div>
    <div class="hour_num">01</div>
  </div>
  <div class="hour_col"><div class="hour_num">02</div></div>
  <div class="hour_col"><div class="hour_num">03</div></div>
  <div class="hour_col"><div class="hour_num">04</div></div>
  <div class="hour_col"><div class="hour_num">05</div></div>
  <div class="hour_col"><div class="hour_num">06</div></div>
  <div class="hour_col"><div class="hour_num">07</div></div>
  <div class="hour_col"><div class="hour_num">08</div></div>
  <div class="hour_col"><div class="hour_num">09</div></div>
  <div class="hour_col"><div class="hour_num">10</div></div>
  <div class="hour_col"><div class="hour_num">11</div></div>
  <div class="hour_col"><div class="hour_num">12</div></div>
  <div class="hour_col"><div class="hour_num">13</div></div>
  <div class="hour_col"><div class="hour_num">14</div></div>
  <div class="hour_col"><div class="hour_num">15</div></div>
  <div class="hour_col"><div class="hour_num">16</div></div>
  <div class="hour_col"><div class="hour_num">17</div></div>
  <div class="hour_col"><div class="hour_num">18</div></div>
  <div class="hour_col"><div class="hour_num">19</div></div>
  <div class="hour_col"><div class="hour_num">20</div></div>
  <div class="hour_col"><div class="hour_num">21</div></div>
  <div class="hour_col"><div class="hour_num">22</div></div>
  <div class="hour_col"><div class="hour_num">23</div></div>
  <div class="hour_col"><div class="hour_num">24</div></div>
  
  
</div>

<div id="gantt_main_div" style="height: ${600}px;">

  <div class="hour_col" style="height: ${div_height + 50}px; border-left: 2px solid #ccc;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  <div class="hour_col" style="height: ${div_height + 50}px;"></div>
  
  
  ${ all_bars }
</div>
`;
    
  $('#big_stats_table_box').html(gantt_main_div);
    
  setTimeout(function(){ $('.cit_tooltip').tooltip();  }, 300);  

  };
  this_module.create_gantt = create_gantt;
  
  
  
  
  
  
  function stats_trazi() {
    
    
    // napravi kopiju od cit data
    var stats_query = JSON.stringify( this_module.cit_data );
    stats_query = JSON.parse( stats_query );
    
    delete stats_query.stats_time_grupa;
    delete stats_query.gantt;
    delete stats_query.book_or_park;
    delete stats_query.page_num;
    
    delete stats_query.from_time;
    delete stats_query.to_time;
    delete stats_query.reservation_from_time;
    delete stats_query.reservation_to_time;
    
    
    if ( $('#stats_from_date').val() == '' ) {
      alert('OBAVEZAN POČETNI DATUM !!!');
      return;
    };
    
    if ( $('#stats_to_date').val() == '' ) {
      alert('OBAVEZAN ZAVRŠNI DATUM !!!');
      return;
    };
     
    var stats_from_date = $('#stats_from_date');
    var stats_to_date = $('#stats_to_date');
    
    var stats_from_time = $('#stats_from_time');
    var stats_to_time = $('#stats_to_time');
    
    var start_time = get_DATE_as_milisec(stats_from_date).ms_at_zero + get_time_as_milisec(stats_from_time);
    var end_time = get_DATE_as_milisec(stats_to_date).ms_at_zero + get_time_as_milisec(stats_to_time);

    
    var query_copy = JSON.stringify( stats_query );
    query_copy = JSON.parse( query_copy );
    
    // obriši sve propertije sa null vrijednosti
    $.each( query_copy, function( q_key, q_value ) {
      if ( q_value == null ) delete query_copy[q_key];
    });
    
    var and_array = [];
    
    // sve uguraj u array tako da mogu to staviti pod $and dio querija
    $.each( query_copy, function( q_key, q_value ) {
      var query_obj = {};
      if ( q_key == 'customer_tags' && $.isArray(q_value) && q_value.length > 0 ) {
        
        // ovaj property može biti nula ili nekoliko objekata
        /*
        "customer_tags": [
          {
              "sifra": "TELE2",
              "naziv": "TELE 2"
            }
          ]
        */
        
        query_obj["current_tag"] = {};
        query_obj["current_tag"]["$in"] = [];
        
        
        $.each( q_value, function( customer_tag_index, customer_tag_objekt ) {
          
          var tag_sifra = customer_tag_objekt.sifra;
          if ( tag_sifra == 'nema_grupu' ) tag_sifra = null;
          
          // trebam to pretvoriti u nešto slično ovome:
          // "customer_tags.sifra": { "$in" : ["TELE2", "SIGNIFY", null, ....] }
          query_obj["current_tag"]["$in"].push(tag_sifra);
          
        });
        
      } else {
        query_obj[q_key] = q_value;  
      };
      
      and_array.push(query_obj);
    });
    
    
    if ( this_module.cit_data.book_or_park == 'BOOK' ) {
    
      and_array.push({ 
        $or: [
          { 
            reservation_from_time: { $lte: start_time }, 
            reservation_to_time: { $gte: end_time } 
          }, 
          { 
            reservation_from_time: { $gte: start_time, $lte: end_time }, 
            reservation_to_time: { $gte: end_time } 
          },
          { 
            reservation_from_time: { $lte: start_time },
            reservation_to_time: { $gte: start_time, $lte: end_time  },
          },
          { 
            reservation_from_time: { $gte: start_time, $lte: end_time },
            reservation_to_time: { $gte: start_time, $lte: end_time },
          },
          /* nedovršeni booking */
          { 
            reservation_from_time: { $lt: end_time },
            reservation_to_time: null
          }
        ]
      });
      
      stats_query = { $and: and_array };
      
    };
    
    
    if ( this_module.cit_data.book_or_park == 'PARK' ) {
    
      and_array.push({ 
        $or: [
          { 
            from_time: { $lte: start_time }, 
            to_time: { $gte: end_time } 
          }, 
          { 
            from_time: { $gte: start_time, $lte: end_time }, 
            to_time: { $gte: end_time } 
          },
          { 
            from_time: { $lte: start_time },
            to_time: { $gte: start_time, $lte: end_time  },
          },
          { 
            from_time: { $gte: start_time, $lte: end_time },
            to_time: { $gte: start_time, $lte: end_time },
          },
          /* nedovršeni parking */
          { 
            from_time: { $lt: end_time },
            to_time: null
          }
        ]
      });
      
      stats_query = { $and: and_array };
    };
    
    
    if ( this_module.cit_data.book_or_park == 'BOTH' ) {
      
      var and_array_for_book = JSON.stringify(and_array);
      and_array_for_book = JSON.parse(and_array_for_book);
      

      and_array_for_book.push({ 
        $or: [
          { 
            reservation_from_time: { $lte: start_time }, 
            reservation_to_time: { $gte: end_time } 
          }, 
          { 
            reservation_from_time: { $gte: start_time, $lte: end_time }, 
            reservation_to_time: { $gte: end_time } 
          },
          { 
            reservation_from_time: { $lte: start_time },
            reservation_to_time: { $gte: start_time, $lte: end_time  },
          },
          { 
            reservation_from_time: { $gte: start_time, $lte: end_time },
            reservation_to_time: { $gte: start_time, $lte: end_time },
          },
          /* nedovršeni booking */
          { 
            reservation_from_time: { $lt: end_time },
            reservation_to_time: null
          }
        ]
      });
      
      
      var and_array_for_park = JSON.stringify(and_array);
      and_array_for_park = JSON.parse(and_array_for_park);
      

      and_array_for_park.push({ 
        $or: [
          { 
            from_time: { $lte: start_time }, 
            to_time: { $gte: end_time } 
          }, 
          { 
            from_time: { $gte: start_time, $lte: end_time }, 
            to_time: { $gte: end_time } 
          },
          { 
            from_time: { $lte: start_time },
            to_time: { $gte: start_time, $lte: end_time  },
          },
          { 
            from_time: { $gte: start_time, $lte: end_time },
            to_time: { $gte: start_time, $lte: end_time },
          },
          /* nedovršeni parking */
          { 
            from_time: { $lt: end_time }, 
            to_time: null
          }
        ]
      });   
    
      stats_query = { $or: [ { $and: and_array_for_book } , { $and: and_array_for_park } ] }
      
    };
    
    
    stats_query.start = start_time;
    stats_query.end = end_time;
    

    var stats_trazi_props = {
      desc: 'za stats traži gumb - prikazuje rezultate za stats',
      local: false,
      url: '/search_actions',
      return: {},
      custom_headers: ['PARK', 'USER', 'BOOK OD', 'BOOK DO', 'BOOK TRAJANJE', 'PARK OD', 'PARK DO', 'PARK TRAJANJE' ],
      show_cols: 
      ['pp_sifra', 'user_email', 'reservation_from_time', 'reservation_to_time', 'book_duration', 'from_time', 'to_time', 'duration' ],
      query: stats_query,
      parent: '#big_stats_table_box',
      list_class: 'sidebar_list',
      page: this_module.cit_data.page_num,
    };
  
    search_database(null, stats_trazi_props, 'parallel' ).then(
      function( result ) {
        
        
        if ( result.success == true && result.rows.length > 0 ) {
          
          window.last_timestamp_for_create_excel = result.timestamp;
          
          // $('body').html( JSON.stringify( result ) );
          
          
          var stats_od_find =
`
<div class="stats_od_find_box">
  <b>UNIQUE:</b>
  <div class="stats_od_find_big">${result.distinct}<span>&nbsp;&nbsp;${result.distinct > 1 ? 'users' : 'user'}</span></div>
</div>
<div class="stats_od_find_box">
  <b>UKUPNO:</b>
  <div class="stats_od_find_big">${result.count}<span>&nbsp;&nbsp;${result.count > 1 ? 'actions' : 'action'}</span></div>
</div>
<div class="stats_od_find_box">
  <b>TIME BOOK:</b>
  <div class="stats_od_find_big">${ zaokruziIformatirajIznos(result.time_book/(1000*60*60), 2) }<span>&nbsp;&nbsp;h</span></div> 
</div>
<div class="stats_od_find_box">
  <b>TIME PARK:</b>
  <div class="stats_od_find_big">${ zaokruziIformatirajIznos(result.time_park/(1000*60*60), 2) }<span>&nbsp;&nbsp;h</span></div> 
</div>
<div class="stats_od_find_box">  
  <b>PAID BOOK:</b>
  <div class="stats_od_find_big">${ zaokruziIformatirajIznos(result.novac_book, 2) }<span>&nbsp;&nbsp;kn</span></div>
</div>
<div class="stats_od_find_box">  
  <b>PAID PARK:</b>
  <div class="stats_od_find_big">${ zaokruziIformatirajIznos(result.novac_park, 2) }<span>&nbsp;&nbsp;kn</span></div>
</div>
`;    
          
          $('#stats_od_find_col').html(stats_od_find);       
          
          
          var result_array = result.rows ? result.rows : [];
          
          if (
            this_module.cit_data.gantt == true &&
            this_module.cit_data.stats_time_grupa == 'SAT' &&
            end_time - start_time <= (1000*60*60*24) // ako je interval manji od jednog dana !!
          ) {
            
            this_module.create_gantt(result_array);
            return;
          };
          
          $.each(result_array, function(row_index, row) {
            
            // za svaki slučaj ako uopće ne postoje propertiji onda ih stavi da budu null !!!
            if ( !row.book_duration ) result_array[row_index].book_duration = null;
            if ( !row.duration ) result_array[row_index].duration = null;
            
            $.each(row, function(cell_key, cell_value) {
              

              if ( cell_key == 'pp_sifra' ) {
                result_array[row_index][cell_key] = find_pp_name(cell_value);
              };
              
              
              
              if (
                cell_key == 'reservation_from_time' ||
                cell_key == 'reservation_to_time'   ||
                cell_key == 'from_time'             ||
                cell_key == 'to_time'
                ) {
                
                if ( cell_value == null ) {
                  result_array[row_index][cell_key] = '';
                } else {
                  result_array[row_index][cell_key] = getMeADateAndTime(cell_value).datum + ' ' +  getMeADateAndTime(cell_value).vrijeme;
                };
                
              };
              
              if (
                cell_key == 'book_duration' ||
                cell_key == 'duration'
                ) {
                
                if ( cell_value == null ) {
                  result_array[row_index][cell_key] = '';
                } else {
                  result_array[row_index][cell_key] = get_time_as_hh_mm_ss(cell_value);
                  
                };
                
              };
              
              
              if ( result_array[row_index][cell_key] == null ) {
                result_array[row_index][cell_key] = '';
              };
              
            }); // loop po svakom propertiju od jednog actiona
            
          }); // loop po svim redovima (actions)
          
          create_cit_result_list( result_array, stats_trazi_props );
      
          
        } 
        else {

          var result_array = result.rows ? result.rows : [];
          
          console.log( 'nema ničega' );
          
          create_cit_result_list( result_array, stats_trazi_props );
          
        }

      },
      function( error ) { 
      
        console.log( 'došlo je do greške u stats module ---- > stats_query ' );
        console.log(error);
        
      }
    );
    
    
  }
  this_module.stats_trazi = stats_trazi;
  
  function stats_previous_page() {
    if ( this_module.cit_data.page_num > 1 ) {
      this_module.cit_data.page_num -= 1;
      $('#stats_page_num').text(this_module.cit_data.page_num);
      this_module.stats_trazi();
    };
  };
  this_module.stats_previous_page = stats_previous_page;

  
  function stats_next_page() {
    
    this_module.cit_data.page_num += 1;
    $('#stats_page_num').text(this_module.cit_data.page_num);
    this_module.stats_trazi();
  };
  this_module.stats_next_page = stats_next_page;
  
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {
    
    get_empty_cit_data();
    
  };
  
  function add_perfectbar_and_click_events() {
    
    wait_for( `$('#big_stats_table_box').find('.search_result_table').length > 0`, function() {
      
      
      $('#big_stats_table_box').find('.search_result_table').css('max-height', 'unset');
      
      setTimeout(function() {
        
        window.cit_main_area_perfectbar.update();
        
      }, 200);
      
      /*
      if ( !window.stats_table_perfectbar ) {
        window.stats_table_perfectbar = new PerfectScrollbar( $('#big_stats_table_box').find('.search_result_table')[0] );
      } else {
        window.stats_table_perfectbar.update();
      };
      */
      
      
    }, 500000);
    
  };
  this_module.add_perfectbar_and_click_events = add_perfectbar_and_click_events;
  
  function choose_pp(selected_park) {
    var curr_input = $('#'+current_input_id);
    curr_input.val( selected_park.pp_name );
    this_module.cit_data.pp_sifra = selected_park.pp_sifra;
    console.log(this_module.cit_data.pp_sifra);
  };
  this_module.choose_pp = choose_pp;

    
  function choose_time_grupa(grupa_obj) {
    $('#'+current_input_id).val(grupa_obj.naziv);
    this_module.cit_data.stats_time_grupa = grupa_obj.sifra;
    console.log(this_module.cit_data.stats_time_grupa);
  };
  this_module.choose_time_grupa = choose_time_grupa;
  
  function choose_stats_book_or_park(selected) {
    $('#'+current_input_id).val(selected.naziv);
    this_module.cit_data.book_or_park = selected.sifra;
    console.log(this_module.cit_data.book_or_park);
  };
  this_module.choose_stats_book_or_park = choose_stats_book_or_park;
  
  
  function stats_tag_pill_html(tag) {
    
    var pill_html = 
        
`
<div id="customer_tag_${tag.sifra}" class="cit_tag_pill" style="margin-top: 0;">
${tag.naziv}<i class="fas fa-times"></i>
</div>
`;
    
    return pill_html;
    
  };
  this_module.stats_tag_pill_html = stats_tag_pill_html;
  
  
  function choose_tagovi_stats(selected_tag) {
    
    var curr_input = $('#'+current_input_id);
    curr_input.val( selected_tag.sifra );
    
    console.log(selected_tag);
    
    var tag_pill_html = stats_tag_pill_html(selected_tag);

    // ako već NE postoji pill sa ovim tagom onda ga ubaci
    if ( $('#stats_tag_container').find(`#customer_tag_${selected_tag.sifra}`).length == 0 ) {
      
      // onda ubaci taj pill tj kreiraj ga u HTML-u
      $('#stats_tag_container').append(tag_pill_html);
      
      if ( !this_module.cit_data.customer_tags ) this_module.cit_data.customer_tags = [];
      
      this_module.cit_data.customer_tags.push(selected_tag);
      
      // obnovi listener za brisanje tag pill jer je sad došao ovaj novi
      setTimeout( function() {
        $('.cit_tag_pill .fas').off('click', this_module.delete_customer_tag);
        $('.cit_tag_pill .fas').on('click', this_module.delete_customer_tag);
      }, 100);
      
    };
    
  };
  this_module.choose_tagovi_stats = choose_tagovi_stats;

  
  function delete_customer_tag() {
    
    var customer_tag_sifra = $(this).closest('.cit_tag_pill')[0].id.replace('customer_tag_', '');
    
    $('#customer_tag_' + customer_tag_sifra ).remove();
    
    var delete_this_index = null;
    $.each(this_module.cit_data.customer_tags, function(customer_tag_index, tag_object) {
      if ( customer_tag_sifra == tag_object.sifra ) delete_this_index = customer_tag_index;
    });
    if ( delete_this_index !== null ) this_module.cit_data.customer_tags.splice(delete_this_index, 1);
      
  };
  this_module.delete_customer_tag = delete_customer_tag;
    
  
  function show_gantt(data) {
    this_module.cit_data.gantt = data;
    console.log(this_module.cit_data.gantt);
  };
  this_module.show_gantt = show_gantt;

  
  function create_excel() {

    
    if ( !window.last_timestamp_for_create_excel ) return;

    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: ( '/create_excel/' + window.last_timestamp_for_create_excel ),
      dataType: "json",
      timeout: 1000*20
    })
    .done(function (response) {


      if ( response.success == true ) {
        
        window.open(response.excel_path);

      } else {

        console.error('greska u excelu');
        
      };

    })
    .fail(function (fail_obj) {

      console.error('greska u excelu ------- > FAIL AJAX !!!!');
    });

    
  };
  this_module.create_excel = create_excel;
  
  
  this_module.cit_loaded = true;
 
}
  
  
};