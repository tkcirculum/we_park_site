
var cit_user = null;
// this is for saving repeating interval to variable so I can cancel it later
var search_delay_id = null;

var local_array_GLOBAL = null;
var remote_array_GLOBAL = null;

var image_popup_origin_parent = null;

var current_input_id = null;
var current_input_props = null;
var current_input_results = null;

var current_cit_search = null;
var current_cit_select_fields = null;
var current_cit_return_fields = null;
var current_cit_show = null;
var current_cit_hide = null;
var current_cit_list_container = null;
var current_just_show = null;

var current_custom_headers = null;
var current_col_widths = null;

var current_sort_by = null;

var current_list_class = null;



var current_search_config = null;


// ovo je za local listu u js fileu
var local_or_remote_search = null;


var SEARCH_RESULT_ROW_INDEX = null;

var solar_naponi_obj = null;
var solar_struje_obj = null;
var aku_naponi_obj = null;
var aku_struje_obj = null;

var sol_napon_max = 28.5;
var sol_struja_max = 10;
var aku_napon_max = 25;
var aku_struja_max = 10;



function cit_reset_global_variables() {
  
  window.pp_edit_mode = false;
  
  cit_user = null;
  search_delay_id = null;
  
  local_array_GLOBAL = null;
  remote_array_GLOBAL = null;
  
  image_popup_origin_parent = null;

  current_input_id = null;
  current_input_props = null;
  current_input_results = null;
  
  current_cit_search = null;
  current_cit_select_fields = null;
  current_cit_return_fields = null;
  current_cit_show = null;
  current_cit_hide = null;
  current_cit_list_container = null;
  current_just_show = null;
  
  current_custom_headers = null;
  current_col_widths = null;
  
  current_sort_by = null;
  current_list_class = null;
  
  current_search_config = null;
  
  
  local_or_remote_search = null;
  
  SEARCH_RESULT_ROW_INDEX = null;
  
};



