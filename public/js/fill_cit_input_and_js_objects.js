function reset_cit_data_property() {
  
  var customer_module = window['/main_modules/customers_module/customers_module.js'];
  if ( customer_module && current_input_id == 'customer_acquisition' ) {
    customer_module.cit_data.customer_acquisition = null;
  }; 
  
  var stats_module = window['/main_modules/stats_module/stats_module.js'];
  
  if ( stats_module ) {
    // ------------------------------------
    if ( current_input_id == 'stats_user_tag' ) {
      stats_module.cit_data.current_tag = null;
      console.log(stats_module.cit_data);
    };
    // ------------------------------------
    if ( current_input_id == 'stats_choose_user' ) {
      stats_module.cit_data.user_number = null;
      console.log(stats_module.cit_data);
    };
    //------------------------------------
    
    if ( current_input_id == 'stats_izaberi_park' ) {
      stats_module.cit_data.pp_sifra = null;
      console.log(stats_module.cit_data);
    };
    //------------------------------------
    
    
  };
  
}; // kraj reset cit data property


function fill_cit_input_and_js_objects( data ) {
  
  var curr_input = $('#'+current_input_id);

  
  // ----------------------------- PARKING_PLACES_MODULE -----------------------------
  // ----------------------------- PARKING_PLACES_MODULE -----------------------------
  // ----------------------------- PARKING_PLACES_MODULE -----------------------------
  
  
  var pp_module = window['/main_modules/parking_places/parking_places_module.js'];
  
  if ( pp_module ) {
        
    if ( current_input_id == 'pp_choose_owner' ) {
      pp_module.select_pp_owner(data);
    };
    
    if ( current_input_id == 'choose_day' ) {
      pp_module.select_pp_price_day(data);
    };
    
    if ( current_input_id == 'dyn_pp_price' ) {
      pp_module.switch_dyn_pp_price();
    }; 
        
    if ( current_input_id == 'solar_time_grupa' ) {
      pp_module.choose_solar_time_grupu(data);
    };
    
    if ( current_input_id == 'we_graf_visible' ) {
      pp_module.choose_we_graf_visible(data);
    };   
    
    if ( current_input_id == 'pp_fixed_open' ) {
      pp_module.set_pp_fixed_open(data);
    };
    
    if ( current_input_id == 'pp_fixed_close' ) {
      pp_module.set_pp_fixed_close(data);
    };
  
  }; // end if parking places module is loaded !!!!
  
  // ----------------------------- CUSTOMER_MODULE -----------------------------
  // ----------------------------- CUSTOMER_MODULE ----------------------------
  // ----------------------------- CUSTOMER_MODULE ----------------------------
  
  var customer_module = window['/main_modules/customers_module/customers_module.js'];
  
  if ( customer_module ) {

    
    if ( current_input_id == 'customers_izaberi_park' ) {
      customer_module.choose_pp_for_action_table(data);
    };
    
    if ( current_input_id == 'tagovi_customer' ) {
      customer_module.choose_tagovi_customer(data);
    };
    
    
    if ( current_input_id == 'add_vip_status_pp_list' ) {
      customer_module.select_pp_for_vip(data);
    };  
  

    if ( current_input_id == 'customer_acquisition' ) {
      customer_module.select_kanal_akvizicije(data);
    };

    if ( current_input_id == 'customer_obrtnik' ) {
      customer_module.cit_data.obrtnik = data;
    };
    
    if ( current_input_id == 'customer_pdv' ) {
      customer_module.cit_data.pdv = data;
    };
    
    if ( current_input_id == 'customer_gdpr' ) {
      customer_module.cit_data.gdpr = data;
    };    
    
  }; // end if customer module is loded !!!!!
  
  
  
  // ----------------------------- STATS MODULE-----------------------------
  // ----------------------------- STATS MODULE-----------------------------
  // ----------------------------- STATS MODULE-----------------------------
  
  var stats_module = window['/main_modules/stats_module/stats_module.js'];
  if ( stats_module ) {
    
    if ( current_input_id == 'stats_izaberi_park' ) {
      stats_module.choose_pp(data);
    };
    
    if ( current_input_id == 'stats_user_tag' ) {
      stats_module.choose_tagovi_stats(data);
    };
    
    if ( current_input_id == 'stats_time_grupa' ) {
      stats_module.choose_time_grupa(data);
    };
    
    if ( current_input_id == 'stats_book_or_park' ) {
      stats_module.choose_stats_book_or_park(data);
    };
    
    
    if ( current_input_id == 'gantt_graf' ) {
      stats_module.show_gantt(data);
    };
    
    
    if ( current_input_id == 'stats_choose_user' ) {
      stats_module.select_stats_user(data);
    };

    
  }; // kraj ako je loaded stats module
  
  
  // ----------------------------- PONUDA MODULE-----------------------------
  // ----------------------------- PONUDA MODULE--------- ili products ------
  // ----------------------------- PONUDA MODULE-----------------------------
  
  var products_module = window['/main_modules/products_module/products_module.js'];
  if (products_module) {
    
    if ( current_input_id == 'select_ponuda_user' ) {
      products_module.select_ponuda_user(data);
    };
    
    if ( current_input_id == 'ponuda_pp_list' ) {
      products_module.choose_ponuda_pp(data);
    };
    
    if ( current_input_id == 'ponuda_special_places' ) {
      products_module.choose_ponuda_special_place(data);
    };
    
        
    if ( current_input_id == 'ponuda_vip_sub' ) {
      products_module.is_vip_sub(data);
    };
    
  };
    
  
  
  
  // ----------------------------- PORTIR MODULE-----------------------------
  // ----------------------------- PORTIR MODULE-----------------------------
  // ----------------------------- PORTIR MODULE-----------------------------
  
  var portir_module = window['/main_modules/portir_module/portir_module.js'];
  if (portir_module) {

    if ( current_input_id == 'portir_pp_list' ) {
      portir_module.choose_portir_pp(data);
    };
    
    if ( current_input_id == 'portir_special_places' ) {
      portir_module.choose_portir_special_place(data);
    };
    
    
    if ( current_input_id == 'portir_reg_input' ) {
      portir_module.select_user_row_from_search_regs(data);
    };
    
    
  };
  
  
  
}; // kraj cijele funkcije fill cit input and js object