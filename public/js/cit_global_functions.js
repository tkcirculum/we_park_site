
function wait_for(condition, callbackFunction, period, expired_callback) {

  var new_wait_for_object = {
      timeStamp: Date.now(),
      condition: condition
  };

  if ( !window.wait_interval_array ) window.wait_interval_array = [];
  window.wait_interval_array.push(new_wait_for_object);
  var interval_id = setInterval(function() {
      check_condition(interval_id, condition, callbackFunction, period, expired_callback);
  }, 30);

  window.wait_interval_array[window.wait_interval_array.length - 1].id = interval_id;

  // if (period == 'inf') {
  return interval_id;
  //};
}; // end of wait for  function


function check_condition(interval_id, condition, callbackFunction, period, expired_callback) {
    var waiting_time = 50000;
    if (typeof period !== 'undefined' && period !== 'inf') {
        waiting_time = Number(period);
    };

    var resolved_condition = null;


    /*
    function Date(n){
      return ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"][n%7 || 0];
    }
    function runCodeWithDateFunction(obj){
        return Function('"use strict";return ( function(Date){ return Date(5) } )')()(Date);
    }
    console.log( runCodeWithDateFunction("function(Date){ return Date(5) }") )
    */


    if ($.type(condition) == "function") resolved_condition = condition();
    if ($.type(condition) == "string") resolved_condition = eval(condition);

    if (resolved_condition !== true) {

        /* -------------- if period is 'inf' then dont do this check at all --------------  */
        // effectively this means 
        if (period == 'inf') {

            /* 
            ------------- 
            DONT DO ANY CHECKS IS TIME EXPIRED ---> BECAUSE LISTENER IS INFINITE
            ----------------
            */
        } else {
            // console.log('waiting for condition:    ' + condition + '    to be true.....');
            // just a pointers to delete later
            var remove_this_indexes = [];
            // iterate array of interval object ids
            $.each(window.wait_interval_array, function(index, interval_object) {
                // when I find right id
                if (parseInt(interval_object.id) === parseInt(interval_id)) {
                    // check timestamp to see is expired
                    if (Date.now() - Number(interval_object.timeStamp) > waiting_time) {
                        // if difference is greater then defined period/waiting time
                        // remove this interval
                        clearInterval(interval_id);
                        // save this index for later delete
                        remove_this_indexes.push(index);

                        // console.log('condition  ' + condition + ' DID NOT fulfil IN ' + waiting_time / 1000 + 'sec .. aborting .....');

                        if (typeof expired_callback !== 'undefined') {
                            expired_callback();
                        };
                    };
                };
            });

            $.each(remove_this_indexes, function(ind, index_number) {
                window.wait_interval_array.splice(index_number, 1);
            });

            remove_this_indexes = [];

        }; // END OF if period is NOT 'inf' and it is NOT undefined

    } else {

        // IF CONDITION IS TRUE !!!!!!!!!1



        if (period == 'inf') {

            // if period is 'inf' just run callback function 
            // BUT DONT STOP THE LISTENER !!!!!!!!

            callbackFunction();
        } else {

            var remove_this_indexes = [];

            $.each(window.wait_interval_array, function(index, interval_object) {
                if (parseInt(interval_object.id) === parseInt(interval_id)) {
                    clearInterval(interval_id);

                    // save this index to later remove
                    remove_this_indexes.push(index);

                    callbackFunction();
                };
            });

            $.each(remove_this_indexes, function(ind, index_number) {
                window.wait_interval_array.splice(index_number, 1);
            });

            remove_this_indexes = [];

        }; // end if period is NOT 'inf'

    }; // END IF IF CONDITION IS TRUE

}; 
// end of check_is_loaded function


function cit_rand_min_max(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
};

function cit_rand_min_max_float(min, max) { // min and max included 

  var float_num = (Math.random() * (max - min + 1) + min);
  // if ( float_num > max ) float_num = max;
  return float_num;
  
};

function set_fixed_open_or_close_ajax(open_or_close, ime_rampe, new_state ) {

  
    var req_url = '';
  
  
    if ( open_or_close == 'open') req_url = `/fixed_open_rampa/${ime_rampe}/${new_state}`;
    if ( open_or_close == 'close') req_url = `/fixed_close_rampa/${ime_rampe}/${new_state}`;
  
  
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: req_url,
      dataType: "json"
      })
    .done(function(response) {
      
      console.log(response);
      if ( response.success !== true ) {
        
        if ( open_or_close == 'open') popup_error_fixed_open();
        if ( open_or_close == 'close') popup_error_fixed_close();
        
      };
      
    })
    .fail(function(error) {
      if ( open_or_close == 'open') popup_error_fixed_open();
      if ( open_or_close == 'close') popup_error_fixed_close();
      console.error(error);
    });
    
  
  
  
};

function add_search_sort(options) {
  
  var new_options = options;
  
  if ( options.sort_by ) {
    
    if ( options.current_cit_search == 'search_customers' ) {
      
      options.sort_by = options.sort_by.split(',');
      
      var sort_object = {};
      
      $.each( options.sort_by, function(sort_index, sort_item) {
        
        var sort_item = sort_item.split(':');
        var sort_key = sort_item[0];
        var sort_value = sort_item[1];
        sort_object[sort_key] = Number(sort_value);
        
      });
      
      options.sort_by = sort_object;  
    };
    
  };
  
  return options;
};


function GLOBAL_perfectbar_and_click_events(props) {
  
  if ( props.parent == '#pp_customers_list_place_here' ) {
    var customers_list_module =  window['/right_sidebar/cit_search_list/customers_list/cit_customers_list_module.js'];
    if ( customers_list_module ) {
      customers_list_module.add_perfectbar_and_click_events();
    };
  };
  
  
  
  if ( props.parent == '#park_places_list_place_here' ) {
    var pp_list_module =  window['/right_sidebar/cit_search_list/park_places/cit_pp_list_module.js'];
    if ( pp_list_module ) {
      pp_list_module.add_perfectbar_and_click_events();
    };
  };
  
  
  if ( props.parent == '#big_stats_table_box' ) {
    var stats_module =  window['/main_modules/stats_module/stats_module.js'];
    if ( stats_module ) {
      stats_module.add_perfectbar_and_click_events();
    };
  };
  
};


function toggle_global_progress_bar(show) {
  
  if (show) $('#cit_global_progress_bar').addClass('cit_show');
  if (!show) $('#cit_global_progress_bar').removeClass('cit_show');
}


function create_result_header(search_object, props) {
  
  delete search_object.__v;
  
  
  var count_cit_hide = props.hide_cols ? props.hide_cols.length : 0;
  // broj stupaca je zapravo ukupan broj propertija u objektu minus propertiji koje treba sakriti
  var number_of_columns = Object.keys(search_object).length - count_cit_hide;

  
  if ( props.show_cols ) {
    number_of_columns = props.show_cols.length;
  };
  

  var columns = '';
  var col_count = 0;

  function run_col_creation(key, value) {
    
    var style_hide = '';
    
    if ( props.hide_cols && $.inArray( key, props.hide_cols ) > -1 ) style_hide = 'display: none;';
    
    if ( props.show_cols ) {
      // ako je key u show arrayu
      if ( $.inArray( key, props.show_cols ) > -1 ) style_hide = '';
      // ako key nije u arrayu
      if ( $.inArray( key, props.show_cols ) == -1 ) style_hide = 'display: none;';
    };
    
    if ( props.col_widths && props.col_widths[col_count] == '0' ) style_hide = 'display: none;';

    var sirina_columne = `calc(${ props.col_widths ? props.col_widths[col_count] : (100/number_of_columns)}% + 1px)`;
    
    if ( style_hide == 'display: none;' ) sirina_columne = 0;
    
    var property_index = null;
    if ( props.show_cols && props.custom_headers ) {
      
      $.each(props.show_cols, function(pr_ind, propery_name) {
        if ( key ==  propery_name ) property_index = pr_ind;
      });
    };
    // prije nego postaviš imena propertija kao naslove u stupcima replace sve underscore znakove sa space
    columns += 
    `
    <div  class="search_result_column header_column" 
          style="width: ${sirina_columne}; ${style_hide}">
      ${ props.custom_headers ? props.custom_headers[property_index] : key.replace(/_/g, " ") } 
      <i class="fa fa-caret-down"></i>
    </div>

    `;
    
    // if ( sirina_columne > 0 ) 
    
    col_count += 1;
 
  };
  
  if ( props.return && Object.keys(props.return).length > 0 ) {
    
    // ako sam definirao specifična polja za return
    $.each(props.return, function(return_key, return_val) {  
      $.each(search_object, function(item_key, item_value) {
        if ( item_key == return_key ) run_col_creation(item_key, item_value);
      }); 
    });  
    
  } else if ( props.show_cols && props.show_cols.length > 0 ) {
    
    $.each(props.show_cols, function(show_ind, show_col_key) {  
      $.each(search_object, function(item_key, item_value) {
        if ( item_key == show_col_key ) run_col_creation(item_key, item_value);
      }); 
    }); 
    
  } else {
  
    $.each(search_object, function(item_key, item_value) {
      run_col_creation(item_key, item_value);
    });
    
  };
  
  
  return columns;
};


function create_result_row(search_object, props) {

  delete search_object.__v;
  
  var count_cit_hide = props.hide_cols ? props.hide_cols.length : 0;
  // broj stupaca je zapravo ukupan broj propertija u objektu minus propertiji koje treba sakriti
  var number_of_columns = Object.keys(search_object).length - count_cit_hide;

  if ( props.show_cols ) {
    number_of_columns = props.show_cols.length;
  };
    

  var columns = '';
  var col_count = 0;
  

  
  function run_col_creation(key, value) {
    
    // VAŽNO !!!!!
    // KADA JE PROPERTY OZNAČEN KAO HIDDEN ONDA IPAK IZGENERIRAM ĆELIJU ALI JE SAKRIJEM
    // TJ BUDE DISPLAY NONE !!!!!!
    var style_hide = '';
    
    if ( props.hide_cols && $.inArray( key, props.hide_cols ) > -1 ) style_hide = 'display: none;';
    
    if ( props.show_cols ) {
      // ako je key u show arrayu
      if ( $.inArray( key, props.show_cols ) > -1 ) style_hide = '';
      // ako key nije u arrayu
      if ( $.inArray( key, props.show_cols ) == -1 ) style_hide = 'display: none;';
    };
    
    if ( props.col_widths && props.col_widths[col_count] == '0' ) style_hide = 'display: none;';
    
    var sirina_columne = `calc(${ props.col_widths ? props.col_widths[col_count] : (100/number_of_columns)}% + 1px)`;
    
    if ( style_hide == 'display: none;' ) sirina_columne = 0;
    
    // ----------------- AKO JE VALUE ARRAY KAO NA PRIMJER car_plates UNUTAR USER OBJEKATA -----------------
    // ----------------- AKO JE VALUE ARRAY KAO NA PRIMJER car_plates UNUTAR USER OBJEKATA -----------------
    
    if ( $.isArray(value) ) {
      
      if ( key == 'car_plates') {
        
        
      };
      
      
    };

    // ako je value string tipa i ako u sebi sadrži .png ili .jpg
    if ( $.isArray(value) == false && String(value).indexOf('.png') > -1 ||  String(value).indexOf('.jpg') > -1 ) {
      // onda ga tretiraj kao sliku !!!! tj wrepaj ga u img tag
      value = `<img src="/img/${value}" class="img-responsive cit_search_result_image">`;
    };

    columns += 
    `
    <div  class="search_result_column ${key}" 
          style="width: ${sirina_columne}; ${style_hide}">
      ${value}
    </div>

    `;

    col_count += 1;    
    
  };
  
  
  if ( props.return && Object.keys(props.return).length > 0 ) {
    
    $.each(props.return, function(return_key, return_val) {  
      $.each(search_object, function(item_key, item_value) {
        if ( item_key == return_key ) run_col_creation(item_key, item_value);
      }); 
    }); 
    
  } else if ( props.show_cols && props.show_cols.length > 0 ) {
    
    $.each(props.show_cols, function(show_ind, show_col_key) {  
      $.each(search_object, function(item_key, item_value) {
        if ( item_key == show_col_key ) run_col_creation(item_key, item_value);
      }); 
    }); 
    
  } else {
  
    $.each(search_object, function(item_key, item_value) {
      run_col_creation(item_key, item_value);
    });
    
  };

  return columns;
};


function create_cit_result_list( result_array, props ) {
  
  var final_result = '';
  
  if ( result_array.length > 0 ) {

    // POŠTO SAM NAKNADNO DODAVAO POLJA U BAZI
    // sada imam nejednak broj polja u različitim kupcima
    // zato tražim kupca sa maksimalnim brojem polja 
    // i to uzimam kao referencu za polja
    var max_property_count = 0;
    var index_of_max_property_object = null;

    $.each(result_array, function(search_arr_index, search_object) {

      if ( Object.keys(search_object).length > max_property_count ) {
        max_property_count = Object.keys(search_object).length;
        index_of_max_property_object = search_arr_index;
      };

    });

    var result_rows_HTML = '';

    var list_class = props.list_class ? props.list_class : ''; 

    var header_row_HTML = 
    `
    <div class="search_result_row header ${list_class}">
    ${create_result_header(result_array[index_of_max_property_object], props)}
    </div>
    `;

    $.each(result_array, function(search_arr_index, search_object) {

      // usporedi sve propertije od trenutnog objekta sa objektom koji ima najviše propertija

      $.each(result_array[index_of_max_property_object], function(max_obj_key, max_obj_value) {

        // VAŽNO !!!!!! - ako trenutni objekt NEMA neki od propertija koji ima referentni objekt
        // VAŽNO !!!!!! - ako trenutni objekt NEMA neki od propertija koji ima referentni objekt
        // VAŽNO !!!!!! - ako trenutni objekt NEMA neki od propertija koji ima referentni objekt

        // onda ga kreiraj i dodaj mu vrijednost null !!!!!
        if ( typeof search_object[max_obj_key] == 'undefined' ) {
          search_object[max_obj_key] = null;
        };

      });

      var sifra_ili_id_class = '';
      if ( search_object._id ) sifra_ili_id_class = '_id';
      if ( search_object.sifra ) sifra_ili_id_class = 'sifra';


      // pogledaj jel ima id  
      var row_unique_id = search_object._id ? search_object._id : '';
      // ako nema id onda gledaj jel ima sifru
      if ( !row_unique_id ) row_unique_id = search_object.sifra ? search_object.sifra : '';
      // ako nema niti sifru ond akreiraj uniq id od vremena i od indexa ovog loopa
      if ( !row_unique_id ) row_unique_id = Date.now() + '_' + search_arr_index;
      var result_row_HTML = 
          `
          <div class="search_result_row ${list_class} ${sifra_ili_id_class}" id="${ 'list_item_' + row_unique_id }">
          ${create_result_row(search_object, props)}
          </div>

          `;
          result_rows_HTML += result_row_HTML;
    });

    final_result = 

    `
    ${header_row_HTML}
    <div class="search_result_table cit_scrollbar">
    ${result_rows_HTML}
    </div>

    `;
    
    
  } else {
  
    // ----------------------------- ako je result array prazan !!!!
    final_result = '<div id="nema_rezultata">Nije pronađen niti jedan zapis u bazi</div>'
  
  }; 

  
  var place_function = props.position || 'html';
  
  var default_parent = '#search_autocomplete_container';
  
  $( props.parent || default_parent )[place_function](final_result);

  
  GLOBAL_perfectbar_and_click_events(props);

  // ako nisam naveo parent onde je search_autocomplete_container
  if ( !props.parent ) {
    
      var current_input = $('#'+current_input_id)[0];
      pozicioniraj_drop_listu(current_input);
    
      $('#search_autocomplete_container').css({
        'opacity': '1',
        'pointer-events': 'all'
      });

      if ( !window.autocomplete_perfect_bar ) {
      window.autocomplete_perfect_bar = new PerfectScrollbar( $('#search_autocomplete_container .search_result_table')[0] );
      } else {
        window.autocomplete_perfect_bar.update();
      };
  };

}; // kraj create result list



function search_database( trazi_string, props, parallel ) {

  
  // inače je current input uvijek polje na koje je user kliknuo ili na njemu traži nešto
  var curr_input = $('#' + current_input_id );
  
  // ALI !!!!!!! postoje slučajevi kada generiram tablicu automatski i user nije niti na jednom input polju
  // tada je parent neki objekt koji nije input polje
  
  if ( props.parent && props.parent !== '#search_autocomplete_container' ) {
    
    // onda tretiram parent element kao da je input polje !!!!!
    // i kasnije spremam we result u DOM data property od tog parenta
    curr_input = $( props.parent );
  };
  
  return new Promise(function(resolve, reject) {

    var throttle_time = 300;

    // ako je show on click i ako nije ništa upisano u input !!!!
    // to znači da želim da se lista samo pokaže kao da je klasična drop lista ili select option polje
    if ( props.show_on_click == true && trazi_string == null ) throttle_time = 0;

    if ( parallel == 'parallel' ) throttle_time = 0;

    // ako je request za park places ne treba user !!!!!
    if (!cit_user && props.url !== '/search_park_places' ) {
      prom.reject('NISTE ULOGIRANI U APLIKACIJU !!!');
      return prom;
    };

    if ( !props.url ) {
      reject('NEMA URL-a !!!');
      return;
    };


    var search_body = {
      trazi_string: ( trazi_string || null ),
      query: ( props.query || {} ),
      find_in: (props.find_in || null ),
      return: (props.return || {} ),
      sort: ( props.sort || null ),
      filter: (props.filter || null ),
      limit: (props.limit || 100 ),
      page: (props.page || 0 )
    };


    if ( !window.cit_xhr ) window.cit_xhr = {};
    if ( !window.cit_xhr[props.desc] ) window.cit_xhr[props.desc] = {};

    // ako ima arg parallel to znači da ga ne smijem abortati !!!!!
    // ako nije parallel onda abortiraj ako je prošlo manje vremena od throttle time
    if ( parallel !== 'parallel' && Date.now() - ( window.cit_xhr[props.desc].time || 0 ) < throttle_time ) {
      clearTimeout( window.cit_xhr[props.desc].id );
    };

    window.cit_xhr[props.desc].time = Date.now();

    // slow down ajax request firing to throttle time
    search_delay_id = setTimeout(function () {

      var req = window.cit_xhr[ props.desc ].ajax;
      if (req && req.readyState != 4) {
          req.abort();
      };

      toggle_global_progress_bar(true);


      window.cit_xhr[props.desc].ajax = 
      $.ajax({
        headers: {
            'X-Auth-Token': ( cit_user ? cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: props.url,
        data: JSON.stringify(search_body),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result_array) {

        toggle_global_progress_bar(false);


        if ( result_array.success == false ) {

          if ( result_array.msg && typeof window[result_array.msg] == 'function' ) {
            
            window[result_array.msg]();
            
          } else if ( result_array.msg ) {
            
            var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;
            var popup_text = result_array.msg;
            show_popup_modal('error', popup_text, null, 'ok' );
            $('#cit_popup_modal_OK').off('click');
            $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });                     
                     
          };

          resolve([]);
          console.error("GREŠKA U PREUZIMANJU PODATAKA !");

          if ( result_array.error ) console.log( result_array.error );

        } else if ( result_array.error && result_array.error.indexOf('No such end point to return json') > -1 ) {

          resolve([]);
          console.error("GREŠKA U PREUZIMANJU PODATAKA !");

        } else {

          curr_input.data('we_result', result_array);
          current_input_results = result_array;
          resolve( result_array);

        };

      })
      .fail(function (error) {


        curr_input.data('we_result', null);
        current_input_results = null;

        toggle_global_progress_bar(false);
        if ( error.statusText && error.statusText !== 'abort' ) console.error("GREŠKA U PREUZIMANJU PODATAKA !");
        reject(error);
        
      });


    }, throttle_time);  

    window.cit_xhr[props.desc].id = search_delay_id;
  
  }); // kraj promisa  
    
};


function fill_user_metadata( data, saved_field_id, edited_field_id, last_login_field_id ) {
  

  var user_saved_html = 
      `Spremio: ${ data && data.user_saved ? data.user_saved.full_name : ''}&nbsp;
      ${ data && data.user_saved ? getMeADateAndTime(data.user_saved.time_stamp).datum : '' }&nbsp;
      ${ data && data.user_saved ? getMeADateAndTime(data.user_saved.time_stamp).vrijeme : '' }
      `;

  $( saved_field_id ).html(user_saved_html);



  var user_edited_html =
      `Editirao: ${ data && data.user_edited ? data.user_edited.full_name : ''}&nbsp;
      ${ data && data.user_edited ? getMeADateAndTime(data.user_edited.time_stamp).datum : ''}&nbsp;
      ${ data && data.user_edited ? getMeADateAndTime(data.user_edited.time_stamp).vrijeme : '' }
      `;

  $(edited_field_id).html(user_edited_html);  

  
  if ( last_login_field_id ) {
    
     var last_login_html =
          `Last Login: ${ (data && data.last_login ) ? getMeADateAndTime(data.last_login).datum : ''}&nbsp;
          ${ (data && data.last_login ) ? getMeADateAndTime(data.last_login).vrijeme : ''}&nbsp;
          `;

     $(last_login_field_id).html(last_login_html);  
  };
  
};


function search_local(search_string, props) {
  
  current_input_results = null;
  
  var local_result = [];
  
    /* ---------------------- ITERIRAJ ARRAY SVIH OBJEKATA ---------------------- */
    $.each(we_local_arr[props.list], function(search_arr_index, search_object) {

      // ovak sprečavam da ponovim isti red ako u više polja/stupaca nadjem string koji user traži !!!!
      var this_row_created = false;
      
      /* ---------------------- ITERIRAJ PROPERTJE U JEDNOM OBJEKTU ---------------------- */
      $.each(search_object, function(item_key, item_value) {

        var search_this_field = props.find_in ? ( $.inArray(item_key, props.find_in) > -1 ) : true ; 
        
        /* ------------------ OVDJE ZAPRAVO PRONALAZIM TRAŽENI STRING  ------------------ */
        if (  
          !props.show_on_click                                         &&
          String(item_value).toLowerCase().indexOf(search_string) > -1 &&
          this_row_created == false                                    &&
          search_this_field == true 
          
          ) {
          this_row_created == true;
          local_result.push(search_object);
        };
        
        /* ------------------ AKO JE just show local ONDA PRIKAŽI SVE !!!!!!! ------------------ */
        if (  
          props.show_on_click == true &&
          this_row_created == false   &&
          search_this_field == true 
          
          ) {
          
          this_row_created = true;
          local_result.push(search_object);  
        };
        
      });
      
    });
  
    $('#'+current_input_id).data('we_result', local_result);
    current_input_results = local_result;
    return local_result;
  
};


function activate_result_line_with_arrow_keys(event) {

  var drop_list = $('#search_autocomplete_container');
  
  var list_rows = drop_list.length > 0 ? drop_list.find('.search_result_table .search_result_row') : null;
  
  if ( drop_list.css('opacity') == '1' && list_rows && list_rows.length > 0 ) {

    // if arrow UP
    if ( event.which == 38 ) {

      event.preventDefault();

      if ( SEARCH_RESULT_ROW_INDEX == null || SEARCH_RESULT_ROW_INDEX == 0 ) {
        // if line is frist or not initiated
        SEARCH_RESULT_ROW_INDEX = list_rows.length - 1;

      } else if ( SEARCH_RESULT_ROW_INDEX > 0 ) {

        SEARCH_RESULT_ROW_INDEX -= 1;
      };

      list_rows.removeClass('hovered');
      list_rows.eq(SEARCH_RESULT_ROW_INDEX).addClass('hovered');

    };



    // if arrow DOWN
    if ( event.which == 40 ) {
      event.preventDefault();

      if ( 
        SEARCH_RESULT_ROW_INDEX == null || 
        SEARCH_RESULT_ROW_INDEX == list_rows.length - 1 
      ) {
        // if line is first or not initiated
        SEARCH_RESULT_ROW_INDEX = 0 ;
        
      } else if ( SEARCH_RESULT_ROW_INDEX < list_rows.length - 1 ) {
        
        SEARCH_RESULT_ROW_INDEX += 1;
      };

      list_rows.removeClass('hovered');
      list_rows.eq(SEARCH_RESULT_ROW_INDEX).addClass('hovered');

    };


    // if ENTER
    if ( event.which == 13 ) {
      event.preventDefault();
      if ( SEARCH_RESULT_ROW_INDEX !== null )  list_rows.eq(SEARCH_RESULT_ROW_INDEX).click();
    };

    // if ESCAPE
    if ( event.which == 27 ) {
      
      console.log('user je pritusnuo ESC .... sakrivam drop listu');
      close_cit_drop_list();
      
    };
    

  };

};


function check_is_input_hidden(input) {
  
  var element_is_hidden = false;
  
  if ( $(input).closest('.show_hide_element').length > 0 ) {
    
    var parent_height = $(input).closest('.show_hide_element').css('height');
    parent_height = Number( parent_height.replace('px', ''));
    
    if ( parent_height < 20 ) {
      close_cit_drop_list();
      element_is_hidden = true;
    };
    
  };
  
  var element_parent = $(input).parent();
  
  if ( element_parent.css('display') == 'none' ||  element_parent.css('opacity') == '0' ) {
    close_cit_drop_list();
    element_is_hidden = true;
  };
  
  
  if ( element_is_hidden == true ) return true;
  
  return false;
};


function pozicioniraj_drop_listu(input_polje) {
  

  
  /* -------------------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  /* ----- START -------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  /* -------------------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  
  // ovo su podaci o poziciji kliknutog polja

  var top = $(input_polje).offset().top + parseInt( $(input_polje).css('height') );
  var this_width = parseInt( $(input_polje).css('width') );
  var left_middle = $(input_polje).offset().left + this_width / 2;
  
  
  // ako je mali ekran tj mobitel
  if ( $(window).width() < 700 ) {
    
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0',
      'top': top + 'px',
      'left': '0px',
      'width': '100%',
      'margin-left': '0px',
      'overflow-x': 'auto'
      
    });

    
    var search_box_width = '100%';
    
    if ( $(input_polje).hasClass('simple') ) {
      search_box_width = (current_input_props && current_input_props.list_width) ? current_input_props.list_width : 500;
      search_box_width = search_box_width + 'px';
    };
    
    if ( $(input_polje).hasClass('same_width') && this_width > $(window).width() ) {
      search_box_width = this_width + 'px';
    };

    
    $('#search_autocomplete_container .search_result_row.header').css({
      'width': search_box_width,
    });

    $('#search_autocomplete_container .search_result_table').css({
      'width': search_box_width,
    });
    
    
    return;
  };
  
  
  if ( $(input_polje).hasClass('simple') ) {
    
    var search_box_width = (current_input_props && current_input_props.list_width) ? current_input_props.list_width : 500;
    
    var margin_left =  -search_box_width/2;
    
    
    // ako drop lista izlazi s ekrana na ljevu stranu
    if ( left_middle - search_box_width/2 < 0 ) {
      var left_outside_screen = search_box_width/2 - left_middle;
      left_middle = left_middle + left_outside_screen;
    };
    
    // ako drop lista izlazi s ekrana na ljevu stranu
    if ( left_middle + search_box_width/2 > $(window).width() ) {
      var right_outside_screen = search_box_width/2 + left_middle;
      left_middle = left_middle - right_outside_screen;
    };
    
    
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0',
      'top': top + 'px',
      'left': left_middle + 'px',
      'width': search_box_width + 'px',
      'margin-left': ( -search_box_width/2) + 'px'

    });

    
  }
  else if ( $(input_polje).hasClass('same_width') ) {
    
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0',
      'top': top + 'px',
      'left': left_middle + 'px',
      'width': this_width + 'px',
      'margin-left': ( -this_width/2) + 'px'

    });
    
  }  
  else {
    
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0',
      'top': top + 'px',
      'left': 10 + 'px',
      'width': 'calc(100vw - 40px)',
      'margin-left': 0 + 'px'

    });

  };
  
  /* -------------------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  /* ----- END ---------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  /* -------------------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  
    
  
};


function handle_cit_input_click() {
  
  current_input_id = this.id;
  
};


function close_cit_drop_list() {
  

  $('#search_autocomplete_container').css({
    'pointer-events': 'none',
    'opacity': '0'
  });

  $('#search_autocomplete_container').html('');

  SEARCH_RESULT_ROW_INDEX = null;


  
}


function handle_hide_autocomplete_list(e) {
  // ako je click target NIJE neki red ili element u search spremnik onda SAKRIJ AUTOCOMPLETE LISTU !!!!  
  if ( $(e.target).closest('#search_autocomplete_container').length == 0 ) {
    
    close_cit_drop_list();
    
  };

}


function show_hide_form_element(element, show) {
  if (show) {
    var scroll_height = element[0].scrollHeight;
    element.css('height', scroll_height + 'px');
  } else {
    element.css('height', 0 + 'px');
  }
};


// kada user klikne na malu sliku u autocomplete search result tablici
// onda se slika prikaže uvećana u modal popup-u
function handle_click_result_image(e) {
  
  e.stopPropagation();
  
  // prvo removam spremnik ako postoji od prije
  $('#image_popup_spremnik').remove();
  
  image_popup_origin_parent = $(this).parent();
  
  // popup spremnik je samo element u koji stavljam klon od kliknute slike u autocomplete search result tablici
  var image_popup_HTML = 
      `
      <div id="image_popup_spremnik">
      </div>

      `;
  // ubacim spremnik u body
  $('body').prepend(image_popup_HTML);
  // zatim ubacim klon od slike u spremnik
  $('#image_popup_spremnik').prepend($(this).clone());
  
  // zatim registriram listener da kada user klikne na uvećanu sliku
  // onda se popup sa slikom obriše tj nestane
  $('#image_popup_spremnik').off('click');
  $('#image_popup_spremnik').on('click', function(e) {
    e.stopPropagation();
    $(this).remove();
  });
  
}



function escape_text(text, replaceWith) {

  var uri_text = encodeURI(text);

  for (i = 0; i < uri_text.length; i++) {
    // loop through string, replacing carriage return encoding with HTML break tag

    if (uri_text.indexOf("%0D%0A") > -1) {
      // Windows encodes returns as \r\n hex
      uri_text = uri_text.replace("%0D%0A", replaceWith);
    } else if (uri_text.indexOf("%0A") > -1) {
      // Unix encodes returns as \n hex
      uri_text = uri_text.replace("%0A", replaceWith);
    } else if (uri_text.indexOf("%0D") > -1) {
      // Macintosh encodes returns as \r hex
      uri_text = uri_text.replace("%0D", replaceWith);
    };

  };

  uri_text = decodeURI(uri_text); // unescape all other encoded characters

  return uri_text;

}
    

function get_entire_object_from_list(global_list, find_id, remote, query_funkcije_array ) {
  
  if (remote == 'remote') {
  
  var pronasao_item_remote = null;
  $.each(global_list, function(list_index, item) {
    if ( item._id == find_id ) {
      pronasao_item_remote = item;
    };
  });
  
    // napravi kopiju objekta
    pronasao_item_remote = JSON.stringify(pronasao_item_remote);
    pronasao_item_remote = JSON.parse(pronasao_item_remote);
    
    return pronasao_item_remote;
    
  } 
  else {
    
    
    // lokalne liste pretrazujem po sifri, a ne po id-ju !!!!!!!!
    var pronasao_item_local = null;
    

    if ( query_funkcije_array ) {

        pronasao_item_local = global_list;

        query_funkcije_array.forEach(function(query_func, filter_index) {

          try {
            var filter = new Function('proizvodi', query_func );

            pronasao_item_local = filter(pronasao_item_local);
          }
          catch(err) {
            console.log({error: err, msg: 'GREŠKA PRILIKOM IZVRŠENJA QUERY FUNKCIJE LOKALNO'});
          };

        });


        if ( pronasao_item_local.length == 1 ) {
          pronasao_item_local = pronasao_item_local[0];

        } else {
          console.log('GREŠKA PRILIKOM IZVRŠENJA QUERY FUNKCIJE LOKALNO - pronadjen više od jedan red !!!!');
          return;
        }
      
        // napravi kopiju objekta
        var pronasao_item_local_string = JSON.stringify(pronasao_item_local);
        pronasao_item_local_copy = JSON.parse(pronasao_item_local_string);

        return pronasao_item_local_copy;

      };

    $.each(global_list, function(list_index, item) {
        if ( item.sifra == find_id ) {
          pronasao_item_local = item;
        };
    });

    var pronasao_item_local_string = JSON.stringify(pronasao_item_local);
    pronasao_item_local_copy = JSON.parse(pronasao_item_local_string);


    return pronasao_item_local_copy;

  }; // kraj ako je lokalna baza tj lista


  };


  
function open_close_right_side_lists(list_id) {
  
    $(".cit_place_list_here").each( function () {
      if ( this.id !== list_id ) {
        $(this).addClass('cit_close');
      } else {
        $(this).removeClass('cit_close');
      };
    });  
  
};


window.get_all_parks_timestamp = 0;
  
window.get_all_parks_cached = null;


function get_park_list(odakle_zovem) {
  

  // window.all_pp_all_data = null;
  
  
  return new Promise(function(resolve, reject) {
  
  // ako sam već napravio ovaj request u manje od 1 sec !!!
  if ( Date.now() - window.get_all_parks_timestamp < 2*1000 ) {
    // uzmi iz cache-a
    window.all_pp_all_data = window.get_all_parks_cached;
    console.log(' -------------------- uzimam cache od all pp all data -------------------- ')
    
    resolve(window.all_pp_all_data);
    return;
  };
  
  
  console.log(odakle_zovem);
  
  
  // wait_for(`window.cit_user !== null && typeof window.cit_user !== "undefined"`, function() {
    
    var all_pp_list_props = {
      desc: 'za dobivanje globalne liste parkinga',
      local: false,
      url: '/search_park_places',
      return: {},
      query: {},
    };
  

    search_database(null, all_pp_list_props, 'parallel' ).then(
      function( result_array ) {

        if ( !result_array || ( $.isArray(result_array) && result_array.length == 0 ) ) {

          reject('NO PARK LIST ---- array is empty !!!!!');

        } else {

          window.all_pp_all_data = result_array;
          
          
          
          if ( 
            window.all_pp_all_data &&
            $.isArray(window.all_pp_all_data) &&
            window.all_pp_all_data.length > 0
          ) {
            
            window.get_all_parks_cached = window.all_pp_all_data;
            window.get_all_parks_timestamp = Date.now();
            
            // ---- izvlačim sve grupe is special places od svakog parka i postavljam ih u local list  
            // loop po svim parkiinzima
            $.each(window.all_pp_all_data, function( pp_ind, park_obj ) {
              var this_park_special_places = [];
              // prvo pogledaj jel park ima special place
              if ( park_obj.special_places && Object.keys(park_obj.special_places).length > 0 ) {
                
                // ako ima onda izvuci sve special places 
                $.each( park_obj.special_places, function( special_place_key, special_place_object ) {
                  this_park_special_places.push( { sifra: special_place_key, naziv: special_place_key } );
                });
                
                // i stavi ih u local list
                // ali prije toga dodaj "praznu grupu" tj opciju bez grupe
                this_park_special_places.push( { sifra: 'nema_grupu', naziv: 'NEMA GRUPU' } );
                we_local_arr[park_obj.pp_name + '_special_places'] = this_park_special_places;
                
              };
              
            });

          };
          
          
          console.log(window.all_pp_all_data);
          resolve(result_array);
        }

      },
      function( error ) { 
        
        if (error.statusText == 'abort') {
          
          window.all_pp_all_data = window.get_all_parks_cached;
          console.log(' -------------------- uzimam cache od all pp all data -------------------- ')
          resolve(window.all_pp_all_data);
          
        } else {
          reject('Error on get park list !!!');
          console.error(error);
          
        };
      }
    );

  /*  
  }, 1000*30, function() { 
    var poruka = 'User nije ulogiran. Odustajem od park places requesta :(';
    console.error(poruka); 
    def.reject(poruka);
  });  
  */
    
  }); // kraj promisa
    
};

// get_park_list();  










function find_park(arg_id) {
  
  var found_park = null;
  $.each(window.all_pp_all_data, function(pp_index, park) {
    if ( park._id == arg_id ) {
      
      found_park = park;
      
    };
    
  });
  return found_park;
};


function find_pp_name(pp_sifra) {
  var park_name = '';
  $.each(window.all_pp_all_data, function(pp_index, place) {
    if (place.pp_sifra == pp_sifra ) park_name = place.pp_name;
  });
  return park_name;
};


function get_user_products_state(customer_num) {
  
  var def = $.Deferred();
  
  
  if ( !cit_user || !cit_user.user_number ) {
    def.resolve(null);
    return def;
  };
    
  
  var zajednicki_user_num = customer_num;
  
  
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: '/get_user_products_state/' + zajednicki_user_num,
      dataType: "json"
    })
    .done(function (response) {

      if ( response.success == true ) {
        
        console.log( ' ovo je product objekt ' );
        console.log( response )
        
        if ( cit_user && Number(customer_num) == cit_user.user_number ) {
          window.all_user_products = response;
        };
        def.resolve(response);
        
      } else {
        
        def.resolve(null);
        console.error('greška pri preuzimanju producta customera');  
      };

    })
    .fail(function (err) {
      
      def.resolve(err);
      
      console.error('greška pri preuzimanju producta customera');

    });
    
  return def;
  
  
};


function select_result_red(e) {

  
  if ( $(this).closest('#search_autocomplete_container').length == 0 ) return;
  
  if ( $(this).hasClass('header') ) return;
  
  // inače resultate spremam u data property od polja na kojem user traži nešto ili je kliknuo to polje
  var input_results = $('#'+current_input_id).data('we_result');
  
  
  if ( !input_results ) return;
  
  var selected_object = null;
  var sifra = null;
  var _id = null;
  
  var identifier = this.id.replace('list_item_', '');

  
  
  if ( $(this).hasClass('sifra') == true ) {
    sifra = identifier;
  };
    
  if ( $(this).hasClass('_id') == true ) {
    _id = identifier
  };
  
  
  if ( sifra ) {
    $.each(input_results, function(red_index, red) {
      if ( red.sifra == sifra ) selected_object = red;
    });
  };

  if ( _id ) {
    $.each(input_results, function(red_index, red) {
      if ( red._id == _id ) selected_object = red;
    });
  };
  
  fill_cit_input_and_js_objects( selected_object );

  // sakrij rezultate !!!!!
  $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0'
    });

};


var glavni_buttoni_fixni = false;


var solar_animation_throttle_id = null;

// kad user scrolla window onda lista rezultata prati trenutno kliknuto polje
function drop_list_follows_curr_input(e) {
  
  console.log('  -------------- cit_main_area scroll ---------------- ')

  if ( $('#search_autocomplete_container').css('opacity') == "1") {
    var top_inputa = $('#' + current_input_id).offset().top + parseInt( $('#' + current_input_id).css('height') );
    $('#search_autocomplete_container').css('top', top_inputa + 'px');
  };
  
  

  /*
  clearTimeout(solar_animation_throttle_id);
  solar_animation_throttle_id = setTimeout( function() {
  
    if ( $('#pp_solar_panel_card').length > 0 ) {
      if ( $('#pp_solar_panel_card').offset().top < 300 )  {
        get_solar_data();
      };
    };
  }, 1200);
  */

};


function open_close_sidebar_list() {
  
  var arrow = $(this).find('.fa-caret-up');
  // lista se nalazi odmah ispod trake na koju sam kliknuo
  var list = $(this).next();
  if ( list.hasClass('cit_close') ) {
    arrow.css('transform', 'rotateZ(0)')
    list.removeClass('cit_close');
  } else {
    arrow.css('transform', 'rotateZ(180deg)')
    list.addClass('cit_close');
  }
}


function set_switch_html(current_module, switch_id, data_key) {
  
  if ( current_module.cit_data[data_key] == true ) {
    $(switch_id).addClass('on').removeClass('off');
  } else {
    $(current_module).addClass('off').removeClass('on');
  };

};


function set_switch_data(current_module, switch_id, data_key) {
  
  current_module.cit_data[data_key] = $(switch_id).hasClass('on') ? true : false;
  
};


function toggle_cit_switch(arg_state) {

  
  current_input_id = this.id;
  
  var switch_state = null;
  
  if ( arg_state === true ) {
    $(this).removeClass('off').addClass('on');
    switch_state = true;
  };
  
  if ( arg_state === false ) {
    $(this).removeClass('on').addClass('off');
    switch_state = false;
  };

  
  // nemoj ići dalje ako je ova funkcija dobila argument za state
  // ako nije true ili false onda to znači da je ova funkcije trigerirana sa klikom tj eventom
  if ( arg_state !== true && arg_state !== false ) {

    if ( $(this).hasClass('off') ) {
      $(this).removeClass('off').addClass('on');
      switch_state = true;
    } else {
      $(this).removeClass('on').addClass('off');
      switch_state = false;
    };
    
  };
  
  fill_cit_input_and_js_objects( switch_state );
  
};


function show_save_or_edit_button(show_id, hide_id) {
  
  // ako funkciji dam oba idja onda to znači sakrij oba butona
  if ( show_id && hide_id ) {
    $('#' + show_id).css('display', 'none');
    $('#' + hide_id).css('display', 'none');
  } 
  else {
  
    $('#' + show_id).css('display', 'flex');
    
    // hide id mora biti suprotan od show id
    // ako je npr save button vidljiv onda trebam sakriti edit
    var hide_id = show_id.replace('save_', 'edit_');
    // akoje edit vidljiv onda tražim save button
    hide_id = show_id.replace('edit_', 'save_');

    // sakrij ga
    $('#' + hide_id).css('display', 'none');
  };
  
};


function create_ms_at_zero_and_week_day(add_days) {

  var date = new Date();
  var yyyy = date.getFullYear();
  var mnt = date.getMonth();

  var day = date.getDate() + (add_days || 0); // plus or minus days    

  // nedjelja je 0 ---> subota je 6
  var week_day = date.getDay();

  var day_at_zero = new Date(yyyy, mnt, day, 0, 0, 0, 0);
  var ms_at_zero = Number(day_at_zero); //  day_at_zero.getMilliseconds();

  return {
    ms_at_zero: ms_at_zero,
    week_day: week_day
  };

};
  

function get_DATE_as_milisec(input_element, add_days) {
  
  var day_at_zero = 0;
  
  var date = new Date();
  var yyyy = date.getFullYear();
  var mnt = date.getMonth();
  var day = date.getDate() + (add_days || 0); // plus or minus days    

  var date_string = '';
      
  if ( input_element ) date_string = input_element.val();

  var date_je_ok = true;
  
  // ako je user greškom napisao više točaka
  date_string = date_string.replace(/\.\.\./g, ".");
  date_string = date_string.replace(/\.\./g, ".");
  date_string = date_string.replace(/\.\./g, ".");
  
  // NAPOMENA ----> ako je date_string == "" onda će split biti array s jednim praznim itemom [""] !!!!!
  var date_arr = date_string.split('.');
    
  // ako je samo dan ili ako je prazan string
  if ( date_arr.length == 1 && date_string !== '' ) {
    if ( date_arr[0] !== '' ) day = Number(date_arr[0]) + (add_days || 0);
  }; 
  
  // ako je dan.mjesec
  if ( date_arr.length >= 2 ) {
    if ( date_arr[0] !== '' ) day = Number(date_arr[0]) + (add_days || 0);
    if ( date_arr[1] !== '' ) mnt = Number(date_arr[1]) - 1;
  }; 

  // ako je dan.mjesec.godina
  if ( date_arr.length >= 3 ) {
    if ( date_arr[0] !== '' ) day = Number(date_arr[0]) + (add_days || 0);
    if ( date_arr[1] !== '' ) mnt = Number(date_arr[1]) - 1;
    if ( date_arr[2] !== '' ) yyyy = Number(date_arr[2]);
    
  }; 
  
  day_at_zero = new Date(yyyy, mnt, day, 0, 0, 0, 0);
  
  var d = new Date();
  var current_hours   = d.getHours();
  var current_minutes = d.getMinutes();
  var current_seconds = d.getSeconds();
  
  var current_time = new Date(yyyy, mnt, day, current_hours, current_minutes, current_seconds, 0);
  var ms_current_time = Number(current_time);
 
  // nedjelja je 0 ---> subota je 6
  var week_day = day_at_zero.getDay();

  var ms_at_zero = Number(day_at_zero); //  day_at_zero.getMilliseconds();

  return {
    ms_at_zero: ms_at_zero,
    week_day: week_day,
    ms_current_time: ms_current_time
  };

};


function get_time_as_milisec(input_element) {
    
  
  var milisec_time = null;
  
  var time_string = input_element.val();
  
  if ( time_string == '' ) return 0;
  
  var vrijeme_je_ok = true;
  
  if ( time_string.length < 8 ) vrijeme_je_ok = false;
  if ( time_string.length == 2 || time_string.length == 1 ) vrijeme_je_ok = true;
  
  var sat = 0;
  var min = 0;
  var sek = 0;
  
  
  if ( time_string.length == 8 ) {

    time_string = time_string.split(':');

    if (time_string.length !== 3 ) vrijeme_je_ok = false;

    if ($.isNumeric( time_string[0] ) == false ) vrijeme_je_ok = false;
    if ($.isNumeric( time_string[1] ) == false ) vrijeme_je_ok = false;
    if ($.isNumeric( time_string[2] ) == false ) vrijeme_je_ok = false;

    sat = Number(time_string[0]);
    min = Number(time_string[1]);
    sek = Number(time_string[2]);
    
  };
  
  
  if ( time_string.length == 2 || time_string.length == 1  ) {

    if ( $.isNumeric( time_string ) == false ) {
      vrijeme_je_ok = false;
    } else {
      
      if ( time_string.length == 1 ) time_string = '0' + time_string;
      input_element.val(`${time_string}:00:00`);
      
    };
    
    sat = Number(time_string);
    min = 0;
    sek = 0;
    
  };
  
  
  
  if ( sat > 24 || sat < 0 ) vrijeme_je_ok = false;
  if ( min > 59 || min < 0 ) vrijeme_je_ok = false;
  if ( sek > 59 || sek < 0 ) vrijeme_je_ok = false;
  

  
  if ( vrijeme_je_ok == false ) {
    
    input_element.css('border', '1px solid red');
    
  } else {
  
    input_element.css('border', '');
    milisec_time = sek*1000 + min*60*1000 + sat*60*60*1000;
  };
  
  return milisec_time;
  
}


function get_time_as_hh_mm_ss(arg_time, from_or_to) {
  
  if ( !arg_time && arg_time !== 0 ) return '';
  
  var time = arg_time;
  // pošto svako završno vrijeme  smanjim za 200 milisekundi 
  // sada kada vraćam vrijeme nazad u string moram to dodati !!!!!
  
  if (from_or_to == 'to') time = time + 200;
  
  var sat = parseInt( time/(60*60*1000) );
  var modulus_sat = time/(60*60*1000) - sat;
  
  var min = parseInt( modulus_sat*60 );
  var modulus_min = modulus_sat*60 - min;
  
  var sek_string = zaokruziIformatirajIznos(modulus_min*60, 0);
  var sek = parseInt(sek_string) || 0;
  if ( sek == 60 ) sek = 59;
  
  if ( String(sat).length < 2 ) sat = '0' + sat;
  if ( String(min).length < 2 ) min = '0' + min;
  if ( String(sek).length < 2 ) sek = '0' + sek;
   
  var time_string = sat + ':' + min + ':' + sek;
  
  
  return time_string;
  
}


function get_cit_date_from(input_element) {


  if ( !input_element ) return;
  if ( input_element.length == 0 ) return;

  /*
  ------------------------------------
  BITNO:
  Pravilan upis datume je npr 20.04.2018 ili 20.04.2018. <--- dakle može i ne mora biti točka na kraju !!!!
  ------------------------------------
  */

  var datum_upisano_from = $('#trazi_sve_ponude_datum_from').val();

  if (datum_upisano_from == '') return null;

  var start_date = datum_upisano_from.split('.');

  if (start_date.length < 3 ) return 'error';

  var date_num = null;

  var dan = Number( start_date[0] );
  var mjesec = Number( start_date[1] );
  var godina = Number( start_date[2] );

  if ( !$.isNumeric(dan) ) date_num = 'error';
  if ( !$.isNumeric(mjesec) ) date_num = 'error';
  if ( !$.isNumeric(godina) ) date_num = 'error';

  if ( date_num == null ) date_num = new Date(godina, mjesec-1, dan, 0, 0).getTime();

  return date_num;

};


function get_cit_date_to(input_element) {


  if ( !input_element ) return;
  if ( input_element.length == 0 ) return;
  /*
  ------------------------------------
  BITNO:
  Pravilan upis datume je npr 20.04.2018 ili 20.04.2018. <--- dakle može i ne mora biti točka na kraju !!!!
  ------------------------------------
  */

  var datum_upisano_to = $('#trazi_sve_ponude_datum_to').val();

  if ( datum_upisano_to == '' ) return null;

  var end_date = datum_upisano_to.split('.');

  if (end_date.length < 3 ) return 'error';

  var dan = Number( end_date[0] );
  var mjesec = Number( end_date[1] );
  var godina = Number( end_date[2] );

  var date_num = null;

  if ( !$.isNumeric(dan) ) date_num = 'error';
  if ( !$.isNumeric(mjesec) ) date_num = 'error';
  if ( !$.isNumeric(godina) ) date_num = 'error';

  // JAKO BITNO !!!!
  // UVIJEK DODAJ JEDAN DAN PLUS JER PRETRAGA TREBA UZIMATI I CIJELI ZAVRŠNI DAN !!!
  // dakle 20.07.2018  će zapravo biti  kraj tog dana ondosno početak dana poslje  -----> 21.07.2018 00:00
  // ..javascript je pametan i sam se brine o tome da prebaci datum u novi mjesec
  // dakle 32.11. će javascript sam pretvoriti u 01.12. !!!!!

  // i da ... mjesec je nula-based !!!

  if ( date_num == null ) date_num = new Date(godina, mjesec-1, dan+1, 0, 0).getTime();


  return date_num;

};


function is_super() {
  
  var is_super = false;
  
  if ( window.cit_user &&
    window.cit_user._id == '5d9f47f16ce0b535e5441a3a' || /* Circulum user */
    window.cit_user._id == '5e32374d220ba4253d747c0e' /* karlo user */
    ) {       
    is_super = true;
  };
  return is_super;
  
};


function register_autocomplete_listeners() {

  // this is for saving repeating interval to variable so I can cancel it later
  search_delay_id = null;
  // this is to know is already ajax in progress - I dont want to fire multiple parallel requests !!!!!
  

  $('body').off('click', '.cit_list_bar', open_close_sidebar_list);
  $('body').on('click', '.cit_list_bar', open_close_sidebar_list); 
  
  
  $('body').off('click', '.cit_switch', toggle_cit_switch);
  $('body').on('click', '.cit_switch', toggle_cit_switch); 



function handle_we_auto(e) {
    
  var this_value = $(this).val();
    
  var props = current_input_props;
  
  var input_hidden = check_is_input_hidden(this);
  
  if ( input_hidden ) return;
  
  // provjeri da nije stralica gore dolje ili enter ili escape ili tab 
  if (
    e.which !== 38 &&
    e.which !== 40 &&
    e.which !== 13 &&
    e.which !== 27 && 
    e.which !== 9 
  ) {
    
    // ako je iz nekog razloga sakrivena drop lista onda simuliraj klik na trenutno polje tako da se lista pojavi
    if ( $('#search_autocomplete_container').css('opacity') == '0' ) {
      console.log('lista je sakrivena -- pozivam  handle autocomplete click da je pokažem ...');
      $(this).click();
    };
    
  } else {
    
    // ako je gore ili dolje strelica
    if ( e.which == 38 && e.which == 40 ) {
      console.log('prije activate result line with arrow keys:   ' + SEARCH_RESULT_ROW_INDEX);
      activate_result_line_with_arrow_keys(e);
      console.log('POSLJE /////////////////// activate result line with arrow keys:   ' + SEARCH_RESULT_ROW_INDEX);
      return;
    };
      
  };
  
  
  // trezi tek nakon što user upiše 3 znaka
  var min_string_length = 3;
  
  
  if ( this_value.length < min_string_length ) {
    if ( this_value.length == 0 ) reset_cit_data_property();
    return;
  };
  
  var search_string = this_value.toLowerCase();
  
  if ( props.local ) {
    // ako sam našao array koji odgovara ovoj pretrazi znači da imam to već lokalno !!!!
    var local_result = search_local(search_string, props);
    create_cit_result_list( local_result, props );
    
  } 
  else {
    
    search_database(search_string, props).then(
      function( remote_result ) { 
        create_cit_result_list( remote_result, props ); 
      },
      function( error ) { console.error( error ); }
    )
  };

}; // kraj we auto

  
function handle_we_click(e) {

  current_input_id = this.id;
  current_input_props = $(this).data('we_auto');
  
  var props = current_input_props;
  
  if ( !props ) {
    console.error( `input element s idjem ${current_input_id} nema we_auto data !!!!`);
    console.error( `prekidam autocomplete func`);
    return;
  };
  
  SEARCH_RESULT_ROW_INDEX = null;
  
  
  e.stopPropagation();

  var input_hidden = check_is_input_hidden(this);
  
  if ( input_hidden ) return;
  
  var this_input = $(this);
  
  // ako se array nalazi u remote bazi:
  if ( props.local && props.show_on_click ) {
    
    var local_result = search_local(null, props);
    create_cit_result_list( local_result, props );
    
  }; // kraj ako ne postoji local array
  
  
  // ako NIJE local baza nego je u remote bazi
  if ( !props.local && props.show_on_click) {
    
    search_database(null, props).then(
      function( remote_result ) { 
        create_cit_result_list( remote_result, props ); 
      },
      function( error ) { console.error( error ); }
    )
    
  }; // kraj ako je remote search
  
  
}; // KRAJ handle autocomplete click
  
 
  $('body').off('click', '.we_auto', handle_we_click);
  $('body').on('click', '.we_auto', handle_we_click);
  
  $('body').off('keyup', '.we_auto', handle_we_auto);
  $('body').on('keyup', '.we_auto', handle_we_auto);
  

  $('body').off('click', '.cit_input', handle_cit_input_click);
  $('body').on('click', '.cit_input', handle_cit_input_click);

  $('body').off('click', handle_hide_autocomplete_list);
  $('body').on('click', handle_hide_autocomplete_list);


  $('body').off('click', '.cit_search_result_image', handle_click_result_image);
  $('body').on('click', '.cit_search_result_image', handle_click_result_image);    

  
  $('body').off('click', '.search_result_row', select_result_red);
  $('body').on('click', '.search_result_row', select_result_red);
  

};


// default time grupa za graf
// window.solar_time_grupa = {"sifra": "MIN", "naziv": "PO MINUTAMA"};


function create_solar_data_from_ajax(response) {
  
  
  window.last_solar_data = response;
  
 
  if ( response.solar_data_current !== null ) {

    
    if ( response.solar_data_current ) {
      
      setTimeout( function() { 
        window.animate_circle_bar('solar_napon_box', response.solar_data_current.ch_0, 0, 0, sol_napon_max, 'V'); 
      }, 0 );

    };
    
    /*
    setTimeout( function() {
      window.animate_circle_bar('solar_struja_box', response.solar_data_current.ch_1, 0, 0, sol_struja_max, 'A'); 
    }, 400 );

    setTimeout( function() { 
      window.animate_circle_bar('aku_napon_box', response.solar_data_current.ch_2, 0, 0, aku_napon_max, 'V'); 
    }, 800 );

    setTimeout( function() {
      window.animate_circle_bar('aku_struja_box', response.solar_data_current.ch_3, 0, 0, aku_struja_max, 'A'); 
    }, 1200 );
  */

  }; // kraj ifa da current solar data nije null !!!
  
  var solar_napon_points = [];
  var solar_struja_points = [];
  var aku_napon_points = [];
  var aku_struja_points = [];

  $.each(response.solar_data_history, function( solar_ind, solar_obj ) {

      solar_napon_points.push(solar_obj.channels.ch_0);
      solar_struja_points.push(solar_obj.channels.ch_1);
      aku_napon_points.push(solar_obj.channels.ch_2);
      aku_struja_points.push(solar_obj.channels.ch_3);

  }); 

  window.graf_data = {
    time_grupa: (window.solar_time_grupa.sifra || 'MIN' ),
    id: 'graf_solar',
    lines: [
      {id: 'line_solar_napon', color: '#d10000', points: solar_napon_points },
      {id: 'line_solar_struja', color: '#f400d7', points: solar_struja_points },
      {id: 'line_aku_napon', color: '#00770e', points: aku_napon_points },
      {id: 'line_aku_struja', color: '#248efe', points: aku_struja_points }
    ]
  };

  var pp_module = window['/main_modules/parking_places/parking_places_module.js'];
  
  pp_module.create_we_graf(window.graf_data);

  
};


  
function get_solar_data(rampa_name) {

  var pp_module = window['/main_modules/parking_places/parking_places_module.js'];
  
  // ako ništa nije upisano u prvo polje za početni datum
  // onda upiši automatski
  // ako je nešto upisano ili selektirano onda uzmi te vrijednosti
  pp_module.we_graf_setup();
  
  /*
  window.solar_from_date
  window.solar_to_date
  window.solar_time_grupa
  window.we_graf_visible
  */
  

  if (
    window.solar_time_grupa.sifra == 'MIN' &&
    window.solar_to_date - window.solar_from_date > 1000*60*60*24
  ) {
    
    alert(
`
Za rezoluciju po minuti potrebno je izabrati interval unutar jednog dana.
Zbog prevelike količine podataka, veći intervali nisu mogući.
`
    );  
    return;
    
  };  
  
  var graf_query = {
    ime_rampe: rampa_name,
    from_time: window.solar_from_date,
    to_time: window.solar_to_date,
    time_grupa: window.solar_time_grupa.sifra
  };
  
  
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "POST",
      url: "/get_solar_data",
      data: JSON.stringify(graf_query),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {

      console.log(response);

      // prvo očisti arrays za točke
      var solar_napon_points = [];
      var solar_struja_points = [];
      var aku_napon_points = [];
      var aku_struja_points = [];

      /* -------------------------- SAMO ZA POTREBE TESTIRANJA -------------------------- */

      /*
      var dummy_history = [];

      var response = {};


      for(p = 1; p <= 24; p++) {

        var curr_time = window.solar_from_date + (p * cit_rand_min_max(1000*60,1000*60*60) );
        if (curr_time > window.solar_to_date ) curr_time = window.solar_to_date;

        var p1 = cit_rand_min_max_float(22,23);
        var p2 = cit_rand_min_max_float(4,5);
        var p3 = cit_rand_min_max_float(19,20);
        var p4 = cit_rand_min_max_float(2,3);

        p1 = zaokruzi(p1, 2);
        p2 = zaokruzi(p2, 2);
        p3 = zaokruzi(p3, 2);
        p4 = zaokruzi(p4, 2);



      var new_unit_solar_object = {
        frequency: 'SAT',
        channels: {
          ch_0: 0,
          ch_1: 0,
          ch_2: 0,
          ch_3: 0
        },
        time: curr_time,
        rampa_name: 'rampa_tzm',
        count: null,
      };



        new_unit_solar_object.channels.ch_0 = p1;
        new_unit_solar_object.channels.ch_1 = p2;
        new_unit_solar_object.channels.ch_2 = p3;
        new_unit_solar_object.channels.ch_3 = p4;


        var new_unit_solar_object = JSON.stringify(new_unit_solar_object);
        new_unit_solar_object = JSON.parse(new_unit_solar_object);

        dummy_history.push(new_unit_solar_object);

      }; // kraj for loop-a

      
      for( g = 2; g <= 10; g++ ) {
        dummy_history[g].channels.ch_0 = null;
        dummy_history[g].channels.ch_1 = null;
        dummy_history[g].channels.ch_2 = null;
        dummy_history[g].channels.ch_3 = null;
      };
      
      // sortiraj uzlasno od manjeg vremena prema većem
      if ( dummy_history.length > 1 ) dummy_history.sort( window.by('time', false, Number ) );
      
      response.solar_data_history = dummy_history;
      
      create_solar_data_from_ajax(response);
      
      return;
      
      */
      
      /* -------------------------- SAMO ZA POTREBE TESTIRANJA -------------------------- */
      

      if ( response.success == true ) {
        create_solar_data_from_ajax(response);
        // setTimeout( function() { window.animate_we_graf_lines(window.graf_data); }, 600 );  
      } 
      else {
        popup_error_solar_data();
      };
    })
    .fail(function(error) {
      popup_error_solar_data();
      console.error(error);
    });
    
  

};


function set_obican_user_parking(park, state) {
  
  
  var obican_user_parking_array = window.localStorage.getItem('obican_user_parking_array');
  if ( obican_user_parking_array !== null ) {
    obican_user_parking_array = JSON.parse(obican_user_parking_array);
  } else {
    obican_user_parking_array = [];
  };
  

  // AKO VEĆ POSTOJI OVA PP SIFRA ONDA JU OBRIŠI --------------------------
  // to radim uvijek bez obzira jel argumnet state true ili false !!!!! --------------------------
  
  var obrisi_ovaj_index = null;
  $.each(obican_user_parking_array, function(index, pp_sifra) {
    if ( pp_sifra == park.pp_sifra ) obrisi_ovaj_index = index;
  });
  if ( obrisi_ovaj_index !== null ) obican_user_parking_array.splice(obrisi_ovaj_index, 1);
  
  
  // samo ako je state true onda dodaj sifru od parkinga
  if ( state == true ) obican_user_parking_array.push(park.pp_sifra);

  // spremi nove izmjene nazad u local storage
  window.localStorage.setItem('obican_user_parking_array', JSON.stringify(obican_user_parking_array) );
  
  
};

 


function get_active_booking_or_parking( pp_sifra, user_num ) {

  
  return new Promise( function( resolve, reject ) { 

    if ( !user_num ) {
      console.error('ERROR get active book an park ------> nema argumenta user num !!!!');
      reject(false);
      return;
    };

    $.ajax({
        type: 'GET',
        url: ( '/get_active_booking_or_parking/' + pp_sifra + '/' + user_num),
        dataType: "json"
    })
    .done(function(response) {

        console.log(response);

        if ( response.success == true ) {

          // nije potrebno da radim ovo odmah jer trebam sačekati animaciju tj da se uopće generira HTML !!!!!
          // if ( park.pp_sifra !== 'all' ) setup_park_page_GUI(response, park);

          resolve(response);

        } else {

          reject(false);

          if ( response.msg && window[response.msg] && typeof window[response.msg] == 'function' ) {
            window[response.msg](); 
          }
          else {
            // ako msg nije nikakav popup koji sam prepoznao onda pokreni ovaj općeniti popup !!!!
            popup_greska_prilikom_parkinga();
          };

        };
    })
    .fail(function(error) {
      
      console.error("WE_SITE Error on get active book and park func !!!");

      if ( error.msg && window[error.msg] && typeof window[error.msg] == 'function' ) {
        window[error.msg]();
      } else {
        // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
        popup_greska_prilikom_parkinga();
      };
      
      reject(error);

    }); // kraj ajax

  }); // kraj promisa
  
}; // kraj get active booking or parking

 



$(document).ready(function () {


  register_autocomplete_listeners(); 


});
