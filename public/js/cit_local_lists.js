var we_local_arr = {

  proba_find: [
    {"sifra": "OIKD001","naziv": "Text 001"},
    {"sifra": "OIKD002","naziv": "Text 2"},
    {"sifra": "OIKD003","naziv": "Text 3"},
    {"sifra": "OIKD004","naziv": "Text 4"},
    {"sifra": "OIKD005","naziv": "Text 5"},
    {"sifra": "OIKD006","naziv": "Text 6"},
  ],
  
  user_tags: [
    {"sifra": "TELE2","naziv": "TELE 2"},
    {"sifra": "SIGNIFY","naziv": "SIGNIFY"},
    {"sifra": "STED","naziv": "ŠTED GRUPA"},
    {"sifra": "ZCZZ","naziv": "ŽUPANIJSKE CESTE"},
    {"sifra": "MM","naziv": "MICRO MACRO"},
    {"sifra": "VLASNIK","naziv": "VLASNIK"},
    {"sifra": "PRETPLATA","naziv": "PRETPLATA"},
    
    
    
    {"sifra": "nema_grupu","naziv": "NEMA GRUPU"},

  ],
    
  cit_select_days: [
      {"sifra": "any_day","naziv": "BILO KOJI DAN"},
      {"sifra": "1","naziv": "PONEDJELJAK"},
      {"sifra": "2","naziv": "UTORAK"},
      {"sifra": "3","naziv": "SRIJEDA"},
      {"sifra": "4","naziv": "ČETVRTAK"},
      {"sifra": "5","naziv": "PETAK"},
      {"sifra": "6","naziv": "SUBOTA"},
      {"sifra": "0","naziv": "NEDJELJA"}
    ],
  
  kanal_akvizicije : [
    {"sifra": "AKV001","naziv": "MARKO"},
    {"sifra": "AKV002","naziv": "PERO"},
    {"sifra": "AKV003","naziv": "JOZO"},
    {"sifra": "AKV004","naziv": "IVICA"},
    
  ],
  
  tip_karte_hr : [
    {"sifra": "VOU","naziv": "VOUCHER"},
    {"sifra": "MON","naziv": "MJESEČNA KARTA"},
  ],
  
  stats_grupiranje_hr : [
    {"sifra": "TICKET","naziv": "PO TIPU KARTE"},
    {"sifra": "TAG","naziv": "PO TAGU"},
    {"sifra": "WEEKDAY","naziv": "PO DANU U TJEDNU"},
  ],  
  
  
  time_grupe : [
    {"sifra": "SAT", "naziv": "PO SATIMA"},
    {"sifra": "DAN", "naziv": "PO DANIMA"},
    {"sifra": "TJEDAN", "naziv": "TJEDNO"},
    {"sifra": "MJESEC", "naziv": "MJESEČNO"},
  ],   
  
  
    
  solar_time_grupe : [
    {"sifra": "MIN", "naziv": "PO MINUTAMA"},
    {"sifra": "SAT", "naziv": "PO SATIMA"},
    {"sifra": "DAN", "naziv": "PO DANIMA"},
    /*
    {"sifra": "TJEDAN", "naziv": "TJEDNO"},
    {"sifra": "MJESEC", "naziv": "MJESEČNO"},
    */
  ],
  
  we_graf_visible : [
    {"sifra": "SVI", "naziv": "SVI GRAFOVI"},
    {"sifra": "SOLAR_N", "naziv": "NAPON SOLAR"},
    {"sifra": "SOLAR_S", "naziv": "STRUJA SOLAR"},
    {"sifra": "AKU_N", "naziv": "NAPON AKUMULATOR"},
    {"sifra": "AKU_S", "naziv": "STRUJA AKUMULATOR"},
  ],  
  
  stats_book_or_park: [
    
    {"sifra": "BOTH","naziv": "REZERVACIJE i PARKING"},
    {"sifra": "BOOK","naziv": "SAMO RESERVIRANO"},
    {"sifra": "PARK","naziv": "SAMO PARKING"},
    
  ],  
  
  
    
};