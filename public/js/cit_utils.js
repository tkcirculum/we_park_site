// ADDITIONAL OPTIONS FOR NATIVE JAVASCRIPT SORT ------- from http://jsfiddle.net/gfullam/sq9U7/  
function by(path, reverse, primer, then) {
  var get = function (obj, path) {
      if (path) {
        path = path.split('.');
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        };
        return obj[path[len]];
      }
      return obj;
    },
    prime = function (obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function (a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B) ? -1 :
      (A > B) ? 1 :
      (typeof then === 'function') ? then(a, b) : 0
    ) * [1, -1][+!!reverse];
  };
};


// remove all non-printable characters like [ \r\n\t\f\v ] 
// but only in front of text on each line
// I use this because of ugly white space when inserting html string templates !!!!!
function vd_remove_white_space(html) {

  var new_html = '';
  var split_html = html.split('\n');

  $.each(split_html, function (index, line) {

    // remove all wierd non-printable characters from line start
    var new_line = line.replace(/^\s+/gm, '');
    // trim the back of the line also
    new_line = new_line.trim();

    if (new_line.length > 0) {
      new_html += new_line + ' '; // maybe use '\n' for better readability ???? TODO !!!
    };

  });

  // trim whole html just to be sure
  new_html = new_html.trim()
  // remove white space in fron of new element & behind element
  new_html = new_html.replace(/ </g, '<');
  new_html = new_html.replace(/> /g, '>');

  return new_html.trim();

};


function cit_white_space_trim(x) {
  return x.replace(/^\s+|\s+$/gm,'');
};


function getMeADateAndTime(number, cit_format) {
  var today = null;
  
  // ako nije undefined ili null onda napravi datum od broja 
  if ( typeof number !== 'undefined' && number !== null ) {
    today = new Date( number );  
  } else {
    today = new Date();  
  };
  
  var sek = today.getSeconds();
  var min = today.getMinutes();
  var hr = today.getHours();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; // January is 0!
  var yyyy = today.getFullYear();

  // ako je samo jedna znamenka
  if (dd < 10) {
    dd = '0' + dd;
  };
  // ako je samo jedna znamenka
  if (mm < 10) {
    mm = '0' + mm;
  };

  // ako je samo jedna znamenka
  if (sek < 10) {
    sek = '0' + sek;
  };


  // ako je samo jedna znamenka
  if (min < 10) {
    min = '0' + min;
  };

  // ako je samo jedna znamenka
  if (hr < 10) {
    hr = '0' + hr;
  };

  if ( cit_format == 'd.m.y' ) {
    today =  dd + '.' + mm + '.' + yyyy;
  };

  
  // ako nije neveden format ili ako je sa crticama
  if ( cit_format == 'y_m_d' ) {
    today = yyyy + '_' + mm + '_' + dd;
  };  
  
  
  // ako nije neveden format ili ako je sa crticama
  if ( !cit_format || cit_format == 'y-m-d' ) {
    today = yyyy + '-' + mm + '-' + dd;
  };
  
  

  
  
  time = hr + ':' + min + ':' + sek;
  
  // EXPERIMENTAL !!!!!!:
  // stavio sam mogućnost da mi upiše datum / vrijeme na svim elementima koji imaju 
  // istu klasu kao ove - tj ako u html-u stoji element sa ovim klasama i ako na onload pokrenem ovu funckiju
  // sadržaj svakog elementa će dobiti datum / vrijeme ovisno o klasi koju ima

  // $(".getMeADate").html(today);
  // $(".getMeATime").html(time);

  return {
    datum: today,
    vrijeme: time
  }

};


function zaokruziIformatirajIznos(num, zaokruzi_na) {
  // primjer : var num = 123456789.56789;

  var brojZnamenki = 2;
  if ( typeof zaokruzi_na !== 'undefined' )  brojZnamenki = parseInt( zaokruzi_na );
  

  var zaokruzeniBroj = zaokruzi(num, brojZnamenki);
  

  var stringBroja = String(zaokruzeniBroj);
  // 123456789
  var lijeviDio = stringBroja.split('.')[0] + "";
  var tockaPostoji = stringBroja.indexOf(".");
  var desniDio = "00";
  if (tockaPostoji > -1) {
    desniDio = stringBroja.split('.')[1] + "";
  };

  /*
  if (Number(desniDio) < 10) {
    desniDio = '0' + Number(desniDio);
  };
  */

  
  var broj_je_pozitivan = true;
  
  // ako je broj negativan 
  if ( lijeviDio.substring(0,1) == '-' ) {
    lijeviDio = lijeviDio.substring(1);
    broj_je_pozitivan = false;
  };
  
  var stringLength = lijeviDio.length;
  
  // ne zanimaju me brojevi s tri znamenke i manje jer oni nemaju točke
  if (stringLength >= 4) {
    // pretvaram string u array
    lijeviDio = lijeviDio.split('');
    // gledam koliko stane grupa po 3 znamaneke
    // i onda uzmem ostatak - to je jednostavna modulus operacija
    var dotPosition = stringLength % 3;
    // zanima me koliko treba biti točaka u broju
    // to dobijem tako da podijelim broj znamenki sa 3 i gledam samo cijeli broj
    var dotCount = parseInt(stringLength / 3)

    // postavim prvu točku
    // NAPOMENA u slučajevima kada je modulus = 0 ova funkcije postavi točku čak i na počatek broja
    // tu točku kasnije obrišem
    // svaka sljedeća točka je UVIJEK 4 pozicije udesno !!!!
    // napomena - logično bi bilo da je svake 3 pozicije udesno, 
    // ali moram brojati i točku koju sam stavio i ona povećava length od stringa
    for (i = 1; i <= dotCount; i++) {
      // stavi točku
      lijeviDio.splice(dotPosition, 0, ".");
      // id na poziciju current + 4 
      dotPosition = dotPosition + 4;
    };
    
    
    // spoji nazad sve znakove u jedan string !!!
    lijeviDio = lijeviDio.join('')

    // kad se dogodi da je modulus === 0 onda postavim točku na nultu poziciju
    // i sad je moram obrisati !!!!!!!
    if (lijeviDio.charAt(0) === ".") {
      // uzimam sve osim prvog znaka tj točke
      lijeviDio = lijeviDio.substring(1);
    }
  };

  // ponovi nulu onoliko puta koliko treba da decimale imaju onoliko stringova 
  // koliko je definirano sa argumentom [zaokruzi_na] 
  // ( ... od kojeg gore dobijem varijablu brojZnamenki )
  if (desniDio.length < brojZnamenki) {
    desniDio = desniDio + '0'.repeat(brojZnamenki - desniDio.length);
  };

  
  var formatiraniBroj = lijeviDio + "," + desniDio;

  if ( desniDio == "00" &&  brojZnamenki == 0 )  formatiraniBroj = lijeviDio;
  
  if ( broj_je_pozitivan == false ) formatiraniBroj = '-' + formatiraniBroj;
  
  return formatiraniBroj;
  
};

/* /////////////////////////    ZAOKRUŽI BROJ   ///////////////////////////////////// */

function zaokruzi(broj, brojZnamenki) {

 var broj = parseFloat(broj);

 if ( typeof brojZnamenki == 'undefined' ) {
   brojZnamenki = 0;
 };
 var multiplicator = Math.pow(10, brojZnamenki);
 broj = parseFloat((broj * multiplicator).toFixed(11));
 var test =(Math.round(broj) / multiplicator);
 // + znak ispred je samo js trik da automatski pretvori string u broj
 return +(test.toFixed(brojZnamenki));
};


function find_user_full_ime_po_broju(broj_usera) {
  var ime_usera = null;
    $.each(brojevi_svih_usera_GLOBAL, function (index_usera, user) {
      if ( user.user_number == Number(broj_usera) ) {
        ime_usera = user.full_name;
      };
    });
  
if (ime_usera) return ime_usera;
};

