function MAKE_STORE(store_data) {

  var cit_store = store_data || {};
  
  function obj_prop_or_arr_item(curr_prop) {
    
    var curr_type = 'obj_prop';
    if ( $.isNumeric(curr_prop) == true  ) curr_type = 'arr_item';
    return curr_type;
    
  };
  
  var cit_store_history = [];

 
  this.history = function() {
    
    // deep copy od historija
    var cit_store_history_copy = JSON.parse( JSON.stringify(cit_store_history) );
    // deep copy od trenutnog stanja
    var cit_store_copy = JSON.parse( JSON.stringify(cit_store) );
    
    // dodaj kopiju trenutnog stanja u history 
    cit_store_history_copy.push(cit_store_copy);
    
    return cit_store_history_copy;
  };  
    
  this.find = function(property_chain, is_update) {
    
    // init action da je false
    var find_success = false;

    if (!property_chain) {
      console.error('OBAVEZNO UPISATI PROPERTY CHAIN ZA CIT STORE !!!');
      return;
    };
  
    var prop_dots = property_chain.replace(/\[/g,'.' );
    var prop_dots = prop_dots.replace(/\]/g,'' );
    // dobijem 'mama.tata.0.arr.2'
    var prop_dots = prop_dots.replace(/\.\.\./g,'.' );
    var prop_dots = prop_dots.replace(/\.\./g,'.' );
    // brišem višak točaka (ako ih ima) i dobijem
    // 'mama.tata.0.arr.2'


    var prop_dots = prop_dots.split('.');
    
    if ( prop_dots[prop_dots.length - 1] == '' ) prop_dots.splice(prop_dots.length - 1, 1);
    if ( prop_dots[0] == '' ) prop_dots.splice(0, 1);

    var found_prop = cit_store;
    var curr_chain = 'cit_store';
    var curr_prop_copy = null;

    $.each(prop_dots, function(prop_ind, curr_prop) {

      curr_prop_copy = curr_prop;
      
      curr_chain += ('.'+curr_prop);
      
      if ( found_prop && curr_prop !== '' ) {

        var prop_type = obj_prop_or_arr_item(curr_prop);

        // ako je ovo array onda ce curr prop biti broj npr arr[2]
        if ( prop_type == 'arr_item' ) curr_prop = Number(curr_prop);
        
        
        // ako je zadnji propery onda je uspješno pronašao property !!!!!!!!!
        
        if ( prop_ind == prop_dots.length - 1 ) {

          find_success = true;
          
        } 
        else {
          // ako nije zadnji property u string chainu onda 
          // pretvory found prop u njegov child object
          // tj pomakni se jednu kariku u chainu 
          found_prop = found_prop[curr_prop];

        };
        
        
        // kraj ako trenutni property chain postoji  
        // i ako sam property string nije ""
      } 
      else {
        console.error(curr_chain + ' NE POSTOJI !!!!! ');
      };

    }); // kraj loopa po svim stringovima unutar string chain-a
    
    if ( find_success == true ) find_success = found_prop[curr_prop_copy];
    
    if ( is_update && find_success !== false ) {
      
      return {
        find_success: find_success,
        found_prop: found_prop,
        curr_prop: curr_prop_copy
      }
      
    } else if ( is_update && find_success === false ) {
      return false;
      
    } else if ( !is_update ) {
      return find_success;
    };

  };
  
  
  
  this.update = function(property_chain, new_data, desc, action_type ) {
    
    
    var update_success = false;
    
    // 
    if ( typeof new_data == 'undefined' && action_type !== 'DELETE' ) {
      console.error('OBAVEZNO UPISATI NEW DATA ZA UPDATE CIT STORE-a !!!');
      return;
    };  


    if (!desc) {
      console.error('OBAVEZNO UPISATI DESC ZA UPDATE CIT STORE-a !!!');
      return;
    };
    
    // if ( !eval('cit_store.'+property_chain) && action_type == 'DELETE' ) {
    /*
    if ( data_to_update == 'property_chain_error' && action_type == 'DELETE' ) {
      console.error(property_chain + ' NE POSTOJI i NE MOGU GA OBRRISATI -a !!!');
      return;
    };
    */
    
    var data = this.find(property_chain, true);
    
    if ( !data ) return;
    
    var found_prop = data.found_prop;
    var curr_prop = data.curr_prop;
    
    
    desc = (action_type == 'DELETE' ? 'DELETE ' : '') + property_chain +  ' >>>>> ' + desc;

    console.log(`%c CHAIN: ${property_chain} STORE UPDATE DESC: ${desc} `, "background-color: green; color: white;"); 
    
    var cit_store_copy = JSON.stringify(cit_store);
    cit_store_history.push( JSON.parse(cit_store_copy) );

    // ako array od historja ima više od 10 itema onda izbaci prvi tj. najstariji
    if ( cit_store_history.length > 10 ) cit_store_history.shift();
    
    
    // ako property VEĆ POSTOJI !!!
    if ( typeof found_prop[curr_prop] !== 'undefined' ) {

      // ako nema action type to znači da ga tertiram kao EDIT ili novi upis !!!!!

      if ( !action_type ) {
        found_prop[curr_prop] = new_data;
        update_success = true;
        cit_store.desc = desc;
      };

      if ( action_type == 'DELETE' ) {

        if ( prop_type == 'arr_item' ) {
          // delete array item
          found_prop.splice(curr_prop, 1);
          update_success = true;
          cit_store.desc = desc;
        } 
        else if ( prop_type == 'obj_prop' ) {

          // delete object property
          delete found_prop[curr_prop];
          update_success = true;
          cit_store.desc = desc;
        };

      }; // kraj ako je DELETE


    // kraj ako ovaj property postoji  
    } 
    else {

      // ako property ne postoji !!!!!
      if ( prop_type == 'arr_item' ) {

        if ( curr_prop !== found_prop.length ) {
          // ako NE POSTOJI !!! current property
          // nije bitno jel action type EDIT ILI DELETE
          // ako je user u string chainu naveo krivi index array-a
          // javit ću mu grešku bez obzira na action type


          console.error(
            'KRIVI INDEX OD ARRAYA - OVAJ ARRAY IMA '
            + (found_prop.length-1)
            + ' ITEMA, STOGA SLJEDEĆI INDEX TREBA BITI '
            + found_prop.length + ' A SADE JE ' + curr_prop
          );
          console.error('OVO JE PROPERTY CHAIN: ' + property_chain);

        } else {
          // ako je user upisao sljedeći index po redu tj kako treba
          // onda samo push data to array
          if ( action_type !== 'DELETE' ) {
            found_prop.push(new_data);
            update_success = true;
            cit_store.desc = desc;

          } else {
            console.error(
              `
              NE MOGU OBRISATI ITEM S INDEXOM ${curr_prop}
              JER TAJ INDEX NE POSTOJI ----> 
              ARRAY IMA MAX INDEX ${found_prop.length-1} :(
              `
            );
          }; // kraj ako je/nije DELETE ITEMA U ARRAJU

          // kraj provjere jel ovo sljedeći index u arraju 
          // npr array ima 3 itema i indexe od 0 do 2 i sada user upiše index 3 tj 4. član arraja  
        };

      // kraj ako je array 
      } 
      else if ( prop_type == 'obj_prop' ) {

        if ( !action_type ) {
          found_prop[curr_prop] = new_data;
          update_success = true;
          cit_store.desc = desc;
        };

        if ( action_type == 'DELETE' ) {
          console.error(`PROPERTY ${property_chain} NE POSTOJI I NE MOGU GA OBRISTAI !!!`);
        };

      }; // kraj ako je property

    }; // kraj ako property  za update ne postoji

    if ( update_success == true ) update_success = found_prop[curr_prop];
    
    return update_success;
    
    
  };
  
}; // END OF MAKE STORE


// test data
 var init_data = {
  desc: 'FIRST STATE',
  mama: {
    tata: [
      { arr: [333, 444, 555] },
      20,
      30
    ]
  }
};

var STORE_1 = new MAKE_STORE(init_data);

console.log( STORE_1 );

console.log( STORE_1.get().mama.tata[0].arr[2] );

STORE_1.update('mama.tata[0].arr[2]', 'umjesto 555', 'opis');

console.log( STORE_1.get().mama.tata[0].arr[2] );
