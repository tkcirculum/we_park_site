
var square_spinner_visible = false;

/* ////////////////////////// ESCAPING HTML SO I CAN SAVE IT AS STRING IN JSON  //////////////////////////*/
function escapeHtml(html) {
  var div = document.createElement('div');
  div.appendChild(document.createTextNode(html));
  return div.innerHTML;
};


/* ////////////////////////// UN-ESCAPING STRING FROM JSON SO I CAN PLACE IT AS HTML  //////////////////////////*/
function unescapeHtml(string) {
  var div = document.createElement('div');
  div.innerHTML = string;
  var child = div.childNodes[0];
  return child ? child.nodeValue : '';
};


/* ///////////////////////////  TOGGLE SPINNER  /////////////////////////// */
var show_main_repuzzlic_logo = null;


clearTimeout(show_main_repuzzlic_logo);

show_main_repuzzlic_logo = setTimeout(function() {
  $("#main_area_logo").addClass('show');
}, 1000);


function toggle_square_spinner(show_or_hide) {

  if (window.SHOW_SPINNER == false) show_or_hide = 'hide';

  if (show_or_hide == 'show') {

    // $("#main_area_logo").removeClass('show');

    square_spinner_visible = true;
    /* -----------START ------------------  SHOW SPINNER -----------------------------*/
    $('#square_spinner').velocity({
      opacity: [1, 0],
      size: [1, 0.8],
    }, 300, "swing");

    $("#layer_below_modal").css({
      'opacity': '1',
      'pointer-events': 'all'
    });

    /* -----------END ------------------  SHOW SPINNER  -----------------------------*/
  } 
  else if ( show_or_hide == 'hide') {

    square_spinner_visible = false;
    /* -----------START ------------------  HIDE SPINNER  -----------------------------*/
    $('#square_spinner').velocity({
      opacity: [0, 1],
      size: [0.8, 1],
    }, 200, "swing");

    if ( u_toku_generiranje_dokumenta == null ) {
      $("#layer_below_modal").css({
        'opacity': '0',
        'pointer-events': 'none'
      });
    };

    /* -----------END ------------------  HIDE SPINNER  -----------------------------*/


    clearTimeout(show_main_repuzzlic_logo);
    show_main_repuzzlic_logo = setTimeout(function() {
      $("#main_area_logo").addClass('show');
    }, 1000);


  };

}; // END OF toggle_square_spinner() function

function cit_place_component(parent_element, component_html, placement) {

    if ( !placement ) parent_element.html(component_html);
    if ( placement == 'prepend' ) parent_element.prepend(component_html);
    if ( placement == 'append' ) parent_element.append(component_html);
    if ( placement == 'after' ) parent_element.after(component_html);
    if ( placement == 'before' ) parent_element.before(component_html);

};
  

function get_cit_module(url, load_css) {

  var prom = $.Deferred();
  
  var url_module_id = url.replace(/\//g, '_');
  url_module_id = url_module_id.replace(/\./g, '_');
  
  var css_module_id = url_module_id.replace('_js', '_css');
  var css_url = url.replace('.js', '.css');

  
  if ( load_css == 'load_css' && $('#' + css_module_id).length == 0 ) {
    var css_link =
    `<link rel="stylesheet" id="${css_module_id}" href="${css_url}">`;
    $("head").append(css_link);
  };
  
  // ako ne postoji script tag sa ovim idjem onda ga napravi i ubaci u body
  if ($('#' + url_module_id).length == 0) {

    $.ajax({
        type: "GET",
        url: url,
        dataType: "script",
        cache: true,
      })
      .done(function (module) {

        var module_code =
`
<script id="${url_module_id}" type="text/javascript">
(function () {
  window['${url}'] = {};

  var module_url = '${url}';

  ${module}

  $.each(module_object, function(key, item) {
    window['${url}'][key] = item;
  });

}());

//# sourceURL=${url}
</script>
`;

        $('body').append(module_code);
        
        // prilikom prvog loadanja pokreni sve funkcije unutar scripts propertija
        wait_for( `typeof  window["${url}"] !== "undefined"`, function() {
          if ( window[url].scripts ) window[url].scripts();
          prom.resolve(window[url]);
        }, 50000);

      })
      .fail(function (err) {
        alert("GREŠKA PRI UČITAVANJU MAIN MODULA !!!!!");
        prom.reject(err);
      });

  }
  else {
    // iako je ovaj modul već loadiran trebam ponovo pokrenuti funkciju koja se nalazi u properiju scripts
    if ( window[url].scripts ) window[url].scripts();
    prom.resolve(window[url]);
  };
    
  return prom;
  
};


$(document).ready(function () {
  
  toggle_global_progress_bar(true); 
  
  var loaded_count = 0;
  var loaded_comp_array = [];
    
  var all_cit_comps = [
    '/main_area/site_popup_modal/site_popup_modal.js',
    '/main_area/on_board_card/on_board_card.js',
    '/right_sidebar/cit_search_list/park_places/cit_pp_list_module.js',
    '/cit_components/cit_switch/cit_switch_module.js'
  ];  
    
  // LOAD ALL MODULES FOR GENERAL COMPONENTS
  $.each(all_cit_comps, function (index, module_url) {
    get_cit_module(module_url, 'load_css');  
  });
  
  function all_components_loaded() {
    
    var all_comps_loaded = false;
    
    loaded_count = 0;
    loaded_comp_array = [];
    
    $.each(all_cit_comps, function(index, url) {
      if ( window[url] && window[url].cit_loaded ) {
        loaded_count += 1;
        loaded_comp_array.push(url);
      };
    });
    if (all_cit_comps.length == loaded_count ) all_comps_loaded = true;
    return all_comps_loaded ;
  };
  
  wait_for( 
    all_components_loaded, 
    function() {
      
      if ( !window.cit_main_area_perfectbar ) { 
        window.cit_main_area_perfectbar =  new PerfectScrollbar( $('#cit_main_area')[0] );
      } else {
        window.cit_main_area_perfectbar.update();
      };
      
      if ( window.cit_main_area_perfectbar ) {
        $('#cit_main_area')[0].removeEventListener('ps-scroll-y', drop_list_follows_curr_input);
        $('#cit_main_area')[0].addEventListener('ps-scroll-y', drop_list_follows_curr_input);
      };  
      
    
    toggle_global_progress_bar(false); 
    
    console.log('ALL CIT COMPONENTS SUCCESSFULLY LOADED HOORAY !!!!'); 
    
    var cit_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'];
    cit_popup_modal.create('proba', $('body'), 'prepend');
    
    get_cit_module('/left_sidebar/left_sidebar_module.js').then(function (left_sidebar_module) {

      left_sidebar_module.create('proba', $('#cit_left_sidebar') );

    });

    get_cit_module('/right_sidebar/right_sidebar_module.js', 'load_css').then(function (right_sidebar_module) {
      right_sidebar_module.create('proba', $('#cit_right_sidebar'));
    });  
    
},
    ( 1000 * 60 * 5  ), // timeout is 2 min
    function() { 
      alert('Neki se moduli/componente nisu učitale !!!!');
      toggle_global_progress_bar(false); 
      var missing_modules = [];

      $.each(all_cit_comps, function(index, url) {
        var url_missing = true;
        $.each(loaded_comp_array, function(index, loaded_url) {
          if ( url == loaded_url ) url_missing = false;
        });
        if ( url_missing == false ) missing_modules.push(url);
      });

      console.log(missing_modules);

    }
  );    


}); // END OF $(document).ready ....