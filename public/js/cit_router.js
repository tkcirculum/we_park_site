window.history.pushState({page: 'start'}, "START PAGE");

var we_hash = window.location.hash;


window.onpopstate = function(event) {
  
  
  if ( event.state == null && !window.location.hash ) {
    
    // alert('sada js state null !!!');
    
    if( confirm('ŽELITE LI IZAĆI IZ APLIKACIJE ???') ) {
      window.history.back();
    } else {
      window.history.forward();
    };

  };
  
  
  if ( event.state && event.state.page && event.state.page == 'start') {
    
    // alert('PONOVO JE NA STARTU');
    
  } else if (event.state) {
    
    alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
    
  };
  
};



function create_and_open_park_places(placement) {
  
  // resetiram varijablu koja pokazuje jel označavanje koordinata na karti omogućeno ili ne !!!!
  window.we_set_map_crosshair = null;

  // deaktiviraj sve tipke na ljevom meniju
  $('.left_menu_button').removeClass('cit_active');
  // aktiviraj samo button za parking places
  $('#left_menu_button_parking_places').addClass('cit_active');

  get_park_list('pozivam get park list unutar func create and open park places');

  open_close_right_side_lists('park_places_list_and_find');

  get_cit_module('/main_modules/parking_places/parking_places_module.js', 'load_css').then(
    function(parking_places_module) { 
      parking_places_module.create( null, $('#cit_main_area'), placement );

      // resetiraj cit data od park places
      setTimeout( function() {
        parking_places_module.get_empty_cit_data();
      }, 50);

    }); // kraj then od get cit module

}; 
  

function cit_module_router() {

  if ( we_hash ) {
    
    // -------------------------- PROTIR MODULE  --------------------------
    // -------------------------- PROTIR MODULE  --------------------------
    // -------------------------- PROTIR MODULE  --------------------------
    
    if ( we_hash.indexOf('portir_control') > -1 )  {
      
      wait_for(`typeof window.cit_user !== 'undefined' && window.cit_user !== null`, function() {
        // ako je heinzelova
        if ( we_hash.indexOf('/14') > -1 ) {
          if ( window.cit_user.user_number == 777 ) alert( 'Heinzelova portir_control !!!!!' );
        };
        // ako je prisavlje
        if ( we_hash.indexOf('/3') > -1 ) {
          if ( window.cit_user.user_number == 777 ) alert( 'Heinzelova portir_control !!!!!' );
        };
        
      }, 1000*50 );
      
    };
    
  } else {
    
    // ovo je default kada nema hash-a u url-u !!!!!!
    
    // -------------------------- PARK PLACE MODULE  --------------------------
    // -------------------------- PARK PLACE MODULE  --------------------------
    // -------------------------- PARK PLACE MODULE  --------------------------
    
    create_and_open_park_places();
  };
  
};


function uvijet_za_module_selection() {
  return (
    typeof cit_user !== 'undefined' &&
           cit_user !== null        &&
    $('#left_menu_button_parking_places').length > 0 &&
    $('#cit_main_area').length > 0
  )
};

wait_for(uvijet_za_module_selection, function() {

  window.pp_edit_mode = false;
  cit_module_router();
  
}, 50*1000);   



  
    $(document).ready(function() {

      
      document.addEventListener('visibilitychange', function(ev) {
        
        if ( document.visibilityState == 'visible' && !we_hash ) {
          
          // provjeri je otvorena parking place stranica tako što provjeriš jel link u lijevom meniju aktivan
          if ( $('#left_menu_button_parking_places').hasClass('cit_active') ) {
            // ----------------- KLIKNI JOŠ JEDNOM NA LINK PARKIRALIŠTA
            // ------------------ iako je već aktivan ----> želim inicirati osvježavanje liste !!!!!!!
            $('#left_menu_button_parking_places').click();
          };
          
          
          // provjeri je otvorena parking place stranica tako što provjeriš jel link u lijevom meniju aktivan
          if ( $('#left_menu_button_customers').hasClass('cit_active') ) {
            // ----------------- KLIKNI JOŠ JEDNOM NA LINK PARKIRALIŠTA
            // ------------------ iako je već aktivan ----> želim inicirati osvježavanje liste !!!!!!!
            $('#left_menu_button_customers').click();
          };
     
        };
        
      });
      
    
    });

