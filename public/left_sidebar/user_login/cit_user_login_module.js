var module_object = {
  create: function( data, parent_element, placement ) {
    var component_id = null;
    var component_html =
    `
<div id="cit_user_box" class="cit_center">
  <div id="cit_user_avatar_img"></div>
  <div id="display_user_name" class="cit_center"></div>
</div>

<div id="cit_user_icon_box">
  <div title="LOGOUT" class="user_avatar_icons cit_center cit_tooltip" 
       id="user_logout_button" data-toggle="tooltip" data-placement="top">
    <i class="fas fa-door-open"></i>
  </div>
  <div class="user_avatar_icons cit_center">
    <i class="fas fa-tools"></i>
  </div>
  <div class="user_avatar_icons cit_center">
    <i class="fas fa-user-cog"></i>
  </div>
  
</div>

<div id="login_register_container" class="cit_hide">
  <!--NAME-->
  <div id="label_user_name" class="cit_label">Nadimak</div>
  <input id="user_name" type="text" class="cit_input" required>

  <!--PASS-->
  <div id="label_user_pass" class="cit_label">Lozinka</div>
  <input id="user_pass" type="password" class="cit_input" required>

  <div id="register_first_part" style="width: 100%; overflow: hidden;">

    <!-- REPEAT PASS -->
    <div id="label_repeat_user_pass" class="cit_label">Ponovi Lozinku</div>
    <input id="repeat_user_pass" type="password" class="cit_input" required>

    <!--FULL NAME-->
    <div id="label_user_full_name" class="cit_label">Ime i Prezime</div>
    <input id="user_full_name" type="text" class="cit_input" required>

    <!--EMAIL-->
    <div id="label_user_email" class="cit_label">Email</div>

    <input id="user_email" type="email" class="cit_input" required>

  </div>

  <!--LOGIN BUTTON-->
  <div id="user_login_button" class="cit_button" style="margin-top: 20px; letter-spacing: 0.03rem;">PRIJAVA</div>

  <div id="zelim_se_registrirati">Želim se registrirati!</div>

  <div id="register_second_part" style="width: 100%; overflow: hidden;">
    <!-- REGISTER BUTTON -->
    <!-- <div id="label_user_register_button" class="cit_label" style="margin: 5px 0; text-align: center; padding: 0;">Or</div> -->
    <div id="user_register_button" class="cit_button" style="margin-top: 0; letter-spacing: 0.03rem;">REGISTRACIJA</div>
    <div id="vec_imam_racun_link">Već imam račun</div>
  </div>
  
  
  

</div>

    `;
   
    setTimeout(function() { 
      $('.cit_tooltip').tooltip();
    }, 100);

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    };

    return {
      html: component_html,
      id: component_id
    };


  },
 
scripts: function () {

  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 

  var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;

wait_for(`$('#login_register_container').length > 0`, function() {

  this_module.check_user();
  if ( cit_user ) {
    this_module.monitor_mouse_move(true);
    
  } else {
    var on_board_module = window['/main_area/on_board_card/on_board_card.js']
    on_board_module.create( null, $('#cit_main_area') );
    
    window.pp_edit_mode = false;
    create_and_open_park_places('append');
  };
 
  
  /* /////////////////////////// CLICK EVENT ON LOGIN BUTON  (to login) /////////////////////////// */
  $('#user_login_button').off('click');
  $('#user_login_button').on('click', function(e) {
      user_login();
  });
  $('#user_register_button').off('click');
  $('#user_register_button').on('click', function(e) {
      user_registration();
  });

  /* //////////////////////// EVENT LSITENER PRESS ENTER KEY ON PASS FIELD (to login) //////////////////////// */
  $('#user_pass').off('keypress');
  $("#user_pass").on('keypress', function(e) {
      if (e.keyCode === 13) user_login();
  });

  $('#user_logout_button').off('click');
  /* //////////////////////// EVENT LSITENER FOR LOGIN BUTTON //////////////////////// */
  $("#user_logout_button").on('click', function() {
      user_logOUT();
  });
  
  /* //////////////////////// VEĆ IMAM RAČUN //////////////////////// */
  $('#vec_imam_racun_link').off('click');
  $("#vec_imam_racun_link").on('click', function() {
      user_loggedOut_UI('login_form');
  });  
  
  
  
  /* //////////////////////// LINK ŽELIM SE REGATI //////////////////////// */
  $('#zelim_se_registrirati').off('click');
  $("#zelim_se_registrirati").on('click', function() {
      user_loggedOut_UI('register_form');
  });    
  
  
}, 500000);



function monitor_mouse_move(turn_on) {
  if (turn_on) {
    $('body').off('click', check_user);
    $('body').on('click', check_user);
  } else {
    $('body').off('click', check_user);
  };

}; // end of monitor mouse move
this_module.monitor_mouse_move = monitor_mouse_move;
  
  
/* /////////////////// START //////////////////////// WHEN LOGGED OUT  /////////////////////////////////////////// */

function user_loggedOut_UI(formUI) {
  

  $("#cit_user_icon_box").addClass('cit_hide');

  // show login / register form
  if ( formUI == 'login_form' ) $('#login_register_container').removeClass('cit_hide register').addClass('login');

  if ( formUI == 'register_form' ) $('#login_register_container').removeClass('cit_hide login').addClass('register');
  
  $('#cit_left_menu').css({'pointer-events': 'none', 'opacity': '0' });

}; // end of user loggedOut UI
this_module.user_loggedOut_UI = user_loggedOut_UI;
  
/* /////////////////// START //////////////////////// WHEN LOGGED IN  /////////////////////////////////////////// */

function user_loggedIn_UI() {
  
  
  $('#login_reg_span').text('GLAVNI MENU');
  
  // ako je ime već prikazano to znači da je GUI već u stanju da je user ulogiran
  // zato ne moraš ići dalje
  if ( $('#display_user_name').hasClass('show') == true ) return;
  
  var prvo_slovo_imena = cit_user.name.substring(0,1).toUpperCase();
  
  $('#cit_user_avatar_img').text(prvo_slovo_imena);
  $('#cit_user_avatar_img').css('transform', 'rotateY(0deg)');

  $('#display_user_name').html( cit_user ? cit_user.full_name : '' );
  // SHOW user name 
  $('#display_user_name').addClass('show');

  $('#login_register_container').addClass('cit_hide').removeClass('register login');

  $("#cit_user_icon_box").removeClass('cit_hide');

  $('#cit_left_menu').css({'pointer-events': 'all', 'opacity': '1' });
  
    
}; // end of user  loggedIn UI
this_module.user_loggedIn_UI = user_loggedIn_UI;
  
function user_logOUT() {

  
  $('#login_reg_span').text('LOGIN / REGISTER');
  
  $('#cit_user_avatar_img').text('');
  $('#cit_user_avatar_img').css('transform', 'rotateY(270deg)');

  
  $('#display_user_name').html( '' );
  // SHOW user name 
  $('#display_user_name').removeClass('show');

  monitor_mouse_move(false);
  cit_user = null;
  window.localStorage.removeItem('cit_user');

  $('#login_register_container input[type="password"]').val('');
  user_loggedOut_UI('login_form');
  
  $('#cit_left_menu').css({'pointer-events': 'none', 'opacity': '0' });
  

};
this_module.user_logOUT = user_logOUT;
  

function user_registration() {

    var all_registration_fileds = ['user_name', 'user_pass', 'repeat_user_pass', 'user_full_name', 'user_email'];
    var too_short = "";
    var pass_repeat_same = false;

    $('#login_register_container .cit_input').css('border', '');

    for (var i = 0; i < all_registration_fileds.length; i++) {
        if ($('#' + all_registration_fileds[i]).val().length < 3) {
            too_short += all_registration_fileds[i] + ', ';
            $('#' + all_registration_fileds[i]).css('border', '1px solid #ff0058');
        };
    };

    if (too_short !== "") {
        alert('Polja ' + too_short.slice(0, -2) + '  nedostaju ili imaju manje od 3 slova!');
        return;
    };


    if ($('#user_pass').val() !== $('#repeat_user_pass').val()) {
        alert('Lozinka koju ste ponovili nije ista kao lozinka u prvom polju !!!');
        $('#login_register_container .cit_input').css('border', '');
        $('#user_pass').css('border', '1px solid #ff0058');
        $('#repeat_user_pass').css('border', '1px solid #ff0058');
        return;
    }

    var mail = $('#user_email').val();
    var is_email_ok = mail.search(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

    if (is_email_ok === -1 || mail.indexOf('.') === -1) {
        alert('Email adresa nije dobra!');
        $('#login_register_container .cit_input').css('border', '');
        $('#user_email').css('border', '1px solid #ff0058');
        return;
    };


  
    var time_now = Date.now();  
  
    var user_form_data = {
      
      user_saved: null,
      user_edited: null,

      saved: time_now,
      edited: time_now,
      last_login: time_now,

      user_address: '',
      user_tel: '',

      business_name: '',
      business_address: '',
      business_email: '',
      business_tel: '',
      business_contact_name: '',
      
      oib: null,
      iban: null,

      gdpr: true,

      /* START ---------- OVO SU SVE TIME STAMPOMVI */
      gdpr_saved: time_now,
      gdpr_accassed: time_now,
      user_deleted: null,
      /* END ---------- OVO SU SVE TIME STAMPOMVI */


      car_plates: null,

      customer_tags: null,

      balance: 0,
      customer_acquisition: null,
      customer_vip: null,
      park_places_owner: null,

      name: $('#user_name').val(),
      full_name: $('#user_full_name').val(),
      password: $('#user_pass').val(),
      hash: "",
      email: $('#user_email').val(),
      email_confirmed: false,
      welcome_email_id: "",
      roles: {
        admin: true,
        we_app_user: false,
        super_admin: false
      }
      
    };

    $.ajax({
            type: 'POST',
            url: '/create_user',
            data: JSON.stringify(user_form_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        })
        .done(function(user) {


            $('#user_pass').val('');
            $('#repeat_user_pass').val('');

            if (user.success == true) {
              
              $('#login_reg_span').text('GLAVNI MENU');

              window.localStorage.setItem('cit_user', JSON.stringify(user));
              cit_user = window.localStorage.getItem('cit_user');
              cit_user = JSON.parse(cit_user);

              user_loggedIn_UI();

              console.log("user is logged in !!!!");
              // START MONITORING MOUSE FOR MOVMENT AND CHECKING USER TOKEN !!!!
              monitor_mouse_move(true);

              // show_popup_modal(show_popup, popup_text, popup_progress, show_buttons)
              var popup_text = 
                  `
                    <h5>Bok ${cit_user.name}!</h5>
                    <p>
                    Molim te pogledaj pristigli e-mail i potvrdi registraciju tako da klikneš na veliki plavi gumb "POTVRDITE"<br>(na dnu email-a)<br><br>
                    Želimo ti ugodno korištenje We Park aplikacije!<br>
                    </p>
                  `;
              
              show_popup_modal(true, popup_text, null, 'ok');
              $('#cit_popup_modal_OK').off('click');
              $('#cit_popup_modal_OK').on('click', function() {
                show_popup_modal(false, popup_text, null, 'ok');
              });


            } else {
              
              $('#login_reg_span').text('LOGIN / REGISTER');
              // ----------------- ako je user.success == false :
              
              
              $('#user_pass').val('');
              $('#repeat_user_pass').val('');
              
              
              
              if ( user.name_taken == true ||  user.email_taken == true ) {
                
                var nadimak_ili_email = 'nadimak'
                if (  user.email_taken == true ) nadimak_ili_email = 'email';
                
                var popup_text =  
                  `
                    <h5>SOORRY</h5>
                    <p>
                    Ovaj ${nadimak_ili_email} je zauzet :(<br><br>
                    Probaj neki drugi...<br>
                    </p>
                  `;
                show_popup_modal('warning', popup_text, null, 'ok');
                
                $('#cit_popup_modal_OK').off('click');
                $('#cit_popup_modal_OK').on('click', function() {
                  show_popup_modal(false, popup_text, null, 'ok');
                });

                
              } 
              else {
                
                var popup_text = 'Došlo je do greške prilikom registracije!';
                
                if ( user.msg ) popup_text = user.msg;
                
                show_popup_modal('error', popup_text, null, 'ok');
                
                $('#cit_popup_modal_OK').off('click');
                $('#cit_popup_modal_OK').on('click', function() {
                  show_popup_modal(false, popup_text, null, 'ok');
                });

                
              };
              
            };
              
              
              
        })
        .fail(function(error) {
      
            $('#login_reg_span').text('LOGIN / REGISTER');
            var popup_text = "Greška prilikom registracije korisnika!!!";
            // show_popup_modal(show_popup, popup_text, popup_progress, show_buttons)
            show_popup_modal(true, popup_text, null, 'ok');
            $('#cit_popup_modal_OK').off('click');
            $('#cit_popup_modal_OK').on('click', function() {
              show_popup_modal(false, popup_text, null, 'ok');
            });
        })


}
this_module.user_registration = user_registration;
  
  
  

function user_login() {

    var user_form = {
        name: $('#user_name').val(),
        password: $('#user_pass').val(),
    };

    $.ajax({
            type: 'POST',
            url: '/authenticate',
            data: JSON.stringify(user_form),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        })
        .done(function(user) {

            $('#user_pass').val('');
            $('#repeat_user_pass').val('');

            if ( typeof user.success !== 'undefined' && user.success !== false ) {

              $('#login_reg_span').text('GLAVNI MENU');
              
              // set user data to local storage
              window.localStorage.setItem('cit_user', JSON.stringify(user));
              // get user data back from local storage  --- to be sure all is ok
              cit_user = window.localStorage.getItem('cit_user');
              // make js object from local storage data
              cit_user = JSON.parse(cit_user);


              // animate user form ( collapse form and show user name ...and show log out button ) 
              user_loggedIn_UI();

              console.log("user is logged in !!!!");
              // START MONITORING MOUSE FOR MOVMENT AND CHECKING USER TOKEN !!!!
              monitor_mouse_move(true);

              $('#cit_left_menu').css({'pointer-events': 'all', 'opacity': '1' });

              // OTVARAM ODMAH PARKING PLACES MODULE SA OVIM KLIKOM !!!
              setTimeout(function() {
                $('#left_menu_button_parking_places').click();
              }, 200);

            } else {
              
              $('#login_reg_span').text('LOGIN / REGISTER');

              $('#user_pass').val('');
              $('#repeat_user_pass').val('');
              // response property succsess == false
              // alert(user.msg);

              popup_login_nije_uspio();

              $('#cit_left_menu').css({'pointer-events': 'none', 'opacity': '0' });
              
            };
        })
        .fail(function(error) {
          console.log(error);
          popup_login_error();
          $('#cit_left_menu').css({'pointer-events': 'none', 'opacity': '0' });
        });

}; // end of user login
this_module.user_login = user_login;
  
  
function check_token() {
  
  return new Promise(function(resolve, reject){

    var cit_user = window.localStorage.getItem('cit_user');

    if ( !cit_user ) {
      console.log('nefostaje user unutar check token func')
      resolve(null);
      return;
    };


    cit_user = JSON.parse(cit_user);

    // get token from GLOBAL user object
    var token_object = {
        token: cit_user.token
    };

      // get expiration from GLOBAL user object
    var token_expiration = Number(cit_user.token_expiration);
    // get diff from current time
    var remaining_time = token_expiration - Date.now();

    // if expiration time is in less then 5 min !!!!
    // and greater then 0
    if (remaining_time > 0 && (remaining_time < 5 * 60 * 1000)) {

        // then make special request for prologing token duration
        // actually it is creation of brand new token !!!!!!  
      $.ajax({
        type: 'POST',
        url: '/prolong',
        data: JSON.stringify(token_object),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function(new_token_object) {
        // usually when token check fails then instead of token 
        // I get property success: false
        // so I handle that like error !!!!
        if (new_token_object.success == false) {

          popup_sesija_je_istekla();
          //alert("Your session has expired.\n\nPlease log in again.");
          cit_user = null;
          window.localStorage.removeItem('cit_user');
          resolve('expired');

        } else {
          // write new token data to GLOBAL cit_user object
          cit_user.token = new_token_object.token;
          cit_user.token_expiration = Number(new_token_object.token_expiration);

          // write in local storage user object with new token data
          window.localStorage.setItem('cit_user', JSON.stringify(cit_user));

          resolve('prolonged');

        }
      })
      .fail(function(error) {
        popup_error_produzenje_tokena();
        // alert("Greška kod produženja korisničkog tokena !!!!");
        resolve(null);
      });

    } 
    else if (remaining_time <= 0) {

      popup_sesija_je_istekla();
      user_loggedOut_UI('login_form');
      cit_user = null;
      window.localStorage.removeItem('cit_user');
      resolve(null);

    } else {
      // ako je sve ok  tj ako token ima više od 5 minuta da zastari
      // onda ne treba raditi ništa
      resolve('ok');

    };

  }); // kraj promisa
  
};
this_module.check_token = check_token;

function check_user(e) {

  
    if ( !cit_user) {
      cit_user = window.localStorage.getItem('cit_user');
      if (cit_user) cit_user = JSON.parse(cit_user);
    };

    if (!cit_user) {
        user_loggedOut_UI('register_form');
    };
    
    if (!cit_user) return;
  
try {
    check_token().then(function(token_state) {
      
      
      if (cit_user && token_state == null) {
        user_loggedOut_UI('login_form');
      };
      
      
      if ( cit_user && token_state == 'expired') {
        user_loggedOut_UI('login_form');
        cit_user = null;
        window.localStorage.removeItem('cit_user');
      };
      
      
      if (cit_user && ( token_state == 'prolonged' || token_state == 'ok' ) ) {
        wait_for(`$('#login_register_container').length > 0`, function() {
          user_loggedIn_UI();
        }, 500000);
      };
      
    });

}
catch(err) {
  console.log(err);
}

}; // end of check_user function
this_module.check_user = check_user;  
  
this_module.cit_loaded = true;   
  
}
  
  
};
