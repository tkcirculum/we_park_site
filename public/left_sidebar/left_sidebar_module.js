var module_object = {
  create: function( comp_data, parent_element, placement ) {
    
    var cit_user_login_module_url = '/left_sidebar/user_login/cit_user_login_module.js';
    get_cit_module(cit_user_login_module_url);
    
    var cit_left_menu_url = '/left_sidebar/left_main_menu/left_main_menu.js';
    get_cit_module(cit_left_menu_url, 'load_css');
    
    
    
    function all_components_needed() {
      return (
        window[cit_user_login_module_url]                    && 
        window[cit_user_login_module_url].cit_loaded == true &&
        window[cit_left_menu_url]                            &&
        window[cit_left_menu_url].cit_loaded == true
      )
    };
    
    wait_for(all_components_needed, function() {
      var component_id = null;
      var login_module = window[cit_user_login_module_url];
      var left_menu = window[cit_left_menu_url];
      
      var component_html = 
`
${login_module.create().html}

${left_menu.create().html}



<a href="https://join.slack.com/t/wepark-forum/shared_invite/zt-gjtxj7pc-QQj9UCP9Cv9FavohhbwQlw" style="display: block; width: 50%; height: auto; margin: 20px auto 0;"  target="_blank">

<img  id="slack_forum_logo" 
      class="cit_tooltip" 
      src="img/slack_forum_logo.png"
      style="width: 100%; height: auto;"
      data-toggle="tooltip" data-placement="bottom"
      title="Forum za sva pitanja u vezi WePark aplikacije!">  

</a>

`;

      if ( parent_element ) {
        cit_place_component(parent_element, component_html, placement);
        
        setTimeout( function() {
          if ( !window.leftmenu_perfect_bar ) {
            window.leftmenu_perfect_bar = new PerfectScrollbar( $('#cit_left_sidebar')[0] );
          } else {
            window.leftmenu_perfect_bar.update();
          };
          
          wait_for(`$('#slack_forum_logo').length > 0`, function() { 
            $('.cit_tooltip').tooltip();
          }, 500000)
          
        }, 300);
        
      }; 
      
      return {
        html: component_html,
        id: component_id
      };

    }, (60 * 1000 * 20)); // 20 min timeout
    
  } 
};
