
var module_object = {
  create: function( data, parent_element, placement ) {
    var component_id = null;
    var component_html =
    `
<div class="cit_switch_wrapper">
  <div id="${data.id}_label" class="cit_label">${data.label_text}</div>
  <div id="${data.id}" class="cit_switch on">
        <div class="switch_circle"></div>
        <div class="on_text switch_text">DA</div>
        <div class="off_text switch_text">NE</div>
  </div>
</div>

    `;
   
    setTimeout(function() { 
      $('.cit_tooltip').tooltip();
    }, 20);

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    }

  },
 
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
 
  
  // OVDJE STAVITI SVE FUNKCIJE !!!!
  
  
  this_module.cit_loaded = true;
  
  
}
  
  
};