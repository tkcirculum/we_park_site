var module_object = {
  create: function( data, parent_element, placement ) {
    
    
    var component_id = null;
    var component_html =
`
<div id="${data.cit_autocomplete_input_id}_label"  class="cit_label">${data.cit_autocomplete_label || '' }</div>
<input  autocomplete="off" id="${data.cit_autocomplete_input_id}" 
        data-cit_search="${data.cit_search}"
        data-cit_select_fields="${data.cit_select_fields}"
        data-cit_return_fields="${data.cit_return_fields}"
        data-cit_list_container="${data.cit_list_container}"

        ${ data.col_widths ? 'data-col_widths="' + data.col_widths + '"' : '' }
        ${ data.custom_headers ? 'data-custom_headers="' + data.custom_headers + '"' : '' }

        ${ data.cit_show ? 'data-cit_show="' + data.cit_show + '"' : '' }
        ${ data.cit_hide ? 'data-cit_hide="' + data.cit_hide + '"' : '' }
        data-just_show="${data.just_show}"
        ${ data.sort_by ? 'data-sort_by="' + data.sort_by + '"' : '' }
        ${ data.list_class ? 'data-list_class="' + data.list_class + '"' : '' }
        type="${data.cit_type}" 
        class="cit_autocomplete ${data.cit_list_width}">
`;
   
    setTimeout(function() { 
      $('.cit_tooltip').tooltip();
    }, 20);

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    }

  },
 
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
 
  
  
  // OVDJE STAVITI SVE FUNKCIJE !!!!
  
  
  this_module.cit_loaded = true;
  
  
}
  
  
};