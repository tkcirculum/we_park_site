
$(document).ready(function() {  
  

  
var customer_product = {
  amount: 200,
  balance: 200,
  buy_time: 1576621039691,
  code: "10009-nadoplata---1576615837374",
  desc: "CARD-VISA",
  is_free: false,
  pp_sifra: null,
  price: 200,
  sifra: "1259",
  type: "card",
  user_number: 10009,
  valid_from: null,
  valid_to: null,
  _id: "5df953ef15efd42c622b5599"
};
  
  
  var customer_product = {
    "sifra" : "1257",
    "pp_sifra" : "5",
    "code" : "10009-pretplata-2-5-1575971956880",
    "amount" : 720,
    "desc" : "CARD-MASTERCARD",
    "user_number" : 10009,
    "is_free" : false,
    "price" : 720,
    "type" : "monthly",
    "valid_from" : 1575972035349.0,
    "valid_to" : 1581116400000.0,
    "balance" : 720,
    "__v" : 0
}
  
var user = {
    "car_plates" : [ 
        "FFFF", 
        "GGGG"
    ],
    "customer_tags" : [ 
        {
            "sifra" : "TELE2",
            "naziv" : "TELE 2"
        }
    ],
    "customer_vip" : [ 
        {
            "_id" : "5da6854316382f6f5875e04f",
            "pp_name" : "Visoko učilište Algebra"
        }
    ],
    "pp_owner_park_places" : null,
    "user_saved" : null,
    "user_edited" : {
        "user_id" : "5d9f47f16ce0b535e5441a3a",
        "user_number" : 10009,
        "user_name" : "Circulum",
        "full_name" : "Proba Probić",
        "time_stamp" : 1571252634762.0
    },
    "saved" : 1570719728736.0,
    "edited" : 1571252634762.0,
    "last_login" : 1572954435090.0,
    "user_address" : "Nikole Tesle 41",
    "user_tel" : "2222",
    "business_name" : "Circulum Dev Team",
    "business_address" : "Kupinečka 54",
    "business_email" : "info@circulum.hr",
    "business_tel" : "01 555 555",
    "business_contact_name" : "Toni Kutlić",
    "oib" : 111111111111.0,
    "iban" : "2222222222222",
    "gdpr" : true,
    "gdpr_saved" : 1570719728736.0,
    "gdpr_accassed" : 1570719728736.0,
    "user_deleted" : null,
    "balance" : 500,
    "customer_acquisition" : {
        "sifra" : "AKV001",
        "naziv" : "MARKO"
    },
    "name" : "Circulum",
    "full_name" : "Proba Probić",
    "hash" : "pbkdf2$10000$214d2a3a46a7bc074d4b484e162919845cb5a27f206ba9535c5703ddc5873d928eed909d3dbe7a78c7703db32279d4dd9233854e562071015a5ace2cea0a4b60$a9448b921aad5a8a072ef788a8416cee7d60f160ced89c15625158026fd16f1c7d1af14bdd2f7d2512d385ab6a92ee0b0d5fbcd343559e154c780e5d8a428349",
    "email" : "tkutlic@gmail.com",
    "email_confirmed" : false,
    "welcome_email_id" : "1570719729154-1605874083508-1577566183508",
    "roles" : {
        "admin" : true,
        "we_app_user" : false,
        "super_admin" : false
    },
    "user_number" : 10009,
    "__v" : 0,
    "obrtnik" : true,
    "pdv" : true,
    "park_places_owner" : []
};




create_newsletter( customer_product, user );


function create_newsletter( customer_product, user ) {


  // nadoplata ili pretplata
  var offer = customer_product.code.split('-')[1];
  
  // ako je pretplata koliko mjeseci 
  var duration = customer_product.code.split('-')[2];
  // ako je pretplata za koje je to parkiralište
  var curr_pp_id = customer_product.code.split('-')[3];

  
  function capitalizeFLetter(word) { 
    var result = word[0].toUpperCase() + word.slice(1); 
    return result;
  };
    
 
function getMeADateAndTime(number, cit_format) {
  var today = null;
  
  // ako nije undefined ili null onda napravi datum od broja 
  if ( typeof number !== 'undefined' && number !== null ) {
    today = new Date( number );  
  } else {
    today = new Date();  
  };
  
  var sek = today.getSeconds();
  var min = today.getMinutes();
  var hr = today.getHours();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; // January is 0!
  var yyyy = today.getFullYear();

  // ako je samo jedna znamenka
  if (dd < 10) {
    dd = '0' + dd;
  };
  // ako je samo jedna znamenka
  if (mm < 10) {
    mm = '0' + mm;
  };

  // ako je samo jedna znamenka
  if (sek < 10) {
    sek = '0' + sek;
  };


  // ako je samo jedna znamenka
  if (min < 10) {
    min = '0' + min;
  };

  // ako je samo jedna znamenka
  if (hr < 10) {
    hr = '0' + hr;
  };

  if ( cit_format == 'd.m.y' ) {
    today =  dd + '.' + mm + '.' + yyyy;
  };
  
  
  
  // ako nije neveden format ili ako je sa crticama
  if ( cit_format == 'y_m_d' ) {
    today = yyyy + '_' + mm + '_' + dd;
  };
    

  // ako nije neveden format ili ako je sa crticama
  if ( !cit_format || cit_format == 'y-m-d' ) {
    today = yyyy + '-' + mm + '-' + dd;
  };
  

  
  time = hr + ':' + min + ':' + sek;
  
  // EXPERIMENTAL !!!!!!:
  // stavio sam mogućnost da mi upiše datum / vrijeme na svim elementima koji imaju 
  // istu klasu kao ove - tj ako u html-u stoji element sa ovim klasama i ako na onload pokrenem ovu funckiju
  // sadržaj svakog elementa će dobiti datum / vrijeme ovisno o klasi koju ima

  // $(".getMeADate").html(today);
  // $(".getMeATime").html(time);

  return {
    datum: today,
    vrijeme: time
  }

};


  function zaokruziIformatirajIznos(num, zaokruzi_na) {
    // primjer : var num = 123456789.56789;

    var brojZnamenki = 2;
    if ( typeof zaokruzi_na !== 'undefined' )  brojZnamenki = parseInt( zaokruzi_na );


    var zaokruzeniBroj = zaokruzi(num, brojZnamenki);


    var stringBroja = String(zaokruzeniBroj);
    // 123456789
    var lijeviDio = stringBroja.split('.')[0] + "";
    var tockaPostoji = stringBroja.indexOf(".");
    var desniDio = "00";
    if (tockaPostoji > -1) {
      desniDio = stringBroja.split('.')[1] + "";
    };

    /*
    if (Number(desniDio) < 10) {
      desniDio = '0' + Number(desniDio);
    };
    */


    var broj_je_pozitivan = true;

    // ako je broj negativan 
    if ( lijeviDio.substring(0,1) == '-' ) {
      lijeviDio = lijeviDio.substring(1);
      broj_je_pozitivan = false;
    };

    var stringLength = lijeviDio.length;

    // ne zanimaju me brojevi s tri znamenke i manje jer oni nemaju točke
    if (stringLength >= 4) {
      // pretvaram string u array
      lijeviDio = lijeviDio.split('');
      // gledam koliko stane grupa po 3 znamaneke
      // i onda uzmem ostatak - to je jednostavna modulus operacija
      var dotPosition = stringLength % 3;
      // zanima me koliko treba biti točaka u broju
      // to dobijem tako da podijelim broj znamenki sa 3 i gledam samo cijeli broj
      var dotCount = parseInt(stringLength / 3)

      // postavim prvu točku
      // NAPOMENA u slučajevima kada je modulus = 0 ova funkcije postavi točku čak i na počatek broja
      // tu točku kasnije obrišem
      // svaka sljedeća točka je UVIJEK 4 pozicije udesno !!!!
      // napomena - logično bi bilo da je svake 3 pozicije udesno, 
      // ali moram brojati i točku koju sam stavio i ona povećava length od stringa
      for (i = 1; i <= dotCount; i++) {
        // stavi točku
        lijeviDio.splice(dotPosition, 0, ".");
        // id na poziciju current + 4 
        dotPosition = dotPosition + 4;
      };


      // spoji nazad sve znakove u jedan string !!!
      lijeviDio = lijeviDio.join('')

      // kad se dogodi da je modulus === 0 onda postavim točku na nultu poziciju
      // i sad je moram obrisati !!!!!!!
      if (lijeviDio.charAt(0) === ".") {
        // uzimam sve osim prvog znaka tj točke
        lijeviDio = lijeviDio.substring(1);
      }
    };

    // ponovi nulu onoliko puta koliko treba da decimale imaju onoliko stringova 
    // koliko je definirano sa argumentom [zaokruzi_na] 
    // ( ... od kojeg gore dobijem varijablu brojZnamenki )
    if (desniDio.length < brojZnamenki) {
      desniDio = desniDio + '0'.repeat(brojZnamenki - desniDio.length);
    };


    var formatiraniBroj = lijeviDio + "," + desniDio;

    if ( desniDio == "00" &&  brojZnamenki == 0 )  formatiraniBroj = lijeviDio;

    if ( broj_je_pozitivan == false ) formatiraniBroj = '-' + formatiraniBroj;

    return formatiraniBroj;

  };

  /* /////////////////////////    ZAOKRUŽI BROJ   ///////////////////////////////////// */

  function zaokruzi(broj, brojZnamenki) {

   var broj = parseFloat(broj);

   if ( typeof brojZnamenki == 'undefined' ) {
     brojZnamenki = 0;
   };
   var multiplicator = Math.pow(10, brojZnamenki);
   broj = parseFloat((broj * multiplicator).toFixed(11));
   var test =(Math.round(broj) / multiplicator);
   // + znak ispred je samo js trik da automatski pretvori string u broj
   return +(test.toFixed(brojZnamenki));
  };


  var pp_name = '';
  
  global.park_places_list.forEach(function(pp_obj, pp_index) {
    if ( pp_obj.pp_sifra == curr_pp_id ) pp_name = pp_obj.pp_name;
  });
  
  
  
  var ime_paketa = '';
  var trajanje = '';
  var pocetak = '';
  var kraj = '';
  
  if ( offer == 'pretplata' ) {
    
    ime_paketa = capitalizeFLetter(offer) + ' za parkiralište ' + pp_name;
    
    ime_paketa = `  <div style="width: 100%; float: left; text-align: center;">Proizvod/usluga: </div>
                    <div style="  width: 100%;
                                  float: left;
                                  font-weight: 700;
                                  text-align: center;
                                  font-size: 13px;
                                  margin-bottom: 10px;">${ ime_paketa }</div>`;
    
    trajanje = Number(duration)*30 + ' dana';
    
    trajanje = `  <div style="width: 50%; float: left; clear: left;">Trajanje: </div>
                  <div style="width: 50%; float: left; text-align: right;">${trajanje}</div>`;
    
    pocetak = ` <div style="width: 50%; float: left; clear: left;">Početak: </div>
                <div style="width: 50%; float: left; text-align: right;">

<div style="float: right; margin-left: 6px;">${getMeADateAndTime(customer_product.valid_from).vrijeme}</div>
<div style="float: right;">${getMeADateAndTime(customer_product.valid_from).datum}</div>

                  
                </div>`;
    
    kraj = `    <div style="width: 50%; float: left; clear: left;">Završetak: </div>
                <div style="width: 50%; float: left; text-align: right;">
                  
<div style="float: right; margin-left: 6px;">${getMeADateAndTime(customer_product.valid_to).vrijeme}</div>
<div style="float: right;">${getMeADateAndTime(customer_product.valid_to).datum}</div>
                  
                </div>`;
    
    
  } 
  else {
    
    ime_paketa = capitalizeFLetter(offer) + ' korisničkog računa';
    
    ime_paketa = `  <div style="width: 100%; float: left; text-align: center;">Proizvod/usluga: </div>
                    <div style="  width: 100%;
                                  float: left;
                                  font-weight: 700;
                                  text-align: center;
                                  font-size: 13px;
                                  margin-bottom: 10px;">${ ime_paketa }</div>`;
    
  };
  
  
    var datum_transakcije = ` <div style="width: 50%; float: left; clear: left;">Datum transakcije: </div>
                            <div style="width: 50%; float: left; text-align: right;">
                              
<div style="float: right; margin-left: 6px;">${getMeADateAndTime(customer_product.buy_time).vrijeme}</div>
<div style="float: right;">${getMeADateAndTime(customer_product.buy_time).datum}</div>

                            </div>`;
  
  
  var broj_racuna_mjesta_kase = ` <div style="width: 50%; float: left; clear: left;">Račun broj: </div>
                                  <div style="width: 50%; float: left; text-align: right;">
                                  ${customer_product.racun_br + '/1/1' }
                                  </div>`;

  
  
  var osnovica = `<div style="width: 50%; float: left; clear: left;">Cijena: </div>
            <div style="width: 50%; float: left; text-align: right;">${zaokruziIformatirajIznos(customer_product.price/1.25)}kn</div>`;
  
  var iznos_pdv = `<div style="width: 50%; float: left; clear: left;">Pdv 25%: </div>
                  <div style="  width: 50%;
                                float: left;
                                text-align: right;">

                  ${zaokruziIformatirajIznos(customer_product.price/1.25*0.25)}kn

                  </div>`;
  
  var iznos = `<div style="width: 50%; float: left; clear: left; font-size: 18px; font-weight: 700;">Iznos s pdv: </div>
            <div style="  width: 50%;
                          float: left;
                          text-align: right;
                          font-size: 18px;
                          font-weight: 700;">

            ${zaokruziIformatirajIznos(customer_product.price)}kn

            </div>`;
  
  var zki = ` <div style="width: 10%; float: left; clear: left; font-size: 10px;">ZKI:</div>
              <div style="width: 90%; float: left; text-align: right; font-size: 10px;">
                ${customer_product.zki || '' }
              </div>`;
      
  var jir = ` <div style="width: 10%; float: left; clear: left; font-size: 10px;">JIR:</div>
              <div style="width: 90%; float: left; text-align: right; font-size: 10px;">
                ${customer_product.jir || '' }
              </div>`;
  
      
      
      
  
  var html = `
      
      
      
<div id="${Math.random().toString(16).substring(2,10)}" 
style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #fff;
            position: relative;
            margin: 0 auto;
            padding: 0;
            max-width: 1000px;">

<!--
  div id="${Math.random().toString(16).substring(2,10)}" 
style="  font-size: 50px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    color: #fff;
                    font-weight: 700;
                    line-height: 60px;
                    letter-spacing: normal;
                    text-align: left;
                  position: absolute;
                  width: 50%;
                  height: auto;
                  top: 30px;
                  right: 0;">
        
       Nova WePark Aplikacija!

      </div> 
-->        

  
  <!--<img src="https://wepark.circulum.it:9000/img/001_nl_bg.jpg" style="width: 100%; height: auto;" />-->
  <img src="https://wepark.circulum.it:9000/img/003_nl/naslovna_var_301.jpg" style="width: 100%; height: auto;" />
  
  
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />

  
  
  
  <div style="float: none;
    width: 100%;
    max-width: 340px;          
    text-align: center;
    font-size: 27px;
    font-weight: 700;
    margin: 0px auto 30px;
    line-height: normal;
    letter-spacing: normal;
    color: #06253f;">
<br>
  
Molimo Vas ažurirajte<br>
WePark aplikaciju
  
<br>


</div>
  <br style="display: block; clear: both;">
   
   
<div style="padding: 10px; float: none; clear: both; background-color: #e64531; border-radius: 8px; max-width: 340px; margin: 0 auto;">
          
          <img src="https://wepark.circulum.it:9000/img/001_nl_warning_sign.jpg" 
       style="width: 100%; height: auto; max-width: 400px; clear: both; display: block; float: none; margin: 0 auto;" />
  
          <h2 style="color: #6f6f6f; text-align: center; color: #fff; margin-top: 0;">BITNA NAPOMENA</h2>
  
          <p style="padding-left: 0;
                    text-align: center;
                    padding-right: 0;
                    line-height: 1.6; 
                    color: #fff;">
            
            U zadnjih par dana dobili smo nekoliko žalbi da aplikacija ne radi kako treba!<br>
            <br>
            Razlog je veliki update koji smo napravili prošli tjedan.
            <br>
            Kako bi osigurali ispravan rad aplikacije molimo Vas da napravite update aplikacije.
            
            
            </p>
  

        
        </div>   
  
<br style="display: block; clear: both;">
            
            <p style="padding-left: 0;
                    text-align: center;
                    padding-right: 0;
                    line-height: 1.6; 
                    color: #06253f;">
  
            
            Ukoliko imate bilo kakvih pitanja molimo vas kontaktirajte nas na:<br><br>
            toni.kutlic@wepark.eu
            <br>
            ili
            <br>
            00385 92 3133 015
            
          </p>
  
  
        <br>
        <br>
   
   
     <br style="display: block; clear: both;">
   
        <br>
        <br>

<div style="padding: 0; float: none; clear: both; width: 100%; max-width: 360px; margin: 0 auto;">
  
<h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #06253f; text-align: center;">
  <img style="width: 30px;
    height: auto;
    margin-right: 3px;
    position: relative;
    top: 8px;"
    src="https://wepark.circulum.it:9000/img/003_nl/small_android_icon.jpg" />
  Android
  </h2>
<div style="padding-left: 0;
          text-align: left;
          padding-right: 0;
          line-height: 1.6;">
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">1</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Posjetite Google Play Store</div>
  </div>
    
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">2</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Kliknite ikonu menija na vrhu stranice i izaberite "Moje aplikacije i igre" (My apps & games)</div>
  </div>
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">3</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Izaberite link Ažuriranje/Nadopuna (Updates)</div>
  </div>
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">4</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Na dnu ekrana se nalazi lista svih aplikacija koje treba ažurirati</div>
  </div>
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">5</div>
    
    <div style="width: calc(100% - 50px); padding-top: 5px;">Ako na toj listi vidite WePark aplikaciju, kliknite na gumb AŽURIRAJ (UPDATE)</div>
  
  </div>  
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">6</div>
    
    <div style="width: calc(100% - 50px); padding-top: 5px;">Ako na toj listi NEMA WePark aplikacije, onda ste već ažurirali aplikaciju i sve je ok.</div>
  
  </div>
  
  
  <br>
  <br>
  
  
  </div>   
  <br>
  <img src="https://wepark.circulum.it:9000/img/003_nl/android_update_page_1.jpg" style="width: 100%; max-width: 262px; height: auto;" />
  <br>
  <img src="https://wepark.circulum.it:9000/img/003_nl/android_update_page_2.jpg" style="width: 100%; max-width: 262px; height: auto;" />
</div>        
  
<br style="display: block; clear: both;">  
  
  

<div style="padding: 0; float: none; clear: both; width: 100%; max-width: 360px; margin: 0 auto;">
  
<h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #06253f; text-align: center;">
  <img style="width: 30px;
    height: auto;
    margin-right: 3px;
    position: relative;
    top: 2px;"
    src="https://wepark.circulum.it:9000/img/003_nl/small_apple_icon.jpg" />
  iPhone
</h2>
  
<div style="padding-left: 0;
          text-align: left;
          padding-right: 0;
          line-height: 1.6;">
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">1</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Posjetite App Store</div>
  </div>
    
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">2</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Kliknite ikonu vašeg profila u gornjem desnom kutu</div>
  </div>
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">3</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Na dnu ekrana će biti lista svih aplikacija koje se trebaju ažurirati</div>
  </div>
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">4</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Ako na toj listi vidite WePark aplikaciju, kliknite na gumb AŽURIRAJ (UPDATE) </div>
  </div>
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">5</div>
    
    <div style="width: calc(100% - 50px); padding-top: 5px;">Ako na toj listi NEMA WePark aplikacije, onda ste već ažurirali aplikaciju i sve je ok.</div>
  
  </div>  
  
    
  <br>
  <br>
    
  </div>    
  
  <br>
  <img src="https://wepark.circulum.it:9000/img/003_nl/ios_update_page_1.jpg" style="width: 100%; max-width: 262px; height: auto;" />
  <br>
  <img src="https://wepark.circulum.it:9000/img/003_nl/ios_update_page_2.jpg" style="width: 100%; max-width: 262px; height: auto;" />
  
</div>        
  
<br style="display: block; clear: both;">  
  
    
  
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_download_app.jpg" style="width: 100%; height: auto; max-width: 400px;" />
  
  
  <div style="width: 100%; height: auto; clear: both; text-align: center; margin: 10px 0 30px;">
    
    <a href="https://play.google.com/store/apps/details?id=com.wePark" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/google_play.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    <a href="https://apps.apple.com/us/app/wepark-park-with-smile/id1314272266?ls=1" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/app_store.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    
  </div>
     
<br style="display: block; clear: both;">    
  
  
<div style="padding: 0; float: none; clear: both; width: 100%; max-width: 400px; margin: 0 auto;">
  
<h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">Kontakt</h2>
<p style="padding-left: 0;
          text-align: center;
          padding-right: 0;
          line-height: 1.6;">

  Ako imate bilo koje pitanje molimo obratite se na<br>
  +385 92 3133 015<br>
  ili na email<br>
  toni.kutlic@wepark.eu
</p>
</div>        


<br style="display: block; clear: both;">
  
  
        <br>
        <br>
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_footer.jpg" style="width: 100%; height: auto; display: block; clear: both;"/>
  

    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #fff;
                padding: 0;">

      
      
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />



      <div id="${Math.random().toString(16).substring(2,10)}" style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 18px;
        border-top: 1px solid #eaeaea;
        padding-top: 10px;
        background-color: #fff;
        letter-spacing: 0.06rem;">
        

        <b>Odgovorna osoba:</b><br>
        <b>Karlo Starčević (direktor)</b><br><br>
        <b>Wepark smart parking d.o.o.</b><br>
        <b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
        <b>OFFICE:</b>&nbsp;zgrada IGH, Rakušina 1, 10000 Zagreb<br>
        <b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
        <b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
        <b>WEB:</b>&nbsp;www.wepark.eu<br>
        <br>
        <div style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
        Društvo je upisano u sudski registar Trgovačkog suda u Zagrebu pod brojem 081131571. Temeljni kapital u iznosu od 20.000 kn je uplaćen u cijelosti. Račun se vodi kod Privredne Banke Zagreb. Član društva Karlo Starčević.
        </div>
        <br style="display: block; clear: both;">
        
        
      </div>        
      
      <br id="${Math.random().toString(16).substring(2,10)}"  style="display: block; clear: both;">
    </div>  

    
</div>     


  
  
`;
  
 $('#place_newsletter_here').html(html);
  
};
  
}); // end of doc ready 
  
  