var module_object = {
  create: function( data, parent_element, placement ) {

    var component_id = data.id;
    var component_html =
    `
<div id="${data.id}" class="cit_search_list">
  
</div>

    `;
   
    setTimeout(function() { 
      $('.cit_tooltip').tooltip();
    }, 50);

    if ( parent_element ) {
      
      cit_place_component(parent_element, component_html, placement);
      
      var this_module = window[module_url]; 
      this_module.data = data;
    };

    return {
      html: component_html,
      id: component_id
    };


  },
 
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  
  function create_pp_list() {
  wait_for(`$('#park_places_list_place_here').length > 0`, function () {

    // provjeri je otvorena parking place stranica tako što provjeriš jel link u lijevom meniju aktivan
    if ( $('#left_menu_button_parking_places').hasClass('cit_active') ) {
      
      
      var pp_list_props = {
        desc: 'za dobivanje liste parkinga u desnom meniju',
        local: false,
        url: '/search_park_places',
        col_widths: [0, 100, 0, 0, 0, 0, 0],
        custom_headers: ['ID', 'Naziv parkinga', 'Adresa', 'OUT', 'lat', 'lng', 'PUB' ],
        return: { _id: 1, pp_name: 1, pp_address: 1, pp_count_out_num: 1, pp_latitude: 1, pp_longitude: 1, published: 1 },
        // hide_cols: ['_id', 'pp_address', 'pp_count_out_num', 'pp_latitude', 'pp_longitude', 'published'],
        show_cols: ['pp_name'],
        query: {},
        show_on_click: true,
        parent: '#park_places_list_place_here',
        list_class: 'sidebar_list'
      };

      // resetiraj js scroll bar
      if ( window.pp_list_perfectbar ) {
        window.pp_list_perfectbar.destroy();
        window.pp_list_perfectbar = null;
      };
      

      $('#park_places_list_place_here').find('.search_result_table').remove();

      
      // resetiram globalni array svih parkinga na null
      window.pp_data_for_list = null;
    
      search_database(null, pp_list_props, 'parallel' ).then(
        function( result_array ) {
          
          window.pp_data_for_list = result_array;
          
          this_module.create_initial_pp_list(result_array, pp_list_props );
        },
        function( error ) { 
          console.error( error );
        }
      );

    }; // kraj uvijeta ako je parking place stranica/module aktivana
    
  }, 5000000 );      
  };
  this_module.create_pp_list = create_pp_list;
  
  
  function open_park_place(id) {
    
    $('#parking_places_form input').val('');
    
      var park_props = {
        desc: 'otvaranje specifičnog parkinga',
        local: false,
        url: '/search_park_places',
        return: {},
        query: {_id: id },
        show_on_click: true,
        parent: 'none'
      };
    
    
    search_database(id, park_props).then(
      function( result_array ) {
        console.log(result_array[0]);
        
        var pp_module = window['/main_modules/parking_places/parking_places_module.js'];
        pp_module.cit_data = result_array[0];
        
        pp_module.data_to_form(pp_module.cit_data);
        show_save_or_edit_button('edit_park_place_btn');

        
        window.pp_edit_mode = true;
        
        setTimeout( function() {
          
          var pp_module = window['/main_modules/parking_places/parking_places_module.js'];
          pp_module.map_cursor_za_odabir_koordinata();
          
          // ako se radi o tehnozavodu koji ima id "6"
          // ------------- SOLAR PANEL JE VIDLJIV SAMO ZA PARK 15
          // ------------- SOLAR PANEL JE VIDLJIV SAMO ZA PARK 15
          // ------------- SOLAR PANEL JE VIDLJIV SAMO ZA PARK 15
          
          if ( pp_module.cit_data.pp_sifra == '15' ) {
            $('#pp_solar_panel_title_row').css('display', 'block');
            $('#pp_solar_panel_card').css('display', 'block');
            
            // obriši postojeći graf da mogu kreirati novi
            $('#graf_solar').remove();
            
            /*
            window.graf_data = {
              time_grupa: 'MIN',
              id: 'graf_solar',
              lines: [
                {id: 'line_solar_napon', color: 'red', points: [ 1, 2, 3, 10, 5, 8, 10, 2 ] },
                {id: 'line_solar_struja', color: '#248efe', points: [ 0.5, 3.56, 8.57, 12.82, 6.05, 4.85, 7.16, 3.56 ] }
              ]
            };
            */

            
            // setTimeout( function() { window.create_we_graf(window.graf_data); }, 200);

          } else {
            
            $('#pp_solar_panel_title_row').css('display', 'none');
            $('#pp_solar_panel_card').css('display', 'none');
            
          };
          
        }, 200);
        
        /*
        var pp_owners_url = '/main_modules/owners_module/owners_module.js';
        var pp_owners_mod = window[pp_owners_url];
        pp_owners_mod.cit_data = result_array[0].pp_owner;
        pp_owners_mod.data_to_form(pp_owners_mod.cit_data);
        show_save_or_edit_button('edit_pp_owner_btn');
        */
        
    
      },
      function( error ) { 
        console.error( error );
      }
    );
    
    
    
  };
  this_module.open_park_place = open_park_place;

  function mark_not_published_pp() {
    
    var all_parks = $('#park_places_list_place_here').data('we_result');
    $.each( all_parks, function(pp_index, park) {
      if ( park.published == false ) {
        $('#list_item_' + park._id).prepend('<div class="not_published_tag">NEOBJAVLJENO</div>')
        
      }
    });
    
  };
  this_module.mark_not_published_pp = mark_not_published_pp;
  
  function create_initial_pp_list(result_array, props ) {
    
    
    $(".cit_place_list_here").each( function () {
      if ( this.id !== 'park_places_list_and_find' ) {
        $(this).addClass('cit_close');
      } else {
        $(this).removeClass('cit_close');
      };
    });
    
    
    create_cit_result_list( result_array, props );

    setTimeout( function() { 
      this_module.add_perfectbar_and_click_events();
      this_module.mark_not_published_pp();
      
      
    }, 100);
 
  };
  this_module.create_initial_pp_list = create_initial_pp_list;

  
  function add_perfectbar_and_click_events() {
    
   
    wait_for( `$('#park_places_list_place_here').find('.search_result_table').length > 0`, function() {
      
      if ( !window.pp_list_perfectbar ) {
        window.pp_list_perfectbar = new PerfectScrollbar( $('#park_places_list_place_here').find('.search_result_table')[0] );
      } else {
        window.pp_list_perfectbar.update();
      };
      
    }, 500000);
      
    var pp_list_row = $('#park_places_list_place_here .search_result_row');

    pp_list_row.off('click');
    pp_list_row.on('click', function(e) {
      var pp_id = this.id.replace('list_item_', '');
      this_module.open_park_place(pp_id);
    });
    
  };
  this_module.add_perfectbar_and_click_events = add_perfectbar_and_click_events;
  
  this_module.cit_loaded = true;   

}
  
  
};
