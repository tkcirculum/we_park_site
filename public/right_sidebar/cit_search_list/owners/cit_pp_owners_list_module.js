var module_object = {
  create: function( data, parent_element, placement ) {

    var component_id = data.id;
    var component_html =
    `
<div id="${data.id}" class="cit_search_list">
  
</div>

    `;
   
    setTimeout(function() { 
      $('.cit_tooltip').tooltip();
    }, 50);

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
      var this_module = window[module_url]; 
      this_module.data = data;
    };

    return {
      html: component_html,
      id: component_id
    };


  },
 
scripts: function () {

  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  

  wait_for(`$('#pp_owners_list_place_here').length > 0`, function () {

    // provjeri je otvorena PP OWNER  stranica tako što provjeriš jel link u lijevom meniju aktivan
    if ( $('#left_menu_button_owners').hasClass('cit_active') ) {

      var pp_owners_search_options = {
        search_array: null,
        current_input_id:  null,
        current_cit_search: 'search_pp_owners',
        current_cit_select_fields: 'all',
        current_cit_return_fields: '_id,full_name,user_address,email',
        custom_headers: 'ID,Ime,Adresa,Email',
        col_widths: '0,34,33,33',
        current_cit_show: '',
        current_cit_hide: '',
        parent_container: $('#pp_owners_list_place_here'),
        filter_funkcije: null,
        design: 'sidebar_list',
        just_show: true,
        limit: 100
      };

      if ( window.pp_owners_list_perfectbar ) {
        window.pp_owners_list_perfectbar.destroy();
        window.pp_owners_list_perfectbar = null;
      };
      $('#pp_owners_list_place_here').find('.search_result_table').remove();

      search_database(null, pp_owners_search_options).then(
        function( result_array ) { this_module.create_initial_pp_owners_list(result_array, pp_owners_search_options ) },
        function( error ) { 
          console.error( error );
        }
      );


    }; 
    
  }, 5000000 );      
  
  
  function open_pp_owner(id) {
    
    $('#owners_component input').val('');
    
    
    var pp_search_options = {
      search_array: null,
      current_input_id:  null,
      current_cit_search: 'search_pp_owners',
      current_cit_select_fields: '_id',
      current_cit_return_fields: 'all',
      custom_headers: '',
      col_widths: '',
      current_cit_show: '',
      current_cit_hide: '',
      filter_funkcije: null,
      design: 'sidebar_list',
      just_show: true,
      limit: null
    };
    
    search_database(id, pp_search_options).then(
      function( result_array ) {
        console.log(result_array[0]);
        var pp_owners_mod_url = '/main_modules/owners_module/owners_module.js';
        var pp_owners = window[pp_owners_mod_url];
        pp_owners.cit_data = result_array[0];
        
        pp_owners.data_to_form(pp_owners.cit_data);
        
        show_save_or_edit_button('edit_pp_owner_btn');
    
      },
      function( error ) { 
        console.error( error );
      }
    );
    
    
    
  };
  this_module.open_pp_owner = open_pp_owner;

  
  function create_initial_pp_owners_list(result_array, pp_owner_search_options ) {
    
    
    create_cit_result_list( result_array, pp_owner_search_options, 'append' );

    window.pp_owners_list_perfectbar = new PerfectScrollbar( $('#pp_owners_list_place_here').find('.search_result_table')[0] );

    var pp_owners_list_row = $('#pp_owners_list_place_here .search_result_row');

    pp_owners_list_row.off('click');
    pp_owners_list_row.on('click', function(e) {
      var pp_owner_id = this.id.replace('list_item_', '');
      this_module.open_pp_owner(pp_owner_id);
    });  
 
  };
  this_module.create_initial_pp_owners_list = create_initial_pp_owners_list;
  
 
  function get_cit_date_from(input_element) {


    if ( !input_element ) return;
    if ( input_element.length == 0 ) return;

    /*
    ------------------------------------
    BITNO:
    Pravilan upis datume je npr 20.04.2018 ili 20.04.2018. <--- dakle može i ne mora biti točka na kraju !!!!
    ------------------------------------
    */

    var datum_upisano_from = $('#trazi_sve_ponude_datum_from').val();

    if (datum_upisano_from == '') return null;

    var start_date = datum_upisano_from.split('.');

    if (start_date.length < 3 ) return 'error';

    var date_num = null;

    var dan = Number( start_date[0] );
    var mjesec = Number( start_date[1] );
    var godina = Number( start_date[2] );

    if ( !$.isNumeric(dan) ) date_num = 'error';
    if ( !$.isNumeric(mjesec) ) date_num = 'error';
    if ( !$.isNumeric(godina) ) date_num = 'error';

    if ( date_num == null ) date_num = new Date(godina, mjesec-1, dan, 0, 0).getTime();

    return date_num;

  };

  this_module.cit_loaded = true;   

}
  
  
};
