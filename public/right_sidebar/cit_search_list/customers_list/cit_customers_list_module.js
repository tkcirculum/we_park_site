var module_object = {
  create: function( data, parent_element, placement ) {

    var component_id = data.id;
    var component_html =
    `
<div id="${data.id}" class="cit_search_list">
  
</div>

    `;
   
    setTimeout(function() { 
      $('.cit_tooltip').tooltip();
    }, 50);

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
      var this_module = window[module_url]; 
      this_module.data = data;
    };

    return {
      html: component_html,
      id: component_id
    };


  },
 
scripts: function () {

  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  

  wait_for(`$('#pp_customers_list_place_here').length > 0`, function () {

    // provjeri je otvorena parking place stranica tako što provjeriš jel link u lijevom meniju aktivan
    if ( $('#left_menu_button_customers').hasClass('cit_active') ) {
      
      
      var customer_list_props = {
        desc: 'za dobivanje liste customera u desnom stupcu',
        local: false,
        url: '/search_customers',
        col_widths: [0, 65, 35 ],
        custom_headers: ['ID', 'Email', 'Nadimak'],
        return: { _id: 1, email: 1, name: 1 },
        hide_cols: ['_id'],
        query: {},
        show_on_click: true,
        parent: '#pp_customers_list_place_here',
        list_class: 'sidebar_list'
      };    
 

      $('#pp_customers_list_place_here').find('.search_result_table').remove();

      search_database(null, customer_list_props).then(
        function( result_array ) { 
          this_module.create_customer_list(result_array, customer_list_props );
        },
        function( error ) { 
          console.error( error );
        }
      );

    }; // kraj uvijeta ako je parking place stranica/module aktivana
    
  }, 5000000 );      
  
  
  function open_customer(id) {
    
    // obriši sve inpute unutar customer component diva ( to je glavni dio u sredini sa cijelom formom !!! )
    $('#customers_component input').val('');
    
      var customer_props = {
        desc: 'kod otvaranja profila customera',
        local: false,
        url: '/search_customers',
        return: {},
        query: {_id: id },
        show_on_click: true,
        parent: 'none'
      };
    
    
    search_database(id, customer_props).then(
      function( result_array ) {
        console.log(result_array[0]);
        
        var customer_module = window['/main_modules/customers_module/customers_module.js'];
        customer_module.cit_data = result_array[0];
        
        customer_module.data_to_form(customer_module.cit_data);
        show_save_or_edit_button('edit_customer_btn');
        
        get_user_products_state(customer_module.cit_data.user_number).then(
          function(response) {
            
            
            customer_module.site_monthly_list(response);
            customer_module.site_user_balance(response);
            customer_module.site_card_num_list(response);
            
          },
          function(error) { console.log(error)}
        );
        
      },
      function( error ) { 
        console.error( error );
      }
    );
    
    
    
  };
  this_module.open_customer = open_customer;

  
  function create_customer_list(result_array, customer_list_props ) {
    
    // loop po svim divovima za liste u desnom sidebaru
    $(".cit_place_list_here").each( function () {
      if ( this.id !== 'pp_customers_list_and_find' ) {
        $(this).addClass('cit_close');
      } else {
        $(this).removeClass('cit_close');
      };
      
    });
    
    create_cit_result_list( result_array, customer_list_props );

    
    this_module.add_perfectbar_and_click_events(); 
 
    // otvori profil od current usera ako user nije admin !!!!
    setTimeout( function() {
      if ( window.cit_user && is_super() == false ) open_customer(window.cit_user._id);
    }, 200);
    
  };
  this_module.create_customer_list = create_customer_list;

  
  
  
  function add_perfectbar_and_click_events() {
    
    if ( !window.customer_list_perfectbar ) {
      window.customer_list_perfectbar = new PerfectScrollbar( $('#pp_customers_list_place_here').find('.search_result_table')[0] );
    } else {
      window.customer_list_perfectbar.update();
    };
      
    var customer_list_row = $('#pp_customers_list_place_here .search_result_row');

    customer_list_row.off('click');
    customer_list_row.on('click', function(e) {
      var customer_id = this.id.replace('list_item_', '');
      this_module.open_customer(customer_id);
    });   
    
  };
  this_module.add_perfectbar_and_click_events = add_perfectbar_and_click_events;
  
  
  this_module.cit_loaded = true;   

}
  
  
};









