var module_object = {
  create: function( comp_data, parent_element, placement ) {
    
    
    var customer_list_module_url = '/right_sidebar/cit_search_list/customers_list/cit_customers_list_module.js';
    get_cit_module(customer_list_module_url, 'load_css');
    
    function all_components_needed() {
      return (
        window[customer_list_module_url]                    && 
        window[customer_list_module_url].cit_loaded == true
      )
    };
    
    wait_for(all_components_needed, function() {
      
      var component_id = null;
      
      var component_html = 
`
<div id="pp_customers_list_bar" class="cit_list_bar">SVI KORISNICI <i class="fas fa-caret-up"></i></div>
<div id="pp_customers_list_and_find" class="cit_place_list_here cit_close">

  <div id="pp_customers_find_place_here">
    <div id="sidebar_find_user_label" class="cit_label">Pronađi korisnika:</div>
    <input autocomplete="off" id="sidebar_find_user" style="margin: 0 5px 10px; width: calc(100% - 10px);" type="text" class="cit_input we_auto simple">  
  </div>
  <div id="pp_customers_list_place_here"></div>
  
</div>

<div id="park_places_list_bar" class="cit_list_bar">SVA PARKIRALIŠTA <i class="fas fa-caret-up"></i></div>
<div id="park_places_list_and_find" class="cit_place_list_here">
  <div id="park_places_find_place_here"></div>
  <div id="park_places_list_place_here"></div>
</div>

<div id="pp_owners_list_bar" class="cit_list_bar">SVI VLASNICI <i class="fas fa-caret-up"></i></div>
<div id="pp_owners_list_and_find" class="cit_place_list_here">
  <div id="pp_owners_find_place_here"></div>
  <div id="pp_owners_list_place_here"></div>
</div>


`;
      

    wait_for( `$('#sidebar_find_user').length > 0`, function() {

      var this_module = window[module_url];     
      
      $('#sidebar_find_user').data('we_auto', {
        desc: 'pretraživanje usera u right sidebar listi',
        local: false,
        url: '/search_customers',
        col_widths: [0, 65, 35 ],
        custom_headers: ['ID', 'Email', 'Nadimak'],
        return: { _id: 1, email: 1, name: 1 },
        hide_cols: ['_id'],
        query: {},
        show_on_click: false,
        parent: '#pp_customers_list_place_here',
        list_class: 'sidebar_list'
      });
      
    }, 500000);
      
      

      if ( parent_element ) {
        cit_place_component(parent_element, component_html, placement);
      }; 
      
      return {
        html: component_html,
        id: component_id
      };

    }, ( 60 * 1000 )); // 1 min timeout
    
  }, 
  
  scripts: function() {
    /*
    wait_for(`$('.cit_search_list').length > 0`, function() {
      
      $('.cit_scrollbar').each(function() {
        new PerfectScrollbar(this); 
      });
      
    }, 5000000);
    */
  }
};
