var module_object = {
  
  create: function( data, parent_element, placement ) {
    
    var component_id = null;
    
    var component_html =
    `
<div id="layer_under_cit_popup_modal">
  <div id="cit_popup_modal">
    <div id="cit_popup_modal_message">
    <!--HERE I PUT POPUP HTML MESSAGES -->
    </div>
    
    <div id="cit_popup_modal_progress_bar">
      <div id="cit_popup_modal_complete_bar">0%</div>
    </div>  
    
    <div id="cit_popup_modal_button_row">
      <div class="cit_popup_modal_button" id="cit_popup_modal_YES">${ w.we_trans( 'id__cit_popup_modal_YES', w.we_lang) }</div>
      <div class="cit_popup_modal_button" id="cit_popup_modal_OK">OK</div>
      <div class="cit_popup_modal_button" id="cit_popup_modal_NO">${ w.we_trans( 'id__cit_popup_modal_NO', w.we_lang) }</div>
    </div>
    
  </div>
</div>
    `;
   
    
    wait_for( `$('#cit_popup_modal').length > 0`, function() {
      if ( $('.cit_tooltip').length > 0 ) $('.cit_tooltip').tooltip();
    }, 50000000 );
    
    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    } 
    else {
      return {
        html: component_html,
        id: component_id
      } 
    };
      
  },
 
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module
  var this_module = window[module_url]; 

function show_popup_modal(show_popup, popup_text, popup_progress, show_buttons) {
  
  if ( !show_popup ) {
    
    // sakrije layer ispod modala
    $('#layer_under_cit_popup_modal').removeClass('cit_show');     
    
    // sakrij modal
    // $('#cit_popup_modal').removeClass('cit_show');

    return;
  };
  
  
  $('#cit_popup_modal_message').removeAttr('style');
  
  
  $('#cit_popup_modal_message').html(popup_text || '');
  
  var progres_text = popup_progress ? (popup_progress + '%') : '';
  
  $('#cit_popup_modal_complete_bar').text(progres_text);
  
  
  /* -------------START--------------------- IF SHOW POPUP ---------------------------------- */
  
  if ( show_popup && $('#layer_under_cit_popup_modal').hasClass('cit_show') == false ) {
    
      // always erase/reset all previous events if any on YES, NO and OK buttons !!!!
      $('#cit_popup_modal_YES').off('click');
      $('#cit_popup_modal_OK').off('click');
      $('#cit_popup_modal_NO').off('click');

      // pokaži layer ispod modala
      $('#layer_under_cit_popup_modal').addClass('cit_show');  

      // pokaži modal
      // $('#cit_popup_modal').addClass('cit_show');
     
      // remove all classes
      $('#cit_popup_modal_message').removeClass('cit_error');
      $('#cit_popup_modal_message').removeClass('cit_warning');
      $('#cit_popup_modal_message').removeClass('cit_success');
      $('#cit_popup_modal_message').removeClass('cit_pay_success');
      $('#cit_popup_modal_message').removeClass('cit_pay_warning');
      $('#cit_popup_modal_message').removeClass('cit_pay_error');
      
      if ( show_popup == 'error' )  $('#cit_popup_modal_message').addClass('cit_error');
      if ( show_popup == 'warning' )  $('#cit_popup_modal_message').addClass('cit_warning');
      if ( show_popup == 'pay_success' )  $('#cit_popup_modal_message').addClass('cit_pay_success');
      if ( show_popup == 'pay_warning' )  $('#cit_popup_modal_message').addClass('cit_pay_warning');
      if ( show_popup == 'pay_error' )  $('#cit_popup_modal_message').addClass('cit_pay_error');
    
      if ( popup_progress ) {
        $('#cit_popup_modal_progress_bar').css({'display': 'block'});
        $('#cit_popup_modal_complete_bar').css('width', popup_progress + '%');
      } else {
        $('#cit_popup_modal_progress_bar').css({'display': 'none'});
      };

      if ( !show_buttons ) {
        $('#cit_popup_modal_button_row').css('display', 'none');
      };

    
      // resetiraj boju gumba OK
      $('#cit_popup_modal_OK').css({
        'background-color': '',
        'box-shadow': '',
        'color': ''
      });  
    
      if ( show_popup == 'warning' ) {
        $('#cit_popup_modal_OK').css({
          'background-color': '#ffd300',
          'box-shadow': '0px 3px #af9102',
          'color': '#503601'
        });
      };
    
      if ( show_popup == 'error' ) {
        $('#cit_popup_modal_OK').css({
          'background-color': '#ff0a00',
          'box-shadow': '0px 3px #bf0b03',
          'color': '#fff'
        });
      };

      if (show_buttons == 'yes/no') {
        $('#cit_popup_modal_button_row').css('display', 'flex');
        $('#cit_popup_modal_button_row').css('justify-content', 'space-between');

        $('#cit_popup_modal_YES').css('display', 'block');
        $('#cit_popup_modal_OK').css('display', 'none');
        $('#cit_popup_modal_NO').css('display', 'block'); 
        
        
        setTimeout(function() {
          
          var yes_btn_text =  w.we_trans( 'id__cit_popup_modal_YES', w.we_lang)
          var no_btn_text =  w.we_trans( 'id__cit_popup_modal_NO', w.we_lang)
          
          $('#cit_popup_modal_YES').text(yes_btn_text);
          $('#cit_popup_modal_OK').text('OK');
          $('#cit_popup_modal_NO').text(no_btn_text);
          
        }, 50);
        
      };

      if (show_buttons == 'ok') {
        $('#cit_popup_modal_button_row').css('display', 'flex');
        $('#cit_popup_modal_button_row').css('justify-content', 'center');

        $('#cit_popup_modal_YES').css('display', 'none');
        $('#cit_popup_modal_OK').css('display', 'block');
        $('#cit_popup_modal_NO').css('display', 'none'); 
      };  
    
  }; 
  
/* -------------END--------------------- IF SHOW POPUP ---------------------------------- */

  
};
this_module.show_popup_modal = show_popup_modal;
 
this_module.cit_loaded = true;  
 
}
  
  
};