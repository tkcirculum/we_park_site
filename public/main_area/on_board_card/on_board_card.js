var module_object = {
  create: function( data, parent_element, placement ) {
    

    var this_module = window[module_url]; 
    
    var component_id = null;
      
      
    var component_html =
    `
<div id="on_board_card">
  <img id="on_board_we_logo" src="/img/wepark_logo_white.png">
  <div id="on_board_title">Prijava vašeg parkirališta</div>
  <div id="on_board_text">
    
    <div class="row" style="margin-top: 30px;">
      <div class="col-md-3 col-sm-12">  
        <div class="cit_list_item">
      <div>1.</div>
      <p>Molimo Vas registrirajte se tako da popunite polja u lijevom stupcu i kliknite <span>REGISTRACIJA</span></p>
    </div>
      </div>
      <div class="col-md-3 col-sm-12">  
        <div class="cit_list_item">
      <div>2.</div>
      <p>
        Ako ste već koristite našu mobilnu aplikaciju kliknite na <span style="background-color: transparent; color: #e64532; padding: 0;">Već imam račun</span>, upišite nadimak i lozinku koju imate u aplikaciji i kliknite na <span>PRIJAVA</span>
      </p>
    </div>
      </div>
      <div class="col-md-3 col-sm-12">  
        <div class="cit_list_item">
      <div>3.</div>
      <p>Kada ste se ulogirali, kliknite na link 
        <span style="background-color: transparent; font-weight: 600; letter-spacing: 0.08rem; padding: 0; color: #abaaaa;">
          <i class="fas fa-parking" style="margin-right: 5px;"></i>PARKIRALIŠTA
        </span> 
        u lijevom stupcu.</p>
    </div>
      </div>
      <div class="col-md-3 col-sm-12">  
        <div class="cit_list_item">
      <div>4.</div>
      <p>Nakon toga popunite podatke o svom parkiralištu u poljima ispod karte.</p>
    </div>
      </div>
    </div>  
    
    <div class="row" style="margin-top: 30px;">
      <div class="col-md-3 col-sm-12">
        <div class="cit_list_item">
      <div>5.</div>
      <p>
        Također možete pronaći vaš parking na karti i kliknite na to mjesto.
        Koordinate parkinga će se pojaviti u poljima zemljopisna širina i dužina.</p>
    </div>
      </div>
      <div class="col-md-3 col-sm-12">
        <div class="cit_list_item">
      <div>6.</div>
      <p>Ako niste sigurni što trebate upisati, ne brinite!</p>
    </div>
      </div>
      <div class="col-md-3 col-sm-12">
        <div class="cit_list_item">
      <div>7.</div>
      <p>Upišite samo informacije koje znate i želite!</p>
    </div>
      </div>
      <div class="col-md-3 col-sm-12">
        <div class="cit_list_item">
      <div>8.</div>
      <p>Važno je samo da upišete broj telefona kako bi vas naši agenti mogli nazvati :)</p>
    </div>
      </div>
  </div>
  
</div>
    `;
    
      // ovo je arbitrarni uvijet  - uzeo sam prvi label u formi kao uvijet kad se pojavi ....
  wait_for( `$('#on_board_card').length > 0`, function() {
    
    $('.cit_tooltip').tooltip();
    
    $('#start_saving_pp_btn').off('click');
    $('#start_saving_pp_btn').on('click', this_module.start_saving_pp_process );
    
  }, 5000000 );      

    if ( parent_element ) {
      
      cit_place_component(parent_element, component_html, placement);
        
      setTimeout( function() {
        window.cit_main_area_perfectbar.update();
      }, 200);

    }; 
    
    return {
      html: component_html,
      id: component_id
    };

  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  
  // var show_popup_modal = window['/main_area/site_popup_modal/site_popup_modal.js'].show_popup_modal;
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {
    
    this_module.cit_data = {};
  };
  
  
  this_module.cit_loaded = true;
 
}
  
  
};