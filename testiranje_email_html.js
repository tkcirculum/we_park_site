$(document).ready(function() {  
  
  
  
  var newsletter_html = 
      
`
<div id="${Math.random().toString(16).substring(2,10)}" 
style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #fff;
            position: relative;
            margin: 0 auto;
            padding: 0;
            max-width: 1000px;">

<!--
  div id="${Math.random().toString(16).substring(2,10)}" 
style="  font-size: 50px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    color: #fff;
                    font-weight: 700;
                    line-height: 60px;
                    letter-spacing: normal;
                    text-align: left;
                  position: absolute;
                  width: 50%;
                  height: auto;
                  top: 30px;
                  right: 0;">
        
       Nova WePark Aplikacija!

      </div> 
-->        
<!--  
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_bg.jpg" style="width: 100%; height: auto;" />
  -->
  
  <!--
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_download_app.jpg" style="width: 100%; height: auto; max-width: 400px;" />
  
  
  <div style="width: 100%; height: auto; clear: both; text-align: center; margin: 10px 0 30px;">
    
    <a href="https://play.google.com/store/apps/details?id=com.wePark2" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/google_play.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    <a href="https://apps.apple.com/us/app/wepark-park-with-smile/id1314272266?ls=1" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/app_store.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    
  </div>
  
  
 --> 
    <div id="${Math.random().toString(16).substring(2,10)}" 
style="  height: auto;
                  text-align: center;
                  font-family: Arial, Helvetica, sans-serif;
                  padding: 0;
                  position: relative;
                  width: 100%;">

      <div id="${Math.random().toString(16).substring(2,10)}"  style="    
        font-size: 13px;
        margin-top: 40px;
        margin-bottom: 20px;
        color: #454545;
        font-weight: 400;
        text-align: left;
        line-height: 26px;
        max-width: 500px;
        clear: both;
        width: 90%;
        margin: 0 auto;
        padding: 10px;
        height: auto;
        background-color: #fff;
        border-radius: 10px;
        border: 1px solid #e8e6e6;
        letter-spacing: 0.04rem;">


<!--<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> -->

        
              
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />


        
<div style="float: left;
    width: 100%;
    text-align: center;
    font-size: 27px;
    font-weight: 700;
    margin-top: 0px;
    line-height: normal;
    letter-spacing: normal;
    color: #06253f;">
<br>
Plan lansiranja nove aplikacije
<br>
<br>

</div>

        
        <div style="padding: 10px; float: left; clear: both;">
          <img src="https://wepark.circulum.it:9000/img/002_nl/002_nl_29_01.png" style=" width: 100%;
                                                        height: auto;
                                                        display: block;
                                                        margin: 0 auto;" />
          <h2 style="color: #6f6f6f; text-align: center;">29.01. stara aplikacija radi cijeli dan</h2>
          <p style="padding-left: 0;
                    text-align: justify;
                    padding-right: 0;
                    line-height: 1.6;">
            Kako bi maksimalno ublažili prelazak na novi app, sutra će stara aplikacija raditi cijeli dan.
          </p>
        
        </div>
        
        <div style="padding: 10px; float: left; clear: both;">
          
          <img src="https://wepark.circulum.it:9000/img/002_nl/002_nl_29_01_16h.png" style=" width: 100%;
                                                            height: auto;
                                                            display: block;
                                                            margin: 0 auto;" />
          
          
          <h2 style="color: #6f6f6f; text-align: center;">29.01.u 16 sati ćemo vam poslati link za preuzimanje nove aplikacije </h2>
          <p style="padding-left: 0;
                    text-align: justify;
                    padding-right: 0;
                    line-height: 1.6;">
            Nakon što završite s parkiranjem i ako ste sigurni da više nećete koristiti parking, instalirajte novu aplikaciju. Nova aplikacija još uvijek neće biti funkcionalna.
            <br><br>
            Napomena:<br>
            <br>
          <span style="text-align: left; 
                       display: block;
                       padding: 20px;
                       background-color: #e64530;
                       color: #fff;
                       font-size: 14px;
                       font-weight: 700;
                       border-radius: 10px;">
          
            ANDROID korisnici će imati i staru i novu aplikaciju na mobitelu, dok će iPhone korisnici imati samo novu aplikaciju.
            Korisnici ANDROID-a mogu obrisati staru aplikaciju nakon instalacije nove.
          
          </span>
            
          </p>
        
        </div>
        
        <div style="padding: 10px; float: left;  clear: both;">
          
          <img src="https://wepark.circulum.it:9000/img/002_nl/002_nl_30_01_6h.png" style=" width: 100%;
                                                  height: auto;
                                                  display: block;
                                                  margin: 0 auto;" />

          <h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">
            30.01. u 6 sati ujutro nova aplikacija će postati aktivna!
          </h2>

        </div>
        

<div style="float: left;
    width: 100%;
    text-align: center;
    font-size: 27px;
    font-weight: 700;
    margin-top: 0px;
    line-height: normal;
    letter-spacing: normal;
    color: #06253f;">
<br>
<br>
Prvi korak kada aplikacija postane aktivna?
<br>
<br>

</div>

        
        
        <div style="padding: 10px; float: left;  clear: both;">
          
          <img src="https://wepark.circulum.it:9000/img/002_nl/002_nl_login.jpg" style=" width: 100%;
                                                  height: auto;
                                                  display: block;
                                                  margin: 0 auto;" />

          <h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">
Obavezna promjena lozinke za postojeće korisnike
          </h2>
          <p style="padding-left: 0;
                    text-align: justify;
                    padding-right: 0;
                    line-height: 1.6;">
Zaista smo jako puno stvari promijenili u novoj verziji aplikacije uključujući i sigurnosni sustav.
Zbog toga je neophodno upisati novu verziju lozinke kako bi osigurali bolju zaštitu vaših podataka.
          </p>
        </div>         
        
        
        <div style="padding: 10px; float: left;  clear: both;">
          
          <img src="https://wepark.circulum.it:9000/img/002_nl/002_nl_change_pass.jpg" style=" width: 100%;
                                                  height: auto;
                                                  display: block;
                                                  margin: 0 auto;" />

          <h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">
            Izaberite promjenu lozinke
          </h2>
          <p style="padding-left: 0;
                    text-align: center;
                    padding-right: 0;
                    line-height: 1.6;">
            Promjena lozinke je dostupna i na formi za registraciju kao i na formi za login.
          </p>
        </div>          
        
    
        
        <div style="padding: 10px; float: left;  clear: both;">
          
          <img src="https://wepark.circulum.it:9000/img/002_nl/002_nl_mail_to_reset.jpg" style=" width: 100%;
                                                  height: auto;
                                                  display: block;
                                                  margin: 0 auto;" />

          <h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">
            Unesite email i novu lozinku
          </h2>
          <p style="padding-left: 0;
                    text-align: center;
                    padding-right: 0;
                    line-height: 1.6;">
Potrebno je upisati email adresu od postojećeg korisničkog računa.<br>
Lozinka mora imati minimalno 6 znakova.<br><br>

Nakon što kliknete na gumb POŠALJI EMAIL ZA POTVRDU potrebno je pronaći mail koji smo vam poslali i kliknuti na gumb POTVRDI koji se nalazi u mailu.

Ako se ne možete prisjetiti email adrese ili budete imali bilo koji drugi problem, molimo nazovite kontakt ispod.
<br>

<br>
Sretno parkiranje!
<br>
            
            
          </p>
        </div>        
        
        
        
        <div style="padding: 0; float: left;  clear: both; width: 100%;">
          <h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">Kontakt</h2>
          <p style="padding-left: 0;
                    text-align: center;
                    padding-right: 0;
                    line-height: 1.6;">
            
            Ako imate bilo koje pitanje molimo obratite se na<br>
            +385 92 3133 015<br>
            ili na email<br>
            toni.kutlic@wepark.eu
          </p>
        </div>        

        
        <br style="display: block; clear: both;">
        
                
        <br>
        <br>
        
      </div>

      <br style="display: block; clear: both;">
      
    </div>
  
  
  
        <br>
        <br>
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_footer.jpg" style="width: 100%; height: auto; display: block; clear: both;"/>
  

    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #fff;
                padding: 0;">

      
      
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />



      <div id="${Math.random().toString(16).substring(2,10)}" style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 18px;
        border-top: 1px solid #eaeaea;
        padding-top: 10px;
        background-color: #fff;
        letter-spacing: 0.06rem;">
        

        <b>Odgovorna osoba:</b><br>
        <b>Karlo Starčević (direktor)</b><br><br>
        <b>Wepark smart parking d.o.o.</b><br>
        <b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
        <b>OFFICE:</b>&nbsp;zgrada IGH, Rakušina 1, 10000 Zagreb<br>
        <b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
        <b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
        <b>WEB:</b>&nbsp;www.wepark.eu<br>
        <br>
        <div style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
        Društvo je upisano u sudski registar Trgovačkog suda u Zagrebu pod brojem 081131571. Temeljni kapital u iznosu od 20.000 kn je uplaćen u cijelosti. Račun se vodi kod Privredne Banke Zagreb. Član društva Karlo Starčević.
        </div>
        <br style="display: block; clear: both;">
        
        
      </div>        
      
      <br id="${Math.random().toString(16).substring(2,10)}"  style="display: block; clear: both;">
    </div>  

    
</div>     



`;  
  

  $('body').prepend(newsletter_html);
  
}); // end of doc ready 
  
  