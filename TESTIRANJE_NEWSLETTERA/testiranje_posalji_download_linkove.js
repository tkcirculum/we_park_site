$(document).ready(function() {  
  
  
  
  var newsletter_html = 
      
`
<div id="${Math.random().toString(16).substring(2,10)}" 
style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #fff;
            position: relative;
            margin: 0 auto;
            padding: 0;
            max-width: 1000px;">

<!--
  div id="${Math.random().toString(16).substring(2,10)}" 
style="  font-size: 50px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    color: #fff;
                    font-weight: 700;
                    line-height: 60px;
                    letter-spacing: normal;
                    text-align: left;
                  position: absolute;
                  width: 50%;
                  height: auto;
                  top: 30px;
                  right: 0;">
        
       Nova WePark Aplikacija!

      </div> 
-->        

  
  <img src="https://wepark.circulum.it:9000/img/001_nl_bg.jpg" style="width: 100%; height: auto;" />
  
  
  
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />

  
  
  
  <div style="float: left;
    width: 100%;
    text-align: center;
    font-size: 27px;
    font-weight: 700;
    margin-top: 0px;
    margin-bottom: 30px;
    line-height: normal;
    letter-spacing: normal;
    color: #06253f;">
<br>
  
Napokon linkovi za instalaciju<br>
nove WePark aplikacije :)
  
<br>


</div>
  <br style="display: block; clear: both;">

<div style="padding: 0; float: none; clear: both; width: 100%; max-width: 400px; margin: 0 auto;">
<h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">BITNA NAPOMENA</h2>
<p style="padding-left: 0;
          text-align: center;
          padding-right: 0;
          line-height: 1.6;">
    Kada instalirate aplikaciju, trenutno nećete moći koristiti sve funkcionalnosti i nećete se moći ulogirati.<br>
    S obzirom da trenutno još uvijek ima parkiranih automobila koji su ušli sa postojećom aplikacijom,
    još uvijek nismo aktivirali novu aplikaciju.
  <br>
  <br>
  
  <b>Nova aplikacija će biti u potpunosti funkcionalna sutra ujutro.</b>
    
  </p>    
</div>        
  
<br style="display: block; clear: both;">  
  
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_download_app.jpg" style="width: 100%; height: auto; max-width: 400px;" />
  
  
  <div style="width: 100%; height: auto; clear: both; text-align: center; margin: 10px 0 30px;">
    
    <a href="https://play.google.com/store/apps/details?id=com.wePark" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/google_play.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    <a href="https://apps.apple.com/us/app/wepark-park-with-smile/id1314272266?ls=1" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/app_store.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    
  </div>
     
<br style="display: block; clear: both;">    
  
  
<div style="padding: 0; float: none; clear: both; width: 100%; max-width: 400px; margin: 0 auto;">
  
<h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">Kontakt</h2>
<p style="padding-left: 0;
          text-align: center;
          padding-right: 0;
          line-height: 1.6;">

  Ako imate bilo koje pitanje molimo obratite se na<br>
  +385 92 3133 015<br>
  ili na email<br>
  toni.kutlic@wepark.eu
</p>
</div>        


<br style="display: block; clear: both;">
  
  
        <br>
        <br>
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_footer.jpg" style="width: 100%; height: auto; display: block; clear: both;"/>
  

    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #fff;
                padding: 0;">

      
      
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />



      <div id="${Math.random().toString(16).substring(2,10)}" style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 18px;
        border-top: 1px solid #eaeaea;
        padding-top: 10px;
        background-color: #fff;
        letter-spacing: 0.06rem;">
        

        <b>Odgovorna osoba:</b><br>
        <b>Karlo Starčević (direktor)</b><br><br>
        <b>Wepark smart parking d.o.o.</b><br>
        <b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
        <b>OFFICE:</b>&nbsp;zgrada IGH, Rakušina 1, 10000 Zagreb<br>
        <b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
        <b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
        <b>WEB:</b>&nbsp;www.wepark.eu<br>
        <br>
        <div style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
        Društvo je upisano u sudski registar Trgovačkog suda u Zagrebu pod brojem 081131571. Temeljni kapital u iznosu od 20.000 kn je uplaćen u cijelosti. Račun se vodi kod Privredne Banke Zagreb. Član društva Karlo Starčević.
        </div>
        <br style="display: block; clear: both;">
        
        
      </div>        
      
      <br id="${Math.random().toString(16).substring(2,10)}"  style="display: block; clear: both;">
    </div>  

    
</div>     



`;  
  

  $('body').prepend(newsletter_html);
  
}); // end of doc ready 
  
  