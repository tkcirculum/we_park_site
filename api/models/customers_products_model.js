'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var customer_product_object = { 
 
  /* postojeci fieldovi  */    
  
  user_number: { type: Number, required: false },
  pp_sifra: { type: String, required: false },
  
  sifra: { type: String, required: false },
  code: { type: String, required: false },
  amount: { type: Number, required: false },
  
  desc : { type: String, required: false },
  buy_time: { type: Number, required: false },
  start_time: { type: Number, required: false },
  
  is_free: { type: Boolean, required: false },
  

  /* moji fieldovi  */    
  price: { type: Number, required: false },
  type: { type: String, required: false },
  valid_from: { type: Number, required: false },
  valid_to: { type: Number, required: false },
  balance: { type: Number, required: false },
  
  tag: { type: String, required: false },
  
  /* jesam li poslao mail da istice pretplata za 3 dana ???? */
  reminder: { type: Boolean, required: false },
  
  /* ovo su podaci vezani za fiskalizaciju */
  zki: { type: String, required: false },
  jir: { type: String, required: false },
  racun_br: { type: String, required: false },
  fis_id: { type: String, required: false },
  fis_greska: { type: String, required: false },
  fis_time: { type: Number, required: false },
  
  storno: { type: Number, required: false }
  
}; 

var customer_product_schema = new Schema( 
  customer_product_object,  
  { collection : 'customer_products'});  
 
module.exports = mongoose.model('CustomerProduct', customer_product_schema);



