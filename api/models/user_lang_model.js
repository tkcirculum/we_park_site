'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var user_lang_object = { 
  
  user_number: { type: Number, required: false },
  lang: { type: String, required: false }

}; 

var user_lang_schema = new Schema( 
  user_lang_object,  
  { collection : 'user_lang'});  
 
module.exports = mongoose.model('UserLang', user_lang_schema);
