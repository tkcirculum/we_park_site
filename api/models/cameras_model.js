'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;  

var we_cameras_schema = new Schema({  
  name: { type: String, required: false },
  img_w: { type: Number, required: false },
  img_h: { type: Number, required: false },
  crop_w: { type: Number, required: false },
  crop_h: { type: Number, required: false },
  distort: { type: String, required: false }
  
  }, { collection : 'cameras' }); 
 
 
module.exports = mongoose.model('Camera', we_cameras_schema);
