'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;  

var userCounterSchema = new Schema({  
  count: Number,
}, { collection : 'users_counter' }); 
 
 
module.exports = mongoose.model('UserCounter', userCounterSchema);
