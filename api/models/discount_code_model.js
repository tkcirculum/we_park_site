'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var discount_code_object = { 
 
  /* postojeci fieldovi  */    
  
  user_number: { type: Number, required: false },
  dis_amount: { type: Number, required: false },
  dis_code: { type: String, required: false },
  
  perc: { type: Number, required: false },
  
  desc : { type: String, required: false },
  buy_time: { type: Number, required: false }
}; 

var discount_code_schema = new Schema( 
  discount_code_object,  
  { collection : 'discount_codes'});  
 
module.exports = mongoose.model('Discount', discount_code_schema);



