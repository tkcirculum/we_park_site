'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 


var User_Object = { 
  
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  last_login: { type: Number, required: false },
  
  
  user_address: { type: String, required: false },
  user_tel: { type: String, required: false },
  
  business_name: { type: String, required: false },
  business_address: { type: String, required: false },
  business_email: { type: String, required: false },
  business_tel: { type: String, required: false },
  
  business_contact_name: { type: String, required: false },
  
  oib: { type: String, required: false },
  iban: { type: String, required: false },
  
  pdv: { type: Boolean, required: false },
  obrtnik: { type: Boolean, required: false },
  
  gdpr: { type: Boolean, required: false },
  
  /* START ---------- OVO SU SVE TIME STAMPOVI */
  gdpr_saved: { type: Number, required: false },
  gdpr_accassed: { type: Number, required: false },
  user_deleted: { type: Number, required: false },
  /* END ---------- OVO SU SVE TIME STAMPOMVI */
  
  
  car_plates: { type: [Schema.Types.Mixed], required: false },
  
  customer_tags: { type: [Schema.Types.Mixed], required: false },
  
  balance: { type: Number, required: false },
  
  customer_acquisition: { type: Schema.Types.Mixed, required: false },
  
  customer_vip: { type: [Schema.Types.Mixed], required: false },
  
  /* park_places_owner: [{ type: Schema.Types.ObjectId, ref: 'ParkPlace' }], */
  
  park_places_owner: { type: [Schema.Types.Mixed], required: false },
  
  name: { type: String, required: true },
  user_number: { type: Number, required: false },
  
  
  full_name: { type: String, required: false },
  
  hash: { type: String, required: true },
  email: { type: String, required: true },
  email_confirmed: { type: Boolean, required: false },
  welcome_email_id: { type: String, required: true },
  roles: { type: Schema.Types.Mixed, required: false },
  
  new_pass_request: { type: String, required: false },
  new_pass_email_id: { type: String, required: false },
  new_pass_email_confirmed: { type: Boolean, required: false },
  
  r1: { type: Boolean, required: false },
  
  visited_pp: { type: [Schema.Types.Mixed], required: false },
  
}; 

var UserSchema = new Schema( 
  User_Object,  
  { collection : 'users'});  
 
module.exports = mongoose.model('User', UserSchema);
