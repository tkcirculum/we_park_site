'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var solar_data_object = { 
  frequency: { type: String, required: false },
  channels: { type: Schema.Types.Mixed, required: false },
  time: { type: Number, required: false },
  rampa_name: { type: String, required: false },
  dest: { type: Number, required: false },
  date: { type: Number, required: false }
}; 

var solar_data_schema = new Schema( 
  solar_data_object,  
  { collection : 'solar_data'});  
 
module.exports = mongoose.model('Solar', solar_data_schema);

