'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var stats_object = { 
  park_places_count: { type: Number, required: false },
}; 

var stats_schema = new Schema( 
  stats_object,  
  { collection : 'stats'});  
 
module.exports = mongoose.model('Stats', stats_schema);
