'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var park_place_object = { 
  
  
  
  /* ovo su postojeci propsi */
  pp_sifra: { type: String, required: false },
  pp_address: { type: String, required: false },
 
  contact: { type: String, required: false },
  indoors: { type: Boolean, required: false },
  pp_latitude: { type: Number, required: false },
  pp_longitude: { type: Number, required: false },
  pp_max_height: { type: Number, required: false },
  pp_name: { type: String, required: false },
  video: { type: Boolean, required: false },
  enter_device_id: { type: String, required: false },
  exit_device_id: { type: String, required: false },
  has_exit_device: { type: Boolean, required: false },
  
  pp_count_out_num: { type: Number, required: false },
  
    
  /* ovo su novi propsi */  
  
  fixed_open: { type: Boolean, required: false },
  fixed_close: { type: Boolean, required: false },
  
  pp_tel: { type: String, required: false },
  pp_email: { type: String, required: false },
  
  
  pp_count_in_num: { type: Number, required: false },
  pp_available_out_num: { type: Number, required: false },
  pp_available_in_num: { type: Number, required: false },
  pp_owners: { type: [Schema.Types.Mixed], required: false },
  
  pp_prices: { type: [Schema.Types.Mixed], required: false },
  
  pp_current_price: { type: Number, required: false },
  
  pp_images: { type: [Schema.Types.Mixed], required: false },
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  
  special_places: { type: Schema.Types.Mixed, required: false },
  
  contract_exp_date: { type: Number, required: false },
  
  published: { type: Boolean, required: false },
  
  ramps: { type: [Schema.Types.Mixed], required: false },
  barriers: { type: [Schema.Types.Mixed], required: false },
  main_ramp: { type: Boolean, required: false },
  
  broken: { type: Boolean, required: false },
  maint: { type: Boolean, required: false }
  
}; 

var park_place_schema = new Schema( 
  park_place_object,  
  { collection : 'park_places'});  
 
module.exports = mongoose.model('ParkPlace', park_place_schema);
