'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;  

var racun_counter_schema = new Schema({  
  count: Number,
}, { collection : 'racun_num' }); 
 
 
module.exports = mongoose.model('RacunCounter', racun_counter_schema);
