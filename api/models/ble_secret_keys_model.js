'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;  

var ble_secret_keys_schema = new Schema({  
  secret_from_mac: { type: Schema.Types.Mixed, required: false },
}, { collection : 'ble_secret_keys' }); 
 
 
module.exports = mongoose.model('BleSecretKey', ble_secret_keys_schema);
