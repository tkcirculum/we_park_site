'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var pay_card_obj = { 
  
  user_number: { type: Number, required: false },
  card: { type: Schema.Types.Mixed, required: false },
  token: { type: Schema.Types.Mixed, required: false }
  
}; 

var pay_card_schema = new Schema( 
  pay_card_obj,  
  { collection : 'cards'});  
 
module.exports = mongoose.model('Card', pay_card_schema);
