'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var action_object = { 
  
  status: { type: String, required: false },
  /* postojeci fieldovi  */  
  sifra : { type: String, required: false },
  from_time: { type: Number, required: false },
  to_time: { type: Number, required: false },
  paid: { type: Number, required: false },  
  pp_sifra : { type: String, required: false },
  user_number: { type: Number, required: false },

  /* user_email: { type: Schema.Types.ObjectId, ref: 'User' }, */

  was_reserved_id: { type: String, required: false },
  mobile_from_time: { type: Number, required: false },



  /* moji fieldovi  */  
  user_set_book_duration:  { type: Number, required: false },

  book_duration: { type: Number, required: false },

  reservation_from_time: { type: Number, required: false },
  reservation_to_time: { type: Number, required: false },
  reservation_expired: { type: Boolean, required: false },
  reservation_canceled: { type: Boolean, required: false },
  reservation_paid: { type: Number, required: false }, 

  current_price: { type: Number, required: false }, 

  pack_type: { type: String, required: false },
  duration: { type: Number, required: false },
  revenue: { type: Number, required: false },
  owner_revenue: { type: Number, required: false },
  currency: { type: String, required: false },

  indoor: { type: Boolean, required: false },

  current_tag: { type: String, required: false },

  user_status: { type: String, required: false },

  reg: { type: String, required: false },
  
  portir: { type: Boolean, required: false }
  
}; 



var action_schema = new Schema( 
  action_object,  
  { collection : 'park_actions', toJSON: { virtuals: true } });  


action_schema.virtual('_user', {
  ref: 'User', // The model to use
  localField: 'user_number', // Find people where `localField`
  foreignField: 'user_number', // is equal to `foreignField`
  // If `justOne` is true, 'members' will be a single doc as opposed to
  // an array. `justOne` is false by default.
  justOne: true,
  options: { sort: { email: -1 }, limit: 5 }
});

module.exports = mongoose.model('ParkAction', action_schema);