'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var solar_data_days_object = { 
  channels: { type: Schema.Types.Mixed, required: false },
  time: { type: Number, required: false },
  rampa_name: { type: String, required: false }
}; 

var solar_data_days_schema = new Schema( 
  solar_data_days_object,  
  { collection : 'solar_data_days'});  
 
module.exports = mongoose.model('SolarDay', solar_data_days_schema);

