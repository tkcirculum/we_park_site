var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ParkPlace = mongoose.model('ParkPlace');

var CustomerProduct = mongoose.model('CustomerProduct');

var User = mongoose.model('User');

var Card = mongoose.model('Card');

var Discount = mongoose.model('Discount');


var nodemailer = require('nodemailer');
var path = require("path");


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


var park_actions_controller = require('./park_actions_controller.js');

var find_controller = require('./find_controller.js');


// ADDITIONAL OPTIONS FOR NATIVE JAVASCRIPT SORT ------- from http://jsfiddle.net/gfullam/sq9U7/  
window.by = function(path, reverse, primer, then) {
  var get = function (obj, path) {
      if (path) {
        path = path.split('.');
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        };
        return obj[path[len]];
      }
      return obj;
    },
    prime = function (obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function (a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B) ? -1 :
      (A > B) ? 1 :
      (typeof then === 'function') ? then(a, b) : 0
    ) * [1, -1][+!!reverse];
  };
};

function cit_rand_min_max(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
};

function convert_mongoose_object_to_js_object(mongoose_object) {
  
  var js_object = mongoose_object.toJSON();
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};



function send_new_pp_alert_by_mail(park_place, user) {
  
  var transporter = nodemailer.createTransport(global.we_mail_setup);

  var html = racun_template.html_racuna(customer_product, user);

  // setup email data
  var mailOptions = {
      from: `"WePark App" <${global.we_email_address}>`, // sender address
      to: 'karlo.starcevic@wepark.eu', // list of receivers
      subject: 'Novi unos parkirališta', // Subject line
      html:  html // html body
  };

    // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {

    if (error) {
     console.error({ 
        success: false, 
        msg: "User nije spremljen!!! Problem u slanju maila  za konfirmaciju !!!!",
        error: error
      });

    } else {

      console.log({ 
        success: true, 
        msg: "Mail je poslan i sve je ok",
      });

    };


  }); // end of send welcome email

  
};


function save_pp_owner( req, res, i, all_owners_clean, park_place ) {
  
  User.findOneAndUpdate(
  { _id: all_owners_clean[i]._id },
  { $set: {"roles.pp_owner": all_owners_clean[i].roles.pp_owner }},
  {new: true}, 
  function(err, user) {

    // akda je i varijabla u loop jednaka length arraya-1
    // onda je to zadnji user koje sam updateao i šaljem response da
    // je sve savar parkinga gotov
    if ( i == all_owners_clean.length - 1 ) {
      
      
      
      res.json({ success: true, msg: "Parkiralište spremljeno", pp_id: park_place._id});
      
      
      
    };

  }); // kraj update user
  
  
  
}; // kraj save pp owner !!!



exports.save_park_place = function(req, res) {

  ParkPlace.estimatedDocumentCount({}, function (err, count) {
    
    req.body.pp_sifra = String(count + 10);
    
    
    // ako kojim slučajem netko pokuša prevariti sustav tako da inicira sve button umjesto edit butona
    // provjerim jel ima _id ----> ako ima onda je to već postojeći parking !!!!!
    
    if ( typeof req.body._id !== 'undefined' && req.body._id !== null  ) {
      
      res.send({ success: false, msg: 'Nije moguće ponovo spremiti isto parkiralište :)' });
      return;
    
      
    } else {
    
      // ako nema _id to znači da je novi parking !!!!!

      var new_pp = new ParkPlace(req.body);
      new_pp.save(function(err, park_place) {
        
        
        if (err) {
          res.send({ success: false, msg: "Parkiralište nije spremljeno!", err: err});
          return;
        };

        var clean_park_place = null;
        var pp_sifra_od_saved_pp = null;
        var pp_owners_ids = [];


        if (park_place) {
          clean_park_place = convert_mongoose_object_to_js_object(park_place);
          pp_sifra_od_saved_pp = clean_park_place.pp_sifra;
        };


        if ( clean_park_place && clean_park_place.pp_owners ) {
          $.each( clean_park_place.pp_owners, function(owner_index, owner) {
            pp_owners_ids.push(owner._id);
          });
        };


        // pronadji sve usere od idjeva koje sam izolirao iz pp_owners arraya
        User.find( { _id: { $in: pp_owners_ids } }, function(err, all_owners) {

          if (err) {
            res.send({ success: false, msg: 'Došlo je dogreške prilikom pronalaska usera!' });
            return;
          };

          var all_owners_clean = [];
          if ( all_owners.length > 0 ) {

            // prvo pokupi sve ownere  i napravi array clean usera od njih
            $.each(all_owners, function( own_ind, owner ) { 
              var clean_owner = convert_mongoose_object_to_js_object(owner);
              all_owners_clean.push(clean_owner);
            });

            // zatim dodaj svakom owneru šifru od ovog parkirališta u propertiju roles.pp_owner
            $.each(all_owners_clean, function( c_ind, c_owner ) { 

              var pp_own_array = c_owner.roles.pp_owner;

              // ako do sada user nije imao niti jedan pp u svom vlasništvu
              if ( !pp_own_array ) pp_own_array = [];

              // ako ima već neke parkinge provjeri jel već ima ovaj parking
              var vec_je_upisan_pp = false;
              $.each( pp_own_array, function(pp_sifra_index, pp_owner_sifra ) {
                if ( pp_owner_sifra == pp_sifra_od_saved_pp ) vec_je_upisan_pp = true;
              });

              // ako već nije upisan ovaj pp onda ga pushaj u array
              if ( vec_je_upisan_pp == false ) {
                pp_own_array.push(pp_sifra_od_saved_pp);
              };

              all_owners_clean[c_ind].roles.pp_owner = pp_own_array;

            }); // kraj loopa po scim clean ownerima


            var i;
            // vrti po svim ownerima i spremi novi array sa pp siframa od ovog parkinga !!!!!
            for (i=0; i < all_owners_clean.length; i++ ) {

              save_pp_owner( req, res, i, all_owners_clean, park_place );

            };

          };

        }); 

      }); // end of spremi parkiralište 

    };
  });

}; // KRAJ SPREMI PARK PLACE 


exports.edit_park_place = function(req, res) {
  
  var token = req.headers['x-auth-token'] || null;
  
  if ( token ) {
      
    find_controller.check_is_super(token).then(
    function( result ) {
      
      var pp_sifra_editiranog_parka = String( req.body.pp_sifra );
      
      if ( result.success == true ) {
        
        if ( 
          ( 
            result.user.roles &&
            result.user.roles.pp_owner &&
            $.isArray(result.user.roles.pp_owner) &&
            $.inArray(pp_sifra_editiranog_parka, result.user.roles.pp_owner ) > -1 
          )
          
          ||
          
          result.super_admin == true  
           
        ) {

          ParkPlace.findOneAndUpdate({ _id: req.params.pp_id }, req.body, { new: true }, function(err, park_place) {
            if (err) res.send({ success: false, msg: "Parkiralište nije ažurirano!", err: err });
            res.json({ success: true, msg: "Parkiralište ažurirano", pp_id: park_place._id});
          });
          return;
          
        } else {
          // ako nije super admin i nije vlasnik ovog parkinga
          res.json({ success: false, msg: 'Nemate ovlasti za ovu radnju!' });  
          
        };
        
      } else {
        
        res.json({ success: false, msg: 'Došlo je do greške prilikom spremanja parkirališta!' }); 
        return;
        
      };

    })
    .catch(function(err) {
      
      // ako se dogodi bilo koja greška kod verifikacije tokena ili pronalaska usera
      res.json({ success: false, msg: 'Došlo je do greške prilikom spremanja parkirališta!' }); 
      
    });

  } 
  else {
    // vrati da nema ovlasti ako je request bez tokena !!!!
    res.json({ success: false, msg: 'Nemate ovlasti za ovu radnju!' });    
    
  };
  
};


exports.get_all_park_places = function(req, res) {
  
  // TODO ------------------------- OBAVEZNO OVO VRATI NAZAD DA SE NE PRIKAZUJE 
  // var new_query = { published: true };
  var new_query = {};
  
  query_park_places(new_query).then(
    function(result) { res.json(result); },
    function(error) { res.json(error); }
  );
};



function query_park_places(query) {
  
  return new Promise( function( resolve, reject ) {
    
    ParkPlace.find(query, function(err, park_places) {
      
      if (err) {
        
        reject({ success: false, msg: "NEMA PARKIRALIŠTA ZBOG MONGODB ERRORA !!!!", err: err});
        return;
        
      };

      var clean_park_places = [];

      if ( park_places.length > 0 ) {

        // prvo pretvori sve mongoose objekte u obične js objekte
        park_places.forEach(function(pp_obj, pp_index) {
          var clean_pp_obj = convert_mongoose_object_to_js_object(pp_obj);
          // i ubaci ih u clean array
          clean_park_places.push(clean_pp_obj);
        });

        // zatim za svaki objekt unutar ovog novog clean array-a 
        // izračunaj koja je trenutna dinamička cijena
        clean_park_places.forEach(function(pp_obj, pp_index) {
          clean_park_places[pp_index].pp_current_price = park_actions_controller.find_current_price(pp_obj);
        });
        
        var not_contract_exp = [];
        var time_now = Date.now();
        clean_park_places.forEach(function(pp_obj, pp_index) {
          if ( pp_obj.contract_exp_date ) {
            // ako postoji contract date onda provjeri jel istekao
            if ( time_now < pp_obj.contract_exp_date ) not_contract_exp.push(pp_obj);
          } else {
            // ako ne postoji contract exp date onda tretiraj kao da nije istekao !!!!!
            not_contract_exp.push(pp_obj);                    
          };
        });
        
        // samo ako je query == {} to znači da vraća sve parkinge !!!
        if ( Object.keys(query).length == 0 ) global.park_places_list = not_contract_exp;
        
        resolve({ success: true, msg: "LISTA PARKIRALIŠTA", park_places: not_contract_exp });  
      } else {
        
        reject({ success: false, msg: "NIJE NAŠAO NITI JEDNO PARKIRALIŠTE !!!!", park_places: not_contract_exp });  
        
      };

     

    });
    
  }); // end of promise
  
};
exports.query_park_places = query_park_places;


// pokreni odmah ovaj query tako da u memoriji imam global park places list !!!!!
query_park_places({});


exports.get_specific_pp = function(req, res) {
  
  ParkPlace.find({ _id: req.params.pp_id }, function(err, specific_pp) {
    if (err) {
      res.send({ success: false, msg: "GREŠKA U DOBIVANJU SPECIFIC PARKIRALIŠTA", err: err});
      return;
    };
    res.json({ success: true, msg: "SPECIFIC PARKIRALIŠTE", specific_pp: specific_pp });
  });
  
};


