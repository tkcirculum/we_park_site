var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var nodemailer = require('nodemailer');

var fs = require('fs')

var path = require("path");

var xl = require('excel4node');


var ParkPlace = mongoose.model('ParkPlace');
var ParkAction = mongoose.model('ParkAction');

var User = mongoose.model('User');

var jwt = require('jsonwebtoken');


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


var park_actions_controller = require('./park_actions_controller.js');


global.search_actions_result_array_cache = [];




function convert_mongoose_object_to_js_object(mongoose_object) {
  
  var js_object = mongoose_object.toJSON();
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};




function run_filter( docs, filter_funkcije) {
  
  var filtrirani_docs = docs;
  
  if ( filter_funkcije !== null && $.isArray(filter_funkcije) ) {
    
    if ( filter_funkcije.length > 0 ) {
      filter_funkcije.forEach(function(filter_func, filter_index) {
        try {
          var filter = new Function('docs', filter_func );
          filtrirani_docs = filter(filtrirani_docs);
        }
        catch(err) {
          res.send({error: err, msg: 'GREŠKA PRILIKOM IZVRŠENJA FILTER FUNKCIJE'});
        }
      }); // kraj for each
      
    }; // kraj ako postoji barem jedna func u filter arrayu

  }; // kraj ako filter nije null i ako je array
  
  return filtrirani_docs;

};


function check_is_super(token) {

  return new Promise( function(resolve, reject) {

    var super_admin = false;
    
    
    if ( token == 'pp_request' ) {
      resolve({ success: true, user: 'pp_request' , super_admin: true });
      return;
    };
    
    jwt.verify(token, 'super_tajna', function(err, decoded_user) { 

    if (err) {
      reject({ success: false, msg: 'Greška kod verifikacije usera!', error: err });    
      return;
    };

    if (
      decoded_user._id == '5d9f47f16ce0b535e5441a3a' || /* Circulum user */
      decoded_user._id == '5e32374d220ba4253d747c0e' || /* karlo user */
      decoded_user._id == '5e4d44c135ed162e9dcc06d2' /* Krešimir Marušić user */
      
      ) {
      super_admin = true;
      };

      User.findOne(
      { _id: decoded_user._id },
      function(err, user) {

        if (err) {

          reject({ success: false, msg: 'Greška kod pronalaska usera!', error: err });
          return
          
        };

        if (user) {
          
          var clean_user = convert_mongoose_object_to_js_object(user);
          resolve({ success: true, user: clean_user, super_admin: super_admin });

        } else {
          // ako nije našao usera !!!
          resolve({ success: false, msg: 'Nije pronađen user!' });
        };


      }); // kraj find user        

    }); // kraj verifikacije tokena  

  }); // kraj return promisa
  
}; // kraj check is super func
exports.check_is_super = check_is_super;

global.check_is_super = check_is_super;

    
function crop_actions_to_interval(akcije, start, end) {

  $.each(akcije, function(ind, action ) {

    var has_book = false;
    var has_park = false;

    // ponekad se dogodi zbog server delay-a da početno vrijeme bude veće od završnog vremena
    if ( action.reservation_to_time !== null && action.reservation_from_time > action.reservation_to_time ) {
      action.reservation_to_time = null;
      action.reservation_from_time = null;
      action.book_duration = null;
    };
    
    // ponekad se dogodi zbog server delay-a da početno vrijeme bude veće od završnog vremena
    if ( action.to_time !== null && action.from_time > action.to_time ) {
      action.to_time = null;
      action.from_time = null;
      action.duration = null;
    };
    
    akcije[ind].reservation_from_time_c = action.reservation_from_time;
    akcije[ind].reservation_to_time_c = action.reservation_to_time;

    akcije[ind].from_time_c = action.from_time;
    akcije[ind].to_time_c = action.to_time;

    
    akcije[ind].book_duration_c = action.book_duration;
    akcije[ind].duration_c = action.duration;


    if ( action.reservation_from_time ) {
      has_book = true;
      action.has_book = true;
    };
    
    if ( action.from_time ) {
      has_park = true;
      action.has_park = true;
    };

    if ( has_book ) {

      // postavi na start ako je pocetak prije intervala
      if ( action.reservation_from_time < start ) {
        akcije[ind].reservation_from_time_c = start;
      };

      if ( action.hasOwnProperty('book_duration') == false || action.book_duration == null ) {
        // ako nema book duration to znači da trebam prikazati booking do ovog trenutnka
        var time_now = Date.now();
        var finished_time = (end > time_now) ? time_now : end;
        akcije[ind].book_duration_c = finished_time - action.reservation_from_time_c;
        akcije[ind].reservation_to_time_c = finished_time;
      } else if ( action.book_duration > 0 ) {
        akcije[ind].book_finished = true;
      };


      // start unutra end unutra
      if ( action.reservation_from_time >= start && action.reservation_to_time !== null && action.reservation_to_time <= end ) {
        akcije[ind].book_duration_c = (action.book_duration || action.book_duration_c);
        akcije[ind].reservation_from_time_c = action.reservation_from_time;
        akcije[ind].reservation_to_time_c = action.reservation_to_time;
      };

      // start prije end unutra
      if ( action.reservation_from_time <= start && action.reservation_to_time !== null && action.reservation_to_time <= end ) {
        akcije[ind].book_duration_c = action.reservation_to_time - start;
        akcije[ind].reservation_from_time_c = start;
        akcije[ind].reservation_to_time_c = action.reservation_to_time;
      };

      // start unutra end poslje
      if ( action.reservation_from_time >= start && action.reservation_to_time !== null && action.reservation_to_time >= end ) {
        akcije[ind].book_duration_c = end - action.reservation_from_time;
        akcije[ind].reservation_from_time_c = action.reservation_from_time;
        akcije[ind].reservation_to_time_c = end;
      };

      // start prije end poslje
      if ( action.reservation_from_time <= start && action.reservation_to_time !== null && action.reservation_to_time >= end ) {
        akcije[ind].book_duration_c = end - start;
        akcije[ind].reservation_from_time_c = start;
        akcije[ind].reservation_to_time_c = end;
      };

      
      if ( akcije[ind].book_duration_c ) {
        akcije[ind].reservation_paid =  ( akcije[ind].book_duration_c/( 1000*60*60 ) ) * akcije[ind].current_price;
      } else {
        akcije[ind].reservation_paid = 0;
      };

    }; // kraj has book

    
    
    
    if ( has_park ) {

      // postavi na start ako je pocetak prije intervala
      if ( action.from_time < start  ) {
        akcije[ind].from_time_c = start;
      };


      if ( action.hasOwnProperty('duration') == false || action.duration == null ) {
        // ako nema book duration to znači da trebam prikazati booking do ovog trenutnka
        var time_now = Date.now();
        var finished_time = (end > time_now) ? time_now : end;
        
        akcije[ind].duration_c = finished_time - action.from_time_c;
        akcije[ind].to_time_c = finished_time;
        
      } else if ( action.duration > 0 ) {
        akcije[ind].park_finished = true;
      };


      // start unutra end unutra
      if ( action.from_time >= start && action.to_time !== null && action.to_time <= end ) {
        akcije[ind].duration_c = (action.duration || action.duration_c);
        akcije[ind].from_time_c = action.from_time;
        akcije[ind].to_time_c = action.to_time;
      };

      // start prije end unutra
      if ( action.from_time <= start && action.to_time !== null && action.to_time <= end ) {
        akcije[ind].duration_c = action.to_time - start;
        akcije[ind].from_time_c = start;
        akcije[ind].to_time_c = action.to_time;
      };

      // start unutra end poslje
      if ( action.from_time >= start && action.to_time !== null && action.to_time >= end ) {
        akcije[ind].duration_c = end - action.from_time;
        akcije[ind].from_time_c = action.from_time;
        akcije[ind].to_time_c = end;
      };

      // start prije end poslje
      if ( action.from_time <= start && action.to_time !== null && action.to_time >= end ) {
        akcije[ind].duration_c = end - start;
        akcije[ind].from_time_c = start;
        akcije[ind].to_time_c = end;
      };

      if ( akcije[ind].duration_c ) {
        akcije[ind].paid = ( akcije[ind].duration_c/( 1000*60*60 ) ) * akcije[ind].current_price;
      } else {
        akcije[ind].paid = 0;
      };

    }; // kraj has park

    
    
  });

  return akcije;

};


function run_find( req, res, super_admin, arg_user ) {
  
  var DB_model = null;  
  
  if ( req.originalUrl == '/search_park_places') DB_model = ParkPlace;
  
  if ( req.originalUrl == '/search_pp_owners') DB_model = User;
  
  
  if ( req.originalUrl == '/stats_search_customers') DB_model = User;
  if ( req.originalUrl == '/search_customers') DB_model = User;
  
  if ( req.originalUrl == '/search_actions' ) DB_model = ParkAction;
  
  if ( req.originalUrl == '/search_regs' ) DB_model = User;
  
  
  if ( DB_model == null ) {
    res.send({ success: false, msg: 'No such end point for searching.' });
    return;
  };
  
  
  /*
  DB_model.estimatedDocumentCount({}, function (err, count) {
    console.log( 'OVO JE BROJ DOKUMENATA U ' + req.originalUrl + ' kolekciji  : '  +  count );
  
  });
  */
  
  
  var sortiraj_po = req.body.sort || null;
  
  
  
  var list_limit = req.body.limit;

  // if ( req.originalUrl == '/search_actions' ) list_limit = 20000;

  // populate objects if any of these queries 
  // if ( req.originalUrl == '/search_pp_owners' ) model_populate = model_find.populate('park_places_owner');
  // if ( req.originalUrl == '/search_park_places' ) model_populate = model_find.populate('pp_owner');


  var find_query = {};
  
  var trazi_string = req.body.trazi_string || null;
  
  
  var trazi_polja = [];
  var trazi_array = [];
  var filter_funkcije = req.body.filer || null;
  var filtrirano = null;
  
  var start = null;
  var end = null;
  
  var kreirao_query = false;
  
  // ako je query prazan objekt i ako je search string null ili prazan  ----> onda traži SVE !!!!
  if ( req.body.query && Object.keys(req.body.query).length == 0 && !trazi_string ) {
    find_query = {};
    kreirao_query = true;
  
  }; 
  
  
  
  if ( req.originalUrl == '/search_regs' ) {
    req.body.query = {
      $or: [
        { "car_plates.reg": trazi_string.replace(/(?!ć|Ć|č|Č|š|Š|đ|Đ|ž|Ž)\W/g, '').replace(/\_/g, '').toUpperCase() },
        { email: { "$regex": trazi_string, "$options": "i" } }
      ]
    };
  };
  
  
  // ali ako postoji query onda je to prioritet tj traži sve po tom queriju
  if ( kreirao_query == false && req.body.query && Object.keys(req.body.query).length > 0 ) {
    
    find_query = req.body.query;
    
    if ( req.originalUrl == '/search_actions' ) {

      start = find_query.start;
      end = find_query.end;

      delete find_query.start;
      delete find_query.end;
      
    };
    
    kreirao_query = true;
    
  };
  
  // ako je query prazan, 
  // ali search string je neki tekst 
  // -----> izaberi sve string propertije i traži string u njima s regexom
  if ( 
    kreirao_query == false                  &&
    req.body.query                          &&
    Object.keys(req.body.query).length == 0 &&
    trazi_string !== null
  ) {
    
    // ONDA TRAŽI PO SVIM STRING PROPERTIJIMA !!!!!!
    var all_model_keys = Object.keys( DB_model.schema.paths );
    
    // izvuci iz modela samo string propertije 
    all_model_keys.forEach(function(key, key_index) {
      if ( DB_model.schema.paths[ key ].instance == 'String' ) trazi_polja.push(key);
    });
    
    trazi_polja.forEach(function(polje, index) {
      var search_objekt = {};
      search_objekt[polje] = { "$regex": trazi_string, "$options": "i" };
      trazi_array.push(search_objekt);
    });
    
    // if ( req.originalUrl == '/search_pp_owners' ) trazi_array.push({"roles.owner": true});

    find_query = { $or: trazi_array };
    
    kreirao_query = true;
    
  };
    
  // ako je postoji array polja find_in
  // i postoji search string 
  // onda traži regex samo u tim properijima
  if ( 
    kreirao_query == false      &&
    req.body.find_in            &&
    req.body.find_in.length > 0 &&
    trazi_string !== null 
  ) {

    trazi_array = [];
    
    req.body.find_in.forEach(function(polje, index) {
      
      var search_objekt = {};
      
      search_objekt[polje] = { "$regex": trazi_string, "$options": "i" };
      trazi_array.push(search_objekt);
      
    });
    
    // if ( req.originalUrl == '/search_pp_owners' ) trazi_array.push({"roles.owner": true});

    find_query = { $or: trazi_array };
    kreirao_query = true;
    
  };
  
  
  // ako nije ništa od gore navedenog onda jednostavno traži sve !!!!!!
  if ( kreirao_query == false ) {
    find_query = {};
  };
  

  // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
  // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
  // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
  
  if ( super_admin == false && req.originalUrl == '/search_actions' )  {
    
    var pp_owner_book_query = true;
    var pp_owner_park_query = true;
    var jel_trazi_svoje_parkinge = true;  
    
    // ako je ovaj user owner od bilo kojeg  parkirališta !!!
    if ( 
      arg_user.roles &&
      arg_user.roles.pp_owner &&
      $.isArray(arg_user.roles.pp_owner) &&
      arg_user.roles.pp_owner.length > 0
    ) {
      
        // ako query ima or u sebi onda je i park i book
        if ( find_query['$or'] ) {
          
          // ako postoji pp_sifra u prvom queriju
          if ( find_query['$or'][0].pp_sifra ) {
            // ali nije niti jedna od njegovih
            if ( $.inArray( find_query['$or'][0].pp_sifra, arg_user.roles.pp_owner ) == -1 ) pp_owner_book_query = false; 
          } else {
            // ako nema pp sifra u queriju
            pp_owner_book_query = null;
          };
          
          // ako postoji pp_sifra u drugom queriju
          if ( find_query['$or'][1].pp_sifra ) {
            // ali nije niti jedna od njegovih
            if ( $.inArray( find_query['$or'][1].pp_sifra, arg_user.roles.pp_owner ) == -1 ) pp_owner_park_query = false; 
          } else {
            // ako nema pp sifra u queriju
            pp_owner_park_query = null;
          };

          // ako bilo koji od querija ima pp_sifra koja nije u njegovom vlasništvu !!!
          if ( pp_owner_book_query == false || pp_owner_park_query == false  ) jel_trazi_svoje_parkinge = false;
          
          if ( pp_owner_book_query == null || pp_owner_park_query == null  ) jel_trazi_svoje_parkinge = null;
          

        } else {
          
          // ako NEMA find_query['$or'] u queriju ali postoji pp_sifra 
          if ( 
            find_query.pp_sifra &&
            $.inArray( find_query.pp_sifra, arg_user.roles.pp_owner ) == -1 
          ) {
            jel_trazi_svoje_parkinge = false;
          };
           
          // ako nema $or u queriju i ne postoji pp sifra !!!
          if ( !find_query.pp_sifra ) jel_trazi_svoje_parkinge = null;
          
        };


        if ( jel_trazi_svoje_parkinge == false ) {
          res.json({ success: false, msg: 'popup_nemate_ovlasti' });
          return;
        };

      // ako je ova varijabla null to znači ne traži niti jedan specifični parking
      // već je to polje ostavio prazno !!!
        if ( jel_trazi_svoje_parkinge == null ) {

          var svi_userovi_pp_array = [];

          // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
          // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
          // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
          $.each( arg_user.roles.pp_owner,  function(pp_index, pp_owner_sifra) {
            var pp_query = { pp_sifra: pp_owner_sifra };
            svi_userovi_pp_array.push(pp_query);
          });

          // dodaj queriju and i stavi u drugi item sve pp_sifre od ownera
          find_query = { 
            $and: [ 
              find_query,
              { $or: svi_userovi_pp_array }
            ]
          };

        }; // kraj ako u queriju nema pp sifri ali je user vlasnik jednog ili više parkinga !!!!

      // kraj ifa ako je owner tj  ima nešto u pp_powner arraju
      } else {
        
        // ako nije owner ničega
        res.json({ success: false, msg: 'popup_nemate_ovlasti' });
        return;
        
      };
    
    
  }; // kraj ako nije admin i ako je search actions !!!
  
  
  
  var model_find =  DB_model.find(find_query);
  // model populate je isti kao i model find
  var model_populate = model_find;
  // ali ako se radi o actions onda populate nije isti već dodajem _user u svaki doc
  if ( req.originalUrl == '/search_actions' ) model_populate = model_find.populate('_user');
  
  var items_skip = req.body.page ? ((req.body.page - 1) * list_limit) : 0;
  
  // TODO OBRISATI KASNIJE
  /*
  
  if ( req.originalUrl == '/search_actions' ) {
    list_limit = 100000;
    items_skip = 0;
  };
  
  */
  
  
  model_populate
    .limit(list_limit)
    .skip( items_skip )
    .sort( (sortiraj_po || { _id: -1 }) )
    .select( req.body.return )
    .exec(function(err, docs) {

    
    
    if (err) {
      res.send({ success: false, msg: 'Došlo je do greške kod pretrage akcija u bazi !!!', error: err });
      return;
    };
    
    var clean_docs = [];
    
    if ( docs.length > 0 ) {
      $.each(docs, function(doc_index, doc ) {
        var clean_doc = convert_mongoose_object_to_js_object(doc);
        
        
        
        // ------------------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        // --------START------ OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        // ------------------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        
        if ( req.originalUrl == '/search_actions' ) {
          
          if ( clean_doc._user ) {
            // ako postoji _user koji nastaje od populate action_schema.virtual('_user' ....
            // onda upiši email u varijablu user email
            clean_doc.user_email = clean_doc._user ? clean_doc._user.email : clean_doc.user_number;
            delete clean_doc._user;
          } else {
            // ako nema user email-a onda samo uzmi user number
            clean_doc.user_email = String( clean_doc.user_number );
          };
        
        };
        
        
        if ( req.originalUrl == '/search_regs' ) {

          
        };
        
        // ------------------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        // --------END-------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        // ------------------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        
        
        clean_docs.push(clean_doc);
      });
      
      // ako se radi o action queriju za statistiku onda odreži sve book i park akcije da stanu u interval od - do tj  start - end
      if ( req.originalUrl == '/search_actions' ) clean_docs = crop_actions_to_interval(clean_docs, start, end);
      
    };
    
    
    
    filtrirano = run_filter( clean_docs, filter_funkcije);

    
    
    // ako je pretraga plark places injektiraj current price
    if ( req.originalUrl == '/search_park_places' ) {
      
      filtrirano.forEach(function(pp_obj, pp_index) {
        filtrirano[pp_index].pp_current_price = park_actions_controller.find_current_price(pp_obj);
      });
    };
    
    
    if ( 
      super_admin == false 
      && 
      ( req.originalUrl == '/search_customers' || req.originalUrl == '/stats_search_customers' )
      ) {
    
      // ako je ovo owner on nekog parkirališta !!!!
      if ( 
        arg_user.roles &&
        arg_user.roles.pp_owner &&
        $.isArray(arg_user.roles.pp_owner) &&
        arg_user.roles.pp_owner.length > 0
      ) {

        var samo_useri_s_njegovih_pp = [];
        
        
        filtrirano.forEach(function(user_obj, user_index) {
          
          var user_posjetio_ownerov_pp = false;
          // provjeri jel user posjedio jedan od ownerovih parkova !!!!
          $.each( user_obj.visited_pp, function(vis_index, vis_pp) {
            if ( $.inArray(vis_pp, arg_user.roles.pp_owner) > -1 ) {
              user_posjetio_ownerov_pp = true;
            };
          });
          
          // ako je onda ubaci ovog usera u novi array samo sa posjetiteljima ownerovih parkova
          if ( user_posjetio_ownerov_pp == true ) samo_useri_s_njegovih_pp.push(user_obj);
          
        });
        
        filtrirano = samo_useri_s_njegovih_pp;
      } 
      else {
        
        // ako nije owner od niti jednog parkirališta i nije super admin
        
        var samo_current_user = null;
        filtrirano.forEach(function(user_obj, user_index) {
          if ( user_obj._id == arg_user._id ) {
            samo_current_user = user_obj;
          };
        });

        if ( samo_current_user !== null ) {
          // ako je našao current usera onda ga ubaci u array
          filtrirano = [samo_current_user];
        } else {
          // ako nije našao usera onda vrati prazan array
          filtrirano = [];
        };
        
      }; // kraj else ako nije owner parkirališta
    
  };

  

    if ( req.originalUrl == '/search_actions' ) {

      
      
      var clean_agg_actions = [];
      var uniq_users = [];
      
      var count = 0;
      var distinct = 0;
      var time_book = 0;
      var time_park = 0;
      var novac_book = 0;
      var novac_park = 0;
      

      
      // keširam aggregaciu akcija !!!!!!!!!!!
      // ako je string od querija isti kao i string od prošlog querija
      // onda vrati keširani resultat !!!!!
      if ( JSON.stringify(find_query) == global.cache_find_query ) {
        
        // 
        var cache_object = JSON.parse(global.cache_agg_result);
        // moram ubaciti nove rows jer je možda user kliknuo na novu stranicu
        // ovak ostaju svi podaci od sume i agregacije ali se mijenja array u rows ( to su sve akcije na jednom page-u)
        // na taj način izbjegavam pravljenje sume i agregacije svih podataka
        // samo predajem rezultate od trenutnog page-a
        
        cache_object.rows = filtrirano;
        
        res.json( cache_object );
        return;
        
      };
      
      global.cache_find_query = JSON.stringify(find_query);
          
      // VAŽNO !!! Ponovo zovem bazu da mi vrati sve actionse po queriju
      DB_model.find( find_query )
      .populate('_user') /* pogledati u park_actions_model.js za objašnjenje kako napraviti join action i user */
      .exec(function(err, agg_actions) {
      
        if ( err ) {
          res.send({ 
            success: false,
            msg: 'Došlo je do greške kod prikupljanja sume podataka podataka za actions pretragu !!!', 
            error: err
          });
          return;
        };

        if ( agg_actions.length > 0 ) {
          
          $.each(agg_actions, function( agg_index, agg_act) {
            var cl_action = convert_mongoose_object_to_js_object(agg_act);
            
            // 
            cl_action.user_email = cl_action._user ? cl_action._user.email : cl_action.user_number;
            
            if ( $.inArray( cl_action.user_email, uniq_users ) == -1 ) {
              uniq_users.push(cl_action.user_email);
            };
            
            if ( cl_action._user ) delete cl_action._user;
            
            clean_agg_actions.push(cl_action);
          });
          
          
          clean_agg_actions = crop_actions_to_interval(clean_agg_actions, start, end);
          
          count = clean_agg_actions.length;
          distinct = uniq_users.length;
          
          var time_now = Date.now();
          
          
          // SUMA SVIH PAID BOOKING I PARKINGA 
          $.each(clean_agg_actions, function( cl_ind, cl_act) {
            
            if ( cl_act.book_duration_c ) {
              time_book += cl_act.book_duration_c;
              var book_paid = ( cl_act.book_duration_c/( 1000*60*60 ) ) * cl_act.current_price;
              novac_book += book_paid;
            };
            
            if ( cl_act.duration_c ) {
              time_park += cl_act.duration_c;
              var park_paid = ( cl_act.duration_c/( 1000*60*60 ) ) * cl_act.current_price;
              novac_park += park_paid;
            };
            
          });
          
        }; // kraj jel postoje actions

        
        // ---------- VAŽNO ----------
        // pretvaram već postojeći array filtrirano u objekt
        // a postojeći array stavljam da bude unutar tog novog objekta
        // to je samo zato što vraćam varijablu
        
        var agg_result = {
          success: true,
          rows: filtrirano,

          count: count,
          distinct: distinct,
          time_book: time_book,
          time_park: time_park,
          novac_book: novac_book,
          novac_park: novac_park,
          timestamp: Date.now(),
        };
        
        global.cache_agg_result = JSON.stringify(agg_result);
        
        global.search_actions_result_array_cache.push({actions: clean_agg_actions, timestamp: agg_result.timestamp } );
        
        // ako array od cache-a ima više od 10 itema onda izbaci prvi tj. najstariji
        if ( global.search_actions_result_array_cache.length > 10 ) {
          global.search_actions_result_array_cache.shift();
        };
        
        res.json(agg_result);

        
      }); // kraj finda svih action za kreiranje agregata tj sume itd
      
      // kraj ako je pretraga po actions
    } else { 
      
      
      res.json(filtrirano);
      
      
    };

  });

  
  
  // zaustavi danji rad ako je request za sve zapise
  // if (trazi_polja == 'all') return;
  /*
  DOBIJEM NEŠTO SLIČNO OVOME:
  -----------------------------------------------------------
  [
    { "en.name": { "$regex": trazi_string, "$options": "i" }},
    { "de.name": { "$regex": trazi_string, "$options": "i" }},
    { "hr.name": { "$regex": trazi_string, "$options": "i" }},
    { "sr.name": { "$regex": trazi_string, "$options": "i" }},
  ] 
  -----------------------------------------------------------
  */
  
  /*
  UBACITI OVO PRIJE .exec AKO ŽELIM SELEKTIRATI SAMO NEKA POLJA:
  -----------------------------------------------------------
  .select(select_objekt)

  dobijem nešto slično ovome
  .select({
    _id: 1,
    status: 1,
    gametype_id: 1,
    coverImg: 1,
    "config.status.lastChangedAt": 1,
    "hr.name": 1, 
    "en.name": 1, 
    "de.name": 1, 
    "sr.name": 1
  })
  -----------------------------------------------------------
  */  
  
};

function run_node_4_excel(timestamp, actions_array, res) {
  
  var wb = new xl.Workbook();
  
  
   // Create a reusable style
  var excel_nums = wb.createStyle({
    font: {
      size: 12,
    },
    /*numberFormat: '#,##0.00; (#,##0.00); -',*/
    numberFormat: '0.00',
    
  });
  
  
  var header_style = wb.createStyle({
    
    font: {
      size: 14,
      bold: true 
    },
    
    alignment: {
      horizontal: 'center',
    },
    
    fill: {
      type: 'pattern',
      patternType: 'solid',
      bgColor: '#c6c6c6',
      fgColor: '#c6c6c6',
    }
  
  });
  
  
  var sum_style = wb.createStyle({
    font: {
      size: 12,
      bold: true,
      color: 'red'
    },

    numberFormat: '#,##0.00',
  
  });
  
    var sum_style_no_dec = wb.createStyle({
    font: {
      size: 12,
      bold: true,
      color: 'red'
    },

    numberFormat: 'General',
  
  });
  
  
  // Add Worksheets to the workbook
  var ws = wb.addWorksheet('UKUPNO');
  // var ws2 = wb.addWorksheet('Sheet 2');
  
  
  
  function find_pp_name(action) {
   
    var pp_name = '';
    
    $.each(global.park_places_list, function(park_ind, park) {

      if ( park.pp_sifra == action.pp_sifra ) pp_name = park.pp_name;

    });
      
    return pp_name;  
      
    
  };
  
  
    
  function make_unique_array(email, unique_array) {
   
    if ( email && $.inArray( email, unique_array ) == -1  ) unique_array.push(email);
    
  };
  
  
  

    var book_sheet_objects = {};
    var park_sheet_objects = {};
    var ukupno_objects = {};

  
  $.each(global.park_places_list, function(park_ind, park) {
    
    book_sheet_objects[park.pp_name] = wb.addWorksheet(`${park.pp_name} Book`);
    park_sheet_objects[park.pp_name] = wb.addWorksheet(`${park.pp_name} Park`);
    
  });

  
  
  $.each(global.park_places_list, function(park_ind, park) {
    
    var book_sheet = book_sheet_objects[park.pp_name];
    var park_sheet = park_sheet_objects[park.pp_name];
    
    ukupno_objects[park.pp_name] = {};
    
    
    var book_headers = [
      "Email",
      "Grupa",
      "Satus korisnika",
      "Ime parkinga",
      "Cijena / h",
      "Book START",
      "Book END",
      "Book Trajanje",
      "Book Iznos",
      "POČEO PRIJE",
      "JOŠ TRAJE",
      "Odabrano trajanje"
    ];
    
    
    var park_headers = [
      "Email",
      "Grupa",
      "Satus korisnika",
      "Ime parkinga",
      "Cijena / h",
      "Park START",
      "Park END",
      "Park Trajanje",
      "Park Iznos",
      "POČEO PRIJE",
      "JOŠ TRAJE"
    ];    
    
    
    $.each(book_headers, function(header_ind, text) {
      
      book_sheet
        .cell(2, header_ind+1)
        .string(text)
        .style(header_style);

    });
    
    $.each(park_headers, function(header_ind, text) {
      
      park_sheet
        .cell(2, header_ind+1)
        .string(text)
        .style(header_style);

    });
    

    
    var book_row = 2;
    var book_duration_sum = 0;
    var book_paid_sum = 0;
    var book_unique = [];
    
    
    $.each(actions_array, function(action_ind, action) {
      
      if ( action.has_book && action.pp_sifra == global.park_places_list[park_ind].pp_sifra ) {
        
        
        make_unique_array(action.user_email, book_unique);
        
        book_row += 1;
        
        var book_start_date =  '';
        var book_start_time =  '';
        
        if ( action.reservation_from_time ) {
          var book_start = global.getMeADateAndTime( action.reservation_from_time, 'y-m-d');
          book_start_date = book_start.datum;
          book_start_time = book_start.vrijeme;
        };
        
        
        
        var book_end_date =  '';
        var book_end_time =  '';
        
        var book_paid_num = 0;
        
        if ( action.reservation_to_time_c ) {
          
          var book_end = global.getMeADateAndTime( action.reservation_to_time, 'y-m-d');
          book_end_date = book_end.datum;
          book_end_time = book_end.vrijeme;
          
          book_paid_num = 
            ( action.book_duration_c || 0 )/(1000*60*60) * action.current_price;

          
          book_duration_sum += ( action.book_duration_c || 0 )/(1000*60*60);
          book_paid_sum += book_paid_num;
          
          
          
        };
        
        
        var poceo_prije = '';
        var jos_traje = '';
        
        if ( action.reservation_from_time < action.reservation_from_time_c ) poceo_prije = 'DA';
        if ( !action.reservation_to_time ) jos_traje = 'DA';
        

        book_sheet
        .cell(book_row, 1)
        .string(action.user_email)
        .style(excel_nums);

        book_sheet
        .cell(book_row, 2)
        .string(action.current_tag || '')
        .style(excel_nums);  

        book_sheet
        .cell(book_row, 3)
        .string(action.user_status || '')
        .style(excel_nums);

        book_sheet
        .cell(book_row, 4)
        .string( find_pp_name(action) || '')

        book_sheet
        .cell(book_row, 5)
        .number(action.current_price || 0)
        .style(excel_nums);
        // .style(excel_nums);

        book_sheet
        .cell(book_row, 6)
        .string( book_start_date + ' ' + book_start_time )
        .style(excel_nums);       
        
        book_sheet
        .cell(book_row, 7)
        .string( book_end_date + ' ' + book_end_time )
        .style(excel_nums);  
        
        
        
        book_sheet
        .cell(book_row, 8)
        .number( (action.book_duration_c || 0)/(1000*60*60) )
        .style(excel_nums);
        // .style(excel_nums); 
        
        
        
        book_sheet
        .cell(book_row, 9)
        .number( book_paid_num )
        .style(excel_nums);
        // .style(excel_nums); 
        
        
        
        book_sheet
        .cell(book_row, 10)
        .string( poceo_prije)
        .style(excel_nums);
        // .style(excel_nums); 
                
        
        book_sheet
        .cell(book_row, 11)
        .string( jos_traje)
        // .style(excel_nums); 
        
        book_sheet
        .cell(book_row, 12)
        .number( action.user_set_book_duration )
        .style(excel_nums);
        // .style(excel_nums); 
        
        
        
      }; // kraj ako ima has book 

      
      
    });
    
    
    book_sheet
    .cell(1, 8)
    .number( book_duration_sum )
    .style(sum_style);
    // .style(excel_nums); 



    book_sheet
    .cell(1, 9)
    .number( book_paid_sum )
    .style(sum_style);
    // .style(excel_nums); 

    
    ukupno_objects[park.pp_name].book_duration_sum = book_duration_sum;
    ukupno_objects[park.pp_name].book_paid_sum = book_paid_sum;
    ukupno_objects[park.pp_name].book_count = book_row - 2;
    ukupno_objects[park.pp_name].book_unique = book_unique.length;
    
    
    
    
    var park_row = 2;
    var park_duration_sum = 0;
    var park_paid_sum = 0;
    var park_unique = [];
    
    $.each(actions_array, function(action_ind, action) {
      
      if ( action.has_park && action.pp_sifra == global.park_places_list[park_ind].pp_sifra ) {
        
        make_unique_array(action.user_email, park_unique);
        
        park_row += 1;
        
        var park_start_date =  '';
        var park_start_time =  '';
        
        if ( action.from_time ) {
          var park_start = global.getMeADateAndTime( action.from_time, 'y-m-d');
          park_start_date = park_start.datum;
          park_start_time = park_start.vrijeme;
        };
        
        
        
        var park_end_date =  '';
        var park_end_time =  '';
        
        var park_paid_num = 0;
        
        if ( action.to_time_c ) {
          
          var park_end = global.getMeADateAndTime( action.to_time, 'y-m-d');
          park_end_date = park_end.datum;
          park_end_time = park_end.vrijeme;
          
          park_paid_num = ( action.duration_c || 0 )/(1000*60*60) * action.current_price;
          
          park_duration_sum += ( action.duration_c || 0 )/(1000*60*60);
          park_paid_sum += park_paid_num;
          
          
        };
        
        
        var poceo_prije = '';
        var jos_traje = '';
        
        if ( action.from_time < action.from_time_c ) poceo_prije = 'DA';
        if ( !action.to_time ) jos_traje = 'DA';
        

        park_sheet
        .cell(park_row, 1)
        .string(action.user_email)
        .style(excel_nums);

        park_sheet
        .cell(park_row, 2)
        .string(action.current_tag || '')
        .style(excel_nums);

        park_sheet
        .cell(park_row, 3)
        .string(action.user_status || '')
        .style(excel_nums);

        park_sheet
        .cell(park_row, 4)
        .string( find_pp_name(action) || '')

        park_sheet
        .cell(park_row, 5)
        .number(action.current_price || 0)
        .style(excel_nums);
        // .style(excel_nums);       

        park_sheet
        .cell(park_row, 6)
        .string( park_start_date + ' ' + park_start_time )
        
        park_sheet
        .cell(park_row, 7)
        .string( park_end_date + ' ' + park_end_time )
        
        
        park_sheet
        .cell(park_row, 8)
        .number( (action.duration_c || 0)/(1000*60*60) )
        .style(excel_nums);
        

        park_sheet
        .cell(park_row, 9)
        .number( park_paid_num )
        .style(excel_nums);
        // .style(excel_nums); 
                
        
        
        park_sheet
        .cell(park_row, 10)
        .string( poceo_prije)
        
                
        
        park_sheet
        .cell(park_row, 11)
        .string( jos_traje)
        
        
        /*
        park_sheet
        .cell(park_row, 12)
        .string( action._id)
        */
        
      }; // kraj ako ima has book 

      
      
    });
        
    
    park_sheet
    .cell(1, 8)
    .number( park_duration_sum )
    .style(sum_style);
    // .style(excel_nums); 



    park_sheet
    .cell(1, 9)
    .number( park_paid_sum )
    .style(sum_style);
    // .style(excel_nums); 

    
    ukupno_objects[park.pp_name].park_duration_sum = park_duration_sum;
    ukupno_objects[park.pp_name].park_paid_sum = park_paid_sum;
    ukupno_objects[park.pp_name].park_count = park_row - 2;
    ukupno_objects[park.pp_name].park_unique = park_unique.length;
    
    
  }); // kraj loopa po svim parkinzima

  
  ws.cell(1, 1)
    .string('PARKING IME')
    .style(header_style);
    
  
  ws.cell(1, 2)
    .string('SATI BOOK')
    .style(header_style);
  
  ws.cell(1, 3)
    .string('SATI PARK')
    .style(header_style);
  
  ws.cell(1, 4)
    .string('IZNOS BOOK')
    .style(header_style);
  
  ws.cell(1, 5)
    .string('IZNOS PARK')
    .style(header_style);
  
    
  ws.cell(1, 6)
    .string('BROJ BOOK')
    .style(header_style);
  
  ws.cell(1, 7)
    .string('BROJ PARK')
    .style(header_style);
  
  
  ws.cell(1, 8)
    .string('BROJ KORISNIKA BOOK')
    .style(header_style);
  
  ws.cell(1, 9)
    .string('BROJ KORISNIKA PARK')
    .style(header_style);
  
  
  
  var ukupno_row = 1;
  $.each(ukupno_objects, function(park_name_key, sum_object) {
    
    
    ukupno_row += 1;

    
  ws.cell(ukupno_row, 1)
    .string(park_name_key);
  
  ws.cell(ukupno_row, 2)
    .number(sum_object.book_duration_sum)
    .style(excel_nums);
  
  ws.cell(ukupno_row, 3)
    .number(sum_object.park_duration_sum)
    .style(excel_nums);
  
  ws.cell(ukupno_row, 4)
    .number(sum_object.book_paid_sum)
    .style(excel_nums);
  
  ws.cell(ukupno_row, 5)
    .number(sum_object.park_paid_sum)
    .style(excel_nums);
    
  ws.cell(ukupno_row, 6)
    .number(sum_object.book_count);
  
  ws.cell(ukupno_row, 7)
    .number(sum_object.park_count);
  
  ws.cell(ukupno_row, 8)
    .number(sum_object.book_unique);
  
  ws.cell(ukupno_row, 9)
    .number(sum_object.park_unique);
    
  });
  
  
  ws.cell(ukupno_row+1, 2)
    .formula(`SUM(B2:B${ukupno_row})`)
    .style(sum_style);
  ws.cell(ukupno_row+1, 3)
    .formula(`SUM(C2:C${ukupno_row})`)
    .style(sum_style);
  
  ws.cell(ukupno_row+1, 4)
    .formula(`SUM(D2:D${ukupno_row})`)
    .style(sum_style);
  
  ws.cell(ukupno_row+1, 5)
    .formula(`SUM(E2:E${ukupno_row})`)
    .style(sum_style);
  
    
  ws.cell(ukupno_row+1, 6)
    .formula(`SUM(F2:F${ukupno_row})`)
    .style(sum_style_no_dec);
  
  ws.cell(ukupno_row+1, 7)
    .formula(`SUM(G2:G${ukupno_row})`)
    .style(sum_style_no_dec);
  
  ws.cell(ukupno_row+1, 8)
    .formula(`SUM(H2:H${ukupno_row})`)
    .style(sum_style_no_dec);
  
  ws.cell(ukupno_row+1, 9)
    .formula(`SUM(I2:I${ukupno_row})`)
    .style(sum_style_no_dec);
  
  
  /*
  // Set value of cell A1 to 100 as a number type styled with paramaters of style
  ws.cell(1, 1)
    .number(100.44)
    .style(excel_nums);

  // Set value of cell B1 to 200 as a number type styled with paramaters of style
  ws.cell(1, 2)
    .number(200.33099)
    .style(excel_nums);

  // Set value of cell C1 to a formula styled with paramaters of style
  ws.cell(1, 3)
    .formula('A1 + B1')
    .style(excel_nums);

  // Set value of cell A2 to 'string' styled with paramaters of style
  ws.cell(2, 1)
    .string('string')
    .style(excel_nums);

  // Set value of cell A3 to true as a boolean type styled with paramaters of style but with an adjustment to the font size.
  ws.cell(3, 1)
    .bool(true)
    .style(excel_nums)
    .style({font: {size: 14}});
  
  */
  
  wb.write(path.join(global.appRoot + '/public/excel/' + timestamp + '.xlsx'), function(err, stats) {
    if (err) {
      
      console.error(err);
      
    } else {
      
      // console.log(stats); // Prints out an instance of a node.js fs.Stats object
      
      // obriši excel file nakon 2 min
      setTimeout(function() {
          // try to remove the file and log the result
          fs.unlink(path.join(global.appRoot + '/public/excel/' + timestamp + '.xlsx'), (err) => {
            if (err) throw err;
            console.log(`Deleted ${ path.join(global.appRoot + '/public/excel/' + timestamp + '.xlsx') }`);
          });

      }, 1000*60*2);

      
      
      var excel_path = '/excel/' + timestamp + '.xlsx';
      res.json({ success: true, excel_path: excel_path });
    }
  });
  
  
};


exports.create_excel = function(req, res) {
  
  var timestamp = Number(req.params.timestamp);
  
  
  var found_cache = null;
  $.each( global.search_actions_result_array_cache, function(cache_ind, cache_obj) {
    if ( cache_obj.timestamp == timestamp ) {
      found_cache = cache_obj.actions;
    };
  });
  
  if ( found_cache == null  ) {
    
    res.json({ success: false, msg: 'Server nije našao podatke za Excel. Molimo ponovo kliknite PRIKAŽI REZULTATE i nakon toga kliknite EXCEL gumb.' });
  } else {
    
    // var timestamp = Date.now();
    
    run_node_4_excel(timestamp, found_cache, res );
    
  };
  
};


exports.cit_search_remote = function(req, res) {
  
  
  var token = req.headers['x-auth-token'] || null;
  
  
  // iznimka  je kada radim request za park places ---_> user ne mora biti ulogiran
  if ( token ) {
      
    check_is_super(token).then(
      
    function( result ) {
      
      
      // iznimka  je kada radim request za park places ---_> user ne mora biti ulogiran
      if ( result.success == true  ) {
        
        // ovo je glavna funkcija za find
        run_find( req, res, result.super_admin, result.user );
        return;
        
        
      } else {
        res.json([]);
        return;
        
      };

    })
    .catch(function(err) {
      res.json([]); 
    });

  } else {
    // vrati prazan array ako nema token
    res.json([]);    
    
  };
  
}; // end of cit_search_remote 


exports.trazi_sve_proizvode = function(req, res) {
  
  var trazi_string = req.body.trazi_string;

  var search_array = [
        { "kupac.ime": { "$regex": trazi_string, "$options": "i" }},
        { "kupac.prezime": { "$regex": trazi_string, "$options": "i" }},
        { "kupac.naziv_tvrtke": { "$regex": trazi_string, "$options": "i" }},
        { "kupac.mjesto": { "$regex": trazi_string, "$options": "i" }},
        { "kupac.adresa": { "$regex": trazi_string, "$options": "i" }},
        { "kupac.telefon": { "$regex": trazi_string, "$options": "i" }},
        { "user_kreirao.user_name": { "$regex": trazi_string, "$options": "i" }},
        { "user_editirao.user_name": { "$regex": trazi_string, "$options": "i" }},
        { "ponuda_number": { "$regex": trazi_string, "$options": "i" }}
      ];
  
  
  
  var search_query_object = { $or: search_array };
  
  if ( !trazi_string ) {
    search_array = [];
    search_query_object = {};
  };
  

  var datum_from = req.body.datum_from;
  var datum_to = req.body.datum_to;

  
  if ( datum_from || datum_to ) {
    // ako postoji neki string onda and stavi taj search pod or
    var and_array = search_array.length > 0 ? [{ $or: search_array }] : [];
    
    if (datum_from) and_array.push( { timestamp_kreirano: { $gte : datum_from }} );
    if (datum_to) and_array.push( { timestamp_kreirano: { $lte : datum_to }} );
    
    search_query_object = { $and: and_array };
    
  };
  
  var trazi_status = req.body.status;

if ( trazi_status ) {
  
  if ( trazi_string || datum_from || datum_to ) {
    if ( search_query_object.$and ) {
      search_query_object.$and.push({ "status": trazi_status });
    } else {
      search_query_object = {
        $and: [{ $or: search_array }, { "status": trazi_status }]
      }
    }
  } else {
    
    search_query_object = { "status": trazi_status };
  };
  
};
  
    // ako je trazi_polja == daj_sve
    Ponuda
      .find(search_query_object)
      .limit(200)
      .sort({ timestamp_kreirano: -1 })
      .select({
        _id : 1, 
        "kupac.ime" : 1,
        "kupac.prezime" : 1,
        "kupac.naziv_tvrtke" : 1,
        "timestamp_kreirano": 1,
        "status": 1,
        "user_kreirao": 1,
        "user_editirao": 1,
        "kratka_opaska": 1
      }) 
      .exec(function(err, ponude) {
        if (err) res.send(err);
        res.json(ponude);
      });

  
}; // end of trazi sve proizvode


