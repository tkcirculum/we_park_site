var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ParkPlace = mongoose.model('ParkPlace');
var ParkAction = mongoose.model('ParkAction');
var User = mongoose.model('User');

var CustomerProduct = mongoose.model('CustomerProduct');

var WE_I18N = require('./we_server_i18n');


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


var customer_products_controller = require('./customer_products_controller');
var pp_controller = require('./park_places_controller');

console.log( '------------------------------- ovo je DATE now '  );
console.log( Date.now() );


// ADDITIONAL OPTIONS FOR NATIVE JAVASCRIPT SORT ------- from http://jsfiddle.net/gfullam/sq9U7/  
window.by = function(path, reverse, primer, then) {
  var get = function (obj, path) {
      if (path) {
        path = path.split('.');
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        };
        return obj[path[len]];
      }
      return obj;
    },
    prime = function (obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function (a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B) ? -1 :
      (A > B) ? 1 :
      (typeof then === 'function') ? then(a, b) : 0
    ) * [1, -1][+!!reverse];
  };
};

function cit_rand_min_max(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
};


function create_ms_at_zero_and_week_day(add_days) {

  var date = new Date();
  var yyyy = date.getFullYear();
  var mnt = date.getMonth();
  
  var day = date.getDate() + (add_days || 0 ); // plus or minus days    

  // nedjelja je 0 ---> subota je 6
  var week_day = date.getDay();

  var day_at_zero = new Date(yyyy, mnt, day, 0, 0, 0, 0);
  var ms_at_zero = Number(day_at_zero); //  day_at_zero.getMilliseconds();

  return {
    ms_at_zero: ms_at_zero,
    week_day: week_day
  };

};
exports.create_ms_at_zero_and_week_day = create_ms_at_zero_and_week_day;


function calc_dynamic_price(park, price) {


  
  if ( price.pp_basic_price_out == null ) return null;

  var basic_price = price.pp_basic_price_out;
  
  var dyn_price = basic_price;

  var granica = price.dyn_zauzetost_granica; // npr ako je zauzetost veća od 50 posto tek oda povisuj cijenu!!!!

  var max_price = basic_price * park.max_pp_price_factor; // Boris mi je rekao da max cijena duplo veća od osnovne !!!!!

  var count_in = park.pp_count_in_num;
  var count_out = park.pp_count_out_num;

  var available_in = park.pp_available_in_num;
  var available_out = park.pp_available_out_num;

  var zauzetost_in_percent = 100;
  var zauzetost_out_percent = 100;


  // trenutno nema IN mjesta
  // var zauzetost_in_count = count_in;
  // if ( count_in ) zauzetost_in = available_in / count_in;

  var zauzetost_out_percent = available_out / count_out * 100;

  
  // ako uopće postoji max price faktor tj veći je od nule !!!!!
  // i ako je zauzetost veća od postavljene 
  if ( park.max_pp_price_factor && zauzetost_out_percent > granica ) {

    var zauzetost_out_count = count_out - available_out;

    var zadnje_mjesto_s_basic_price = Math.floor( count_out * granica/100 );

    var mjesta_koja_poskupljuju = count_out - zadnje_mjesto_s_basic_price;

    var korak_povecanja_cijena = (max_price - basic_price) / mjesta_koja_poskupljuju;

    var prvo_slobodno_mjesto = zauzetost_out_count + 1;

    var diff_mjesta = prvo_slobodno_mjesto - zadnje_mjesto_s_basic_price;

    var poskupljenje = diff_mjesta * korak_povecanja_cijena;

    dyn_price = basic_price + poskupljenje;

  };

  return dyn_price;

};
exports.calc_dynamic_price = calc_dynamic_price;


function find_current_price(park) {

  var dynamic_pp_price = null;

  if ( park.pp_prices && $.isArray(park.pp_prices) && park.pp_prices.length > 0 ) {

    var time_stamp = Date.now();

    var zero_time_object = create_ms_at_zero_and_week_day();

    var ms_at_zero = zero_time_object.ms_at_zero;
    var week_day = zero_time_object.week_day;


    park.pp_prices.sort( window.by('pp_price_time_from', false, Number ) );

    var price_already_found = false;

    /*
    ----------------------------- BITNO -----------------------------

    prvo prođi loop ako postoji u nekoj od cijena točan dan kao trenutni 

    ----------------------------- BITNO -----------------------------
    */

    $.each( park.pp_prices, function( pp_price_index, price) {
      // NAPOMENA UVJEK KADA PIŠE time_to oduzimam 200 ms
      // sada tih 200 ms dodajem nazad !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if (
        time_stamp >= (ms_at_zero + price.pp_price_time_from) 
        &&
        time_stamp <  (ms_at_zero + price.pp_price_time_to + 200) 
        && 
        week_day == Number(price.pp_price_day.sifra)
      ) {

        price_already_found = true;
        dynamic_pp_price = calc_dynamic_price( park, price );

      };

    });

    // NASTAVI DALJE SAM AKO NISAM NAŠAO CIJENU ZA SPECIFIČAN DAN U TJEDNU
    // ... ili možda nema niti jedne cijene sa upisanim specifičnim danom u tjednu
    if ( price_already_found !== true ) {

      $.each( park.pp_prices, function( pp_price_index, price) {
          // NAPOMENA UVJEK KADA PIŠE time_to oduzimam 200 ms
          // sada tih 200 ms dodajem nazad !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          if (
            time_stamp >= (ms_at_zero + price.pp_price_time_from)
            &&
            time_stamp <  (ms_at_zero + price.pp_price_time_to + 200)
            && 
            price.pp_price_day.sifra == 'any_day'
          ) {

            dynamic_pp_price = calc_dynamic_price( park, price );

          };

        });
      
    };

  }; // end  ako postoj array cijena


  return dynamic_pp_price;



}; // end find cur price func
exports.find_current_price = find_current_price;


function convert_mongoose_object_to_js_object(mongoose_object) {
  
  var js_object = mongoose_object.toJSON();
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};


function update_user_products(pp_sifra, user_number, paid, action_obj) {
  

  if (
    user_number == 182 ||
    user_number == 184 ||
    user_number == 185 ||
    user_number == 410
  ) {
    user_number = 333;  // ovo je zajednički user za sve s&t korisnike
  };
  
  
  if ( paid == null || typeof paid == 'undefined' ) paid = 0;
  
  // ako je paid manji od nule pretvori ga u pozitiv ( jer je to sigurno neka greška )
  if ( paid < 0 ) paid = -1 * paid;
  
  return new Promise( function(resolve, reject) {
  
  CustomerProduct
      .find({ user_number: user_number })
      .sort({ buy_time: -1 })
      .exec(function(err, products) {

        if (err) {
          console.error("greška prilikom pronalaska user producta !!!!");
          console.log(err);
          reject({success: false, msg: "greška prilikom pronalaska user producta !!!!", err: err });
        } else {

        var clean_products = [];
      
        if ( products.length > 0 ) {
          
          products.forEach(function(product_obj, product_index) {
            var clean_product_obj = convert_mongoose_object_to_js_object(product_obj);
            clean_products.push(clean_product_obj);
          });
          
          // poredaj od najstarijeg datume prema najnovijem datumu
          clean_products.sort( window.by('buy_time', false, Number ) );

          var nasao_product_i_platio = false;
          
          var time_stamp = Date.now();
          
          
          // ------------------------------------------------------ PRVO VRTI PO SVIM MJESEČNIM PRODUKTIMA
          clean_products.forEach(function(product_obj, product_index) {
            
            // uvijek prvo gledam jel ima mjesečnu pretplatu i tu probam napraviti update
            if ( 
              nasao_product_i_platio == false                                  &&
              ( product_obj.type == 'monthly' || product_obj.type == 'vip' )   &&
              product_obj.valid_from <= time_stamp                             &&
              product_obj.valid_to > time_stamp                                &&
              Number(product_obj.pp_sifra) == Number(pp_sifra)                 &&
              
              
              /*
              product tag  i action current tag mogu biti oboje null ili
              na primjer mogu oboje biti TELE2
              */
              
              ( product_obj.tag == action_obj.current_tag || action_obj.user_status == 'vip')
            ) {
              
              nasao_product_i_platio = true;

              CustomerProduct.findOneAndUpdate(
                { _id: product_obj._id },
                { $inc: { balance: -paid } },
                { new: true }, function(err, customer_product) {
                if (err) reject({ success: false, msg: "popup_update_products_error", err: err});
                resolve({ success: true, msg: "Customer product updated!", customer_product: customer_product});
              });
              
            };
                    
          }); // end od loopa kroz sve clean produkte dok tražim montly 
              
          
          // ------------------------------------------------------NAKON TOGA VRTI PO SVIM VOUCHERIMA 
          
          clean_products.forEach(function(product_obj, product_index) {
            
            var time_stamp = Date.now();
            
            if ( 
              nasao_product_i_platio == false 
              &&
              ( product_obj.type == 'voucher' || product_obj.type == 'card' )
            ) {
              
              nasao_product_i_platio = true;
              
              CustomerProduct.findOneAndUpdate(
                { _id: product_obj._id },
                { $inc: { balance: -paid } },
                { new: true }, function(err, customer_product) {
                if (err) reject({ success: false, msg: "popup_update_products_error", err: err});
                resolve({ success: true, msg: "Customer product updated!", customer_product: customer_product});
              });
              
            };
            
          }); // end od loopa kroz sve clean produkte dok tražim montly 
          
          // ako je još uvijek ova varijabla  false
          // to znači da je user ušao u parking dok je imao važeću mjesečnu kartu
          // ali je izašao kada više mjesečna nije bila važeća

          // if ( nasao_product_i_platio == false  ) 
          
          
        } else {
          
          reject({ success: false, msg: "no_active_products" });
          
        }; // / end ako user ima bilo koji product 
          
          
        }; // end ako customer product find nije javio error tj ako je uspjeh /
    
      });
    
  }); // end of promise
};


function check_avail(action_obj, park_data ) {
  
  
  var ima_mjesta = true;
  
  // UVIJEK VRATI DA IMA MJESTA AKO JE VIP USER ILI SUPER ADMIN !!!!
  if ( action_obj.user_status == 'vip' || action_obj.user_status == 'super' ) return true;
  
  
  
  if ( action_obj.current_tag ) {
  
    if ( park_data.special_places[action_obj.current_tag].avail_out == 0 ) ima_mjesta = false;

  } else {
  

    // provjeri jel ima mjesta AKO JE ZA VANI
    if ( action_obj.indoor == false && park_data.pp_available_out_num == 0 ) {
      ima_mjesta = false;
    };

    // provjeri jel ima mjesta AKO JE ZA UNUTRA
    if ( action_obj.indoor == true && !park_data.pp_available_in_num == 0 ) {
      ima_mjesta = false;
    };
    
  };
  
  return ima_mjesta;
  
};


function check_balance(booking_or_parking, user_products, action_obj) {
  
  var ima_lovu = true
  
  // ako nema pretplatu za ovaj parking

  // ------- NEMA MJESEČNU i ------------ OVO JE UVIJET ZA BOOKING -------------------
  // onda pogledaj jel ima love za booking koji je odabrao
  // mora imati love cijena_parkinga_po_satu X broj_sati_bookinga !!!!!!!
  if (
    booking_or_parking == 'booking'
    &&
    user_products.balance_sum < ( action_obj.current_price * action_obj.user_set_book_duration )
  ) {

    ima_lovu = false;

  };

  // ------ NEMA MJESEČNU i ------------- OVO JE UVIJET ZA NEW PARKING -------------------
  // onda pogledaj jel ima love na vaucheru tj sumi svih vaučera

  if (
    booking_or_parking == 'parking'
    &&
    user_products.balance_sum <= 0
  ) {
    ima_lovu = false;

  };   

  // svakako postavi da ima lovu ako je cijena parkinga nula tj besplatno
  if ( action_obj.current_price == 0 ) ima_lovu = true;
  
  return ima_lovu;
  
};


function check_pp_monthly( user_products, park_data, action_obj ) {
  
  var mjesecna_za_ovaj_pp = false;

  var sve_mjesecne = user_products.monthly_arr;

  // loop kroz sve mjesečne koje ima korisnik i provjeri jel ima mjesečnu za ovaj parking !!!
  // NAPOMENA ovaj array ima samo mjesecne koje su aktive tj koje su u toku !!!!
  if ( sve_mjesecne && sve_mjesecne.length > 0 ) {
    
    /*
    obavezno mora biti tag od producta jednak current tagu userove akcije
    npr ako user ima pretplatu od svoje firme onda će mu u productu pisati da je to TELE2 tag
    ako se dogodi da nema mjesta u TELE2 dijelu parkinga onda user se može prebaciti na OBIČNOG USERA
    u tom slučaju current tag mu neće biti TELE2 već će biti null
    i onda se njegov current tag neće poklapati za tagom od objekta pretplate
    */

    $.each(sve_mjesecne, function( mjesecna_index, mjesecna ) {
       
      if ( Number(mjesecna.pp_sifra) == Number(park_data.pp_sifra) ) { 
        
        mjesecna_za_ovaj_pp = true; 
        
        if ( mjesecna.tag ) {
          // ako ima special tag ali taj tag nije isti kao action tag 
          // to znači da je ušao kao neki drugi user na primjer usao je kao obični user ne jedno parkiralište
          // ali ima specijalno mjesto za neko drugo parkiralište !!!!
          if ( mjesecna.tag !== action_obj.current_tag ) { mjesecna_za_ovaj_pp = false; };
        };
        
      };                                                                      
      
    });
    
  };
  
  return mjesecna_za_ovaj_pp;
};


function save_new_action( action_obj ) {
  
  return new Promise( function( resolve, reject ) {

    var new_park_action = new ParkAction(action_obj);  

    new_park_action.save(function(err, park_action) {

      if (err) reject({ success: false, msg: "Park action not saved!", err: err});
        
      var clean_action = convert_mongoose_object_to_js_object( park_action );  
      resolve( { success: true, msg: "Park action is saved!", action: clean_action } );

    }); // end of spremi action

  });
  
}
exports.save_new_action = save_new_action;

function save_new_available_state( action_obj, num, park_data ) {
  
  
  return new Promise( function( resolve, reject ) {
  
    var update_object = {};
    
    // ako je current current tag null ili ""
    // onda je to običan user
    // i stoga radim update na "običnim" mjestima
    if ( !action_obj.current_tag ) {
    
      var new_in_avail = park_data.pp_available_in_num + num;
      var new_out_avail = park_data.pp_available_out_num + num;
      // za svaki slučaj ako se dogodi neka greška NIKAKO NE SMJEM PODIĆI AVAILABLE NUMBER DA BUDE VEĆI OD MOGUĆEG !!!!!
      if ( new_in_avail > park_data.pp_count_in_num ) new_in_avail = park_data.pp_count_in_num;
      if ( new_out_avail > park_data.pp_count_out_num ) new_out_avail = park_data.pp_count_out_num;

      // isto tako moram za svaki slučaj paziti da available number ne bude manji od nula !!!!
      if ( new_in_avail < 0 ) new_in_avail = 0;
      if ( new_out_avail < 0 ) new_out_avail = 0;



      if ( action_obj.indoor ) update_object = { pp_available_in_num: new_in_avail };
      if ( !action_obj.indoor ) update_object = { pp_available_out_num: new_out_avail };

    };
    
    
    // ako je current tag nešto kao TELE2 ili SIGNIFY
    // onda je specijalni user
    // i stoga radim update na spcifičnim mjestima
    if ( action_obj.current_tag ) {
      
      var new_in_avail = park_data.special_places[action_obj.current_tag].avail_in + num;
      var new_out_avail = park_data.special_places[action_obj.current_tag].avail_out + num;
      
      // za svaki slučaj ako se dogodi neka greška NIKAKO NE SMJEM PODIĆI AVAILABLE NUMBER DA BUDE VEĆI OD MOGUĆEG !!!!!
      if ( new_in_avail > park_data.special_places[action_obj.current_tag].count_in ) {
        new_in_avail = park_data.special_places[action_obj.current_tag].count_in;
      };
      if ( new_out_avail > park_data.special_places[action_obj.current_tag].count_out ) {
        new_out_avail = park_data.special_places[action_obj.current_tag].count_out;
      };

      // isto tako moram za svaki slučaj paziti da available number ne bude manji od nula !!!!
      if ( new_in_avail < 0 ) new_in_avail = 0;
      if ( new_out_avail < 0 ) new_out_avail = 0;

      var avail_in_key =  'special_places.' + action_obj.current_tag + '.avail_in';
      var avail_out_key =  'special_places.' + action_obj.current_tag + '.avail_out';

      if ( action_obj.indoor ) update_object = { [avail_in_key]: new_in_avail };
      if ( !action_obj.indoor ) update_object = { [avail_out_key]: new_out_avail };

    };    
     
    
    ParkPlace.findOneAndUpdate(
      { _id: park_data._id },
      update_object,
      { new: true }, function(err, park_place) {
        // pošto cijela ova operacija save new action može biti booking ali može biti i parking
        // setiram da je booking  
        var msg = 'popup_greska_prilikom_bookinga';  
        // ali ako je from time property koji može biti samo za parking različit od null ----> onda je ovo novi parking
        if ( action_obj.from_time !== null )  msg = 'popup_greska_prilikom_parkinga'; 
        
        if (err) {
          reject({ success: false, msg: msg, err: err});
        } else {
          resolve({ success: true, msg: "Park avail number updated!", park: park_place });
        };
        
    }); //end of mongoose update
    
    
  }); // end of prom

};


exports.portir_avail_new_state = function(req, res) {
  
  var action_obj = req.body.action_obj;
  var num = req.body.num;
  var park_data = null;
  
  pp_controller.query_park_places({ pp_sifra: req.body.pp_sifra })
  .then(
  function(pp_result) { 
    park_data = pp_result.park_places[0];
    return save_new_available_state( action_obj, num, park_data );
  })
  .then(
    function(result) { res.json(result) },
    function(error) { res.json(error) }
  );
  
};

exports.new_booking = function(req, res) {
  
  var action_obj = req.body;
  var action_sifra = Date.now() + '-' + cit_rand_min_max(1, 10000);
  action_obj.sifra = action_sifra;
  
  // provjeri za svaki slučaj da možda nema već booking aktivan 
  // to je fail safe u slučaju da nešto krene posve pos zlu 
  // i ne prikažem useru u aplikaciji da  je već bookirao ovaj parking !!!
  
  var time_now = Date.now();
  
  
  
  var new_query = {
    pp_sifra: action_obj.pp_sifra,
    user_number: action_obj.user_number,
    status: 'booked',
    reservation_from_time: { $ne: null },
    reservation_to_time: null
  };
  
  
  var park_data = null;
  var user_products = null;
  var ima_mjesecnu = null;
  var ima_novce = null;
  var ima_mjesta = null;
  
  var saved_action = null;
  
  var booking_price = null;

  
  query_active_booking(new_query)
  .then(
  function(booking_result) { 
    
    // booking result mora biti prazan array jer je ovo new booking
    if ( booking_result.actions.length > 0 ) {
      
      return new Promise( function( resolve, reject ) {
        reject({ success: false, msg: 'vec_postoji_booking' });
      });
      
    } else {
    return pp_controller.query_park_places({ pp_sifra: action_obj.pp_sifra }) 
    };
  })
  .then(
  function(pp_result) { 
    park_data = pp_result.park_places[0];

    action_obj.current_price = park_data.pp_current_price;
    
    // ali ako user ima neki od status npr super, owner ili free ili vip onda njemu parking nije zatvoren !!!!!!!
    if ( park_data.pp_current_price == null && action_obj.user_status == '' ) {
      
      return new Promise( function( resolve, reject ) {
        reject({ success: false, msg: 'popup_closest_open_time', park: park_data });
      });
      
    } else {
      
      return customer_products_controller.run_user_products_state(action_obj.user_number);
    };
  })
  .then(
  function(products) {
    
    // ovo je za new booking
    
    user_products = products;
    
    ima_mjesecnu = check_pp_monthly(user_products, park_data, action_obj);
    // prvi arg u chekiranju vaučera je da li je ovo booking ( false za parking )
    if ( !ima_mjesecnu ) ima_novce = check_balance('booking', user_products, action_obj);
    
    var ima_mjesta = check_avail(action_obj, park_data);

    // ako nema niti mjesečnu niti novce onda prekini promise chain !!!!
    if ( !ima_mjesecnu && !ima_novce ) {
      
      return new Promise( function( resolve, reject ) {
        reject({ success: false, msg: 'popup_nemate_lovu_ili_pretplatu' });
      });
      
    } else if ( !ima_mjesta ) {
      
      return new Promise( function( resolve, reject ) {
        reject({ success: false, msg: 'popup_nema_vise_mjesta' });
      });
      
    } else {
      
      // booking_price = action_obj.user_set_book_duration * action_obj.current_price;
      // action_obj.reservation_paid = booking_price;
      return save_new_action(action_obj);
      
    };
      
  })
  .then(
  function(result) {
    saved_action = result.action;
    return save_new_available_state( saved_action, -1, park_data );
  })
  .then(
  function(result) {
    
    result.action = saved_action;
  
    User.findOneAndUpdate(
      { user_number: saved_action.user_number },
      { $addToSet: { visited_pp: saved_action.pp_sifra } },
      {new: true}, 
      function(up_user_err, up_user) {
        if (up_user_err) {
          res.json({ success: false, error: up_user_err });
        } else {
          res.json(result);
        }
      });      
    
  })
  .catch(function(error) {
    res.json(error);
  })
  

}; // KRAJ new_booking


exports.cancel_booking = function(req, res) {
  var pp_sifra = req.params.pp_sifra;
  var user_number = Number( req.params.user_number);
  
  query_cancel_booking(pp_sifra, user_number)
    .then(
      function(result) { res.json(result) },
      function(error) { res.json(error) }
    )  
  
};


function query_cancel_booking(pp_sifra, user_number) {
  
  var pp_sifra = pp_sifra;
  var user_number = user_number;
  
  var new_query = {
    pp_sifra: pp_sifra,
    user_number: user_number,
    status: 'booked',
    reservation_from_time: { $ne: null },
    reservation_to_time: null
  };
  
  var action_obj = null;
  
  var park_data = null;
  var user_products = null;
  var ima_mjesecnu = null;
  var ima_novce = null;
  var ima_mjesta = null;
  
  var saved_action = null;
  
  
  var time_now = Date.now();
  var time_passed = null;
  var time_in_min = null;
  var price_so_far = null;
    
  return new Promise( function( resolve_global, reject_global ) {

    query_active_booking(new_query)
    .then(
    function(booking_result) { 

      // booking result NE SMJE BITI PRAZAN JER ONDA NEMAM ŠTO CANCELATI :)
      if ( booking_result.actions.length == 0 ) {

        return new Promise( function( resolve, reject ) {
          reject({ success: false, msg: 'nema_bookinga' });
        });

      } else {
        action_obj = booking_result.actions[0];

        return pp_controller.query_park_places({ pp_sifra: pp_sifra }) 
      };    
    })
    .then(
    function(pp_result) { 
      park_data = pp_result.park_places[0];
      return customer_products_controller.run_user_products_state(action_obj.user_number);
      
    })
    .then(
    function(products) {
      
      
      time_passed = time_now - action_obj.reservation_from_time;
      time_in_min = Math.ceil( time_passed / (1000*60) );
      price_so_far = time_in_min * (action_obj.current_price/60);

      return update_user_products(pp_sifra, user_number, price_so_far, action_obj);
      
    })
    .then(
    function(updated_product) {

      action_obj.reservation_to_time = time_now;
      action_obj.book_duration = time_passed;
      action_obj.reservation_canceled = true;
      action_obj.reservation_paid = price_so_far;
      
      return query_edit_park_action( action_obj._id, action_obj );

    })
    .then(
    function(result) {
      saved_action = result.action;
      return save_new_available_state( saved_action, 1, park_data );
    })
    .then(
    function(result) {
      result.action = saved_action;
      resolve_global(result);
    })
    .catch(function(error) {
      reject_global(error);
    });
  
  });
  
};
exports.query_cancel_booking = query_cancel_booking;



function query_check_park(query) {
  
  
  var action_obj = {
    current_tag: query.current_tag,
    user_status: query.user_status,
    user_number: query.user_number,
    indoor: query.indoor
  };
  
  var pp_sifra = query.pp_sifra
    
  var park_data = null;
  var user_products = null;
  var ima_mjesecnu = null;
  var ima_novce = null;
  var ima_mjesta = null;
  
  var saved_action = null;
  var booking_in_progress = false;
  var parking_in_progress = false;
  
  return new Promise( function( resolve_global, reject_global ) {  

    var active_booking_query = {
    
      pp_sifra: pp_sifra,
      user_number: action_obj.user_number,
      status: 'booked',
      reservation_from_time: { $ne: null },
      reservation_to_time: null
    
    };
    
    var active_parking_query = {
    
      pp_sifra: pp_sifra,
      user_number: action_obj.user_number,
      status: 'parked',
      from_time: { $ne: null },
      to_time: null
    
    };
    
    query_active_booking(active_booking_query)
    .then(
    function(booking_result) { 

      // booking actions je array i ako ima nešto u njemu onda postoji booking za ovaj park
      if ( booking_result.actions.length > 0 ) {
        booking_in_progress = true; 
      };
      
      return query_active_parking(active_parking_query)
      
    })
    .then(
    function(parking_result) { 

      // booking actions je array i ako ima nešto u njemu onda postoji booking za ovaj park
      if ( parking_result.actions.length > 0 ) {
        parking_in_progress = true; 
      };
      
      return pp_controller.query_park_places({ pp_sifra: pp_sifra })
      
    })
    
    .then(
    function(pp_result) { 
      park_data = pp_result.park_places[0];

      // ako je vrijeme null to znači da je parking zatvoren
      // ako je običan user onda mu je status ""
      if ( 
        park_data.pp_current_price == null &&
        action_obj.user_status == ''          
        /* &&  booking_in_progress == false */ 
        /* ovo nema smisla - zašto bi useru dao da uđe izvan radnog vremena samo zato što je bookirao dok je radilo ???? */
      ) {

        /*
        return new Promise( function( resolve, reject ) {
          reject({ success: false, msg: 'popup_closest_open_time', park: park_data });
        });
        */
        
        // promijenio sam reject u global ----> NISAM SIGURAN HOĆE LI TO RADITI KAKO TREBA !!!!!
        reject_global({ success: false, msg: 'popup_closest_open_time', park: park_data });
        return;
        
      } else {
        
        // ako user status nije prazan onda: 
        // user ima neki od status npr super, owner ili free  ili vip onda njemu parking nije zatvoren !!!!!!!

        return customer_products_controller.run_user_products_state(action_obj.user_number);

      };
    })
    .then(
    function(products) {

      // ovo je za check park

      user_products = products;

      ima_mjesecnu = check_pp_monthly(user_products, park_data, action_obj);
      
      // prvi arg u chekiranju vaučera je da li je ovo booking ( false za parking )
      if ( !ima_mjesecnu ) ima_novce = check_balance('parking', user_products, action_obj);

      var ima_mjesta = true;
      
      if ( booking_in_progress == false ) ima_mjesta = check_avail(action_obj, park_data);
      
      if ( parking_in_progress == true ) {
        // VAŽNO  - ovaj dio je koristan samo ako je request poslan od picam na rampi
        // inače ako user koristi gumb na mobitelu nikada neće moći kliknuti na ulazak u parking
        // ako je već ušao jer interface mu neće niti prikazati taj gumb !!!!!
        resolve_global({ success: false, msg: 'popup_postoji_vise_parkinga' });
      };

      // ako nema niti mjesečnu niti novce onda prekini promise chain !!!!
      if ( !ima_mjesecnu && !ima_novce ) {

        resolve_global({ success: false, msg: 'popup_nemate_lovu_ili_pretplatu' });
 
      } else if ( !ima_mjesta ) {

        resolve_global({ success: false, msg: 'popup_nema_vise_mjesta' });
 
      } else {
        resolve_global({ success: true });

      };

    })
    .catch(function(error) {
      reject_global({success: false, msg: 'popup_greska_prilikom_parkinga', err: error });
    });

  }); // kraj global promisa

}; // KRAJ query check park


exports.request_check_park = function(req, res) {
  
  query_check_park(req.body)
    .then(
      function(result) { res.json(result) },
      function(error) { res.json(error) }
    );  
};


exports.enter_park = function(req, res) {
  
  var action_obj = req.body;
  var action_sifra = Date.now() + '-' + cit_rand_min_max(1, 10000);
  action_obj.sifra = action_sifra;
  
  query_enter_park(action_obj)
    .then(
      function(result) { res.json(result) },
      function(error) { res.json(error) }
    )  
  
};




function query_enter_park(action_obj) {

  // provjeri za svaki slučaj da možda nema već booking aktivan 
  // to je fail safe u slučaju da nešto krene posve pos zlu 
  // i ne prikažem useru u aplikaciji da  je već bookirao ovaj parking !!!
  
  
  var active_booking_query = {
    
      pp_sifra: action_obj.pp_sifra,
      user_number: action_obj.user_number,
      status: 'booked',
      reservation_from_time: { $ne: null },
      reservation_to_time: null
    };
  
  var park_data = null;
  var user_products = null;
  var ima_mjesecnu = null;
  var ima_novce = null;
  var ima_mjesta = null;
  var booking_in_progress = false;
  
  var saved_action = null;
  
  var active_parking_query = {
    
      pp_sifra: action_obj.pp_sifra,
      user_number: action_obj.user_number,
      status: 'parked',
      from_time: { $ne: null },
      to_time: null
    
    };
  
  var existing_booking = null;
  var existing_parking = null;
  
  return new Promise( function( super_resolve, super_reject ) {
  
  
  query_active_booking(active_booking_query)
  .then(
  function(booking_result) { 
    existing_booking = booking_result;
    return query_active_parking(active_parking_query);
  })
  .then(
  function(parking_result) { 
    
    existing_parking = parking_result;
    
    // ako je već registrirano parkiranje na tom parkingu !!!!
    if ( existing_parking.actions.length > 0 ) {
      return new Promise( function( resolve, reject ) { 
        reject({  success: false, msg: 'popup_postoji_vise_parkinga' });
      });
      
    };
    
    // ako postoji booking onda 
    if ( existing_booking.actions.length > 0 ) {
      booking_in_progress = true;
      // ako trebam napraviti cancel booking 
      // onda izvrtim sve to i ovaj cijeli chain mi vrati result.action
      return query_cancel_booking( action_obj.pp_sifra, action_obj.user_number);
    } else {
      // ako nema bookinga vrati promise u kojem se nalazi vec postojeći action objekt za parking
      // u biti vraćam objekt koji već imam samo što sam ga wrappao u promise da tako nastavim chain
      return new Promise( function( resolve, reject ) { resolve({ action: action_obj }) });
    };
    
  })
  .then(
  function(result) { 
    
    action_obj = result.action;
  
    action_obj.from_time = Date.now();
    action_obj.status = 'parked';
    
    return pp_controller.query_park_places({ pp_sifra: action_obj.pp_sifra });
      
  })
  .then(
  function(pp_result) { 
    park_data = pp_result.park_places[0];

    action_obj.current_price = park_data.pp_current_price;
    
    // ako je current price null to znači da je parking zatvoren
    // ako je običan user onda mu je status ""
    if ( park_data.pp_current_price == null && action_obj.user_status == '' ) {
      
      return new Promise( function( resolve, reject ) {
        reject({ success: false, msg: 'popup_closest_open_time', park: park_data });
      });
      
    } else {
      
      // ali ako user ima neki od status npr super, owner ili free onda njemu parking nije zatvoren !!!!!!!
      return customer_products_controller.run_user_products_state(action_obj.user_number);
      
    };
  })
  .then(
  function(products) {

    user_products = products;
    
    ima_mjesecnu = check_pp_monthly(user_products, park_data, action_obj);
    // prvi arg u chekiranju vaučera je da li je ovo booking ( false za parking )
    if ( !ima_mjesecnu ) ima_novce = check_balance('parking', user_products, action_obj);
    
    var ima_mjesta = true;
    
    // NEMOJ PROVJERAVATI JEL IMA MJESTA AKO JE USER VEĆ BOOKIRAO OVAJ PARKING U KOJEG SAD ULAZI !!!!!
    // tada sigurno ima mjesta jer ga je rezervirao :)
    if ( booking_in_progress == false ) ima_mjesta = check_avail(action_obj, park_data);

    // ako nema niti mjesečnu niti novce onda prekini promise chain !!!!
    if ( !ima_mjesecnu && !ima_novce ) {
      
      return new Promise( function( resolve, reject ) {
        reject({ success: false, msg: 'popup_nemate_lovu_ili_pretplatu' });
      });
      
    } else if ( !ima_mjesta ) {
      
      return new Promise( function( resolve, reject ) {
        reject({ success: false, msg: 'popup_nema_vise_mjesta' });
      });
      
    } else {
      // ako je action objekt već postojao tj ako je već bio booking
      // onda samo editiraj taj action od bookinga i pretvori ga u parking
      if ( action_obj._id ) return query_edit_park_action(action_obj._id, action_obj );
      // ako je ovo posve novi parking
      if ( !action_obj._id ) return save_new_action(action_obj);
      
    };
      
  })
  .then(
  function(result) {
    saved_action = result.action;
    return save_new_available_state( saved_action, -1, park_data );
  })
  .then(
  function(result) {
    result.action = saved_action;
    
    User.findOneAndUpdate(
      { user_number: saved_action.user_number },
      { $addToSet: { visited_pp: saved_action.pp_sifra } },
      {new: true}, 
      function(up_user_err, up_user) {
        if (up_user_err) {
          super_reject({ success: false, error: up_user_err });
        } else {
          super_resolve(result);
        }
      });
    
  })
  .catch(function(error) {
    super_reject(error);
  })
  
  }); // kraj od super promisa !!!!!
  
  
}; // KRAJ enter park

exports.query_enter_park = query_enter_park; 


exports.exit_park = function(req, res) {
  
  var pp_sifra = req.params.pp_sifra;
  var user_number = req.params.user_number;
  
  query_exit_parking(pp_sifra, user_number)
    .then(
      function(result) { res.json(result) },
      function(error) { res.json(error) }
    )  
  
};



function query_exit_parking(pp_sifra, user_number) {
  
  var pp_sifra = pp_sifra;
  var user_number = user_number;
  
  
  var parking_query = {
    pp_sifra: pp_sifra,
    user_number: user_number,
    from_time: { $ne: null },
    to_time: null
  };
  
  var action_obj = null;
  
  var park_data = null;
  var user_products = null;
  var ima_mjesecnu = null;
  var ima_novce = null;
  var ima_mjesta = null;
  
  var saved_action = null;
  
  
  var time_now = Date.now();
  var time_passed = null;
  var time_in_min = null;
  var price_so_far = null;
    
  return new Promise( function( resolve_global, reject_global ) {

    query_active_parking(parking_query)
    .then(
    function(parking_result) { 

      // booking result NE SMJE BITI PRAZAN JER ONDA NEMAM ŠTO CANCELATI :)
      if ( parking_result.actions.length == 0 ) {

        return new Promise( function( resolve, reject ) {
          reject({ success: false, msg: 'nema_parkinga' });
        });

      } else {
        action_obj = parking_result.actions[0];
        return pp_controller.query_park_places({ pp_sifra: pp_sifra }) 
      };    
    })
    .then(
    function(pp_result) { 
      park_data = pp_result.park_places[0];
      return customer_products_controller.run_user_products_state(action_obj.user_number);
    })
   .then(
    function(products) {

      // ovo je za new booking

      user_products = products;

      time_passed = time_now - action_obj.from_time;
      time_in_min = Math.ceil( time_passed / (1000*60) );
      price_so_far = time_in_min * (action_obj.current_price/60);

      return update_user_products(pp_sifra, user_number, price_so_far, action_obj);

    })
    .then(
    function(updated_product) {

      action_obj.to_time = time_now;
      action_obj.duration = time_passed;
      action_obj.paid = price_so_far;
      return query_edit_park_action( action_obj._id, action_obj );

    })
    .then(
    function(result) {
      saved_action = result.action;
      return save_new_available_state( saved_action, 1, park_data );
    })
    .then(
    function(result) {
      result.action = saved_action;
      resolve_global(result);
    })  
    .catch(function(error) {
      reject_global(error);
    });
  
  });
  
};
exports.query_exit_parking = query_exit_parking;


function run_exp_book() {
  
  
  var action_object = global.exp_bookings[0];
  
  var time_now = Date.now();
          
  var vrijeme_isteka = action_object.user_set_book_duration*60*60*1000 + action_object.reservation_from_time;
  var jos_ostalo_vremena =  vrijeme_isteka - time_now;
  var pp_sifra = action_object.pp_sifra;
  var user_number = action_object.user_number;

  var saved_action = null;
  var park_data = null;

  // ako je ostalo manje od 0 sekundi
  if ( jos_ostalo_vremena <= 0 ) {


    action_object.reservation_to_time = vrijeme_isteka;
    action_object.book_duration = vrijeme_isteka - action_object.reservation_from_time;
    action_object.reservation_expired = true;

    var time_in_min = Math.ceil( action_object.book_duration / (1000*60) );
    var price_so_far = time_in_min * (action_object.current_price/60);

    action_object.reservation_paid = price_so_far;

    // update ovaj action sa novim informacijama
    query_edit_park_action( action_object._id, action_object )
    .then(
    function(result) {
      saved_action = result.action;
      // oduzmi lovu useru iz mjesečne pretplate ili iz balansa
      return update_user_products(pp_sifra, user_number, price_so_far, saved_action);
    })
    .then(
    function(updated_product) {
        return pp_controller.query_park_places({ pp_sifra: pp_sifra }) 
    })
    .then(
    function(pp_result) {
      park_data = pp_result.park_places[0];
      return save_new_available_state( saved_action, 1, park_data );
    })
    .then(
    function() {
      global.exp_bookings.splice(0, 1);
      if ( global.exp_bookings.length > 0 ) run_exp_book();
      console.log( 'BOOKING EXPIRED FOR BOOKING ACTION ID: ' + action_object._id ); 
    })
    .catch(
    function(error) {
      
      console.error( 'ERROR WHEN UPDATING EXPIRATION FOR ACTION ID: ' + action_object._id ); 
      console.error(error);
      
      global.exp_bookings.splice(0, 1);
      if ( global.exp_bookings.length > 0 ) run_exp_book();
      
    });  
    
  // kraj ako je prošlo vrijeme  
    
  } 
  else {
    // ako je booking još uvijek u intervalu koji je user odredio onda KRENI DALJE !!!!
    // odreži prvi action item i nastavi dalje
    global.exp_bookings.splice(0, 1);
    if ( global.exp_bookings.length > 0 ) run_exp_book();
    
  };  
  

};


global.exp_bookings = [];

function set_expired_bookings() {

  global.exp_bookings_interval_id = setInterval( function() {
    
    var book_query = {
      pp_sifra: { $ne: null },
      status: 'booked',
      reservation_from_time: { $ne: null },
      reservation_to_time: null,
    };
    
    query_active_booking(book_query)
    .then(
    function(result) {
      
      if ( result.actions.length > 0 ) console.log(' PRONAŠAO BOOKINGA U EXP PROVJERI: ' + result.actions.length );
      
      // SAMO AKO JE NAŠAO BOOKINGE I AKO JE ZAVRŠIO SA AŽURIRANJEM PROŠLOG ARRAYA BOOKINGA !!!!!!!!!!
      if ( result.actions.length > 0 && global.exp_bookings.length == 0 ) {

        // save sve bookinge u globalni array !!!!!
        global.exp_bookings = result.actions;
        
        run_exp_book();
        
      }; // ako je našao actions i ako je završio sa ažuriranjem prošlog arraya
        
    })
    .catch(
    function(response) {
      console.log('DOŠLO JE DO GREŠKE U PRETRAŽIVANJU EXP BOOKINGA');
      console.error(response);
      // ako sam dobio poruku da je topology uništen to znači da je konekcija s bazom prekinuta
      // ... pa zato moram napraviti novu konekciju
      if ( response.err.message.indexOf("Topology was destroyed") > -1 ) {
        global.conn_to_db();
      };
    })
    
  }, 1000*60); // provjerava exp booking svake minute !!!
  
  
  
};

clearInterval( global.exp_bookings_interval_id );
global.exp_bookings_interval_id = null;

set_expired_bookings();


function send_info_mail(to_mail) {
  
    
        var transporter = nodemailer.createTransport(global.we_mail_setup);
 

        var email_user_name = user_object.name;
        var html = GENERATE_MAIL.generate(link, email_user_name).html;

        // setup email data
        var mailOptions = {
            from: `"WePark App" <${global.we_email_address}>`, // sender address
            to: to_mail, // list of receivers
            subject: 'WePark', // Subject line
            html:  html // html body
        };

          // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            
            res.json({ 
              success: false, 
              msg: "User nije spremljen!!! Problem u slanju maila  za konfirmaciju !!!!",
              error: error
            });
          
          };
          
          // add new email id to user object
          user_object.welcome_email_id = welcome_email_id;
          
          var new_user = new User(user_object);
          // save to DB
          new_user.save(function(err, user) {
            if (err) res.send(err);
            
            //console.log('Message %s sent: %s', info.messageId, info.response);
            
            // create a token for new user !!!!
            
            var user_copy = convert_mongoose_object_to_js_object(user);
            
            if ( user && user_copy.roles && user_copy.roles.we_app_user ) cit_pass_timeout = 720 * 1000; // to je  točno 500 dana
           
            
            var sign_obj = {
              _id: user_copy._id,
              hash: user_copy.hash
            };
           
            
            var token = jwt.sign(sign_obj, 'super_tajna', {
              expiresIn : 60 * cit_pass_timeout // expires in 30 min
            });

            // return user info including token as JSON
            
            // using now +  (expiresIn miliseconds) minus 10 sec just to be sure I'm in proper interval

            /* ----------------------- HERE IS RESPONSE FOR USER SAVE !!!!!  ----------------------- */

            
            user_copy.user_id = user._id;
            user_copy.success = true;
            user_copy.msg = 'Enjoy your token!';
            user_copy.token = token;
            user_copy.token_expiration = Date.now() + (1000 * 60 * cit_pass_timeout) - 10000;

            res.json(user_copy);

          }); // end of user save  
            
        }); // end of send welcome email

        
  
  
}; // kraj moje funkcije spremi usera






/*


/////////////////////////////////// BATCH IMPORTITANJE ///////////////////////////////////
/////////////////////////////////// BATCH IMPORTITANJE ///////////////////////////////////
/////////////////////////////////// BATCH IMPORTITANJE ///////////////////////////////////

var actions_file = require('./import_actions');

var park_actions_import = actions_file.generate_array();

var import_length = park_actions_import.length;

var saved_actions_sifre = [];

exports.import_actions = function(req, res) {

  
  var spremljeno_count = 0;
  
  park_actions_import.forEach(function(action_object, action_index) {
    
    
    var alreday_saved = false;
    park_actions_import.forEach(function(saved_id, saved_index) {
      if ( saved_id == action_object.sifra  ) alreday_saved = true;
    });
    
    if ( alreday_saved == false ) {

      var new_park_action = new ParkAction(action_object);
      new_park_action.save(function(err, park_action) {
        if (err) {
          console.error(' greska u spremanju actiona sa sifrom: ' + action_object.sifra )
        } else {

          spremljeno_count += 1;
          console.log( 'spremljeno:  ' + spremljeno_count + '/' + import_length);
          saved_actions_sifre.push(action_object.sifra);

        };

      }); // end of spremi parkiralište

    } else {
      
      console.log('već sam spremio: ' + action_object.sifra );
      
    };
    
  });

}; 

/////////////////////////////////// BATCH IMPORTITANJE ///////////////////////////////////
/////////////////////////////////// BATCH IMPORTITANJE ///////////////////////////////////
/////////////////////////////////// BATCH IMPORTITANJE ///////////////////////////////////


*/

// KRAJ SPREMI PARK PLACE 



exports.edit_park_action = function(req, res) {
  
  
  query_edit_park_action(req.params.park_action_id, req.body)
    .then(
      function(result) { res.json(result) },
      function(error) { res.json(error) }
    );
  
};


function query_edit_park_action(arg_id, query) {
  
  
  return new Promise( function( resolve, reject ) {

    ParkAction.findOneAndUpdate(
      { _id: arg_id },
      query,
      { new: true }, function(err, saved_action) {
      if (err) reject({ success: false, msg: "Park action not updated!", err: err});
        
      var clean_action = convert_mongoose_object_to_js_object(saved_action);
      resolve({ success: true, msg: "Park action updated!", action: clean_action });
    });
  
  });  
    
};

exports.query_edit_park_action = query_edit_park_action;


/// ovo je za tablicu na admin sučelju i nema veze sa app-om na mobitelu
exports.get_customer_park_actions = function(req, res) {
  
  var page = req.body.page;
  var limit = req.body.limit;
  
  var skip = (page - 1) * limit;
  
 
  ParkAction.find({ user_number: Number(req.params.user_num) })
  .sort({ reservation_from_time: -1, from_time: -1 })
  .skip(skip)
  .limit(limit)
  .exec(function(err, actions) {
    if (err) res.send(err);
    res.json(actions);
  });
  
};



exports.get_active_booking_or_parking = function(req, res) {

  var pp_sifra = req.params.pp_sifra;
  var user_number = Number(req.params.user_num);


  if ( pp_sifra == 'all' ) pp_sifra = { $ne: null };

  var book_query = {
    pp_sifra: pp_sifra,
    user_number: user_number,
    status: 'booked',
    reservation_from_time: { $ne: null },
    reservation_to_time: null
  };

  // pretraži sve parkinge od ovog usera koji imaju početno vrijeme ali nemaju završno vrijeme
  var park_query = {
    pp_sifra: pp_sifra,
    user_number: user_number,
    status: 'parked',
    from_time: { $ne: null },
    to_time: null
  };
  
  var active_book_park = {
    success: true,
    booking: [],
    parking: []
  };
  
  query_active_booking(book_query)
  .then(
  function(result) { 
    if ( result.actions.length > 0 ) {
      
      result.actions.forEach(function(action_obj, action_index) {
        active_book_park.booking.push(action_obj);
      });  
  
    };
    
    return query_active_parking(park_query);
  })
  .then(
  function( result ) {
    if ( result.actions.length > 0 ) {
      result.actions.forEach(function(action_obj, action_index) {
        active_book_park.parking.push(action_obj);
      });  
    };
      
    res.json( active_book_park ); 
    
  })
  .catch(
  function(error) {
    res.json(error);
  })
  
};


function query_active_booking(query) {
  
  
  return new Promise(function(resolve, reject) {

    ParkAction.find(query)
    .exec(function(err, actions) {

      if (err) {
        reject({ success: false, msg: 'greška prilikom query active booking', err: err });
      }
      else if (actions.length > 1 && typeof query.pp_sifra.$ne == "undefined" ) { 
        // ako postoji više bookinga ali ja ih NISAM TRAŽIO više od jendnoga tj za sve parkinge
        // nego samo za jedan parking
        reject({ success: false, msg: 'postoji_vise_bookinga' });
      }
      else {
                
        var clean_actions_arr = [];
        actions.forEach(function(action_obj, action_index) {
          var clean_action = convert_mongoose_object_to_js_object( action_obj );
          clean_actions_arr.push(clean_action);
        });
        resolve({ success: true, msg: 'lista rezerviranih parkinga', actions: clean_actions_arr })
      };

    });

    
  });    
    
};
exports.query_active_booking = query_active_booking;



exports.get_active_parking = function(req, res) {
  
  var pp_sifra = req.params.pp_sifra;
  var user_number = Number(req.params.user_num);
  
  // pretraži sve parkinge od ovog usera koji imaju početno vrijeme ali nemaju završno vrijeme
  var new_query = {
    pp_sifra: pp_sifra,
    user_number: user_number,
    from_time: { $ne: null },
    to_time: null
  };
  
  
  query_active_parking(new_query).then(
    function(result) { res.json(result) },
    function(error) { res.json(error); }
  );  

};


function query_active_parking(query) {

  return new Promise(function(resolve, reject) {
    // pretraži sve parkinge koji imaju početno vrijeme ali nemaju završno vrijeme
    ParkAction.find(query)
    .exec(function(err, actions) {

      if (err) {
        reject({ success: false, msg: 'greška prilikom get_active_parking', err: err });

      } else if (actions.length > 1 && typeof query.pp_sifra.$ne == "undefined" ) {      
        reject({ success: false, msg: 'popup_postoji_vise_parkinga' });

      } else { 
        
        var clean_actions_arr = [];
        actions.forEach(function(action_obj, action_index) {
          var clean_action = convert_mongoose_object_to_js_object( action_obj );
          clean_actions_arr.push(clean_action);
        });  
        
        resolve({ success: true, msg: 'objekt aktivnih parkinga', actions: clean_actions_arr });

      };

    })
  
  }); // end prom  
  
};
exports.query_active_parking = query_active_parking;



global.add_visited_pp_to_users = function() {
  
    User.find( {}, 
    function(err, all_users) {
      if (err) {
        console.log('greška kod pretrage usera unutar add visited pp func !!!') 
      };
      
      
      var all_clean_users = [];
      if ( all_users.length > 0 ) {
        
        $.each(all_users, function( user_index, user ) {
          var clean_user = convert_mongoose_object_to_js_object(user);
          all_clean_users.push(clean_user);
        });
       
        $.each(all_clean_users, function( clean_user_index, clean_user ) {
          
          ParkAction.find( {user_number: clean_user.user_number }, 
            function(action_err, all_user_actions) {
            
            if (action_err) {
              console.log('graška prilikom find actions za usera : ' + clean_user.email );
            } else {

              var all_visited_pp = [];
              var clean_actions = [];
              if ( all_user_actions.length > 0 ) {

                $.each(all_user_actions, function( action_index, action ) {
                  var clean_act = convert_mongoose_object_to_js_object(action);
                  clean_actions.push(clean_act);
                });

                $.each(clean_actions, function( act_index, c_act ) {
                  if ( $.inArray( c_act.pp_sifra, all_visited_pp ) == -1 ) {
                    all_visited_pp.push(c_act.pp_sifra); 
                  };
                });

                User.findOneAndUpdate(
                  { user_number: clean_user.user_number },
                  { $set: { visited_pp: all_visited_pp } },
                  {new: true}, 
                  function(err, user) {
                    if (err) {
                      console.log('graška prilikom update-a za : ' + all_visited_pp + ' kod usera: ' + clean_user.email );
                    } else {
                      console.log('dodao sam visited_pp: ' + all_visited_pp + ' u usera: ' + clean_user.email );
                    }
                  });      

              }; // ako postoje user actions

            }; // kraj ako postoje akcije od usra
            
          }); // end find all user actions
          
        }); // end loop clean users
        
      }; // ako je našao usera 
    
  }); // end of User.findOne
  
  
}; // kraj func add visited pp





function get_time_as_hh_mm_ss(arg_time, from_or_to) {
  
  var time = arg_time;
  // pošto svako početno vrijeme povećavam za 200 milisekundi 
  // sada kada vraćam vrijeme nazad u string moram to dodati nazad !!!!!
  if (from_or_to == 'to') time = time + 200;
  
  var sat = parseInt( time/(60*60*1000) );
  var modulus_sat = time/(60*60*1000) - sat;
  
  var min = parseInt( modulus_sat*60 );
  var modulus_min = modulus_sat*60 - min;
  
  var sek_string = zaokruziIformatirajIznos(modulus_min*60, 0);
  var sek = parseInt(sek_string) || 0;
  if ( sek == 60 ) sek = 59;
  
  if ( String(sat).length < 2 ) sat = '0' + sat;
  if ( String(min).length < 2 ) min = '0' + min;
  if ( String(sek).length < 2 ) sek = '0' + sek;
   
  var time_string = sat + ':' + min + ':' + sek;
  
  return time_string;
  
};








function popup_closest_open_time(park) {


  park.pp_prices.sort( window.by('pp_price_time_from', false, Number ) );

  var time_stamp = Date.now();


  var next_day = 0;
  var add_days = 0;
  var opens_in_time = null;
  var price_already_found = false;

  // loop za sljedećih 100 dana ... valjda će se parkiralište otvoriti za to vrijeme :)
  for ( next_day = 0; next_day <= 100; next_day++ ) {

      var zero_time_object = create_ms_at_zero_and_week_day(next_day);
      var ms_at_zero = zero_time_object.ms_at_zero;
      var week_day = zero_time_object.week_day;

      if ( price_already_found == false ) {

        // prvo tražim intervale sa specifičnim danom
        // intervali sa specifičnim danom IMAJU PRIORITET !!!!!
        $.each( park.pp_prices, function( pp_price_index, price) {
          if ( 
            time_stamp < (ms_at_zero + price.pp_price_time_from) 
              && 
            week_day == Number(price.pp_price_day)
          ) {
            price_already_found = true;
            add_days = next_day;
            opens_in_time = (ms_at_zero + price.pp_price_time_from) - time_stamp;
          };
        });

        // onda tražim intervale koji nemaju specifični dan

        $.each( park.pp_prices, function( pp_price_index, price) {
          if ( time_stamp < (ms_at_zero + price.pp_price_time_from) ) {
            price_already_found = true;
            add_days = next_day;
            opens_in_time = (ms_at_zero + price.pp_price_time_from) - time_stamp;
          };
        });
        
      };

  }; // kraj loopa za dodavanje novog dana

  var user_lang = global.all_user_langs[user_number] || 'hr';
  
  var popup_text = 
`
<h2 id="closest_open_time_title">${ WE_I18N.trans( 'closest_open_time_title', user_lang ) }</h5>
<p>
<span id="closest_open_time_text">${WE_I18N.trans( 'closest_open_time_text', user_lang ) }</span><br>
${ add_days > 0 ? ( add_days + 'd & ') : ''}
${ opens_in_time !== null ? get_time_as_hh_mm_ss(opens_in_time) : '???' }
<br>
</p>
`;
  
return popup_text;
  
};




exports.picam_enter_pp = function(req, res) {

  var user_number = Number(req.params.user_number);
  var ramp_name = String(req.params.ramp_name);
  var just_check_access = String(req.params.just_check_access);
  var arg_reg = String(req.params.reg);
  
  var park_place = null;
  var user_obj = null;
   
  var to_user_only = null;
  
  if ( 
    !just_check_access              || 
    just_check_access == 'null'     ||
    just_check_access == ''         ||
    just_check_access == 'none'     ||
    just_check_access == 'undefined'
  ) {   
    just_check_access = null;
  };
  
  
  
  if ( 
    !arg_reg               || 
    arg_reg == 'null'      ||
    arg_reg == 'none'      ||
    arg_reg == ''          ||
    arg_reg == 'undefined'
  ) {
    arg_reg = null;
  };
  
  
  var fino_ime_rampe = ramp_name.replace('_', ' ').toUpperCase();
  var user_lang = global.all_user_langs[user_number] || 'hr';
  var email_subject = `PORUKA OD ${fino_ime_rampe} !!!!`;
  
  var enter_park_response = null;
  
  var email_body = WE_I18N.trans( 'popup_greska_prilikom_parkinga', user_lang );
  
  global.query_picam_enter_pp(user_number, ramp_name, just_check_access, arg_reg).then(
    function(result) { 
      
      enter_park_response = result;
      
      
      // AKO JE OVO PRVI POZIV OD PICAM-a tj provjera jel može ući na parking !!!!
      // AKO JE OVO PRVI POZIV OD PICAM-a tj provjera jel može ući na parking !!!!
      // AKO JE OVO PRVI POZIV OD PICAM-a tj provjera jel može ući na parking !!!!
      
      if ( enter_park_response.success == false && just_check_access !== null ) {

        console.error('-------------- PICAM OPEN RAMP SUCCESS == FALSE !!!!!! ---------------');
        console.log(enter_park_response);
        
        if ( enter_park_response.msg == 'user_just_exited' ) {
          res.json(enter_park_response);
          return;    
        };
        
        
        // -------------------------- AKO IMA NEKA PORUKA
        if ( enter_park_response.msg && enter_park_response.msg.indexOf('popup_') > -1 ) {
          

          find_user_by_number(user_number)
          .then(
          function(user_result) {
            user_obj = user_result.user;
            return pp_controller.query_park_places({ ramps: ramp_name }) 
          })
          .then(
          function(pp_result) {
            
            park_place = pp_result.park_places[0];

            if ( enter_park_response.msg == 'popup_closest_open_time' ) {
              email_body = popup_closest_open_time(park_place);
              // dodajem result na kraju kao sakriveni komentar u HTML-u
              email_body = email_body + `<!-- ${ JSON.stringify(enter_park_response) } -->`;
              to_user_only = 'to_user_only';
            };
            
            if ( enter_park_response.msg == 'popup_nemate_lovu_ili_pretplatu' ) {
              to_user_only = 'to_user_only';
              email_body = WE_I18N.trans( 'popup_nemate_lovu_ili_pretplatu', user_lang );
              // dodajem result na kraju kao sakriveni komentar u HTML-u
              email_body = email_body + `<!-- ${ JSON.stringify(enter_park_response) } -->`;
            };
            
            
            if ( enter_park_response.msg == 'popup_nema_vise_mjesta' ) {
              to_user_only = 'to_user_only';
              email_body = WE_I18N.trans( 'popup_nema_vise_mjesta', user_lang );
              // dodajem result na kraju kao sakriveni komentar u HTML-u
              email_body = email_body + `<!-- ${ JSON.stringify(enter_park_response) } -->`;
            };
            
            
            if ( enter_park_response.msg == 'popup_postoji_vise_parkinga' ) {
              to_user_only = 'to_user_only';
              email_body = WE_I18N.trans( 'popup_postoji_vise_parkinga', user_lang );
              // dodajem result na kraju kao sakriveni komentar u HTML-u
              email_body = email_body + `<!-- ${ JSON.stringify(enter_park_response) } -->`;
            };            
            
            
            global.send_rampa_status_by_mail(email_subject, email_body, user_obj, to_user_only); 
            
          }); // kraj then-a nakon queryja za pp place
          
          // kraj ako je msg popup
        }
        else {
          
          // ako nema nikakvog msg u response onda pošalji generičku poruku da je greška
          email_body = WE_I18N.trans( 'popup_greska_prilikom_parkinga', user_lang );
          // dodajem result na kraju kao sakriveni komentar u HTML-u
          email_body = email_body + `<!-- ${ JSON.stringify(enter_park_response) } -->`;
          
          find_user_by_number(user_number)
          .then(
          function(user_result) {
            user_obj = user_result.user;
            global.send_rampa_status_by_mail(email_subject, email_body, user_obj); 
          });

        }; 
          
      }; // kraj ako je success FALSE  i radi se o just checking !!!!!
      
      // šaljem odgovor prema raspberryju tako da završim request !!!!!
      // RASPBERRYIJU JE SAMO BITNO DA .success == true
      // tako da nastavi sa otvaranjem rampe !!!!!
      res.json(result);
    
    },
    function(error) { 
      
      email_body = WE_I18N.trans( 'popup_greska_prilikom_parkinga', user_lang );
      
      // dodajem grešku na kraju kao sakriveni komentar u HTML-u
      email_body = email_body + `<!-- ${ JSON.stringify(error) } -->`;
      
      find_user_by_number(user_number)
      .then(
      function(result) {
        user_obj = result.user;
        global.send_rampa_status_by_mail(email_subject, email_body, user_obj); 
      });
      
      res.json(error);
      
    }); // kraj query picam enter pp  

}; // kraj func picam enter pp




function find_user_by_number(user_number) {
  
  return new Promise( function( resolve, reject ) {
    User.findOne({ user_number: user_number })
    .exec(function(err, user) {

      if (err) {
        reject({ success: false, msg: 'Greška kod pretrage usera', err: err });
        return;
      } else { 
        
        if ( !user ) {
          reject({ success: false, msg: 'Nije našao usera' });
          return;
        };
        
        var clean_user = convert_mongoose_object_to_js_object( user );
        resolve({ success: true, msg: 'nasao usera po broju', user: clean_user });

      };

    }); // kraj exec od user find
    
  });
  
};


global.find_user_by_number = find_user_by_number;


function find_current_tag(user, park) {
  
  return new Promise( function( tag_resolve, tag_reject ) {

  var found_customer_tag = null;
    
  if ( 
      user                             &&  
      user.customer_tags               &&
      $.isArray(user.customer_tags)    &&
      user.customer_tags.length > 0    &&
      park.special_places              &&
      Object.keys(park.special_places).length > 0  
  ) {
    
    // loop po svim special places ( KOJI JE OBJEKT ) unutar jednog parka
    $.each( park.special_places, function( place_tag, spec_place_obj ) {
      // loop da provjerim svaki specijal place sa svakim customer tagom ( KOJI JE ARRAY )
      $.each( user.customer_tags, function( tag_index, tag_obj ) {

        // ako jesam našao match onda ne traži dalje !!!!!!
        if ( found_customer_tag == null && place_tag == tag_obj.sifra ) {

          found_customer_tag = tag_obj.sifra;

          // ako je npr tele2 parking avail = nula
          // i ako ima mjesta na običnim parkinzima
          if (
            spec_place_obj.avail_out == 0 &&
            park.pp_available_out_num > 0
          ) {

            tag_reject({ success: false, msg: 'sms_zelis_biti_obican' });

          } else if ( spec_place_obj.avail_out > 0 ) {

            tag_resolve( found_customer_tag );

          };

        }; // kraj ifa ako je našao match


      }); // kraj loopa po customer tags

    });  // kraj loopa po park special places


  }; // kraj ako postoji u useru customer tags i ako postoji u parku special places 
    
  if ( found_customer_tag == null ) tag_resolve( found_customer_tag );
    
  }); // kraj promisa
  
}; // kraj find current tag 
global.find_current_tag = find_current_tag;


function get_user_status(cit_user, park) {
  
  
  var user_status = '';
    
    if ( 
      cit_user                           && /* ako postoji user*/
      cit_user.roles                     && /* ako postoji roles u useru */
      cit_user.roles.pp_vip              && /* ako postoji prop pp owner */ 
      $.isArray( cit_user.roles.pp_vip ) && /* ako je pp owner array */
      cit_user.roles.pp_vip.length > 0 /* ako array nije prazan */
    ) {
      
      if ( $.inArray( park.pp_sifra, cit_user.roles.pp_vip) > -1 ) user_status = 'vip';
    };
  
  
    
    if ( 
      cit_user                            &&
      cit_user.roles                      &&
      cit_user.roles.free_pp              &&
      $.isArray( cit_user.roles.free_pp ) &&
      cit_user.roles.free_pp.length > 0
    ) {
      
      if ( $.inArray( park.pp_sifra, cit_user.roles.free_pp) > -1 ) user_status = 'free';
      
    };  
  
  
    if ( 
      cit_user                             &&
      cit_user.roles                       &&
      cit_user.roles.super_admin
    ) {
      
      if ( cit_user.roles.super_admin == true ) user_status = 'super';
      
    };  
  
  
  return user_status;
  
};


function check_is_user_just_exited(user_number, park) {
  
  
    return new Promise(function(resolve, reject) {
 
    // provjeri jel user malo prije izašao  --- točnije u zadnjih 30 sekundi
    ParkAction.find({ 
      user_number: user_number,
      pp_sifra: park.pp_sifra,
      to_time: { $gt: (Date.now() - 1000*30) }
    })
    .exec(function(err, actions) {
      

      if (err) {
        reject({ success: false, poruka: 'greška prilikom check_is_user_just_exited', err: err });
        return;
      };
      
      if (actions.length > 1 ) {      
        resolve({ exiting: true, msg: 'user_just_exited' });
      } else {
        resolve({ exiting: false, poruka: 'user_is_not_exiting' });
      };

    }); // end of query just exited
      
  }); // end of promise !!!
  
  
};



global.query_picam_enter_pp = function (user_number, ramp_name, just_check_access, arg_reg ) {

  return new Promise( function( resolve_global, reject_global ) {
  
    var user = null;
    var enter_or_exit = null;
    var park = null;
    var user_current_tag = null;
    
    find_user_by_number(user_number)
    .then(
    function(result) {
      user = result.user;
      
      return pp_controller.query_park_places({ ramps: ramp_name }) 
    })
    .then(
    function(result) {
      
      park = result.park_places[0];
      
      return find_current_tag(user, park);
    })
    .then(
    function(resolved_current_tag) {
      
      user_current_tag = resolved_current_tag;
      
      return check_is_user_just_exited(user.user_number, park);
    
    })
    .then(
    function(just_exited_result) {
      
      // ako je auto izlazi sa parkirališta, ali ga kamera snimi od straga ( stražnju regu)
      // onda prekini ovaj proces sa success = false
      if ( just_exited_result.exiting == true && just_check_access ) {
        resolve_global({success: false, msg: 'user_just_exited' });
        return;
      };
      
      
      if ( just_check_access ) {
        
        var query_obj = {
          current_tag: user_current_tag,
          user_status: get_user_status(park),
          user_number: user_number,
          indoor: false,
          pp_sifra: park.pp_sifra,
        };

        return query_check_park(query_obj);
        
        
      } 
      else {
      
        var time_stamp = Date.now();
        var action_sifra = Date.now() + '-' + cit_rand_min_max(1, 10000);
        
        var action_obj = {

          //* postojeci fieldovi  /  
          sifra : action_sifra,
          from_time: time_stamp,
          to_time: null,
          paid: null,  
          pp_sifra : String(park.pp_sifra),
          user_number: user.user_number,
          was_reserved_id: null,
          mobile_from_time: null,

          //* moji fieldovi  /

          status: 'parked',
          user_set_book_duration: null,

          reservation_from_time: null,
          reservation_to_time: null,
          reservation_expired: null,
          reservation_canceled: null,
          reservation_paid: null,

          current_price: park.pp_current_price,

          pack_type: null,
          duration: null,
          revenue: null,
          owner_revenue: null,
          currency: 'kn',

          indoor: false,

          current_tag: user_current_tag,

          user_status: get_user_status(park),
          
          reg: (arg_reg || null)

        };

        return query_enter_park(action_obj);
      };
      
    })
    
    .then(
    function(result) {
      resolve_global(result);
      
    })
    .catch(
    function(fail) {
      
      console.error('---------------- PICAM OPEN RAMP FAIL !!!!! -----------------');
      console.log(fail);
      
      if ( fail.msg && fail.msg == 'sms_zelis_biti_obican' ) {
        
        ///// POŠALJI SMS POŠTO JE OVO ULAZAK SA KAMEROM
        
      };
      
      reject_global(fail);
    });
    
  }); // end uwry pi open ramp 
  
};
exports.query_picam_enter_pp = global.query_picam_enter_pp;


// todo --- nije jos gotovo !!!!!!!
exports.actions_mjesecno = function(req, res) {

  var start_time = Number(req.params.start);
  
  ParkAction.find({ 
    
    $or: [ 
      { from_time: { $gte: start_time } },
      { reservation_from_time: { $gte: start_time } } 
    ]
    
  })
  .exec(function(err, actions) {
    
    
    var clean_actions = [];
    $.each(actions, function(action_index, act) {
      var clean_act = convert_mongoose_object_to_js_object(act);
      clean_actions.push(clean_act);
    });
    
    
    User.find({})
    .select({ email: 1, user_number: 1 })
    .exec(function(err, users) {
      
      if (err) res.send(err);

      var clean_users = [];
      $.each(users, function(user_index, user) {
        var clean_user = convert_mongoose_object_to_js_object(user);
        clean_users.push(clean_user);
      });

      $.each(clean_actions, function(action_index, act) {
        var act_user_num = act.user_number;
        
        $.each(clean_users, function(user_index, user) {
          if ( user.user_number == act_user_num ) clean_actions[action_index].user_number = user.email;
        });
      });
      
      
      res.json(clean_actions);
    });


  });

};
  
