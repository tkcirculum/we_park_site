  
function func_call_button(lang) {

var call_button =  {  
hr: function() {  

var btn = 
`
Kliknite broj ispod za pomoć<br>
<a href="tel:00385923133015" 
style="padding: 5px;
text-decoration: none;
color: black;
background-color: #fff;
margin-top: 10px;
display: block;
border-radius: 20px; 
font-size: 20px;">00385 92 3133 015</a>

`;   

return btn;  

},
en: function() {  

var btn = 
`
Click number below to call support<br>
<a href="tel:00385923133015" 
style="padding: 5px;
text-decoration: none;
color: black;
background-color: #fff;
margin-top: 10px;
display: block;
border-radius: 20px;
font-size: 20px;">00385 92 3133 015</a>

`;   

return btn;  

},  
};

return call_button[lang]();  
  
}; // kraj funkcije get_call_button


function trans(selector, lang, data) {
  
var  lang_object = {
/* ---------------------- PAY CARD PAGE TO SELECT TOKEN  ---------------------- */
loader_text_ws_pay: {
hr: `SPAJANJE NA WSPAY`,
en: `CONNECTING TO WSPAY`,
},
i_want_token_span: {
hr: `ŽELIM KORISTITI TOKEN`,
en: `I WANT TO USE TOKEN`,
},
  
wspay_explanation_text: {
hr: `S TOKEN opcijom, samo kod prve kupnje potrebno je upisati podatke kartice. Svaki sljedeći put možete direktno plaćati iz aplikacije.<br><br> 
<b>Pritiskom na tipku "IDI NA WSPAY" pristajete na uvjete kupnje.</b><br>`,

en: `With TOKEN option, only on your first purchase you are required to enter card information. Every subsequent time you can pay directly from the app.<br><br> 
<b>By clicking "GO TO WSPAY" you agree to the purchase terms.</b><br>`,
},    
  
  
new_token_system: {
hr: `Na snagu stupa stroga regulativa PSD2 po kojoj sve transakcije koje inicira kupac moraju biti osigurane snažnom provjerom korisnika. To znači da kada kupac tokenizira karticu i inicira ponovnu kupovinu kod istog trgovca, mora upisati CVV broj sa poleđine kartice i mora proći 3DS sigurnosnu provjeru kartice.<br>`,

en: `Strict PSD2 regulations come into force, according to which all transactions initiated by the customer must be secured by a strong user check. This means that when a customer is using card token and initiates a repurchase from the same merchant, customer must enter CVV number on the back of the card and must pass a 3DS card security check.<br>`,
},    
  
  
wspay_cekamo_odobrenje: {
hr: 
`
Nažalost još uvijek čekamo papirologiju za pokretanje kartičnog poslovanja :(<br>
Čim dobijemo odobrenje odmah javljamo!<br>
U međuvremenu, možete nadoplatiti račun ili kupiti pretplatu preko vouchera.<br>
Za sve detalje nazovite:<br>
<a href="tel:00385923133015" 
style="padding: 5px;
text-decoration: none;
color: black;
background-color: #fff;
margin-top: 10px;
display: block;
border-radius: 20px; 
font-size: 20px;">00385 92 3133 015</a>
<br>
<br>

`,

en: 
`
Unfortunately we are still waiting for the paperwork to start the credit card payment :(<br>
As soon as we get approval we will notify you immediately!<br>
In the meantime, you can increase  your balance or buy a subscription via voucher.<br>
For details call:<br>
<a href="tel:00385923133015" 
style="padding: 5px;
text-decoration: none;
color: black;
background-color: #fff;
margin-top: 10px;
display: block;
border-radius: 20px; 
font-size: 20px;">00385 92 3133 015</a>
<br>
<br>

`,
},    
  

  
  
goto_ws_pay_site_btn: {
hr: `IDI NA WSPAY`,
en: `GO TO WSPAY`,
},
  
  
  
new_token_btn: {
hr: `PLATI`,
en: `PAY`,
},  
  
  
wspay_obavezni_podaci: {
hr: 
`

<h4>Sve o WSPay<sup>TM</sup> sustavu</h4>
<p>
WSPay je siguran sustav za online plaćanje, plaćanje u realnom vremenu, kreditnim i debitnim karticama te drugim načinima plaćanja. WSPay kupcu i trgovcu osiguravaju siguran upis i prijenos upisanih podataka o karticama što podvrđuje i PCI DSS certifikat koji WSPay ima. WSPay koristi SSL certifikat 256 bitne enkripcije te TLS 1.2 kriptografski protokol kao najviše stupnjeve zaštite kod upisa i prijenosa podataka.
</p>

<h4>Currency conversion</h4>
<p>
  All payments will be effected in Croatian currency. The charged amount on your credit card account is converted into your local currency according to the exchange rate of credit card associations.
</p>

<h4>Kontakt</h4>
<p style="text-align: center;">
  
<b>Odgovorna osoba:</b> Karlo Starčević (direktor)<br>
<b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
<b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
<br>

<b>WePark smart parking d.o.o.</b><br>
<b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
<b>OFFICE:</b>&nbsp;Janka Rakuše 1, Zagreb<br>
<b>WEB:</b>&nbsp;www.wepark.eu<br>
  
  
<b>Nadležni sud:</b>&nbsp;Trgovački sud u Zagrebu<br>  
<b>MBS:</b>&nbsp;081131571<br>  
<b>OIB:</b>&nbsp;77372456863<br>    
  
</p>

<h4>Proizvodi i usluge</h4>
<p>
  WePark smart parking d.o.o. ( u daljnjem tekstu WePark ) putem WSPAY servisa omogućava plaćanje dvije vrste usluge:<br>
  1. Nadoplata računa za plaćanje parkinga<br>
  2. Plaćanje pretplate za korištenje parkinga<br>
</p>

<h4>Uvjeti prodaje i poslovanja</h4>
<p>
1. Korisnik može putem mobilne aplikacije i WSPAY servisa nadoplatiti račun i platiti pretplatu.<br>
2. Nakon uspješne transakcije korisnik će automatski imati dodatni iznos na stanju računa ili novu pretplatu.<br>

<br>
</p>

<p style="text-align: center;">
Za više detalja, molimo vas pogledajte
  <br>
<a href="https://wepark.circulum.it:9000/TermsofUse.html" target="_blank" style="color: #2ffeff;">Uvjete poslovanja</a>
</p>


<h4>Izjava o prikupljanju i korištenju osobnih podataka</h4>
<p>
  WePark obavezuje se pružati zaštitu osobnim podacima kupaca, na način da prikuplja samo nužne, osnovne podatke o kupcima/ korisnicima koji su nužni za ispunjenje naših obveza; informira kupce o načinu korištenja prikupljenih podataka, redovito daje kupcima mogućnost izbora o upotrebi njihovih podataka, uključujući mogućnost odluke žele li ili ne da se njihovo ime ukloni s lista koje se koriste za marketinške kampanje. Svi se podaci o korisnicima strogo čuvaju i dostupni su samo djelatnicima kojima su ti podaci nužni za obavljanje posla. Svi djelatnici WePark tvrtke i poslovni partneri odgovorni su za poštivanje načela zaštite privatnosti.
</p>

<h4>Način zaštite povjerljivih podataka</h4>
<p>
  Unos i prijenos osobnih podataka i podataka o broju kreditne kartice zaštićen je SSL protokolom 256-bitne enkripcije koju osigurava WSpay™ sustav za on line autorizaciju kreditnih karica. Autorizacija i naplata kreditnih kartica radi se korištenjem sustava WSpay™ za autorizaciju i naplatu kartica u realnom vremenu.
</p>

<div id="ikone_kartica" style="margin: 20px 0 0; border-radius: 10px; padding: 5px;">
  
  <a href="https://www.americanexpress.hr/" target="_blank"><img src="img/ikone_kartica/AmericanExpress50.jpg" style="border-radius: 4px;" /></a>
  <a href="https://www.visa.com.hr/" target="_blank"><img src="img/ikone_kartica/Visa50.gif" style="border-radius: 4px;"/></a>
  <a href="https://www.mastercard.com/hr/" target="_blank"><img src="img/ikone_kartica/MasterCard50.gif" style="border-radius: 8px;"/></a>
  <a href="https://www.mastercard.hr/hr-hr/consumers/find-card-products/debit-cards/maestro-debit.html" target="_blank"><img src="img/ikone_kartica/maestro50.gif" style="border-radius: 8px;"/></a>
  <a href="https://www.discover.com/" target="_blank"><img src="img/ikone_kartica/discover_50.gif" style="border-radius: 4px;"/></a>
  
</div>



<div id="ikone_sigurnosti" style="margin: 20px 0 30px; border-radius: 10px; padding: 5px;">
  
  <img src="img/ikone_kartica/WSPayPassLogo.png" style="border-radius: 4px;" />
  <img src="img/ikone_kartica/pci-dss-certified.png" style="border-radius: 4px;" />
  <img src="img/ikone_kartica/wsPay-GDPR-compliant.png" style="border-radius: 4px;" />
  <a href="https://www.wspay.info/mastercard-securecode.html" target="_blank"><img src="img/ikone_kartica/mastercard-identity-check.png" style="border-radius: 4px;" /></a>
  <a href="https://www.wspay.info/verified-by-visa.html" target="_blank"><img src="img/ikone_kartica/visa-secure.png" style="border-radius: 4px;" /></a>
</div>


`,
  
  
  
  
  
  
en: 
`

<h4>All about WSpay<sup>TM</sup> payment</h4>
<p>
WSPay is a secure system for online real-time payment, credit and debit cards and other payment methods. WSPay provides the buyer and merchant with secure entry and transfer of card data, which is confirmed by the PCI DSS certificate that WSPay holds. WSPay uses 256 bit encryption SSL certification and TLS 1.2 cryptographic protocol as the highest levels of data entry and transfer protection.
</p>

<h4>Currency conversion</h4>
<p>
  All payments will be effected in Croatian currency. The charged amount on your credit card account is converted into your local currency according to the exchange rate of credit card associations.
</p>

<h4>About Us</h4>
<p style="text-align: center">
  
<b>Person Responsible: </b>Karlo Starcevic (Director)<br>
<b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
<b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
<br>

<b>WePark smart parking d.o.o.</b><br>
<b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
<b>OFFICE:</b>&nbsp;Janka Rakuše 1, Zagreb<br>
<b>WEB:</b>&nbsp;www.wepark.eu<br>
  
  
<b>Jurisdiction:</b>&nbsp;Commercial Court of Zagreb<br>  
<b>MBS:</b>&nbsp;081131571<br>  
<b>VAT number:</b>&nbsp;HR77372456863<br>    
  
</p>

<h4>Products and services</h4>
<p>
  WePark smart parking d.o.o. (hereinafter referred to as WePark) through the WSPAY service provides the payment for two types of services:<br>
  1. Payment to increase a parking balance<br>
  2. Payment of a parking subscription<br>
</p>

<h4>Conditions of sale and business</h4>
<p>
1. The user can top up the account and pay the subscription through the mobile application and the WSPAY service.<br>
2. After a successful transaction, the user will automatically receive an additional amount on their account balance or a new subscription.<br>

<br>

</p>

<p style="text-align: center;">
For more details, please see
  <br>
<a href="https://wepark.circulum.it:9000/TermsofUse.html" target="_blank" style="color: #2ffeff;">Terms of Use</a>
</p>

<h4>Collection and use of personal information</h4>
<p>
  WePark is committed to protecting the personal information of customers by collecting only necessary, basic customer / customer information that is necessary to fulfill our obligations; informs customers about how they use the information they collect, regularly gives customers the choice of using their information, including the ability to decide whether or not their name should be removed from the lists used for marketing campaigns. All customer information is strictly kept and is only available to employees who need it. All WePark Company employees and business partners are responsible for compliance with the privacy principles.
</p>

<h4>Security of confidential information</h4>
<p>
  The entry and transfer of personal and credit card information is protected by the SSL protocol of 256-bit encryption provided by the WSpay ™ system for online credit card authorization. Credit card authorization and payment is done using the WSpay ™ system for real-time card authorization and payment.
</p>


<div id="ikone_kartica" style="margin: 20px 0 0; border-radius: 10px; padding: 5px;">
  
  <a href="https://www.americanexpress.hr/" target="_blank"><img src="img/ikone_kartica/AmericanExpress50.jpg" style="border-radius: 4px;" /></a>
  <a href="https://www.visa.com.hr/" target="_blank"><img src="img/ikone_kartica/Visa50.gif" style="border-radius: 4px;"/></a>
  <a href="https://www.mastercard.com/hr/" target="_blank"><img src="img/ikone_kartica/MasterCard50.gif" style="border-radius: 8px;"/></a>
  <a href="https://www.mastercard.hr/hr-hr/consumers/find-card-products/debit-cards/maestro-debit.html" target="_blank"><img src="img/ikone_kartica/maestro50.gif" style="border-radius: 8px;"/></a>
  <a href="https://www.discover.com/" target="_blank"><img src="img/ikone_kartica/discover_50.gif" style="border-radius: 4px;"/></a>
  
</div>



<div id="ikone_sigurnosti" style="margin: 20px 0 30px; border-radius: 10px; padding: 5px;">
  
  <img src="img/ikone_kartica/WSPayPassLogo.png" style="border-radius: 4px;" />
  <img src="img/ikone_kartica/pci-dss-certified.png" style="border-radius: 4px;" />
  <img src="img/ikone_kartica/wsPay-GDPR-compliant.png" style="border-radius: 4px;" />
  <a href="https://www.wspay.info/mastercard-securecode.html" target="_blank"><img src="img/ikone_kartica/mastercard-identity-check.png" style="border-radius: 4px;" /></a>
  <a href="https://www.wspay.info/verified-by-visa.html" target="_blank"><img src="img/ikone_kartica/visa-secure.png" style="border-radius: 4px;" /></a>
</div>

`,
},
  
    
cancel_payment_text: {
hr: 
`
<h3>Odustali ste od uplate</h3>
Zatvorite ovaj prozor kako bi ste nastavili koristiti WePark aplikaciju.<br>
Nadamo se da je sve u redu!<br>
${func_call_button('hr')}<br>

`,
en:
`
<h3>You have canceled your payment</h3>
Close this window to continue using the WePark application.<br>
We hope all is well!<br>
${func_call_button('en')}<br>

`,  
},     
    
error_payment_text: {
hr: 
`
<h3>Došlo je do greške!</h3>
Uplata nije bila uspješna.<br>
Molimo vas zatvorite ovaj prozor i probate ponovo.<br>
Provjerite točnost svih podataka koje ste upisali.<br>
${func_call_button('hr')}<br>

`,
en:
`
<h3>Error has occurred! </h3>
Payment was unsuccessful. <br>
Please close this window and try again.<br>
Check the accuracy of all the information you have entered. <br>
${func_call_button('en')}<br>

`,  
},     
  
success_payment_text: {
hr: 
`
<h3>Uplata je uspješna!</h3>
Račun će biti poslan na vašu email adresu.<br>
Ukoliko koristite TOKEN opciju, sljedeći put ćete moći platiti direktno iz WePark aplikacije!<br>
Zatvorite ovaj prozor, i uživajte u super parking mjestu :)<br>

`,
en:
`
<h3>Payment successful!</h3>
The bill will be sent to your email address. <br>
If you chose TOKEN option, next time you will be able to pay directly from WePark app! <br>
Close this window, and enjoy the great parking space :) <br>

`,  
},     
        
uspjesno_iskoristen_voucher: {
  hr: function(amount) { return `Uspješno je iskorišten voucher. Vaše stanje je uvećano za ${amount}kn.` },
  en: function(amount) { return `Voucher successfully validated. Your balance has been increased by ${amount}kn.` },
},
    
uspjesno_iskoristen_voucher_za_pretplatu: {
  hr: function(object) { 
    return `Uspješno ste iskoristili voucher za pretplatu na parkiralište ${object.pp_name} za sljedećih ${object.duration} dana!` 
  },
  en: function(object) { 
    return `You successfully validated a voucher to subscribe to parking lot ${object.pp_name} for next ${object.duration} days!` 
  },
},
    
nema_dovoljno_love_na_voucheru: {
  hr: function(object) { 
    return `Nemate dovoljan iznos na voucheru za ovu pretplatu. Iznos vouchera je ${object.iznos_vouchera}kn, a cijena pretplate je ${object.cijena_pretplate}kn!` 
  },
  en: function(object) { 
    return `You do not have sufficient funds on voucher for this subscription. Voucher amount is ${object.iznos_vouchera}kn, and the subscription price is ${object.cijena_pretplate}kn!` 
  },
},   
  
  
find_user_error: {
hr: 
`
<h3>Došlo je do greške!</h3>
Baza korisnika trenutno ne radi :(<br>

${func_call_button('hr')}<br>

`,
en:
`
<h3>User database error!</h3>
The customer database is currently down :(<br>

${func_call_button('en')}<br>

`,  
},  
  

  
  
find_user_error_samo_text: {
hr: 
`
Baza korisnika trenutno ne radi :(
`,
en:
`
The customer database is currently down :(

`,  
},  
  
no_user_found: {
hr: 
`
<h3>Došlo je do greške!</h3>
Aplikacija nije pronašla korisnika.<br>
${func_call_button('hr')}<br>
`,
en:
`
<h3>User database error!</h3>
We did not find this user in the database<br>
${func_call_button('en')}<br>

`,  
},  
    
  
no_user_found_samo_text: {
hr: 
`
Aplikacija nije pronašla korisnika.
`,
en:
`
We did not find this user in the database.

`,  
},  
  
closest_open_time_title: {
hr: `Sorry :( Parking je zatvoren!`,
en: `Sorry :( Parking is closed!`,
},
  
closest_open_time_text: {
hr: `Otvara se za:`,
en: 'Opens in:',
},  

popup_greska_prilikom_parkinga: {
hr:
`
<h2>UH OH !</h2>
<p style="margin: 0;">
Došlo je do greške na serveru prilikom ulaska u parking :(<br>
${func_call_button('hr')}<br>

</p>
`,
en:
`
<h2>UH OH !</h2>
<p style="margin: 0;">
There was an error on server while entering parking :( <br>
${func_call_button('en')}<br>
</p>
`,
},  
    
popup_nemate_lovu_ili_pretplatu: {
hr:
`
<h2>Nemate dovoljno sredstava</h2>
<p>
Nažalost nemate dovoljno sredstava na stanju ili niste pretplaćeni za ovo parkiralište!
<br>
</p>
`,
en:
`
<h2>Not enough funds!</h2>
<p>
Unfortunately you do not have enough funds or you have not subscribed to this parking lot!
<br>
</p>
`,
},
  
  
popup_nema_vise_mjesta: {
hr:
`
<h2>Sva mjesta su popunjena!</h2>
<p>
Upravo je zadnje mjesto zauzeto :(<br>
Probajte malo kasnije ...
<br>
</p>

`,
en:
`
<h2>Parking is full!</h2>
<p>
Just now the last lot was taken :( <br>
Try again a little later ...
<br>
</p>
`,
},  
  
popup_napon_too_low: {
hr:
`
<h2>Rampa je otvorena</h2>
<p>
Rampa je u otvorenom položaju zbog nedostatka električne struje.
<br>
</p>

`,
en:
`
<h2>Barrier is open</h2>
<p>
The ramp is in the open position due to lack of electricity.
<br>
</p>
`,
},  
  
  
popup_postoji_vise_parkinga: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške u sustavu :(<br>
Zabilježeno je da ste već ušli na ovaj parking!<br>
${func_call_button('hr')}<br>
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error :( <br>
It is recorded that you have already entered this parking lot!<br>
${func_call_button('en')}<br>
<br>
</p>
`,
}, 
  
  
  
  
  
}; // kraj lang objekta
  
  
  if ( typeof lang_object[selector][lang] == 'function') {
      
    if ( data ) return lang_object[selector][lang](data);
    if ( !data ) return lang_object[selector][lang]();
  } else {
    return lang_object[selector][lang];
  };

}; // traj trans funkcije

exports.trans = trans;
