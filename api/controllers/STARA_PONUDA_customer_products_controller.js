var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CustomerProduct = mongoose.model('CustomerProduct');
var ParkPlace = mongoose.model('ParkPlace');

var User = mongoose.model('User');

var Card = mongoose.model('Card');

var Discount = mongoose.model('Discount');


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


// ADDITIONAL OPTIONS FOR NATIVE JAVASCRIPT SORT ------- from http://jsfiddle.net/gfullam/sq9U7/  
window.by = function(path, reverse, primer, then) {
  var get = function (obj, path) {
      if (path) {
        path = path.split('.');
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        };
        return obj[path[len]];
      }
      return obj;
    },
    prime = function (obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function (a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B) ? -1 :
      (A > B) ? 1 :
      (typeof then === 'function') ? then(a, b) : 0
    ) * [1, -1][+!!reverse];
  };
};



function cit_rand_min_max(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
};

  

exports.save_customer_product = function(req, res) {

  var new_customer_product = new CustomerProduct(req.body);
  new_customer_product.save(function(err, customer_product) {
    if (err) res.send({ success: false, msg: "Customer product activation not saved!", err: err});
    res.json({ success: true, msg: "Customer product activation saved!", customer_product_id: customer_product._id});
  }); // end of spremi customer product activation  

}; // KRAJ SPREMI


function query_save_customer_debt(debt, pp_sifra, user_num) {
  
  
  if ( debt > 0 ) debt = -debt;
  
  var time_now = Date.now();
  var sifra = time_now + '-' + cit_rand_min_max(1, 10000);
  
  var customer_product_object = { 
 
  /* postojeci fieldovi  */    
  
  user_number: user_num,
  pp_sifra: pp_sifra,
  
  sifra: sifra,
  code: null,
  amount: null,
  
  desc : null,
  buy_time: time_now,
    
  is_free: null,

  /* moji fieldovi  */    
  price: null,
  type: 'debt',
  valid_from: null,
  valid_to: null,
  balance: debt,

};
  
  return new Promise(function(resolve, reject) {

    var new_customer_product = new CustomerProduct(customer_product_object);
    new_customer_product.save(function(err, customer_product) {
      if (err) reject({ success: false, msg: "Customer product activation not saved!", err: err});
      resolve({ success: true, msg: "Customer product activation saved!", debt: customer_product});
    }); // end of spremi customer product activation  

  });
};
exports.query_save_customer_debt = query_save_customer_debt;


exports.edit_customer_product = function(req, res) {
  
  CustomerProduct.findOneAndUpdate({ _id: req.params.customer_product_id }, req.body, { new: true }, function(err, customer_product) {
    if (err) res.send({ success: false, msg: "Customer product not updated!", err: err});
    res.json({ success: true, msg: "Customer product updated!", customer_product_id: customer_product._id});
  });
  
};



exports.get_customer_products_for_table = function(req, res) {
  
  var page = req.body.page;
  var limit = req.body.limit;
  
  var skip = (page - 1) * limit;
  
 
  CustomerProduct.find({ user_number: Number(req.params.user_num) })
  .sort({ buy_time: -1, valid_from: -1 })
  .skip(skip)
  .limit(limit)

  .exec(function(err, products) {
    if (err) res.send(err);
    res.json(products);
  });
  
};


function convert_mongoose_object_to_js_object(mongoose_object) {
  
  var js_object = mongoose_object.toJSON();
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};


function run_user_products_state(user_num) {
  
  return new Promise( function(resolve, reject) {
    
    
    
    if (
      user_num == 182 ||
      user_num == 184 ||
      user_num == 185 ||
      user_num == 410
    ) {
      user_num = 333;  // ovo je zajednički user za sve s&t korisnike
    };
    
    // ovo može biti bilo koji objekt  - tj može biti mjesečna pretplata i može biti kupovina vaučera
    CustomerProduct
    .find({ user_number: user_num })
    .sort({ buy_time: -1, valid_from: -1 })
    .exec(function(err, products) {
      if (err) {
        console.log('GREŠKA U DOBIVANJU CUSTOMER PRODUCTA');
        reject({ success: false, msg: "GREŠKA U DOBIVANJU CUSTOMER PRODUCTA", err: err})
        return;
      };
      

        var clean_products = [];
        var sum_of_voucher_balance = 0;
        var monthly_arr = [];
        var user_cards = [];

        if ( products.length > 0 ) {

          // prvo pretvori sve produkte iz mongo db formata u običan js array
          products.forEach(function(product_obj, product_index) {
            var clean_product_obj = convert_mongoose_object_to_js_object(product_obj);
            clean_products.push(clean_product_obj);
          });

          var time_stamp = Date.now();
          
          // po tom novom običnom array napravi loop
          clean_products.forEach(function(product_obj, product_index) {

            // zbroji svu lovu koju ima na svim productima koji su vaučer

            if ( product_obj.type == 'card' || product_obj.type == 'voucher' || product_obj.type == 'debt' ) {
              sum_of_voucher_balance += product_obj.balance;
            };

            // ako je monthly onda
            // pogledaj od-do datume jel se današnji datum nalazi unutar toga
            if ( 
              ( product_obj.type == 'monthly' || product_obj.type == 'vip' ) &&
              time_stamp > product_obj.valid_from                            &&
              time_stamp < product_obj.valid_to
            ) {
              monthly_arr.push(product_obj);
            };

          });
          
          Card.find({ user_number: user_num, token: null })
          .exec(function(err, cards) {
            if (err) {
              console.log('GREŠKA U DOBIVANJU USER CARDS');
              reject({ success: false, msg: "GREŠKA U DOBIVANJU CUSTOMER PRODUCTA", err: err})
              return;
            };
            
            
            if ( cards.length > 0 ) {
            
              // prvo pretvori sve produkte iz mongo db formata u običan js array
              cards.forEach(function(card_obj, card_index) {
                
                var clean_card_obj = convert_mongoose_object_to_js_object(card_obj);
                // u bazi držim prvo plaćanje koje ima card.Signature
                // ali i sva ostala plaćanja sa TOKENOM koja nemaju card.Signature !!!!
                // zato provjerim jel ima ovaj propery - ako ima to znači da je prvo plaćanje s karticom !!!
                if ( clean_card_obj.card.Signature ) {
                  user_cards.push({ sign: clean_card_obj.card.Signature, num: clean_card_obj.card.drop_list_name });
                };
              });
              
            };
   
            resolve({ success: true, balance_sum: sum_of_voucher_balance, monthly_arr: monthly_arr, cards: user_cards });
            
          });

        } else {
          // ako user nema niti jedan product
          console.log('nisam našao niti jedan user product');
          resolve({ success: true, balance_sum: 0, monthly_arr: [], cards: [] });
          
        };

        

    }); // kraj find produkte  tj vaučere i validne mjesečne karte
    
  }); // end of prom
  
}; 
exports.run_user_products_state = run_user_products_state;


exports.get_user_products_state = function(req, res) {
 
  var user_num = Number(req.params.user_num);
 
  run_user_products_state(user_num).then(
    function ( response ) { res.json(response) },
    function ( error ) { res.json(error) }
  );

};


exports.check_discount_code = function (req, res) {
  
  var dis_code = req.params.dis_code + '';
  
  Discount.findOne({ 'dis_code': dis_code })
  .exec(function(err, discount) {
    
    if (err) {
      console.log('GREŠKA U DOBIVANJU USER CARDS');
      res.json({ success: false, msg: "popup_discount_code_error", err: err });
      return;
    };


    if ( discount ) {
      var clean_discount = convert_mongoose_object_to_js_object(discount);
      var discount_perc = clean_discount.perc;
      
      if ( clean_discount.user_number !== null ) {
        res.json({ success: false, msg: "popup_discount_used" });
        return;
      };
      
      res.json({ success: true, msg: "popup_discount_code_success", discount: discount_perc });
    }
    else {
      res.json({ success: false, msg: "popup_discount_code_not_found" });
    };
  });
  
  
  
};


global.convert_start_time_to_by_time = function() {
  
    var obradjeno_produkata = 0;

    // ovo može biti bilo koji objekt  - tj može biti mjesečna pretplata i može biti kupovina vaučera
    CustomerProduct
    .find({})
    .exec(function(err, products) {
      if (err) {
        console.log('GREŠKA U DOBIVANJU CUSTOMER PRODUCTA KOD UPDATE TO buy_time');
        return;
      };
      

        var clean_products = [];
      
        if ( products.length > 0 ) {

          // prvo pretvori sve produkte iz mongo db formata u običan js array
          products.forEach(function(product_obj, product_index) {
            var clean_product_obj = convert_mongoose_object_to_js_object(product_obj);
            clean_products.push(clean_product_obj);
          });

          // po tom novom običnom array napravi loop
          clean_products.forEach(function(product_obj, product_index) {
            if ( !product_obj.buy_time ) {
              
              
              CustomerProduct.findOneAndUpdate(
                { _id: product_obj._id },
                { $set: { buy_time: product_obj.start_time } },
                { new: true }, function(err, customer_product) {
                if (err) {
                  console.log( "greška u zapisivanju produkta sa idjem: " + product_obj._id);
                  console.log(err)  
                } else {
                  obradjeno_produkata += 1;
                  console.log( "upravo obrađen item: " + obradjeno_produkata + '/1246');
                };
              });
            };
          });
          
        } else {
          console.log('nisam našao niti jedan user product');
        };

    }); // kraj find produkte  tj vaučere i validne mjesečne karte
  
}; 



global.vou_users =

  [
509, 
538, 
539, 
10043, 
10045, 
532, 
533, 
534, 
535, 
549, 
551   
  
];
  
/*  

TELE2 IDIJEVI 
  [
  655, 
666, 
667, 
10042, 
10047, 
10054, 
656, 
657, 
658, 
659, 
660, 
662, 
665, 
668, 
669, 
670, 
673, 
674, 
685, 
686, 
687, 
688, 
813, 
821, 
962
]
*/
  
/*  
[{"user_number":509},{"user_number":538},{"user_number":539},{"user_number":533},{"user_number":534},{"user_number":551},{"user_number":549},{"user_number":532},{"user_number":535},{"user_number":543},{"user_number":10043},{"user_number":10045}];
*/

/*
[{"user_number":686},{"user_number":656},{"user_number":687},{"user_number":665},{"user_number":657},{"user_number":668},{"user_number":667},{"user_number":666},{"user_number":670},{"user_number":673},{"user_number":685},{"user_number":674},{"user_number":688},{"user_number":660},{"user_number":662},{"user_number":658},{"user_number":659},{"user_number":813},{"user_number":669},{"user_number":821},{"user_number":962},{"user_number":10042},{"user_number":10047}]

*/



function save_new_discount_object(random_dis_code, desc) {
  
          
  var dis_object = {
    user_number : null,
    buy_time : null,
    dis_amount : null,
    dis_code : random_dis_code,
    perc : 10,
    desc : ( desc || "-------------")
  };

  var new_discount_product = new Discount(dis_object);

  new_discount_product.save(function(err, dis_product) {
    
    if (err) {
      console.error({ success: false, msg: "Discount product NOT saved!", err: err});
    } else {
      global.new_dis_count += 1;
      console.log("Discount product saved! count: " + global.new_dis_count );
    };

  }); // end of spremi discount item
  
  
};


global.create_discount_codes = function(how_many, desc) {
  
  var previous_discount_codes = [];
  global.new_dis_count = 0;
  
  Discount.find({})
  .select({dis_code: 1})
  .exec(function(err, all_discounts) {
    
    if (err) {
      console.log('GREŠKA U DOBIVANJU POSTOJEĆIH discount objekata');
      console.error({ success: false, msg: "popup_discount_code_error", err: err });
      return;
    };
    
    if ( all_discounts.length > 0 ) {
      
      all_discounts.forEach(function(dis, index) {
        previous_discount_codes.push(dis.dis_code);
      });
      
      
      for ( i = 1; i <= how_many; i++ ) {
        
        
        var new_code = Math.random().toString(16).substring(2, 10);     
        new_code = new_code.replace(/0/g,"z");
        new_code = new_code.replace(/1/g,"x");
        
        var dup = 0;
         
        while ( $.inArray(new_code, previous_discount_codes) > -1 && dup < 3000 ) {
        
          dup += 1;
          console.log('duplikat: ' + dup);
          new_code = Math.random().toString(16).substring(2, 10);
          new_code = new_code.replace(/0/g,"z");
          new_code = new_code.replace(/1/g,"x");
          
        };        
        
        previous_discount_codes.push(new_code);
          
        save_new_discount_object(new_code);
        
      }; // kraj for loop  
      
    }
    else {
      console.log({ success: false, msg: "nije našao niti jedan discount objekt" });
      var new_code = Math.random().toString(16).substring(2, 10);     
      new_code = new_code.replace(/0/g,"z");
      new_code = new_code.replace(/1/g,"x");
      
      previous_discount_codes.push(new_code);
      save_new_discount_object(new_code);
      
    };
    
  });
  
  
  
  
};


global.calc_to_time = function( user_number, arg_pp_sifra, days, tag, START_DATE ) {
  
  return new Promise( function( resolve, reject ) {

    run_user_products_state(user_number)
      .then(
      function(user_products) {

        var new_days = days;

        // BITNO !!!!!!!!!! ova funkcija vraća samo pretplate koje su važeće
        // dakle ne vraća zastarijele pretplate  koje su istekle !!!!

        var monthly_subs = user_products.monthly_arr;
        // sortiraj od manjeg prema većem
        monthly_subs.sort( window.by('valid_from', false, Number ) );

        // ako user već ima pretplatu za trenutno parkiralište ----> dakle produžuje pretplatu dok još ima aktivnu
        $.each( monthly_subs, function( monthly_index, monthly ) {
          
          // ako već ima pretplatu za ovo parkiralište onda pomakni valid to da bude nastavak već postojećeg valid to

          var tag_u_postojecoj_pretplati = monthly.tag ? monthly.tag : false;
          var tag_argument = tag ? tag : false;

          var tagovi_su_isti = false;

          // ako su tagovi neki string onda moraju biti isti
          if ( tag_u_postojecoj_pretplati == tag_argument ) tagovi_su_isti = true;

          // ako su tagovi undefined ili null ili "" -------> oba su type castana na false !!!!
          // i ako su oba false onda opet definiraj da su isti 
          if ( tag_u_postojecoj_pretplati == false && tag_argument == false ) tagovi_su_isti = true;

          // ako su tagovi isti i ako je ista pp sifra
          // !!!!!!!!
          // I AKO ZAVRŠNI DATUM POSTOJEĆE PRETPLATE JE VEĆI OD POČETNOG DATUMA NOVE PRETPLATE
          // !!!!!!!!
          if ( 
            tagovi_su_isti                           &&
            monthly.pp_sifra == String(arg_pp_sifra) &&
            monthly.valid_to > START_DATE
          ) {

            var one_day_in_ms = 1000*60*60*24;

            // novi start pretplate bi trebao biti kraj postojeće pretplate
            // ALI !!!  ako je kraj postojeće pretplate star tj davno prije je završio
            // onda startaj pretplatu od sada
            var novi_pocetak = monthly.valid_to || 0;
            var sada = Date.now();
            if ( sada > novi_pocetak ) novi_pocetak = sada;

            // prvo odredim datum od kraja prošle pretplate + broj dana
            var new_valid_to = novi_pocetak + (one_day_in_ms * days);
            // koliko je ms od sada do valid to vremena
            new_days = new_valid_to - Date.now();
            // pretvori ms u dane
            new_days = new_days/(1000*60*60*24);
            // vrati dane
            resolve(new_days);

          }; // kraj ako su tagovi isti

        }); // kraj loop each monghly sub 
        
        resolve(days);  

      }); // kraj then querija za get products 

  }); // kraj return promise

}; // kraj


global.save_new_voucher = function( voucher_code, amount, discount, user_number, desc, type, park, days, tag, START_DATE ) {
  
  if ( !START_DATE ) START_DATE = Date.now();
  
  global.calc_to_time( user_number, park, days, tag, START_DATE )
  .then(
  function(new_days) {
    
    var price = amount ? amount : null;

    if ( discount && amount) {
      var discount_koef = (100 - discount)/100;
      price = amount * discount_koef;
    };

    if ( amount == 0 ) price = 0;

    var one_day_in_ms = 1000*60*60*24;

    var new_voucher = {
      
      sifra: (Date.now() + "-" + voucher_code),
      pp_sifra: (park ? String(park) : null),
      code: ('v' + voucher_code),
      amount: amount,
      desc: (desc || 'ADMIN GENERATOR'),
      start_time: null,
      user_number: ( user_number || null ),
      is_free: false,
      price: price,
      type: ( park ? type : "voucher"),
      valid_from: ( park ? START_DATE : null ),
      valid_to: ( park ? (Date.now() + (one_day_in_ms * new_days) ) : null ),
      balance: amount,
      buy_time: ( user_number ? Date.now() : null ),
      tag: ( tag || null ),
      
    };

    var new_voucher_product = new CustomerProduct(new_voucher);

    new_voucher_product.save(function(err, voucher) {

      if (err) {
        console.error({ success: false, msg: "Voucher product  NOT saved!", err: err});
      } else {
        
        if ( typeof global.new_voucher_count == 'undefined' ) global.new_voucher_count = 0;
        
        global.new_voucher_count += 1;
        console.log("Voucher product saved! count: " + global.new_voucher_count );
      };

    }); // end of spremi discount item

  }); // kraj then od calculacije new days
  
}; // kraj save new voucher 


function loop_until_not_duplicate(previous_vouchers, dup) {
  
  
  var new_code = Math.random().toString(16).substring(2, 10);
  // generiraj novi code sve dok ne dođem do nekog koji je različit od prijašnjih !!!!!!
  // ili dok je broj ponavljanja manji od 30K
  while ( $.inArray(new_code, previous_vouchers) > -1 && dup < 30000 ) {

    dup += 1;
    console.log('duplikat: ' + dup);
    new_code = Math.random().toString(16).substring(2, 10);
    new_code = new_code.replace(/0/g,"z");
    new_code = new_code.replace(/1/g,"x");

  };
  
  return {
    new_code: new_code,
    dup: dup,
  }

};


global.create_vouchers = function(how_many, amount, discount, user_number, desc, type, park, days, tag, START_DATE, VOUCHER_ID ) {
  
  if ( typeof how_many == 'undefined' || how_many == 0 ) return;
  
  if ( typeof amount == 'undefined' ) return;
  
  var previous_vouchers = [];
  global.new_voucher_count = 0;
  
  if ( !VOUCHER_ID ) { 
  
    CustomerProduct
    .find( { code: { $ne: null } })
    .select({code: 1})
    .exec(function(err, all_vouchers) {

      if (err) {
        console.log('GREŠKA U DOBIVANJU POSTOJEĆIH voucher objekata');
        console.error({ success: false, msg: "popup_discount_code_error", err: err });
        return;
      };

      // --------------------------------------------------------------------------------
      // --------start-----------------counter how many ---------------------------------
      // --------------------------------------------------------------------------------
      for ( i = 1; i <= how_many; i++ ) {
        
        // var user_number = global.vou_users[i - 1];
        
        // ako nisam dao voucher id kao arg ( koji je zapravo obični timestamp ) onda generiraj alfanumerički id
        // ali ako sam dao id kao argument onda nemoj probavati generirati new code
        var new_code = null;
        var dup = 0;
      
        if ( all_vouchers.length > 0 || previous_vouchers.length > 0 ) {

          all_vouchers.forEach(function(voucher, index) {
            previous_vouchers.push( voucher.code.replace('v', '') );
          });


          var new_code = Math.random().toString(16).substring(2, 10);     
          new_code = new_code.replace(/0/g,"z");
          new_code = new_code.replace(/1/g,"x");

          var new_code_result = loop_until_not_duplicate(previous_vouchers, dup);

          new_code = new_code_result.new_code;
          dup = new_code_result.dup;
          previous_vouchers.push(new_code);


        }
        else {
          // ako nema u zabi niti jedan voucher
          console.log({ success: false, msg: "OVO JE PRVI PRODUCT ---> nije našao niti jedan product objekt prije ovog" });
          
          var new_code = Math.random().toString(16).substring(2, 10);
          new_code = new_code.replace(/0/g,"z");
          new_code = new_code.replace(/1/g,"x");
          previous_vouchers.push(new_code);

        };

        
        global.save_new_voucher(new_code, amount, discount, user_number, desc, type, park, days, tag );
        
      }; // kraj for loop od how many   
      // --------------------------------------------------------------------------------
      // --------end-------------------counter how many ---------------------------------
      // --------------------------------------------------------------------------------


    }); // pronadji sve vouchere u bazi

  }
  else {
    
    for ( g = 1; g <= how_many; g++ ) {
      var new_code = VOUCHER_ID + '-' + g;
      global.save_new_voucher(new_code, amount, discount, user_number, desc, type, park, days, tag );
    };
    
  }; // kraj else ( ako mu JESAM dao VOUCHER_ID )
  
  
};



exports.create_products = function(req, res) {
  
  var product_data = req.body;
  var token = req.headers['x-auth-token'] || null;
  
  // iznimka  je kada radim request za park places ---_> user ne mora biti ulogiran
  if ( token ) {
      
    global.check_is_super(token)
    .then(
    function( result ) {
      // iznimka  je kada radim request za park places ---_> user ne mora biti ulogiran
      if ( result.success == true  ) {
        // ovo je glavna funkcija za find
        run_find( req, res, result.super_admin, result.user );
        return;
      } else {
        res.json([]);
        return;
      };
    })
    .catch(function(err) {
      res.json([]); 
    });

  } else {
    // vrati prazan array ako nema token
    res.json([]);    
    
  };
  
  
};


exports.get_active_pp_and_tag_products = function(req, res) {
  
  var pp_sifra = req.params.pp_sifra;
  var tag = (req.params.tag == 'none') ? null : req.params.tag ;
  var start_time = Number(req.params.start_time);
  
  CustomerProduct
    .find({ pp_sifra: pp_sifra, tag: tag, valid_from: {$lt: start_time}, valid_to: {$gt: start_time}  })
    .exec(function(err, products) {
  
    if (err) {
      console.log('GREŠKA U DOBIVANJU svih aktivnih CUSTOMER PRODUCTA ');
      res.json({ success: false, msg: "GREŠKA U DOBIVANJU svih aktivnih CUSTOMER PRODUCTA", err: err})
      return;
    };
    
    var clean_products = [];
    
    if (products.length > 0 ) {

      // prvo pretvori sve produkte iz mongo db formata u običan js array
      products.forEach(function(prod, prod_ind) {
        var clean_prod = convert_mongoose_object_to_js_object(prod);
        clean_products.push(clean_prod);
      });
      
      // arbitrarno najveći datum je za 500 godina u budućnosti :)
      var min_end_date = Date.now() + 1000*60*60*24*365*500;
      
      clean_products.forEach(function(clean_prod, clean_prod_ind) {
        if ( clean_prod.valid_to < min_end_date && clean_prod.type !== 'voucher' && clean_prod.type !== 'card' ) min_end_date = clean_prod.valid_to;
      });
      
    };
    
    res.json({ success: true, count: products.length, first_end: min_end_date });
    
  }); // kraj find produkte  tj vaučere i validne mjesečne karte
  
};



