
var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(
`
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="qrcode.js"></script>



</head>
<body>
  
<div id="qrcode"></div>  

</body>
</html>


`);


var $ = require("jquery")(window);

var QR = require('./qrcode');


exports.html_racuna = function ( customer_product, user ) {
  

  function generiraj_qr_time(time) {

    var unix_time = time || Date.now();
    var time_now = new Date(unix_time);

    var yyyy = time_now.getFullYear() + '';

    var mon = time_now.getMonth() + 1 + '';
    if ( mon.length == 1 ) mon = '0' + mon;

    var dd = time_now.getDate() + '';
    if ( dd.length == 1 ) dd = '0' + dd;

    var hh = time_now.getHours() + '';
    if ( hh.length == 1 ) hh = '0' + hh;

    var mm = time_now.getMinutes() + '';
    if ( mm.length == 1 ) mm = '0' + mm;

    var ss = time_now.getSeconds() + '';
    if ( ss.length == 1 ) ss = '0' + ss;

    var qr_time = yyyy + mon + dd + '_' + hh + mm;

    return qr_time;


  };

  function capitalizeFLetter(word) { 
    var result = word[0].toUpperCase() + word.slice(1); 
    return result;
  };


  function zaokruziIformatirajIznos(num, zaokruzi_na) {
    // primjer : var num = 123456789.56789;

    var brojZnamenki = 2;
    if ( typeof zaokruzi_na !== 'undefined' )  brojZnamenki = parseInt( zaokruzi_na );


    var zaokruzeniBroj = zaokruzi(num, brojZnamenki);


    var stringBroja = String(zaokruzeniBroj);
    // 123456789
    var lijeviDio = stringBroja.split('.')[0] + "";
    var tockaPostoji = stringBroja.indexOf(".");
    var desniDio = "00";
    if (tockaPostoji > -1) {
      desniDio = stringBroja.split('.')[1] + "";
    };

    /*
    if (Number(desniDio) < 10) {
      desniDio = '0' + Number(desniDio);
    };
    */


    var broj_je_pozitivan = true;

    // ako je broj negativan 
    if ( lijeviDio.substring(0,1) == '-' ) {
      lijeviDio = lijeviDio.substring(1);
      broj_je_pozitivan = false;
    };

    var stringLength = lijeviDio.length;

    // ne zanimaju me brojevi s tri znamenke i manje jer oni nemaju točke
    if (stringLength >= 4) {
      // pretvaram string u array
      lijeviDio = lijeviDio.split('');
      // gledam koliko stane grupa po 3 znamaneke
      // i onda uzmem ostatak - to je jednostavna modulus operacija
      var dotPosition = stringLength % 3;
      // zanima me koliko treba biti točaka u broju
      // to dobijem tako da podijelim broj znamenki sa 3 i gledam samo cijeli broj
      var dotCount = parseInt(stringLength / 3)

      // postavim prvu točku
      // NAPOMENA u slučajevima kada je modulus = 0 ova funkcije postavi točku čak i na počatek broja
      // tu točku kasnije obrišem
      // svaka sljedeća točka je UVIJEK 4 pozicije udesno !!!!
      // napomena - logično bi bilo da je svake 3 pozicije udesno, 
      // ali moram brojati i točku koju sam stavio i ona povećava length od stringa
      for (i = 1; i <= dotCount; i++) {
        // stavi točku
        lijeviDio.splice(dotPosition, 0, ".");
        // id na poziciju current + 4 
        dotPosition = dotPosition + 4;
      };


      // spoji nazad sve znakove u jedan string !!!
      lijeviDio = lijeviDio.join('')

      // kad se dogodi da je modulus === 0 onda postavim točku na nultu poziciju
      // i sad je moram obrisati !!!!!!!
      if (lijeviDio.charAt(0) === ".") {
        // uzimam sve osim prvog znaka tj točke
        lijeviDio = lijeviDio.substring(1);
      }
    };

    // ponovi nulu onoliko puta koliko treba da decimale imaju onoliko stringova 
    // koliko je definirano sa argumentom [zaokruzi_na] 
    // ( ... od kojeg gore dobijem varijablu brojZnamenki )
    if (desniDio.length < brojZnamenki) {
      desniDio = desniDio + '0'.repeat(brojZnamenki - desniDio.length);
    };


    var formatiraniBroj = lijeviDio + "," + desniDio;

    if ( desniDio == "00" &&  brojZnamenki == 0 )  formatiraniBroj = lijeviDio;

    if ( broj_je_pozitivan == false ) formatiraniBroj = '-' + formatiraniBroj;

    return formatiraniBroj;

  };

  function zaokruzi(broj, brojZnamenki) {

   var broj = parseFloat(broj);

   if ( typeof brojZnamenki == 'undefined' ) {
     brojZnamenki = 0;
   };
   var multiplicator = Math.pow(10, brojZnamenki);
   broj = parseFloat((broj * multiplicator).toFixed(11));
   var test =(Math.round(broj) / multiplicator);
   // + znak ispred je samo js trik da automatski pretvori string u broj
   return +(test.toFixed(brojZnamenki));
  };

  
  var qr_price_string = zaokruzi(customer_product.price, 2) + '';
  
  var znamenke_qr = qr_price_string.split('.')[0];
  var decimale_qr = qr_price_string.split('.')[1];
  if ( !decimale_qr ) decimale_qr = '00';
  if ( decimale_qr.length < 2 ) decimale_qr = decimale_qr + '0'.repeat(2 - 	decimale_qr.length);
  
  
  // bez decimalne točke
  qr_price_string = znamenke_qr + decimale_qr;  
  
  
  // nadoplata ili pretplata
  var offer = customer_product.code.split('-')[1];
  
  // ako je pretplata koliko mjeseci 
  var duration = customer_product.code.split('-')[2];
  // ako je pretplata za koje je to parkiralište
  var curr_pp_id = customer_product.code.split('-')[3];
  
  var qr_code_url = '';
  var QR_DOM = '';
  var QR_HTML = '';
  
  
  if ( customer_product.jir ) {
    
    qr_code_url = `https://porezna.gov.hr/rn?jir=${customer_product.jir}&datv=${generiraj_qr_time(customer_product.buy_time)}&izn=${qr_price_string}`;
    
    QR_DOM = new QR.qr_create( $("#qrcode")[0], {
      text: qr_code_url,
      width: 128,
      height: 128,
      colorDark : "#000000",
      colorLight : "#ffffff",
      correctLevel : QR.qr_create.CorrectLevel.L
    });
    
    
    // $('#qrcode table').css('margin', '0 auto');
    

  var all_rows_html = '';
  
  $('#qrcode tr').each(function() {
    
    var this_row = this;
    var row_html = '';
    
    var row_tds_html = '';
    $(this_row).find('td').each(function() {
    
      
      if ( $(this).css('background-color') == "rgb(0, 0, 0)" ) {
        // CRNA ĆELIJA
        row_tds_html += `<div style="width: 3px; height: 3px; float: left; display: flex;"><img src="https://wepark.circulum.it:9000/img/bbg.png" /></div>`;
      } else {
        // BIJELA ĆELIJA
        row_tds_html += `<div style="width: 3px; height: 3px; float: left; display: flex;"></div>`;
      };

    
    });  
    row_html = `<div style="width: ${$(this_row).find('td').length*3}px; height: 3px; float: left;">${row_tds_html}</div>`;
    all_rows_html += row_html;
    
  });
  
  var qr_divs = 
`
<div id="qr_divs" style="margin: 2mm 0; width: 100%; display: flex; align-items: center; justify-content: center;">

<div style="width: ${ $('#qrcode tr').eq(0).find('td').length*3 }px; height: ${$('#qrcode tr').length*3}px; margin: 0 auto;">
${all_rows_html}
</div>  
  
</div>
`;
  
  
  // $('#qrcode').after(qr_divs);
  // $('#qrcode').html('');
    

  // QR_HTML = $("#qrcode")[0].outerHTML;


  // QR_HTML = $("#qr_divs")[0].outerHTML;
  // console.log( QR_HTML );

  QR_HTML = qr_divs
  console.log( qr_divs );

    
  };
  



  var pp_name = '';
  
  global.park_places_list.forEach(function(pp_obj, pp_index) {
    if ( pp_obj.pp_sifra == curr_pp_id ) pp_name = pp_obj.pp_name;
  });
  
  
  
  var ime_paketa = '';
  var trajanje = '';
  var pocetak = '';
  var kraj = '';
  

  var storno_linija = '';
  
  if ( customer_product.storno ) {
    storno_linija = 
`
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>
<span style="color: #d62e19; font-weight: 700;">STORNO RAČUNA BR: ${customer_product.storno}</span>
`;

};
  
  
var poslovni_podaci = '';
  
  
if ( user.r1 ) {
    
poslovni_podaci = 
      
`
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 

<div style="width: 100%; float: left; clear: both;">
Podaci kupca:<br>
OIB: ${ user.oib || '' }<br>
NAZIV: ${user.business_name || ''}<br>
ADRESA: ${user.business_address || ''}
</div>
`;
    
    
}; // kraj ako je ovo r1 == true
  
  if ( offer == 'pretplata' ) {
    
    ime_paketa = capitalizeFLetter(offer) + ' za parkiralište ' + pp_name;
    
    ime_paketa = `  <div style="width: 100%; float: left; text-align: center;">Proizvod/usluga: </div>
                    <div style="  width: 100%;
                                  float: left;
                                  font-weight: 700;
                                  text-align: center;
                                  font-size: 13px;
                                  margin-bottom: 5px;">${ ime_paketa }</div>`;
    
    trajanje = Number(duration)*30 + ' dana';
    
    trajanje = `  <div style="width: 50%; float: left; clear: left;">Trajanje: </div>
                  <div style="width: 50%; float: left; text-align: right;">${trajanje}</div>`;
    
    pocetak = ` <div style="width: 50%; float: left; clear: left;">Početak: </div>
                <div style="width: 50%; float: left; text-align: right;">

<div style="float: right; margin-left: 6px;">${global.getMeADateAndTime(customer_product.valid_from).vrijeme}</div>
<div style="float: right;">${global.getMeADateAndTime(customer_product.valid_from).datum}</div>

                  
                </div>`;
    
    kraj = `    <div style="width: 50%; float: left; clear: left;">Završetak: </div>
                <div style="width: 50%; float: left; text-align: right;">
                  
<div style="float: right; margin-left: 6px;">${global.getMeADateAndTime(customer_product.valid_to).vrijeme}</div>
<div style="float: right;">${global.getMeADateAndTime(customer_product.valid_to).datum}</div>
                  
                </div>`;
    
    
  } 
  else {
    
    ime_paketa = capitalizeFLetter(offer) + ' korisničkog računa';
    
    ime_paketa = `  <div style="width: 100%; float: left; text-align: center;">Proizvod/usluga: </div>
                    <div style="  width: 100%;
                                  float: left;
                                  font-weight: 700;
                                  text-align: center;
                                  font-size: 13px;
                                  margin-bottom: 5px;">${ ime_paketa }</div>`;
    
  };
  
  
    var datum_transakcije = ` <div style="width: 50%; float: left; clear: left;">Datum transakcije: </div>
                            <div style="width: 50%; float: left; text-align: right;">
                              
<div style="float: right; margin-left: 6px;">${global.getMeADateAndTime(customer_product.buy_time).vrijeme}</div>
<div style="float: right;">${global.getMeADateAndTime(customer_product.buy_time).datum}</div>

                            </div>`;
  
  
  var broj_racuna_mjesta_kase = ` <div style="width: 50%; float: left; clear: left;">Račun broj: </div>
                                  <div style="width: 50%; float: left; text-align: right;">
                                  ${customer_product.racun_br + '/1/1' }
                                  </div>`;

  
  
  var osnovica = `<div style="width: 50%; float: left; clear: left;">Cijena: </div>
            <div style="width: 50%; float: left; text-align: right;">${zaokruziIformatirajIznos(customer_product.price/1.25)}kn</div>`;
  
  var iznos_pdv = `<div style="width: 50%; float: left; clear: left;">Pdv 25%: </div>
                  <div style="  width: 50%;
                                float: left;
                                text-align: right;">

                  ${zaokruziIformatirajIznos(customer_product.price/1.25*0.25)}kn

                  </div>`;
  
  var iznos = `<div style="width: 50%; float: left; clear: left; font-size: 18px; font-weight: 700;">Iznos s pdv: </div>
            <div style="  width: 50%;
                          float: left;
                          text-align: right;
                          font-size: 18px;
                          font-weight: 700;">

            ${zaokruziIformatirajIznos(customer_product.price)}kn

            </div>`;
  
  var zki = ` <div style="width: 10%; float: left; clear: left; font-size: 10px;">ZKI:</div>
              <div style="width: 90%; float: left; text-align: right; font-size: 12px;">
                ${customer_product.zki || '' }
              </div>`;
  
  
  
  
  var jir_link = customer_product.jir ? `<a href="${qr_code_url}" target="_blank">${customer_product.jir}</a>` : '';
  
      
  var jir = ` <div style="width: 10%; float: left; clear: left; font-size: 10px;">JIR:</div>
              <div style="width: 90%; float: left; text-align: right; font-size: 12px;">

              ${jir_link}


              </div>`;
  

  var racun_code = ` <div style="width: 10%; float: left; clear: left; font-size: 10px;">CODE:</div>
            <div style="width: 90%; float: left; text-align: right; font-size: 12px;">
              ${customer_product.code || '' }
            </div>`;


  
  var racun_html = `
  
<div style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #f3fcff;
            padding: 3px 0 0;">

    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                padding: 3px;">

 
      <div style="  font-size: 16px;
                    margin-top: 0px;
                    margin-bottom: 3px;
                    color: #1a2840;
                    font-weight: 400;
                    line-height: 22px;
                    letter-spacing: normal;">
        
        WePark Račun <br>
      </div>

      <div style="    
        font-size: 13px;
        margin-top: 40px;
        margin-bottom: 20px;
        color: #454545;
        font-weight: 400;
        text-align: left;
        line-height: 19px;
        max-width: 400px;
        clear: both;
        width: auto;
        margin: 0 auto;
        padding: 10px;
        height: auto;
        background-color: #fff;
        border-radius: 10px;
        border: 1px solid #e8e6e6;
        letter-spacing: 0.04rem;">
        

${poslovni_podaci}


${storno_linija}


<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 
OPERATER: WEPARKAPP

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 
${datum_transakcije}

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 
${broj_racuna_mjesta_kase}

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 
NAČIN PLAĆANJA: KREDITNA KARTICA

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 
${ime_paketa}
${trajanje}
${pocetak}
${kraj}
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>
${osnovica}
${iznos_pdv}
${iznos}
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>        
${zki}
${jir}
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>
${racun_code}
        
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>
${QR_HTML}
        
        
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>
        
</div>

<br style="display: block; clear: both;">
      
</div>


    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #f3fcff;
                padding: 0;">

      
      
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 0 auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />

      <br style="display: block; clear: both;">

      <div id="u_${Date.now() + 123}" style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 15px;
        border-top: 1px solid #eaeaea;
        padding-top: 0;
        background-color: #fff;
        letter-spacing: 0.06rem;">

        <br style="display: block; clear: both;">
        <a id="u_${Date.now() + 5685155}" href="https://wepark.circulum.it:9000/TermsofUse.html" target="_blank">UVJETI POSLOVANJA</a><br>
        <b id="u_${Date.now() + 222}" >Odgovorna osoba za izdavanje računa:</b><br>
        <b>Karlo Starčević (direktor)</b><br>
        <b>Wepark smart parking d.o.o.</b><br>
        <b>OIB:</b>&nbsp;77372456863<br>
        <b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
        <b>OFFICE:</b>&nbsp;Janka Rakuše 1, Zagreb<br>
        <b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
        <b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
        <b>WEB:</b>&nbsp;www.wepark.eu<br>
        <br style="display: block; clear: both;">
        <div id="u_${Date.now() + 555}" style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 5px; max-width: 650px; margin: 0 auto;">
        Društvo je upisano u sudski registar Trgovačkog suda u Zagrebu pod brojem 081131571. Temeljni kapital u iznosu od 20.000 kn je uplaćen u cijelosti. Račun se vodi kod Privredne Banke Zagreb. Član društva Karlo Starčević.
        </div>
        <br style="display: block; clear: both;">
      </div>        
      <br style="display: block; clear: both;">
    </div>  

    
</div> 

    
  
`;
  
  return racun_html;
  
};



global.make_racun = exports.html_racuna;



  
  global.qr_product = {
    
    "sifra" : "1766",
    "pp_sifra" : null,
    "code" : "9-nadoplata---1606421737414--",
    "amount" : 20,
    "desc" : "TOKEN-MVISA",
    "buy_time" : 1606421748280.0,
    "user_number" : 9,
    "is_free" : false,
    "price" : 20,
    "type" : "card",
    "valid_from" : null,
    "valid_to" : null,
    "balance" : 20,
    "tag" : null,
    "__v" : 0,
    "fis_greska" : null,
    "fis_id" : "2bf22ee0-3024-11eb-8915-e5e5760f596a",
    "fis_time" : 1606421748280.0,
    "jir" : "93cf9507-6f49-474c-b6ef-afd0c10aea19",
    "racun_br" : "28",
    "zki" : "8b2e59cfc1842a02bf165ec2c99107f8"
    
    
};

  
global.qr_user = {
  
    "_id" : "5e32374d220ba4253d747c0e",
    "user_number" : 9,
    "user_saved" : null,
    "user_edited" : null,
    "saved" : 1510394683000.0,
    "edited" : 1580338046963.0,
    "last_login" : 1580338046963.0,
    "user_address" : "Žumberačka ulica, 10000 Zagreb Croatia",
    "user_tel" : "",
    "business_name" : "WePark",
    "business_address" : "",
    "business_email" : "",
    "business_tel" : "",
    "business_contact_name" : "",
    "oib" : null,
    "iban" : null,
    "gdpr" : true,
    "gdpr_saved" : 1580338046963.0,
    "gdpr_accassed" : 1580338046963.0,
    "user_deleted" : null,
    "car_plates" : [ 
        {
            "time" : 1595513166701.0,
            "reg" : "ZG2709AO"
        }
    ],
    "customer_tags" : null,
    "balance" : 3056.29,
    "customer_acquisition" : null,
    "customer_vip" : null,
    "park_places_owner" : null,
    "name" : "karlo",
    "full_name" : "Karlo Starčević",
    "hash" : "pbkdf2$10000$d270f7ad2eb3a6f750dd7efcb5049457879d70711aaa45568898a021183c07ff14e1541d0e55a5df6a5b839484fe847d8dfa1e33d6cb6a19f0b48a8b674aeda6$0ac475bdede2e3ba2cb8d9e83376dc40cff51199347b497c32393a046942a56d62501db8f26550340d0d61eacb9b24736c7f86eafd43ac13b670bf3c606ff624",
    "email" : "tkutlic@gmail.com",
    "email_confirmed" : true,
    "welcome_email_id" : "",
    "roles" : {
        "admin" : true,
        "we_app_user" : true,
        "super_admin" : true
    },
    "new_pass_email_confirmed" : true,
    "new_pass_email_id" : "9-1587968267774-8822536159008",
    "new_pass_request" : null,
    "r1" : false,
    "visited_pp" : [ 
        "3", 
        "5", 
        "14", 
        "15", 
        "16"
    ]
  
}


