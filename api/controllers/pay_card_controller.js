process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ParkPlace = mongoose.model('ParkPlace');
var ParkAction = mongoose.model('ParkAction');
var User = mongoose.model('User');

var Card = mongoose.model('Card');

var CustomerProduct = mongoose.model('CustomerProduct');

var Discount = mongoose.model('Discount');

var RacunCounter = mongoose.model('RacunCounter');

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);



var axios = require('axios').default;

axios.defaults.adapter = require('axios/lib/adapters/http');



var md5 = require('md5');
var sha512 = require('js-sha512');

var WE_I18N = require('./we_server_i18n');


var customer_products_controller = require('./customer_products_controller');

var fiskal_controller = require('./fiskal_controller');

var pp_controller = require('./park_places_controller');



/* /////////////////////////    ZAOKRUŽI BROJ   ///////////////////////////////////// */
function zaokruzi(broj, brojZnamenki) {

 var broj = parseFloat(broj);

 if ( typeof brojZnamenki == 'undefined' ) {
   brojZnamenki = 0;
 };
 var multiplicator = Math.pow(10, brojZnamenki);
 broj = parseFloat((broj * multiplicator).toFixed(11));
 var test =(Math.round(broj) / multiplicator);
 // + znak ispred je samo js trik da automatski pretvori string u broj
 return +(test.toFixed(brojZnamenki));
};


function zaokruziIformatirajIznos(num, zaokruzi_na) {
  // primjer : var num = 123456789.56789;

  var brojZnamenki = 2;
  if ( typeof zaokruzi_na !== 'undefined' )  brojZnamenki = parseInt( zaokruzi_na );
  

  var zaokruzeniBroj = zaokruzi(num, brojZnamenki);
  

  var stringBroja = String(zaokruzeniBroj);
  // 123456789
  var lijeviDio = stringBroja.split('.')[0] + "";
  var tockaPostoji = stringBroja.indexOf(".");
  var desniDio = "00";
  if (tockaPostoji > -1) {
    desniDio = stringBroja.split('.')[1] + "";
  };

  /*
  if (Number(desniDio) < 10) {
    desniDio = '0' + Number(desniDio);
  };
  */

  var stringLength = lijeviDio.length;

  // ne zanimaju me brojevi s tri znamenke i manje jer oni nemaju točke
  if (stringLength >= 4) {
    // pretvaram string u array
    lijeviDio = lijeviDio.split('');
    // gledam koliko stane grupa po 3 znamaneke
    // i onda uzmeme ostatak - to je jednostavna modulus operacija
    var dotPosition = stringLength % 3;
    // zanima me koliko treba biti točaka u broju
    // to dobijem tako da podijelim broj znamenki sa 3 i gledam samo cijeli broj
    var dotCount = parseInt(stringLength / 3)

    // postavim prvu točku
    // NAPOMENA u slučajevima kada je modulus = 0 ova funkcije postavi točku čak i na počatek broja
    // tu točku kasnije obrišem
    // svaka sljedeća točka je UVIJEK 4 pozicije udesno !!!!
    // napomena - logično bi bilo da je svake 3 pozicije udesno, 
    // ali moram brojati i točku koju sam stavio i ona povećava length od stringa
    for (i = 1; i <= dotCount; i++) {
      // stavi točku
      lijeviDio.splice(dotPosition, 0, ".");
      // id na poziciju current + 4 
      dotPosition = dotPosition + 4;
    }
    // koristim regex da obrišem sve zareze jer oni nažalost ostaju nakon join array-a
    lijeviDio = lijeviDio.join().replace(/,/g, "");

    // kad se dogodi da je modulus === 0 onda postavim točku na nultu poziciju
    // i sad je moram obrisati !!!!!!!
    if (lijeviDio.charAt(0) === ".") {
      // uzimam sve osim prvog znaka tj točke
      lijeviDio = lijeviDio.slice(1, lijeviDio.length);
    }
  };

  // ponovi nulu onoliko puta koliko treba da decimale imaju onoliko stringova 
  // koliko je definirano sa argumentom [zaokruzi_na] 
  // ( ... od kojeg gore dobijem varijablu brojZnamenki )
  if (desniDio.length < brojZnamenki) {
    desniDio = desniDio + '0'.repeat(brojZnamenki - desniDio.length);
  };

  
  var formatiraniBroj = lijeviDio + "," + desniDio;

  if ( desniDio == "00" &&  brojZnamenki == 0 )  formatiraniBroj = lijeviDio;
  
  return formatiraniBroj;
  
};



// ovo je jedan mjesec tj 30 dana izraženo u mili sec
var dana_30_u_ms = 1000 * 60 * 60 * 24 * 30;


// ADDITIONAL OPTIONS FOR NATIVE JAVASCRIPT SORT ------- from http://jsfiddle.net/gfullam/sq9U7/  
window.by = function(path, reverse, primer, then) {
  var get = function (obj, path) {
      if (path) {
        path = path.split('.');
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        };
        return obj[path[len]];
      }
      return obj;
    },
    prime = function (obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function (a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B) ? -1 :
      (A > B) ? 1 :
      (typeof then === 'function') ? then(a, b) : 0
    ) * [1, -1][+!!reverse];
  };
};

function cit_rand_min_max(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
};

function create_ms_at_zero_and_week_day(add_days) {

  var date = new Date();
  var yyyy = date.getFullYear();
  var mnt = date.getMonth();
                              // plus or minus days    
  var day = date.getDate() + (add_days || 0 );

  // nedjelja je 0 ---> subota je 6
  var week_day = date.getDay();

  var day_at_zero = new Date(yyyy, mnt, day, 0, 0, 0, 0);
  var ms_at_zero = Number(day_at_zero); //  day_at_zero.getMilliseconds();

  return {
    ms_at_zero: ms_at_zero,
    week_day: week_day
  };

};
exports.create_ms_at_zero_and_week_day = create_ms_at_zero_and_week_day;

function convert_mongoose_object_to_js_object(mongoose_object) {
  
  var js_object = mongoose_object.toJSON();
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};




function create_card_page( data, user_lang, user_num, card_signature, Token, TokenNumber ) {
  
  /* PRIMJER
  var data = {
    ShoppingCartID: ShoppingCartID,
    TotalAmount: TotalAmount,
    Signature: Signature
  };
  */
  
  
var token_data_html =
`
<input type="hidden" name="Token" value="${Token}">
<input type="hidden" name="TokenNumber" value="${TokenNumber}">
`;
  
  
var red_background_style = `style="background-color: #d3301e;"`;  
  
var token_option_html =
`  
<div id="token_option_box" class="cit_radio_group cit_hide">
<div id="token_option" class="cit_radio_item">
<div class="cit_radio_circle"><div class="cit_radio_dot"></div></div>
<span id="i_want_token_span">${ WE_I18N.trans( 'i_want_token_span', user_lang )}</span>
</div>
</div>     
`; 
    

  
var submit_button = 
`<div id="goto_ws_pay_site_btn">${ WE_I18N.trans( 'goto_ws_pay_site_btn', user_lang )}</div>`; 
  
  
if ( card_signature ) {
  submit_button = 
`<div id="goto_ws_pay_site_btn">${ WE_I18N.trans( 'new_token_btn', user_lang )}</div>`; 
}; 
  

function save_is_token_to_local_storage() {  

  var is_token = '';
  if (card_signature) { is_token = `window.localStorage.setItem('is_token', 'true' );`; }
  else { is_token = `window.localStorage.setItem('is_token', 'false' );`; };
  return is_token;
};
  
  
var html = 
    
    
`
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>WePark Payment</title>
  <meta name="description" content="WePark Payment">
  <meta name="author" content="We Park Ltd">
  <meta name="viewport" content="viewport-fit=cover, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no, width=device-width" />
  <link rel="stylesheet" href="css/normalize.css">  
  <link rel="stylesheet" href="css/titllium_font_face.css">  
  <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/card_pay_page.css">
  
</head>

<body>
  <div id="ws_pay_form_overlay" style="opacity: 0; pointer-events: none; transition: opacity 0.3s ease-in;">
    <img id="loader_ws_pay" src="/img/loader_ws_pay.png" />
    <div id="loader_text_ws_pay">${ WE_I18N.trans( 'loader_text_ws_pay', user_lang )}</div>
  </div>
  
  <div id="card_page_container">
  
    <a class="ws_pay_slika" href="http://www.wspay.info" title="WSpay - Web Secure Payment Gateway" target="_blank">
      <img src="https://wepark.circulum.it:9000/img/placanje_wspay.png">
    </a>
  
    <!-- red_background_style  ${ user_num == 99999 ? '' : '' }  -->
    <div id="ws_pay_info_box" style="flex: 1;">
      
<!-- OBRISATI UVJET USER 99999 KAD PRORADI WSPAY -->
<!-- ${ /* user_num == 99999 ? token_option_html : '' */ '' } -->
      
    ${ card_signature ? '' : token_option_html }
      
<!-- OBRISATI UVJET USER 99999 KAD PRORADI WSPAY -->
    <span id="wspay_explanation_text" style="float: left; width: 100%; overflow: hidden; display: block;">  
    <!-- ${ /* user_num == 99999 ? WE_I18N.trans( 'wspay_explanation_text', user_lang ) : WE_I18N.trans( 'wspay_cekamo_odobrenje', user_lang )*/ '' } -->

      ${ card_signature ? WE_I18N.trans( 'new_token_system', user_lang ) : WE_I18N.trans( 'wspay_explanation_text', user_lang ) }
      <br> 
    </span>
      
    </div>

  <div id="form_box">
    <form id="ws_pay_form" name="ws_pay_form" action="${global.wspay_form_url}" method="POST">
      <!--OVDJE UBACUJEM IS TOKEN INPUT AKO USER ODABERE ŽELIM KORISTITI TOKEN-->

      <input type="hidden" name="Version" value="2.0">
      <input type="hidden" name="Lang" value="${user_lang.toUpperCase()}">
      <input type="hidden" name="ShopID" value="WEPARK">
      <input type="hidden" name="ShoppingCartID" value="${data.ShoppingCartID}">
      <input type="hidden" name="TotalAmount" value="${data.TotalAmount}">
      <input type="hidden" name="Signature" value="${data.Signature}">
      <input type="hidden" name="PaymentPlan" value="0000">
      ${card_signature ? token_data_html : ''}
      <input type="hidden" name="ReturnURL" value="${global.we_payment_url}/payment_success">
      <input type="hidden" name="CancelURL" value="${global.we_payment_url}/payment_cancel">
      <input type="hidden" name="ReturnErrorURL" value="${global.we_payment_url}/payment_error">
    </form>
    
    
    <!-- OBRISATI UVJET USER 99999 KAD PRORADI WSPAY -->

    ${ /* user_num == 99999 ? submit_button : '' */ '' }

    ${ submit_button }
    
  </div>
    
    
    
  <div id="wspay_obavezni_podaci">

    ${ WE_I18N.trans( 'wspay_obavezni_podaci', user_lang )}

    
  </div>   

  </div> <!--KRAJ CARD PAGE CONTAINERA-->  
  

  
  <!--SCRIPTS-->
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="css/bootstrap/js/bootstrap.min.js"></script>
  <!-- <script src="js/simple-scrollbar.min.js"></script> -->
  <script src="js/md5.min.js"></script>
  
  <script>


  $(document).ready(function() {

    window.localStorage.setItem('user_lang', '${user_lang}' );

    ${ save_is_token_to_local_storage() }

    var query = window.location.search.substring(1).split('&');
    
    var user_number = query[0].split('=')[1];
    var offer = query[1].split('=')[1];
    var pp = query[2].split('=')[1];
    var amount = query[3].split('=')[1];
    
    var jedinice = Number( amount.slice(0,amount.length-2) );
    var decimale = Number( amount.slice(-2) )/100;
    
    var amount_string =  amount.slice(0,amount.length-2) + ',' + amount.slice(-2);
    amount = jedinice + decimale;
        
    $('#token_option_box').off('click');
    $('#token_option_box').on('click', function() {
      
      var circle = $('#token_option');
      $('#IsTokenRequest').remove();
      if ( !circle.hasClass('cit_selected') ) {
        circle.addClass('cit_selected');
        var token_input_html = '<input id="IsTokenRequest" type="hidden" name="IsTokenRequest" value="1">';
        $('#ws_pay_form').prepend(token_input_html);
      } else {
        circle.removeClass('cit_selected');
        $('#IsTokenRequest').remove();
      };
      
    });

    

    function submit_ws_form() {
      // prikazi overlay i loader
      $('#ws_pay_form_overlay').css({ 'opacity': '1', 'pointer-events': 'all' });
      // submitaj formu
      $('#ws_pay_form').submit();
    };
    
    $('#goto_ws_pay_site_btn').off('click');    
    $('#goto_ws_pay_site_btn').on('click', submit_ws_form);
    
  }); // kraj document ready


    var response_od_ws_form = {
      "CustomerFirstname": "Toni",
      "CustomerSurname": "Kutlic",
      "CustomerAddress": "Rakušina 4",
      "CustomerCountry": "Hrvatska",
      "CustomerZIP": "10000",
      "CustomerCity": "Zagreb",
      "CustomerEmail": "tkutlic@gmail.com",
      "CustomerPhone": "092 3133 015",
      "ShoppingCartID": "1574977354631",
      "Lang": "HR",
      "DateTime": "28.11.2019. 22:43:39",
      "Amount": "17",
      "ECI": "",
      "STAN": "67265",
      "Partner": "Pbz",
      "WsPayOrderId": "c3540cca-029a-463c-8ca3-1e332cfd425d",
      "PaymentType": "AMEX",
      "CreditCardNumber": "377504-----1000",
      "PaymentPlan": "0000",
      "Token": "a156c125-6ef2-4501-852a-16b457deb822",
      "TokenNumber": "1000",
      "TokenExp": "2003",
      "ShopPostedPaymentPlan": "0",
      "Success": "1",
      "ApprovalCode": "078567",
      "ErrorMessage": "",
      "Signature": "34f1d855181382ce166ddc198ecee1e5"
  }

  /*
  POTREBNO PROVJERITI SIGNATURE

   ShopID
   SecretKey
   ShoppingCartID  SecretKey
   Success
   SecretKey
   ApprovalCode
   SecretKey

  */


    var response_od_ws_tok = {
      "WsPayOrderId": "f75f30eb-d824-4e5d-9049-fb6ce93a6ed7",
      "ShoppingCartID": "1574981103845",
      "CustomerFirstName": "Toni",
      "CustomerLastName": "Kutlic",
      "CustomerAddress": "Rakusina 4",
      "CustomerCity": "Zagreb",
      "CustomerCountry": "Hrvatska",
      "CustomerPhone": "092 3133 015",
      "CustomerZIP": "10000",
      "CustomerEmail": "tkutlic@gmail.com",
      "ECI": "",
      "XID": null,
      "CAVV": null,
      "UCAF": null,
      "MaskedPan": "377504*****1000",
      "CreditCardName": "MAMEX",
      "PaymentPlan": "0000",
      "ApprovalCode": "189210",
      "ErrorMessage": null,
      "Approved": "1",
      "TotalAmount": "5000",
      "Signature": "7883a82e851de407344f16870ac002ed",
      "STAN": "67266",
      "Partner": "Pbz",
      "TransactionDateTime": "20191128234503",
      "ActionSuccess": "1"
  };


  
  </script>
  
  
  
</body>

</html>`;   

return html;

}; // kraj get html for page

exports.pay_card_processing = function(req, res) {
  
  console.log( req.query );
  var query = req.query;
  
  
  if (!query) {
    res.send({ error: 'No query!' }); 
    return;
  };
  
  var user_number = query.user_number;
  
  if (!user_number) {
    res.send({ error: 'No user!' }); 
    return;
  };
  
  var offer = query.offer;
  if (!offer) {
    res.send({ error: 'Offer data missing!' }); 
    return;
  };  
  
  var duration = query.duration;
  if (!duration) {
    res.send({ error: 'Duration data missing!' }); 
    return;
  };
  
  if (duration == 'none') duration = '';
  
  var pp = query.pp;
  if (!pp) {
    res.send({ error: 'Parking data missing!' }); 
    return;
  };
  
  if (pp == 'none') pp = '';
  
  // ovo je broj sa dvije decimale ALI bez točke ili zareza npr 50000 ------> zapisujem ga kao string
  var amount = query.amount; 
  
  // amount = '5022';

  
  
  if (!amount) {
    res.send({ error: 'Amount data missing!' }); 
    return;
  };
  
  var discount = query.discount;
  if (!discount) {
    res.send({ success: false, error: 'Discount percentage data missing!' }); 
    return;
  };
  if (discount == 'none') discount = '';
  
  var discount_id = query.discount_id;
  if (!discount_id) {
    res.send({ success: false, error: 'Discount code missing!' }); 
    return;
  };
  if (discount_id == 'none') discount_id = '';
  
  
  var card_signature = query.card_signature;
  if (card_signature == 'none') card_signature = '';

  
  if ( discount !== '' ) {
    var prvi_dio_amounta = amount.slice(0, amount.length-2);
    // npr 500.22
    prvi_dio_amounta = prvi_dio_amounta.replace('.');
    
    var jedinice = Number( prvi_dio_amounta );
    var decimale = Number( amount.slice(-2) )/100;
    var amount_num = jedinice + decimale;
    
    var discount_number = Number(discount);
    
    amount = ( (100 - discount_number)/100 ) * amount_num;
    
    amount = zaokruziIformatirajIznos(amount, 2);
    
    // OVO JE PROCES KAKO DA OD npr 2.500,55  DOBIJEM ------> 250055
    
    // dobijem 2.500
    jedinice = amount.split(',')[0];
    
    // ako postoji točka za tisućice onda je obriši !!!! -----> dobijem 2500
    jedinice = jedinice.replace('.', '');
    
    // dobijem 55
    decimale = amount.split(',')[1];
    
    // dobijem 250055
    amount = jedinice + decimale;

    
  };

  var prvi_dio_amounta = amount.slice(0, amount.length-2);
  // za svaki slučaj removaj decimalnu točku ako postoji 
  // npr 500.22
  prvi_dio_amounta = prvi_dio_amounta.replace('.');
  // želim dobiti broj sa zarezom i dvije decimale npr: 500,00 ------> ali zapisujem ga kao string
  var amount_string =  prvi_dio_amounta + ',' + amount.slice(-2);

  // ovo je pravi broj sa decimalnom točkom i dvije decimale
  var jedinice = Number( amount.slice(0, amount.length-2) );
  var decimale = Number( amount.slice(-2) )/100;
  var amount_num = jedinice + decimale;

  var time_now = Date.now();

  
  var token_pay = '';
  if (card_signature) token_pay = '-TOK';
  
  var ShoppingCartID = 
      user_number + '-' + offer + '-' + duration + '-' + pp + '-' + time_now + '-' + discount + '-' + discount_id + token_pay;

  var hash_string = 
      global.form_ShopID +
      global.form_SecretKey + 
      ShoppingCartID + 
      global.form_SecretKey + 
      amount + 
      global.form_SecretKey;
  
  
  var Signature = sha512(hash_string);

  var data = {
    ShoppingCartID: ShoppingCartID,
    TotalAmount: amount_string,
    Signature: Signature
  };
  
  var user_lang = global.all_user_langs[user_number];
  if (!user_lang) user_lang = 'hr';
  
  
  
  var page_html = '';
  
  if ( card_signature ) {
  
  Card.findOne({ 'card.Signature': card_signature })
  .exec(function(err, card) {
    if (err) {
      console.log('GREŠKA U DOBIVANJU USER CARDS');
      res.json({ success: false, msg: "GREŠKA U PRETRAZI TOKENA", err: err });
      return;
    };

    if ( card ) {
      var clean_kartica = convert_mongoose_object_to_js_object(card);
      var Token = clean_kartica.card.Token;
      var TokenNumber = clean_kartica.card.TokenNumber;
      
      page_html = create_card_page(data, user_lang, user_number, card_signature, Token, TokenNumber );
      // this is just exapmle of sending raw html
      res.set('content-type','text/html');
      res.send( page_html );  
      
    }
    else {
      res.json({ success: false, msg: "GREŠKA U find kartice za token", err: err });
      
    };
    
  }); // kraj querija u bazu za token kartice po signaturu
    
  }
  else {
    
    /* ---------------- AKO NIJE PLAĆANJE TOKENOM ---------------- */
    
    page_html = create_card_page(data, user_lang, user_number, false );
    // this is just exapmle of sending raw html
    res.set('content-type','text/html');
    res.send( page_html );  
  }

}; // KRAJ pay card page


// PRIMJER ODGOVARA KOD PLAĆANJA KARTICOM 
var odgovor_placanje_karticom = {
  "CustomerFirstname": "Toni",
  "CustomerSurname": "Kutlić",
  "CustomerAddress": "Rakušina 4",
  "CustomerCountry": "Hrvatska",
  "CustomerZIP": "10000",
  "CustomerCity": "Zagreb",
  "CustomerEmail": "tkutlic@gmail.com",
  "CustomerPhone": "092 3133 015",
  "ShoppingCartID": "1575542807233",
  "Lang": "HR",
  "DateTime": "5.12.2019. 11:47:58",
  "Amount": "17",
  "ECI": "0",
  "STAN": "67697",
  "Partner": "Pbz",
  "WsPayOrderId": "15b9216d-f18e-437b-bfa5-da77b115ecfb",
  "PaymentType": "MASTERCARD",
  "CreditCardNumber": "MASTERCARD------5007",
  "PaymentPlan": "0000",
  "Token": "b6ddade2-b27b-40ae-8d1b-068695f4760c",
  "TokenNumber": "5007",
  "TokenExp": "2212",
  "ShopPostedPaymentPlan": "0",
  "Success": "1",
  "ApprovalCode": "004202",
  "ErrorMessage": "",
  "Signature": "234f8f9e5b801b7211668f75f4f41ade"
}



function html_page_template(data, page_type, msg_selector) {

  
  var image = 'img/payment_success_slika.png';
  
  var current_page = '/payment_success'; 
      
  
  
  if ( page_type == 'cancel' ) {
    image = 'img/payment_cancel_slika.png';
    current_page = '/payment_cancel';
  };
  
  if ( page_type == 'error' ) {
    image = 'img/payment_error_slika.png';
    current_page = '/payment_error'
  };
  
  function set_current_page() {
    
    var curr_page_code = '';
    if ( current_page ) {
      curr_page_code = `var current_page = "${current_page}"`;
    } else {
      curr_page_code = `var current_page = null`;
    };
    return curr_page_code;
  };
  

  function set_msg_selector() {
    
    var msg_selector_code = '';
    if ( msg_selector ) {
      msg_selector_code = `var msg_selector = "${msg_selector}";`;
    } else {
      msg_selector_code = `var msg_selector = null;`;
    };
    return msg_selector_code;
  };
  

  var html = 
  `
  <!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
    <title>We Park Card Pay</title>
    <meta name="description" content="We Park Card Pay">
    <meta name="author" content="We Park Ltd">

  <meta name="viewport" content="viewport-fit=cover, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no, width=device-width" />
  <link rel="stylesheet" href="css/normalize.css">  
  <link rel="stylesheet" href="css/titllium_font_face.css">  
  <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/card_pay_page.css">
  


  </head>
  <body style="display: flex; align-items: center; justify-content: center; height: 100vh;">

    <div id="card_page_container">
    
    <img class="ws_pay_slika" src="${image}" style="max-width: 40%; height: auto;">
      
    <div id="ws_pay_info_box" style="text-align: center; font-size: 13px; padding: 10px; margin-top: 20px;">
      <!-- OVDJE S AJAXOM STAVLJAM TEXT KAD PROČITAM LOCAL STORAGE DA VIDIM KOJI JE JEZIK -->
      
    </div>

  <!--${ JSON.stringify(data) }-->
    </div>
  </body>
    
  <script src="js/jquery-3.2.1.min.js"></script>  
    
  <script>
  
    $(document).ready(function() {

      ${set_current_page()}
  
      ${set_msg_selector()}

      
      var user_lang = window.localStorage.getItem('user_lang');
      if ( !user_lang ) user_lang = 'hr';
      
      
      $.ajax({
          type: 'GET',
          url: (current_page + '_text/' + user_lang + '/' + (msg_selector || 'none') ),
          dataType: "json"
      })
      .done(function(response) {
          if ( response.success == true ) {
              $('#ws_pay_info_box').html( response.html );
          } else {
            console.error('ERROR IN AJAX WEHN GETTING TEXT FOR WSPAY RESPONESE PAGE  !!!!!');
          };
      })
      .fail(function(error) {
        console.error('ERROR IN AJAX WEHN GETTING TEXT FOR WSPAY RESPONESE PAGE  !!!!!');
      });
      
    }) // kraj document ready

    
  </script>  
    
  </html>
  `;   
  return html;
  
};


function send_error_response(req, res, token_obj, err, msg_selector, user_lang ) {
  
  if ( token_obj )  {
    // ako je token onda šalje json kao odgovor jer je poslan request s ajaxom
    
    var msg = "GREŠKA PRILIKOM SPREMANJA novog customer producta putem tokena";
    if ( msg_selector ) msg = WE_I18N.trans( (msg_selector + '_samo_text'), user_lang );
    res.json({ success: false, msg: msg, err: err });
    
  } else {
    // ako je plaćeno ws pay formom šaljem html
    res.set('content-type','text/html');
    res.send( html_page_template(err, 'error', msg_selector) ); 
  };  
  
};

function send_success_response(req, res, token_obj, msg_selector) {
  
  if ( token_obj ) {
    // odgovor za ajax
    res.json({ success: true, msg: "SPREMLJEN novi customer product putem tokena" });
  } else {
    // odgovor za ws pay formu
    res.set('content-type','text/html');
    res.send( html_page_template(req.query, 'success') ); 
  };
  
};




function get_user_r1_and_fiskaliziraj(user_number, token_obj, product_saved, novi_racun_br, req, res ) {
  
  User.findOne(
    { user_number: user_number }, 
    function(err, user) {
    
    if (err) {
      var user_lang = global.all_user_langs[user_number];
      if ( !user_lang ) user_lang = 'hr';
      
      send_error_response(req, res, token_obj, err, 'find_user_error', user_lang );
      return;
    };
    
    if (!user) {
      // if no user
      // this is just exapmle of sending raw html
      var user_lang = global.all_user_langs[user_number];
      if ( !user_lang ) user_lang = 'hr';
      
      send_error_response(req, res, token_obj, null, 'no_user_found', user_lang );
    } 
    else {
      
      var clean_user = convert_mongoose_object_to_js_object(user);
      
      fiskal_controller.run_fiskal(product_saved, novi_racun_br.count, clean_user);
      send_success_response(req, res, token_obj);
      
    }; // kraj ako je našao usera
    
  }); // end of User.findOne
  
}


function func_save_new_product(req, res, token_obj, user) {
  
  // default je url parametri tj request query ( node automatski pretvori url paramatre u query koji je json )
  var query_data = req.query;
  
  // ako je token onda je to bio post i data je u post bodiju
  if ( token_obj ) query_data = req.body;
        
  CustomerProduct.estimatedDocumentCount({}, function (err, products_count) {

    // ShoppingCartID je moj string koji sam kreirao ovako npr: "10009-pretplata-2-5-1575949193142"
    var user_num = query_data.ShoppingCartID.split('-')[0];
    // nadoplata ili pretplata
    var offer = query_data.ShoppingCartID.split('-')[1];
    // ako je pretplata koliko mjeseci 
    var duration = query_data.ShoppingCartID.split('-')[2];
    // ako je pretplata za koje je to parkiralište
    var curr_pp_id = query_data.ShoppingCartID.split('-')[3];
    
    // na 4 indexu u cart idju je običan javascript timestamp ---> Date.now()
        
    // ako je bio neki popust onda će postotak biti predzadnji u arrayu 
    var curr_discount = query_data.ShoppingCartID.split('-')[5];
    
    // ako je bio neki popust onda će dis code biti zadnji u arrayu 
    var curr_dis_code = query_data.ShoppingCartID.split('-')[6];
        
    
    // ako je bio neki popust onda će dis code biti zadnji u arrayu 
    var token_pay = query_data.ShoppingCartID.split('-')[7];

    // oba ova propertija su zapravo ista stvar - kratica za ime kreditne kuće kao na primjer AMEX ili VISA i slično
    
    // OVO JE VIŠE NE KORISTIM U NOVOM SISTEMU ZA TOKEN PREKO FORME !!!
    // var desc = ('CARD-' + query_data.PaymentType); // po defaultu je kartica i ona ima ime kartice u properiju payment type
    // if ( token_obj ) desc = ('TOKEN-' + token_obj.CreditCardName); // ako je to token plaćanje onda gledaj cerdit card name  
    
    // PO NOVOM SISTEMU ZA PLAĆANJE TOKENOM PREKO FORME !!!!!
    var desc = 'CARD-' + query_data.PaymentType;
    if ( token_pay ) desc = 'TOKEN-' + query_data.PaymentType;
    
    user_num = Number(user_num);
    
    if ( duration == '' || duration == 'none') {
      duration = 1;
    } else {
      duration = Number(duration);  
    };
    
    if ( curr_pp_id == '' ) curr_pp_id = null;

    var product_type = 'card';
    
    var valid_from = null;
    var valid_to = null;
    
    var time_now = Date.now();
    var date_now = new Date(); 
    
    var current_tag = null;

    
    // ako je offer pretplata onda treba upisati valid from  i valid to
    if ( offer == 'pretplata' ) {
      product_type = 'monthly';
      valid_from = time_now;
      // valid_to = new Date(yyyy, mnt, day, 0, 0, 0, 0);
      valid_to = time_now + (dana_30_u_ms * duration);
    };

    customer_products_controller.run_user_products_state(user_num)
    .then(
    function(user_products) {
      
      // BITNO !!!!!!!!!! ova funkcija vraća samo pretplate koje su važeće
      // dakle ne vraća zastarijele pretplate  koje su istekle !!!!
      
      var monthly_subs = user_products.monthly_arr;
      // sortiraj od manjeg prema većem
      monthly_subs.sort( window.by('valid_from', false, Number ) );
      
      // ako user već ima pretplatu za trenutno parkiralište ----> dakle produžuje pretplatu dok još ima aktivnu
      $.each( monthly_subs, function( monthly_index, monthly ) {
        // ako već ima pretplatu za ovo parkiralište onda pomakni valid to da bude nastavak već postojećeg valid to
        
        // novi start pretplate bi trebao biti kraj postojeće pretplate
        // ALI !!!  ako je kraj postojeće pretplate star tj davno prije je završio
        // onda startaj pretplatu od sada
        var novi_pocetak = monthly.valid_to || 0;
        var sada = Date.now();
        if ( sada > novi_pocetak ) novi_pocetak = sada;
        
        if ( monthly.pp_sifra == curr_pp_id ) {
          valid_to = novi_pocetak + (dana_30_u_ms * duration);
          
          current_tag = monthly.tag;
        };
        
      });

      // RacunCounter kolekcija ima samo jedan dokument i taj dokument ima samo jedan propery, a to je count !!!!!
      // inc znači increment  - u biti povećavam zadnji upisani counter za +1 !!!!
      // $ne: null je bezveze query koji će uvijek vratiti true jer count nikada nije null !!!
      
      var povecaj_racun_br = 1;
      // NEMOJ POVEĆAVATI BROJ AKO JE SERVER U DEV MODU !!!!!
      // NEMOJ POVEĆAVATI BROJ AKO JE SERVER U DEV MODU !!!!!
      if ( global.dev_or_prod == 'dev' ) povecaj_racun_br = 0;
      
      
      RacunCounter.findOneAndUpdate(
        { count: { $ne: null } }, 
        { $inc: { count: povecaj_racun_br } },
        { "new": true })
        .exec(function(err, novi_racun_br) {
        if (err) {
          console.log( 'NEŠTO NIJE U REDU S RACUN COUNTEROM !!!!!!!' );
          send_error_response(req, res, token_obj, err);
        };
        
        var iznos = Number( query_data.amount_string.replace(',', '.') );
        var cijena_bez_popusta = iznos;
        
        if ( curr_discount !== 'none' && curr_discount !== '' ) {
 
          cijena_bez_popusta = iznos / ((100 - Number(curr_discount))/100);
          cijena_bez_popusta = zaokruzi(cijena_bez_popusta, 2);
        };
        

        // ------------------- BITNO ---------------------
        // ------------------- BITNO ---------------------
        // ------------------- BITNO ---------------------
        
        // TODO - napravi bolje tj. treba bit tag varijabla 
        
        // ako je iznad kod provjere jel ima postojeću pretplatu za ovaj parking
        // našao u postojećoj pretplati neki tag npr TELE2 ili STED ili PRETPLATA
        // onda će uzeti taj tag koji već ima u aktivnoj pretplati
        
        // ali ako nema nikakav tag tj nema aktivnu pretplatu od prije 
        // i uzeo je Heinzelovu onda mu moram forcati da tag bude PRETPLATA
        if ( current_tag == null && curr_pp_id == '14' ) current_tag = 'PRETPLATA'; 
        
        // ------------------- BITNO ---------------------
        // ------------------- BITNO ---------------------
        // ------------------- BITNO ---------------------
        
        
        var new_customer_product_obj = {
            sifra: (products_count + 10 + ''),
            pp_sifra: curr_pp_id,
            code: query_data.ShoppingCartID,
            amount: cijena_bez_popusta,
            desc: desc,
            buy_time: time_now,
            user_number: user_num,
            is_free : false,
            price : iznos,
            type : product_type,
            valid_from : valid_from,
            valid_to : valid_to,
            balance : cijena_bez_popusta,
            tag: null
        };

        var new_product = new CustomerProduct(new_customer_product_obj);

        new_product.save(function(err, product_saved) {

          if (err) {

            send_error_response(req, res, token_obj, err);
            return;

          } 
          else {
            // nakon što sam spremio novi product sada trebam updateati discount objekt
            // naravno ako je user upisao valjani discount code ( to sam provjerio jel valjan u drugom requestu prije plaćanja )
            if ( curr_discount !== 'none' && curr_discount !== '' ) {

            Discount.findOneAndUpdate(
              { dis_code: curr_dis_code },
              { user_number: user_num, dis_amount: cijena_bez_popusta, buy_time: Date.now() }, 
              { "new": true })
              .exec(function(err, updated_discount) {

                if (err) {
                  console.error( 'NEŠTO NIJE U REDU S UPDATEOM DISCOUNT OBJEKTA !!!!!!!' );
                  send_error_response(req, res, token_obj, err);
                  return;
                };

                if ( !updated_discount ) {
                  console.error( 'NIJE PRONAŠAO DISCOUNT OBJEKT !!!!!!!' );
                  send_error_response(req, res, token_obj, null);
                  return;
                };

                if ( updated_discount ) {
                  var clean_discount = convert_mongoose_object_to_js_object(updated_discount);
                  get_user_r1_and_fiskaliziraj(user_num, token_obj, product_saved, novi_racun_br, req, res );
                  
                }; // kraj ako postoji updated diskount

              }); // kraj querija za update discounta u bazi

            }
            else {
              // ako nema discounta onda samo pokreni generiranje računa i fiskalizacije
              get_user_r1_and_fiskaliziraj(user_num, token_obj, product_saved, novi_racun_br, req, res );
              
            }; // kraj provjere jel ima ili nema discount
            
          }; // kraj ako je spremanje producta prošlo bez errora

        }); // kraj mongoose spramanja new product

      }); // kraj get new racun broj
     
      // kraj get user products
    })
    .catch(
    function(err) {

      send_error_response(req, res, token_obj, err);

    }); // kraj querija za userove producte
    


  }); // kraj customer product count estimation


}; // kraj save new product


function func_save_new_payment(new_card, req, res) {
   
    new_card.save(function(err, card_saved) {
      
      if (err) {
        // ako record plaćanja s karticom ima property token to znači da je plaćeno s tokenom
        // VRAĆAM JSON RESPONSE
        if ( new_card.token ) {
          res.json({ success: false, msg: "GREŠKA PRILIKOM SPREMANJA TOKEN UPLATE u cards collection", err: err });
        } else {
          // AKO NIJE PLAĆANJE TOKENOM 
          // VRAĆAM HTML RESPONSE
          res.set('content-type','text/html');
          res.send( html_page_template(err, 'error') );
        };
        
        return;
      } 
      else {
        
        // ako objekt u sebi ima token propery koji nije null to znači da je bilo plaćanje tokenom
        func_save_new_product(req, res, new_card.token);
        
        
      };
    });// end of spremi novi card   
};


var bla = {
  "CustomerFirstname": "Toni",
  "CustomerSurname": "Kutlić",
  "CustomerAddress": "Rakušina 4",
  "CustomerCountry": "Hrvatska",
  "CustomerZIP": "10000",
  "CustomerCity": "Zagreb",
  "CustomerEmail": "tkutlic@gmail.com",
  "CustomerPhone": "092 3133 015",
  "ShoppingCartID": "1009-nadoplata---1576725502829",
  "Lang": "HR",
  "DateTime": "19.12.2019. 4:21:38",
  "Amount": "20",
  "ECI": "6",
  "STAN": "68602",
  "Partner": "Pbz",
  "WsPayOrderId": "9d59218a-c129-42ac-b6f8-49e76e003d37",
  "PaymentType": "VISA",
  "CreditCardNumber": "457418------9023",
  "PaymentPlan": "0000",
  "Token": "c768b989-899a-4136-84b8-9b54ace791c6",
  "TokenNumber": "9023",
  "TokenExp": "2212",
  "ShopPostedPaymentPlan": "000",
  "Success": "1",
  "ApprovalCode": "073275",
  "ErrorMessage": "",
  "Signature": "bbb77724e1ee6b049203e2691009c7f6"
}


function save_card_payment_and_customer_product(user, req, res) {
  
  // ako postoji property token tj ako je user kliknuo da želi spremiti token
  // onda moram spremiti zapis u bazu
  if ( req.query.Token ) {

    var last_4_numbers = req.query.CreditCardNumber.slice(-4);
    var card_name = req.query.PaymentType;
    
    // drop list name je ime koje se prikazuje useru u mobitelu
    req.query.drop_list_name = card_name + '-----' + last_4_numbers;
    req.query.type = 'CARD';

    // provjeri jel ovaj user već ima spremljenu ovu karticu .... nema smisla da istu karticu spremam više puta !!!!
    // također gledam i exp date koji je jedan exp od same kartice ---> ponekad kartica ostane ista ali se produži trajanje
    
    
    // TODO - ovo moram kasnije napraviti da u listi za tokene ne prikazujem staru verziju kartice koja je istekla !!!!!!
    
    
    Card.find({ 
      user_number: user.user_number,
      'card.CreditCardNumber': req.query.CreditCardNumber,
      'card.type': 'CARD',
      'card.TokenExp': req.query.TokenExp /* obavezno moram tražiti i exp jer možda user ima noviju verziju kartice s novim exp datumom */
      })
      .exec(function(err, cards) {
      
        if (err) {
          send_error_response(req, res, null, err);
          return;
        };

        // ako je user već platio ovom karticom onda samo pošalji response da je plaćeno
        if ( cards.length > 0 ) {
          
          /*
          var clean_cards = [];
          var max_token_exp = 0;
          var newest_card = null;
          $.each(cards, function(card_ind, card) {
            var cl_card = convert_mongoose_object_to_js_object(card);
            
            if ( Number(cl_card.card.TokenExp) > max_token_exp ) {
              max_token_exp = Number(cl_card.card.TokenExp);
              newest_card = cl_card;
            };
            
            clean_cards.push(cl_card);
          });
          */
          
          // iako je već plaćao ovom karticom user je iz nekog razloga odlučio opet platiti karticom A NE TOKENOM ZA TU KARTICU
          // možda prošli puta nije skužio da je označio da želi token 
          // i sad opet plaća putem ws pay-a i opet je označio token
          // bez obzira moram spremiti koji je product kupio !!!!!
          func_save_new_product(req, res);
          return;
        }
        else {
          
          // ako je ovo prvo plaćanje ovom karticom onda moram spremiti tu karticu
          var new_card_object = {
            user_number: user.user_number,
            card: req.query,
            token: null
          };
          var new_card = new Card(new_card_object);
          func_save_new_payment(new_card, req, res);
          
        };
      });
  } 
  else {
    // ako je user platio karticom ali nije izabrao spremanje tokena onda samo pokreni spramanje producta
    func_save_new_product(req, res);
  };
  
};



exports.payment_success = function(req, res) {
  
  var odgovor = {
    "CustomerFirstname": "Toni",
    "CustomerSurname": "Kutlić",
    "CustomerAddress": "Rakušina 4",
    "CustomerCountry": "Hrvatska",
    "CustomerZIP": "10000",
    "CustomerCity": "Zagreb",
    "CustomerEmail": "tkutlic@gmail.com",
    "CustomerPhone": "092 3133 015",
    "ShoppingCartID": "10009-pretplata-1-4-1576792066849",
    "Lang": "HR",
    "DateTime": "19.12.2019. 22:51:07",
    "Amount": "405",
    "ECI": "1",
    "STAN": "68663",
    "Partner": "Pbz",
    "WsPayOrderId": "26de32b3-17f9-4f63-b4b3-bd7e552c33c5",
    "PaymentType": "MASTERCARD",
    "CreditCardNumber": "548866------5007",
    "PaymentPlan": "0000",
    "Token": "1d083084-e235-4115-8440-81f3a934087f",
    "TokenNumber": "5007",
    "TokenExp": "2212",
    "ShopPostedPaymentPlan": "000",
    "Success": "1",
    "ApprovalCode": "949285",
    "ErrorMessage": "",
    "Signature": "91d04f28621d57cc037c64ee1a0b21ed",

  };
  
  
var odgovor_NEW_TOKEN_preko_forme = {  
  
  Amount: "20",
  ApprovalCode: "677647",
  CreditCardNumber: "457418------9023",
  CustomerAddress: "Zumberacka 32a",
  CustomerCity: "Zagreb",
  CustomerCountry: "Croatia",
  CustomerEmail: "tech@wepark.eu",
  CustomerFirstname: "Karlo",
  CustomerPhone: "0919597890",
  CustomerSurname: "Starcevic",
  CustomerZIP: "00000",
  DateTime: "20201204125824",
  ECI: "",
  ErrorMessage: "",
  Lang: "HR",
  Partner: "Pbz",
  PaymentPlan: "0000",
  PaymentType: "VISA",
  STAN: "103531",
  ShopPostedLang: "HR",
  ShopPostedPaymentPlan: "0000",
  ShoppingCartID: "777-nadoplata---1607083088926--",
  Signature: "49f1fd5a41c736e53a239cbe85ec8a54bb2d64621610f2b2297f8d5960197d20c76286a2bb15195ab37eb17aeb7df6c4172e7bc3efae512ed2e96a706dcf607c",
  Success: "1",
  WsPayOrderId: "5c73b94d-c885-4d33-953d-189d9edfe285",
  
}
  
  

  if ( Object.keys(req.query).length == 0 ) {
    res.send({ success: false, msg: 'Query data is missing !!!!!!!!' }); 
    return;
    
  };
  
 
/*  
 ShopID
 SecretKey
 ShoppingCartID 
 SecretKey
 Success
 SecretKey
 ApprovalCode
 SecretKey
*/  
  
  var sign_provjera = 
      
      global.form_ShopID + 
      global.form_SecretKey + 
      req.query.ShoppingCartID +
      global.form_SecretKey + 
      req.query.Success +
      global.form_SecretKey + 
      req.query.ApprovalCode +
      global.form_SecretKey;
  
  sign_provjera = sha512(sign_provjera);
  
  if ( sign_provjera !== req.query.Signature ) {
    
    res.set('content-type','text/html');
    res.send( html_page_template('!!!!!!!!!!!!!!!SIGNATURE IS INVALID !!!!!', 'error') ); 
    
    return;
  };
  
  /*
  OVO JE PRIMJER URL-a
  
  localhost:8080/payment_success?CustomerFirstname=Toni&CustomerSurname=Kutli%c4%87&CustomerAddress=Raku%c5%a1ina+4&CustomerCountry=Hrvatska&CustomerZIP=10000&CustomerCity=Zagreb&CustomerEmail=tkutlic%40gmail.com&CustomerPhone=092+3133+015&ShoppingCartID=10009-nadoplata---1576615837374-20-1234&Lang=HR&DateTime=17.12.2019.%2021:51:27&Amount=60,55&ECI=6&STAN=68514&Partner=Pbz&WsPayOrderId=9ad80597-2f48-4215-b7c2-9c2b91ad916b&PaymentType=VISA&CreditCardNumber=457418------9023&PaymentPlan=0000&Token=54fa6105-1e39-42bd-87cc-d175c80b8811&TokenNumber=9023&TokenExp=2212&ShopPostedPaymentPlan=0&Success=1&ApprovalCode=049983&ErrorMessage=&Signature=c4528bd982a66b58a46d13b9ca18244f
  
  
  */
  
  
/*  
localhost:8080/payment_success?CustomerFirstname=Toni&CustomerSurname=Kutli%c4%87&CustomerAddress=Raku%c5%a1ina+4&CustomerCountry=Hrvatska&CustomerZIP=10000&CustomerCity=Zagreb&CustomerEmail=tkutlic%40gmail.com&CustomerPhone=092+3133+015&ShoppingCartID=10009-pretplata-1-4-1576792731818-30-1234&Lang=HR&DateTime=19.12.2019.%2022:59:34&Amount=100,55&ECI=&STAN=68665&Partner=Pbz&WsPayOrderId=1ab274f4-7030-4e85-9707-19d6521c1890&PaymentType=AMEX&CreditCardNumber=377504-----1000&PaymentPlan=0000&Token=64613d19-5e9a-458c-b068-284a88fb3049&TokenNumber=1000&TokenExp=2206&ShopPostedPaymentPlan=000&Success=1&ApprovalCode=119573&ErrorMessage=&Signature=d8e3608e03752223e18216f1c59b676a
*/

  
  var sdsdsd = {
    "CustomerFirstname": "Toni",
    "CustomerSurname": "Kutlić",
    "CustomerAddress": "Rakušina+4",
    "CustomerCountry": "Hrvatska",
    "CustomerZIP": "10000",
    "CustomerCity": "Zagreb",
    "CustomerEmail": "tkutlic@gmail.com",
    "CustomerPhone": "092+3133+015",
    "ShoppingCartID": "10009-pretplata-1-4-1576792731818",
    "Lang": "HR",
    "DateTime": "19.12.2019. 22:59:34",
    "Amount": "405,23",
    "ECI": "",
    "STAN": "68665",
    "Partner": "Pbz",
    "WsPayOrderId": "1ab274f4-7030-4e85-9707-19d6521c1890",
    "PaymentType": "AMEX",
    "CreditCardNumber": "377504-----1000",
    "PaymentPlan": "0000",
    "Token": "64613d19-5e9a-458c-b068-284a88fb3049",
    "TokenNumber": "1000",
    "TokenExp": "2206",
    "ShopPostedPaymentPlan": "000",
    "Success": "1",
    "ApprovalCode": "119573",
    "ErrorMessage": "",
    "Signature": "d8e3608e03752223e18216f1c59b676a"
  }
  
  
  
  console.log( req.query );
  
  // ShoppingCartID je moj string koji sam kreirao ovako npr: "10009-pretplata-2-5-1575949193142"
  var user_number = req.query.ShoppingCartID.split('-')[0];
  user_number = Number(user_number);
  
  // ovo je broj sa zarezom i dvije decimale npr: 500,00 ------> ali dobijem ga kao string
  var amount = req.query.Amount;
 
  var znamenke = null;
  var decimale = null;
  
  // ako postoji zarez u stringu znači da ima decimale
  if ( amount.indexOf(',') > -1) {
    
    znamenke = amount.split(',')[0];
    
    // ako mi ws pay kojim slučajem vraća broj sa točkom za tisućice onda obriši tu točku !!!!!
    znamenke = znamenke.replace('.', '');
    
    decimale = amount.split(',')[1];
    
    if ( decimale.length == 1 ) decimale = decimale + '0';
    
  } else {
    // problem je što ako je cijeli broj onda ne dobijem decimale !!!!
    znamenke = amount;
    decimale = '00';
    
  };
  
  req.query.amount_string = znamenke + ',' + decimale;
  
  
    // find the user
  User.findOne(
    { user_number: user_number }, 
    function(err, user) {
    
    if (err) {
      
      var user_error_text  = 

`
<h3>Došlo je do greške!</h3>
Baza korisnika trenutno ne radi :(<br>
Molimo vas nazovite broj:<br>
<a href="tel:00385923133015" 
style="padding: 5px;
text-decoration: none;
color: black;
background-color: #fff;
margin-top: 10px;
display: block;
border-radius: 20px;">00385 92 3133 015</a>

`;
      
      res.set('content-type','text/html');
      res.send( html_page_template(req.query, 'error', 'user_find_error' ) );       
      
      return;
      
    };
    
    if (!user) {
      // if no user
      // this is just exapmle of sending raw html
      
      var no_user_text  = 

`
<h3>Došlo je do greške!</h3>
Aplikacije nije pronašla korisnika.<br>
Molimo vas nazovite broj:<br>
<a href="tel:00385923133015" 
style="padding: 5px;
text-decoration: none;
color: black;
background-color: #fff;
margin-top: 10px;
display: block;
border-radius: 20px;">00385 92 3133 015</a>

`;
      res.set('content-type','text/html');
      res.send( html_page_template(req.query, 'error', 'no_user' ) ); 
    } 
    else {
      
      var clean_user = convert_mongoose_object_to_js_object(user);
      
      save_card_payment_and_customer_product(clean_user, req, res);
      
    }; // kraj ako je našao usera
    
  }); // end of User.findOne
 
}; // KRAJ payment success


exports.payment_cancel = function(req, res) {
  
  
  if ( Object.keys(req.query).length == 0 ) {
    res.send({ success: false, msg: 'Query data is missing !!!!!!!!' }); 
    return;
    
  };

  
  
  console.log( req.query );
  // this is just exapmle of sending raw html
  res.set('content-type','text/html');
  res.send( html_page_template(req.query, 'cancel') );  
 
}; // KRAJ payment cancel 


exports.payment_error = function(req, res) {
  
  if ( Object.keys(req.query).length == 0 ) {
    res.send({ success: false, msg: 'Query data is missing !!!!!!!!' }); 
    return;
  };
  
  console.log( req.query );
  // this is just exapmle of sending raw html
  res.set('content-type','text/html');
  res.send( html_page_template(req.query, 'error') );  
 
}; // KRAJ payment error 


exports.payment_callback = function(req, res) {
  
  console.log( req.query );
  
  // this is just exapmle of sending raw html
  // res.set('content-type','text/html');
  // res.send( html_page_template(req.query, success_text, 'success') ); 
 
  res.send({ msg: 'Data received'  }); 
  
}; // KRAJ payment callback 





exports.payment_success_text = function(req, res) {
 
  var lang_selector = 'success_payment_text';
  var user_lang = req.params.user_lang;
  var msg_selector = req.params.msg_selector;
  if ( msg_selector && msg_selector !== 'none' ) lang_selector = msg_selector;
  
  
  res.json( { success: true, html: WE_I18N.trans( lang_selector, user_lang ) });
  
}; // KRAJ payment success text



exports.payment_cancel_text = function(req, res) {
 
  var lang_selector = 'cancel_payment_text';
  var user_lang = req.params.user_lang;
  var msg_selector = req.params.msg_selector;
  if ( msg_selector && msg_selector !== 'none' ) lang_selector = msg_selector;
  
  
  res.json( { success: true, html: WE_I18N.trans( 'cancel_payment_text', user_lang ) });
  
}; // KRAJ payment success text



exports.payment_error_text = function(req, res) {
 
  var lang_selector = 'error_payment_text';
  var user_lang = req.params.user_lang;
  var msg_selector = req.params.msg_selector;
  if ( msg_selector && msg_selector !== 'none' ) lang_selector = msg_selector;
  
  
  res.json( { success: true, html: WE_I18N.trans( 'error_payment_text', user_lang )} );
  
}; // KRAJ payment success text





function send_token_data(json_obj, user_number, req, res) {
  
  
  axios.post(global.wspay_token_url, json_obj)
  .then(function (response) {
    
    var data = response.data;
    
    console.log('response data od token plaćanja:')
    console.log(data);
    

    /*

  ActionSuccess: "1"
  ApprovalCode: "614998"
  Approved: "1"
  CAVV: null
  CreditCardName: "MMASTERCAR"
  CustomerAddress: "Rakusina 4"
  CustomerCity: "Zagreb"
  CustomerCountry: "Hrvatska"
  CustomerEmail: "tkutlic@gmail.com"
  CustomerFirstName: "Toni"
  CustomerLastName: "Kutlic"
  CustomerPhone: "092 3133 015"
  CustomerZIP: "10000"
  ECI: ""
  ErrorMessage: null
  MaskedPan: "548866******5007"
  Partner: "Pbz"
  PaymentPlan: "0000"
  STAN: "67965"
  ShoppingCartID: "10009-nadoplata---1575949193142"
  Signature: "29cf693409e1d0162683be98e8c9fe12"
  TotalAmount: "22200"
  TransactionDateTime: "20191210043953"
  UCAF: null
  WsPayOrderId: "add712d2-200c-4577-8217-3415ba1d7359"
  XID: null

    */

    // ako nema propertija eror message !!! onda je prošla naplata
    if ( !data.ErrorMessage ) {

      
      
/*

• ShopID
• SecretKey
• ShoppingCartID
• SecretKey
• Accepted
• SecretKey
• ApprovalCode
• SecretKey

*/

 
    var sign_provjera = 
      
      global.token_ShopID + 
      global.token_SecretKey + 
      data.ShoppingCartID +
      global.token_SecretKey + 
      data.Approved +
      global.token_SecretKey + 
      data.ApprovalCode +
      global.token_SecretKey;
  
      
      sign_provjera = sha512(sign_provjera);
      
      
      if ( sign_provjera !== data.Signature ) {
        res.json({ success: false, msg: "SIGNATURE NIJE DOBAR", err: "SIGNATURE NIJE DOBAR" });
        return;
      };
      
      // kada je prvi put plaćanje karticom onda u card propertiju stoji cijeli veliki objekt
      // i u tom objektu postavim property type da bude CARD
      // ali kada se radi o TOKEN PLAĆANJU onda samo stavim taj jedan jedini property type i postavim ga na TOKEN
      var new_token_object = {
        user_number: user_number,
        card: {type: 'TOKEN'},
        token: data
      };
      var new_token = new Card(new_token_object);
      func_save_new_payment(new_token, req, res);

      
      
      
      
    } else {
      res.json({ success: false, msg: "GREŠKA PRILIKOM TOKEN UPLATE", err: data.ErrorMessage });
    };    
    
    
    
  })
  .catch(function (error) {
    console.log(error);
    res.json({ success: false, msg: "GREŠKA PRILIKOM TOKEN UPLATE", err: error });
    
  });
  
  
  /*
  $.ajax({
      type: 'POST',

      url: global.wspay_token_url,

      data: JSON.stringify(json_obj),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
  })
  .done(function(response) {

  })
  .fail(function(error) {
      
  });
  */

  
};


exports.pay_with_token = function(req, res) {

  
  // kad placam tokenom onda saljem POST ajax u obliku jsona
  // zato je query u bodiju od requesta
  console.log( req.body );
  
  if (!req.body) {
    res.send({ success: false, error: 'No req body!' }); 
    return;
  };

  
  // sign je string koji sam dobio kada je user prvi puta plaćao karticom preko ws forme
  var sign = req.body.sign;
  
  if (!sign) {
    res.send({ success: false, error: 'No token signature!' }); 
    return;
  };
  
  
  var user_number = req.body.user_number;
  
  if (!user_number) {
    res.send({ success: false, error: 'No user!' }); 
    return;
  };
  
  var offer = req.body.offer;
  if (!offer) {
    res.send({ success: false, error: 'Offer data missing!' }); 
    return;
  };  
  
  var duration = req.body.duration;
  if (!duration) {
    res.send({ success: false, error: 'Duration data missing!' }); 
    return;
  };
  
  if (duration == 'none') duration = '';
  
  var pp = req.body.pp;
  if (!pp) {
    res.send({ success: false, error: 'Parking data missing!' }); 
    return;
  };
  
  if (pp == 'none') pp = '';
  
  
  var discount = req.body.discount;
  
  if (!discount) {
    res.send({ success: false, error: 'Parking data missing!' }); 
    return;
  };
  if (discount == 'none') discount = '';
  
  
  
  var discount_id = req.body.discount_id;
  
  if (!discount_id) {
    res.send({ success: false, error: 'Parking data missing!' }); 
    return;
  };
  if (discount_id == 'none') discount_id = '';
  
  
  // ovo je broj sa dvije decimale ALI bez točke ili zareza npr 50000 ------> zapisujem ga kao string
  var amount = req.body.amount; 
  
  if (!amount) {
    res.send({ success: false, error: 'Amount data missing!' }); 
    return;
  };
  
  if ( discount !== '' ) {
    
    var prvi_dio_amounta = amount.slice(0, amount.length-2);
    prvi_dio_amounta = prvi_dio_amounta.replace('.');

    
    var jedinice = Number( prvi_dio_amounta );
    var decimale = Number( amount.slice(-2) )/100;
    
    var amount_num = jedinice + decimale;
    
    var discount_number = Number(discount);    

    amount = ((100 - discount_number)/100) * amount_num;
    
    amount = zaokruziIformatirajIznos(amount, 2);
    
    // vrati nazad u oblik bez decimalne točke kao npr 2.500,55 ------> 250055
    jedinice = amount.split(',')[0];
    // ako postoji točka za tisućice onda je obriši !!!!
    jedinice = jedinice.replace('.', '');
    
    decimale = amount.split(',')[1];
    amount = jedinice + decimale;
    
  };
  
  
  
  var prvi_dio_amounta = amount.slice(0, amount.length-2);
  prvi_dio_amounta = prvi_dio_amounta.replace('.');
  // želim dobiti broj sa zarezom i dvije decimale npr: 500,00 ------> ali zapisujem ga kao string
  var amount_string =  prvi_dio_amounta + ',' + amount.slice(-2);

  // ovo je pravi broj sa decimalnom točkom i dvije decimale

  var jedinice = Number( prvi_dio_amounta );
  var decimale = Number( amount.slice(-2) )/100;
  var amount_num = jedinice + decimale;

  var unix_time = Date.now();
  var time_now = new Date(unix_time);

  var ShoppingCartID = 
      user_number + '-' + offer + '-' + duration + '-' + pp + '-' + unix_time + '-' + discount + '-' + discount_id + '-TOK';
  
  // upisaujem ove dvije stvari u postojeći body od ovog posta
  req.body.ShoppingCartID = ShoppingCartID;
  req.body.amount_string = amount_string;

  // var ShoppingCartID = 10009 + '-' + 'nadoplata' + '-' + '' + '-' + '' + '-' + unix_time;
  var TotalAmount = amount;
  
  var hash_string = 
      global.token_ShopID +
      global.token_SecretKey +
      ShoppingCartID + 
      global.token_SecretKey +
      amount +
      global.token_SecretKey;
  
  
  var Signature = sha512(hash_string);


  var yyyy = time_now.getFullYear() + '';
  
  var mon = time_now.getMonth() + 1 + '';
  if ( mon.length == 1 ) mon = '0' + mon;
  
  var dd = time_now.getDate() + '';
  if ( dd.length == 1 ) dd = '0' + dd;

  var hh = time_now.getHours() + '';
  if ( hh.length == 1 ) hh = '0' + hh;

  var mm = time_now.getMinutes() + '';
  if ( mm.length == 1 ) mm = '0' + mm;

  var ss = time_now.getSeconds() + '';
  if ( ss.length == 1 ) ss = '0' + ss;

  var date_and_time = yyyy + mon + dd + hh + mm + ss;

  /*
  var odgovor_primjer = {
    "CustomerFirstname": "Toni",
    "CustomerSurname": "Kutlić",
    "CustomerAddress": "Rakušina 4",
    "CustomerCountry": "Hrvatska",
    "CustomerZIP": "10000",
    "CustomerCity": "Zagreb",
    "CustomerEmail": "tkutlic@gmail.com",
    "CustomerPhone": "092 3133 015",
    "ShoppingCartID": "10009-nadoplata---1575948683451",
    "Lang": "HR",
    "DateTime": "10.12.2019. 4:32:01",
    "Amount": "50",
    "ECI": "0",
    "STAN": "67964",
    "Partner": "Pbz",
    "WsPayOrderId": "e27c47d9-93ff-4cc0-882f-827f198fe315",
    "PaymentType": "MASTERCARD",
    "CreditCardNumber": "548866------5007",
    "PaymentPlan": "0000",
    "Token": "5a80b425-2c15-4613-96f4-edc5589f7b90",
    "TokenNumber": "5007",
    "TokenExp": "2212",
    "ShopPostedPaymentPlan": "0",
    "Success": "1",
    "ApprovalCode": "459724",
    "ErrorMessage": "",
    "Signature": "82c32bb4b2846288ce5804d2d2a46862",
    "drop_list_name": "MASTERCARD-----5007"
  };
  
  */



  
  var json_obj = {
      "Version": "2.0",
      "ShopID": "WEPARKTOK",
      "ShoppingCartID": ShoppingCartID,
      "Signature": Signature, 
      "PaymentPlan": "0000",
      "TotalAmount": TotalAmount,
      "DateTime": date_and_time
    };
  
  Card.findOne({ 'card.Signature': sign })
  .exec(function(err, card) {
    
    if (err) {
      console.log('GREŠKA U DOBIVANJU USER CARDS');
      res.json({ success: false, msg: "GREŠKA U find kartice za token", err: err });
      return;
    };


    if ( card ) {
  
      var clean_kartica = convert_mongoose_object_to_js_object(card);
      
      json_obj.Token = clean_kartica.card.Token;
      json_obj.TokenNumber = clean_kartica.card.TokenNumber;

      // nije obavezno ali eto
      
      /*
      
      json_obj.CustomerFirstname = clean_kartica.card.CustomerFirstname;
      json_obj.CustomerSurname = clean_kartica.card.CustomerSurname;
      json_obj.CustomerAddress = clean_kartica.card.CustomerAddress;
      json_obj.CustomerCountry = clean_kartica.card.CustomerCountry;
      json_obj.CustomerZIP = clean_kartica.card.CustomerZIP;
      json_obj.CustomerEmail = clean_kartica.card.CustomerEmail;
      json_obj.CustomerPhone = clean_kartica.card.CustomerPhone;
      
      */
      
      send_token_data(json_obj, user_number, req, res);

    }
    else {
      res.json({ success: false, msg: "GREŠKA U find kartice za token", err: err });
    };
    
  }); // kraj querija u bazu za token kartice po signaturu
  
  
  
    
};



function update_voucher_product(clean_voucher, product_update_objekt, req, res, offer, duration) {
  

  CustomerProduct.findOneAndUpdate(
    { _id: clean_voucher._id },
    product_update_objekt,
    { new: true }, 
    function(err, customer_product) {

    if (err) {
      res.json({ success: false, msg: "Problem u updatanju vouchera.", err: err});
      return;
    }
    else {
      
      var poruka = '';
      
      if ( offer == 'nadoplata' ) {
        
        var user_lang = global.all_user_langs[product_update_objekt.user_number];
        if ( !user_lang ) user_lang = 'hr';
        
        poruka = WE_I18N.trans( 'uspjesno_iskoristen_voucher', user_lang, clean_voucher.amount );
        
      };

      if ( offer == 'pretplata' ) {
        
        var pp_name = '';
        
        global.park_places_list.forEach(function(pp_obj, pp_index) {
          if ( pp_obj.pp_sifra == product_update_objekt.pp_sifra ) pp_name = pp_obj.pp_name;
        });
        
        var user_lang = global.all_user_langs[product_update_objekt.user_number];
        if ( !user_lang ) user_lang = 'hr';
        
        var poruka_data = { pp_name: pp_name, duration: duration*30 };
        
        poruka = WE_I18N.trans( 'uspjesno_iskoristen_voucher_za_pretplatu', user_lang, poruka_data );
        
        
        // poruka = `Uspješno je iskoristili voucher za pretplatu na parkiralište ${pp_name} za sljedećih ${duration*30} dana!`;
        
      };

      res.json({ success: true, msg: poruka})
      
    };  

  });
      
        
  
  
};


exports.pay_with_voucher = function(req, res) {

  
  // kad placam tokenom onda saljem POST ajax u obliku jsona
  // zato je query u bodiju od requesta
  console.log( req.body );
  
  
  
  
  if (!req.body) {
    res.send({ success: false, error: 'No req body!' }); 
    return;
  };

  var user_number = req.body.user_number;
  
  if (!user_number) {
    res.send({ success: false, error: 'No user!' }); 
    return;
  };
  
  var offer = req.body.offer;
  if (!offer) {
    res.send({ success: false, error: 'Offer data missing!' }); 
    return;
  };  
  
  var duration = req.body.duration;
  if (!duration) {
    res.send({ success: false, error: 'Duration data missing!' }); 
    return;
  };
  
  if (duration == 'none') duration = '';
  
  var pp = req.body.pp;

  
  if (!pp) {
    res.send({ success: false, error: 'Parking data missing!' }); 
    return;
  };
  if (pp == 'none') pp = '';

  
  
  var cijena_pretplate = 0;
  
  // ako park place id postoji tj ako je user izabrao pretplatu na neko parkiralište
  if ( pp !== '' ) {
    
    var price = req.body.price;
    if ( price == '' || price == 'none' ) {
      res.send({ success: false, error: 'Price data missing!' }); 
      return;
    };
      cijena_pretplate = Number(price);
  };
  
  // ovo je broj sa vije decimale ALI bez točke ili zareza npr 50000 ------> zapisujem ga kao string
  var code = req.body.code.trim(); 
  
  if (!code) {
    res.send({ success: false, error: 'Voucher code missing!' }); 
    return;
  };
  
  CustomerProduct.findOne({ code: code, type: 'voucher'})
  .exec(function(err, voucher_product) {
    
    if (err) {
      console.log('GREŠKA U find producta po voucher code');
      res.json({ success: false, msg: "GREŠKA U find producta po voucher code", err: err });
      return;
    };

    if ( !voucher_product ) {
      res.json({ success: false, msg: "Nije pronađen voucher s tom šifrom." });
      return;
    };
    
    var clean_voucher = convert_mongoose_object_to_js_object(voucher_product);

    if ( clean_voucher.buy_time !== null ) {
      res.json({ success: false, msg: "Voucher je već iskorišten!"});
      return;
    };
    
    
    if ( offer == 'pretplata' ) {

      var iznos_vouchera = clean_voucher.amount;
      
      if ( iznos_vouchera < cijena_pretplate ) {
        
        var user_lang = global.all_user_langs[user_number];
        if ( !user_lang ) user_lang = 'hr';
        
        var poruka_data = { iznos_vouchera: iznos_vouchera, cijena_pretplate: cijena_pretplate };
        
        res.json({
          success: false,
          msg: WE_I18N.trans( 'nema_dovoljno_love_na_voucheru', user_lang, poruka_data )
        });
        return;  
      };

    };
    
    customer_products_controller.run_user_products_state(user_number)
    .then(
    function(user_products) {

      var time_now = Date.now();

      var product_update_objekt = {
        user_number: user_number,
        buy_time: time_now,
        balance: clean_voucher.amount
      };


      if ( offer == 'pretplata' ) {
        
        duration = duration ? Number(duration) : 1
        
        var valid_to = time_now + (dana_30_u_ms * duration);
        
        // BITNO !!!!!!!!!! ova funkcija vraća samo pretplate koje su važeće
        // dakle ne vraća zastarijele pretplate !!!!
        var monthly_subs = user_products.monthly_arr;
        monthly_subs.sort( window.by('valid_from', false, Number ) );

        // ako user već ima pretplatu za trenutno parkiralište ----> dakle produžuje pretplatu dok još ima aktivnu
        $.each( monthly_subs, function( monthly_index, monthly ) {
          // ako već ima pretplatu za ovo parkiralište onda pomakni valid to da bude nastavak već postojećeg valid to
          if ( monthly.pp_sifra == (pp + '') ) {
            valid_to = monthly.valid_to + (dana_30_u_ms * duration);
          };
        });


        product_update_objekt = { 
          type: 'monthly',
          desc: ('VOUCHER-'+clean_voucher.desc),
          user_number: user_number,
          buy_time: time_now,
          balance: clean_voucher.amount,
          pp_sifra: pp,
          valid_from: time_now,
          valid_to: valid_to
        };
        
      };

      update_voucher_product(clean_voucher, product_update_objekt, req, res, offer, duration);

    })
    .catch(
    function(err) {
      
      console.log('GREŠKA prilkom provjere user producta u kupnji vouchera');
      res.json({ success: false, msg: "GREŠKA prilkom provjere user producta u kupnji vouchera", err: err });

    }); // kraj func za get user products

    
  }); // kraj pronalazka voucher producta s tim codom
  
    
};



