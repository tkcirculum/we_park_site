  
global.all_shellys = {
  tzm_shelly_1: { up: 'E2d733', down: 'E2d706' }
};

global.lista_rampi = [
  { ime: 'rampa_tzm', ssh_port: 45001, vnc_port: 55001 },
  { ime: 'zero_1', ssh_port: 45002, vnc_port: 55002 }
];


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var nodemailer = require('nodemailer');

var path = require("path");

var ParkPlace = mongoose.model('ParkPlace');
var ParkAction = mongoose.model('ParkAction');

var User = mongoose.model('User');

var Solar = mongoose.model('Solar');
var SolarDay = mongoose.model('SolarDay');
var Camera = mongoose.model('Camera');

var path = require('path');

var server_settings = require('../../settings');

var jwt = require('jsonwebtoken');

var fs = require('fs');

var request = require('request');

var FormData = require('form-data');


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);

var { exec } = require("child_process");
var { spawn } = require("child_process");

var axios = require('axios').default;

axios.defaults.adapter = require('axios/lib/adapters/http');




// ADDITIONAL OPTIONS FOR NATIVE JAVASCRIPT SORT ------- from http://jsfiddle.net/gfullam/sq9U7/  
window.by = function(path, reverse, primer, then) {
  var get = function (obj, path) {
      if (path) {
        path = path.split('.');
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        };
        return obj[path[len]];
      }
      return obj;
    },
    prime = function (obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function (a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B) ? -1 :
      (A > B) ? 1 :
      (typeof then === 'function') ? then(a, b) : 0
    ) * [1, -1][+!!reverse];
  };
};




function zaokruzi(broj, brojZnamenki) {

  var broj = parseFloat(broj);

  if ( typeof brojZnamenki == 'undefined' ) {
   brojZnamenki = 0;
  };
  
  var multiplicator = Math.pow(10, brojZnamenki);
  broj = parseFloat((broj * multiplicator).toFixed(11));
  var test =(Math.round(broj) / multiplicator);
  // + znak ispred je samo js trik da automatski pretvori string u broj
  return +(test.toFixed(brojZnamenki));
  
};


function cit_white_space_trim(x) {
  return x.replace(/^\s+|\s+$/gm, '');
};

function wait_for(condition, callbackFunction, period, expired_callback) {

  var new_wait_for_object = {
      timeStamp: Date.now(),
      condition: condition
  };

  if ( !global.wait_interval_array_for_ramps ) global.wait_interval_array_for_ramps = [];
  global.wait_interval_array_for_ramps.push(new_wait_for_object);
  
  var interval_id = setInterval(function() {
      check_condition(interval_id, condition, callbackFunction, period, expired_callback);
  }, 30);

  global.wait_interval_array_for_ramps[global.wait_interval_array_for_ramps.length - 1].id = interval_id;

  // if (period == 'inf') {
  return interval_id;
  //};
}; // end of wait for  function


function check_condition(interval_id, condition, callbackFunction, period, expired_callback) {
    var waiting_time = 50000;
    if (typeof period !== 'undefined' && period !== 'inf') {
        waiting_time = Number(period);
    };

    var resolved_condition = null;


    /*
    function Date(n){
      return ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"][n%7 || 0];
    }
    function runCodeWithDateFunction(obj){
        return Function('"use strict";return ( function(Date){ return Date(5) } )')()(Date);
    }
    console.log( runCodeWithDateFunction("function(Date){ return Date(5) }") )
    */


    if ($.type(condition) == "function") resolved_condition = condition();
    if ($.type(condition) == "string") resolved_condition = eval(condition);

    if (resolved_condition !== true) {

        /* -------------- if period is 'inf' then dont do this check at all --------------  */
        // effectively this means 
        if (period == 'inf') {

            /* 
            ------------- 
            DONT DO ANY CHECKS IS TIME EXPIRED ---> BECAUSE LISTENER IS INFINITE
            ----------------
            */
        } else {
            // console.log('waiting for condition:    ' + condition + '    to be true.....');
            // just a pointers to delete later
            var remove_this_indexes = [];
            // iterate array of interval object ids
            $.each(global.wait_interval_array_for_ramps, function(index, interval_object) {

              // pošto node js nema integer idjeva već sprema idjeve u posaeban type koji se rjetko koristi Symbol:
              // moram naći posaban property Symbol(asyncId)
              var interval_object_sym = Object.getOwnPropertySymbols(interval_object.id).find(function(s) {
                return String(s) === "Symbol(asyncId)";
              });
              var interval_object_sym_id = interval_object.id[interval_object_sym];
              
              var current_object_sym = Object.getOwnPropertySymbols(interval_id).find(function(s) {
                return String(s) === "Symbol(asyncId)";
              });
              var current_object_sym_id = interval_id[current_object_sym];
              
              
                // when I find right id
                if ( interval_object_sym_id === current_object_sym_id ) {
                    // check timestamp to see is expired
                    if (Date.now() - Number(interval_object.timeStamp) > waiting_time) {
                        // if difference is greater then defined period/waiting time
                        // remove this interval
                        clearInterval(interval_id);
                        // save this index for later delete
                        remove_this_indexes.push(index);

                        // console.log('condition  ' + condition + ' DID NOT fulfil IN ' + waiting_time / 1000 + 'sec .. aborting .....');

                        if (typeof expired_callback !== 'undefined') {
                            expired_callback();
                        };
                    };
                };
            });

            $.each(remove_this_indexes, function(ind, index_number) {
                global.wait_interval_array_for_ramps.splice(index_number, 1);
            });

            remove_this_indexes = [];

        }; // END OF if period is NOT 'inf' and it is NOT undefined

    } else {

        // IF CONDITION IS TRUE !!!!!!!!!1



        if (period == 'inf') {

            // if period is 'inf' just run callback function 
            // BUT DONT STOP THE LISTENER !!!!!!!!

            callbackFunction();
        } else {

            var remove_this_indexes = [];

            $.each(global.wait_interval_array_for_ramps, function(index, interval_object) {
              
              // pošto node js nema iteger idjeva već sprema idjeve u posaeban type koji se rjetko koristi Symbol:
              // moram naći posaban property Symbol(asyncId)
              var interval_object_sym = Object.getOwnPropertySymbols(interval_object.id).find(function(s) {
                return String(s) === "Symbol(asyncId)";
              });
              var interval_object_sym_id = interval_object.id[interval_object_sym];
              
              var current_object_sym = Object.getOwnPropertySymbols(interval_id).find(function(s) {
                return String(s) === "Symbol(asyncId)";
              });
              var current_object_sym_id = interval_id[current_object_sym];
              
              // when I find right id
              if ( interval_object_sym_id === current_object_sym_id ) {

                  clearInterval(interval_id);

                  // save this index to later remove
                  remove_this_indexes.push(index);

                  callbackFunction();
              };
            });

            $.each(remove_this_indexes, function(ind, index_number) {
                global.wait_interval_array_for_ramps.splice(index_number, 1);
            });

            remove_this_indexes = [];

        }; // end if period is NOT 'inf'

    }; // END IF IF CONDITION IS TRUE

}; 
// end of check_is_loaded function

var park_actions_controller = require('./park_actions_controller.js');


function convert_mongoose_object_to_js_object(mongoose_object) {
  
  var js_object = mongoose_object.toJSON();
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};


console.log('aktiviran rampa controller ------------ !!!!!!!!!!!!!!!!');

global.sve_rampe = [];

global.new_plates_saved_on_ramps = {};


function find_rampa( rampa_name ) {
  
  var found_rampa = null;
  $.each( global.sve_rampe, function(rampa_index, rampa_obj) {
    if ( rampa_name == rampa_obj.rampa_name ) {
      found_rampa = rampa_obj;
    };
  });
  return found_rampa;
};

function update_rampa( rampa_name, new_obj ) {
  
  var found_rampa_index = null;
  $.each( global.sve_rampe, function(rampa_index, rampa_obj) {
    if ( rampa_name == rampa_obj.rampa_name ) {
       found_rampa_index = rampa_index;
    };
  });
  
  // ako postoji rampa s tim imenom onda samo update sa novim idjem
  if ( found_rampa_index !== null ) {
    global.sve_rampe[found_rampa_index] = new_obj;
  } else {
    // ako ne postoji ramp s tim imenom onda pushaj 
    global.sve_rampe.push(new_obj);
  };
  
  return found_rampa_index;
  
};


global.msg_rampa_to_open = function(rampa_name, user_number) {
  
  var rampa = find_rampa( rampa_name );
  
  if ( rampa ) {
    // šaljem message prema specifičnoj rampi tj prema specifičnom socket id-ju
    // ako rampa pošalje nazad odgovor  - to će biti zabilježeno unutar listenera  on "connection" !!!
    global.we_io.to(`${rampa.socket_id}`).emit(rampa.rampa_name, `${user_number}`);
  } else {
    console.log('nije pronašao rampu koju treba otvoriti !!!!!!');
  };
  
};

global.msg_zero_to_close = function(rampa_name, user_number) {
  
  var rampa = find_rampa( rampa_name );
  
  if ( rampa ) {
    // šaljem message prema specifičnoj rampi tj prema specifičnom socket id-ju
    // ako rampa pošalje nazad odgovor  - to će biti zabilježeno unutar listenera  on "connection" !!!
    global.we_io.to(`${rampa.socket_id}`).emit(rampa.rampa_name+'_zero_close', `${user_number}`);
  } else {
    console.log('nije pronašao ZERO koju treba zatvoriti !!!!!!');
  };
  
};



global.msg_fixed_open_or_close = function(rampa_name, open_or_close_sufix, to_state) {
  
  var rampa = find_rampa( rampa_name );
  
  if ( rampa ) {
    // šaljem message prema specifičnoj rampi tj prema specifičnom socket id-ju
    // ako rampa pošalje nazad odgovor  - to će biti zabilježeno unutar listenera  on "connection" !!!
    global.we_io.to(`${rampa.socket_id}`).emit(rampa.rampa_name + open_or_close_sufix, `${to_state}`);
  } else {
    console.log('nije pronašao rampu koju treba otvoriti !!!!!!');
  };
  
};


global.msg_shell_command = function(rampa_name, shell_com) {
  
  var rampa = find_rampa( rampa_name );
  
  if ( rampa ) {
    // šaljem message prema specifičnoj rampi tj prema specifičnom socket id-ju
    // ako rampa pošalje nazad odgovor  - to će biti zabilježeno unutar listenera  on "connection" !!!
    global.we_io.to(`${rampa.socket_id}`).emit(rampa.rampa_name + '_shell_command', `${shell_com}`);
  } else {
    console.log('nije pronašao rampu s kojoj treba poslati shell komandu !!!!!!');
  };
  
};


global.requests_for_bar_position = {};

global.requests_to_move_rampa = {};

global.requests_to_fixed_open = {};
global.requests_to_fixed_close = {};

global.camera_preview_images = {};

global.we_motion_detect = {};

global.solar_data = {};



global.new_plates_saved_on_this_ramps = {};


exports.open_rampa = function(req, res) { 
  
  
  var rampa_name = req.params.ime_rampe;
  var user_number = parseInt(req.params.user_number);
  
  var rampa_i_user = rampa_name + '_' + user_number;
  
  global.requests_to_move_rampa[rampa_i_user] = 'pending';
  
  // ova funkcija pokreće socket message prema rampi !!!
  global.msg_rampa_to_open(rampa_name, user_number);
  
  
  function odgovor_rampe(res) {
    return function() { 
  
      var rampa_msg = global.requests_to_move_rampa[rampa_i_user];
      
      if ( rampa_msg == 'opened' ) {
        global.rampa_status_errors[rampa_i_user] = null;
        res.json({success: true, msg: 'opened' });
      };
      
      if ( rampa_msg == 'closed' ) {
        global.rampa_status_errors[rampa_i_user] = null;
        res.json({success: false, msg: 'closed' });
      };      
      
      if ( rampa_msg == 'in_process' ) {
        res.json({success: false, msg: 'popup_ramp_in_process' });
      };
      
      if ( rampa_msg == 'error' ) {
        global.rampa_status_errors[rampa_i_user] = Date.now();
        res.json({ success: false, msg: 'popup_greska_prilikom_parkinga' });
      };
      
      if ( rampa_msg == 'napon_too_low' ) {
        global.rampa_status_errors[rampa_i_user] = Date.now();
        res.json({ success: false, msg: 'popup_napon_too_low' });
      };      
      
      if ( rampa_msg == 'infra_red' ) {
        global.rampa_status_errors[rampa_i_user] = Date.now();
        res.json({ success: false, msg: 'popup_infra_red' });
      };  
      
      
      if ( rampa_msg == 'fixed_close_state' ) {
        global.rampa_status_errors[rampa_i_user] = Date.now();
        res.json({ success: false, msg: 'popup_fixed_close_state' });
      }; 
      
      if ( rampa_msg == 'fixed_open_state' ) {
        global.rampa_status_errors[rampa_i_user] = Date.now();
        res.json({ success: false, msg: 'popup_fixed_open_state' });
      };       
      
    }; // kraj return func
    
    
  }; // kraj odgovor rampe
  
  function timeout_rampe(res) {
    return function() { 
      res.json({success: false, msg: 'popup_greska_prilikom_parkinga'});
    };
  };
  
  // čekaj da raspberry odgovori sa socket msg !!!
  // kada socket dobije odgovor onda u global objekt promjeni iz pending u:
  // 1) opened
  // 2) in_process
  // 3) error
  wait_for(
    `global.requests_to_move_rampa['${rampa_i_user}'] !== 'pending'`,
    odgovor_rampe(res),
    1000*10, // timeout 10 sec
    timeout_rampe(res)
  );
  
};


exports.close_zero = function(req, res) { 
  
  
  var rampa_name = req.params.ime_rampe;
  var user_number = parseInt(req.params.user_number);
  
  var rampa_i_user = rampa_name + '_' + user_number;
  
  global.requests_to_move_rampa[rampa_i_user] = 'pending';
  
  // ova funkcija pokreće socket message prema rampi !!!
  global.msg_zero_to_close(rampa_name, user_number);
  
  
  function odgovor_rampe(res) {
    return function() { 
  
      var rampa_msg = global.requests_to_move_rampa[rampa_i_user];
      
      if ( rampa_msg == 'closed' ) {
        global.rampa_status_errors[rampa_i_user] = null;
        res.json({success: true, msg: 'closed' });
      };           
      
      
      if ( rampa_msg == 'opened' ) {
        global.rampa_status_errors[rampa_i_user] = null;
        res.json({success: false, msg: 'opened' });
      };
 
      
      if ( rampa_msg == 'in_process' ) {
        res.json({success: false, msg: 'popup_ramp_in_process' });
      };
      
      if ( rampa_msg == 'error' ) {
        global.rampa_status_errors[rampa_i_user] = Date.now();
        res.json({ success: false, msg: 'popup_greska_prilikom_parkinga' });
      };
      
      if ( rampa_msg == 'napon_too_low' ) {
        global.rampa_status_errors[rampa_i_user] = Date.now();
        res.json({ success: false, msg: 'popup_napon_too_low' });
      };      
      
      if ( rampa_msg == 'fixed_close_state' ) {
        global.rampa_status_errors[rampa_i_user] = Date.now();
        res.json({ success: false, msg: 'popup_fixed_close_state' });
      }; 
      
      if ( rampa_msg == 'fixed_open_state' ) {
        global.rampa_status_errors[rampa_i_user] = Date.now();
        res.json({ success: false, msg: 'popup_fixed_open_state' });
      };       
      
    }; // kraj return func
    
    
  }; // kraj odgovor rampe
  
  function timeout_rampe(res) {
    return function() { 
      res.json({success: false, msg: 'popup_greska_prilikom_parkinga'});
    };
  };
  
  // čekaj da raspberry odgovori sa socket msg !!!
  // kada socket dobije odgovor onda u global objekt promjeni iz pending u:
  // 1) opened
  // 2) in_process
  // 3) error
  wait_for(
    `global.requests_to_move_rampa['${rampa_i_user}'] !== 'pending'`,
    odgovor_rampe(res),
    1000*10, // timeout 10 sec
    timeout_rampe(res)
  );
  
};



exports.fixed_open_rampa = function(req, res) { 
  
  
  var rampa_name = req.params.ime_rampe;
  var new_state = req.params.new_state;
  
  global.requests_to_fixed_open[rampa_name] = 'pending';
  
  // ova funkcija pokreće socket message prema rampi !!!
  global.msg_fixed_open_or_close(rampa_name, '_fixed_open', `${new_state}`);

  
    
  function timeout_rampe(res) {
    return function() { 
      var rampa_msg = global.requests_to_fixed_open[rampa_name];
      res.json({success: false, state: rampa_msg, error: 'TIME OUT' });
    };
  };

  
  function odgovor_rampe(res) {
    return function() { 
  
      var rampa_msg = global.requests_to_fixed_open[rampa_name];
      
      if ( rampa_msg == 'ok' ) {
      
      ParkPlace.findOneAndUpdate(
      { ramps:  rampa_name },
      { $set: { fixed_open:  (new_state == 'true') ? true : false }},
      {new: true}, 
      function(err, updated_park) {

        if (err) {
          res.json({success: false, state: rampa_msg, error: err });
          return;
        };

        res.json({success: true, state: rampa_msg });

      }); // kraj update fixed open property id mongodb
 
        // kraj ako je msg ok
      } else {
        // ako msg nije bio "ok"
        res.json({success: false, state: rampa_msg });
        
        
      }; 
    };
  };

  // čekaj da raspberry odgovori sa socket msg !!!
  // kada socket dobije odgovor onda u global objekt promjeni iz pending u:
  // 1) ok
  // 2) not_ok
  // 3) ili neki error -----> oposite, too_low, 
  wait_for(
    `global.requests_to_fixed_open['${rampa_name}'] !== 'pending'`,
    odgovor_rampe(res),
    1000*10, // timeout 10 sec
    timeout_rampe(res)
  );


  

  
};



exports.fixed_close_rampa = function(req, res) { 
  
  var rampa_name = req.params.ime_rampe;
  var new_state = req.params.new_state;
  
  global.requests_to_fixed_close[rampa_name] = 'pending';
  
  // ova funkcija pokreće socket message prema rampi !!!
  global.msg_fixed_open_or_close(rampa_name, '_fixed_close', `${new_state}`);

  
  function timeout_rampe(res) {
    return function() { 
      var rampa_msg = global.requests_to_fixed_open[rampa_name];
      res.json({success: false, state: rampa_msg, error: 'TIME OUT' });
    };
  };
  
  function odgovor_rampe(res) {
    return function() { 
  
      var rampa_msg = global.requests_to_fixed_close[rampa_name];
      
      if ( rampa_msg == 'ok' ) {

        ParkPlace.findOneAndUpdate(
        { ramps:  rampa_name },
        { $set: { fixed_close: (new_state == 'true') ? true : false }},
        {new: true}, 
        function(err, updated_park) {

          if (err) {
             res.json({success: false, state: rampa_msg, error: err });
            return;
          };

          res.json({success: true, state: rampa_msg });

        }); // kraj update fixed CLOSE property

        
      } else {
        
        // ako msg nije ok
        res.json({success: false, state: rampa_msg });
      }
        
    }; // kraj return funkcije
    
  };

  // čekaj da raspberry odgovori sa socket msg !!!
  // kada socket dobije odgovor onda u global objekt promjeni iz pending u:
  // 1) open
  // 2) close
  // 3) error
  wait_for(
    `global.requests_to_fixed_close['${rampa_name}'] !== 'pending'`,
    odgovor_rampe(res),
    1000*10, // timeout 10 sec
    timeout_rampe(res)
  );

 
    
};


function channels_to_object(channels) {
  
  var channels_string = cit_white_space_trim( channels );

  channels_string = channels_string.split('|');

  var ch_0 = Number( channels_string[0] ) * 0.000885; // /32768 * 30;
  var ch_1 = Number( channels_string[1] ) * 0.000885; // /32768 * 30;
  var ch_2 = Number( channels_string[2] ) * 0.000885; // /32768 * 30;
  var ch_3 = Number( channels_string[3] ) * 0.000885; // /32768 * 30;

  ch_0 = zaokruzi(ch_0, 2);
  ch_1 = zaokruzi(ch_1, 2);
  ch_2 = zaokruzi(ch_2, 2);
  ch_3 = zaokruzi(ch_3, 2);


  var channels_obj = {
    ch_0: ch_0,
    ch_1: ch_1,
    ch_2: ch_2,
    ch_3: ch_3
  };  
  
  return channels_obj;
  
  
  
};


function write_solar_data_to_db(rampa_name, solar_data, arg_frequency ) {
  
  if ( !solar_data || ( $.isArray(solar_data) && solar_data.length == 0) ) return;
  
  if ( !arg_frequency ) arg_frequency = '30sec';
  
  // resetiraj solar array
  var solar_array = [];
  
  var saved_solar_index = 0;
  
  // ako dobijem samo string kao npr : `56456|54|0|1`
  if ( typeof solar_data == 'string' ) {
    
    var channels_obj = channels_to_object( solar_data );
    
    var db_solar_object = {
      frequency: arg_frequency,
      channels: channels_obj,
      time: Date.now(), // sam kreiram vrijeme jer sam dobio samo string
      rampa_name: rampa_name,
    };
    solar_array.push(db_solar_object);
    
  } else {
    
    // ako sam dobio array onda samo stringove prtvori u objekt i dodaj ime rampe
    // podatke kojen dobivam u obliku [ { time: 65443543445, channels: '321354|22|454545|4555545' } ]
    
    $.each( solar_data, function( sol_index, sol_obj ) {
      
      
      if ( arg_frequency == '30sec' ) {
        
        var channels_obj = channels_to_object( sol_obj.channels );

        var db_solar_object = {
          frequency: arg_frequency,
          channels: channels_obj,
          time: sol_obj.time,
          rampa_name: rampa_name,
        };
        solar_array.push(db_solar_object);
        
      } else  {
        // ovo je kada direktno sprema 1hour objekte koje sam generirau u funkciji convert 30sec to 1hour
        solar_array.push(sol_obj);
        
      };
      

    });
    
  }; // kraj ako sam dobio array
  
  // PAZI OVO JE LOOP !!!!!
  // PAZI OVO JE LOOP !!!!! ----> ide jedan po jedan item u arrayu sve dok ne spremi sve u bazu
  // PAZI OVO JE LOOP !!!!! ----> obično bude 20 komada itema
  // PAZI OVO JE LOOP !!!!!
  function save_solar_obj( solar_index ) {
    
    var new_solar_data = new Solar(solar_array[ solar_index ]);  

    new_solar_data.save(function(err, solar_data) {

      if (err) {
        
        console.error(err);
        console.error('GREŠKA KOD SPREMANJA ' + (solar_index+1) + '/' + solar_array.length + ' od solar array za rampu: ' + rampa_name );

        // bez obzira što se dogodila greška probaj nastaviti spremati dalje !!!!
        if ( solar_index < solar_array.length - 1 ) {
          save_solar_obj( solar_index+1 );
        };
        
        return;
      };
      
      console.log('spremio sam ' + (solar_index+1) + '/' + solar_array.length + ' od solar array za rampu: ' + rampa_name );

      if ( solar_index < solar_array.length - 1 ) {
        
        save_solar_obj( solar_index+1 );
        
      } else {
        console.log('spremio sam ----- CIJELI SOLAR ARRAY ----- za rampu: ' + rampa_name );
      };

    }); // end of spremi solar data
  
  }; // kraj save solar objekt
  
  if ( solar_array.length > 0 ) save_solar_obj(0);
  
}; // kraj write solar data to db


function delete_30sec_solar_data(solar_ids_array) {
  
  
  Solar.deleteMany({ _id: { $in: solar_ids_array } }, function(err, result) {
    if (err) {
      console.err(err);
    } else {
      console.log(result)
    }
  });
  
};


global.check_ssh_pwd = function( ime_rampe, ssh_port, vnc_port ) {

  var ssh_tun = spawn('sudo', [ '-u', 'toni', 'ssh', '-o', `StrictHostKeyChecking=no`, `-o`, `UserKnownHostsFile=/dev/null`, '-p', `${ssh_port}`, 'pi@localhost', 'pwd' ]);

  ssh_tun.stdout.on("data", data => {
    console.log("%c-------------- CHECKING SSH PWD --------------", "background-color: #d1d100; color: black;");  
    console.log( data.toString('utf8') );

    if ( data.toString('utf8').trim().indexOf('/home/pi') == -1 ) {
      global.msg_shell_command(ime_rampe, `/usr/bin/ssh -t -t -R ${ssh_port}:127.0.0.1:22 toni@spori.circulum.it`);
    } else {
      console.log("%c-------------- SSH PWD JE OK --------------", "background-color: green; color: white;");  
    };

  }); // kraj ssh_tun std out

  ssh_tun.stderr.on("data", data => {
    
    // inače kad je sve ok ssh ipak vrati ovu poruku koju tretira kao standard error output
    // Warning: Permanently added '[localhost]:45001' (ECDSA) to the list of known hosts.
    // !!!!!! ali to nije error i sve je ok !!!!!!
    // AKO JE BILO KOJA DRUGA PORUKA ZA ERROR ONDA POŠALJI SSH COMMAND
    if ( data.toString('utf8').trim().indexOf('to the list of known hosts') == -1 ) {
      global.msg_shell_command(ime_rampe, `/usr/bin/ssh -t -t -R ${ssh_port}:127.0.0.1:22 toni@spori.circulum.it`);
    };
    console.log(`SSH PWD stderr: ${ data.toString('utf8').trim() }`);
  });

  ssh_tun.on('error', (error) => {
    global.msg_shell_command(ime_rampe, `/usr/bin/ssh -t -t -R ${ssh_port}:127.0.0.1:22 toni@spori.circulum.it`);
    console.log(`SSH PWD error: ${error.message}`);
  });

  ssh_tun.on("close", code => {
    console.log(`SSH PWD  gotov s exit code ${code}`);
  });

};




function register_socket_events(ramp_ime, socket) {
  
  
// START  loopa za registriranje evenata unutar socket connection
  
  // dobijem razne status kad želim otvoriti ili zatvoriti rampu 
  socket.on(`${ramp_ime}_status`, function( user_number ) {

    console.log( ' socket.id ==  '  + socket.id);

    console.log('potvrda_otvaranja rampe tzm  za usera : ' + user_number );

    var rampa_i_user = `${ramp_ime}_` + parseInt(user_number);

    if ( user_number.indexOf('opened') > -1 ) {
      var samo_user_num = user_number.replace('opened_', '');
      rampa_i_user = `${ramp_ime}_` + parseInt(samo_user_num);
      global.requests_to_move_rampa[rampa_i_user] = 'opened';
    };    

    
    if ( user_number.indexOf('closed') > -1 ) {
      var samo_user_num = user_number.replace('closed_', '');
      rampa_i_user = `${ramp_ime}_` + parseInt(samo_user_num);
      global.requests_to_move_rampa[rampa_i_user] = 'closed';
    };       
    

    if ( user_number.indexOf('in_process') > -1 ) {
      var samo_user_num = user_number.replace('in_process_', '');
      rampa_i_user = `${ramp_ime}_` + parseInt(samo_user_num);
      global.requests_to_move_rampa[rampa_i_user] = 'in_process';
    };

    if ( user_number.indexOf('error') > -1 ) {
      var samo_user_num = user_number.replace('error_', '');
      rampa_i_user = `${ramp_ime}_` + parseInt(samo_user_num);
      global.requests_to_move_rampa[rampa_i_user] = 'error';
    };


    if ( user_number.indexOf('napon_too_low') > -1 ) {
      var samo_user_num = user_number.replace('napon_too_low_', '');
      rampa_i_user = `${ramp_ime}_` + parseInt(samo_user_num);
      global.requests_to_move_rampa[rampa_i_user] = 'napon_too_low';
    };    
    
    if ( user_number.indexOf('infra_red') > -1 ) {
      var samo_user_num = user_number.replace('infra_red_', '');
      rampa_i_user = `${ramp_ime}_` + parseInt(samo_user_num);
      global.requests_to_move_rampa[rampa_i_user] = 'infra_red';
    };     

    if ( user_number.indexOf('fixed_close_state') > -1 ) {
      var samo_user_num = user_number.replace('fixed_close_state_', '');
      rampa_i_user = `${ramp_ime}_` + parseInt(samo_user_num);
      global.requests_to_move_rampa[rampa_i_user] = 'fixed_close_state';
    };

    if ( user_number.indexOf('fixed_open_state') > -1 ) {
      var samo_user_num = user_number.replace('fixed_open_state_', '');
      rampa_i_user = `${ramp_ime}_` + parseInt(samo_user_num);
      global.requests_to_move_rampa[rampa_i_user] = 'fixed_open_state';
    };

    console.log( `-------> OVO JE global.requests_to_move_rampa['${rampa_i_user}'] nakon odgovora:`);
    console.log( global.requests_to_move_rampa[rampa_i_user] );

  });

  // ovo je odgovor od shell commands 
  socket.on(`${ramp_ime}_shell_command`, function( rampa_msg ) {
    console.log(rampa_msg);
  });


  socket.on("check_ssh", function( ime_rampe ) {

    var ssh_port = null;
    var vnc_port = null;
    $.each(global.lista_rampi, function(ramp_ind, ramp) {
      if (ramp.ime == ime_rampe ) {
        ssh_port = ramp.ssh_port;
        vnc_port = ramp.vnc_port;
      };
    });

    global.check_ssh_pwd( ime_rampe, ssh_port, vnc_port );

  }); // kraj soketa check ssh


  socket.on(`${ramp_ime}_fixed_open_state`, function( state ) {
    global.requests_to_fixed_open[ramp_ime] = state;
  });

  
  socket.on(`${ramp_ime}_fixed_close_state`, function( state ) {
    global.requests_to_fixed_close[ramp_ime] = state;
  })

  
  socket.on(`${ramp_ime}_camera_preview`, function( data ) {
    console.log('dobio sam image  od  ${ramp_ime} !!!!!' );
    global.camera_preview_images[ramp_ime] = data.buffer;
  });  

  
  socket.on(`${ramp_ime}_start_picam`, function( state ) {
    console.log('start picam state: ' + state  );
    global.we_motion_detect[ramp_ime] = state;
  });    

  
  socket.on(`${ramp_ime}_stop_picam`, function( state ) {
    console.log('stop picam state: ' + state  );
    global.we_motion_detect[ramp_ime] = state;
  });      

  
  socket.on(`${ramp_ime}_solar_data`, function( channels ) {
    console.log('ovo je solar data string: ' + channels  );
    global.solar_data[ramp_ime] = channels;
  });     

  
  socket.on(`${ramp_ime}_solar_data_for_db`, function( solar_data_array ) {
    console.log('ovo je solar data array FOR DB:' );
    console.log( solar_data_array );

    write_solar_data_to_db(ramp_ime, solar_data_array);

  });

  
  socket.on(`${ramp_ime}_regs_updated`, function( updated_reg_obj ) {

    console.log(`ovo je regs_updated data s ${ramp_ime}, od korisnika  - ` + updated_reg_obj.user_number );
    console.log( updated_reg_obj.plates );

    // kada dobije msg od rampe da je spremila plates od nekog usera onda 
    // 1 ) provjerim jel već postoji taj properti s brojem tog usera
    // 2) ako postoji onda pogledam u arrayu jel postoji objekt za imenom ove rampe
    // 3) ako ima objekt u arraju s imenom ove rampe onda napravim update tj premišem taj objekt s ovim kojeg sam dobio iz socketa
    if (
      global.new_plates_saved_on_ramps[updated_reg_obj.user_number] &&
      $.isArray( global.new_plates_saved_on_ramps[updated_reg_obj.user_number] ) 
    ) {

      var found_ramp = false;

      $.each( global.new_plates_saved_on_ramps[updated_reg_obj.user_number], function( ramp_index, ramp_item ) {
        if ( ramp_item.ramp == `${ramp_ime}` ) {
          found_ramp = true;
          global.new_plates_saved_on_ramps[updated_reg_obj.user_number][ramp_index] = updated_reg_obj;
        };
      });
      // 4) ako nema objekta s ovim imenom rampe onda ga push-am
      if ( found_ramp == false )  global.new_plates_saved_on_ramps[updated_reg_obj.user_number].push(updated_reg_obj);

    } else {
      // 5) ako uopće nema propertija s ovim brojem usera onda ga napravim da bude array
      global.new_plates_saved_on_ramps[updated_reg_obj.user_number] = [];
      // 6) i ubacim u taj array objekt koji sam dobio od socketa 
      global.new_plates_saved_on_ramps[updated_reg_obj.user_number].push(updated_reg_obj);

    };


  }); // kraj regs update

  
  socket.on(`rampa_mail`, function( mail_data ) {

    console.log('ovo je rampa mail data objekt: ');
    console.log(mail_data);

    var email_subject = mail_data.email_subject;
    var email_body = mail_data.email_body;
    var user_number = Number(mail_data.user_number) || null;
    var user = null;

    if ( user_number ) {

      find_user_by_number(user_number)
      .then(
      function(result) {

        user = result.user;

        if ( user ) {

          return global.send_rampa_status_by_mail( email_subject, email_body, user );

        } else {

          // napravio sam promis samo da bih vratio reject !!!!! ( jer nema usera )
          return new Promise( function( resolve, reject ) {
            resolve({ success: false, msg: 'greška prilikom pronalaska usera za rampa email' });
          });

        };

      })
      .catch(
      function(fail) {
        console.error('---------------- RAMPA MAIL FAIL !!!!! -----------------');
        console.error('PRILIKOM SLANJA RAMPA MAIL-a nije našao usera u bazi');
        console.log(fail);
      });



    } 
    else {
      console.log('nema user number pa ne šaljem korisniku  već samo na naš wepark tech mail !!!!!!!! ')
      global.send_rampa_status_by_mail( email_subject, email_body, null );


  };

  });  
  
  
  // ovo je odgovor od zero jel gore ili dolje 
  socket.on(`${ramp_ime}_position`, function( position ) {

    global.requests_for_bar_position[ramp_ime] = position;

  });

  
  
}; // kraj register socket events


// ----------- START --------------- SOCKET CONNECTION --------
// ----------- START --------------- SOCKET CONNECTION --------
// ----------- START --------------- SOCKET CONNECTION --------


global.we_io.sockets.on("connection", function(socket) {
  
  
  
// Display a connected message
console.log("Server-Client Connected!");


socket.on("prijava", function( rampa_name ) {

  console.log('PRIJAVA sa ' + rampa_name + ' !!!!!!!');


  // provjeri jel možda imam od prije ERROR (nije se prijavila 5min) za ovu rampu
  // resetiraj zabilježeni ERROR ako postoji
  if ( global.rampa_prijave_errors[rampa_name] ) {
    
    global.rampa_prijave_errors[rampa_name] = null;
    
    var fino_ime_rampe = rampa_name.replace('_', ' ').toUpperCase();
    var email_subject = `${fino_ime_rampe} JE ONLINE !!!`;
    
    global.send_rampa_status_by_mail(email_subject, `Rampa je ponovo online.<br>Ako ste prije dobili mail da se rampa nije prijavila, zanemarite ga :). Vjerojatno je došlo do privremenog prekida internet veze...`);

  };
  
  
  var novi_socket_obj = {
    rampa_name: rampa_name,
    socket_id: socket.id,
    time: Date.now()
  };

  console.log( socket.id );
  
  var rampa = find_rampa( rampa_name );
  
  // napravi novi objekt sa novim socketom ili zamjeni stari socket id sa ovim novim
  update_rampa( rampa_name, novi_socket_obj );
    
  // samo ako nema ove rampe ( NOVA JE ) tj nije ju server do sada spremio i ako se socket PROMIJENIO !!!
  // ponovo register sve evente 
  if ( !rampa || (rampa.socket_id !== socket.id) ) register_socket_events( rampa_name, socket );
  
});
  
  
  
  
  
}); // kraj cocket on connection

// ----------- END --------------- SOCKET CONNECTION --------
// ----------- END --------------- SOCKET CONNECTION --------
// ----------- END --------------- SOCKET CONNECTION --------


exports.check_fixed_states = function(req, res) {
  
  var rampa_name = req.params.ime_rampe;
  
  ParkPlace.findOne({ ramps: rampa_name }, function(err, specific_pp) {
    if (err) {
      res.send({ success: false, msg: "GREŠKA U DOBIVANJU SPECIFIC PARKIRALIŠTA za GET FIXED STATE", error: err});
      return;
    };
    
    if ( specific_pp ) {
            
      var clean_specific_pp = convert_mongoose_object_to_js_object(specific_pp);
      

      res.json({ 
        success: true, 
        poruka: "fixed states", 
        fixed_open: clean_specific_pp.fixed_open,
        fixed_close: clean_specific_pp.fixed_close 
      });
      
    } 
    else {
      
      res.json({ success: false, poruka: "NIJE NAŠAO PARK!!!!", fixed_open: null, fixed_close: null });
      
    }; // kraj ifa ako je našao specific park
  
  }); // kraj park place find query
  
}; // kraj cijele func check fixed states


exports.check_fixed_barriers = function(req, res) {
  
  var bar_name = req.params.bar_name;
  
  ParkPlace.findOne({ "barriers.name": bar_name }, function(err, specific_pp) {
    if (err) {
      res.send({ success: false, msg: "GREŠKA U DOBIVANJU SPECIFIC PARKIRALIŠTA za GET FIXED STATE", error: err});
      return;
    };
    
    if ( specific_pp ) {
            
      var clean_specific_pp = convert_mongoose_object_to_js_object(specific_pp);
      
      
      var found_bar = null;
      
      $.each(clean_specific_pp.barriers, function(bar_ind, bar_obj) {
        if ( bar_obj.name == bar_name ) found_bar = bar_obj;
        
      });

      res.json({ 
        success: true, 
        poruka: "fixed states", 
        fixed_open: found_bar.fixed_open,
        fixed_close: found_bar.fixed_close 
      });
      
    } 
    else {
      
      res.json({ success: false, poruka: "NIJE NAŠAO PARK!!!!", fixed_open: null, fixed_close: null });
      
    }; // kraj ifa ako je našao specific park
  
  }); // kraj park place find query
  
}; // kraj cijele func check fixed states




function get_bar_pos(all_bars) { 
  
  // resetiraj globalni objekt
  global.requests_for_bar_position = {};
  
  return new Promise(function(resolve, reject) {
  
  $.each(all_bars, function(bar_ind, bar_name) {
    
    global.requests_for_bar_position[bar_name] = 'pending';  
    
    var bar = find_rampa( bar_name );
  
    if ( bar ) {
      // šaljem message prema specifičnoj rampi tj prema specifičnom socket id-ju
      // ako rampa pošalje nazad odgovor  - to će biti zabilježeno unutar listenera  on "connection" !!!
      global.we_io.to(`${bar.socket_id}`).emit(bar.rampa_name+'_get_position', `get_position`);
    } else {
      console.log(`nije pronašao socket za barijeru s imenom ${bar_name} za koju treba provjeriti poziciju !!!!!!`);
    };
  
  }); // kraj loopa po svim barijerama za koje treba 
  
    var timeout = all_bars.length * 2;
    if ( timeout > 6 ) timeout = 6;
 
    // ČEKAJ timeout SEKUNDI 
    setTimeout( function() {
      resolve(global.requests_for_bar_position);
    }, timeout*1000 );

  }); // kraj promisa  
 
  
};



exports.check_bar_position = function(req, res) {
  
  var bar_name = req.params.bar_name;
  
  var bar_query = null;
  
  if ( bar_name !== 'all' ) {
    bar_query = { "barriers.name": bar_name };
  }
  else {
    bar_query = { "barriers": { $exists: true} };
  };
  
  ParkPlace.find( bar_query, function(err, specific_pps) {
    
    if (err) {
      res.send({ success: false, msg: "GREŠKA U DOBIVANJU PARKIRALIŠTA za GET BAR POSITION", error: err});
      return;
    };
    
    var clean_pps = [];
    if ( specific_pps && specific_pps.length > 0 ) {
            
      $.each(specific_pps, function( pp_index, pp) {
        var clean_pp = convert_mongoose_object_to_js_object(pp);
        clean_pps.push(clean_pp);
      });
      
      if ( bar_name !== 'all' ) {
        
        var found_bar = null;
        
        $.each(clean_pps[0].barriers, function(bar_ind, bar_obj) {
          if ( bar_obj.name == bar_name ) found_bar = bar_obj;
        });
        
        /*
        found bar izgleda ovako:
        
        {
          "name" : "zero_1",
          "state" : null,
          "user_number" : null,
          "reserved" : null,
          "from" : null,
          "to" : null,
          "fixed_open" : false,
          "fixed_close" : false
        }
        
        
        
        */
        

        get_bar_pos([found_bar.name])
        .then(
        function(all_positions) {
          
          var state = all_positions[found_bar.name]; // inicijalno bude pending;
          if ( all_positions[found_bar.name] == 'OPEN|NO_CLOSED') state = 'open';
          if ( all_positions[found_bar.name] == 'NO_OPEN|CLOSED') state = 'closed';
          if ( all_positions[found_bar.name] == 'NO_OPEN|NO_CLOSED' || all_positions[found_bar.name] == 'OPEN|CLOSED' ) state = 'error';
          if ( all_positions[found_bar.name] == 'null|null') state = 'error';

          res.json({ 
            success: true, 
            name: found_bar.name,
            poruka: "barrier state", 
            park: clean_pps[0].pp_name,
            fixed_open: found_bar.fixed_open,
            fixed_close: found_bar.fixed_close,
            state: state,
            status : found_bar.state,
            user_number : found_bar.user_number,
            reserved : found_bar.reserved,
            from : found_bar.from,
            to : found_bar.to
          });
          
        }); // kraj thena
          
      }
      else {
        
        
        var sve_barijere = []
        // ako je za sve barijere
        var just_bar_names = [];
        $.each(clean_pps, function(pp_ind, pp_obj) {
          $.each(pp_obj.barriers, function(bar_ind, bar_obj) {
            just_bar_names.push(bar_obj.name);
          });
        });
        

        get_bar_pos(just_bar_names)
        .then(
        function(all_positions) {

          $.each(clean_pps, function(pp_ind, pp_obj) {

            $.each(pp_obj.barriers, function(bar_ind, bar_obj) {

              var state = all_positions[bar_obj.name]; // inicijalno bude paneding;
              if ( all_positions[bar_obj.name] == 'OPEN|NO_CLOSED') state = 'open';
              if ( all_positions[bar_obj.name] == 'NO_OPEN|CLOSED') state = 'closed';
              if ( all_positions[bar_obj.name] == 'NO_OPEN|NO_CLOSED' || all_positions[bar_obj.name] == 'OPEN|CLOSED' ) state = 'error';
              if ( all_positions[bar_obj.name] == 'null|null') state = 'error';
              

              sve_barijere.push({ 
                success: true, 
                name: bar_obj.name,
                poruka: "barrier state", 
                park: pp_obj.pp_name,
                fixed_open: bar_obj.fixed_open,
                fixed_close: bar_obj.fixed_close,
                state: state,
                status : bar_obj.state,
                user_number : bar_obj.user_number,
                reserved : bar_obj.reserved,
                from : bar_obj.from,
                to : bar_obj.to
              });
            
            }); // loop po barijerama
          });  // loop po parkovima

          
          res.json(sve_barijere);
          
          
        }); // kraj thena

      }; // kraj else tj za sve barijere
      
    } 
    else {

      // ako nije našao park po imenu barijere !!!!!
      if ( bar_name !== 'all' ) {

        res.json({ 
          success: false, 
          poruka: "NIJE NAŠAO PARK S OVIM IMENOM BARIJERE!!!!", 
          fixed_open: null,
          fixed_close: null,
          state: null
        });

      };
        
    }; // kraj ifa ako je našao specific park
  
  }); // kraj park place find query
  
}; // kraj cijele func check fixed states


exports.ping = function(req, res) { 

res.send('pong');

};


exports.all_plates = function(req, res) { 

User.find({ car_plates: { $ne: null } })
.select({
  "user_number": 1,
  "car_plates": 1,
  _id: -1
}) 
.exec(function(err, all_plates) {
  if (err) {
    res.send({success: false, msg: 'greška u traženju all car plates', error: err });
    return;
  };

  var all_clean_plates = [];

  if ( all_plates.length > 0 ) {

    $.each( all_plates, function( plate_index, user_plates ) {
      var clean_plates = convert_mongoose_object_to_js_object(user_plates);
      all_clean_plates.push(clean_plates);
    });

  };

  res.json({ success: true, all_plates: all_clean_plates });
});

};


// šaljem nove registracije SVIM raspberryjima koji su spojeni u tom trenutku 
exports.save_new_user_plates = function(req, res) { 

var new_user_plates = req.body;
var user_number = Number(req.body.user_number);
var car_plates = req.body.car_plates;


User.findOneAndUpdate(
{ user_number: user_number },
{ $set: { car_plates: car_plates }},
{new: true}, 
function(err, updated_user) {

  if (err) {
    res.send({success: false, error: err });
    return;
  };

  new_plates_saving(new_user_plates)
  .then(
  function(result){
    res.json(result);
  })
  .catch(
  function(fail) {
    console.error('---------------- SAVING PLATES FAIL !!!!! -----------------');
    console.log(fail);
  });

});    

};


// šaljem nove registracije SVIM raspberryjima koji su spojeni u tom trenutku 
function new_plates_saving(new_user_plates) { 


  return new Promise( function( resolve, reject ) {

    if ( global.we_io ) {

      // pretvori u array tako da ne radi probleme na respberiju kada radim  loop za update 
      // pošto na raspu prvo OBRIŠEM sve rege koje su s tim user numberom
      // i onda vrtim loop po car plates properiju koji je zapravo array
      // i zato ne želim da to bude null :-)
      if ( !new_user_plates.car_plates ) new_user_plates.car_plates = []; 

      global.we_io.sockets.emit('broadcast', new_user_plates );

      global.new_plates_saved_on_ramps[new_user_plates.user_number] = null;

      function update_regs_condition(user_number) {

        return function() {
          var user_updated = global.new_plates_saved_on_ramps[user_number];
          // ako još nema properija s brojem usera
          if ( !user_updated ) return false;
          // ako nema objekata u arrayu koliko ima rampi
          if ( user_updated.length !== global.sve_rampe.length ) return false;  
          // ako ima objekata  točno kliko ima rampi
          if ( user_updated.length == global.sve_rampe.length ) return true;  
        };

      };

      wait_for( update_regs_condition(new_user_plates.user_number), function() {

        resolve({ success: true, saved_on_ramps: global.new_plates_saved_on_ramps[new_user_plates.user_number] });

      }, 
      10*1000,
      function() {
        // čak i ako nije dobio sve odgovore nakon 10 sec ipak pošalji response ali sa false
        resolve({ success: false, proruka: '', saved_on_ramps: global.new_plates_saved_on_ramps[new_user_plates.user_number] });
      });

    } 
    else {
      // ako uopće nema we io global objekta tj nema uopće socketa !!!
      resolve({ success: false, error: 'GREŠKA PRILIKOM SPREMANJA REGISTRACIJA  - NEMA SOCKET VEZE NA SERVERU !!!!' });
    };

  }); // kraj promisa

}; // kraj save new user plates
exports.new_plates_saving = new_plates_saving;


function send_camera_preview_response(req, res, rampa_name, start_or_stop, command ) {



var preview_objekt = {
  start_or_stop : start_or_stop,
  command: command
};

var rampa = find_rampa( rampa_name );

if ( rampa ) {
  // šaljem message prema specifičnoj rampi tj prema specifičnom socket id-ju
  // ako rampa pošalje nazad odgovor to šće biti zabilježeno unuter listenera  on "connection" !!!


  global.we_io.to(`${rampa.socket_id}`).emit(`${rampa.rampa_name}_camera_preview`, preview_objekt)

  global.camera_preview_images[rampa.rampa_name] = null;

  function send_image(res) {

    return function() { 
      res.statusCode = 200;
      res.setHeader("Content-Type", "image/jpeg");
      res.end(global.camera_preview_images[rampa.rampa_name]);
    };

  };


  wait_for(
    `global.camera_preview_images['${rampa.rampa_name}'] !== null`,
    send_image(res),
    1000*10, // timeout 10 sec
    function() { res.sendStatus(404); }
  );



} else {

  console.log('nije pronašao rampu za koju treba camera preview !!!!!!');
  res.sendStatus(404);

};


};



exports.camera_preview = function(req, res) {


var rampa_name = req.params.ime_rampe;
var start_or_stop = req.params.start_or_stop;
var command = req.body.command;

if ( start_or_stop == 'stop' ) {

  send_camera_preview_response(req, res, rampa_name, start_or_stop, command);


} 
  
else {

  // ako postoji distor command
  if ( typeof command !== 'undefined' && command !== null && command !== '' && command !== 'null' ) {

    // prvo upiši distort podatke u bazu pa onda pošalji signal prema raspberriju !!!!
    Camera.findOneAndUpdate( 
      { name: rampa_name },
      { name: rampa_name, distort: command },
      { upsert: true, safe: true, new: true },
      function(err, saved_camera ) {

      if (err) {
        console.log('ERROR !!!!!!! WHEN SAVING CAMERA DISTORT COMMAND IN DATABASE');
        res.json({success: false, err: err, msg: 'ERROR !!!!!!! WHEN SAVING CAMERA DISTORT COMMAND IN DATABASE' });
        console.log(err);

      } else {

        send_camera_preview_response(req, res, rampa_name, start_or_stop, command);

      };
    }); // end of upsert camera distort

  } else {

  // ako nema distort command tj ako je prazna

    send_camera_preview_response(req, res, rampa_name, start_or_stop, command);

  };

};


};


exports.get_last_distort_command = function(req, res) {


var rampa_name = req.params.ime_rampe;

  // prvo upiši distort podatke u bazu pa onda pošalji signal prema raspberriju !!!!
Camera.findOne( 
  { name: rampa_name },
  function(err, saved_camera ) {

  if (err) {
    console.log('ERROR !!!!!!! WHEN GET LAST DISTORT COMMAND');
    res.json({success: false, err: err, msg: 'ERROR !!!!!!! WHEN GET LAST DISTORT COMMAND' });
    console.log(err);

  } else {
    var clean_saved_camera = { distort: '' };

    if (saved_camera) clean_saved_camera = convert_mongoose_object_to_js_object(saved_camera);

    res.json({success: true, distort: clean_saved_camera.distort });

  };
}); // end of query for camera distort


};


global.lokalno_testiranje = false;
if ( server_settings.port == 8080 ) global.lokalno_testiranje = true;


function query_current_solar_data(rampa_name) {


return new Promise( function( resolve, reject ) {

  var rampa = find_rampa( rampa_name );

  if ( rampa || global.lokalno_testiranje ) {
    // šaljem message prema specifičnoj rampi tj prema specifičnom socket id-ju
    // ako rampa pošalje nazad odgovor to će biti zabilježeno unuter listenera  on "connection" !!!

    // ako testiram lokalno onda nemoj slati ovaj msg preko socketa
    if ( !global.lokalno_testiranje ) {
      
      global.we_io.to(`${rampa.socket_id}`).emit(`${rampa.rampa_name}_solar_data`, 'start');
      global.solar_data[rampa.rampa_name] = null;
      
    } else {
      
      // dummy podaci za testiranje lokalno
      resolve({
        ch_0: 28.5,
        ch_1: 0,
        ch_2: 0,
        ch_3: 0,
        success: true
      });
      
    };

  

    function resolve_solar_data() {
        // u global varijablu solar data sam spremio podatke koje sam dobio od socketa !!!!
        var channels_obj = channels_to_object(global.solar_data[rampa.rampa_name]);
        channels_obj.success = true;
        resolve(channels_obj);
    };

    wait_for(
      `global.solar_data['${rampa.rampa_name}'] !== null`,
      resolve_solar_data,
      1000*10, // timeout 10 sec
      function() { 
        console.error('TIME OUT - prošlo je više od 10 sec  i nisam dobio odgovor od rampe: ' + rampa_name + 'za current solar data request !!!!!!');

        resolve({
          success: false,
          msg: 'TIME OUT - prošlo je više od 10 sec  i nisam dobio odgovor od rampe: ' + rampa_name + 'za current solar data request !!!!!!'
        });

      }); // kraj wait for

    // kraj našao rampu
  } 
  else {

    console.error('nije pronašao aktivnu rampu pod imenom' + rampa_name + 'za solar data request !!!!!!');
    reject({success: false, msg: 'nije pronašao aktivnu rampu pod imenom' + rampa_name + 'za solar data request !!!!!!'});

  };

}); // kraj promisa

}; // kraj get current solar data


exports.get_solar_data = function(req, res) {

var ime_rampe = req.body.ime_rampe;
var from_time = Number(req.body.from_time); 
var to_time = Number(req.body.to_time); 
var time_grupa = req.body.time_grupa;

var rampa = find_rampa( ime_rampe );

if ( rampa || global.lokalno_testiranje ) {

  query_current_solar_data(ime_rampe)
  .then(
  function(current_solar_data) {  

    // ako nije uspio očitati tj dobiti trenutne podatke od RPI putem socketa !!!!
    // ako nije uspio očitati tj dobiti trenutne podatke od RPI putem socketa !!!!
    // ako nije uspio očitati tj dobiti trenutne podatke od RPI putem socketa !!!!
    if ( current_solar_data.success === false ) current_solar_data = null;
    
    Solar.find({ rampa_name: ime_rampe, time: { $gte: from_time, $lte: to_time } })
    .exec(function(err, solar_arr) {
      if (err) {
        res.send({success: false, msg: 'greška u traženju solar podataka za rampu', error: err });
        return;
      };

      var clean_solar_arr = [];

      if ( solar_arr.length > 0 ) {
        $.each( solar_arr, function( solar_index, solar_data ) {
          var clean_solar_data = convert_mongoose_object_to_js_object(solar_data);
          clean_solar_arr.push(clean_solar_data);
        });
      };

      var time_unit_array = convert_to_time_unit(clean_solar_arr, ime_rampe, time_grupa, from_time, to_time );
      res.json({ success: true, solar_data_history: time_unit_array, solar_data_current: current_solar_data });

    });    

  })
  .catch(function(error){

    res.json({ success: false, error: error });  

  });

} else {

  console.log('nije pronašao rampu za solar data  !!!!!!');
  // res.sendStatus(404);
  res.json({ success: false, error: 'nije pronašao rampu za solar data request !!!!!!' });
};

}; // kraj get solar data


exports.motion_detect = function(req, res) {

var rampa_name = req.params.ime_rampe;
var start_or_stop = req.params.start_or_stop;


var rampa = find_rampa( rampa_name );

if ( rampa ) {
  // šaljem message prema specifičnoj rampi tj prema specifičnom socket id-ju
  // ako rampa pošalje nazad odgovor to šće biti zabilježeno unuter listenera  on "connection" !!!


  if ( start_or_stop == 'start' ) global.we_io.to(`${rampa.socket_id}`).emit(`${rampa.rampa_name}_start_picam`, 'start');
  if ( start_or_stop == 'stop' ) global.we_io.to(`${rampa.socket_id}`).emit(`${rampa.rampa_name}_stop_picam`, 'stop');

  global.we_motion_detect[rampa.rampa_name] = null;


  function send_response(res, rampa) {
    return function() { 
      res.send({ success: true, state: global.we_motion_detect[rampa.rampa_name] });
    };

  };


  wait_for(
    `global.we_motion_detect['${rampa.rampa_name}'] !== null`,
    send_response(res, rampa),
    1000*10, // timeout 10 sec
    function() { res.sendStatus(404); }
  );


} else {

  console.log('nije pronašao rampu za koju treba motion detect !!!!!!');
  res.sendStatus(404);

};


/*
fs.readFile( path.join(__dir_name + `/public/img/cameras/${rampa_name}.jpg`), function(err, image) {

});
*/

};



function create_status_email( email_subject, email_body ) {

  
var email_body = `<div style="width: 100%; float: left; text-align: center;">STATUS</div>
                  <div style="  width: 100%;
                                float: left;
                                font-weight: 700;
                                text-align: center;
                                font-size: 13px;
                                margin-bottom: 10px;">${ email_body }</div>`;  

var html = `

<div style="height: auto;
          text-align: center;
          font-family: Arial, Helvetica, sans-serif;
          background-color: #f3fcff;
          padding: 20px 0 0;">

  <div style="height: auto;
              text-align: center;
              font-family: Arial, Helvetica, sans-serif;
              padding: 3px;">


    <div style="  font-size: 16px;
                  margin-top: 20px;
                  margin-bottom: 20px;
                  color: #1a2840;
                  font-weight: 400;
                  line-height: 22px;
                  letter-spacing: normal;">

      ${ email_subject } <br>
    </div>

    <div style="    
      font-size: 13px;
      margin-top: 40px;
      margin-bottom: 20px;
      color: #454545;
      font-weight: 400;
      text-align: left;
      line-height: 26px;
      max-width: 400px;
      clear: both;
      width: auto;
      margin: 0 auto;
      padding: 10px;
      height: auto;
      background-color: #fff;
      border-radius: 10px;
      border: 1px solid #e8e6e6;
      letter-spacing: 0.04rem;">

<br style="display: block; clear: both;">

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 
${email_body}

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>

<br style="display: block; clear: both;">

</div>

<br style="display: block; clear: both;">

</div>


  <div style="height: auto;
              text-align: center;
              font-family: Arial, Helvetica, sans-serif;
              background-color: #f3fcff;
              padding: 0;">



      <img style="
        width: 150px;
        height: auto;
        max-width: 200px;
        border-radius: 0;
        margin-top: 100px;
        display: block;
        float: none;
        clear: both;
        margin: 20px auto;"
        alt="WePark Logo" 
        src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />

<br>
<br>

    <div id="u_${Date.now() + 123}" style="
      font-size: 11px;
      margin-top: 0;
      color: #727272;
      font-weight: 400;
      float: left;
      width: 100%;
      line-height: 18px;
      border-top: 1px solid #eaeaea;
      padding-top: 10px;
      background-color: #fff;
      letter-spacing: 0.06rem;">

        <br>
        <br>
        <b style="font-size: 16px;" id="u_${Date.now() + 222}" >MOLIMO ZA STRPLJENJE</b><br>
        <b style="font-size: 16px;">Odgovorne osobe rade na otklonu problema.</b><br><br>
        <b style="font-size: 16px;">Ako imate bilo koji komentar ili napomenu, molimo nazovite broj:</b><br>
        <a  href="tel:00385923133015" 
            style=" padding: 12px;
                    text-decoration: none;
                    color: #fff;
                    background-color: #e64531;
                    margin: 10px auto;
                    display: block;
                    border-radius: 20px;
                    font-size: 20px;
                    font-weight: 700;
                    max-width: 250px;">00385 92 3133 015</a>

      <br>

        <b>Wepark smart parking d.o.o.</b><br>
        <b>OIB:</b>&nbsp;77372456863<br>
        <b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
        <b>OFFICE:</b>&nbsp;Janka Rakuše 1, Zagreb<br>
        <b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
        <b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
        <b>WEB:</b>&nbsp;www.wepark.eu<br>
        <br>
        <div id="u_${Date.now() + 555}" style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
        Društvo je upisano u sudski registar Trgovačkog suda u Zagrebu pod brojem 081131571. Temeljni kapital u iznosu od 20.000 kn je uplaćen u cijelosti. Račun se vodi kod Privredne Banke Zagreb. Član društva Karlo Starčević.
        </div>
        <br style="display: block; clear: both;">


    </div>        

    <br style="display: block; clear: both;">
  </div>  


</div>

`;

return html;


};  



global.send_rampa_status_by_mail = function( email_subject, email_body, user, to_user_only ) {

  return new Promise( function( resolve, reject ) {

    var transporter = nodemailer.createTransport(global.we_mail_setup);

    var to_string = user ? ( user.email + ', tech@wepark.eu' ) : 'tech@wepark.eu';

    
    if (user && user.name == "metascript_contact") to_string = 'tkutlic@gmail.com';
    
    
    
    if ( to_user_only == 'to_user_only' ) to_string = user.email;


    // **********************************************************************
    // ako je port 8080 to znači da je lokalni server na mom kompu 
    // pa zato trebam slati mailove samo sebi dok testiram

    if ( server_settings.port == 8080 ) {
      to_string = 'tkutlic@gmail.com';
    };
    // **********************************************************************

    if ( user ) {
      email_body = `Bok ${user.name},<br>` + email_body; 
    };

    var email_html = create_status_email( email_subject, email_body );


    var mailOptions = {
      from: `"WePark App" <${global.we_email_address}>`, // sender address 

      /* TODO -------> VRATI NAZAD ISPOD NA VARIJABLU to_string !!!!!!!!!!!!!!!!!!  */
      /* TODO -------> VRATI NAZAD ISPOD NA VARIJABLU to_string !!!!!!!!!!!!!!!!!!  */
      /* TODO -------> VRATI NAZAD ISPOD NA VARIJABLU to_string !!!!!!!!!!!!!!!!!!  */

      to: to_string, 
      subject: email_subject, // Subject line
      html:  email_html // html body
    };

      // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {

      if (error) {
       console.error({ 
          success: false, 
          msg: "Status rampe nije poslan!!! Problem u slanju maila sa statusom rampe !!!!",
          error: error
        });

        reject({ 
          success: false, 
          msg: "Status rampe nije poslan!!! Problem u slanju maila sa statusom rampe !!!!",
          error: error
        });

      } else {

        console.log({ 
          success: true, 
          msg: "Mail sa statusom rampe je poslan OK !!!",
        });

        resolve({ 
          success: true, 
          msg: "Mail sa statusom rampe je poslan OK !!!",
        });

      };

    }); // end of send email


  }); // kraj promisa

}; // kraj func send rampa status by mail


function find_user_by_number(user_number) {

return new Promise( function( resolve, reject ) {
  User.findOne({ user_number: user_number })
  .exec(function(err, user) {

    if (err) {
      reject({ success: false, msg: 'Greška kod pretrage usera', err: err });
      return;
    } else { 

      if ( !user ) {
        reject({ success: false, msg: 'Nije našao usera' });
        return;
      };

      var clean_user = convert_mongoose_object_to_js_object( user );
      resolve({ success: true, msg: 'nasao usera po broju', user: clean_user });

    };

  }); // kraj exec od user find

});

};  


global.rampa_prijave_errors = {};
global.rampa_status_errors = {};

// ovaj datum je 31.07.2020 u 17:13
global.last_30sec_to_1hour_time = 1596208433118;





setInterval(function() {

$.each( global.sve_rampe, function(rampa_index, rampa_obj) {

  var loop_rampa_name = rampa_obj.rampa_name;


  // ako je prošlo više od 5 min da se neka od rampi nije prijavila
  // POŠALJI STATUS ALERT NA EMAIL !!!!!! 
  if ( Date.now() - rampa_obj.time > 1000*60*5 ) {

    // šalji samo ako već nisam zabilježio prijašnje error tj error već postoji
    if ( !global.rampa_prijave_errors[loop_rampa_name] ) {

      // zapiši vrijeme tako da znam da sam poslao status
      global.rampa_prijave_errors[loop_rampa_name] = Date.now();
      var fino_ime_rampe = loop_rampa_name.replace('_', ' ').toUpperCase();
      var email_subject = `NE ODAZIVA SE ${fino_ime_rampe} !!!`;

      var email_body = 
`
Rampa se nije prijavila na server duže od 5 minuta!<br>
Ako možete, pričekajte još nekoliko minuta jer postoji mogućnost da je došlo do privremenog prekida internet veze. Ako za par minuta ne dobijete mail da je rampa ponovo online potrebno je otići na tu lokaciju i provjeriti kvar!
<br>
<br>
Ako ubrzo dobijete mail da je rampa ponovo online, zanemarite ovaj mail.
`;

      global.send_rampa_status_by_mail(email_subject, email_body); 


    }; // kraj ako nema prijašnjeg errora 

  }; // kraj ako je proslo vše od 5 min



  // ako je prošlo više od jednog dana 
  if ( Date.now() - global.last_30sec_to_1hour_time > 1000*60*60*24*10 ) {

    // obnovi time stamp 
    // ---------- VAŽNO -------------- IKLJUČIO SAM OVO JER NAM TREBA DETALJNA STATISTIKA
    // ---------- VAŽNO -------------- IKLJUČIO SAM OVO JER NAM TREBA DETALJNA STATISTIKA
    // ---------- VAŽNO -------------- IKLJUČIO SAM OVO JER NAM TREBA DETALJNA STATISTIKA
    
    
    // global.last_30sec_to_1hour_time = Date.now();
    // global.convert_30sec_to_SAT(loop_rampa_name);
    
    
    // ---------- VAŽNO -------------- IKLJUČIO SAM OVO JER NAM TREBA DETALJNA STATISTIKA
    // ---------- VAŽNO -------------- IKLJUČIO SAM OVO JER NAM TREBA DETALJNA STATISTIKA
    // ---------- VAŽNO -------------- IKLJUČIO SAM OVO JER NAM TREBA DETALJNA STATISTIKA
    
    
    

  }; // kraj uvjeta da je prošlo 24 sata od zadnjeg pokretanja


});  


}, 1000*55);




global.convert_30sec_to_SAT = function(loop_rampa_name) {

var converted_1hour_array = [];
var delete_these_30sec_array = [];

// sve 30 sec zapise starije od 10 dana zbroji u zapise u tip 1hour  
Solar.find({ time: { $lt: (Date.now() - 1000*60*60*24 * 10 ) }, frequency: '30sec', rampa_name: loop_rampa_name })
.exec(function(err, solar_arr) {

  if (err) {
    console.error({
      success: false,
      msg: 'greška u traženju solar data starijih od 10 dana sa 30sec frekvencijom',
      error: err 
    });
    return;
  };

  var clean_solar_arr = [];

  if ( solar_arr.length > 0 ) {
    $.each( solar_arr, function( solar_index, solar_data ) {

      var clean_solar_data = convert_mongoose_object_to_js_object(solar_data);
      clean_solar_arr.push(clean_solar_data);

      // pushaj sve ove od 30 sec u array za brisanje
      delete_these_30sec_array.push(clean_solar_data._id);
    });
  };

  var dest_arr = convert_to_time_unit(clean_solar_arr, loop_rampa_name, 'SAT');

  // dodaj 1hour objekte u bazu  
  write_solar_data_to_db(loop_rampa_name, dest_arr, 'SAT' );

  // obriši 30sec objekte koji su sada zamjenjeni sa 1hour objektima
  delete_30sec_solar_data(delete_these_30sec_array);

});   // kraj querija za solar 30sec objekta starije od 10 dana

};



function convert_to_time_unit(clean_solar_arr, loop_rampa_name, time_unit, arg_from_time, arg_to_time ) {

var target_dest_array = [];

if ( !clean_solar_arr || ($.isArray(clean_solar_arr) && clean_solar_arr.length == 0) ) return [];


// sortiraj uzlazno od manjeg vremena prema većem
if ( clean_solar_arr.length > 1 ) clean_solar_arr.sort( window.by('time', false, Number ) );


var last_ind = clean_solar_arr.length - 1;

  // prvo dobijem razliku vremena u milisec
var time_unit_count =  clean_solar_arr[last_ind].time - clean_solar_arr[0].time;

// onda podjelim s određenim brojem da dobijem koliko je prošlo vermena u odabranom unitu
if ( time_unit == 'MIN') time_unit_count = 1440; // time_unit_count = Math.ceil( time_unit_count / (1000*60) );
if ( time_unit == 'SAT') time_unit_count = Math.ceil( time_unit_count / (1000*60*60) );
if ( time_unit == 'DAN') time_unit_count = Math.ceil( time_unit_count / (1000*60*60*24) );


var curr_unit_counter = 0;
var new_objects_counter = 0;

var start_unit = null;

// uzmi prvi time i napravi od njega objekt date tj. start datum
var start_time = arg_from_time; //clean_solar_arr[0].time;
var start_datum = new Date( arg_from_time );

var end_time = arg_to_time; // clean_solar_arr[last_ind].time;
var end_datum = new Date( arg_to_time );



var start_godina = start_datum.getFullYear();
var start_mjesec = start_datum.getMonth();
var start_dan = start_datum.getDate();
var start_sat = start_datum.getHours();


var sum_for_dest = {
  ch_0: 0,
  ch_1: 0,
  ch_2: 0,
  ch_3: 0
};

var average_for_dest = {
  ch_0: 0,
  ch_1: 0,
  ch_2: 0,
  ch_3: 0
};


var new_unit_solar_object = {
  frequency: time_unit,
  channels: average_for_dest,
  time: start_time,
  rampa_name: loop_rampa_name,
  count: null,
};


var unit = 0;
var curr_unit_date = 0;
var next_unit_date = 0;

// dodajem 10 unita više za svaki slučaj da izgleda ljepše na grafu
for( unit = 0; unit <= time_unit_count+10; unit++ ) {

  if ( time_unit == 'MIN') curr_unit_date = new Date(start_godina, start_mjesec, start_dan, start_sat, unit );
  if ( time_unit == 'SAT') curr_unit_date = new Date(start_godina, start_mjesec, start_dan, unit );
  if ( time_unit == 'DAN') curr_unit_date = new Date(start_godina, start_mjesec, unit );

  // bitno je da uvijek resetitam next date tako da na last itemu u arrayu ne ispuni uvijet
  next_unit_date = 0;
  if ( unit <= time_unit_count+10) {
    if ( time_unit == 'MIN') next_unit_date = new Date(start_godina, start_mjesec, start_dan, start_sat, unit+1 );
    if ( time_unit == 'SAT') next_unit_date = new Date(start_godina, start_mjesec, start_dan, unit+1 );
    if ( time_unit == 'DAN') next_unit_date = new Date(start_godina, start_mjesec, unit+1 );
  };    


  // za svaki slučaj provjeri jel nisam prekoračio vrijeme tj nakon end time
  if ( Number(curr_unit_date) <= end_time ) {

    /* ----------------- resetiraj sum obj, avarage obj i counter  */
    sum_for_dest = {
      ch_0: 0,
      ch_1: 0,
      ch_2: 0,
      ch_3: 0
    };

    average_for_dest = {
      ch_0: 0,
      ch_1: 0,
      ch_2: 0,
      ch_3: 0
    };

    curr_unit_counter = 0;

    var prvi_time_unutar_unita = null;

    // loop po svim objektima koje sam našao u queriju
    $.each( clean_solar_arr, function( c_index, c_solar ) {


      if (
        c_solar.time >= Number(curr_unit_date) 
        &&
        c_solar.time < Number(next_unit_date) 
      ) {


        if ( prvi_time_unutar_unita == null ) prvi_time_unutar_unita = c_solar.time;

        sum_for_dest.ch_0 += c_solar.channels.ch_0;
        sum_for_dest.ch_1 += c_solar.channels.ch_1;
        sum_for_dest.ch_2 += c_solar.channels.ch_2;
        sum_for_dest.ch_3 += c_solar.channels.ch_3;

        curr_unit_counter += 1;

        average_for_dest.ch_0 = sum_for_dest.ch_0 / curr_unit_counter;
        average_for_dest.ch_1 = sum_for_dest.ch_1 / curr_unit_counter;
        average_for_dest.ch_2 = sum_for_dest.ch_2 / curr_unit_counter;
        average_for_dest.ch_3 = sum_for_dest.ch_3 / curr_unit_counter;

      };


    }); // kraj loopa po svim solar data itemima  


    // ako je našao barem jedno vrijeme unutar zadanog unita npr unutar jednog SATA
    if ( prvi_time_unutar_unita !== null ) {

      new_unit_solar_object = {
        frequency: time_unit,
        channels: average_for_dest,
        time: prvi_time_unutar_unita,
        rampa_name: loop_rampa_name,
        count: null,
      };

    } 
    else {
      // ako nije našao niti jedan objekt unutar trenutnog unita
      new_unit_solar_object = {
        frequency: time_unit,
        channels: { ch_0: null, ch_1: null, ch_2: null, ch_3: null },
        time: Number(curr_unit_date) + 100, // postavi vrijeme unita + 100 ms
        rampa_name: loop_rampa_name,
        count: null,
      };

    }; 

    new_objects_counter += 1;
    target_dest_array.push(new_unit_solar_object);

  }; // kraj ifa da je current time manje ili jednak end time

}; // kraj for loopa ----- > time unit count

return target_dest_array;

}; // kraj convert to time grupa








// ------------------------------- BARRIER CONTROLLERS -------------------------------
// ------------------------------- BARRIER CONTROLLERS -------------------------------
// ------------------------------- BARRIER CONTROLLERS -------------------------------
// ------------------------------- BARRIER CONTROLLERS -------------------------------


function find_is_barrier_taken_by_user( park_data, user_number ) {
  
  var taken_barrier = null;
  var found_taken_bar = false;
  
  if ( 
    park_data.barriers            && 
    $.isArray(park_data.barriers) && 
    park_data.barriers.length > 0 
  ) { 
    
    // --------------- PRVO GLEDAJ JEL USER IMA RESERVED BARRIER  ---------------
    $.each(park_data.barriers, function(bar_ind, bar) {
      if ( 
        found_taken_bar == false                       &&
        Number(bar.user_number) == Number(user_number) && 
        bar.reserved == true 
      ) {
        found_taken_bar = true;
        taken_barrier = bar;
      };
    });
   
    
    
    // --------------- ONDA GLEDAJ JEL USER IMA PARKED BARRIER  ---------------
    $.each(park_data.barriers, function(bar_ind, bar) {
      if ( 
        found_taken_bar == false                       &&
        bar.state !== null                             &&
        Number(bar.user_number) == Number(user_number) &&
        bar.reserved == null
      ) {
        found_taken_bar = true;
        taken_barrier = bar;
      };
    });    
    
  };
  
  return taken_barrier;
  
};

function find_first_free_or_specific_barrier( park_data, arg_bar_name ) {
  
  var found_bar = null;
  var sve_zauzete_barijere = [];
  
  if ( park_data.barriers && $.isArray(park_data.barriers) && park_data.barriers.length > 0 ) { 
    
    // nadji specifičnu barijeru 
    if ( arg_bar_name ) {
      $.each(park_data.barriers, function(bar_ind, bar) {
        
        if ( bar.name == arg_bar_name && bar.user_number == null && found_bar == null ) {
          found_bar = bar;
        };
        
        if ( bar.user_number !== null ) {
          sve_zauzete_barijere.push(bar);
        };
        
      });
    };
    
    // ako nema argumenta imena barijere onda naji prvu slobodnu barijeru
    if ( !arg_bar_name ) {
      $.each(park_data.barriers, function(bar_ind, bar) {
        
        if ( found_bar == null && bar.user_number == null && bar.reserved == null ) {
          found_bar = bar;
        };
        
        if ( bar.user_number !== null ) {
          sve_zauzete_barijere.push(bar);
        };
        
      });
    };
    
  }; // kraj ifa jel postoje barijere u parku 
  
  return {
    barrier: found_bar,
    all_taken: sve_zauzete_barijere
  };
  
};


exports.get_db_barrier_state = function(req, res) {
  
  var pp_sifra = req.params.pp_sifra;
  var user_number = Number(req.params.user_number);
  
  var bar_state = null;
  var bar_name = null;
  var first_free_bar = null;
  
  ParkPlace.findOne({ pp_sifra: pp_sifra }, function(err, specific_pp) {
    if (err) {
      res.send({ success: false, msg: "GREŠKA U DOBIVANJU SPECIFIC PARKIRALIŠTA za state barijere", error: err});
      return;
    };
    
    if ( specific_pp ) {
            
      var clean_specific_pp = convert_mongoose_object_to_js_object(specific_pp);
      
      
      // prvo provjeri jel sam ja rezervirao ili parkirao na tu barijeru
      var taken_by_user = find_is_barrier_taken_by_user( clean_specific_pp, user_number );

      // ako jeste ovaj user zauzeo tu barijeru onda uzmi ime i state
      if ( taken_by_user ) {
        bar_name = taken_by_user.name;
        bar_state = taken_by_user.state;
      } 
      else {
        // ako user nije zauzeo niti jednu barijeru u ovom parkingu
        first_free_bar = find_first_free_or_specific_barrier(clean_specific_pp).barrier;
        
        if ( first_free_bar ) {
          bar_name = first_free_bar.name;
          bar_state = first_free_bar.state;
        };
        
      };

      // ako user nije zauzeo niti jednu barijeru i nema prve slobodne barijere
      // TO ZNAČI DA SU SVE BARIJERE ZAUZETE !!!!!!!
      if ( !taken_by_user && !first_free_bar ) {
        res.json({ success: false, msg: "popup_sve_barijere_zauzete", state: null, name: null });
        return;
      };

      res.json({ success: true, msg: "SPECIFIC barrier", state: bar_state, name: bar_name });
      
    } 
    else {
      
      res.json({ success: false, msg: "NIJE NAŠAO PARK!!!!", state: null, name: null });
      
    }; // kraj ifa ako je našao specific park
  
  }); // kraj park place find query
  
}; // kraj cijele func get db barrier state


function update_barrier_state_query( pp_sifra, user_number, bar_name, new_state ) {
  
  if ( new_state == 'null' || new_state == 'undefined' || new_state == ''  ) new_state = null;   
  // ako je state null to znači da user podiže barijeru i odlazi 
  // pa stoga i user number treba biti null
  if ( new_state == null ) user_number = null;
  
  return new Promise( function( resolve, reject ) {

    var specific_barrier = null;

    ParkPlace.findOne({ pp_sifra: pp_sifra }, function(err, specific_pp) {
      
      if (err) {
        reject({ success: false, msg: "GREŠKA U DOBIVANJU SPECIFIC PARKIRALIŠTA za update barijere", error: err});
        return;
      };

      if ( specific_pp ) {

        var clean_specific_pp = convert_mongoose_object_to_js_object(specific_pp);

        if ( 
          clean_specific_pp.barriers             &&
          $.isArray(clean_specific_pp.barriers)  &&
          clean_specific_pp.barriers.length > 0
        ) {
          
          $.each( clean_specific_pp.barriers, function( bar_index, bar_obj ) {
            
            if ( bar_obj.name == bar_name ) {
              
              clean_specific_pp.barriers[bar_index].state = new_state;
              clean_specific_pp.barriers[bar_index].user_number = user_number;
              
              specific_barrier = bar_obj;
            };
            
          });
          
        } else {
          
          reject({ success: false, msg: "parking nema niti jednu barijeru", specific_barrier: null });
          return;
        };

        ParkPlace.findOneAndUpdate(
        { pp_sifra: pp_sifra },
        { $set: {barriers: clean_specific_pp.barriers }},
        {new: true}, 
        function(err, updated_park) {

          if (err) {
            
            reject({
              success: false,
              msg: "NIJE uspio napraviti update za novo stanje od ove specific barrier:",
              specific_barrier: specific_barrier,
              error: err
            });
            
            return;
          };

          resolve({ success: true, msg: "SPECIFIC barrier", specific_barrier: specific_barrier });
          
        }); // kraj update park place

      }; // kraj ifa ako je našao specific park

    }); // kraj park place find query
  
  }); // kraj promisa
  
}; // kraj cijele func update db barrier state

exports.update_barrier_state_query = update_barrier_state_query;


exports.update_db_barrier_state = function(req, res) {
  
  var pp_sifra = req.params.pp_sifra;
  var bar_name = req.params.bar_name;
  var user_number = Number(req.params.user_number);
  var up_or_down = req.params.up_or_down;
  
  var new_state = (up_or_down == 'down') ? 'parked' : null;
  
  update_barrier_state_query( pp_sifra, user_number, bar_name, new_state)
  .then(
  function(bar_state) {
    res.json(bar_state);
  })
  .catch(
  function(error) {
    res.json(error);
  });
  
};


exports.metascript_contact_form = function(req, res) {
  
  var full_name = req.body.full_name;
  var contact_email = req.body.contact_email;
  var text = req.body.text;
  
  var captcha = req.body.captcha;
  
  var email_body = 
`
${full_name}<br><br>
${contact_email}<br><br>
${text}

`;
  
  
  var req_obj = {
    secret: "6LdJpyMaAAAAAMpvuy-gw4hI-4-OP4dAESZYDt7C",
    response: captcha
  };
  
  axios.post('https://www.google.com/recaptcha/api/siteverify', null, { params: req_obj })
  .then(function (response) {
    
    var data = response.data;
    
    console.log('response data od google captche:')
    console.log(data);
    
    if (data.success == true) {
      
      global.send_rampa_status_by_mail('METASCRIPT WEB CONTACT', email_body, {name: 'metascript_contact'})
      .then(
      function(success) {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', 'http://metascript.hr');
        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        
        res.json({ success: true, msg: 'OK' });

      })
      .catch(
      function(mail_error) {
        res.json({ success: false, msg: 'NODE MAILER ERROR', error: mail_error });
      });
      
      
    } else {
      // ako nije uspješna verifikacija captche !!!!!!
      res.json({ success: false, msg: 'CAPTCHA ERROR', error: data });
    };
    
  })
  .catch(function (error) {
    
    // AXIOS REQ ERROR !!!!
    console.log(error);
    res.json({ success: false, msg: 'AXIOS REQ ERROR', error: error });
    
  });
  
};


exports.move_shelly = function(req, res) { 
  
  var pp_sifra = req.params.pp_sifra;
  var bar_name = req.params.shelly_name;
  var user_number = parseInt(req.params.user_number);
  var up_or_down = req.params.up_or_down;


// CREATING JAVASCRIPT FORM OBJECT (multipart/form-data)
    var shelly_data = new FormData();
    shelly_data.append('auth_key', 'MmRlNGV1aWQCD15A40D87B94FCB638FA7803894238061DF811CC43E5A834D3049A894DBBC8BFF7AF909277CFD75');

    shelly_data.append('channel', '0');
    shelly_data.append('turn', 'on');
    
    if ( up_or_down == 'up' ) shelly_data.append('id', global.all_shellys[bar_name].up);
    if ( up_or_down == 'down' ) shelly_data.append('id', global.all_shellys[bar_name].down);
    
    
    shelly_data.submit('https://shelly-15-eu.shelly.cloud/device/relay/control/', function(shelly_err, shelly_res) {
      // res – response object (http.IncomingMessage)  //

      console.log(shelly_res);
      console.log(shelly_err);
      
      if ( shelly_err ) {
        
        res.json({ success: false, msg: 'popup_greska_prilikom_parkinga' });
        
      } else {
      
        if (shelly_res.statusCode == 200) {
          
          var new_state = (up_or_down == 'down') ? 'parked' : null;
          
          update_barrier_state_query( pp_sifra, user_number, bar_name, new_state );
          
          res.json({ success: true, msg: 'shelly is ' + up_or_down });
          
          
        } else {
          res.json({ success: false, msg: 'popup_greska_prilikom_parkinga' });
        };
        
      };
      shelly_res.resume();
    });
  
  
};







