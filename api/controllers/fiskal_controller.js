process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var path = require("path");

var request = require('request');
var fs = require('fs');

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


var uuidv1 = require('uuid/v1');
var md5 = require("blueimp-md5");
var node_crypto = require('crypto');
var xmldom = require("xmldom");
var c14n = require("xml-c14n")();

var nodemailer = require('nodemailer');

var x2jconverter = require('fast-xml-parser');

var CustomerProduct = mongoose.model('CustomerProduct');
var User = mongoose.model('User');


var racun_template = require('./racun_body');


/* /////////////////////////    ZAOKRUŽI BROJ   ///////////////////////////////////// */
function zaokruzi(broj, brojZnamenki) {

 var broj = parseFloat(broj);

 if ( typeof brojZnamenki == 'undefined' ) {
   brojZnamenki = 0;
 };
 var multiplicator = Math.pow(10, brojZnamenki);
 broj = parseFloat((broj * multiplicator).toFixed(11));
 var test =(Math.round(broj) / multiplicator);
 // + znak ispred je samo js trik da automatski pretvori string u broj
 return +(test.toFixed(brojZnamenki));
};


function zaokruziIformatirajIznos(num, zaokruzi_na) {
  // primjer : var num = 123456789.56789;

  var brojZnamenki = 2;
  if ( typeof zaokruzi_na !== 'undefined' )  brojZnamenki = parseInt( zaokruzi_na );
  

  var zaokruzeniBroj = zaokruzi(num, brojZnamenki);
  

  var stringBroja = String(zaokruzeniBroj);
  // 123456789
  var lijeviDio = stringBroja.split('.')[0] + "";
  var tockaPostoji = stringBroja.indexOf(".");
  var desniDio = "00";
  if (tockaPostoji > -1) {
    desniDio = stringBroja.split('.')[1] + "";
  };
  

  /*
  if (Number(desniDio) < 10) {
    desniDio = '0' + Number(desniDio);
  };
  */

  
  var broj_je_pozitivan = true;
  
  // ako je broj negativan 
  if ( lijeviDio.substring(0,1) == '-' ) {
    lijeviDio = lijeviDio.substring(1);
    broj_je_pozitivan = false;
  };
  
  var stringLength = lijeviDio.length;
  
  // ne zanimaju me brojevi s tri znamenke i manje jer oni nemaju točke
  if (stringLength >= 4) {
    // pretvaram string u array
    lijeviDio = lijeviDio.split('');
    // gledam koliko stane grupa po 3 znamaneke
    // i onda uzmem ostatak - to je jednostavna modulus operacija
    var dotPosition = stringLength % 3;
    // zanima me koliko treba biti točaka u broju
    // to dobijem tako da podijelim broj znamenki sa 3 i gledam samo cijeli broj
    var dotCount = parseInt(stringLength / 3)

    // postavim prvu točku
    // NAPOMENA u slučajevima kada je modulus = 0 ova funkcije postavi točku čak i na počatek broja
    // tu točku kasnije obrišem
    // svaka sljedeća točka je UVIJEK 4 pozicije udesno !!!!
    // napomena - logično bi bilo da je svake 3 pozicije udesno, 
    // ali moram brojati i točku koju sam stavio i ona povećava length od stringa
    for (i = 1; i <= dotCount; i++) {
      // stavi točku
      lijeviDio.splice(dotPosition, 0, ".");
      // id na poziciju current + 4 
      dotPosition = dotPosition + 4;
    };
    
    
    // spoji nazad sve znakove u jedan string !!!
    lijeviDio = lijeviDio.join('')

    // kad se dogodi da je modulus === 0 onda postavim točku na nultu poziciju
    // i sad je moram obrisati !!!!!!!
    if (lijeviDio.charAt(0) === ".") {
      // uzimam sve osim prvog znaka tj točke
      lijeviDio = lijeviDio.substring(1);
    }
  };

  // ponovi nulu onoliko puta koliko treba da decimale imaju onoliko stringova 
  // koliko je definirano sa argumentom [zaokruzi_na] 
  // ( ... od kojeg gore dobijem varijablu brojZnamenki )
  if (desniDio.length < brojZnamenki) {
    desniDio = desniDio + '0'.repeat(brojZnamenki - desniDio.length);
  };

  
  var formatiraniBroj = lijeviDio + "," + desniDio;

  if ( desniDio == "00" &&  brojZnamenki == 0 )  formatiraniBroj = lijeviDio;
  
  if ( broj_je_pozitivan == false ) formatiraniBroj = '-' + formatiraniBroj;
  
  return formatiraniBroj;
  
};



function convert_mongoose_object_to_js_object(mongoose_object) {
  
  var js_object = mongoose_object;
  
  try {
    js_object = mongoose_object.toJSON();
  }
  catch(err) {
    console.log(err.message);
  }; 
  
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};


function generiraj_fiskal_time(time) {
  
  var unix_time = time || Date.now();
  var time_now = new Date(unix_time);

  var yyyy = time_now.getFullYear() + '';
  
  var mon = time_now.getMonth() + 1 + '';
  if ( mon.length == 1 ) mon = '0' + mon;
  
  var dd = time_now.getDate() + '';
  if ( dd.length == 1 ) dd = '0' + dd;

  var hh = time_now.getHours() + '';
  if ( hh.length == 1 ) hh = '0' + hh;

  var mm = time_now.getMinutes() + '';
  if ( mm.length == 1 ) mm = '0' + mm;

  var ss = time_now.getSeconds() + '';
  if ( ss.length == 1 ) ss = '0' + ss;

  var fiskal_time = dd + '.' + mon + '.' + yyyy + 'T' + hh + ':' + mm + ':' + ss;
  
  return fiskal_time;
  
  
};


global.send_echo = function() {


  var echo_primjer = 
  `<?xml version="1.0" encoding="UTF-8"?>
  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
  xmlns:f73="http://www.apis-it.hr/fin/2012/types/f73">
   <soapenv:Body>
   <f73:EchoRequest>PROBAAAAAA</f73:EchoRequest>
   </soapenv:Body>
  </soapenv:Envelope>
  `; 

  var echo_options = {

    url: global.fiskal_url,
    headers: {
      'Content-Type': 'text/xml;charset=UTF-8',
      'soapAction': "http://e-porezna.porezna-uprava.hr/fiskalizacija/2012/services/FiskalizacijaService/echo"
    },
    body : echo_primjer,
    agentOptions: {
      pfx: fs.readFileSync(__dirname + global.fis_cer_p12_path ),
      passphrase: 'ToNi3007'
    }
  };

  

    // return;
  request.post(echo_options, (error, response, body) => {
      console.log(error);
      console.log(response);
      console.log(body);
  });  


}; // KRAJ SPREMI PARK PLACE 


function send_racun_by_mail(customer_product, user) {
  
  
  var transporter = nodemailer.createTransport(global.we_mail_setup);
    

  var html = racun_template.html_racuna(customer_product, user);

  // setup email data
  
  var mailOptions = {
      from: `"WePark App" <${global.we_email_address}>`, // sender address 
      to: user.email, // list of receivers
      cc: 'racuni@wepark.eu',
      subject: 'WePark Račun', // Subject line
      html:  html // html body
  };

    // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {

    if (error) {
     console.error({ 
        success: false, 
        msg: "Račun nije poslan!!! Problem u slanju fiskalnog računa putem emaila !!!!",
        error: error
      });

    } else {

      console.log({ 
        success: true, 
        msg: "Mail je poslan i sve je ok",
      });

    };


  }); // end of send welcome email

  
};


global.send_racun_by_mail = send_racun_by_mail;

/*
NE KORISTIM VIŠE JER USERA DOBIJEM VEĆ ODMAH KAO ARGUMENT UNUTER RUN FISKAL

function find_user_and_send_racun_by_mail(customer_product, arg_user) {

  // var user_number = customer_product.code.split('-')[0];
  // user_number = Number( user_number );
  
  var user_number = Number( customer_product.user_number ); 
  
  
  User.findOne(
  { user_number: user_number }, 
  function(err, user) {
    if (err) {

        console.error({ 
          success: false, 
          msg: "Dogodila se greška u traženju usera!!!",
          user_number: null,
        });
      return;
    }
    
    if ( user ) {
      
      var clean_user = convert_mongoose_object_to_js_object(user);

      
      if ( clean_user.email ) send_racun_by_mail( customer_product, clean_user );
      
    };

  }); // end of User.findOne

};

*/


global.moj_mail = "tkutlic@gmail.com";


  global.fiskal_product_1 =  {
    _id: "60129ecaf7ae9adc91af6d3f",
    "sifra" : "1824-novi_storno_39",
    "pp_sifra" : "3",
    "code" : "662-pretplata-1-3-1609696984991--",
    "amount" : -252,
    "desc" : "CARD-MAESTRO",
    "buy_time" : 1611832962736.0,
    "user_number" : 777,
    "is_free" : false,
    "price" : -252,
    "type" : "monthly",
    "valid_from" : 1609697159277.0,
    "valid_to" : 1612289159277.0,
    "balance" : -252,
    "tag" : null,
    "racun_br" : "8",
    "storno" : 39
  };
  
  

  global.fiskal_product_2 =  {
    _id: "60129efaf7ae9adc91af6d76",
    "sifra" : "1826_novi_storno_40",
    "pp_sifra" : "3",
    "code" : "655-pretplata-1-3-1609769885914--",
    "amount" : -252,
    "desc" : "CARD-VISA",
    "buy_time" : 1611832962736.0,
    "user_number" : 777,
    "is_free" : false,
    "price" : -252,
    "type" : "monthly",
    "valid_from" : 1609770018319.0,
    "valid_to" : 1612362018319.0,
    "balance" : -252,
    "tag" : null,
    "racun_br" : "9",
    "storno" : 40
  };
  
  
  global.fiskal_product_3 =  {
    _id: "60129f19f7ae9adc91af6da1",
    "sifra" : "1824-storno_novi_storno_41",
    "pp_sifra" : "3",
    "code" : "662-pretplata-1-3-1609696984991--",
    "amount" : -252,
    "desc" : "CARD-MAESTRO",
    "buy_time" : 1611832962736.0,
    "user_number" : 777,
    "is_free" : false,
    "price" : -252,
    "type" : "monthly",
    "valid_from" : 0.0,
    "valid_to" : 100.0,
    "balance" : -252,
    "tag" : null,
    "racun_br" : "10",
    "storno" : 41
    
  };  
  
  
  global.fiskal_product_4 =  {
    _id: "60129f3ff7ae9adc91af6dd0",
    "sifra" : "1824-storno_novi_storno_42",
    "pp_sifra" : "3",
    "code" : "655-pretplata-1-3-1609769885914--",
    "amount" : -252,
    "desc" : "CARD-MAESTRO",
    "buy_time" : 1611832962736.0,
    "user_number" : 777,
    "is_free" : false,
    "price" : -252,
    "type" : "monthly",
    "valid_from" : 0.0,
    "valid_to" : 100.0,
    "balance" : -252,
    "tag" : null,
    "racun_br" : "11",
    "storno" : 42
    
  };   
  
  


global.fiskal_user =   {
  
    "_id" : "5d9f47f16ce0b535e5441a3a",
    "car_plates" : [ 
        {
            "time" : 1595510406347.0,
            "reg" : "AS8888HR"
        }, 
        {
            "time" : 1595510406347.0,
            "reg" : "AS4444HR"
        }, 
        {
            "time" : 1597684912335.0,
            "reg" : "AS1111HR"
        }
    ],
    "customer_tags" : null,
    "customer_vip" : [ 
        {
            "_id" : "5da6854316382f6f5875e04f",
            "pp_name" : "Visoko učilište Algebra"
        }
    ],
    "pp_owner_park_places" : null,
    "user_saved" : null,
    "user_edited" : {
        "user_id" : "5d9f47f16ce0b535e5441a3a",
        "user_number" : 777,
        "user_name" : "Circulum",
        "full_name" : "Proba Probić",
        "time_stamp" : 1571252634762.0
    },
    "saved" : 1570719728736.0,
    "edited" : 1571252634762.0,
    "last_login" : 1601023487729.0,
    "user_address" : "Nikole Tesle 41",
    "user_tel" : "2222",
    "business_name" : "Circulum Dev Team",
    "business_address" : "Kupinečka 54, 10000 Zagreb",
    "business_email" : "info@circulum.hr",
    "business_tel" : "01 555 555",
    "business_contact_name" : "Toni Kutlić",
    "oib" : "12345678912",
    "iban" : "2222222222222",
    "gdpr" : true,
    "gdpr_saved" : 1570719728736.0,
    "gdpr_accassed" : 1570719728736.0,
    "user_deleted" : null,
    "balance" : 500,
    "customer_acquisition" : {
        "sifra" : "AKV001",
        "naziv" : "MARKO"
    },
    "name" : "Circulum",
    "full_name" : "Circulum Dev Team",
    "hash" : "pbkdf2$10000$242301e586a8283ebb14358c2ee31328d66f7e5ce127aacc563d818b6132351238e65b911439030447f30b0449a9d8bd0c65a38399aa8f93c746acb139eb3717$eceee72a0dbe58605dff2f65468792f4906f8715c029194de114c90c84f3949d737a93ca8e9daafb8208286ba1384e14f118215c0af662470c446a9ab09fb5fa",
    "email" : "tkutlic@gmail.com",
    "email_confirmed" : true,
    "welcome_email_id" : "1570719729154-1605874083508-1577566183508",
    "roles" : {
        "admin" : true,
        "we_app_user" : false,
        "super_admin" : false,
        "pp_vip" : [ 
            "6"
        ],
        "portir" : [ 
            "14", 
            "15"
        ],
        "pp_owner" : [ 
            "16"
        ]
    },
    "user_number" : 777,
    "__v" : 0,
    "obrtnik" : true,
    "pdv" : true,
    "park_places_owner" : [],
    "new_pass_email_confirmed" : true,
    "new_pass_email_id" : "777-1581315611207-8815883502441",
    "new_pass_request" : null,
    "r1" : true,
    "visited_pp" : [ 
        "3", 
        "4", 
        "6", 
        "14", 
        "15", 
        "5", 
        "16"
    ]
}



// function run_fiskal(user_product, racun_br, user) {
  
function run_fiskal(user_product, racun_br, arg_user) {

var clean_product = convert_mongoose_object_to_js_object(user_product);
if ( clean_product.__v ) delete clean_product.__v;

/*  
var racun_br = 3;
var clean_product = {
    "_id" : "5def6cc3b3c8f43d239d65e0",
    "sifra" : "1257",
    "pp_sifra" : "5",
    "code" : "10009-pretplata-2-5-1575971956880",
    "amount" : 720,
    "desc" : "CARD-MASTERCARD",
    "user_number" : 10009,
    "is_free" : false,
    "price" : 720,
    "type" : "monthly",
    "valid_from" : 1575972035349.0,
    "valid_to" : 1581116400000.0,
    "balance" : 720,
};
*/  

  

  var racun_id = uuidv1(); 

  var unix_time = Date.now();
  var time_now = new Date(unix_time);

  var date_and_time_slanja = generiraj_fiskal_time();
  
  // ovo je vrijeme koje je zapisano u user product !!!!
  var date_and_time_izdavanja = generiraj_fiskal_time(clean_product.buy_time);

  var oib = '77372456863';

  var u_sustavu_pdv = true;

  var oznaka_sljednoti = 'P';
  
  // ako je prije bila neka greška onda će već postojati račun
  // ako nema je ovo novi račun onda će useti uuid koji je kreiran iznad
  var br_racuna = clean_product.racun_br || ( racun_br + '' );

  var br_prostora = '1';
  var br_kase = '1';

  var porezna_stopa = '25.00';
  
  var osnovica = clean_product.price / 1.25; 
  osnovica = zaokruzi(osnovica, 2) + ''; 
  var znamenke_osn = osnovica.split('.')[0];
  var decimale_osn = osnovica.split('.')[1];
  if ( !decimale_osn ) decimale_osn = '00';
  if ( decimale_osn.length < 2 ) decimale_osn = decimale_osn + '0'.repeat(2 - decimale_osn.length);
  osnovica = znamenke_osn + '.' + decimale_osn;
  
  var ukupan_iznos = zaokruzi(clean_product.price, 2) + '';
  var znamenke_uku = ukupan_iznos.split('.')[0];
  var decimale_uku = ukupan_iznos.split('.')[1];
  if ( !decimale_uku ) decimale_uku = '00';
  if ( decimale_uku.length < 2 ) decimale_uku = decimale_uku + '0'.repeat(2 - decimale_uku.length);
  ukupan_iznos = znamenke_uku + '.' + decimale_uku;
  
  var iznos_poreza = Number(ukupan_iznos) - Number(osnovica);
  iznos_poreza = zaokruzi(iznos_poreza, 2) + ''; 
  var znamenke_por = iznos_poreza.split('.')[0];
  var decimale_por = iznos_poreza.split('.')[1];
  if ( !decimale_por ) decimale_por = '00';
  if ( decimale_por.length < 2 ) decimale_por = decimale_por + '0'.repeat(2 - decimale_por.length);
  iznos_poreza = znamenke_por + '.' + decimale_por;
  
  
  var nacin_placanja = 'K';

  var oib_operatera = oib;

  var priprema_za_hash = oib + date_and_time_izdavanja.replace('T', ' ') + br_racuna + br_prostora + br_kase + ukupan_iznos;

  var crypto_sign = node_crypto.createSign('RSA-SHA1');

  crypto_sign.write(priprema_za_hash);
  crypto_sign.end();

  var osobni_klj = fs.readFileSync( __dirname + global.fis_private_key_path );
  var osobni_klj_ascii = osobni_klj.toString('ascii');
  var signature_hex = crypto_sign.sign(osobni_klj, 'hex');

  console.log(signature_hex);

  var zki = md5(signature_hex);
  var naknadna_dostava = false;
  
  
var racun_zahtjev =
    
`<tns:RacunZahtjev Id="racunId" xmlns:tns="http://www.apis-it.hr/fin/2012/types/f73" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.apis-it.hr/fin/2012/types/f73 ../schema/FiskalizacijaSchema.xsd">
<tns:Zaglavlje>
<tns:IdPoruke>${racun_id}</tns:IdPoruke>
<tns:DatumVrijeme>${date_and_time_slanja}</tns:DatumVrijeme>
</tns:Zaglavlje>
<tns:Racun>
<tns:Oib>${oib}</tns:Oib>
<tns:USustPdv>${u_sustavu_pdv}</tns:USustPdv>
<tns:DatVrijeme>${date_and_time_izdavanja}</tns:DatVrijeme>
<tns:OznSlijed>P</tns:OznSlijed>
<tns:BrRac>
<tns:BrOznRac>${br_racuna}</tns:BrOznRac>
<tns:OznPosPr>${br_prostora}</tns:OznPosPr>
<tns:OznNapUr>${br_kase}</tns:OznNapUr>
</tns:BrRac>
<tns:Pdv>
<tns:Porez>
<tns:Stopa>${porezna_stopa}</tns:Stopa>
<tns:Osnovica>${osnovica}</tns:Osnovica>
<tns:Iznos>${iznos_poreza}</tns:Iznos>
</tns:Porez>
</tns:Pdv>
<tns:IznosUkupno>${ukupan_iznos}</tns:IznosUkupno>
<tns:NacinPlac>K</tns:NacinPlac>
<tns:OibOper>${oib_operatera}</tns:OibOper>
<tns:ZastKod>${zki}</tns:ZastKod>
<tns:NakDost>${naknadna_dostava}</tns:NakDost>
</tns:Racun>
</tns:RacunZahtjev>`;    


var zahtjev_dom = (new xmldom.DOMParser()).parseFromString(racun_zahtjev);
 
var canonicaliser = c14n.createCanonicaliser("http://www.w3.org/2001/10/xml-exc-c14n#");
 
console.log("canonicalising with algorithm: " + canonicaliser.name());
console.log("-----------------");
console.log("INPUT");
console.log("-----------------");
console.log(racun_zahtjev);
console.log("-----------------");

  
function run_canon_for_zahtjev(zahtjev_dom) { 
  
  return function(err, xml_canon)  {
  
    if (err) {
      return console.warn(err.stack);
    };

    console.log("RESULT");
    console.log("");
    console.log(xml_canon);
    
    console.log(zahtjev_dom);

    var node_crypto_hash = node_crypto.createHash('SHA1');
    var xml_hash_binary = node_crypto_hash.update(xml_canon); // .replace(/(\r\n|\n|\r)/gm, "")
    var xml_base64 = xml_hash_binary.digest('base64'); 

    
var signature_xml_string =
`<Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
<SignedInfo>
<CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />
<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />
<Reference URI="#racunId">
<Transforms>
<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />
<Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />
</Transforms>
<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />
<DigestValue>${xml_base64}</DigestValue>
</Reference>
</SignedInfo>
</Signature>`;     


    // pretvori signature string u xml
    var signature_xml = (new xmldom.DOMParser()).parseFromString(signature_xml_string);
    
    // get dom element od signature xml-a
    var signature_xml_dom_element = signature_xml.getElementsByTagName('Signature').item(0);
    
    // appendaj signature element u xml od zahtjeva
    zahtjev_dom.documentElement.appendChild(signature_xml_dom_element);
    
    
    var signed_info_dom = zahtjev_dom.getElementsByTagName('SignedInfo').item(0);
    
    function run_canon_sign_info(zahtjev_dom) {

      return function(err, signed_info_canon) {
        if (err) {
          return console.warn(err.stack);
        };
        
        console.log(zahtjev_dom);

        var crypto_za_signature = node_crypto.createSign('RSA-SHA1');

        crypto_za_signature.write( signed_info_canon ); // .replace(/(\r\n|\n|\r)/gm, "")
        crypto_za_signature.end();

        var osobni_klj = fs.readFileSync( __dirname + global.fis_private_key_path );
        var osobni_klj_ascii = osobni_klj.toString('ascii');
        var signature_value_base64 = crypto_za_signature.sign(osobni_klj_ascii, 'base64'); 
        // kad za testitanje želim namjerno kreirati grešku dodam: + 'NAMJERNO_SAM_NAPRAVIO_GREŠKU';

        var signature_value_xml = (new xmldom.DOMParser()).parseFromString(`<SignatureValue>${signature_value_base64}</SignatureValue>`);
        var signature_value_dom_element = signature_value_xml.getElementsByTagName('SignatureValue').item(0);

        var signature_elem = zahtjev_dom.getElementsByTagName('Signature').item(0);
        signature_elem.appendChild(signature_value_dom_element);


var key_info_string =
`<KeyInfo>
<X509Data>
<X509Certificate>${global.fiskal_cert}</X509Certificate>
<X509IssuerSerial>
<X509IssuerName>C=HR,O=Financijska agencija,CN=Fina Demo CA 2014</X509IssuerName>
<X509SerialNumber>194010991629139613675178939477502815821</X509SerialNumber>
</X509IssuerSerial>
</X509Data>
</KeyInfo>`;
        
        var key_info_xml = (new xmldom.DOMParser()).parseFromString(key_info_string);
        var key_info_dom_element = key_info_xml.getElementsByTagName('KeyInfo').item(0);
        
        var signature_elem = zahtjev_dom.getElementsByTagName('Signature').item(0);
        signature_elem.appendChild(key_info_dom_element);
        
        console.log(signature_value_base64);

        var pretvarac_u_string = new xmldom.XMLSerializer();
        
        var zahtjev_s_potpisom_string = pretvarac_u_string.serializeToString(zahtjev_dom);
        
var racun_s_potpisom = 
`<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
<soapenv:Body>
${zahtjev_s_potpisom_string}
</soapenv:Body>
</soapenv:Envelope>`;

        var racun_options = {

          url: global.fiskal_url,
          headers: {
            'Content-Type': 'text/xml;charset=UTF-8',
            'soapAction': "http://e-porezna.porezna-uprava.hr/fiskalizacija/2012/services/FiskalizacijaService/racuni"
          },
          body : racun_s_potpisom,
          agentOptions: {
            pfx: fs.readFileSync(__dirname + global.fis_cer_p12_path ),
            passphrase: 'ToNi3007'
          }
        };

        request.post(racun_options, (error, response, body) => {
          console.log(error);
          console.log(response);
          console.log(body);


          if ( error ) {
            
            
          };
          
          var conv_options = {
              attributeNamePrefix : "_",
              ignoreNameSpace : true,
              trimValues: true,
          };

          var body_xml_to_json = x2jconverter.parse(body, conv_options);

          console.log(body_xml_to_json);

          console.log(JSON.stringify(body_xml_to_json, undefined, 2));

          
            /*
            
            ovo su podaci vezani za fiskalizaciju 
            
            zki: { type: String, required: false },
            jir: { type: String, required: false },
            racun_br: { type: String, required: false },
            fis_id: { type: String, required: false },
            fis_greska: { type: String, required: false },
            fis_time: { type: Number, required: false }
          
            */
          
          var greske = body_xml_to_json.Envelope.Body.RacunOdgovor.Greske;
          
          if ( greske ) {
            
            // GREŠKA U FISKALIZACIJI !!!!!
            // GREŠKA U FISKALIZACIJI !!!!!
            // GREŠKA U FISKALIZACIJI !!!!!
            // GREŠKA U FISKALIZACIJI !!!!!
            
            var greske_string =
`

${new Date()}
${ JSON.stringify(greske) }

`;

            // dodaj novu grešku staroj grešci ako već postoji u objektu
            greske_string = ( clean_product.fis_greska || '' ) + JSON.stringify(greske_string);
            
            var fis_time = Date.now();
            
            CustomerProduct.findOneAndUpdate(
              { _id: clean_product._id },
              {
                zki: (zki || null), 
                jir: null, 
                racun_br: (racun_br || null),
                fis_id: (racun_id || null),
                fis_greska: (greske_string || null),
                fis_time: null
              },
              { new: true }, function(err, customer_product) {
              
                if (err) {
                  console.error({ success: false, msg: "popup_update_products_error", err: err});
                  return;
                }
                else {
                  var clean_customer_product = convert_mongoose_object_to_js_object(customer_product);
                  
                  console.log({ 
                    success: true,
                    msg: "Customer product updated SA FISKAL GREŠKOM !!!!!!",
                    customer_product: clean_customer_product
                  });
                  
                  send_racun_by_mail(clean_customer_product, arg_user);
                };  
                
            });
            
          } 
          else {

            
            /* AKO NEMA GREŠAKA U FISKALIZACIJI !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  */
            /* AKO NEMA GREŠAKA U FISKALIZACIJI !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  */
            /* AKO NEMA GREŠAKA U FISKALIZACIJI !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  */
            /* AKO NEMA GREŠAKA U FISKALIZACIJI !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  */
            
            
            var jir = body_xml_to_json.Envelope.Body.RacunOdgovor.Jir;
            
            
            if ( jir ) {
              
              var fis_time = Date.now();
              
              CustomerProduct.findOneAndUpdate(
                { _id: clean_product._id },
                { 
                  zki: (zki || null),
                  jir: (jir || null),
                  racun_br: (racun_br || null),
                  fis_id: (racun_id || null),
                  fis_greska: null,
                  fis_time: fis_time
                },
                { new: true }, function(err, customer_product) {
                  
                if (err) {
                  console.error({ success: false, msg: "popup_update_products_error", err: err});
                  return;
                }
                else {
                  var clean_customer_product = convert_mongoose_object_to_js_object(customer_product);
                  
                  console.log({ 
                    success: true,
                    msg: "Customer product updated sa fiskalizacijom !",
                    customer_product: clean_customer_product
                  });
                  
                  send_racun_by_mail(clean_customer_product, arg_user);
                };
                
              });
              
            }; // kraj ako postoji jir
            
          }; // kraj od else tj ako nema greške
          
        }); // kraj request ajax poziva ...i odgovora

      }; // kraj callback funkcije za canon sign info
      
    }; // kraj wrapper funkcije canon sign info   
    
    canonicaliser.canonicalise(signed_info_dom, run_canon_sign_info(zahtjev_dom) ); // kraj canonizacije za signed info  

  };  // kraj call back funcije za canonizaciju zahtjeva

}; // glavna funkcija za canon zahtjeva
  
canonicaliser.canonicalise( zahtjev_dom.documentElement, run_canon_for_zahtjev(zahtjev_dom) );  // kraj canonizacije za racun zahtjev

}; // KRAJ run fiskal
exports.run_fiskal = run_fiskal;

global.run_fiskal = run_fiskal;



var odgovor =
    
{
  "Envelope": {
    "Body": {
      "RacunOdgovor": {
        "Zaglavlje": {
          "IdPoruke": "9b876410-1de1-11ea-b040-01ed7e6cb012",
          "DatumVrijeme": "13.12.2019T20:49:00"
        },
        "Jir": "b8e451f6-9fe6-4a3e-9411-18d2c10b9ae6",
        "Signature": {
          "SignedInfo": {
            "CanonicalizationMethod": "",
            "SignatureMethod": "",
            "Reference": {
              "Transforms": {
                "Transform": [
                  "",
                  ""
                ]
              },
              "DigestMethod": "",
              "DigestValue": "2UYicfpWgxX1QA7jhRo4FrSsS/I="
            }
          },
          "SignatureValue": "hcm/WTMEnMiXtJpQEnzSm5HwzYj6RFlkNX55hXYzG5oyriaRh/3BUa0CLJOqXxFTIQIUadoQ0OWVW5WFlE2Veg/y37dyOOusWl7JVqX+0hvjEwJgcuDPwNzxmg/P72b65q3jyAY9V33kGoMtwz9jEVaCz8AQTw9baMGFLeP2BWRLhCt8ABAvHmI7HkSC1n23Atp3d9D5ZhoMysSFp5jmg1QYbbet4GsfabAP6J1THAqC1qp8C67oRE3LJUVUz1eHa1oBSzSqV6XSVwU8iAq6ZBQsrmbhZhQIGAsiWr3EM3rk+OgosOdW3iaX9J7Qq/OOrgjiekSV/XLQCs6PuNcfmQ==",
          "KeyInfo": {
            "X509Data": {
              "X509Certificate": "MIIHKjCCBRKgAwIBAgIQbfN2Z+WT+UoAAAAAUyv7UjANBgkqhkiG9w0BAQsFADBIMQswCQYDVQQGEwJIUjEdMBsGA1UEChMURmluYW5jaWpza2EgYWdlbmNpamExGjAYBgNVBAMTEUZpbmEgRGVtbyBDQSAyMDE0MB4XDTE4MDcxNjA4MDYzNVoXDTIwMDcxNjA4MDYzNVowZzELMAkGA1UEBhMCSFIxFzAVBgNVBAoTDkFQSVMgSVQgRC5PLk8uMRYwFAYDVQRhEw1IUjAyOTk0NjUwMTk5MQ8wDQYDVQQHEwZaQUdSRUIxFjAUBgNVBAMTDWZpc2thbGNpc3Rlc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDIdoWGKtE7qA4cK5jBVgWRSvgr9M1F6GJG+AW6HTQG0EurK5mmbmn6Dam0pSoItzxj1B2rcInPeYY6B3iYo4D9M1IWp7entRhqD2DL4SY0ytLMKVdAuuRPwCQ1fBu5N2OOYFVnQ+2htdhFcMBN/E9MnaA85dGket1r+wcYcdMp1xk3/DCghZxNYP7Hq/iQaNRMnSLsOJILHVerB3/VNQSku494MqL14mOx7XIXmguRlolQFxwyhMSPWPKsJ1aI8DYGaZJM6YrAEkS904DU++UlAAqCpKetgKSaESaWrYESJIz6Hd/I9f2JY00D36wNs1mBCSh4UKtWie2IAtY1sImBAgMBAAGjggLvMIIC6zAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwQGCCsGAQUFBwMCMIGsBgNVHSAEgaQwgaEwgZQGCSt8iFAFIA8EAjCBhjBBBggrBgEFBQcCARY1aHR0cDovL2RlbW8tcGtpLmZpbmEuaHIvY3BzL2Nwc25xY2RlbW8yMDE0djItMC1oci5wZGYwQQYIKwYBBQUHAgEWNWh0dHA6Ly9kZW1vLXBraS5maW5hLmhyL2Nwcy9jcHNucWNkZW1vMjAxNHYyLTAtZW4ucGRmMAgGBgQAj3oBAjB9BggrBgEFBQcBAQRxMG8wKAYIKwYBBQUHMAGGHGh0dHA6Ly9kZW1vMjAxNC1vY3NwLmZpbmEuaHIwQwYIKwYBBQUHMAKGN2h0dHA6Ly9kZW1vLXBraS5maW5hLmhyL2NlcnRpZmlrYXRpL2RlbW8yMDE0X3N1Yl9jYS5jZXIwJQYDVR0RBB4wHIEaYnJhbmltaXIuYmxhemljQGFwaXMtaXQuaHIwggEYBgNVHR8EggEPMIIBCzCBpqCBo6CBoIYoaHR0cDovL2RlbW8tcGtpLmZpbmEuaHIvY3JsL2RlbW8yMDE0LmNybIZ0bGRhcDovL2RlbW8tbGRhcC5maW5hLmhyL2NuPUZpbmElMjBEZW1vJTIwQ0ElMjAyMDE0LG89RmluYW5jaWpza2ElMjBhZ2VuY2lqYSxjPUhSP2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3QlM0JiaW5hcnkwYKBeoFykWjBYMQswCQYDVQQGEwJIUjEdMBsGA1UEChMURmluYW5jaWpza2EgYWdlbmNpamExGjAYBgNVBAMTEUZpbmEgRGVtbyBDQSAyMDE0MQ4wDAYDVQQDEwVDUkwxMDAfBgNVHSMEGDAWgBQ7hFoU9cU84Ug7XdEnNXvVZbwOKjAdBgNVHQ4EFgQUlB667bz8TJ7FE8pguP1V5Ax3O4QwCQYDVR0TBAIwADANBgkqhkiG9w0BAQsFAAOCAgEAFy+UmjGinskZV2GwK0OXPoK8KvfUVNUImvbK9EwOlxVuePb+8z+dU9CZYgPyjbgsCIE8CPW4quhmkgcUKiPuFKa+TW4i/hq9x9j9RiEvbCfX3aG+NbA235kM1ODGIRLObx7urSuX9/kO1d6GCWMke/tP9C1Cl5VguvjS2LmAEkulK4c8OLnVrLiMKDgSS4OfsnZEME5Uk80ibTo9fEZa6kS7R3RAfztLjaSXqeqvmBJi8hoHwrnAnyVW89Nf+hzcNj0NojBvhEoma91TsC9ED1C7w43SsuPQSwVCT9fVIgLMDMIXldhEDiXA04RoYHEs2ll80WsS3jO11qAj6zBOjjs2A9Vte3+Y4hCjSgHELOwLECZhBsmj7rF9BiDwNb9h4xG/+3xyWRUKlIcwHBtdenrUNqseq3pIdNYIujcZcbMK9zcJBvb/cQ17vzkCV7VnP0N4pdIPR+kYJfixtjqqNxOxBOvH0Vpx4xWDNY/TXvaqdN8SFaEeEc/ly+FUkNgJxL2ANFClonQDKMy0V+owMajH/J9AMWPWUzf8k9c0apri/6uNLtXGwXfraSNvNkEf+5aPQkEmgMpFzThwaUp3xdy+Sj0jBUW0XpIKKJX5wOCVbp3x0H0ci5wTbCeidpLaHo7hlheT2vkzlR0Fc6BaydbQyxk5LvK0xTXS9gTsRI8=",
              "X509IssuerSerial": {
                "X509IssuerName": "CN=Fina Demo CA 2014, O=Financijska agencija, C=HR",
                "X509SerialNumber": 1.4614998123306471e+38
              }
            }
          }
        }
      }
    }
  }
};


var greska = 
    
    
{
  "Envelope": {
    "Body": {
      "RacunOdgovor": {
        "Zaglavlje": {
          "IdPoruke": "24fee380-1de2-11ea-8270-fb280ec3da35",
          "DatumVrijeme": "13.12.2019T20:52:51"
        },
        "Greske": {
          "Greska": {
            "SifraGreske": "s001",
            "PorukaGreske": "Poruka nije u skladu s XML shemom :  cvc-simple-type 1: element {http://www.w3.org/2000/09/xmldsig#}SignatureValue value 'TSDC5kTgX6slUJCdzeto1ey17ZD7bNaaXbxBOI+bFVsyKDcZcDHNro/fzSofuN7vBqcqNne8x6SotF/djUIT65IlEpMyHMaA0IFMTgtii7Hqhf7jR5Wv5Eci8JF+PFPGcOi52GF9jOxK7Oir4Q2sGZN5xM5/fisZCId9KXMtkv6Mjqwn4tc+W4Ur+Y/8TbcoGBjHeToSQe6/q4CD6NhQa48DvMKKBPVfKMQ2QiPX9hlUq5jrKJrH+Adjp++gflqualstjPe2JUnhMvBNx/Y4i0qHhndIItYFOZLl/CGeaHGJbwC63dfeMvKbqbZECTGysFua/vlwKEWLGkOl9MvSWg==555' is not a valid inst"
          }
        },
        "Signature": {
          "SignedInfo": {
            "CanonicalizationMethod": "",
            "SignatureMethod": "",
            "Reference": {
              "Transforms": {
                "Transform": [
                  "",
                  ""
                ]
              },
              "DigestMethod": "",
              "DigestValue": "jlFFePCRLzhmRSaKMGyJzg+2n1U="
            }
          },
          "SignatureValue": "a/e2TCFTy70waUp0xZ1N89JUBx1FAfZCPEUBU+n3fRgw3MCYAc5geyUzkPWlMQI6NK6I8vgMo5ZaCOjXOndewLlO5hqNy8jp8H10ALZfGWbtMqbVpdoGKPPNpEKfe8V6UCh9Y/ptYTH+Wb6oQhXihxrRjRcwxzUImr+ce9rh2pdjxO8Bg6rk6yMRCUV2Of3QKc3dPqxxCLt7J7CiV9TUtijE9PrOnnb9vSg8Iaqvjt9ay9Xiyu1VPW67pHJT5ezggYHSUbtFQW+NvPKbhdt9WzFYVb5ryc13Wi4UPj94r3c7XhbMPHDN0Vsh/X6dIIYWej+wsBfd89iz7//g2GoPMg==",
          "KeyInfo": {
            "X509Data": {
              "X509Certificate": "MIIHKjCCBRKgAwIBAgIQbfN2Z+WT+UoAAAAAUyv7UjANBgkqhkiG9w0BAQsFADBIMQswCQYDVQQGEwJIUjEdMBsGA1UEChMURmluYW5jaWpza2EgYWdlbmNpamExGjAYBgNVBAMTEUZpbmEgRGVtbyBDQSAyMDE0MB4XDTE4MDcxNjA4MDYzNVoXDTIwMDcxNjA4MDYzNVowZzELMAkGA1UEBhMCSFIxFzAVBgNVBAoTDkFQSVMgSVQgRC5PLk8uMRYwFAYDVQRhEw1IUjAyOTk0NjUwMTk5MQ8wDQYDVQQHEwZaQUdSRUIxFjAUBgNVBAMTDWZpc2thbGNpc3Rlc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDIdoWGKtE7qA4cK5jBVgWRSvgr9M1F6GJG+AW6HTQG0EurK5mmbmn6Dam0pSoItzxj1B2rcInPeYY6B3iYo4D9M1IWp7entRhqD2DL4SY0ytLMKVdAuuRPwCQ1fBu5N2OOYFVnQ+2htdhFcMBN/E9MnaA85dGket1r+wcYcdMp1xk3/DCghZxNYP7Hq/iQaNRMnSLsOJILHVerB3/VNQSku494MqL14mOx7XIXmguRlolQFxwyhMSPWPKsJ1aI8DYGaZJM6YrAEkS904DU++UlAAqCpKetgKSaESaWrYESJIz6Hd/I9f2JY00D36wNs1mBCSh4UKtWie2IAtY1sImBAgMBAAGjggLvMIIC6zAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwQGCCsGAQUFBwMCMIGsBgNVHSAEgaQwgaEwgZQGCSt8iFAFIA8EAjCBhjBBBggrBgEFBQcCARY1aHR0cDovL2RlbW8tcGtpLmZpbmEuaHIvY3BzL2Nwc25xY2RlbW8yMDE0djItMC1oci5wZGYwQQYIKwYBBQUHAgEWNWh0dHA6Ly9kZW1vLXBraS5maW5hLmhyL2Nwcy9jcHNucWNkZW1vMjAxNHYyLTAtZW4ucGRmMAgGBgQAj3oBAjB9BggrBgEFBQcBAQRxMG8wKAYIKwYBBQUHMAGGHGh0dHA6Ly9kZW1vMjAxNC1vY3NwLmZpbmEuaHIwQwYIKwYBBQUHMAKGN2h0dHA6Ly9kZW1vLXBraS5maW5hLmhyL2NlcnRpZmlrYXRpL2RlbW8yMDE0X3N1Yl9jYS5jZXIwJQYDVR0RBB4wHIEaYnJhbmltaXIuYmxhemljQGFwaXMtaXQuaHIwggEYBgNVHR8EggEPMIIBCzCBpqCBo6CBoIYoaHR0cDovL2RlbW8tcGtpLmZpbmEuaHIvY3JsL2RlbW8yMDE0LmNybIZ0bGRhcDovL2RlbW8tbGRhcC5maW5hLmhyL2NuPUZpbmElMjBEZW1vJTIwQ0ElMjAyMDE0LG89RmluYW5jaWpza2ElMjBhZ2VuY2lqYSxjPUhSP2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3QlM0JiaW5hcnkwYKBeoFykWjBYMQswCQYDVQQGEwJIUjEdMBsGA1UEChMURmluYW5jaWpza2EgYWdlbmNpamExGjAYBgNVBAMTEUZpbmEgRGVtbyBDQSAyMDE0MQ4wDAYDVQQDEwVDUkwxMDAfBgNVHSMEGDAWgBQ7hFoU9cU84Ug7XdEnNXvVZbwOKjAdBgNVHQ4EFgQUlB667bz8TJ7FE8pguP1V5Ax3O4QwCQYDVR0TBAIwADANBgkqhkiG9w0BAQsFAAOCAgEAFy+UmjGinskZV2GwK0OXPoK8KvfUVNUImvbK9EwOlxVuePb+8z+dU9CZYgPyjbgsCIE8CPW4quhmkgcUKiPuFKa+TW4i/hq9x9j9RiEvbCfX3aG+NbA235kM1ODGIRLObx7urSuX9/kO1d6GCWMke/tP9C1Cl5VguvjS2LmAEkulK4c8OLnVrLiMKDgSS4OfsnZEME5Uk80ibTo9fEZa6kS7R3RAfztLjaSXqeqvmBJi8hoHwrnAnyVW89Nf+hzcNj0NojBvhEoma91TsC9ED1C7w43SsuPQSwVCT9fVIgLMDMIXldhEDiXA04RoYHEs2ll80WsS3jO11qAj6zBOjjs2A9Vte3+Y4hCjSgHELOwLECZhBsmj7rF9BiDwNb9h4xG/+3xyWRUKlIcwHBtdenrUNqseq3pIdNYIujcZcbMK9zcJBvb/cQ17vzkCV7VnP0N4pdIPR+kYJfixtjqqNxOxBOvH0Vpx4xWDNY/TXvaqdN8SFaEeEc/ly+FUkNgJxL2ANFClonQDKMy0V+owMajH/J9AMWPWUzf8k9c0apri/6uNLtXGwXfraSNvNkEf+5aPQkEmgMpFzThwaUp3xdy+Sj0jBUW0XpIKKJX5wOCVbp3x0H0ci5wTbCeidpLaHo7hlheT2vkzlR0Fc6BaydbQyxk5LvK0xTXS9gTsRI8=",
              "X509IssuerSerial": {
                "X509IssuerName": "CN=Fina Demo CA 2014, O=Financijska agencija, C=HR",
                "X509SerialNumber": 1.4614998123306471e+38
              }
            }
          }
        }
      }
    }
  }
}    




  var newsletter_html = 
      
`
<div id="${Math.random().toString(16).substring(2,10)}" 
style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #fff;
            position: relative;
            margin: 0 auto;
            padding: 0;
            max-width: 1000px;">

<!--
  div id="${Math.random().toString(16).substring(2,10)}" 
style="  font-size: 50px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    color: #fff;
                    font-weight: 700;
                    line-height: 60px;
                    letter-spacing: normal;
                    text-align: left;
                  position: absolute;
                  width: 50%;
                  height: auto;
                  top: 30px;
                  right: 0;">
        
       Nova WePark Aplikacija!

      </div> 
-->        
  
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_bg.jpg" style="width: 100%; height: auto;" />
  
  
  <!--
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_download_app.jpg" style="width: 100%; height: auto; max-width: 400px;" />
  
  
  <div style="width: 100%; height: auto; clear: both; text-align: center; margin: 10px 0 30px;">
    
    <a href="https://play.google.com/store/apps/details?id=com.wePark2" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/google_play.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    <a href="https://apps.apple.com/us/app/wepark-park-with-smile/id1314272266?ls=1" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/app_store.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    
  </div>
  
  
 --> 
    <div id="${Math.random().toString(16).substring(2,10)}" 
style="  height: auto;
                  text-align: center;
                  font-family: Arial, Helvetica, sans-serif;
                  padding: 0;
                  position: relative;
                  width: 100%;">

      <div id="${Math.random().toString(16).substring(2,10)}"  style="    
        font-size: 13px;
        margin-top: 40px;
        margin-bottom: 20px;
        color: #454545;
        font-weight: 400;
        text-align: left;
        line-height: 26px;
        max-width: 500px;
        clear: both;
        width: 90%;
        margin: 0 auto;
        padding: 10px;
        height: auto;
        background-color: #fff;
        border-radius: 10px;
        border: 1px solid #e8e6e6;
        letter-spacing: 0.04rem;">


<!--<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> -->


        <div id="${Math.random().toString(16).substring(2,10)}" style="padding: 10px; float: left; clear: both; background-color: #e64531; border-radius: 8px;">
          
          <img src="https://wepark.circulum.it:9000/img/001_nl_warning_sign.jpg" 
       style="width: 100%; height: auto; max-width: 400px; clear: both; display: block; float: none; margin: 0 auto;" />
  
          <h2 id="${Math.random().toString(16).substring(2,10)}" style="color: #6f6f6f; text-align: center; color: #fff; margin-top: 0;">29.01.2020. u srijedu<br>Aplikacija WePark će se zamijeniti s novom verzijom i postojeća više neće raditi!</h2>
          
          <p style="padding-left: 0;
                    text-align: center;
                    padding-right: 0;
                    line-height: 1.6; 
                    color: #fff;">
            
            Stanje na vašem računu ostaje isto!<br>
            Kada nova aplikacija počne s radom, moći ćete je preuzeti na Google Play i Apple Store servisima.<br>
            Dan prije (28.01.2020.) ćete dobiti newsletter s linkom za preuzimanje!<br>
            <br>
            Nova verzija aplikacije će imati niz značajnih tehnoloških izmjena u odnosu na postojeću aplikaciju i stoga smo odlučili napraviti potpuni prijelaz bez podrške za staru aplikaciju.
            
            <br>
            Ukoliko imate bilo kakvih pitanja molimo vas kontaktirajte nas na:<br><br>
            <a href="mailto:toni.kutlic@wepark.eu" target="_blank" style="color: #fff;">toni.kutlic@wepark.eu</a>
            <br>
            ili
            <br>
            00385 92 3133 015
            
          </p>
        
          <br>
          
          <p style="padding-left: 0;
                    text-align: center;
                    padding-right: 0;
                    line-height: 1.6; 
                    color: #fff;">

            <br>
            Ukoliko imate bilo kakvih pitanja molimo vas kontaktirajte nas na:<br><br>
            <a href="mailto:toni.kutlic@wepark.eu" target="_blank">toni.kutlic@wepark.eu</a>
            <br>
            ili
            <br>
            00385 92 3133 015
            
          </p>          
          
        </div>        
        
        
        <br>
        <br>
        
        
        
        
        <div style="padding: 10px; float: left; width: 100%; text-align: center; font-size: 20px; font-weight: 700; margin-top: 30px;">
          Predstavljamo Vam novu verziju aplikacije WePark.<br>
          <br>
          
        </div>

        
        <div style="padding: 10px; float: left; clear: both;">
          <img src="https://wepark.circulum.it:9000/img/001_nl_credit_card.jpg" style=" width: 100px;
                                                        height: auto;
                                                        display: block;
                                                        margin: 0 auto;" />
          <h2 style="color: #6f6f6f; text-align: center;">Plaćanje kreditnom karticom.</h2>
          <p style="padding-left: 0;
                    text-align: justify;
                    padding-right: 0;
                    line-height: 1.6;">
            Od sada parkiralište možete platiti svojom kreditnom karticom. U suradnji sa WSPAY payment gateway-om pružamo siguran i brz način kartičnog plaćanja. Dodatno imamo opciju korištenja token tehnologije kako bi izbjegli uzastopno upisivanje osobnih podataka. Uz pomoć tokena sada možete jednim klikom gumba izvršiti plaćanje direktno iz aplikacije!
          </p>
        
        </div>
        
        <div style="padding: 10px; float: left; clear: both;">
          
          <img src="https://wepark.circulum.it:9000/img/001_nl_monthly_payment.jpg" style=" width: 100px;
                                                            height: auto;
                                                            display: block;
                                                            margin: 0 auto;" />
          
          
          <h2 style="color: #6f6f6f; text-align: center;">Mogućnost mjesečne pretplate s popustima.</h2>
          <p style="padding-left: 0;
                    text-align: justify;
                    padding-right: 0;
                    line-height: 1.6;">
            Nova aplikacija nudi opciju mjesečne pretplate. U ponudi možete birati od 1 do 3 mjeseca pretplate sa popustima od 10, 15 i 20%.
          </p>
        
        </div>
        
        <div style="padding: 10px; float: left;  clear: both;">
          
          <img src="https://wepark.circulum.it:9000/img/001_nl_speed.jpg" style=" width: 100px;
                                                  height: auto;
                                                  display: block;
                                                  margin: 0 auto;" />

          <h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">Brža prijava i odjava na parkiralištu.</h2>
          <p style="padding-left: 0;
                    text-align: justify;
                    padding-right: 0;
                    line-height: 1.6;">
            Od sada više ne morate nekoliko puta potvrditi ulazak i izlazak na parkiralištu. Značajno pojednostavljenje sučelja osigurava uštedu vašeg dragocjenog vremena! Također, pojednostavili smo gumbe za rezervaciju parkirališta zajedno sa odabirom trajanja rezervacije.
            Na primjer, možete rezervirati parkirno mjesto koliko god sati želite unaprijed. U svakom trenutku možete prekinuti rezervaciju, a biti će Vam naplaćeno samo vrijeme do trenutka prekida. Ukoliko istekne vrijeme koje ste odabrali, aplikacija će automatski prekinuti rezervaciju!
          </p>
        </div>        
        
        
        <div style="padding: 10px; float: left;  clear: both;">
          
          <img src="https://wepark.circulum.it:9000/img/001_nl_modern.jpg" style=" width: 100px;
                                                  height: auto;
                                                  display: block;
                                                  margin: 0 auto;" />
          
    
          <h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">Novi moderni dizajn.</h2>
          <p style="padding-left: 0;
                    text-align: justify;
                    padding-right: 0;
                    line-height: 1.6;">
            
            WePark je dobio novi izgled. Informacije su prezentirane na jasniji i ugodniji način. Mi smo zadovoljni rezultatom i nadamo se da ćete biti i vi :).
          </p>
          
          
        </div>        

        
        <br style="display: block; clear: both;">
        
                
        <br>
        <br>
        
      </div>

      <br style="display: block; clear: both;">
      
    </div>
  
  
  
        <br>
        <br>
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_footer.jpg" style="width: 100%; height: auto; display: block; clear: both;"/>
  

    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #fff;
                padding: 0;">

      
      
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />



      <div id="${Math.random().toString(16).substring(2,10)}" style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 18px;
        border-top: 1px solid #eaeaea;
        padding-top: 10px;
        background-color: #fff;
        letter-spacing: 0.06rem;">
        

        <b>Odgovorna osoba:</b><br>
        <b>Karlo Starčević (direktor)</b><br><br>
        <b>Wepark smart parking d.o.o.</b><br>
        <b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
        <b>OFFICE:</b>&nbsp;zgrada IGH, Rakušina 1, 10000 Zagreb<br>
        <b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
        <b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
        <b>WEB:</b>&nbsp;www.wepark.eu<br>
        <br>
        <div style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
        Društvo je upisano u sudski registar Trgovačkog suda u Zagrebu pod brojem 081131571. Temeljni kapital u iznosu od 20 000 kn je uplaćen u cijelosti. Račun se vodi kod Privredne Banke Zagreb. Član društva Karlo Starčević.
        </div>
        <br style="display: block; clear: both;">
        
        
      </div>        
      
      <br id="${Math.random().toString(16).substring(2,10)}"  style="display: block; clear: both;">
    </div>  

    
</div>     



`;


global.we_nl_error_emails = [];

global.send_newsletter = function(user_email_adress, email_index) {
  
  
  var transporter = nodemailer.createTransport(global.we_mail_setup);
  
  
  
 var html = 
      
`


    
    
      
<div id="${Math.random().toString(16).substring(2,10)}" 
style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #fff;
            position: relative;
            margin: 0 auto;
            padding: 0;
            max-width: 1000px;">

<!--
  div id="${Math.random().toString(16).substring(2,10)}" 
style="  font-size: 50px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    color: #fff;
                    font-weight: 700;
                    line-height: 60px;
                    letter-spacing: normal;
                    text-align: left;
                  position: absolute;
                  width: 50%;
                  height: auto;
                  top: 30px;
                  right: 0;">
        
       Nova WePark Aplikacija!

      </div> 
-->        

  
  <!--<img src="https://wepark.circulum.it:9000/img/001_nl_bg.jpg" style="width: 100%; height: auto;" />-->
  <img src="https://wepark.circulum.it:9000/img/003_nl/naslovna_var_301.jpg" style="width: 100%; height: auto;" />
  
  
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />

  
  
  
  <div style="float: none;
    width: 100%;
    max-width: 340px;          
    text-align: center;
    font-size: 27px;
    font-weight: 700;
    margin: 0px auto 30px;
    line-height: normal;
    letter-spacing: normal;
    color: #06253f;">
<br>
  
Molimo Vas ažurirajte<br>
WePark aplikaciju
  
<br>


</div>
  <br style="display: block; clear: both;">
   
   
<div style="padding: 10px; float: none; clear: both; background-color: #e64531; border-radius: 8px; max-width: 340px; margin: 0 auto;">
          
          <img src="https://wepark.circulum.it:9000/img/001_nl_warning_sign.jpg" 
       style="width: 100%; height: auto; max-width: 400px; clear: both; display: block; float: none; margin: 0 auto;" />
  
          <h2 style="color: #6f6f6f; text-align: center; color: #fff; margin-top: 0;">BITNA NAPOMENA</h2>
  
          <p style="padding-left: 0;
                    text-align: center;
                    padding-right: 0;
                    line-height: 1.6; 
                    color: #fff;">
            
            U zadnjih par dana dobili smo nekoliko žalbi da aplikacija ne radi kako treba!<br>
            <br>
            Razlog je veliki update koji smo napravili prošli tjedan.
            <br>
            Kako bi osigurali ispravan rad aplikacije molimo Vas da napravite update aplikacije.
            
            
            </p>
  

        
        </div>   
  
<br style="display: block; clear: both;">
            
            <p style="padding-left: 0;
                    text-align: center;
                    padding-right: 0;
                    line-height: 1.6; 
                    color: #06253f;">
  
            
            Ukoliko imate bilo kakvih pitanja molimo vas kontaktirajte nas na:<br><br>
            toni.kutlic@wepark.eu
            <br>
            ili
            <br>
            00385 92 3133 015
            
          </p>
  
  
        <br>
        <br>
   
   
     <br style="display: block; clear: both;">
   
        <br>
        <br>

<div style="padding: 0; float: none; clear: both; width: 100%; max-width: 360px; margin: 0 auto;">
  
<h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #06253f; text-align: center;">
  <img style="width: 30px;
    height: auto;
    margin-right: 3px;
    position: relative;
    top: 8px;"
    src="https://wepark.circulum.it:9000/img/003_nl/small_android_icon.jpg" />
  Android
  </h2>
<div style="padding-left: 0;
          text-align: left;
          padding-right: 0;
          line-height: 1.6;">
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">1</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Posjetite Google Play Store</div>
  </div>
    
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">2</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Kliknite ikonu menija na vrhu stranice i izaberite "Moje aplikacije i igre" (My apps & games)</div>
  </div>
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">3</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Izaberite link Ažuriranje/Nadopuna (Updates)</div>
  </div>
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">4</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Na dnu ekrana se nalazi lista svih aplikacija koje treba ažurirati</div>
  </div>
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">5</div>
    
    <div style="width: calc(100% - 50px); padding-top: 5px;">Ako na toj listi vidite WePark aplikaciju, kliknite na gumb AŽURIRAJ (UPDATE)</div>
  
  </div>  
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">6</div>
    
    <div style="width: calc(100% - 50px); padding-top: 5px;">Ako na toj listi NEMA WePark aplikacije, onda ste već ažurirali aplikaciju i sve je ok.</div>
  
  </div>
  
  
  <br>
  <br>
  
  
  </div>   
  <br>
  <img src="https://wepark.circulum.it:9000/img/003_nl/android_update_page_1.jpg" style="width: 100%; max-width: 262px; height: auto;" />
  <br>
  <img src="https://wepark.circulum.it:9000/img/003_nl/android_update_page_2.jpg" style="width: 100%; max-width: 262px; height: auto;" />
</div>        
  
<br style="display: block; clear: both;">  
  
  

<div style="padding: 0; float: none; clear: both; width: 100%; max-width: 360px; margin: 0 auto;">
  
<h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #06253f; text-align: center;">
  <img style="width: 30px;
    height: auto;
    margin-right: 3px;
    position: relative;
    top: 2px;"
    src="https://wepark.circulum.it:9000/img/003_nl/small_apple_icon.jpg" />
  iPhone
</h2>
  
<div style="padding-left: 0;
          text-align: left;
          padding-right: 0;
          line-height: 1.6;">
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">1</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Posjetite App Store</div>
  </div>
    
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">2</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Kliknite ikonu vašeg profila u gornjem desnom kutu</div>
  </div>
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">3</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Na dnu ekrana će biti lista svih aplikacija koje se trebaju ažurirati</div>
  </div>
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">4</div>
    <div style="width: calc(100% - 50px); padding-top: 5px;">Ako na toj listi vidite WePark aplikaciju, kliknite na gumb AŽURIRAJ (UPDATE) </div>
  </div>
  
  <div style="display: flex; align-items: flex-start; margin-bottom: 10px;">
    <div style="width: 40px; height: 40px; border-radius: 20px; font-size: 20px; margin-right: 10px; float: left; display: block; background-color: #e64531; color: #fff; line-height: 40px; text-align: center;">5</div>
    
    <div style="width: calc(100% - 50px); padding-top: 5px;">Ako na toj listi NEMA WePark aplikacije, onda ste već ažurirali aplikaciju i sve je ok.</div>
  
  </div>  
  
    
  <br>
  <br>
    
  </div>    
  
  <br>
  <img src="https://wepark.circulum.it:9000/img/003_nl/ios_update_page_1.jpg" style="width: 100%; max-width: 262px; height: auto;" />
  <br>
  <img src="https://wepark.circulum.it:9000/img/003_nl/ios_update_page_2.jpg" style="width: 100%; max-width: 262px; height: auto;" />
  
</div>        
  
<br style="display: block; clear: both;">  
  
    
  
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_download_app.jpg" style="width: 100%; height: auto; max-width: 400px;" />
  
  
  <div style="width: 100%; height: auto; clear: both; text-align: center; margin: 10px 0 30px;">
    
    <a href="https://play.google.com/store/apps/details?id=com.wePark" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/google_play.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    <a href="https://apps.apple.com/us/app/wepark-park-with-smile/id1314272266?ls=1" target="_blank" style="text-decoration: none; outline: none;">
    <img src="https://wepark.circulum.it:9000/img/app_store.jpg" style="width: 200px; height: auto; margin: 0 10px;" />
    </a>
    
  </div>
     
<br style="display: block; clear: both;">    
  
  
<div style="padding: 0; float: none; clear: both; width: 100%; max-width: 400px; margin: 0 auto;">
  
<h2 id="${Math.random().toString(16).substring(2,10)}"  style="color: #6f6f6f; text-align: center;">Kontakt</h2>
<p style="padding-left: 0;
          text-align: center;
          padding-right: 0;
          line-height: 1.6;">

  Ako imate bilo koje pitanje molimo obratite se na<br>
  +385 92 3133 015<br>
  ili na email<br>
  toni.kutlic@wepark.eu
</p>
</div>        


<br style="display: block; clear: both;">
  
  
        <br>
        <br>
  
  <img src="https://wepark.circulum.it:9000/img/001_nl_footer.jpg" style="width: 100%; height: auto; display: block; clear: both;"/>
  

    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #fff;
                padding: 0;">

      
      
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />



      <div id="${Math.random().toString(16).substring(2,10)}" style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 18px;
        border-top: 1px solid #eaeaea;
        padding-top: 10px;
        background-color: #fff;
        letter-spacing: 0.06rem;">
        

        <b>Odgovorna osoba:</b><br>
        <b>Karlo Starčević (direktor)</b><br><br>
        <b>Wepark smart parking d.o.o.</b><br>
        <b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
        <b>OFFICE:</b>&nbsp;zgrada IGH, Rakušina 1, 10000 Zagreb<br>
        <b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
        <b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
        <b>WEB:</b>&nbsp;www.wepark.eu<br>
        <br>
        <div style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
        Društvo je upisano u sudski registar Trgovačkog suda u Zagrebu pod brojem 081131571. Temeljni kapital u iznosu od 20.000 kn je uplaćen u cijelosti. Račun se vodi kod Privredne Banke Zagreb. Član društva Karlo Starčević.
        </div>
        <br style="display: block; clear: both;">
        
        
      </div>        
      
      <br id="${Math.random().toString(16).substring(2,10)}"  style="display: block; clear: both;">
    </div>  

    
</div>     

    



`;  
  
  // setup email data
  
  // 'igor.cerinski@wepark.eu, kresimir.marusic@outlook.com, milan.mesic@wepark.eu, tomislav.music@wepark.eu, zoran.ljubic@wepark.eu'
  
  var mailOptions = {
      from: `"WePark App" <${global.we_email_address}>`, // toni.kutlic@wepark.eu sender address
      to:  user_email_adress,  // user_email_adress, // list of receivers
      subject: 'Ažurirajte WePark App', // Subject line
      html:  html // html body
  };

    // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {

    if (error) {
     console.error({ 
        success: false, 
        msg: "greška u slanju newslettera!!!!",
        error: error
      });
      
      global.we_nl_count_error += 1;
      global.we_nl_error_emails.push(user_email_adress);
      
      console.log( 'GREŠKA U SLANJU MAILA COUNT = ' + global.we_nl_count_error + ' email = ' + user_email_adress  );


    } else {

      console.log({ 
        success: true, 
        msg: "Mail je poslan i sve je ok",
      });
      global.we_nl_count_ok += 1;
      console.log( 'POSLAN MAIL ' + global.we_nl_count_ok + ' / ' + global.we_NL_emails.length  );
      console.log( 'MAIL adresa je ' + user_email_adress  );

    };


  }); // end of send welcome email

  
};




global.we_NL_emails =
  [ "druzak@gmail.com","karlo.starcevic92@gmail.com","marketing.adriatic@signify.com","teodora.cota@molsoncoors.com","sanja.slamar@molsoncoors.com","boris.sivec@gmail.com","maja_medic@yahoo.com","nikica@outlook.com","dragokranjcec@gmail.com","kresimir.kos@gmail.com","kpletikosa@gmail.com","igor.cerinski@wepark.eu","darko.dvorzak@algebra.hr","skerlj@gmail.com","damirsertic0304@gmail.com","boris.cavlek@gmail.com","snjezana.lucic3@gmail.com","zeljka.knezovic@mobendo.com","sinisa.durdevic@gmail.com","ddragisic@msn.com","isucic@gmail.com","lusucic@gmail.com","danijelasp@gmail.com","mihaela.kelava@gmail.com","damirstarcevic1@gmail.com","edin.hodzic2@in2.eu","zoran.ljubic@wepark.eu","luka.vukovic.look@gmail.com","tomislav.mikolavcic@utilitas.hr","ivan.pavlek@snt.hr","danijela.harambasic@gmail.com","boris.agatic0@gmail.com","dario.kurnik@utilitas.hr","fajkovic.sasa@gmail.com","marko.ignjatovic@outlook.com","zoran.milic@har.hr","ivic.stipe@signify.com","matjaz.dremel@signify.com","tin.pusec@signify.com","matija.marcius@signify.com","admin@wepark.eu","marin.petrunic@gmail.com","damir.agic@snt.hr","natasa.subasic@signify.com","elena.balazic@signify.com","mario.spoljar@gmail.com","anita1.basic@gmail.com","danica.radunovic@signify.com","ivucak@gmail.com","cicnavi@gmail.com","marko.lektoric@snt.hr","jurisic.darijo@gmail.com","jukicivana13@gmail.com","tomislav.music@gmail.com","igor.sipka@gmail.com","ivan@frenos.hr","adela.sg@gmail.com","msimac@gmail.com","tomislavsaki@gmail.com","tomislav2112@gmail.com","vrkic3@gmail.com","dstanare@gmail.com","vplasc1@gmail.com","spetracic@gmail.com","petra.scuric@gmail.com","jasezovemvlatka@gmail.com","gorazdsurla@yahoo.co.uk","vlr150774@gmail.com","milan.mesic@snt.hr","ana.begic234@gmail.com","branko@itcs.hr","karlotestxiaomi@banit.me","teostarcevic1@gmail.com","silvija.horvat@tele2.com","luka.aralica@tele2.com","ivana.boban@tele2.com","sinisa.petkovic@tele2.com","silvija-marija.curic@tele2.com","ivana.dropuljic@tele2.com","ante.cancar@tele2.com","zeljka.radonic@tele2.com","vesna.cvrljevic@tele2.com","mladen.haramustek@tele2.com","danijela.perica@tele2.com","lidijaug@gmail.com","kata.prolic@tele2.com","goran.hodnik@tele2.com","smiljan.sapina@tele2.com","ivana.cabraja@trs.ina.hr","ivan.marasovic@zg.t-com.hr","dario.turkalj1@gmail.com","ivana.mravlincic@tele2.com","petar.sakic@tele2.com","dario.simac@tele2.com","arijana.zebec@tele2.com","petar.vajdic@gmail.com","belma@nodefactory.io","brencic@gmail.com","mladen.skypala@gmail.com","petra.stefan@tele2.com","danijela.doncevic@tele2.com","ivan.gagro@gmail.com","mirjana1001@gmail.com","kruno.komorcec@tele2.com","raca.nenad@gmail.com","antrophy@gmail.com","goran.ugarcina@algebra.hr","jc@coustaury.com","katarina@dgroops.com","mariopopovic@hotmail.com","fhavro@gmail.com","kdevcic@gmail.com","gzebec@gmail.com","toncipop@gmail.com","obajic@gmail.com","andrej.petto@hotmail.com","tele2@tele2.com","sever@ksu.hr","ivan03barun@gmail.com","koprivnjak12@gmail.com","josipa.jarni@gmail.com","ana.roje@gmail.com","maric.iv@gmail.com","zoran.pensek@gmail.com","mcar128@gmail.com","ivan.maric@infinum.com","mmesic@gmail.com","snt_grupa@gmail.com","kresimik@hotmail.com","andrea.lamesic@tele2.com","dinko.hecej@signify.com","antun.vujica@gmail.com","damir.turkalj@signify.com","jankivana@gmail.com","antun.vujica@tele2.com","anchins.paroski@gmail.com","frankanic@yahoo.com","anamoric33@gmail.com","slelas1970@gmail.com","fake5@mail.com","brizic.mario@gmail.com","danchy154@gmail.com","pedjaa@gmail.com","samardzija.tanja@gmail.com","martinabarbir21@gmail.com","petra.kursar@gmail.com","d2021491@urhen.com","d2021721@urhen.com","danijelavv@gmail.com","wspay@wspay.info","brunella.bukovic@stedinvest.hr","test@mail123.com","anamarija.drazic@stedbanka.hr","marko.curkovic@stedbanka.hr","ines.toplak@zczz.hr","goran.hajsek@zczz.hr","tanja.kranjac@zczz.hr","ira.velican-smrekar@stedbanka.hr","anto.krstanovic@zczz.hr","josip.kralj@zczz.hr","petra.bestak@stedbanka.hr","tomislav.hrdelja@zczz.hr","ante.rados@zczz.hr","renata.frljuzec@zczz.hr","boris.klemenic@zczz.hr","marko.sulic@zczz.hr","tomislav.mihovilic@zczz.hr","ivan.mamuca@zczz.hr","bmantolic@zczz.hr","stipe.lovric@zczz.hr","kristijan.hrabar@zczz.hr","zdravko.antolkovic@zczz.hr","vkavac@gmail.com","jelena.vurajic@zczz.hr","vlado.kavac@zczz.hr","ivana.zczz@gmail.com","ivana.fuckan@zczz.hr","iva.padovan@zczz.hr","vladimir.pacic@zczz.hr","miroslav.ratkajec@gmail.com","miroslav.ratkajec@zczz.hr","maja.midjan@stedinvest.hr","vedran.blazecki@zczz.hr","zvonimir.bratkovic@zczz.hr","matija.posavec@zczz.hr","zeljka.vujanic@zczz.hr","rade.basic@zczz.hr","gina.corbo.kovacic@zczz.hr","ljubica.simunic@stedbanka.hr","linadujic@gmail.com","tihomir.prlic@zczz.hr","karmen.kos@zczz.hr","marica.galovic@stedbanka.hr","toni.kutlic@wepark.eu","matijacavrag94@gmail.com","kumicina74@gmail.com","marin.krpetic@stedbanka.hr","marina.lazic76@gmail.com","mgrubisic@senso.hr","ivan.bosnjakovic@senso.hr","ljubicd@gmail.com","deanturner@gmail.com","renata.hleb@zczz.hr","tomislavrobic.pmf@gmail.com","ivan.kralj@zczz.hr","zvonko.merkas@gmail.com","bruno.bendekovic@senso.hr","lada.flac+wepark@gmail.com","milosartem@gmail.com","jkurdija@pbz.hr","plavo11@yahoo.com","racunovodstvo@zczz.hr","danijel.pavic@zczz.hr","goran.father@gmail.com","boris.abramovic@wepark.eu","webstudiomail@gmail.com","elvis.matahlija@gmail.com","zlatko.asperger@gmail.com","kresimir.marusic@outlook.com","simona.trsinar@gmail.com","h.kovac@icloud.com","nikolapra@gmail.com","tomislav.bacanek@zczz.hr","bojan.hasic@gmail.com","jurisickristian2@gmail.com","marta.gorican@outlook.com","marioseretin@gmail.com","leo.mrsic@algebra.hr","toni@circulum.it","prodaja@zczz.hr","jmstefanac@baldmonk.com","kathia.reyes@outlook.es","zvonimir.tokic.88@live.com","kskovrlj@gmail.com","vjekoslav.jersic@gmail.com","orijana@yahoo.com","branko.holik@senso.hr","mihael-j@hotmail.com","ivica.patrlj@gmail.com","franjo.lazarin@zczz.hr","dubroginic@gmail.com","ivan.spanic385@gmail.com","josip.belina@gmail.com","renatoklekoveckeko@gmail.com","tatjanaorescaninbabic@gmail.com","ruzica.sam@gmail.com","branimir2k@gmail.com","zvonimirpalic@gmail.com","dibup.marin@gmail.com","dario.suskalo@gmail.com","vpostnova44@gmail.com","joanna.habunek@gmail.com","jegiw81157@newe-mail.com", "lovro@stedinvest.hr","dfsdfs@fefe.com","luka.ljubicic10@gmail.com","kresimir.ljubicic@gmail.com","llozanic7@gmail.com","roknic57@gmail.com","hrco91@gmail.com","nenogan@gmail.com","hexx@protonmail.com","krunoslav.markovic1@gmail.com","srecko.andrlon1@gmail.com","andar999@gmail.com","iva.prorocic@gmail.com","kdrazen@gmail.com","kristina.harmina@yahoo.com","vukapartmani.senj@gmail.com","mpaukica@gmail.com","mladen.milavic@hotmail.com","doddo980@gmail.com","dmomcinovic@gmail.com","mirjana.horvat1@t.ht.hr","zlodi.bukvic@gmail.com","donald.pribanic@zczz.hr","mate@circulum.it","ssanja.ssliskovic@gmail.com","ivana0611@gmail.com","tomislav.razov@gmail.com","kuxonline@gmail.com","bockajvalentina@gmail.com","stovarlazabody@gmail.com","carskirez@msn.com","dora.smolec@gmail.com","elakatina1812@gmail.com","sanin.muminovic@gmail.com","th.h.suermann@gmail.com","majadespot0805@gmail.com","marija1996kovacic@gmail.com","brita2411@gmail.com","Klarakundich99@gmail.com","barbaragranic1105@gmail.com","jedvajmartina@gmail.com","anitap1003@gmail.com","stjepna2003@gmail.com","kadrovska@zczz.hr","miljenko.petrak@gmail.com","dejoanic@yahoo.com","teja.jersin@gmail.com","krispin.stock@gmail.com","mkasovic@gmail.com","tomislav.sandalj@gmail.com","leonarda.bambic@gmail.com","hrvoje.ranogajec@gmail.com","mia.kranjcec@gmail.com","dvuljanko@gmail.com","bartulovic@mazda.hr","m.jankoviczg@gmail.com","miletich.ivona@gmail.com","marinkohorta1@gmail.com","dora.hipsa@gmail.com","filip.h1994@gmail.com","marin.mihajlovic1994@gmail.com","monika.kupresak@tele2.com","sven.srebot@gmail.com","lukarecic@gmail.com","ibirkic@gmail.com","tanja.dvidakovic@yahoo.com","sucic.anna@gmail.com","lukacicgordana@gmail.com","marinbrkic762@gmail.com","kosslec_erro7@hotmail.com","basictea96@gmail.com","bozidar.akrap.sk@gmail.com","karlo.petravic@dpd.hr","topolovec.karla@gmail.com","knez.ana@gmail.com","sara.prenc22@gmail.com","petar.babic1003@gmail.com","kristina.videc@tele2.com","ida.carnohorski@gmail.com","nada@zgt.hr","brigita@zgt.hr","igor@zgt.hr","goran.galic.zg@gmail.com","tomislav.hrgovic@gmail.com","dvasic@gmail.com","mezga.matej@gmail.com","karlabubas9@gmail.com","david.kratky@seznam.cz","antonia.precali@gmail.com","steven61-27@hotmail.fr","topiskor@mail.ru","marko.erlic97@gmail.com","lucijan233@gmail.com","dommagoj.ivancic2@gmail.com","dragan.perakovic@gmail.com","antonelajurisic10@gmail.com","anatesijazg@gmail.com","stjepankrpan6@gmail.com","jojo.snores@gmail.com","aleksandric.dino@gmail.com" ];
  


/*
global.we_NL_emails = [
  'tkutlic@gmail.com',
  'toni.kutlic@wepark.eu'
];
*/


global.already_NL_sent = [];

global.we_nl_count_ok = 0;
global.we_nl_count_error = 0;


global.we_randomIntFromInterval = function(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
};


global.send_nl_in_random_intervals = function(arg_index) {
  
  var current_email = global.we_NL_emails[arg_index];

  
  var new_index =  arg_index + 1;
  var delay = global.we_randomIntFromInterval(1000*3, 1000*10);
  
  if ( $.inArray( current_email, global.already_NL_sent ) == -1 ) {

    global.send_newsletter(current_email);
    global.already_NL_sent.push(current_email);
    
    console.log( 'SLANJE MAILA ZA ' + delay/1000 + ' sekundi.....' );
      
    if ( new_index <= global.we_NL_emails.length - 1 ) {
      
        setTimeout( function() { 
          global.send_nl_in_random_intervals(new_index);  
        }, delay );  

    } else {
      
      console.log('GOTOVO SLANJE MAILOVA !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
      
    };
    
    
  } 
  else {
    
    console.log( 'već poslao mail na ovu adresu ' );
    
    if ( new_index <= global.we_NL_emails.length - 1 ) {
      global.send_nl_in_random_intervals(new_index);  

    } else {
      console.log('GOTOVO SLANJE MAILOVA !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    };
    
  };
  
  
}; // kraj rekurzivne funkcije



