var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BleSecretKey = mongoose.model('BleSecretKey');


exports.secret_from_mac = function(req, res) {
  
  
  // ima samo jedan dokument u ovoj kolekciji i to je dokument sa objektom koji je zapravo dictionary
  /*
  npr:
  "secret_from_mac" : {
        "E732433065BC" : "F21EC38CBA40076A",
        "C46EBE2CF5D7" : "815FBB38D826FCF0",
        "E252003108FE" : "EA261333BD3C8C88",
        .... itd
        }
  */
  
  var ble_mac = req.params.ble_mac;
  
  BleSecretKey.find({ secret_from_mac: { $ne: null } })
  .exec(function(err, secrets_obj) {
    if (err) {
        res.json({ 
          success: false, 
          msg: "Dogodila se greška u mongoose queriju prilikom traženja secreta za ovaj mac !!!",
          secret: null
        });
    };  

    var secret = secrets_obj.secret_from_mac[ble_mac];

    if ( !secret ) {
      res.json({ 
        success: false, 
        msg: "Ne postoji ovaj mac uređaja u bazi i stoga ne mogu naći secret !!!!",
        secret: null
      });

    } else {

      res.json({ 
        success: true, 
        msg: "Ovo je secret !!!!",
        secret: secret
      });

    };

  }); 
  
};


