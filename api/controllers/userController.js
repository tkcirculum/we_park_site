
var mongoose = require('mongoose');

mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var jwt = require('jsonwebtoken');


var scramblePass = require('password-hash-and-salt');

var nodemailer = require('nodemailer');

var email_body = require('./email_body');


var confirm_pass = require('./reset_pass_body');

var UserCounter = mongoose.model('UserCounter');

var UserLang = mongoose.model('UserLang');

var CustomerProduct = mongoose.model('UserLang');


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


var find_controller = require('./find_controller.js');

var rampa_controller = require('./rampa_controller.js');



function convert_mongoose_object_to_js_object(mongoose_object) {
  
  var js_object = mongoose_object.toJSON();
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};


exports.authenticate_a_user = function(req, res) {
  
  var cit_pass_timeout = 720;
  
  var pass = req.body.password;
  
  // find the user
  User.findOne(
    { name: req.body.name }, 
    function(err, user) {
    
    if (err) throw err;
    
    if (!user) {
      // if no user
      res.json({ success: false, msg: 'Authentication failed. User not found.' });
    } 
    else if (user) {
      
      
      
      // if user is found verify a hash 
      scramblePass(pass).verifyAgainst(user.hash, function(error, verified) {
        if(error)
            throw new Error('Something went wrong with hash checking!');
        if(!verified) {
          // if pass is wrong !!!!!!
          res.json({ success: false, msg: 'Authentication failed. Wrong password.' });
        } else {
          
          var user_copy = convert_mongoose_object_to_js_object(user);
          
          if ( user && user_copy.roles && user_copy.roles.we_app_user ) cit_pass_timeout = 720 * 1000; // to je  točno 500 dana
          
          // return the information including token as JSON
          // using now +  (expiresIn miliseconds) minus 10 sec just to be sure I'm  in proper interval
          
          
          // if user is found and password is right
          // create a token
          
          var sign_obj = {
            _id: user_copy._id,
            hash: user_copy.hash
          };
          

          var token = jwt.sign(sign_obj, 'super_tajna', {
            expiresIn : 60 * cit_pass_timeout // expires in 720 min
          });
          
          
          user_copy.user_id = user._id;
          user_copy.success = true;
          user_copy.msg = 'Enjoy your token!';
          user_copy.token = token;
          user_copy.token_expiration = Date.now() + (1000 * 60 * cit_pass_timeout) - 10000;
          
          res.json(user_copy); 

          
        }; // end of ELSE !verified
        
      }); // end of scramblePass
      
    }; // end of user is OK
    
  }); // end of User.findOne
  
}; // end of authenticate_a_user


exports.prolong_token = function(req, res) {
  
  var cit_pass_timeout = 720; // in minutes
  
  
  var old_token = req.body.token;
  // verifies secret and checks exp
  jwt.verify(old_token, 'super_tajna', function(err, decoded_user) {      
      if (err) {
        return res.json({ success: false, msg: 'Failed to authenticate token.' });    
      } else {
        
        // if everything is OK - make a new token
        // decoded is extrapolated user object from token itself !!!!
        // token always has the scrambled user data inside !!!!!
        
        
        /*
        --------------------------------------------------------------------
        OVAKO IZGLEDA ROLES ZA MOBLE USERA
        OBRATI PAŽNJU DA IMA PROPERTY we_app_user !!!!!!!!!
        
        roles: [{
            user: true,
            admin: false,
            we_app_user: true,
            super_admin: false
        }]
        --------------------------------------------------------------------
        */
      
  
      var time_now = Date.now();  
        
      User.findOneAndUpdate(
          { _id: decoded_user._id },
          { $set: { last_login: time_now }},
          {new: true}, 
          function(err, user) {
            if (err) {
              res.send(err);
              return;
            };

            if ( decoded_user && decoded_user.roles && decoded_user.roles.we_app_user ) cit_pass_timeout = 720 * 1000; // to je  točno 500 dana
    
            
            var sign_obj = {
              _id: decoded_user._id,
              hash: decoded_user.hash
            };
            
            var new_token = jwt.sign(sign_obj, 'super_tajna', {
                expiresIn : 60 * cit_pass_timeout // expires in 30 min
            });
            res.json({
              token: new_token,
              token_expiration: Date.now() + (1000 * 60 * cit_pass_timeout) - 10000, // minus 10 sec just to be sure
            });

          });        
        
      };
  });
};


/* TODO -- create add role to user */
// exports.add_role_to


/* TODO -- send request for publisher !!!!!!! */
// exports.request_for_publisher


function spremi_usera(user_num, user_object, original_pass, req, res, new_or_edit) {
  
  
      var cit_pass_timeout = 720; // in minutes
  
  
      scramblePass(original_pass).hash(function(error, new_hash) {
        if(error)
          res.send({ success: false, msg: 'Something went wrong with pass hash !!!' });
        
        
        // Store hash (incl. algorithm, iterations, and salt) 
        user_object.hash = new_hash;
        user_object.user_number = user_num;
        
        if ( 
          user_object.email.indexOf('stedgrupa.hr') > -1 ||
          user_object.email.indexOf('stedbanka.hr') > -1 ||
          user_object.email.indexOf('stedinvest.hr') > -1 ||
          user_object.email.indexOf('dubroginic@gmail.com') > -1
         ) {
          user_object.customer_tags = [ {  "sifra" : "STED",  "naziv" : "STED" } ];
        };
        
        
        
        if ( user_object.email.indexOf('zczz.hr') > -1 ) {
          user_object.customer_tags = [ {  "sifra" : "ZCZZ",  "naziv" : "ZCZZ" } ];
        };
        
        
        if ( user_object.email.indexOf('tehnozavod.hr') > -1 ) {
          user_object.customer_tags = [ {  "sifra" : "TZM",  "naziv" : "TZM" } ];
        };
        
        
        if ( 
          user_object.email.indexOf('bojan.hasic@gmail.com') > -1  ||
          user_object.email.indexOf('bruno.bendekovic@senso.hr') > -1    ||
          user_object.email.indexOf('marijana.grubisic@senso.hr') > -1   ||
          user_object.email.indexOf('zlatko.asperger@gmail.com') > -1
        ) {
          user_object.customer_tags = [ {  "sifra" : "MM",  "naziv" : "MM" } ];
        };
        
        var transporter = nodemailer.createTransport(global.we_mail_setup);
        

    /*
    var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
              user: 'vrata.dizajn.crm@gmail.com', // Your email id
              pass: 'vrataCRM123!321' // Your password
          }          
        }); 
    */
        
        var is_app = false;
        var mail_subject = 'Dobrodošli u WePark Admin';
        
        if ( user_object && user_object.roles && user_object.roles.we_app_user ) {
          mail_subject = 'Dobrodošli u WePark App!';
          is_app = true;
        };
        
        var welcome_email_id = Date.now() + '-' + ( Date.now() + 35154354354 ) + '-' + ( Date.now() + 6846454354 );
        //req.headers.origin  OR  'http://192.168.178.30:8080'
        var link = 'https://wepark.circulum.it:9000/confirm_email/' + welcome_email_id ;
        var email_user_name = user_object.name;
        
        var html = email_body.generate(link, email_user_name, is_app).html;
        
        // setup email data
        var mailOptions = {
            from: `"WePark App" <${global.we_email_address}>`, // sender address
            to: user_object.email, // list of receivers
            subject: mail_subject, // Subject line
            html:  html // html body
        };

          // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
          
          if (error) {
            
            res.json({ 
              success: false, 
              msg: "User nije spremljen!!! Problem u slanju maila  za konfirmaciju !!!!",
              error: error
            });
            
          };
          
          // add new email id to user object
          user_object.welcome_email_id = welcome_email_id;
          
          var new_user = new User(user_object);
          // save to DB
          new_user.save(function(err, user) {
            
            if (err) {
              res.send(err);
            };
            
            // create a token for new user !!!!
            
            var user_copy = convert_mongoose_object_to_js_object(user);
            
            if ( user && user_copy.roles && user_copy.roles.we_app_user ) cit_pass_timeout = 720 * 1000; // to je  točno 500 dana
           
            
            var sign_obj = {
              _id: user_copy._id,
              hash: user_copy.hash
            };
           
            
            var token = jwt.sign(sign_obj, 'super_tajna', {
              expiresIn : 60 * cit_pass_timeout // expires in 30 min
            });

            // return user info including token as JSON
            
            // using now +  (expiresIn miliseconds) minus 10 sec just to be sure I'm in proper interval

            /* ----------------------- HERE IS RESPONSE FOR USER SAVE !!!!!  ----------------------- */

            
            user_copy.user_id = user._id;
            user_copy.success = true;
            user_copy.msg = 'Enjoy your token!';
            user_copy.token = token;
            user_copy.token_expiration = Date.now() + (1000 * 60 * cit_pass_timeout) - 10000;

                   
                        // ako postoji nešto car_plates u objektu novog usera
            if ( user_object.car_plates && $.isArray(user_object.car_plates) && user_object.car_plates.length > 0 ) {
              
              var plates_obj = {
                user_number: user_object.user_number,
                car_plates: user_object.car_plates
              };
              
              rampa_controller.new_plates_saving(plates_obj)
              .then(
              function(result){
                
                if ( result.success == true ) {
                  console.log('---------------- SPREMIO SAM TABLICE na sve rampe !!!!! -----------------');
                } else {
                  console.error('---------------- SAVING PLATES NA SVE RAMPE FALSE !!!!! -----------------');
                  console.log(result);
                };
                
              })
              .catch(
              function(fail) {
                console.error('---------------- SAVING PLATES CATCH FAIL !!!!! -----------------');
                console.log(fail);
              });
              
            };
            
            
            
            
            
            res.json(user_copy);
            
            
    global.create_vouchers(
      1, // how_many
      0, // amount
      0, // discount
      user_object, // user_obj
      'nulti voucher za dugove', // arg_desc
      'voucher', // type
      null, // broj parka
      null, // days
      null, // tag
      null, // START_DATE
      true // DUMMY_VOUCHER_ID
    );
                        
            if ( 

              user_object.email.indexOf('stedgrupa.hr') > -1 ||
              user_object.email.indexOf('stedbanka.hr') > -1 ||
              user_object.email.indexOf('stedinvest.hr') > -1 ||
              user_object.email.indexOf('dubroginic@gmail.com') > -1

               ) {
              global.create_vouchers(1, 300, 0, user_object , 'stedgrupa vip owner', 'vip', 14, 36500, 'STED', null, true);
            };

            
            if ( 

              user_object.email.indexOf('t.ht.hr') > -1

               ) {
              global.create_vouchers(1, 300, 0, user_object , 'tht vip owner', 'vip', 16, 36500, null, null, true);
            };
            

            if ( user_object.email.indexOf('zczz.hr') > -1 ) {
              global.create_vouchers(1,300,0, user_object , 'ZCZZ vip owner', 'vip', 14, 36500, 'ZCZZ', null, true);
            };
            
            
            if ( user_object.email.indexOf('tehnozavod.hr') > -1 ) {
              global.create_vouchers(1,300,0, user_object , 'TZM vip owner', 'vip', 15, 36500, 'TZM', null, true);
            };
              
            if ( 
              user_object.email.indexOf('bojan.hasic@gmail.com') > -1  ||
              user_object.email.indexOf('bruno.bendekovic@senso.hr') > -1    ||
              user_object.email.indexOf('marijana.grubisic@senso.hr') > -1   ||
              user_object.email.indexOf('zlatko.asperger@gmail.com') > -1
            ) {
              global.create_vouchers(1,300,0, user_object, 'MICRO MACRO vip owner', 'vip', 14, 36500, 'MM', null, true);
            };
            
            
          }); // end of user save  
            
        }); // end of send welcome email
                
      }); // end of scramble pass
        
  
  
}; // kraj moje funkcije spremi usera


exports.create_a_user = function(req, res) {

  var user_object = req.body;
  
  // COPY READABLE PASS TO VARIABLE
  var original_pass = String(user_object.password);
      
  // DELETE READABLE PASS !!!!!!!!!! -- just to be safe :-)
  delete user_object.password;
  var name_taken_switch = false;
  var email_taken_switch = false;
  
  User.findOne(
    { name: req.body.name }, 
    function(err, user) {
      if (err) {
        
          res.json({ 
            success: false, 
            msg: "Dogodila se greška u traženju usera!!!",
            user_number: null,
          });
        return;
      };
      
      
      if (user) {
        
        // -------------------------- AKO JE NAŠAO ISTO IME / NADIMAK ----------------------------
        
        name_taken_switch = true;
        
        res.json({ 
            success: false, 
            msg: "Već postoji korisnik s ovim nadimkom !!!",
            user_number: null,
            name_taken: true
          });
        return;
        
        
      } else {
        
        
    
      User.findOne(
        { email: req.body.email }, 
        function(err, user) {
          if (err) throw err;
          
          if (user) {

            // -------------------------- AKO JE NAŠAO ISTI EMAIL  ----------------------------
            
            email_taken_switch = true;

            res.json({ 
                success: false, 
                msg: "Već postoji korisnik s ovim emailom !!!",
                user_number: null,
                email_taken: true
              });
            
            return;
            

          } else {
            
            // -------------------------- AKO NIJE ISTO IME NITI JE ISTI EMAIL ----------------------------

            // user counter kolekcija ima samo jedan dokument i taj dokument ima samo jedan propery, a to je count !!!!!
            // inc znači increment  - u biti povećavam zadnji upisani counter za +1 !!!!
            // $ne: null je bezveze query koji će uvijek vratiti true jer count nikada nije null !!!

              UserCounter.findOneAndUpdate({ count: { $ne: null } }, { $inc: { count: 1 } }, { "new": true })
              .exec(function(err, counter) {
                if (err) {
                    res.json({ 
                      success: false, 
                      msg: "User nije spremljen!!! PROBLEM u dodjeljivanju novog broja useru u kolekciji user counter !!!!",
                      user_number: null
                    });
                };  
                var new_user_number = counter.count;
                var new_or_edit = 'new';

                spremi_usera(new_user_number, user_object, original_pass, req, res, new_or_edit);

                }); // kraj user counter ----> povećaj counter i spremi
            
          };

        }); // end of User.findOne

      };
      
    }); // end of User.findOne
  
    
}; // end of create_a_user function


exports.confirm_user_email = function(req, res) { 
  
  User.findOneAndUpdate(
    { welcome_email_id: req.params.welcome_email_id },
    { $set: { email_confirmed: true }},
    {new: true}, 
    function(err, user) {
      if (err) res.send(err);
      
      // res.json({ msg: "Email confirmed !!!", customer_id: customer._id });
      
      // this is just exapmle of sending raw html
      res.set('content-type','text/html');
      
      
var link_to_we_park_admin = 
`<a style="  background-color: #06253f;
border-radius: 8px;
height: 60px;
width: 200px;
font-size: 14px;
margin: 20px auto 0;
text-align: center;
display: block;
min-width: 200px;
color: #fff;
text-decoration: none;
line-height: 60px;"

target="_blank"
href="https://wepark.circulum.it:9000">

GO TO WE PARK ADMIN

</a>`;
      
      
      var mail_confirmation_page = 
`
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
  <title>WePark Email Confirmation</title>
  <meta name="description" content="We Park Email Confirmation">
  <meta name="author" content="We Park Ltd">

</head>
<body style="display: flex; 
             align-items: center; 
             justify-content: center; 
             flex-direction: column; 
             height: 100vh; 
             font-family: Arial, Helvetica, sans-serif;">




        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />

  
  
  <h4 style="display: block; font-family: Arial, Helvetica, sans-serif; font-size: 30px;  margin: 15px; color: #06253f;">Email potvrđen!</h4>
  <h6 style="display: block; font-family: Arial, Helvetica, sans-serif; font-size: 15px; margin: 0px; color: #06253f;">Uživajte u WePark aplikaciji :)</h6>

${ (user.roles && user.roles.admin == true) ? link_to_we_park_admin : '' }

</body>
</html>
`;      
      res.send(mail_confirmation_page);  
      
    });
};



exports.get_sve_user_brojeve = function(req, res) { 
  
   User.find({})
    .select({
      "user_number": 1,
      "full_name": 1
    }) 
    .exec(function(err, useri) {
      if (err) res.send(err);
      res.json(useri);
    });
};


function run_user_edit(pronadjeni_user, req, res) {
  
  
  var cit_pass_timeout = 720;
  
  if ( pronadjeni_user.roles && pronadjeni_user.roles.we_app_user == true ) {
    cit_pass_timeout = 720 * 1000; // to je  točno 500 dana
  };

  // -------------- OBRIŠI SVE PROPERTIJE KOJI IMAJU VEZS SA TOKENOM I AUTENTIKACIJOM
  
  var user_id = req.body._id;
  
  delete req.body._id;
  delete req.body.user_id;
  delete req.body.success;
  delete req.body.msg;
  delete req.body.token;
  delete req.body.token_expiration;

  // obrisi i roles ----> tako da slučajno ne prebrišem roles koje su u menjuvremenu spremljene u bazu !!!!
  /// .. ali nisu povečene lokalno u cit_user !!!!
  delete req.body.roles;
  
  User.findOneAndUpdate({ _id: user_id }, req.body, { new: true }, function(err, updated_user) {

    if (err) {
      res.send({ success: false, msg: 'Dogodila se greška u spremanju usera !', error: err });
      return;
    };
    
    if ( !updated_user ) {
      res.send({ success: false, msg: 'Dogodila se greška u spremanju usera !', error: 'updated_user je null' });
      return;
    };
    
    var clean_user = convert_mongoose_object_to_js_object(updated_user);

    var sign_obj = {
      _id: clean_user._id,
      hash: clean_user.hash
    };

    var token = jwt.sign(sign_obj, 'super_tajna', {
      expiresIn : 60 * cit_pass_timeout // expires in 720 min
    });


    clean_user.user_id = clean_user._id;
    clean_user.success = true;
    clean_user.msg = 'Enjoy your token!';
    clean_user.token = token;
    clean_user.token_expiration = Date.now() + (1000 * 60 * cit_pass_timeout) - 10000;

    res.json({ success: true, user: clean_user });

  });
          
  
  
  
  
}



function find_user_and_update(req, res) { 
  
// find the user
  User.findOne({ _id: req.body._id }, function(err, user) {
    
    if (err) {
      res.json({ success: false, msg: 'Ažuriranje korisnika nije uspjelo.<br>Dogodila se greška prilikom pretrage usera!' });
      return;
    };
    
    if (!user) {
      // if no user
      res.json({ success: false, msg: 'Ažuriranje korisnika nije uspjelo.<br>Sustav nije pronašao korisnika u bazi!' });
    } 
    else {
      
      var clean_user = convert_mongoose_object_to_js_object(user);
      // ----------------------------- ovo radi samo ako je super admin ili ako editira samog sebe !!!!
      run_user_edit(clean_user, req, res);
      };
    
  }); // end of User.findOne
  
}; // kraj func find user and update !!!



exports.edit_user = function(req, res) {
  


  var edit_user_num = req.body.user_number;
  
  if ( !edit_user_num ) {
    
    res.json({ 
          success: false, 
          msg: "User nije ažuriran! Naispravni podaci usera!",
          user_number: edit_user_num
        });    
    
    return;
    
  };
  
  var token = req.headers['x-auth-token'] || null;
  
  if ( token ) {
      
    find_controller.check_is_super(token).then(
    function( result ) {
      
      var pp_sifra_editiranog_parka = String( req.body.pp_sifra );
      
      if ( result.success == true ) {
        // ako je super admin ili ako sprema sebe samog !!!
        if (  result.super_admin == true || edit_user_num == result.user.user_number ) {
          
          find_user_and_update(req, res);
          
          return;
          
        } else {
          // ako nije super admin i nije vlasnik ovog parkinga
          res.json({ success: false, msg: 'Nemate ovlasti za ovu radnju!' });  
          
        };
        
      } else {
        
        res.json({ success: false, msg: 'Došlo je do greške prilikom spremanja parkirališta!' }); 
        return;
        
      };

    })
    .catch(function(err) {
      
      // ako se dogodi bilo koja greška kod verifikacije tokena ili pronalaska usera
      res.json({ success: false, msg: 'Došlo je do greške prilikom spremanja parkirališta!' }); 
      
    });

  } 
  else {
    // vrati da nema ovlasti ako je request bez tokena !!!!
    res.json({ success: false, msg: 'Nemate ovlasti za ovu radnju!' });    
    
  };
  
};



exports.user_pass_reset = function(req, res) { 

   
  var user_email = req.body.user_email;
  var user_new_pass = req.body.new_pass;
  
  
// find the user
  User.findOne(
    { email: user_email },
    function(err, user) {
    
    if (err) res.json({ success: false, msg: 'popup_error_reset_pass', err: err });
    
    if (!user) {
      // if no user
      res.json({ success: false, msg: 'popup_no_user_by_email' });
    } 
    else if (user) {
      
    var clean_user = convert_mongoose_object_to_js_object(user);
    
    var new_pass_email_id = clean_user.user_number + '-' + Date.now() + '-' + ( Date.now() + 7234567891234 );

    User.findOneAndUpdate(
      { _id: clean_user._id }, 
      { new_pass_request: user_new_pass, new_pass_email_id: new_pass_email_id, new_pass_email_confirmed: false },
      { new: true }, function(err, new_user_db) {

      if (err) res.json({ success: false, msg: 'popup_error_reset_pass', err: err });
  
   
      var transporter = nodemailer.createTransport(global.we_mail_setup);


      var html = confirm_pass.conf_html(new_pass_email_id, clean_user);

      // setup email data
      var mailOptions = {
          from: `"WePark App" <${global.we_email_address}>`, // sender address
          to: clean_user.email, // list of receivers
          subject: 'WePark promjena lozinke', // Subject line
          html:  html // html body
      };

        // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {

        if (error) {

         res.json({ success: false, msg: 'popup_error_reset_pass', err: error });

        } else {

           res.json({ success: true, msg: 'popup_reset_pass_check_email' });

        };


      }); // end of send  email


    });    // kraj update usera s novom šifrom za email confim  


      
    }; // end of user is OK
    
  }); // end of User.findOne
  


};



function create_reset_pass_response_html(is_ok) {
  
  
  var big_text = 'Potvrda promjene lozinke je uspješna!';
  var small_text = 'Sada se možete ulogirati u WePark aplikaciju s novom lozinkom.';
    
  if ( is_ok == false ) {
    big_text = '<span style="color: #cb0606;">Došlo je do greške u potvrdi email adrese!</span>';
    small_text = 'Moguće je da ste već kliknuli na gumb POTVRDI i nova lozinka je već aktivna. Ako se ne možete ulogirati s novom lozinkom, probajte ponovo kliknuti na gumb POTVRDI u emailu. Ukoliko se greška ponovi molimo nazovite:<br> 00385 92 3133 015';
  };
  
  var pass_email_confirm_response = 
`
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
  <title>WePark Email Confirmation</title>
  <meta name="description" content="We Park Email Confirmation">
  <meta name="author" content="We Park Ltd">

</head>


<body style="display: flex; 
             align-items: center; 
             justify-content: center; 
             flex-direction: column; 
             height: 100vh; 
             font-family: Arial, Helvetica, sans-serif;">
  
<h4 style="display: block; 
font-family: Arial, Helvetica, sans-serif;
font-size: 30px;
text-align: center;
margin: 15px; 
color: #777;">
${big_text}
</h4>

<h6 style="display: block;
font-family: Arial, Helvetica, sans-serif;
font-size: 15px;
text-align: center;
margin: 0px; 
padding: 20px;
color: #777;
max-width: 300px;">
${small_text}
</h6>

</body>


</html>
`; 
      
return pass_email_confirm_response;
  
}



exports.pass_reset_confirm = function(req, res) { 
  
  User.findOne(
    { new_pass_email_id: req.params.reset_email_id },
    function(err, user) {
      
      
      if ( !user || err ) {
        var response_html = create_reset_pass_response_html(false);
        // this is just exapmle of sending raw html
        res.set('content-type','text/html');
        res.send(response_html);
        return;
      };
      
      if (user) {
        
        var clean_user = convert_mongoose_object_to_js_object(user);
        
        scramblePass( clean_user.new_pass_request ).hash(function(error, new_hash) {
          
          if (error) {
            var response_html = create_reset_pass_response_html(false);
            // this is just exapmle of sending raw html
            res.set('content-type','text/html');
            res.send(response_html);
            return;
          };
          
          User.findOneAndUpdate(
            { _id: clean_user._id },
            { hash: new_hash, new_pass_request: null, new_pass_email_confirmed: true },
            {new: true}, 
            function(err, user) {
              
              var response_html = create_reset_pass_response_html(true);
              // this is just exapmle of sending raw html
              res.set('content-type','text/html');
              res.send(response_html);
              
            }); // kraj update user
          
        }); // kraj scramble pass

      } else {
        
        var response_html = create_reset_pass_response_html(false);
        // this is just exapmle of sending raw html
        res.set('content-type','text/html');
        res.send(response_html);
        
      };
      
    }); // kraj find user
};



exports.pass_for_token = function(req, res) {
  
  var pass = req.body.pass;
  var user = req.body.user;
  
  // find the user
  User.findOne(
    { name: user }, 
    function(err, user) {
    
    if (err) {
      res.json({ success: false, msg: 'Došlo je do greške prilikom autentikacije!', err: err });
      return;
    };
    
    if (!user) {
      // if no user
      res.json({ success: false, msg: 'Korisnik nije pronađen!' });
      return;
    };

      // if user is found verify a hash 
      scramblePass(pass).verifyAgainst(user.hash, function(error, verified) {
        
        if (error) {
          res.json({ success: false, msg: 'Došlo je do greške prilikom provjere lozinke!', err: error });
          return;
        };
        
        if ( !verified ) {
          // if pass is wrong !!!!!!
          res.json({ success: false, msg: 'Lozinka nije ispravna!' });
        } else {
          
          res.json({ success: true, msg: 'Lozinka je ispravna!' });
          
        }; // end of ELSE !verified
        
      }); // end of scramblePass
      
    
  }); // end of User.findOne
  
}; // end of pass for token


exports.set_user_lang = function(req, res) {
  
  var user_number = req.body.user_number;
  var lang = req.body.lang;


  UserLang.findOneAndUpdate( 
    { user_number: user_number },
    { lang: lang },
    { upsert: true, safe: true, new: true },
    function(err, saved_user_lang) {

    if (err) {
      console.log('ERROR !!!!!!! U SAVE USER LANG FOR USER NUMBER: ' + user_number);
      res.json({success: false, err: err })
      console.log(err);

    } else {
      global.all_user_langs[user_number] = lang;
      res.json({success: true, lang: saved_user_lang.lang })
      
    };
  }); // end of upsert user lang
  
  
  
};


exports.r1_racun_user_podaci = function(req, res) {
  
  
  var user_num = req.body.user_num;
  
  var naziv_firme = req.body.naziv_firme;
  var oib = req.body.oib;
  var adresa = req.body.adresa_firme || '';
  var mjesto = req.body.pp_i_mjesto_firme ? ( ', ' + req.body.pp_i_mjesto_firme ) : '';
  
  var r1 = req.body.r1;
  
  
  var update_object = {};
  
  if ( oib ) update_object.oib = String(oib);
  if ( naziv_firme ) update_object.business_name = naziv_firme;
  if ( adresa ) update_object.business_address = adresa + mjesto;
  
  if ( r1 ) update_object.r1 = true;
  if ( !r1 ) update_object.r1 = false;
  
  
  

  User.findOneAndUpdate(
      { user_number: user_num },
      update_object,
      {new: true}, 
      function(err, user) {
        if (err) {
          res.send({ success: false, msg: 'popup_business_podaci_error', error: err });
        } else {
          res.send({ success: true, msg: 'Business podaci usera SPREMLJENI !!!!!' });
        };
      }); 
  
};


exports.get_user_business_data = function(req, res) { 
  
  User.findOne(
    { user_number: Number(req.params.user_num) },
    function(err, user) {
      
      if ( !user || err ) {
        res.send({ success: false, error: (err || null) } );
        return;
      };
      
      if (user) {
    
        var clean_user = convert_mongoose_object_to_js_object(user);
        
        res.send({
          success: true,
          oib: clean_user.oib,
          business_name: clean_user.business_name,
          business_address: clean_user.business_address          
        });
        
      }; 
    }); // kraj find user
};



exports.check_jel_nova_rega = function(req, res) { 
  
  var reg = req.params.reg;
  var query = {};
  
  // kada novi user koji se još nije registrirao upisuje rege onda on nema user number i zato mora biti opcija pretrage
  // u kojoj ne moram dati user number 
  
  if ( req.params.user_number !== 'none') {
    var user_number = Number(req.params.user_number);
    query = { "car_plates.reg": reg, user_number: { $ne: user_number } };
  } else {
    query = { "car_plates.reg": reg };
  };
  
  // traži usera koji nije ovaj trenutni i koji ima istu registraciju
  User.find(query)
  .select({ full_name: 1, _id: -1 })
  .exec(function(err, useri) {
    if (err) res.send({success: false, msg: 'popup_error_jel_nova_rega', error: err});
    
    if ( useri.length > 0 ) {
      res.json({success: false, msg: 'popup_vec_postoji_reg', useri: useri });
    } else {
      res.json({success: true, msg: 'sve je ok i nitko nije već spremio ovu registraciju' });
    };
    
  });
  
};



exports.update_user_from_db =  function(req, res) {
  
  var user_number = req.params.user_number;
  
  var token = req.body.token || req.query.token || req.headers['x-auth-token'];

      if (token) {
        // verifies secret and checks exp
        jwt.verify(token, 'super_tajna', function(err, decoded) {      
            if (err) {
              return res.json({ success: false, msg: 'Failed to authenticate token.' });    
            } else {


            // find the user
              User.findOne({ _id: decoded._id }, function(err, user) {

                if (err) {
                  res.json({ success: false, msg: 'FIND korisnika nije uspjelo.<br>Dogodila se greška prilikom pretrage usera!' });
                  return;
                };

                if (!user) {
                  // if no user
                  res.json({ success: false, msg: 'FIND korisnika nije uspjelo.<br>Sustav nije pronašao korisnika u bazi!' });
                } 
                else {
                  var clean_user = convert_mongoose_object_to_js_object(user);
                  res.json({ success: true, user: clean_user }); 
                };

              }); // end of User findOne              
              
            }; // kraj else for err od tokena
          
          }); // kraj verify token

      } else {
        
        res.json({ success: false, msg: 'FIND korisnika nije uspjelo. Neuspjela autorizacija !!!!' });
      }; // kraj ako postoji token
  
  
};



