var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');
var nodemailer = require('nodemailer');
var path = require("path");


exports.save_pp_owner = function(req, res) {

  var new_pp_owner = new ParkPlaceOwner(req.body);
  new_pp_owner.save(function(err, pp_owner) {
    if (err) res.send({ success: false, msg: "Vlasnik nije spremljen!", err: err});
    res.json({ success: true, msg: "Vlasnik spremljen", pp_owner_id: pp_owner._id});
  }); // end of spremi parkiralište 

}; // KRAJ SPREMI PARK PLACE 


exports.edit_pp_owner = function(req, res) {
  
  ParkPlaceOwner.findOneAndUpdate({ _id: req.params.pp_owner_id }, req.body, { new: true }, function(err, pp_owner) {
    if (err) res.send({ success: false, msg: "Vlasnik nije ažuriran!", err: err});
    res.json({ success: true, msg: "Vlasnik ažuriran!", pp_id: pp_owner._id});
  });
  
};

