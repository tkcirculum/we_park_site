'use strict';
module.exports = function(app, actions_controller, places_controller) {
  
  // var parkQueries = require('../controllers/park_places_controller');
  var findQueries = require('../controllers/find_controller');

  app.route('/spremi_park_place')
    .post(places_controller.save_park_place);
  
  app.route('/spremi_park_place/:pp_id')
    .put(places_controller.edit_park_place);  
  
  
  app.route('/search_park_places')
    .post(findQueries.cit_search_remote);  
  
  
  app.route('/get_all_park_places')
    .get(places_controller.get_all_park_places);  
  
  
  app.route('/get_specific_pp/:pp_id')
    .get(places_controller.get_specific_pp);   
  

};

