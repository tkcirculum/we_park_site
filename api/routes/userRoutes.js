'use strict';

var cors = require('cors');

module.exports = function(app) {
  
  
  app.use(cors());
  
  var userQueries = require('../controllers/userController');
  var findQueries = require('../controllers/find_controller');

  app.route('/authenticate')
    .post(userQueries.authenticate_a_user);
  
  app.route('/prolong')
    .post(userQueries.prolong_token);  
  
  app.route('/create_user')
    .post(userQueries.create_a_user);
  
  
  app.route('/get_sve_user_brojeve')
    .get(userQueries.get_sve_user_brojeve);
  
  app.route('/confirm_email/:welcome_email_id')
    .get(userQueries.confirm_user_email); 
  
  
  app.route('/edit_user/:user_id')
    .put(userQueries.edit_user); 

  app.route('/search_customers')
    .post(findQueries.cit_search_remote);  
  
  app.route('/search_regs')
    .post(findQueries.cit_search_remote);  
  
  app.route('/user_pass_reset')
    .post(userQueries.user_pass_reset);  
  
  app.route('/pass_reset_confirm/:reset_email_id')
    .get(userQueries.pass_reset_confirm);  
  
    
  app.route('/pass_for_token')
    .post(userQueries.pass_for_token);  
  
  app.route('/set_user_lang')
    .post(userQueries.set_user_lang);  
  
  app.route('/r1_racun_user_podaci')
    .post(userQueries.r1_racun_user_podaci);  
  
  
  app.route('/get_user_business_data/:user_num')
    .get(userQueries.get_user_business_data);  
  
  
  app.route('/check_jel_nova_rega/:user_number/:reg')
    .get(userQueries.check_jel_nova_rega);  
  

  app.route('/update_user_from_db/:user_number')
    .get(userQueries.update_user_from_db);  
  
  
};
