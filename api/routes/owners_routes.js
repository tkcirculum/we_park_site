'use strict';
module.exports = function(app) {
  
  var ownerQueries = require('../controllers/park_places_controller');
  var findQueries = require('../controllers/find_controller');

  /*
  app.route('/spremi_pp_owner')
    .post(ownerQueries.save_pp_owner);
  
  app.route('/spremi_pp_owner/:pp_owner_id')
    .put(ownerQueries.edit_pp_owner);  
  */
  
  app.route('/search_pp_owners')
    .post(findQueries.cit_search_remote);  
  
  
};

