'use strict';
module.exports = function(app) {
  
  var customer_products_queries = require('../controllers/customer_products_controller');
  var findQueries = require('../controllers/find_controller');

  app.route('/save_customer_product')
    .post(customer_products_queries.save_customer_product);
  
  app.route('/edit_customer_product/:customer_product_id')
    .put(customer_products_queries.edit_customer_product);  
  
  
  app.route('/search_customer_products')
    .post(findQueries.cit_search_remote);  
    
  /*
  
  app.route('/import_customer_products')
    .get(customer_products_queries.import_customer_products); 
    
  */
  
  app.route('/get_customer_products_for_table/:user_num')
    .post(customer_products_queries.get_customer_products_for_table);  
  
  
  app.route('/get_user_products_state/:user_num')
    .get(customer_products_queries.get_user_products_state);  
  
  
  app.route('/check_discount_code/:dis_code')
    .get(customer_products_queries.check_discount_code);  
  
  app.route('/create_products')
    .post(customer_products_queries.create_products);  
    
  app.route('/get_active_pp_and_tag_products/:pp_sifra/:tag/:start_time/:end_time')
    .get(customer_products_queries.get_active_pp_and_tag_products);  
  
  
  
};