'use strict';

var pay_card_controller = require('../controllers/pay_card_controller');


module.exports = function(app ) {

  
  app.route('/pay_with_token')
    .post(pay_card_controller.pay_with_token);  
  
  
  app.route('/pay_with_voucher')
    .post(pay_card_controller.pay_with_voucher);  
    
  
  app.route('/pay_card_page')
    .get(pay_card_controller.pay_card_processing);    
  
  app.route('/payment_success')
    .get(pay_card_controller.payment_success);  
  
  
  app.route('/payment_cancel')
    .get(pay_card_controller.payment_cancel);  
    
  
  app.route('/payment_error')
    .get(pay_card_controller.payment_error);  
  
  
/*-------------------- OVO SU GETOVI ZA AJAX DA POŠALJEM TEXT SA ISPRACNIM JEZIKOM ----------------------  */  
  
  app.route('/payment_success_text/:user_lang/:msg_selector')
    .get(pay_card_controller.payment_success_text);  
  
  
  app.route('/payment_cancel_text/:user_lang/:msg_selector')
    .get(pay_card_controller.payment_cancel_text);  
    
  
  app.route('/payment_error_text/:user_lang/:msg_selector')
    .get(pay_card_controller.payment_error_text);  
  
  
  
  app.route('/payment_callback')
    .get(pay_card_controller.payment_callback);    
  
  
};