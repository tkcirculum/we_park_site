'use strict';

var cors = require('cors');

module.exports = function(app) {
  
  
  app.use(cors());
  
  var ble_secret_keys_queries = require('../controllers/ble_secret_keys_controller');
  
  app.route('/ble_secret_keys/:ble_mac')
    .post(ble_secret_keys_queries.secret_from_mac);
  
  
  
};
