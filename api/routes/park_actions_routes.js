'use strict';

var findQueries = require('../controllers/find_controller');


module.exports = function(app, actions_controller, places_controller ) {


  app.route('/edit_park_action/:park_action_id')
    .put(actions_controller.edit_park_action);  
  
  
  app.route('/search_park_actions')
    .post(findQueries.cit_search_remote);  
  
  /*
  app.route('/import_actions')
    .get(park_actions_queries.import_actions); 
  */
  
  app.route('/get_customer_park_actions/:user_num')
    .post(actions_controller.get_customer_park_actions);  

  
  app.route('/get_active_booking_or_parking/:pp_sifra/:user_num')
    .get(actions_controller.get_active_booking_or_parking);  
  
  
  app.route('/new_booking')
    .post(actions_controller.new_booking);
  
  app.route('/cancel_booking/:pp_sifra/:user_number')
    .get(actions_controller.cancel_booking);  

  app.route('/enter_park')
    .post(actions_controller.enter_park);
  
  
  app.route('/portir_avail_new_state')
    .post(actions_controller.portir_avail_new_state);
  
  
  app.route('/exit_park/:pp_sifra/:user_number')
    .get(actions_controller.exit_park); 
  
  app.route('/request_check_park')
    .post(actions_controller.request_check_park);  
  
  
  app.route('/search_actions')
    .post(findQueries.cit_search_remote);
  
  app.route('/create_excel/:timestamp')
    .get(findQueries.create_excel);
  
  app.route('/stats_search_customers')
    .post(findQueries.cit_search_remote);
  
  app.route('/picam_enter_pp/:user_number/:ramp_name/:just_check_access/:reg')
    .get(actions_controller.picam_enter_pp);
  
  // todo --- nije jos gotovo !!!!!!!
  app.route('/actions_mjesecno/:start/:end')
    .get(actions_controller.actions_mjesecno);
  
};