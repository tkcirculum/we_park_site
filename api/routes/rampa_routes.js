'use strict';

var cors = require('cors');

module.exports = function(app, rampa_controller) {
  
  
  app.use(cors());

  app.route('/ping')
    .get(rampa_controller.ping);
  
  
  app.route('/all_plates')
    .get(rampa_controller.all_plates);
  
    
  app.route('/save_new_user_plates')
    .post(rampa_controller.save_new_user_plates);
  
  /*
  app.route('/emit_new_plates')
    .post(rampa_controller.emit_new_plates);
  */
    
  
  app.route('/open_rampa/:ime_rampe/:user_number')
    .get(rampa_controller.open_rampa);
  
  
  // samo za T-COM
  app.route('/open_bar/:ime_rampe/:user_number')
    .get(rampa_controller.open_rampa);
  
  
  
  app.route('/close_zero/:ime_rampe/:user_number')
    .get(rampa_controller.close_zero);
    
  // samo za T-COM
  app.route('/close_bar/:ime_rampe/:user_number')
    .get(rampa_controller.close_zero);
  
  
  app.route('/fixed_open_rampa/:ime_rampe/:new_state')
    .get(rampa_controller.fixed_open_rampa);
  
  
  app.route('/fixed_close_rampa/:ime_rampe/:new_state')
    .get(rampa_controller.fixed_close_rampa);
  
  
  app.route('/camera_preview/:ime_rampe/:start_or_stop')
    .post(rampa_controller.camera_preview);
  
    
  app.route('/get_last_distort_command/:ime_rampe')
    .get(rampa_controller.get_last_distort_command);
  
  
  app.route('/get_solar_data')
    .post(rampa_controller.get_solar_data);  

  
  app.route('/motion_detect/:ime_rampe/:start_or_stop')
    .get(rampa_controller.motion_detect);


  app.route('/save_new_user_plates')
    .post(rampa_controller.save_new_user_plates);

    
  app.route('/move_shelly/:pp_sifra/:shelly_name/:user_number/:up_or_down')
    .get(rampa_controller.move_shelly);

  
  app.route('/get_db_barrier_state/:pp_sifra/:user_number')
    .get(rampa_controller.get_db_barrier_state);     
  
  app.route('/update_db_barrier_state/:pp_sifra/:bar_name/:user_number/:up_or_down')
    .get(rampa_controller.update_db_barrier_state);

  
  app.route('/check_fixed_states/:ime_rampe')
    .get(rampa_controller.check_fixed_states);
  
  app.route('/check_fixed_barriers/:bar_name')
    .get(rampa_controller.check_fixed_barriers);
  
  
  
  app.route('/check_bar_position/:bar_name')
    .get(rampa_controller.check_bar_position);
  
    
  app.route('/metascript_contact_form')
    .post(rampa_controller.metascript_contact_form);
  
  
  
  
  
  
  
  
};
