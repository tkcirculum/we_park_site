
function create_status_email (  email_subject, email_body ) {

  
  var email_body = `<div style="width: 100%; float: left; text-align: center;">STATUS </div>
                    <div style="  width: 100%;
                                  float: left;
                                  font-weight: 700;
                                  text-align: center;
                                  font-size: 13px;
                                  margin-bottom: 10px;">${ email_body }</div>`;  


  var html = `
  
<div style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #f3fcff;
            padding: 20px 0 0;">

    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                padding: 3px;">

 
      <div style="  font-size: 16px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    color: #1a2840;
                    font-weight: 400;
                    line-height: 22px;
                    letter-spacing: normal;">
        
        ${ email_subject } <br>
      </div>

      <div style="    
        font-size: 13px;
        margin-top: 40px;
        margin-bottom: 20px;
        color: #454545;
        font-weight: 400;
        text-align: left;
        line-height: 26px;
        max-width: 400px;
        clear: both;
        width: auto;
        margin: 0 auto;
        padding: 10px;
        height: auto;
        background-color: #fff;
        border-radius: 10px;
        border: 1px solid #e8e6e6;
        letter-spacing: 0.04rem;">
        

<br style="display: block; clear: both;">
        
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 
${email_body}

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>

<br style="display: block; clear: both;">
        
</div>

<br style="display: block; clear: both;">
      
</div>


    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #f3fcff;
                padding: 0;">

      
      
        <img style="
          width: 150px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="WePark Logo" 
          src="https://wepark.circulum.it:9000/img/we_park_logo_email.png" />



      <div id="u_${Date.now() + 123}" style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 18px;
        border-top: 1px solid #eaeaea;
        padding-top: 10px;
        background-color: #fff;
        letter-spacing: 0.06rem;">

          <br>
          <br>
          <b style="font-size: 16px;" id="u_${Date.now() + 222}" >MOLIMO ZA STRPLJENJE</b><br>
          <b style="font-size: 16px;">Odgovorne osobe rade na otklonu problema.</b><br><br>
          <b style="font-size: 16px;">Ako imate bilo koji komentar ili napomenu, molimo nazovite broj:</b><br>
          <a  href="tel:00385923133015" 
              style=" padding: 12px;
                      text-decoration: none;
                      color: #fff;
                      background-color: #e64531;
                      margin: 10px auto;
                      display: block;
                      border-radius: 20px;
                      font-size: 20px;
                      font-weight: 700;
                      max-width: 250px;">00385 92 3133 015</a>
        
        <br>

          <b>Wepark smart parking d.o.o.</b><br>
          <b>OIB:</b>&nbsp;77372456863<br>
          <b>HQ:</b>&nbsp;Žumberačka 32A, Zagreb<br>
          <b>OFFICE:</b>&nbsp;Janka Rakuše 1, Zagreb<br>
          <b>INFO TEL:</b>&nbsp;00385 92 3133 015<br>
          <b>INFO MAIL:</b>&nbsp;info@wepark.eu<br>
          <b>WEB:</b>&nbsp;www.wepark.eu<br>
          <br>
          <div id="u_${Date.now() + 555}" style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
          Društvo je upisano u sudski registar Trgovačkog suda u Zagrebu pod brojem 081131571. Temeljni kapital u iznosu od 20.000 kn je uplaćen u cijelosti. Račun se vodi kod Privredne Banke Zagreb. Član društva Karlo Starčević.
          </div>
          <br style="display: block; clear: both;">

        
      </div>        
      
      <br style="display: block; clear: both;">
    </div>  

    
</div>     
  
  
`;
  
  return html;
  
};  
  