#!/bin/bash

# VAZNO !!!!!!
# prilikom restora mongodb filova potrebno je imati ovu verziju baze 
# ( upisi ovo ispod i dobit ces trenutnu verziju mongodb servera tj. mongod procesa ):
# dpkg -s mongodb-org-server | grep Version
# rezultat:
# Version: 4.2.2
# Config-Version: 4.2.2

# naredba za pregledavanje svih cron jobs
# sudo ps -o pid,sess,cmd afx | egrep -A20 "( |/)cron( -f)?$"

# otvaranje cron file-a sa nano editorom !!!
# export VISUAL=nano; crontab -e

set -o errexit

cd /home/toni/we_baza_bckp

TIME_STAMP=$(date +%Y%m%d_%H%M%S)
mkdir $TIME_STAMP

sudo chmod -R 777 $TIME_STAMP

sudo cp -a /var/lib/mongodb/. /home/toni/we_baza_bckp/$TIME_STAMP

cd /home/toni/we_baza_bckp/$TIME_STAMP

sudo chmod -R 777 *

# izlazim u folder od backapa
cd ..


tar -czvf $TIME_STAMP.tar.gz /home/toni/we_baza_bckp/$TIME_STAMP/

rm -rf /home/toni/we_baza_bckp/$TIME_STAMP

ls -t | tail -n +11 | xargs rm --


