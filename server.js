process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

var server_settings = require('./settings');

var path = require('path');

var express = require('express');
var cors = require('cors');
var app = express();
var https = require('https');
var server = require('http').Server(app);

var fs = require('fs'), fileStream;

var Imap = require('imap');
var inspect = require('util').inspect;
 

var imap_settings = server_settings.imap_settings;


var imap = new Imap();

 
function openInbox(cb) {
  imap.openBox('INBOX', true, cb);
};

global.check_we_mail = function() {

  imap.once('ready', function() {

openInbox(function(err, box) {
  if (err) throw err;
  // , ['SINCE', 'October 23, 2020'] 
  imap.search(['UNSEEN'], function(err, results) {
    
    if (err) throw err;
    
    if (!results || results.length == 0 ) {
      console.log("NEMA NOVIH MAILOVA !!!!!");
      imap.end();
      return;
    }
    
    var f = imap.fetch(results, { bodies: '' });
    
    f.on('message', function(msg, seqno) {
      console.log('Message #%d', seqno);
      var prefix = '(#' + seqno + ') ';
      msg.on('body', function(stream, info) {
        console.log(prefix + 'Body');
        stream.pipe(fs.createWriteStream('msg-' + seqno + '-body.txt'));
      });
      msg.once('attributes', function(attrs) {
        console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
      });
      msg.once('end', function() {
        console.log(prefix + 'Finished');
      });
    });
    f.once('error', function(err) {
      console.log('Fetch error: ' + err);
    });
    f.once('end', function() {
      console.log('Done fetching all messages!');
      imap.end();
    });
  });
});
    
    
  });

  imap.once('error', function(err) {
    console.log(err);
  });

  imap.once('end', function() {
    console.log('Connection ended');
  });

  imap.connect();

};


var privateKey = null;
var certificate = null;
var ca = null;
var https_server = null;

global.we_io = null;

if ( server_settings.ip_address !== '0.0.0.0' ) {

  privateKey = fs.readFileSync('privkey1.pem', 'utf8');
  certificate = fs.readFileSync('cert1.pem', 'utf8');
  ca = fs.readFileSync('chain1.pem', 'utf8');

  https_server = https.createServer({
      key: privateKey,
      cert: certificate,
      ca: ca
    }, app);

  global.we_io = require('socket.io')(https_server);
  
} else {
  
  global.we_io = require('socket.io')(server);
  
};


global.dev_or_prod = 'prod';

global.form_ShopID  = server_settings.form_ShopID;
global.form_SecretKey = server_settings.form_SecretKey;

global.token_ShopID  = server_settings.token_ShopID;
global.token_SecretKey = server_settings.token_SecretKey;


// all park_places

global.park_places_list = [];



/*
global.we_email_address = 'toni@circulum.it';
global.we_mail_setup = {
  host: 'mail.circulum.it',
  port: 587,
  secure: false, // use TLS,
  requireTLS: true,
  auth: {
    user: global.we_email_address,
    pass: 'ToNi3007'
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
};
*/

global.we_email_address = 'app@wepark.eu';

global.we_mail_setup = server_settings.we_mail_setup;

// these methods all returned promise
// checkAuth will auto invoke and it will check smtp auth

  

if ( global.dev_or_prod == 'dev' ) {
  
  
// global.we_payment_url = 'http://10.10.65.156:8080'; 
global.we_payment_url = 'https://wepark.circulum.it:9000';  

/*  
if ( server_settings.ip_address !== '0.0.0.0' ) {
  global.we_payment_url = 'https://wepark.circulum.it:9000';  
};
*/

  
global.wspay_form_url = 'https://formtest.wspay.biz/Authorization.aspx';
global.wspay_token_url = 'https://test.wspay.biz/api/services/ProcessPayment';  
  
global.fiskal_url = 'https://cistest.apis-it.hr:8449/FiskalizacijaServiceTest';   
global.fis_cer_p12_path = '/dev_FISKAL_1.p12';
global.fis_private_key_path = '/dev_private.pem';
  
global.fiskal_cert = 
`MIIGqDCCBJCgAwIBAgIRAJH1KGerlAfAAAAAAF2lbk0wDQYJKoZIhvcNAQELBQAw
SDELMAkGA1UEBhMCSFIxHTAbBgNVBAoTFEZpbmFuY2lqc2thIGFnZW5jaWphMRow
GAYDVQQDExFGaW5hIERlbW8gQ0EgMjAxNDAeFw0xOTEyMTExMDE3MTRaFw0yMTEy
MTExMDE3MTRaMGUxCzAJBgNVBAYTAkhSMTIwMAYDVQQKEylXRVBBUksgU01BUlQg
UEFSS0lORyBELk8uTy4gSFI3NzM3MjQ1Njg2MzEPMA0GA1UEBxMGWkFHUkVCMREw
DwYDVQQDEwhGSVNLQUwgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
ANIr7G0tXX3Qu53Jvqdzh3PnQPiG7Lh/tOgZ8UygHNJePK7COgoleVLt44DDNUM6
E5pUqe3aBV0H+LfxvXJy6LDue0VikWDYaP5frq/mu6kmxUm4Xr7R1Ogtpkek4PXo
iYEkX17BdQ79Vg7regVCX4pLfy/3974rnugT06dXBnmQhCa6xlvEjJ3Dj0T7qaKf
HQrflO2lksJzvYBekWecSb8H1W5vCnyoHxD1UyOvc5eUWcGys1PCvbjiAmgpIz7E
dPc/9cIb1J7naxvvRxcvNlMMt9oaHaTGM4XalrdlwP+Di3kz+VdlTNt/mBbwbeSw
1UQWk29cPr6zObmjsYRUTk8CAwEAAaOCAm4wggJqMA4GA1UdDwEB/wQEAwIEsDAd
BgNVHSUEFjAUBggrBgEFBQcDBAYIKwYBBQUHAwIwUwYDVR0gBEwwSjBIBgkrfIhQ
BSAFAwEwOzA5BggrBgEFBQcCARYtaHR0cDovL2RlbW8tcGtpLmZpbmEuaHIvY3Av
Y3BkZW1vMjAxNHYxLTAucGRmMH0GCCsGAQUFBwEBBHEwbzAoBggrBgEFBQcwAYYc
aHR0cDovL2RlbW8yMDE0LW9jc3AuZmluYS5ocjBDBggrBgEFBQcwAoY3aHR0cDov
L2RlbW8tcGtpLmZpbmEuaHIvY2VydGlmaWthdGkvZGVtbzIwMTRfc3ViX2NhLmNl
cjCCARgGA1UdHwSCAQ8wggELMIGmoIGjoIGghihodHRwOi8vZGVtby1wa2kuZmlu
YS5oci9jcmwvZGVtbzIwMTQuY3JshnRsZGFwOi8vZGVtby1sZGFwLmZpbmEuaHIv
Y249RmluYSUyMERlbW8lMjBDQSUyMDIwMTQsbz1GaW5hbmNpanNrYSUyMGFnZW5j
aWphLGM9SFI/Y2VydGlmaWNhdGVSZXZvY2F0aW9uTGlzdCUzQmJpbmFyeTBgoF6g
XKRaMFgxCzAJBgNVBAYTAkhSMR0wGwYDVQQKExRGaW5hbmNpanNrYSBhZ2VuY2lq
YTEaMBgGA1UEAxMRRmluYSBEZW1vIENBIDIwMTQxDjAMBgNVBAMTBUNSTDE2MB8G
A1UdIwQYMBaAFDuEWhT1xTzhSDtd0Sc1e9VlvA4qMB0GA1UdDgQWBBR2ws9Oxp7K
eoxs39OlwLakBYkRkTAJBgNVHRMEAjAAMA0GCSqGSIb3DQEBCwUAA4ICAQBT2q0j
JkEYCRBIoxGH1nFqU/xB+1InhKx0TM0zgrgSNVg7C4rYhCbwIcQ8+UljnJE4Mrez
HDQFAH/zjDcEw8P52y8td4vXZrxBW8q74tX5y/mI9mXYdoFZfhALQRmgyWcXWEry
ToGBCqPTOLSCqvn0GkyK4lo1+KvqnVLMQF2AhowehSfyBRxcqJFLqmtUZJzVt3jE
BWjDBO3msGgFY1FvVAqecb+dAegRvDvD1MQHNUhsLF6Hut43CXVC+Jcyi0qo2n4n
Sda18eX5fXIXzI+WDYMC0iHg1BCuGWXZuWXIOQ835q5kgDEDHxbN3y5Hgcm3nOYn
nYb1J8af47P5P+NJFJTHIpudSbjlyhYQAm0Gr51BVZ4CvAuMfg+TEfVwyyTlQSPp
Z3gjQ6Zmub8RvzoJ+igVymu0LeBu6BEiifywaElIj5KYoI0XDc32Ciy4x+Eir7Cl
+Uw5gKnWJ1jSj4CZvoYfV1X+A2IXoaFwuz5JzcPxxGEff2/3dA0ccVWBWA8edUkx
rVDHNDnkNm+WvOolWdeNitHsYLa7mkG0sm24DO+NVszmJEioCtn2A4SqCjb5gnJS
1+yNsksjPeafy1qq2SbOvJJbuM2O6O6iBdWGKOGPV2Gy3itR4pK1R0ls52mMKhn1
ovhkRGm312jvFDj7+GgFU3QAFjvvAp045SrIvQ==`;  
  
  
} 
else if ( global.dev_or_prod == 'prod' ) {
  


// PROD:
global.we_payment_url = 'https://wepark.circulum.it:9000';
  
global.wspay_form_url = 'https://form.wspay.biz/Authorization.aspx';
global.wspay_token_url = 'https://secure.wspay.biz/api/services/ProcessPayment';  

global.fiskal_url = 'https://cis.porezna-uprava.hr:8449/FiskalizacijaService';
global.fis_cer_p12_path = server_settings.we_mail_setup;
global.fis_private_key_path = server_settings.we_mail_setup;
  
global.fiskal_cert = server_settings.fiskal_cert;
  
  
};


global.appRoot = path.resolve(__dirname);


var port = process.env.PORT || server_settings.port;

var mongoose = require('mongoose');

mongoose.set('useFindAndModify', false);

var multer  = require('multer');
var bodyParser = require('body-parser');


var jwt = require('jsonwebtoken');
var scramblePass = require('password-hash-and-salt');

var UserModel = require('./api/models/userModel');
var UserCounterModel = require('./api/models/userCounterModel');

var Racun_Counter_Model = require('./api/models/racun_counter_model');


var park_places_model = require('./api/models/park_places_model');
var park_actions_model = require('./api/models/park_actions_model');
var stats_model_model = require('./api/models/stats_model');
var customer_product_model = require('./api/models/customers_products_model');
var discount_code_model = require('./api/models/discount_code_model');
var pay_card_model = require('./api/models/pay_card_model');

var ble_secret_keys_model = require('./api/models/ble_secret_keys_model');


var user_lang_model = require('./api/models/user_lang_model');

var solar_model = require('./api/models/solar_data_model');
var solar_days_model = require('./api/models/solar_data_days_model');

var cameras_model = require('./api/models/cameras_model');

/*

var packs_model = require('./api/models/packs_model');
var stats_model = require('./api/models/stats_model');
*/
// configure app to use bodyParser()
// this will let us get the data from a POST

/*
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
*/


/*
--------------------
var crypto;
try {
  crypto = require('crypto');
} catch (err) {
  console.log('crypto support is disabled!');
};
--------------------
*/


app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));


/*  
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
*/

app.set('json spaces', 2);

app.use(cors());

mongoose.Promise = global.Promise;


global.conn_to_db = function() {

  var conn = mongoose.connect(
    server_settings.data_base_url,
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(
    (conn) => {
      console.log("Conected to wepark DB");
    },
    err => {
      console.log("NOT Conected to wepark DB. This is ERROR:");
      console.log(err);
    }
  );

};

global.conn_to_db();


/*
var page = 
`<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>The HTML5 Herald</title>
  <meta name="description" content="Login Page">
  <meta name="author" content="Repuzzlic Ltd">
  <link rel="stylesheet" href="/css/proba.css?v=1.0">
</head>
<body>
  THIS IS JUST A TEST <br>
  OF REDIRECTION TO LOGIN PAGE
</body>
</html>
`;
*/



/* ------- START ------------------- THIS IS GATEKEEPER !!!!! --------------------------*/

function gatekeeper(req, res, next) {

  // we can specify urls for triggering this gatekeeper
  // for example /secure
  // we can include multiple urls with ||
  // also we can use regExp to pin-point specific url


  // in case url contains parameters like /secure?bla=sdasda&....
  var pure_url = req.url;
  if (req.url.indexOf('?') > -1) {
    pure_url = req.url.split('?')[0]; 
  };
  // EXAMPLE: for /secure path token is required !!!!  
  
  // TODO - this is just for test
  // make real paths later !!!!!
  if ( pure_url.indexOf('/edit_user/') > -1 ) {
    // 1) token can be part of ajax body
    // 2) or part of URL (as url string parameter) 
    // 3) or ajax call header property
    var token = req.body.token || req.query.token || req.headers['x-auth-token'];

      if (token) {
        // verifies secret and checks exp
        jwt.verify(token, 'super_tajna', function(err, decoded) {      
            if (err) {
              return res.json({ success: false, msg: 'Failed to authenticate token.' });    
            } else {
              // if everything is good, save to request for use in other routes
              req.decoded = decoded; 
              // ovo je id od Circulum usera  !!!!
              if ( decoded._id == "5e2f544dbd4d320aafdbf654" ) req.is_super_admin = true;
              
              // next();
            };
          });

        } else {

          // if there is no token
          // return an error
          
          return res.status(403).send({ 
              success: false, 
              msg: 'No token provided, Or token is expired!\n\nPlease Login and try again!' 
          });
          // res.redirect('/login');
        };
    
    

  };

  if (req.url == '/login') {
      
      // this is just exapmle of sending raw html
      // res.set('content-type','text/html');
      // res.send(page);  

  };
  
  next();
    
  };


// ATTACH GATE KEEPER TO ALL ROUTES !!!!!!!!!!!
app.route('/*')
    .get(gatekeeper)
    .post(gatekeeper)
    .put(gatekeeper);


/* ------- END -------------------  GATEKEEPER --------------------------*/

global.after_gate = false;


var ble_secret_key_routes = require('./api/routes/ble_secret_keys_routes');
ble_secret_key_routes(app);


var pay_card_controller = require('./api/controllers/pay_card_controller');
var pay_card_routes = require('./api/routes/pay_card_routes');
pay_card_routes(app, pay_card_controller);


var user_routes = require('./api/routes/userRoutes');
user_routes(app);

var UserLang = mongoose.model('UserLang');


function convert_mongoose_object_to_js_object(mongoose_object) {
  
  var js_object = mongoose_object.toJSON();
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};


global.all_user_langs = {};

global.user_langs_are_set = false;

if ( global.user_langs_are_set == false ) {
  
  global.user_langs_are_set = true;
  
  UserLang.find({}, function(err, all_langs) {
    if (err) {
      console.log('ERROR IN FIND ALL USER LANGS !');
      console.log(err);
      global.user_langs_are_set = false;
    } else {
      all_langs.forEach( function(lang, lang_index) {
        var clean_lang = convert_mongoose_object_to_js_object(lang);
        global.all_user_langs[clean_lang.user_number] = clean_lang.lang;
      });
      global.user_langs_are_set = true;
    };
  }); // end of save export
};



var actions_controller = require('./api/controllers/park_actions_controller');
var places_controller = require('./api/controllers/park_places_controller');

var fiskal_controller = require('./api/controllers/fiskal_controller');


var park_places_routes = require('./api/routes/park_places_routes');
park_places_routes(app, actions_controller, places_controller);


var park_actions_routes = require('./api/routes/park_actions_routes');
park_actions_routes(app, actions_controller, places_controller);


var customer_products_routes = require('./api/routes/customer_products_routes');
customer_products_routes(app);


var pay_card_routes = require('./api/routes/pay_card_routes');
pay_card_routes(app);


var owners_routes = require('./api/routes/owners_routes');
owners_routes(app);



var rampa_controller = require('./api/controllers/rampa_controller.js');
var rampa_routes = require('./api/routes/rampa_routes');
rampa_routes(app, rampa_controller);


var file_storage = multer.diskStorage({
  destination: function (req, file, cb) {
   cb(null, path.join(__dirname + '/public/img/file_upload/'))
   },
   filename: function (req, file, cb) {
     cb( null, Date.now() + "-" + file.originalname) 
   }
}); // prije sam koristio ovo : file.originalname.replace(/-/g, "_") jer sam se bojao da ce zbog crtica u imenu file-a program krivo odrezati ime od prefixa
// ali to nije dobra logika jer kasnije radi probleme u usporedjivanju imena !!!!!!!
 
var file_att = multer({ storage: file_storage });


// app.post('/file_upload', multer_upload );

// var proizvodi_routes = require('./api/routes/proizvodiRoutes');
// proizvodi_routes(app, file_att);

// var kupci_routes = require('./api/routes/kupacRoutes');
// kupci_routes(app);

app.use('/', express.static(path.join(__dirname, '/public'), {dotfiles:'allow'} ) );


/*
MOŽDA KASNIJE ZATREBA !!!!
ovo ide iza path.join(__dirname, '/public')

,
  {
    setHeaders: function(res, path) {
      if ( path.indexOf(".woff2") !== -1 || path.indexOf(".woff") !== -1 ) {
        res.type("application/x-font-woff");
      };
      
      if ( path.indexOf(".ttf") !== -1 ) {
        res.type("application/octet-stream");
      };
      
    }
  }
  
*/


app.use('*', function(req, res, next) {
  var contype = req.headers['content-type'];
  if ( contype && contype.indexOf('application/json') > -1 ) {
    // return res.send({error: 'No such end point to return json: ' + req.originalUrl  });
    return res.send({msg: 'NOT ACTIVE' });
  };
  next();
});

app.use('*', express.static(path.join(__dirname, '/public/404'), {dotfiles:'allow'} ) );


if ( server_settings.ip_address !== '0.0.0.0' ) {

  https_server.listen(port, server_settings.ip_address);

} else {

  server.listen(port, server_settings.ip_address);

};

console.log('WE PARK ADMIN started at ' + server_settings.ip_address + ': ' + port);




// ---------------- GLOBALNE FUNKCIJE
// ---------------- GLOBALNE FUNKCIJE
// ---------------- GLOBALNE FUNKCIJE

 global.getMeADateAndTime = function(number, cit_format) {
   
  var today = null;

  // ako nije undefined ili null onda napravi datum od broja 
  if ( typeof number !== 'undefined' && number !== null ) {
    today = new Date( number );  
  } else {
    today = new Date();  
  };

  var sek = today.getSeconds();
  var min = today.getMinutes();
  var hr = today.getHours();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; // January is 0!
  var yyyy = today.getFullYear();

  // ako je samo jedna znamenka
  if (dd < 10) {
    dd = '0' + dd;
  };
  // ako je samo jedna znamenka
  if (mm < 10) {
    mm = '0' + mm;
  };

  // ako je samo jedna znamenka
  if (sek < 10) {
    sek = '0' + sek;
  };


  // ako je samo jedna znamenka
  if (min < 10) {
    min = '0' + min;
  };

  // ako je samo jedna znamenka
  if (hr < 10) {
    hr = '0' + hr;
  };

  if ( cit_format == 'd.m.y' ) {
    today =  dd + '.' + mm + '.' + yyyy;
  };


  // ako nije neveden format ili ako je sa crticama
  if ( cit_format == 'y_m_d' ) {
    today = yyyy + '_' + mm + '_' + dd;
  };  

  // ako nije neveden format ili ako je sa crticama
  if ( !cit_format || cit_format == 'y-m-d' ) {
    today = yyyy + '-' + mm + '-' + dd;
  };


  time = hr + ':' + min + ':' + sek;

  // EXPERIMENTAL !!!!!!:
  // stavio sam mogućnost da mi upiše datum / vrijeme na svim elementima koji imaju 
  // istu klasu kao ove - tj ako u html-u stoji element sa ovim klasama i ako na onload pokrenem ovu funckiju
  // sadržaj svakog elementa će dobiti datum / vrijeme ovisno o klasi koju ima

  // $(".getMeADate").html(today);
  // $(".getMeATime").html(time);

  return {
    datum: today,
    vrijeme: time
  }

  };
  

